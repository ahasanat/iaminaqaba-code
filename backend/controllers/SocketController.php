<?php

namespace backend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter; 
use yii\filters\AccessControl;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Expression;
use backend\models\Messages;

use frontend\components\ExSession;
/**
 * Site controller
 */
class MessagesController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','message','chat'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
	
	public function beforeAction($action)
	{   
        $this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

    /**
     * Create auth action
     *
     * @return mixed
     */
    public function actions() {
        return [ 
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this,
                    'oAuthSuccess'],
            ],
            'captcha' => [
                //'class' => 'yii\captcha\CaptchaAction',
                'class' => 'mdm\captcha\CaptchaAction',
                'level' => 1,
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function actionMessage()
    {
		$model = new Messages();
        return $this->render('message');
    }

	public function actionChat()
    {
        $model = new Messages();
        return $this->render('chat');
    }	
	
    public function actionListAllUserInfo() {
        $session = Yii::$app->session;
        $email = $session->get('email_id');
        $user_id = (string)$session->get('user_id');
        
        if(isset($_POST) && !empty($_POST)) {
            if((isset($email) && !empty($email)) && isset($user_id) && !empty($user_id)) {
                $post = $_POST;
                $users = Messages::getAllUsers($post);

            }
        }
    }

    public function actionForSelf() {
        $session = Yii::$app->session;
        $email = $session->get('email_id');
        $user_id = (string)$session->get('user_id'); 

        $post = (json_decode($_POST['post'], true));
        $newPost = Messages::getInfoForSelf($post);

        return json_encode($newPost, true);
        exit;
    }

    public function actionForAll() {
        $session = Yii::$app->session;
        $email = $session->get('email_id');
        $user_id = (string)$session->get('user_id'); 

        $post = (json_decode($_POST['post'], true));
        $newPost = Messages::getInfoForAll($post);

        return json_encode($newPost, true);
        exit;
    }


    public function actionRecentMessageUserInformation() {
        $session = Yii::$app->session; 
        $email = $session->get('email_id');
        $user_id = (string)$session->get('user_id');
        //$post = (json_decode($_POST['post'], true));
        $post = $_POST['post'];
        $newPost = Messages::getRecentMessageUserInformation($post, $user_id);

        return json_encode($newPost, true);
        exit;
    }
	
    public function actionLoadHistory() {
        $session = Yii::$app->session; 
        $email = $session->get('email_id');
        $user_id = (string)$session->get('user_id');

        $post = (json_decode($_POST['post'], true));
        $newPost = Messages::getLoadHistory($post);
        
        return json_encode($newPost, true);
        exit;
    }

}
