<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use frontend\models\Comment;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use backend\models\Mail;
/**
 * Site controller
 */
class MailController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','deletecomment','flagpost'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('mail');
    }
	
    public function actionAddmail()
    {
        //print_r($_POST);exit;
        $model = new Mail();
        if(isset($_POST) && !empty($_POST))
        {
            $date = time();
            $name = $_POST['name'];
            $subject = $_POST['subject'];
            $mailbody = $_POST['mailbody'];
            $sendername = $_POST['sendername'];
            $senderemail = $_POST['senderemail'];
            $busnameexist = Mail::find()->where(['tpl_name' => $name ])->one();
            if($busnameexist)
            {
                return 'exist';
            }
            else
            {
                $record = new \backend\models\Mail;
                $record->tpl_name = $name;
                $record->tpl_subject = $subject;
                $record->tpl_body = $mailbody;
                $record->sendername = $sendername;
                $record->senderemail = $senderemail;
                $record->added_on = "$date";
                $record->insert();
                return 'insert';   
            }
        }
    }
    
    public function actionSendmails()
    {
        //print_r($_POST);exit;
        if(isset($_POST) && !empty($_POST))
        {
            $id = $_POST['id'];
            $mt_exist = Mail::find()->where(['_id' => $id ])->one();
            if($mt_exist) 
            {
                try
                {
                    $sendername = $mt_exist['sendername'];
                    $senderemail = $mt_exist['senderemail'];
                    $tpl_subject = $mt_exist['tpl_subject'];
                    $tpl_body = $mt_exist['tpl_body'];
                    $sendto = 'adelhasanat@yahoo.com';
                    $body = '<html>
                                <head>
                                    <meta charset="utf-8" />
                                    <title>Iaminaqaba</title>
                                </head>
                                <body style="margin:0;padding:0;background:#dfdfdf;">
                                    <div style="color: #353535; float:left; font-size: 13px;width:100%; font-family:Arial, Helvetica, sans-serif;text-align:center;padding:40px 0 0;">
                                        <div style="width:600px;display:inline-block;">
                                            <img src="https://www.iaminaqaba.com/frontend/web/assets/baf1a2d0/images/black-logo.png" style="margin:0 0 10px;width:130px;float:left;"/>
                                            <div style="clear:both"></div>
                                            <div style="border:1px solid #ddd;margin:0 0 10px;">
                                                <div style="background:#fff;padding:20px;border-top:10px solid #333;text-align:left;">'.$tpl_body.'</div>
                                            </div>
                                            <div style="clear:both"></div>
                                            <div style="width:600px;display:inline-block;font-size:11px;">
                                                <div style="color: #777;text-align: left;">&copy;  <a href="https://www.iaminaqaba.com" style="text-decoration:none">www.iaminaqaba.com</a> All rights reserved.</div>
                                                <div style="text-align: left;width: 100%;margin:5px  0 0;color:#777;">For support, you can reach us directly at <a href="mailto:csupport@iaminaqaba.com" style="color:#4083BF;text-decoration:none">csupport@iaminaqaba.com</a></div>
                                            </div>
                                        </div>
                                    </div>
                                </body>
                            </html>';
                    $test = Yii::$app->mailer->compose()
                        ->setFrom(array($senderemail => $sendername))
                        ->setTo($sendto)
                        ->setSubject($tpl_subject)
                        ->setHtmlBody($body)
                        /*->setBcc($sendbcc)
                        ->attach($attachfile)*/
                        ->send();
                    return true;
                }
                catch (ErrorException $e)
                {
                    return false;
                }
            }
            else
            {
                return false;   
            }
        }
        else
        {
            return false;   
        }
    }

    public function actionRemove()
    {
        //print_r($_POST);exit;
        if(isset($_POST) && !empty($_POST))
        {
            $id = $_POST['id'];
            $mt_exist = Mail::find()->where(['_id' => $id ])->one();
            if($mt_exist->delete())
            {
                return true;
            }
            else
            {
                return false;   
            }
        }
        else
        {
            return false;   
        }
    }
    
    public function actionSend()
    {
        $model = new Mail();
        $mails = $model->getAll();
        return $this->render('sendmail',['mails' =>$mails]);
    }
	
    public function actionOut()
    {
        \Yii::$app->user->logout();
        return $this->goHome();
    }

   
}
