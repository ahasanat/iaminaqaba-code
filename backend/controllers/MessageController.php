<?php

namespace backend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\filters\VerbFilter; 
use yii\filters\AccessControl;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Expression;
use backend\models\Messages;

use frontend\components\ExSession;
use backend\models\Message;
use backend\models\GiftCost;
/**
 * Site controller
 */
class MessageController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['updatecost', 'Editupdatecost','messageabuse','godeleteabuse'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    /**
     * Create auth action
     *
     * @return mixed
     */
    public function actions() {
        return [ 
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this,
                    'oAuthSuccess'],
            ],
            'captcha' => [
                //'class' => 'yii\captcha\CaptchaAction',
                'class' => 'mdm\captcha\CaptchaAction',
                'level' => 1,
            ],
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    
    public function actionUpdatecost()
    {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            $information = GiftCost::UpdateCost($uId);
            return $this->render('updatecost', array('information' => $information));
            exit;
        }

    }

    public function actionEditupdatecost()
    {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            if(isset($_POST['$price']) && $_POST['$price'] != '') {
                $price = $_POST['$price'];
                $information = GiftCost::EditUpdateCost($uId, $price);
                return $information;
                exit;
            }
        }
    }

    public function actionImages()
    {

        return $this->render('images');
        /*$session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            $information = GiftCost::UpdateCost($uId);
            return $this->render('updatecost', array('information' => $information));
            exit;
        }*/

    }

    public function actionAbuse() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email'); 
        if(isset($uId)) {
            $information = GiftCost::abuseReport($uId);
            return $this->render('abuse', array('information' => $information));
            exit;
        }
    }
    
    public function actionGodeleteabuse() {
        $session = Yii::$app->session;
        $uId = (string)$session->get('email');
        if(isset($uId)) {
            if(isset($_POST['$id']) && $_POST['$id'] != '') {
                $id = $_POST['$id'];
                $information = GiftCost::godeleteabuse($uId, $id);
                return $information;
                exit;
            }
        }
    }
  

}
