<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use backend\models\LoginForm;
use backend\models\Userpost;
use frontend\models\Comment;
use frontend\models\Messages;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\mongodb\Expression;
use backend\components\ExSession;
use frontend\models\Notification;
use frontend\models\ReportPost;
/**
 * Site controller
 */
class PostController
        extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post','deletecomment','flagpost','viewuserposts','viewusercomments'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
		$postlists = ReportPost::find()->all();
        return $this->render('post',['userposts' => $postlists]);
	}
	
	public function actionPostCount()
    {
		$model = new Userpost();
        $postcount = $model->getPostCount();
        //return $this->render('post',['userposts' => $postlists]);
	}
	
	public function actionPhotoCount()
    {
		$model = new Userpost();
        $photocount = $model->getPhotoCount();
        //return $this->render('post',['userposts' => $postlists]);
	}
	
	public function actionMessageCount()
    {
        $messagecount = Messages::find()->count();
		//return $this->render('post',['userposts' => $postlists]);
	}
	
	public function actionFlagpost()
    {
        $model = new Userpost();
        $postlists = $model->getUserpost();
        return $this->render('flagpost',['userposts' => $postlists]);
    }
	
	public function actionViewUserPosts()
    {
		$user_id = $_GET['user_id'];
        $model = new Userpost();
        $postlists = $model->getUserposts($user_id);
        return $this->render('user_posts',['userposts' => $postlists]);
    }
	
	public function actionComments()
    {
      
	    $model =  new \frontend\models\Comment();
        $commentlists = Comment::find()->all();
	    return $this->render('comment',['commentlists' => $commentlists]);

    }
	
	public function actionViewUserComments()
    {
		$user_id = $_GET['user_id'];
	    $model =  new \frontend\models\Comment();
        $commentlists = Comment::find()->where(['user_id'=> $user_id])->all();
	    return $this->render('user_comments',['commentlists' => $commentlists]);

    }
	
	public function actionDeletecomment()
    {
		if(isset($_POST['id']) && !empty($_POST['id'])){
			$id = (string)$_POST['id'];
			$model =  new \frontend\models\Comment();
			$comment = Comment::find()->where(["_id" => "$id"])->one();
			if($comment->delete())
			{
				$notification = new Notification();
				$notification = Notification::find()->where(["comment_id" => "$id"])->one();
				if($notification)
				{
					$notification->delete();
				}
				
			return true;
			}
			return false;
		}
	    
		return $this->render('comment',['commentlists' => $commentlists]);
	}
	
	public function actionDeletepost()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
			$deletepost = new Userpost();
            $post_id = $_POST['id'];
            $deletepost = Userpost::find()->where(['_id' => $post_id])->one();	
			$deletepost->is_deleted = '1';	
            $deletepost->update();
			
			$notification = new Notification();
			Notification::updateAll(['is_deleted' => '1'], ['post_id' => $post_id]);
			
            return true;
			
		}
    }
	
	public function actionPublishpost()
    {	
        if(isset($_POST['id']) && !empty($_POST['id']))
        {
			$date = time();
			$deletepost = new Userpost();
            $post_id = $_POST['id'];
            $deletepost = Userpost::find()->where(['_id' => $post_id])->one();	
			$deletepost->is_deleted = '0';	
            $deletepost->update();
			
			$notification = new Notification();
			Notification::updateAll(['is_deleted' => '0'], ['post_id' => $post_id]);
			
			/* Insert Notification For The Owner of Post For Publishing his/her post*/
			
			$post_owner_id = $deletepost['post_user_id'];
		
			$notification =  new Notification();
			$notification->post_id = "$post_id";
			$notification->user_id = "$post_owner_id";
			$notification->notification_type = 'publishpost';
			$notification->is_deleted = '0';
			$notification->status = '1';
			$notification->created_date = "$date";
			$notification->updated_date = "$date";
			$notification->insert();
			return true;
			
		}
    }
	
	public function actionOut(){
		\Yii::$app->user->logout();

        return $this->goHome();
	}

   
}
