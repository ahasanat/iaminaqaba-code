<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\mongodb\ActiveRecord;
use yii\helpers\HtmlPurifier;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use yii\mongodb\Expression;
use backend\components\ExSession;
use frontend\models\Page;
use frontend\models\Like;
use frontend\models\Notification;
use frontend\models\PageVisitor;
use frontend\models\PostForm;
use frontend\models\PageEndorse;
use frontend\models\PageRoles;
use frontend\models\LoginForm;
use frontend\models\Comment;
use frontend\models\DefaultImage;
use yii\helpers\Url;


class PageController  extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout','allcollection'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	
	public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionAll()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$pages = Page::find()->orderBy(['created_date'=>SORT_DESC])->all();
			return $this->render('all',['pages' =>$pages]);	
		}	
	}
	
	public function actionRemove()
	{
        if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$page_id = (string)$_POST['id'];
			$delete = new Page();
			$delete = Page::find()->where(['page_id' => $page_id])->one();
			if($delete->delete())
			{
				LoginForm::deleteAll(['_id' => "$page_id"]);

				Like::deleteAll(['like_type' => 'page','post_id' => $page_id]);

				Notification::deleteAll(['notification_type' => 'likepage','post_id' => $page_id]);
				Notification::deleteAll(['notification_type' => 'pageinvite','post_id' => $page_id]);
				Notification::deleteAll(['notification_type' => 'pageinvitereview','post_id' => $page_id]);
				Notification::deleteAll(['notification_type' => 'pagereview','page_id' => $page_id]);

				PageVisitor::deleteAll(['page_id' => $page_id]);

				PostForm::updateAll(['is_deleted' => '1'], ['is_page_review' => '1','post_user_id' => $page_id]);
				PostForm::updateAll(['is_deleted' => '1'], ['is_page_review' => '1','page_id' => $page_id]);
				//PostForm::deleteAll(['is_page_review' => '1','post_user_id' => $page_id]);
				//PostForm::deleteAll(['is_page_review' => '1','page_id' => $page_id]);

				PageEndorse::deleteAll(['page_id' => $page_id]);

				PageRoles::deleteAll(['page_id' => $page_id]);
				return true;
			}
			else
			{
				return false;
			}
		}	
	}
	
	public function actionUpdate()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$id = $_POST['id'];
				$status = $_POST['status'];

				if($status == 'Active')
				{
					$dl = "3";
				}
				else
				{
					$dl = "1";
				}
				$update = Page::find()->where(['page_id' => "$id"])->one();
				$update->is_deleted = $dl;
				if($update->update())
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}	
	}
	
	public function actionRemovepost()
	{
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			if(isset($_POST) && !empty($_POST))
			{
				$post_id = $_POST['post_id'];
				PostForm::deleteAll(['_id' => $post_id]);
				Notification::deleteAll(['post_id' => $post_id]);
				Like::deleteAll(['post_id' => $post_id]);
				Comment::deleteAll(['post_id' => $post_id]);
				return true;
			}
			else
			{
				return false;
			}
		}	
	}

	public function actionReview()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$reviewposts = PostForm::find()->where(['is_page_review'=>"1"])->orderBy(['post_created_date'=>SORT_DESC])->all();
			return $this->render('reviewpost',['reviewposts' =>$reviewposts]);
		}	
	}
	
	public function actionPhoto()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$photoposts = PostForm::find()->Where(['is_deleted'=>'0','pagepost'=>'1','is_album'=>'1'])->orderBy(['created_date'=>SORT_DESC])->asarray()->all();
			return $this->render('photopost',['photoposts' =>$photoposts]);
		}	
	}

	public function actionPost()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$posts = PostForm::find()->Where(['is_deleted'=>'0','pagepost'=>'1','is_album'=>null])->orderBy(['created_date'=>SORT_DESC])->asarray()->all();
			return $this->render('post',['posts' =>$posts]);
		}	
	}
	
	public function actionImage()
    {
		if (Yii::$app->user->isGuest)
        {
           return $this->goHome();
        } else {
			$defaultimages = DefaultImage::find()->where(['type'=>"page"])->all();
			return $this->render('image',['defaultimages' =>$defaultimages]);	
		}	
	}
	
	
	public function actionImageCrop()
	{
		$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
		$imgpaths = $front_url.'/uploads/defaultimage';
		if (!file_exists($imgpaths))
		{
			mkdir($imgpaths, 0777, true);
		}
		$dt = time();
		$imgUrl = $_POST['imgUrl'];
		// original sizes
		$imgInitW = $_POST['imgInitW'];
		$imgInitH = $_POST['imgInitH'];
		// resized sizes
		$imgW = $_POST['imgW'];
		$imgH = $_POST['imgH'];
		// offsets
		$imgY1 = $_POST['imgY1'];
		$imgX1 = $_POST['imgX1'];
		// crop box
		$cropW = $_POST['cropW'];
		$cropH = $_POST['cropH'];
		// rotation angle
		$angle = $_POST['rotation'];

		$jpeg_quality = 100;


		$output_filename = $front_url."/uploads/defaultimage/".$dt;

		$what = getimagesize($imgUrl);

		switch(strtolower($what['mime']))
		{
			case 'image/png':
				$img_r = imagecreatefrompng($imgUrl);
				$source_image = imagecreatefrompng($imgUrl);
				$type = '.png';
				break;
			case 'image/jpeg':
				$img_r = imagecreatefromjpeg($imgUrl);
				$source_image = imagecreatefromjpeg($imgUrl);
				error_log("jpg");
				$type = '.jpeg';
				break;
			case 'image/gif':
				$img_r = imagecreatefromgif($imgUrl);
				$source_image = imagecreatefromgif($imgUrl);
				$type = '.gif';
				break;
			default: die('image type not supported');
		}



		// resize the original image to size of editor
		$resizedImage = imagecreatetruecolor($imgW, $imgH);
		imagecopyresampled($resizedImage, $source_image, 0, 0, 0, 0, $imgW, $imgH, $imgInitW, $imgInitH);
		// rotate the rezized image
		$rotated_image = imagerotate($resizedImage, -$angle, 0);
		// find new width & height of rotated image
		$rotated_width = imagesx($rotated_image);
		$rotated_height = imagesy($rotated_image);
		// diff between rotated & original sizes
		$dx = $rotated_width - $imgW;
		$dy = $rotated_height - $imgH;
		// crop rotated image to fit into original rezized rectangle
		$cropped_rotated_image = imagecreatetruecolor($imgW, $imgH);
		imagecolortransparent($cropped_rotated_image, imagecolorallocate($cropped_rotated_image, 0, 0, 0));
		imagecopyresampled($cropped_rotated_image, $rotated_image, 0, 0, $dx / 2, $dy / 2, $imgW, $imgH, $imgW, $imgH);
		// crop image into selected area
		$final_image = imagecreatetruecolor($cropW, $cropH);
		imagecolortransparent($final_image, imagecolorallocate($final_image, 0, 0, 0));
		imagecopyresampled($final_image, $cropped_rotated_image, 0, 0, $imgX1, $imgY1, $cropW, $cropH, $cropW, $cropH);
		// finally output png image
		//imagepng($final_image, $output_filename.$type, $png_quality);
		imagejpeg($final_image, $output_filename.$type, $jpeg_quality);
		$response = Array(
			"status" => 'success',
			"url" => $output_filename.$type
		);
		$pageimage = DefaultImage::find()->where(['type'=>"page"])->one();
		$image = $output_filename.$type;
		$date = time();
		/*if($pageimage){
			$pageimage->image = $image;
			$pageimage->updated_date = "$date"; 
			$pageimage->update();
		}else{*/
			$addimage = new DefaultImage();
			$addimage->type = "page";
			$addimage->image = $image;
			$addimage->updated_date = "$date"; 
			$addimage->insert();
		//}
		return json_encode($response);
		//return $output_filename;
	}
}
