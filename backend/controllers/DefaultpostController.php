<?php
namespace backend\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use frontend\models\DefaultImage;
use frontend\models\Gallery;
use frontend\models\PlaceDiscussion;
use frontend\models\PlaceReview;
use frontend\models\PlaceTip;
use frontend\models\PlaceAsk;
use backend\models\Messagechat;
use backend\models\DefaultPosts;

/**
 * Site controller
 */
class DefaultpostController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
         return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['discussion', 'storeddiscussion'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],      
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function beforeAction($action)
    {   
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionDiscussion()
    {
        return $this->render('discussion');
    }

    public function actionStoreddiscussion()
    {
        $user_id = (string)$_SESSION['email'];
        $is_admin = Messagechat::IsAdmin($user_id);
        if($is_admin) {
            $title = isset($_POST['title']) ? $_POST['title'] : '';
            $description = isset($_POST['description']) ? $_POST['description'] : '';

            DefaultPosts::deleteAll(['module' => 'discussion']);
            $DefaultPosts = new DefaultPosts();
            $DefaultPosts->title = $title;
            $DefaultPosts->description = $description;
            $DefaultPosts->module = 'discussion';
            $DefaultPosts->save();
        }    
    }
}
