<?php
namespace backend\models;
use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;

use yii\web\UploadedFile; 
 
use yii\helpers\ArrayHelper;
use frontend\models\UserForm;
use frontend\models\Personalinfo;

use frontend\models\LocaldriverPostInvite;
use frontend\models\LocaldriverPostInviteMsgs;
use frontend\models\Verify;
use frontend\models\Friend;
use frontend\models\Abuse;
use frontend\models\AbuseStatement;
use backend\models\LocaldriverActivity;




/**
 * This is the model class for collection "occupation".
 *
 * @property \MongoId|string $_id
 * @property mixed $post_text
 * @property mixed $post_status
 * @property mixed $post_created_date
 * @property mixed $post_user_id
 * @property mixed $image
 */

class LocaldriverPost extends ActiveRecord
{

   /*public $imageFile1;*/
    /**
     * @return string the name of the index associated with this ActiveRecord class.
     */
    public static function collectionName()
    {
        return 'localdriver_post';
    }

   /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
         return ['_id', 'user_id', 'creprodesc', 'creproactivity', 'creprolang', 'creprofee', 'created_at', 'updated_at'];

    }

    public function gethostinginfo($id) {
        $userdata = Travelbuddy::find()->where(['user_id' => (string)$id])->asarray()->one();
        return $userdata;
    }

    public function getBulkUserInfo($ids) {
        $newFilterInfos = array();
        if(!empty($ids)) {
            $infos = UserForm::find()->where(['in', (string)'_id', $ids])->asArray()->all();
            if(!empty($infos)) {
                for($I=0; $I<count($infos);$I++) {
                    $key = (string)$infos[$I]['_id'];
                    $newFilterInfos[$key] = $infos[$I];
                    $newFilterInfos[$key.'1'] = $infos[$I];
                }
            }
        }

        return json_encode($newFilterInfos, true);
        exit;
    }
    
    public function getLocaldriverList($id) {
        if(isset($id) && $id != null) {
            $hostsBulkList = LocaldriverPost::find()->Where(['not','flagger', "yes"])->orderBy(['_id' => SORT_DESC])->asarray()->all();
            if(!empty($hostsBulkList)) {
                $ids = ArrayHelper::map($hostsBulkList, 'user_id', 'user_id');
                $information = LocaldriverPost::getBulkUserInfo($ids);
                $information = json_decode($information, true);

                if($information) {
                    for($I=0; $I<count($hostsBulkList);$I++) {
                        $key = (string)$hostsBulkList[$I]['user_id'];
                        if(array_key_exists($key, $information)) {
                            $basicInfo = $information[$key];
                           /* $thumbnail = '';    
                            if(isset($basicInfo['photo']) && substr($basicInfo['photo'],0,4) == 'http') {
                                    $thumbnail = $basicInfo['photo'];
                            } else if(isset($basicInfo['thumbnail']) && $basicInfo['thumbnail'] != '' && file_exists('profile/'.$basicInfo['thumbnail'])) {
                                    $thumbnail = "profile/".$basicInfo['thumbnail'];
                            } else {
                                    $thumbnail = "<?=$baseUrl?>/images/".$basicInfo['gender'].".jpg";
                            }
                            $basicInfo['thumb'] = $thumbnail;*/
                            $hostsBulkList[$I]['userBasicInfo'] = $basicInfo;
                        }
                    }
                }

                return json_encode($hostsBulkList, true);
                exit;
            }
        } 
    }

    
    public function godelete($uid, $id) {
        if($uid) {
            $goDelete = LocaldriverPost::find($id)->Where(['not','flagger', "yes"])->one();
            if($goDelete->delete()) {
                $selectInvitationIds = ArrayHelper::map(LocaldriverPostInvite::find()->select([(string)'_id'])->where(['post_id' => $id])->asarray()->all(), function($scope) { return (string)$scope['_id'];}, '1');
                LocaldriverPostInvite::deleteAll(['post_id' => $id]);
                if(!empty($selectInvitationIds)) {
                    $invitationSubIds = array_keys($selectInvitationIds);
                    LocaldriverPostInviteMsgs::deleteAll(['in', 'postinvite_id', $invitationSubIds]);
                }
                return true;
                exit;
            }
        }
        return false;
        exit;
    }

    public function godeleteabuse($uid, $id) {
        if($uid) {
            $goDelete = Abuse::find($id)->asarray()->one();
            if(!empty($goDelete)) {
                $offer_id = $goDelete['offer_id'];
                $user_id = $goDelete['user_id'];
                Abuse::deleteAll(['offer_id' => $offer_id, 'user_id' => $user_id, 'type' => 'localdriver']);
                $result = array('status' => true, 'class' => "record".$user_id.$offer_id);
                return json_encode($result, true);
                exit;
            }
        }

        $result = array('status' => false);
        return json_encode($result, true);
        exit;
    }

    public function abuseReport($uid) {
        if(isset($uid) && $uid != null) {
            $abuseList = Abuse::find()->where(['type' => 'localdriver'])->orderBy(['_id' => SORT_DESC])->asarray()->all();
            if(!empty($abuseList)) {
                $ids = ArrayHelper::map($abuseList, 'user_id', 'user_id');
                $information = LocaldriverPost::getBulkUserInfo($ids);
                $information = json_decode($information, true);

                if($information) {
                    for($I=0; $I<count($abuseList);$I++) {
                        $key = (string)$abuseList[$I]['user_id'];
                        $reason = $abuseList[$I]['reason'];
                        $reason = explode(",", $reason);


                        $fetchReasonStatement = ArrayHelper::map(AbuseStatement::find()->where(['in', 'code', $reason])->asarray()->all(), 'code', 'statement');

                        if(!empty($fetchReasonStatement)) {
                            $fetchReasonStatement = implode("<br/>", $fetchReasonStatement);
                            $abuseList[$I]['reasonStatement'] = $fetchReasonStatement;
                        }

                        if(array_key_exists($key, $information)) {
                            $basicInfo = $information[$key];

                           /* $thumbnail = '';    
                            if(isset($basicInfo['photo']) && substr($basicInfo['photo'],0,4) == 'http') {
                                    $thumbnail = $basicInfo['photo'];
                            } else if(isset($basicInfo['thumbnail']) && $basicInfo['thumbnail'] != '' && file_exists('profile/'.$basicInfo['thumbnail'])) {
                                    $thumbnail = "profile/".$basicInfo['thumbnail'];
                            } else {
                                    $thumbnail = "<?=$baseUrl?>/images/".$basicInfo['gender'].".jpg";
                            }
                            $basicInfo['thumb'] = $thumbnail;*/
                            $abuseList[$I]['userBasicInfo'] = $basicInfo;
                        }
                    }
                }

                return json_encode($abuseList, true);
                exit;
            }
        }
    }
}