<?php
namespace backend\models;
use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile;

class DefaultPosts extends ActiveRecord
{
    public static function collectionName()
    {
        return 'defaultposts';
    }

    /**
     * @return array list of attribute names.
     */
    public function attributes()
    {
        return ['_id', 'title', 'description','module'];
    }
}