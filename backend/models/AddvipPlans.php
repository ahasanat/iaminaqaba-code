<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile;

class AddvipPlans extends ActiveRecord
{
   
     public static function collectionName()
    {
        return 'vip_plans';
    }

    public function attributes()
    {
        return ['_id','amount','months','percentage','plan_type'];

    }
	
    public function getVipPlans()
    {
        return AddvipPlans::find()->all();
    }
}
