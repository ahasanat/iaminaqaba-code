<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile;

class Userdata extends ActiveRecord
{   
     public static function collectionName()
    {
        return 'user';
    }

    public function attributes()
    {
       return ['_id', 'fb_id', 'username', 'fname','lname','fullname', 'password', 'con_password', 'pwd_changed_date', 'email','alternate_email','photo','thumbnail','profile_picture','cover_photo', 'birth_date','gender','created_date','updated_date','created_at','updated_at','status','phone','isd_code','country','city','citylat','citylong','captcha','vip_flag','reference_user_id','member_type','last_login_time','forgotcode','forgotpassstatus','lat','long','login_from_ip','last_time','last_logout_time','point_total'];

    }

    public function getUser()
    {
        
        $record =  Userdata::find()->select(['fname','lname','fullname','email','status','country','gender','vip_flag'])->where(['not','status','44'])->andwhere(['not','status','4'])->andwhere(['not','status','10'])->asarray()->all();
		
		
		$filterNewRecord = array();
		foreach($record as $record2)
		{
			$isDate = isset($record2['vip_flag']) ? $record2['vip_flag'] : '';
			$isCountry = isset($record2['country']) ? $record2['country'] : '';
			$isGender = isset($record2['gender']) ? $record2['gender'] : '';
			$record2['country'] = $isCountry;
			$record2['gender'] = $isGender;
			if($isDate == 'undefined' || $isDate == null || $isDate == '' || strlen($isDate)<3) {
				
				$filterNewRecord[] = $record2;
			}
			
		}
		
		return $filterNewRecord;
		exit;
	}

	public function getUserCountPerDays($thisMonth)
    {
    	$result = array('status' => false);
     	if($thisMonth) {   
	        $record =  Userdata::find()->select(['created_at'])->where(['not','status','44'])->andwhere(['not','status','4'])->andwhere(['not','status','10'])->asarray()->all();
			
			$filterNewRecord = array();

			foreach($record as $record2)
			{
				$created_at = $record2['created_at'];
				$month = date("m", $created_at);
				if($month == $thisMonth) {
					$day = date("d", $created_at);
					$filterNewRecord[$day][] = 1;
				}
			}

			$newfilterNewRecord = array();
			foreach ($filterNewRecord as $key => $value) {
				if(is_array($value)) {
					$newfilterNewRecord[$key] = count($value);
				} else {
					$newfilterNewRecord[$key] = 1;
				}
			}

			$Current_Year = date('Y');
			$daysCount = cal_days_in_month(CAL_GREGORIAN, $thisMonth, $Current_Year);
			$daysBulkArray = array();
			$countUserPerDay = array();
			foreach (range(1, $daysCount) as $number) {
			    if(strlen($number) == 1) {
			    	$number = '0' . $number;
			    }
			    $daysBulkArray[] = $number;
			    if(!array_key_exists($number, $newfilterNewRecord)) {
			    	$newfilterNewRecord[$number] = 0;
			    }
			}

			ksort($newfilterNewRecord);

			if(count($newfilterNewRecord) == count($daysBulkArray)) {
				$result = array('status' => true, 'label' => $daysBulkArray, 'data' => $newfilterNewRecord);
				return json_encode($result, true);
			}
	    }
	    
	    return json_encode($result, true);
		exit;
	}
	
	public function getusercountmonthyear($thisMonth, $thisYear)
    {
    	$result = array('status' => false);
     	if($thisMonth) {   
	        $record =  Userdata::find()->select(['created_at'])->where(['not','status','44'])->andwhere(['not','status','4'])->andwhere(['not','status','10'])->asarray()->all();
			
			$filterNewRecord = array();

			foreach($record as $record2)
			{
				$created_at = $record2['created_at'];
				$month = date("m", $created_at);
				$year = date("Y", $created_at);
				if($month == $thisMonth && $year == $thisYear) {
					$day = date("d", $created_at);
					$filterNewRecord[$day][] = 1;
				}
			}

			$newfilterNewRecord = array();
			foreach ($filterNewRecord as $key => $value) {
				if(is_array($value)) {
					$newfilterNewRecord[$key] = count($value);
				} else {
					$newfilterNewRecord[$key] = 1;
				}
			}


			$daysCount = cal_days_in_month(CAL_GREGORIAN, $thisMonth, $thisYear);
			$daysBulkArray = array();
			$countUserPerDay = array();
			foreach (range(1, $daysCount) as $number) {
			    if(strlen($number) == 1) {
			    	$number = '0' . $number;
			    }
			    $daysBulkArray[] = $number;
			    if(!array_key_exists($number, $newfilterNewRecord)) {
			    	$newfilterNewRecord[$number] = 0;
			    }
			}

			ksort($newfilterNewRecord);

			if(count($newfilterNewRecord) == count($daysBulkArray)) {
				$result = array('status' => true, 'label' => $daysBulkArray, 'data' => $newfilterNewRecord);
				return json_encode($result, true);
			}
	    }
	    
	    return json_encode($result, true);
		exit;
	}
	
	public function getFrontadmin()
    {
        
        return Userdata::find()->select(['fname','lname','email','status'])->where(['status' => '10'])->all();
    
	}

    public function getUserCount()
    {
        return Userdata::find()->where(['not','status','44'])->andwhere(['not','status','3'])->andwhere(['not','status','4'])->count();
    }
	
	public function getUserdash()
    {
        return Userdata::find()->select(['_id','fullname','fname','created_date','gender'])->where(['not','status','44'])->andwhere(['not','status','3'])->andwhere(['not','status','4'])->orderBy(['created_date'=>SORT_DESC])->limit(8)->offset(0)->all();
    }

    public function getBulkUserInfo($ids) {
        $newFilterInfos = array();
        if(!empty($ids)) {
            $infos = UserForm::find()->where(['in', (string)'_id', $ids])->asArray()->all();
            if(!empty($infos)) {
                for($I=0; $I<count($infos);$I++) {
                    $key = (string)$infos[$I]['_id'];
                    $newFilterInfos[$key] = $infos[$I];
                    $newFilterInfos[$key.'1'] = $infos[$I];
                }
            }
        }

        return json_encode($newFilterInfos, true);
        exit;
    }
    
}
