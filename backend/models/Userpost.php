<?php
namespace backend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\mongodb\ActiveRecord;
use yii\web\UploadedFile;

class Userpost extends ActiveRecord
{
   
     public static function collectionName()
    {
        return 'user_post';
    }

    public function attributes()
    {
        return ['_id','post_type', 'post_ip', 'post_title', 'post_text', 'shared_by','post_status', 'post_created_date', 'post_user_id','image','link_title','link_description','share_by','shared_from','album_title','is_album','album_place','album_img_date','post_privacy','is_timeline','is_deleted','currentlocation','parent_post_id','is_coverpic','share_setting','comment_setting', 'post_tags','custom_share', 'custom_notshare', 'anyone_tag','is_profilepic'];

    }

    public function getUserpost()
    {
        
        return Userpost::find()->select(['post_text','post_title','post_privacy','post_created_date','_id'])->where(['is_deleted'=>'2'])->all();
    
	}
	
	public function getUserposts($id)
    {
        
        return Userpost::find()->select(['post_text','post_title','post_type','post_privacy','post_created_date','_id'])->where(['post_user_id'=> $id])->andwhere(['is_deleted'=> '0'])->all();
    
	}
	
    public function getPostCount()
    {
        return Userpost::find()->where(['is_deleted'=>'0'])->count();
    }
	
	public function getPhotoCount()
    {
        return Userpost::find()->where(['is_deleted'=>'0'])->andwhere(['post_type'=>'profilepic'])->orwhere(['post_type'=>'image'])->orwhere(['post_type'=>'text and image'])->count();
    }
}
