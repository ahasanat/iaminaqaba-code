<?php

namespace backend\models;

use yii\base\Model;
use Yii;
use yii\mongodb\ActiveRecord;
use frontend\models\UserForm;
use frontend\models\Userdata;
use frontend\models\Abuse;
use frontend\models\AbuseStatement;
use yii\helpers\ArrayHelper;    
/**
 * This is the model class for table "session_frontend_user".
 *
 * @property string $id
 * @property integer $user_id
 * @property string $ip 
 * @property integer $expire
 * @property resource $data
 */
class GiftCost extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'message_gift';
    }

    public function attributes() {
        return ['_id', 'price', 'created_by', 'created_at'];
    }

    public function UpdateCost($uid) {
        if(isset($uid)) {
            $records = GiftCost::find()->orderBy(['_id' => SORT_DESC])->asarray()->all();
            
            if(!empty($records)) {
                return json_encode($records, true);
                exit;
            }
        } 

        $records = array();
        return json_encode($records, true);
        exit;
    } 

    public function EditUpdateCost($uid, $price) {
        if(isset($uid) && isset($price)) {
            if($price%5 == 0) {
                $message = new GiftCost();
                $message->price = $price;
                $message->created_by = $uid;
                $message->created_at = strtotime('now');
                if($message->save()) {
                    $fetchLstRcd = GiftCost::find()->orderBy(['_id' => SORT_DESC])->asarray()->one();
                    if(!empty($fetchLstRcd)) {
                        $fetchLstRcd['created_at'] = date("Y-m-d H:i:s", $fetchLstRcd['created_at']);
                        $result = array('status' => true, 'data' => $fetchLstRcd);
                        return json_encode($result, true);
                    }
                }
            }
        } 

        $result = array('status' => false);
        return json_encode($result, true);
    }

    public function getImages($uid) {
        if(isset($uid)) {
            
        } 

        return false;
        exit;
    }

    public function abuseReport($uid) {
        if(isset($uid) && $uid != null) {
            $abuseList = Abuse::find()->where(['type' => 'message'])->orderBy(['_id' => SORT_DESC])->asarray()->all();
            if(!empty($abuseList)) {
                $ids = ArrayHelper::map($abuseList, 'user_id', 'user_id');
                $information = Userdata::getBulkUserInfo($ids);
                $information = json_decode($information, true);

                if($information) {
                    for($I=0; $I<count($abuseList);$I++) { 
                        $key = (string)$abuseList[$I]['user_id'];
                        $reason = $abuseList[$I]['reason'];
                        $reason = explode(",", $reason);


                        $fetchReasonStatement = ArrayHelper::map(AbuseStatement::find()->where(['in', 'code', $reason])->asarray()->all(), 'code', 'statement');

                        if(!empty($fetchReasonStatement)) {
                            $fetchReasonStatement = implode("<br/>", $fetchReasonStatement);
                            $abuseList[$I]['reasonStatement'] = $fetchReasonStatement;
                        }

                        if(array_key_exists($key, $information)) {
                            $basicInfo = $information[$key];
/*
                            $thumbnail = '';    
                            if(isset($basicInfo['photo']) && substr($basicInfo['photo'],0,4) == 'http') {
                                    $thumbnail = $basicInfo['photo'];
                            } else if(isset($basicInfo['thumbnail']) && $basicInfo['thumbnail'] != '' && file_exists('profile/'.$basicInfo['thumbnail'])) {
                                    $thumbnail = "profile/".$basicInfo['thumbnail'];
                            } else {
                                    $thumbnail = "<?=$baseUrl?>/images/".$basicInfo['gender'].".jpg";
                            }
                            $basicInfo['thumb'] = $thumbnail;*/
                            $abuseList[$I]['userBasicInfo'] = $basicInfo;
                        }
                    }
                }

                return json_encode($abuseList, true);
                exit;
            }
        }
    }
    
    public function godeleteabuse($uid, $id) { 
        if($uid) {
            $goDelete = Abuse::find($id)->asarray()->one();
            if(!empty($goDelete)) {
                $offer_id = $goDelete['offer_id'];
                $user_id = $goDelete['user_id'];
                Abuse::deleteAll(['offer_id' => $offer_id, 'user_id' => $user_id, 'type' => 'message']);
                $result = array('status' => true, 'class' => "record".$user_id.$offer_id);
                return json_encode($result, true);
                exit;
            }
        }

        $result = array('status' => false);
        return json_encode($result, true);
        exit;
    }

}