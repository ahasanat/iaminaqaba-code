<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
$this->title = 'Reported Post';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Posts</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Post List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="postlist" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th>View</th>
                  <th>Reason</th>
                  <th>Reported By</th>
				  <th>Added time</th>   
                </tr>
                </thead>
                <tbody>
    <?php foreach($userposts as $userpost){
	$user = LoginForm::find()->where(['_id' => $userpost['reporter_id']])->one();
	?>
	
            <tr>
				<td><a href="../frontend/web/index.php?r=site/travpost&postid=<?= $userpost['post_id'];?>" target='_blank'>View</a></td>
				<?php if(empty($userpost['reason'])){$userpost['reason'] = 'Not added';}?>
                <td><?= $userpost['reason'];?></td>
                <td><?= $user['fullname'];?></td>
				<?php $post_created_date = date('m/d/Y h:i:s', $userpost['created_date']);?>
                <td><?= $post_created_date;?></td>
			</tr>

            <?php }?>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
