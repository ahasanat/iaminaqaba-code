<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Verify';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper vip-admin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Verify Plans</h1>
     <?php  $session =
                    Yii::$app->session;
           echo  $email =
                    $session->get('username'); ?>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Add Verify Plans</h3>
			</div>
			<div class="box-body">
			  <form id="frm" class="topform">
				<div class="frow">
					<label>Please add amount(per year)</label>&nbsp;
					<input type="text" name="amount" id="amount" required/><span class="amnt_notice" style="display: none"></span><br/>
				</div>
				<div class="frow">
					<label>Please add Years</label>&nbsp;
					<input type="text" name="months" id="months" placeholder="" required/><span class="mnth_notice" style="display: none"></span><br/>
				</div>
				<div class="frow">
						<label>Please add % </label>&nbsp;
						<input type="text" name="percentage" id="percentage" placeholder="" required/><span class="percentage_notice" style="display: none"></span><br/>
				</div>
				<div class="frow">
						<label>Please Plan Type </label>&nbsp;
						<select name="plan_type" id="plan_type">
							<option value="">select Plans</option>
							<option value="Most Popular">Most Popular</option>
							<option value="Popular">Popular</option>
							<option value="Best Value">Best Value</option>
						</select>
						<span class="plan_type_notice" style="display: none"></span><br/>
				</div>
				<div class="frow">
					<input type="button" name="add" value="add" onclick="addverifylans()" class="btn btn-primary"/>  
					<input type="reset" name="clear" value="clear" class="btn btn-primary"/>  
				</div>
			  </form>
            </div>
			<script>
				function addverifylans(){
					var amount = $('#amount').val();
					var months = $('#months').val();
					var percentage = $('#percentage').val();
					var plan_type = $('#plan_type').val();
					var reg_amt = /^[0-9]{1}/;
					var reg_mnt = /^[0-9]{1}/;
					
					if(!reg_amt.test(amount))
					{
						$('.amnt_notice').html('Please enter valid amount in dollar only');
					    $('.amnt_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#amount").focus();
						return false;
					}
					if(!reg_mnt.test(months))
					{
						$('.mnth_notice').html('Please enter valid Year in digit only');
					    $('.mnth_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#months").focus();
						return false;
					}
					/*if(!reg_mnt.test(percentage))
					{
						$('.percentage_notice').html('Please enter valid percentage in digit only');
					    $('.percentage_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#percentage").focus();
						return false;
					} */
					else {
						$.ajax({
							url: '?r=site/addverifyplans', 
							type: 'POST',
							data: 'amount=' + amount+'&months='+months+'&percentage='+percentage+'&plan_type='+plan_type,
							success: function (data) {
								 $("#frm")[0].reset();
								 $("#verifyplan").load(window.location + " #verifyplan");	
								 
							}
						});
					}
				}
			</script>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="verifyplan" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>amount(per month)</th>
                  <th>Year</th>
                  <th>Percentage</th>
                  <th>Plans Type</th>
                  <th>Delete</th>
			   </tr>
                </thead>
                <tbody>
    <?php foreach($verify_plans as $verify_plan){ ?>
            <tr>
                <td><?= $verify_plan['amount'];?></td>
                <td><?= $verify_plan['months'];?></td>
				<th><?= $verify_plan['percentage'];?></th>
                <th><?= $verify_plan['plan_type'];?></th>
                <td id="<?=$verify_plan['_id']?>"><a onclick="remove_verify_plan('<?=$verify_plan['_id']?>')" style="cursor: pointer;">Delete</a></td>
			</tr>

            <?php }?>
                
                </tbody>
               
              </table>
            </div>
			<script>
				function remove_verify_plan(id){
					var r = confirm("Are you sure to delete this Verify Plan?");
					if (r == false) {
						return false;
					}
					else
					{
						$.ajax({
								url: '?r=site/removeverifyplans', 
								type: 'POST',
								data: 'id=' + id,
								success: function (data) {
									 
									 $("#"+id).parents('tr').remove();	
									 
								}
							});
					}		
				}
			</script>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <!-- <div class="col-lg-6 col-xs-6">
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>65</h3>

              <p>Unique Visitors</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="javascript:void(0)" class="small-box-footer">More info <i class="mdi mdi-arrow-right-circle"></i></a>
          </div>
        </div> -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
 
