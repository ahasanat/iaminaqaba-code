<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Flagger Listing';
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Flager</h1>
      <!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Flagger List</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
			<form id="frm">
			 <table id="example1" class="table table-bordered table-striped">
				<tr>
					<td>First Name</td>
					<td><input type="text" name="fname" id="fname" placeholder="First Name"/></td>
					<td><span class="fname_notice" style="display: none"></span></td>
				</tr>
				<tr>
					<td>Last Name</td>
					<td><input type="text" name="lname" id="lname" placeholder="Last Name"/></td>
					<td><span class="lname_notice" style="display: none"></span></td>
				</tr>
				<tr>
					<td>Email</td>
					<td><input type="text" name="email" id="email" placeholder="Email"/></td>
					<td><span class="email_notice" style="display: none"></span></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="password" id="password" placeholder="Password"/></td>
					<td><span class="password_notice" style="display: none"></span></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="button" name="submit" value="Add" onclick="add()"/></td>
				</tr>
			 <table>
			 </form>
			 
              <table id="NewFlagger" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
    <?php foreach($userdatas as $userdata){ ?>
            <tr>
                <td><?= $userdata['fname'];?></td>
                <td><?= $userdata['lname'];?></td>
                <td><?= $userdata['email'];?></td>
				<td id="<?=$userdata['_id']?>"><a onclick="delete_user('<?=$userdata['_id']?>')" style="cursor: pointer;">Delete</a></td>
            </tr>

            <?php }?>
                
                </tbody>
				<script>
				function add(){
					
					var fname = $('#fname').val();
					var lname = $('#lname').val();
					var email = $('#email').val();
					var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
					var password = $('#password').val();
					var reg_nm = /^[a-zA-Z\s]+$/;
					var spc = /^\s+$/;	
					
					if(fname == '')
					{
						$('.fname_notice').html('Please enter First Name');
					    $('.fname_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#fname").focus();
						return false;
					}
					
					else if(!reg_nm.test(fname))
					{
						$('.fname_notice').html('Please enter characters only in first name.');
						$('.fname_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#fname").focus();
						return false;
					}
					else if(spc.test(fname))
					{
						$('.fname_notice').html('Blank Space is not allowed Please enter First Name');
					    $('.fname_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#fname").focus();
					}
				
					else if(lname == '')
					{
						$('.lname_notice').html('Please enter Last Name');
					    $('.lname_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#lname").focus();
						return false;
					}
					else if(!reg_nm.test(lname))
					{
						$('.lname_notice').html('Please enter characters only in last name.');
						$('.lname_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#lname").focus();
						return false;
					}
					else if(spc.test(lname))
					{
						$('.lname_notice').html('Blank Space is not allowed Please enter Last Name');
					    $('.lname_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#lname").focus();
					}
					else if(email == '')
					{
						$('.email_notice').html('Please enter Email Id');
					    $('.email_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#email").focus();
						return false;
					}
					else if(spc.test(email))
					{
						$('.email_notice').html('Blank Space is not allowed Please enter Email');
					    $('.email_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#email").focus();
					}
					if(! pattern.test(email))
					{
						$('.email_notice').html('Please Enter Proper Email Id');
						$('.email_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#email").focus();
						return false;
					}
					else if(password == '')
					{
						$('.password_notice').html('Please enter Password');
					    $('.password_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#password").focus();
						return false;
					}
					else if(spc.test(password))
					{
						$('.password_notice').html('Blank Space is not allowed Please enter Password');
					    $('.password_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#password").focus();
					}
					else
					{
					
						$.ajax({
								url: '?r=site/adduser', 
								type: 'POST',
								data: $("#frm").serialize(),
								success: function (data) 
								{
									if(data == 1)
									{
										$("#frm")[0].reset();
										$("#NewFlagger").load(window.location + " #NewFlagger");	
									}
									else
									{
										alert("This email id is already exists");
									}								
								}
							});
					}	
				}
				
				function delete_user(id)
				{
					var r = confirm("Are you sure to delete this frontend admin?");
					if (r == false) 
					{
						return false;
					}
					else 
					{
					
						$.ajax({
							url: '?r=site/deleteadmin', 
							type: 'POST',
							data: 'id=' + id,
							success: function (data) {
								if(data == 1){
									$("#"+id).parents('tr').remove();
								 }
							}
						});
					}
				}
				
			</script>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
