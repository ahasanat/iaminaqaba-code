<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
$this->title = 'Photostream';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper placespage">
	<section class="content-header">
		<h1>Collections</h1>
    </section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<table id="placesphotoslist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Title</th>
								  <th>Image</th>
								  <th>Flagger By</th>
								  <th>Flagger date</th>
								  <th>Flagger Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($data as $post)
								{
									$postid = $post['_id'];
									$posttitle = $post['title'];
									$postdate = date('d-M-Y',$post['flagger_date']);
									$flagger_by = $post['flagger_by'];
									$flagger_by_name = $this->context->getuserdata($flagger_by,'fullname');
									?>
									<tr>
										<td><?= $posttitle?></td>
										<td>
										<?php
											$eximgs = explode(',',$post['image']);
											$eximgs = array_values(array_filter($eximgs));
											foreach ($eximgs as $eximg) {

											$eximg = str_replace('../web/uploads/gallery/', '../../frontend/web/uploads/gallery/', $eximg);

											//if(file_exists($eximg)){
											?>
											<div class="imgpreview-holder">
												<a target="_blank" href="<?=$eximg?>" class="listalbum-box imgpin">
													<img alt="" src="<?=$eximg?>" height="75px" width="75px"/>
												</a>
												<a href="javascript:void(0);" class="delbtn"><i class="zmdi zmdi-delete"></i></a>
											</div>
										<?php } 
										//} 
										?>
										</td>
										<td><?=$flagger_by_name?></td>
										<td><?=$postdate?></td>
										<td>
											<button type="button" class="btn btn-block btn-danger btn-sm" data-id="<?=$postid?>" data-action="unflag" data-module="collections" style="background: blue;" onclick="doperformflag(this)">Unflag</button>
											
											<button type="button" class="btn btn-block btn-danger btn-sm" style="background: red;" data-id="<?=$postid?>" data-action="delete" data-module="collections" onclick="doperformflag(this)">Delete</button>

										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>

<script type="text/javascript">
	function doperformflag(obj){
		$this = $(obj);
		$id = $(obj).attr('data-id');
		$module = $(obj).attr('data-module');
		$action = $(obj).attr('data-action');
		$.ajax({
			type: 'POST',
			url: '?r=flagger/flagperform',
			data: {
				id: $id,
				module: $module,
				action: $action
			},
			success: function(data)
			{
				var result = $.parseJSON(data);
	            if(result.success != undefined && result.success == 'yes') {
	            	$msg = result.msg;
	            	$this.parents('tr').remove();
			    }	
			}
		});
	}
</script>