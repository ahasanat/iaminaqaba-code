<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Mail';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper addbuscat-admin">
    <section class="content-header">
		<h1>Mail</h1>
    </section>
    <section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Iaminaqaba Mail Editor</h3>
						<span class="name_success" style="display: none"></span>
					</div>
				    <form id="frm" class="form-horizontal">
                        <div class="box-body">
							
							<div class="form-group">
								<label class="col-sm-2 control-label">Mail template name</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="name" id="name" required/><span class="name_notice" style="display: none">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Mail subject </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="subject" id="subject" required/><span class="name_subject" style="display: none"></span>
								</div>	
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Mail sender name </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="sname" id="sname" required/><span class="name_sname" style="display: none"></span>
								</div>	
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Mail sender email </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="email" id="email" required/><span class="name_email" style="display: none"></span>
								</div>	
							</div>
							<div class="box-body pad">
								<span class="mailbody" style="display: none"></span>
								<textarea id="mailbody" name="mailbody" rows="10" cols="80" placeholder="This is my textarea to be replaced with mail content."></textarea>
							</div>
						</div>	
						<div class="box-footer">
							<input type="reset" name="clear" value="Clear"  class="btn btn-default"/>
							<input type="button" name="add" value="Add" onclick="addmailtmplt()" class="btn btn-info right"/>
						</div>	
                    </form>
				</div>
            </div>
		</div>
    </section>
</div>

<script>
	function addmailtmplt(){
		var name = $('#name').val();
		var subject = $('#subject').val();
		var sname = $('#sname').val();
		var email = $('#email').val();
		var mailbody = CKEDITOR.instances.mailbody.getData();
		if(name == '')
		{
			$('.name_notice').html('Please enter mail template name');
			$('.name_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
			$("#name").focus();
			return false;
		}
		else if(subject == '')
		{
			$('.name_subject').html('Please enter mail subject');
			$('.name_subject').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
			$("#subject").focus();
			return false;
		}
		else if(sname == '')
		{
			$('.name_sname').html('Please enter mail sender name');
			$('.name_sname').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
			$("#sname").focus();
			return false;
		}
		else if(email == '')
		{
			$('.name_email').html('Please enter mail sender email');
			$('.name_email').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
			$("#email").focus();
			return false;
		}
		else if(mailbody == '')
		{
			$('.mailbody').html('Please enter some mail content');
			$('.mailbody').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
			CKEDITOR.instances.mailbody.focus();
			return false;
		}
		else
		{
			$.ajax({
				url: '?r=mail/addmail', 
				type: 'POST',
				data: 'name='+name+'&subject='+subject+'&sendername='+sname+'&senderemail='+email+'&mailbody='+mailbody,
				success: function (data) 
				{
					if(data == 'insert')
					{
						$('.name_success').html('Mail template added successfully');
						$('.name_success').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
						$("#frm")[0].reset();
					}
					else
					{
						$('.name_notice').html('This mail template name already exist');
						$('.name_notice').css('display','inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
					}
				}
			});
		}
	}
</script>
