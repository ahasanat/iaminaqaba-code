<?php 
use yii\helpers\Html;
$travelasset = backend\assets\TravelAsset::register($this);

$this->title = 'Mail Template';

$travelbaseUrl = $travelasset->baseUrl;
?>

<div class="content-wrapper addbuscat-admin">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Mail Template</h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Add Mail Template</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="maillist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Subject</th>
                  <th>Added on</th>
                  <th>Send</th>
                  <th>Delete</th>
                </tr>
                </thead>
                <tbody>
            <?php foreach($mails as $mail){ ?>
            <tr>
                <td><?= $mail['tpl_name'];?></td>
                <td><?= $mail['tpl_subject'];?></td>
                <td><?= date("d F Y", $mail['added_on']);?></td>
                <td><a id="<?= $mail['_id'];?>" style="cursor: pointer;" onclick="send('<?= $mail['_id'];?>')">Send</a></td>
                <td><a id="<?= $mail['_id'];?>" style="cursor: pointer;" onclick="remove('<?= $mail['_id'];?>')">Delete</a></td>
            </tr>
            <?php } ?>
                </tbody>
              </table>
            </div>
            
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
<script>
	function send(id)
	{
		var r = confirm("Are you sure to send this mail template to iaminaqaba users?");
		if (r == false)
		{
			return false;
		}
		else 
		{
			$.ajax({
				url: '?r=mail/sendmails', 
				type: 'POST',
				data: 'id=' + id,
				success: function (data){
					alert('Mail sent');
				}
			});
		}
	}
	function remove(id)
	{
		var r = confirm("Are you sure to delete this mail template?");
		if (r == false)
		{
			return false;
		}
		else 
		{
			$.ajax({
				url: '?r=mail/remove', 
				type: 'POST',
				data: 'id=' + id,
				success: function (data){
					var row = $("#"+id).parents('tr');
					$('#maillist').dataTable().fnDeleteRow(row);
				}
			});
		}
	}
</script> 