<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
$this->title = 'Admin Users';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Admin Users</h1>
	</section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Admin User List</h3>
						<a href="?r=admin/addadmin"><input type="button" name="add" value="Add Admin User" class="btn btn-info right"/></a>
					</div>
					<div class="box-body">
						<table id="adminlist" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Name</th>
								  <th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($admins as $admin)
								{
									$id = $admin['_id'];
								?>	
									<tr>
										<td><?= $admin['username'];?></td>
										<td><a  id = "<?= $id;?>" href="javascript:void(0)" onclick="remove('<?= $id;?>')">Delete</a></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this admin?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=admin/adminremove', 
			type: 'POST',
			data: 'id=' + id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#adminlist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
</script>