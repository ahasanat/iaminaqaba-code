<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'User Listing';

?>
<div class="content-wrapper">
    <section class="content-header">
		<h1>Free Users</h1> 
		<!-- <ol class="breadcrumb">
        <li><a href="javascript:void(0)"><i class="mdi mdi-gauge"></i> Home</a></li>
        <li><a href="javascript:void(0)">Users</a></li>
		</ol> -->
    </section>
	<!-- Main content -->
	<section class="content"> 
      <div class="row">
        <div class="col-md-12">
          <!-- BAR CHART -->
          <div class="box box-success">

            <select id="selectMonth">
              <option value="1">January</option>
              <option value="2">February</option>
              <option value="3">March</option>
              <option value="4">April</option>
              <option value="5">May</option>
              <option value="6">June</option>
              <option value="7">July</option>
              <option value="8">Auguest</option>
              <option value="9">September</option>
              <option value="10">October</option>
              <option value="11">November</option>
              <option value="12">December</option>
            </select>
            <div class="box-header with-border">
              <h3 class="box-title">Bar chart of free users</h3>

              <div class="box-tools right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="zmdi zmdi-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="zmdi zmdi-close"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="chart">
                <canvas id="barChart" style="height:230px"></canvas>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>

    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box"> 
					<div class="box-header">
						<h3 class="box-title">User List</h3>
					</div>
					<div class="box-body">
						<table id="free_user" class="table table-bordered table-striped">
							<thead>
								<tr>
								  <th>Full Name</th>
								  <th>Email</th>
								  <th>Country</th>
								  <th>Gender</th>
								  <th>Posts</th>
								  <th>Comments</th>
								  <th>Credit History</th>
								  <th>Password</th>
								  <!--<th>Update</th>-->
								  <th>Status</th>
								  <th>Delete</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($userdatas as $userdata){
								$id = $userdata['_id'];	
								?>
								<tr>
									<td><?= $userdata['fullname'];?></td>
									<td><?php echo isset($userdata['email']) ? $userdata['email'] : '';?></td>
									<td><?= $userdata['country'];?></td>
									<td><?= $userdata['gender'];?></td>
									<td><a href="<?php echo Url::to(['post/view-user-posts', 'user_id' => "$id"]);?>">View</a></td>
									<td><a href="<?php echo Url::to(['post/view-user-comments', 'user_id' => "$id"]);?>">View</a></td>
									<td><a href="<?php echo Url::to(['userdata/credit-history', 'user_id' => "$id"]);?>">Click Here</a></td>
									<td id="<?=$userdata['_id']?>"><a href="javascript:void(0)" onclick="reset_password('<?=$userdata['_id']?>')">Reset</a></td>
									<td>
									<?php 
									if($userdata['status']==1){ ?>
									<a href="javascript:void(0)" onclick="update_user('<?=$userdata['_id']?>','1')">Active</a>
									<?php } else if($userdata['status']==2) { ?>
									<a href="javascript:void(0)" onclick="update_user('<?=$userdata['_id']?>','2')">Inactive</a>
									<?php } else { ?>
									<a href="javascript:void(0)" onclick="update_user('<?=$userdata['_id']?>','3')">Inactive</a>
									<?php } ?>
									</td>
									<td id="<?=$userdata['_id']?>"><a href="javascript:void(0)" onclick="delete_user('<?=$userdata['_id']?>','1')">Delete</a></td>
									<!--<td>
										<?php if($userdata['country'] !=''){
											echo $userdata['country'];
										}else{
											echo "---";
										}?></td>
										 <td><a target="_blank" href="<?php $uid = $userdata['_id']; echo Url::to(['userdata/viewuser', 'id' => "$uid"]);  ?>">Update</a>
									</td>-->
								</tr>
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div> 
    </section>
 
<script>

  $(function () {
      getUserDataPerDays(01); //this calls it on load
      $("select#selectMonth").change(function() {
        var $month = $(this).val();
        if($month != undefined || $month != null || $month != '') {
          getUserDataPerDays($month);
        }
      });
  });

  function getUserDataPerDays($month) {
    if($month != undefined || $month != null || $month != '') {
      $.ajax({ 
            url: '?r=userdata/getusercountperdays',
            type: 'POST',
            data: {$month},
            success: function(data) {
                var result = JSON.parse(data);
                if(result.status == true) {
                    $daysBulkArray = result.label;
                    $dataValues = result.data;

                    var areaChartData = {
                    labels: $daysBulkArray,
                    datasets: [
                      {
                        label: "Electronics",
                        fillColor: "rgba(210, 214, 222, 1)",
                        strokeColor: "rgba(210, 214, 222, 1)",
                        pointColor: "rgba(210, 214, 222, 1)",
                        pointStrokeColor: "#c1c7d1",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: $dataValues
                      },
                      {
                        label: "Digital Goods",
                        fillColor: "rgba(60,141,188,0.9)",
                        strokeColor: "rgba(60,141,188,0.8)",
                        pointColor: "#3b8bba",
                        pointStrokeColor: "rgba(60,141,188,1)",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(60,141,188,1)",
                        data: $dataValues
                      }
                    ]
                  };

                  var areaChartOptions = {
                    //Boolean - If we should show the scale at all
                    showScale: true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines: false,
                    //String - Colour of the grid lines
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    //Number - Width of the grid lines
                    scaleGridLineWidth: 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines: true,
                    //Boolean - Whether the line is curved between points
                    bezierCurve: true,
                    //Number - Tension of the bezier curve between points
                    bezierCurveTension: 0.3,
                    //Boolean - Whether to show a dot for each point
                    pointDot: false,
                    //Number - Radius of each point dot in pixels
                    pointDotRadius: 4,
                    //Number - Pixel width of point dot stroke
                    pointDotStrokeWidth: 1,
                    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                    pointHitDetectionRadius: 20,
                    //Boolean - Whether to show a stroke for datasets
                    datasetStroke: true,
                    //Number - Pixel width of dataset stroke
                    datasetStrokeWidth: 2,
                    //Boolean - Whether to fill the dataset with a color
                    datasetFill: true,
                    //String - A legend template
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                    maintainAspectRatio: true,
                    //Boolean - whether to make the chart responsive to window resizing
                    responsive: true
                  };
                //-------------
                  //- BAR CHART -
                  //-------------
                  var barChartCanvas = $("#barChart").get(0).getContext("2d");
                  var barChart = new Chart(barChartCanvas);
                  var barChartData = areaChartData;
                  barChartData.datasets[1].fillColor = "#00a65a";
                  barChartData.datasets[1].strokeColor = "#00a65a";
                  barChartData.datasets[1].pointColor = "#00a65a";
                  var barChartOptions = {
                    //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
                    scaleBeginAtZero: true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines: true,
                    //String - Colour of the grid lines
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    //Number - Width of the grid lines
                    scaleGridLineWidth: 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines: true,
                    //Boolean - If there is a stroke on each bar
                    barShowStroke: true,
                    //Number - Pixel width of the bar stroke
                    barStrokeWidth: 2,
                    //Number - Spacing between each of the X value sets
                    barValueSpacing: 5,
                    //Number - Spacing between data sets within X values
                    barDatasetSpacing: 1,
                    //String - A legend template
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
                    //Boolean - whether to make the chart responsive
                    responsive: true,
                    maintainAspectRatio: true
                  };

                  barChartOptions.datasetFill = false;
                  barChart.Bar(barChartData, barChartOptions);
                }
            }
      }); 
    }
  }

	function delete_user(id,d_type)
	{
		if(d_type =='2')
		{ 
			var r = confirm("Are you sure to Inactive this user?");
		}
		else if(d_type =='1')
		{ 
			var r = confirm("Are you sure to Delete this user?");
		}
			if (r == false) 
			{
				return false;
			}
			else 
			{
				$.ajax({
					url: '?r=userdata/deleteusertype', 
					type: 'POST',
					data: 'id=' + id+ '&d_type=' +d_type,
					success: function (data) {
						var row = $("#"+id).parents('tr');
						$('#free_user').dataTable().fnDeleteRow(row);
					}
				});
			}
	}
		
	function reset_password(id)
	{
		var r = confirm("Are you sure to reset password for this user?");
		if (r == false) {
			return false;
		}
		else 
		{
			$.ajax({
				url: '?r=userdata/reset-password', 
				type: 'POST',
				data: 'id=' + id,
				success: function (data) {
					alert("Password Successfully Changed..!!");
					//var row = $("#"+id).parents('tr');
					//$('#free_user').dataTable().fnDeleteRow(row);
				}
			});
		}
	}


	
</script>
