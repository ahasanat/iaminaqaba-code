<?php 
use yii\helpers\Html;
use frontend\models\LoginForm;
$this->title = 'Payment History';
$user = LoginForm::find()->where(['_id'=> $_GET['user_id']])->one();
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>Credit History</h1>
    </section>

    <!-- Main content -->
    <section class="content">
		
		
		<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
			<h3 class="box-title"><?=$user['fullname']?>'s Credit History</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="credit_history" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Credits</th>
                  <th>Type</th>
                  <th>Descriptions</th>
                </tr>
                </thead>
                <tbody>
    <?php foreach($userdatas as $credits){ ?>
            <tr>
                <td <?php if($credits['credits_desc']== 'transfercredits' || $credits['credits_desc']== 'usedcredits'){?> style="color: red;" <?php } else{ ?> style="color: green;" <?php } ?>><?= $credits['credits'];?></td>
                <td><?= $credits['credits_desc'];?></td>
                <td><?php 
					if($credits['credits_desc']== 'joinvip')
					{ 
						echo 'Become a Vip Member';
					}
					else if($credits['credits_desc']== 'purchasecredits')
					{ 
						echo 'Purchase Credits';
					}
					else if($credits['credits_desc']== 'verify')
					{ 
						echo 'Become a Verify Member';
					}
					else if($credits['credits_desc']== 'transfercredits')
					{ 
						$connect_name = $this->context->getuserdata($credits['detail'],'fullname');
						echo 'By tranfering credits to ' . $connect_name;
					}
					else if($credits['credits_desc']== 'earned')
					{ 
						echo 'Earns';
					}
					else if($credits['credits_desc']== 'usedcredits')
					{ 
						echo 'Used Credits';
					}
					else if($credits['credits_desc']== 'signup')
					{ 
						echo 'Become a new member in iaminaqaba';
					}
					else if($credits['credits_desc']== 'profilephoto')
					{ 
						echo 'For adding the profile photo';
					}
					else if($credits['credits_desc']== 'addconnect')
					{ 
						$connect_name = $this->context->getuserdata($credits['detail'],'fullname');
						echo 'Become a connect with '.$connect_name;
					}
					else if($credits['credits_desc']== 'pagelike')
					{ 
						echo 'Earns Credits for Liking Your Page';
					}
					else if($credits['credits_desc']== 'sharepost')
					{ 
						echo 'Earns Credits by Sharing The Post';
					}
					else
					{ 
						echo '---';
					}
					
					?></td>
                </tr>

            <?php }?>
                
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>