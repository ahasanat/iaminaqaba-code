<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
use frontend\models\Like;
use frontend\models\TravAdsVisitors;
$this->title = 'Active Ads';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Active Ads</h1>
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Active Ad List</h3>
					</div>
					<div class="box-body">
						<table id="allactiveadslist" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Name</th>
									<th>Ad Object</th>
									<th>Created By</th>
									<th>Created Date</th>
									<th>Likes / Impression</th>
									<th>Take Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($ads as $ad)
								{
									$postid = (string)$ad['_id'];
									$adobj = $this->context->getAdType($ad['adobj']);
									$ad_owner = $ad['post_user_id'];
									$owner_name = $this->context->getuserdata($ad_owner,'fullname');
									$like_count = Like::getLikeCount($postid);
									$impression = TravAdsVisitors::getCount($postid,'impression');
								?>
									<tr id="ad_<?=$postid?>">
										<td><?= $ad['adname'];?></td>
										<td><?= $adobj;?></td>
										<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $ad_owner;?>"><?= $owner_name;?></a></td>
										<td><?= date('d-M-Y',$ad['post_created_date']);?></td>
										<td><?= $like_count.' / '.$impression;?></td>
										<td>
											<a target="_blank" href="<?= $front_url;?>?r=site/travpost&postid=<?= $postid;?>">View</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>