<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

$asset = backend\assets\AppAsset::register($this);
$baseUrl = $asset->baseUrl;
$travelasset = backend\assets\TravelAsset::register($this);
$travelbaseUrl = $travelasset->baseUrl;
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?=$travelbaseUrl?>/images/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini adminpanel">
    <script src="<?php echo $travelbaseUrl; ?>/js/jquery.min.js" type="text/javascript"></script>  
<?php $this->beginBody() ?>

<div class="wrapper">
<?= $this->render('header.php',['baseUrl' => $baseUrl]) ?>
<?= $this->render('leftsidebar.php',['baseUrl' => $baseUrl]) ?>
    
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
 
</div>
<?= $this->render('footer.php',['baseUrl' => $baseUrl]) ?>   

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script>
  $(function () {
    $("#example1").DataTable();
	$("#collectionlist").DataTable();
	$("#allpageslist").DataTable();
	$("#pagereviewlist").DataTable();
	$("#pagephotolist").DataTable();
	$("#pagepostlist").DataTable();
	$("#collectionpostlist").DataTable();
	$("#alladslist").DataTable();
	$("#userlist").DataTable();
	$("#maillist").DataTable();
	$("#vipplan").DataTable();
	$("#postlist").DataTable();
	$("#commentlists").DataTable();
	$("#flagpostlist").DataTable();
	$("#allactiveadslist").DataTable();
	$("#alltourslist").DataTable();
	$("#tripexplist").DataTable();
	$("#placesreviewslist").DataTable();
	$("#placeslist").DataTable();
	$("#placestipslist").DataTable();
	$("#placesphotoslist").DataTable();
	$("#placesquestionslist").DataTable();
	$("#referencelist").DataTable();
	$("#buddylist").DataTable({ "aaSorting": [] });
	$("#hostlist").DataTable({ "aaSorting": [] });
	$("#activitylist").DataTable({ "aaSorting": [] });
	$("#free_user").DataTable();
	$("#NewFlagger").DataTable();
	$("#storecatlist").DataTable();
	$("#adminlist").DataTable();
	$("#credit_history").DataTable();
	$("#CoverPhotos").DataTable({ "aaSorting": [] }); 
	$("#userpostlist").DataTable();
	$("#verify_users").DataTable();
	$("#verify_statastics").DataTable();
	$("#occupation").DataTable();
	$("#education").DataTable();
	$("#interests").DataTable();
	$("#language").DataTable();
	$("#flag_page").DataTable();
	$("#sales_statastics").DataTable();
	$("#vip_statastics").DataTable();
	$("#vip_users").DataTable();
	$("#usercommentlist").DataTable();
	$("#collectionimglist").DataTable();
	$('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
	/*$('#exampless').DataTable( {
        "processing": true,
        "ajax": {
            "url": "?r=admin/alls",
            "type": "POST"
        },
        "columns": [
            { "data": "username" },
            { "data": "admin_email"},
        ]
    } );*/
    $("#messageabuse").DataTable({ "aaSorting": [] }); 
    $("#giftcostdetails").DataTable({ "aaSorting": [] }); 
  });
</script>
<?php if(isset($_GET['r']) && strstr($_GET['r'],'mail')){ ?>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script type="text/javascript">
  $(function () {
    CKEDITOR.replace('mailbody');
  });
</script>
<?php } ?>