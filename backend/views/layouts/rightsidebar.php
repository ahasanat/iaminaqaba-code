<footer class="footer">
    <div class="container">
        <p class="left">&copy; My Company <?= date('Y') ?></p>

        <p class="right"><?= Yii::powered() ?></p>
    </div>
</footer> 