<?php
use yii\helpers\Html;
use yii\helpers\Url;
$session = Yii::$app->session;
$role = $session->get('role');
$username = $session->get('username');
?>
<header class="main-header">
    <a href="javascript:void(0)" class="logo">
		<span class="logo-mini"><b>T</span>
		<span class="logo-lg"><b>Iaminaqaba</span>
    </a>
    <nav class="navbar navbar-static-top">
		<a href="javascript:void(0)" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
					  <img src="<?= $baseUrl;?>/dist/img/user2-160x160.jpg" class="user-image" alt="Admin">
					  <span class="hidden-xs"><?= ucfirst($username);?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header">
							<img src="<?= $baseUrl;?>/dist/img/user2-160x160.jpg" class="img-circle" alt="Admin">
							<p><?= ucfirst($username);?> - <?php if($role =='superadmin'){echo "Administrator";} else {echo "Admin";}?> <small>Member since Jan. 2016</small></p>
					    </li>
					    <li class="user-footer">
						    <div class="right">
						        <a href="?r=userdata%2Fout" class="btn btn-primary">Sign out</a>
							</div>
					    </li>
					</ul>
				</li>
			</ul>
		</div>
    </nav>
</header>