<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use frontend\assets\AppAsset;
$this->title = 'Reference';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
?>
<div class="content-wrapper">
	<section class="content-header">
		<h1>Reference</h1>
		<ol class="breadcrumb">
			<li><a href="?r=site"><i class="mdi mdi-gauge"></i> Home</a></li>
			<li><a href="?r=reference/all">Reference</a></li>
		</ol> 
    </section>
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Reference List</h3>
					</div>
					<div class="box-body">
						<table id="referencelist" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Reference</th>
									<th>Recommend</th>
									<th>Category</th>
									<th>From User</th>
									<th>To User</th>
									<th>Created Date</th>	
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($referals as $referal)
								{
									if($referal['is_deleted']=='1'){$status = 'Active';}else{$status= 'Inactive';}
									$toname = $this->context->getuserdata($referal['user_id'],'fullname');
									$fromname = $this->context->getuserdata($referal['sender_id'],'fullname');
									$id = $referal['_id'];
								?>
								<tr>
									<td><?= $referal['referal_text'];?></td>
									<td><?= $referal['recommend'];?></td>
									<td><?= $referal['category'];?></td>
									<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $referal['sender_id'];?>"><?= $fromname;?></a></td>
									<td><a target="_blank" href="<?= $front_url;?>?r=userwall/index&id=<?= $referal['user_id'];?>"><?= $toname;?></a></td>
									<td><?= date('d-M-Y',$referal['created_date']);?></td>
									<td><a  id = "update_<?= $id;?>" href="javascript:void(0)" onclick="update('<?= $id;?>','<?= $status;?>')" ><?= $status;?></a></td>
									<td><a  id = <?= $id;?> href="javascript:void(0)" onclick="remove('<?= $id;?>')">Delete</a></td>
								</tr>
								<?php 	
								}
								?>
								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
<script>
function remove(id)
{
	var r = confirm("Are you sure to delete this reference?");
	if(r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=reference/removeref', 
			type: 'POST',
			data: 'id='+id,
			success: function (data){
				var row = $("#"+id).parents('tr');
				$('#referencelist').dataTable().fnDeleteRow(row);
			}
		});
	}
}
function update(id,status)
{
	
	if(status == 'Active')
	{
		var statuss = 'Inactive';
	}else{
		var statuss = 'Active';
	}
	var r = confirm("Are you sure to "+statuss+" this reference?");
	if (r == false)
	{
		return false;
	}
	else 
	{
		$.ajax({
			url: '?r=reference/referenceupdate', 
			type: 'POST',
			data: 'id=' + id + '&status='+status,
			success: function (data)
			{
				$('#update_'+id).html(statuss);
			}
		});
	}
}
</script>