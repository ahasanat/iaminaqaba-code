<?php 
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'Host Listing';
$front_url = Yii::$app->urlManagerFrontEnd->baseUrl;
$isLast='';
if(isset($information) && !empty($information)) {
	$information = json_decode($information, true);
	if(!empty($information)) {
		$isLast = (string)$information[0]['price'];
	}
} else {
	$information = array();
} 

?>
<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
    	<h1>Gift</h1>
	</section>
	<!-- Main content -->
    <section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
			            <div class="box-header">
			              <h3 class="box-title">Update Gift Cost</h3>
			            </div>
			            <!-- /.box-header -->
			            <div class="box-body">
			              <form role="form">
			                <!-- select -->
			                <div class="form-group">
			                  <label class="col-xs-2">Select</label>
			                  <select class="col-xs-4"  id="giftupdatecostval">
			                  	<?php for($i=0; $i<=500; $i++) {
			                  		if($i%5 == 0) {
			                  			if((string)$i == (string)$isLast) {
				                    		echo '<option value='.$i.' selected> '.$i.' </option>';
				                  		} else {
				                    		echo '<option value='.$i.'> '.$i.' </option>';
				                    	}
				                    }
			                  	} ?>
			                  </select>
			                </div>
			                <div class="box-footer col-xs-12">
			               		<button type="button" onclick="giftupdatecostsubmit();" class="btn btn-primary">Submit</button>
			                </div>
			              </form>
			            </div>
			            <!-- /.box-body -->
			          </div>
				</div>
			</div>
		</div>
    </section>

    <section class="content-header">
    	<h1>Past Gift Cost Details</h1>
	</section>
	
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<h3 class="box-title">Gift Cost List</h3>
					</div>
					<div class="box-body">
						<div class="contentExist">
							<table id="giftcostdetails" class="table table-bordered table-striped">
								<thead>
									<tr><th>Cost</th>
										<th>Created By</th>
										<th>Created at</th>
									</tr>
								</thead>
								<tbody>
								<?php 
								foreach ($information as $key => $value) {
									$id = (string)$value['_id']{'$id'};
									//$userId = $value['user_id'];
									$price = $value['price'];
									$created_by = $value['created_by'];
									$created_at = $value['created_at'];
									echo '<tr class="record'.$id.'">
										<td>'.$price.'</td>
										<td>'.$created_by.'</td>
										<td>'.date("Y-m-d H:i:s", $created_at).'</td
									</tr>';
								} ?>
								</tbody>
							</table>
						</div>	
					</div>
				</div>
			</div>
		</div>
    </section>
</div>
 
<script>
	function giftupdatecostsubmit() {
		$price = $("#giftupdatecostval").val();
		if($price != undefined || $price != null || $price != '') {
			if($price>0) {
				BootstrapDialog.show({
				size: BootstrapDialog.SIZE_SMALL,
				title: 'Update Gift Cost',
				cssClass: 'custom-sdialog',
				message: 'Are you sure to change cost of gift ?',
				buttons: [{
						label: 'Keep',
						action: function (dialogItself) {
							dialogItself.close();
						}
					}, {
						label: 'change',
						action: function (dialogItself) {
							$.ajax({
								url: '?r=message/editupdatecost', 
								type: 'POST',
								data: {$price},
								success: function (data) {
									dialogItself.close();
									$filterResult = JSON.parse(data);
									if($filterResult.status != undefined && $filterResult.status == true) {
										$filterData = $filterResult.data;
										$cost = $filterData.price;
										$created_by = $filterData.created_by;
										$created_at = $filterData.created_at;

										var t = $('#giftcostdetails').DataTable();
        								t.row.add( [$cost, $created_by, $created_at] ).draw( false );
									}
								}
							});
						}
					}]
			});
			}
		}
	}
</script>
