<?php
 
namespace console\controllers;
 
use yii\console\Controller;
 
/**
 * Test controller
 */
class UniqController extends Controller {
	//https://www.youtube.com/watch?v=EZhYp6cPD5E
 
    public function actionWeMissYou() {

    	$date = strtotime(date('m/d/Y'));
        $today = strtotime(date('Ymd'));

        echo '============================================ START .'.date('m/d/Y').'.=============================================\n';
        echo 'Date:- '.date('m/d/Y');
        echo 'Date with string:- '.$date;

        $missingusr = array();
        if($date) {
            if($date == $today) {
                $info = UserForm::find()->select(['email', 'last_login_time', 'last_time', 'last_logout_time'])->asarray()->all();
                if(!empty($info)) {
                    foreach ($info as $key => $value) {
                        $second_last = (isset($value['last_time']) && $value['last_time'] != '') ? $value['last_time'] : ''; 
                        $last_time = (isset($value['last_login_time']) && $value['last_login_time'] != '') ? $value['last_login_time'] : '';
                        $real_last_time = (isset($value['last_logout_time']) && $value['last_logout_time'] != '') ? $value['last_logout_time'] : '';
                        $new_generate_time = '';

                        if($real_last_time != '') {
                            $new_generate_time = $real_last_time;
                        } elseif($last_time != '') {
                            $new_generate_time = $last_time;
                        } elseif($second_last != '') {
                            $new_generate_time = $second_last;
                        } 

                        if($new_generate_time != '') {
                            $days = $today - $new_generate_time;
                            $days = floor($days / (60 * 60 * 24));

                            if($days >= 30) {
                                $missingusr[] = $value;
                            }
                        }
                    }
                }
            }
        }

        if(!empty($missingusr)) {
        
        echo 'Missing Users List:- ======================================================\n';
        print_r($missingusr);
        
            foreach ($missingusr as $key => $value) {
                $email = $value['email'];
                $test = Yii::$app->mailer->compose()
                    ->setFrom('no-reply@iaminaqaba.com')
                    ->setTo($email)
                    ->setSubject('<b>Iaminaqaba- We Miss You.</b>')
                    ->setHtmlBody('<html><head><meta charset="utf-8" /><title>Iaminaqaba</title></head><body style="margin:0;padding:0;">      <div style="color: #353535; float:left; font-size: 13px;width:100%; font-family:Arial, Helvetica, sans-serif;text-align:center;padding:20px 0 0;"><div style="width:600px;display:inline-block;"><img src="https://www.iaminaqaba.com/frontend/web/assets/a8535972/images/logo.png" style="margin:0 0 20px;"/><div style="clear:both"></div><div style="padding:3px;border:1px solid #ddd;margin:0 0 20px;"><div style="background:#f5f5f5;padding:30px;"><div style="text-align: center;font-size: 34px;font-weight: bold;color:#4083BF;margin:0 0 20px;">We missing you !!</div><div style="text-align: center;font-size: 16px;"></div><div style="text-align: center;font-size: 14px;margin:10px 0 0;"><a href="www.iaminaqaba.com" style="font-family: Arial, Helvetica, sans-serif;color: #ffffff;background-color:#4083BF;padding: 8px 20px;text-decoration: none;display:inline-block;margin: 10px 0 0;">Click Here To Login</a></div></div></div></div><div style="float: left;width: 100%;text-align: center"><div style="color: #8f8f8f;text-align: center;">&copy;  www.iaminaqaba.com All rights reserved.</div><div style="text-align: center;font-weight: bold;width: 100%;margin:10px 0 20px;color:#505050;">For anything you can reach us directly at <a href="contact@iaminaqaba.com" style="color:#4083BF">contact@iaminaqaba.com</a></div></div></div></div></body></html>')     
                    ->send();                
            }
        }
    echo '============================================ END =============================================\n';
    }
}