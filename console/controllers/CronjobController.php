<?php
namespace console\controllers;
use Yii;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\mongodb\ActiveRecord;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\HtmlPurifier;
use frontend\models\UserPhotos;
use frontend\models\Gallery;
use frontend\models\General;
use yii\console\Controller;
use fedemotta\cronjob\models\CronJob;

class CronjobController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
      
    public function actions()
    {
        return [
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
                'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];           
    }
    
    public function actionGetphotostreampiccount()  
    {
        $Gallery = ArrayHelper::map(Gallery::find()->where(['type' => 'places'])->andWhere(['not','flagger', "yes"])->asarray()->all(), function($data){return (string)$data['_id'];}, 'image');
        $eximgs = array();
        if(!empty($Gallery)) {
            foreach ($Gallery as $S_Gallery) {
                $c_images = explode(',', $S_Gallery);
                $eximgs = array_merge($eximgs, $c_images);
            }
        }

        $eximgs = array_values(array_filter($eximgs));
        $eximgs = array_unique($eximgs);

        return count($eximgs);
    }

    public function actionStoredphotostreamcount() {
        $count = $this->actionGetphotostreampiccount();
        return General::storedphotostreamcount($count);
    }
}
?>