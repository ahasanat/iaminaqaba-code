<?php
return [
    'components' => [
         'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            //'dsn' => 'mongodb://192.168.1.42:27017/demo',    /* Hiral */ 

            // Local
            //'dsn' => 'mongodb://localhost:27017/demo' 

            // Live
            'dsn' => 'mongodb://adel:hasanat@localhost:27017/travbud',
		],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
        ],
    ],
];
