<?php
if($_SERVER['HTTP_HOST'] != 'localhost') {
    $dsnLink = 'mongodb://adel:hasanat@localhost:27017/travbud';
} else {
    $dsnLink = 'mongodb://localhost:27017/localhost_aqaba';
    //$dsnLink = 'mongodb://localhost:27017/iaminaqaba07-07-2018';
}

return [
    'components' => [
         'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => $dsnLink,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
        ],
        'cache' => [
            'class' => 'yii\redis\Cache',
            'redis' => [
                'hostname' => '34.217.21.181',
                'port' => 6379,
                'database' => 0,
            ]
        ]
    ],
];
