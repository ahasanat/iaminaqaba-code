var fs = require('fs');
var express = require('express');
var app = express();
var https = require('https');
//var server = require('http').createServer();
var server = https.createServer({
    key: fs.readFileSync('/etc/letsencrypt/live/iaminaqaba.com/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/iaminaqaba.com/fullchain.pem')
 },app);
server.listen(4000);
var io = require('socket.io').listen(server);
var logger = require('./utils/logger');
var ip = require('ip');
var mongoose = require('mongoose');

var conversationPath = '/var/www/html/frontend/web/';
mongoose.connect('mongodb://adel:hasanat@localhost/travbud', function(err) {
    if (!err) {
        logger.info("Connected to Live mongodb database!");
    } else {
        logger.error(err);
    }
});


/*var conversationPath = '../../frontend/web/';
mongoose.connect('mongodb://localhost/localhost_aqaba', function(err) {
    if (!err) {
        logger.info("Connected to Local mongodb database!");
    } else {
        logger.error(err);
    }
});
*/
var users = [];
require('./utils/socket')(io, logger, conversationPath);