<?php 
namespace api\modules\v1\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;
use yii\mongodb\ActiveRecord;
use yii\db\ActiveQuery;
use yii\mongodb\Query;
use yii\web\UploadedFile;
use api\modules\v1\models\LoginForm;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

class Connect extends ActiveRecord 
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'connect';
    }

     public function attributes()
    {
        return ['_id', 'from_id', 'to_id', 'status', 'action_user_id','created_date','updated_date'];
    }
    
    public function getUserdata()
    {
        return $this->hasOne(UserForm::className(), ['_id' => 'from_id']);
    }
       
    public function scenarios()
    {
        $scenarios = parent::scenarios();     
        return $scenarios;
    }
   
    
     /**
     * Displays users pending requests.
     *
     * @return mixed
     */
   
    public function getuserConnections($user_id)
     {
            $user_Connections =  Connect::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$user_id"])->all();
            return $user_Connections;
    
     }
     
     /**
     * Displays users pending requests.
     *
     * @return mixed
     */
   
    public function connectPendingRequests()
     {
            $session = Yii::$app->session;
            $uid = (string)$session->get('user_id');
            $pending_request =  Connect::find()->with('userdata')->Where(['not','action_user_id', "$uid"])->andwhere(['status'=>'0'])->andwhere(['to_id'=>"$uid"])->andWhere(['not','from_id', "$uid"])->all();
          return $pending_request;
         
 
     }
     
    public function connectPendingRequestsAPI($uid)
    {
        $pending_request =  Connect::find()->with('userdata')->Where(['not','action_user_id', "$uid"])->andwhere(['status'=>'0'])->andwhere(['to_id'=>"$uid"])->andWhere(['not','from_id', "$uid"])->all();
        return $pending_request;
         
 
    }
     
     /**
     * Displays users pending requests count.
     *
     * @return mixed
     */
   
    public function connectRequestbadge()
     {
            $session = Yii::$app->session;
            $uid = (string)$session->get('user_id');
            $array = [$uid];  
            $result_requests = Connect::find()->Where(['not','action_user_id', "$uid"])->andwhere(['status'=>'0'])->andwhere(['to_id'=>"$uid"])->andWhere(['not','from_id', "$uid"])->all();
         
          $count = count($result_requests);
        
          return $count;
       
          
     }
     
     
      /**
     * Get all user posts Likes in decsending order
     * @param  post id 
     * @return array
     * @Author  Sonali Patel
     * @Date    02-04-2016 (dd-mm-yyyy)
     */  
     public function userlistFirstfive($uid)
     {
		$array = [$uid];
		$exist_ids = $pending_ids = array();   
		$requestexists =  Connect::find()->where(['from_id'=>"$uid"])->all();
		if(!empty($requestexists) && count($requestexists)>0)
		{
			foreach($requestexists AS $requestexist)
			{
				$exist_ids[] = $requestexist['from_id'];
			}
		}
		//
		$requestpendings =  Connect::find()->where(['to_id'=>"$uid"])->all();
		 if(!empty($requestpendings) && count($requestpendings)>0)
		{
			foreach($requestpendings AS $requestpending)
			{
				$pending_ids[] = $requestpending['from_id'];
			}
		}
	  
		
		$array_final = array_unique (array_merge ($exist_ids, $pending_ids,$array));
	   // print_r($array_final);exit;
		$result_Connections = LoginForm::find()->where(['not in','_id',$array_final])->andwhere(['status'=>'1'])->orderBy(['ontop' => SORT_DESC,
	'rand()' => SORT_DESC,])->all();
		$count = count($result_Connections);
		return $result_Connections;
        
     }
    /**
     * Get all user posts Likes in decsending order
     * @param  post id 
     * @return array
     * @Author  Sonali Patel
     * @Date    02-04-2016 (dd-mm-yyyy)
     */  
     public function userlist()
     {

            $session = Yii::$app->session;
            $uid = (string)$session->get('user_id');
            $array = [$uid];
            
            $result_Connections = LoginForm::find()->where(['not in','_id',$array])->andwhere(['status'=>'1'])->orderBy(['_id'=>SORT_DESC])->all();
        
            return $result_Connections;
        
     }
     
     public function searchconnect()
     {

            $session = Yii::$app->session;
            $email = $session->get('name');
            
            
          //  $array = [$email];
            
            $result_Connections = LoginForm::find()->where(['like','email',$email])
                    ->orwhere(['like','fname',$email])
                    ->orwhere(['like','lname',$email])
                    ->orwhere(['like','photo',$email])
                    ->orwhere(['like','phone',$email])
                    ->orwhere(['like','fullname',$email])
                    ->andwhere(['status'=>'1'])->orderBy(['_id'=>SORT_DESC])->all();
    
        
            return $result_Connections;
        
     }
     
     /**
     * Get all users for share post
     * @param  post id 
     * @return array
     * @Author  Alap Shah
     * @Date    28-04-2016 (dd-mm-yyyy)
     */
     function userlistsuggetions($sug)
     {
        if (\Yii::$app->user->isGuest)
        {
            return $this->goHome();
        }
        else
        {
            return LoginForm::find()->where(['like','username',$sug,false])->andwhere(['status'=>'1'])->orderBy(['_id'=>SORT_DESC])->limit(10)->all();
        }
     }
     
     /**
     * Displays count of mutual Connections.
     *
     * @return mixed
     */
     public function mutualconnectcount($id)
     {
        $session = Yii::$app->session;
        $uid = (string)$session->get('user_id');
        
        //Find Connections of both the user 
        $Connections_of_a = Connect::find()->where(['from_id'=>"$uid",'status'=>'1'])->all();
        $Connections_of_b  = Connect::find()->where(['from_id'=>"$id",'status'=>'1'])->all();
       
        //Create array for find intersection
        $arr_a = $arr_b = array();
        foreach($Connections_of_a as $t)
        {
            $arr_a[$t->to_id] = $t->attributes;
        }

        foreach($Connections_of_b as $t1)
        {
            $arr_b[$t1->to_id] = $t1->attributes;

        }
        //Return same/mutual Connections
        $result_mutual = array_intersect_key($arr_a,$arr_b);
        $count = count($result_mutual);
        return $count;
     }
     public function mutualconnectcountAPI($id,$uid)
     {
        //Find Connections of both the user 
        $Connections_of_a = Connect::find()->where(['from_id'=>"$uid",'status'=>'1'])->all();
        $Connections_of_b  = Connect::find()->where(['from_id'=>"$id",'status'=>'1'])->all();
       
        //Create array for find intersection
        $arr_a = $arr_b = array();
        foreach($Connections_of_a as $t)
        {
            $arr_a[$t->to_id] = $t->attributes;
        }

        foreach($Connections_of_b as $t1)
        {
            $arr_b[$t1->to_id] = $t1->attributes;

        }
        //Return same/mutual Connections
        $result_mutual = array_intersect_key($arr_a,$arr_b);
        $count = count($result_mutual);
        return $count;
     }
     public function mutualconnect($id,$uid)
     {
        
        //Find Connections of both the user 
        $Connections_of_a = Connect::find()->where(['from_id'=>"$uid",'status'=>'1'])->all();
        $Connections_of_b  = Connect::find()->where(['from_id'=>"$id",'status'=>'1'])->all();
        $i = $j = $k = 0;
        //Create array for find intersection
        $arr_a = $arr_b = array();
        foreach($Connections_of_a as $t)
        {
            $arr_a[$t->to_id] = $t->attributes;
          //  $i++;
        }

        foreach($Connections_of_b as $t1)
        {
            $arr_b[$t1->to_id] = $t1->attributes;
           // $j++;
        }
        //Return same/mutual Connections
        $result_mutual = array_intersect_key($arr_a,$arr_b);
        foreach($result_mutual as $doc)
        {
            $results[$k] = UserForm::getUserDetails($doc['to_id']); 
            $k++;
        }
       // echo "<pre>";print_r($results);exit;
        return $results;
     }
     /**
     * Displays count of request exists.
     *
     * @return mixed
     */
     public function requestexists($id)
     {
        $session = Yii::$app->session;
        $uid = (string)$session->get('user_id');
        
        $result_exists = Connect::find()->where(['from_id'=>"$id",'to_id'=>"$uid"])->all();
        $count = count($result_exists);
        return $count;
     }
      /**
     * Displays count of already request exists.
     *
     * @return mixed
     */
     public function requestalreadysend($id)
     {
        $session = Yii::$app->session;
        $uid = (string)$session->get('user_id');
  
        $result_exists = Connect::find()->where(['from_id'=>"$uid",'to_id'=>"$id"])->all();
        $count = count($result_exists);

        return $count;
     }
      /**
     * Displays users pending requests.
     *
     * @return mixed
     */
   
    public function getConnectionsCity($uid)
     {
        //$session = Yii::$app->session;
        //$uid = (string)$session->get('user_id');
        $user_Connections =  Connect::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $cities = '';
        foreach($user_Connections AS $user_connect)
        {
            $city = Personalinfo::getCity($user_connect['userdata']['_id']);
            $cities .=  "'".$user_connect['userdata']['city']."',";
        }
      //  exit;
        return $cities;
    
     }
   
   /**
     * Displays users pending requests.
     *
     * @return mixed
     */
   
    public function getConnectionsNames($uid)
     {
        //$session = Yii::$app->session;
        //$uid = (string)$session->get('user_id');
        $user_Connections =  Connect::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $names = '';//echo '<pre>';print_r($user_Connections);
        foreach($user_Connections AS $user_connect)
        {
           $names .=  "'".$user_connect['userdata']['fname'].' '.$user_connect['userdata']['lname']."',";
        }
        // exit;
        return $names;
    
     }
     /**
     * Displays users pending requests.
     *
     * @return mixed
     */
   
    public function getConnectionsLinks($uid)
     {
        //$session = Yii::$app->session;
        //$uid = (string)$session->get('user_id');
        $user_Connections =  Connect::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $names = '';//echo '<pre>';print_r($user_Connections);
        foreach($user_Connections AS $user_connect)
        {
           $names .=  "'".$user_connect['userdata']['_id']."',";
        }
        // exit;
        return $names;
    
     }
     /**
     * Displays users pending requests.
     *
     * @return mixed
     */
   
    public function getConnectionsImages($uid,$from)
     {
        $user_Connections =  Connect::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $images = '';
        foreach($user_Connections AS $user_connect)
        {
            if($from == 'wallajax')
            {
                $mapdp = $this->getimage($user_connect['userdata']['_id'],'thumb');
            }
            else
            {
                $mapdp = $this->context->getimage($user_connect['userdata']['_id'],'thumb');
            }
           $images .=  "'".$mapdp."',";
        }
        // exit;
        return $images;
    
     }
    /**
     * check if two users are Connections or tobe freinds
     * @param  first user id ,second user id 
     * @return array
     * @Author  Alap Shah
     * @Date    28-04-2016 (dd-mm-yyyy)
     */
     function Connections_or_tobe_Connections($to_id,$from_id)
     {
        $is_Connections =  Connect::find()->with('userdata')->Where(['from_id'=>"$from_id",'to_id'=> "$to_id"])->orWhere(['from_id'=> "$to_id",'to_id'=>"$from_id"])->all();
        
        if(count($is_Connections)>0)
            return true;
        else
            return false;
     }
     
     public function getConnectionsCityValue($uid)
     {
        $user_Connections =  Connect::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
        $cities = '';
        foreach($user_Connections AS $user_connect)
        {
            $city = Personalinfo::getCity($user_connect['userdata']['_id']);
            $prepAddr = str_replace(' ','+',$user_connect['userdata']['city']);
            $prepAddr = str_replace("'",'',$prepAddr);
            if(isset($prepAddr) && !empty($prepAddr))
            {
                $geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
                $output = json_decode($geocode);
                $latitude = $output->results[0]->geometry->location->lat;
                $longitude = $output->results[0]->geometry->location->lng;
                $cities .=  "['".$prepAddr."',".$latitude.",".$longitude."],";
            }
        }
        return substr($cities,0,-1);
    
     }
     
	public function getConnectionsMapDetails($uid)
	{
		$user_Connections =  Connect::find()->with('userdata')->Where(['status'=>'1'])->andWhere(['to_id'=> "$uid"])->all();
		$fredetails = '';
		foreach($user_Connections AS $user_connect)
		{
			$freid = $user_connect['userdata']['_id'];
			$frename = $user_connect['userdata']['fullname'];
			$prepAddr = str_replace(' ','+',$user_connect['userdata']['city']);
			$mapdp = $this->context->getimage($freid,'thumb');
			$frelink = Url::to(['userwall/index', 'id' => "$freid"]);
			if(isset($prepAddr) && !empty($prepAddr))
			{
				$connect_content = '\'<img height="18" width="18" src="'.$mapdp.'"/> <a href="'.$frelink.'">'.$frename.'</a>\'';
				$fredetails .=  "[$connect_content],";
			}
		}
		return substr($fredetails,0,-1);
	}
     
}
