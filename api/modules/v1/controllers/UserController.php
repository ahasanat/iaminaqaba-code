<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use api\modules\v1\models\UserForm;
use api\modules\v1\models\UserSetting;
use api\modules\v1\models\Connect;
use api\modules\v1\models\Comment;
use api\modules\v1\models\Like;
use api\modules\v1\models\LoginForm;
use api\modules\v1\models\Notification;
use api\modules\v1\models\ProfileVisitor;
use api\modules\v1\models\HideComment;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Expression;
use yii\web\UploadedFile;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use frontend\models\PostForm;
use frontend\models\Occupation;
use frontend\models\Education;
use frontend\models\Language;
use frontend\models\Interests;
use frontend\models\HidePost;
use frontend\models\SavePost;
use frontend\models\TurnoffNotification;
use frontend\models\NotificationSetting;
use frontend\models\UnfollowConnect;
use frontend\models\MuteConnect;
use frontend\models\BlockConnect;
use frontend\models\SuggestConnect;
use frontend\models\PinImage;
use frontend\models\Credits;
use frontend\models\Personalinfo;
use frontend\models\SecuritySetting;
use frontend\models\Page;

/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class UserController extends ActiveController
{
    
    public $modelClass = 'api\modules\v1\models\UserForm';
   
    /* Authorization Status code */
    private function _getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however, this will suffice
        // for an example
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }
    
    /* Response status */
    private function _sendResponse($status = 200, $body = '', $content_type = 'text/html')
    {
        // set the status
        $status_header = 'HTTP/1.1 ' . $status . ' ' . $this->_getStatusCodeMessage($status);
        header($status_header);
        // and the content type
        header('Content-type: ' . $content_type);

        // pages with body are easy
        if($body != '')
        {
            // send the body
            echo $body;
        }
        // we need to create the body if none is passed
        else
        {
            // create some body messages
            $message = '';

            // this is purely optional, but makes the pages a little nicer to read
            // for your users.  Since you won't likely send a lot of different status codes,
            // this also shouldn't be too ponderous to maintain
            switch($status)
            {
                case 401:
                    $message = 'You must be authorized to view this page.';
                    break;
                case 404:
                    $message = 'The requested URL ' . $_SERVER['REQUEST_URI'] . ' was not found.';
                    break;
                case 500:
                    $message = 'The server encountered an error processing your request.';
                    break;
                case 501:
                    $message = 'The requested method is not implemented.';
                    break;
            }

            // servers don't always have a signature turned on 
            // (this is an apache directive "ServerSignature On")
            $signature = ($_SERVER['SERVER_SIGNATURE'] == '') ? $_SERVER['SERVER_SOFTWARE'] . ' Server at ' . $_SERVER['SERVER_NAME'] . ' Port ' . $_SERVER['SERVER_PORT'] : $_SERVER['SERVER_SIGNATURE'];

            // this should be templated in a real-world solution
            $body = '
                <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "https://www.w3.org/TR/html4/strict.dtd">
                <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                    <title>' . $status . ' ' . $this->_getStatusCodeMessage($status) . '</title>
                </head>
                <body>
                    <h1>' . $this->_getStatusCodeMessage($status) . '</h1>
                    <p>' . $message . '</p>
                    <hr />
                    <address>' . $signature . '</address>
                </body>
                </html>';

            echo $body;
        }
       return true;
    }
    
    /* Check Authorization by headers HTTP_X_USERNAME & HTTP_X_PASSWORD */
    private function _checkAuth()
    {//echo "<pre>";print_r();exit;
        $headers = apache_request_headers();
        $header_username = $headers['HTTP_X_USERNAME'];
        $header_pwd = $headers['HTTP_X_PASSWORD'];
        // Check if we have the USERNAME and PASSWORD HTTP headers set?
        if(!(isset($header_username) and isset($header_pwd))) {
            // Error: Unauthorized
            $this->_sendResponse(401);
        }
        $username = $header_username;
        $password = $header_pwd;
        // Find the user
        $user = UserForm::find('LOWER(email)=?',array(strtolower($username)));
        if($user===null) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Name is invalid');
        } else if(!LoginForm::find()->where(['email' => $username,'password' => $password])->orwhere(['phone'=> $username,'password' => $password])->one()) {
            // Error: Unauthorized
            $this->_sendResponse(401, 'Error: User Password is invalid');
        }
    }
    
    /* Get Image API for image url*/
    public function getimageAPI($userid,$type)
    {
        $resultimg = LoginForm::find()->where(['_id' => $userid])->one();
        if(substr($resultimg['photo'],0,4) == 'http')
        {
            if($type == 'photo')
            {
                $dp = $resultimg['photo'];
            }
            else
            {
                $dp = $resultimg['thumbnail'];
            }
        }
        else
        {
            if(isset($resultimg['thumbnail']) && !empty($resultimg['thumbnail']))
            {
                $dp = "profile/".$resultimg['thumbnail'];
            }
            else if(isset($resultimg['gender']) && !empty($resultimg['gender']))
            {
                $dp = "<?=$baseUrl?>/images/".$resultimg['gender'].'.jpg';
            }
            else
            {
                $dp = "<?=$baseUrl?>/images/Male.jpg";
            }
        }
        return $dp;
    }
    
    /* Get User's Detail by user Id */
    
    public function actionSearchuser()
	{
       // $this->_checkAuth();  /* Only this line will do authentication by header parameters email & password */
       $new_user_id = $_GET['_id'];
       $result = UserForm::find()
               ->where(['_id' => $_GET['_id']])
              ->one();
       
       return $result;
    }
    
   /* Get All User's Detail as List */
    public function actionAlluser()
	{
       $country = new UserForm();
       $result = array();
       $result['status'] = true;
       $result['statusMsg'] = "User List";
       $result['data'] = UserForm::find()->all();
       
       return $result;
    }
    
    /* Update User's Detail */
    public function actionUpdateuser()
	{
        $user = new UserForm();
        $result = array();
        $record = UserForm::find()->where(['_id' => $_POST['_id']])->one();
             if(!empty($record)){
                $record->fname = $_POST['fname'];
                $record->lname = $_POST['lname'];
                $record->phone = $_POST['phone'];
                $record->birth_date = $_POST['birth_date'];
                $record->update();
                $result['status'] = true;
                $result['statusMsg'] = "User Updated successfully";
             }else{
                    $result['status'] = false;
                    $result['statusMsg'] = "User update fail";
             }
        
        $result['data'] = UserForm::find()->where(['_id' => $_POST['_id']])->one();
       
        return $result;
    }
    
    /* Register New User */
    public function actionSignupuser()
	{
        $model = new LoginForm();
        $model->scenario ='signup';
        $modelLog = new LoginForm();
        $modelLog->scenario = 'profile_picture';
		$resultres = array();
        if (isset($_POST['email']) && !empty($_POST['email']))
		{
            $email = strtolower($_POST['email']);
        }
        $result = LoginForm::find()->where(['email' => $email])->one();
        if($result)
		{
			$ids = array();
			$ids['_id'] 	  = $result['_id'];
			$usrdata['id'] 	  = $ids;
			$usrdata['fb_id'] = $result['fb_id']; 
			$usrdata['fname'] = $result['fname']; 
			$usrdata['lname'] = $result['lname'];  
			$usrdata['cover'] = $result['cover_photo'];  
			$usrdata['thumb'] = $result['thumbnail'];  
			$usrdata['email'] = $result['email'];
			$id = (string) $result['_id'];
            $resultres['status'] = false;
            $resultres['statusMsg'] = "User Already Exist";
			$resultres['data'] = $usrdata;
        }
		else
		{
            $response = $model->saverecordAPI();
            $idd = (string) $response['_id'];
            $fname = $response['fname'];
            $lname = $response['lname'];
            $username = LoginForm::generateUsername($fname,$lname);
            $result_unm = LoginForm::find()->where(['_id' => "$idd"])->one();
            if(!empty($_FILES['photo']))
			{
				$profile_picture = $_FILES['photo']['name'];
				$chars = '0123456789';
				$count = mb_strlen($chars);
				for ($i = 0, $rand = ''; $i < 8; $i++)
				{
					$index = rand(0, $count - 1);
					$rand .= mb_substr($chars, $index, 1);
				}
				$profile_picture = $rand . $profile_picture;
				$s3 = LoginForm::find()->where(['email' => $email])->one();
				if(!empty($s3))
				{
					$model->photo = UploadedFile::getInstanceByName( 'photo');
					$model->photo->saveAs('../../frontend/web/profile/'.$profile_picture);

					$s3->thumbnail = $profile_picture;
					$s3->photo = $profile_picture;

					$s3->update();
					//insert profile photo as user post
					$date = time();
					$post = new PostForm();
					$post->post_status = '1';
					$post->post_type = 'profilepic';
					$post->image = $profile_picture;
					$post->post_created_date = $date;
					$post->post_user_id = (string)$s3['_id'];
					$post->insert();
				}
                
            }
            if (!empty($result_unm)) {
                $username = LoginForm::generateUsername($fname,$lname);
                
                $result_unm->username = $username;
                $result_unm->update();
            } else {
               
                $result_unm->username = $username;
                $result_unm->update();
            }
            
            $result_final = LoginForm::find()->where(['_id' => "$idd"])->one();
			$ids = array();
			$ids['_id'] 	  = $result_final['_id'];
			$usrdata['id'] 	  = $ids;
			$usrdata['fb_id'] = $result_final['fb_id']; 
			$usrdata['fname'] = $result_final['fname']; 
			$usrdata['lname'] = $result_final['lname'];  
			$usrdata['cover'] = $result_final['cover_photo'];  
			$usrdata['thumb'] = $result_final['thumbnail'];  
			$usrdata['email'] = $result_final['email'];
            $userSetting = new UserSetting();
            $userSetting->user_id = $idd;
            $userSetting->user_theme ='theme-color';
            $userSetting->insert();

            $resultres['status'] = true;
            $resultres['statusMsg'] = "User Successfully register";
            $resultres['data'] = $usrdata;
        }
        
        return $resultres;
    }
    
    /* User Login method */
    public function actionLoginuser()
	{
        $model = new LoginForm();
        $model->scenario = 'login';
        $resultres = array();
		$value = $model->loginAPI();
		if ($value == "1") 
		{
			$email = strtolower($_POST['email']);
			$password = $_POST['password'];
			$id = LoginForm::getLastInsertedRecord($email);
			$user_id = (string) $id['_id'];
			$update = LoginForm::find()->where(['_id' => "$user_id"])->one();
			$date =  time();
			$update->lat = $_POST['lat'];
			$update->long = $_POST['long'];
			$update->login_from_ip = $_POST['remote_addr'];
			$update->update();
			
			$usrdata = array();
			$fbid =  $update['fb_id'];
			$dp = $update['photo'];

			$nm = (isset($update['fullname']) && !empty($update['fullname'])) ? $update['fullname'] : $update['fname'].' '.$update['lname'];
			$usrdata['id'] = $id; 
			$usrdata['fbid'] = $fbid; 
			$usrdata['name'] = $nm; 
			$usrdata['text'] = $nm; 
			$usrdata['thumb'] = $dp;
			$usrdata['email'] = $update['email'];
			if(isset($update['cover_photo'])){$usrdata['cover_photo'] = $update['cover_photo'];}else{$usrdata['cover_photo'] = "";}
			$resultres['status'] = true;
			$resultres['statusMsg'] = "User Successfully Login";
			$resultres['data'] = $usrdata;
		}
		else 
		{
			$resultres['status'] = false;
			$resultres['statusMsg'] = "User Login fail";
		}
		return $resultres;
    }
    
    /* forgot password by email Id */
    public function actionForgotpassword() 
	{
        $model = new LoginForm();
        if (isset($_POST['forgotemail']) && !empty($_POST['forgotemail'])) {
            $data = array();
            $getinfouser = LoginForm::find()->where(['email' => $_POST['forgotemail']])->orwhere(['alternate_email' => $_POST['forgotemail']])->one();
            if ($getinfouser) {
                $name = $getinfouser->fname;
                $forgot_id = $getinfouser->_id;
                $fname = ucfirst($name);
                $encrypt = strrev(base64_encode($forgot_id));
                $resetlink = "https://www.iaminaqaba.com/frontend/web/index.php?r=site/index&enc=$encrypt";
                
                try {
                    $test =
                            Yii::$app->mailer->compose()
                            ->setFrom(array('csupport@iaminaqaba.com' => 'Iaminaqaba Security'))
                            ->setTo($getinfouser['email'])
                            ->setSubject(''.$fname.', here\'s the link to reset your password')
                            ->setHtmlBody('<html>
            <head>
                    <meta charset="utf-8" />
                    <title>Iaminaqaba</title>
            </head>

            <body style="margin:0;padding:0;background:#dfdfdf;">
                    <div style="color: #353535; float:left; font-size: 13px;width:100%; font-family:Arial, Helvetica, sans-serif;text-align:center;padding:40px 0 0;">
                            <div style="width:600px;display:inline-block;">
                                    <img src="https://www.iaminaqaba.com/frontend/web/assets/baf1a2d0/images/black-logo.png" style="margin:0 0 10px;width:130px;float:left;"/>
                                    <div style="clear:both"></div>
                                    <div style="border:1px solid #ddd;margin:0 0 10px;">
                                            <div style="background:#fff;padding:20px;border-top:10px solid #333;text-align:left;">
                                                    <div style="color: #333;font-size: 13px;margin: 0 0 20px;">Hi ' . $fname . '</div>
                                                    <div style="color: #333;font-size: 13px;">You recently requested a password reset.</div>
                                                    <div style="color: #333;font-size: 13px;margin: 0 0 20px;">To change your Iaminaqaba password, click <a href="' . $resetlink . '" target="_blank">here</a> or paste the following link into your browser: <br/><br/><a href="' . $resetlink . '" target="_blank">' . $resetlink . '</a></div>
                                                    <div style="color: #333;font-size: 13px;">Thank you for using Iaminaqaba!</div>
                                                    <div style="color: #333;font-size: 13px;">The Iaminaqaba Team</div>
                                            </div>
                                    </div>
                                    <div style="clear:both"></div>
                                    <div style="width:600px;display:inline-block;font-size:11px;">
                                       <div style="color: #777;text-align: left;">&copy;  www.iaminaqaba.com All rights reserved.</div>
                                       <div style="text-align: left;width: 100%;margin:5px  0 0;color:#777;">For support, you can reach us directly at <a href="csupport@iaminaqaba.com" style="color:#4083BF">csupport@iaminaqaba.com</a></div>
                               </div>
                            </div>
                    </div>

            </body>
    </html>')
                            ->send();
                    $status = true;
                    $msg = 'Activation link sent to your Email Address';
                } catch (ErrorException $e) {
                    $msg = 'Caught exception: '. $e->getMessage(). "\n";
                    $status = false;
                }
            } else {
                $status = false;
            }
        } else {
            $status = false;
        }
        $rr = array();
            
            $rr['status'] = $status;
            $rr['statusMsg'] = $msg;  
            return $rr;
    }
    
    /* Get All posts by user Id & email address */
    public function actionMainfeed()
	{
		$title = 'sd';
		$model = new LoginForm();
		$email = $_POST['email'];
		$user_id = $_POST['user_id'];
		if (isset($_POST['theme'])) 
		{
            $result = LoginForm::find()->where(['email' => $email])->one();
			$id = $result['_id'];
			$userSetting = UserSetting::find()->where(['user_id' => "$id"])->one();
			$userSetting->user_theme = $_POST['theme'];
			$userSetting->update(); 
        }
        $count_post = PostForm::getUserConnectionsPostsAPICount($user_id);
		if(!isset($_POST['pagenumber']))
		{
			$page_number = 1;
		}
		else
		{
			$page_number = $_POST['pagenumber'];
		}
		$num_rec_per_page=20;
		$start_from = ($page_number-1) * $num_rec_per_page;
		$total_pages = ceil($count_post / $num_rec_per_page); 
		$posts =  PostForm::getUserConnectionsPostsAPI($user_id,$start_from,$num_rec_per_page);
		if(count($posts) > 0)
		{
			$post_array = array();
			$i = 0;
			foreach ($posts as $doc)
			{
				$like_array = array();
				$comment_array = array();
				$feedback_array = array();
				$attachment_array = array();
				$tagedactornames_array = array();
				$pictures_array = array();
				$videos_array = array();
				$links_array = array();
				$shares_array = array();
				$j = 0;
				$k = 0;
				$post_id = (string) $doc['_id'];
				$likes = Like::find()->where(['post_id' => $post_id ,'status' => '1'])->all();
				foreach($likes as $docLike)
				{
					$like_array[$j] = UserForm::getUserDetails($docLike['user_id']); 
					$j++;
				}
				$loaded_comments = Comment::find()->with('user')->with('post')->where(['post_id' => "$post_id",'status' => '1','parent_comment_id' => '0'])->orderBy(['created_date'=>SORT_DESC])->all();
				foreach($loaded_comments as $docComment)
				{
					$comment_array[$k]['main_comment'] = $docComment;
					$comment_array[$k]['actor'] = UserForm::getUserDetails($docComment['user_id']);
					$pcid = (string) $docComment['_id'];
					$main_comment_likes = Like::find()->where(['comment_id' => $pcid ,'status' => '1'])->count();
					$comment_array[$k]['main_comment_likes'] = $main_comment_likes;
					$loaded_sub_comments = Comment::find()->with('user')->with('post')->where(['parent_comment_id' => "$pcid",'status' => '1'])->orderBy(['created_date'=>SORT_DESC])->all();
					$l = 0;
					$subcomment_array = array();
					foreach($loaded_sub_comments as $docSubComment)
					{
						$subcomment_array[$l]['sub_comment'] = $docSubComment;
						$subcomment_array[$l]['actor'] = UserForm::getUserDetails($docSubComment['user_id']); 
						$sub_id = $docSubComment['_id'];
						$sub_comment_likes = Like::find()->where(['comment_id' => "$sub_id" ,'status' => '1'])->count();
						$subcomment_array[$l]['sub_comment_likes'] = $sub_comment_likes;
						$l++;
					}
					$comment_array[$k]['subcomment'] = $subcomment_array;
					$k++;
				}
				if(isset($doc['image']) && $doc['image'] != 'No Image')
				{
					$images = rtrim($doc['image'], ",");
					$images = explode(',',$images );
					$p = 0;
					for($m = 0; $m < count($images); $m++)
					{
						$pictures_array[$p]['urls'] = $images[$m];
						$p++;
					}
				}
				if($doc['post_type'] == 'link')
				{
					$p = 0;
					$links_array[$p]['urls'] = $doc['post_text'];
				}
				if(isset($doc['post_tags']) && $doc['post_tags'] != 'null')
				{
					$tags = explode(',', $doc['post_tags']);
					$p = 0;
					for($m = 0; $m < count($tags); $m++)
					{
						$tagedactornames_array[$p]['tags'] = UserForm::getUserDetails($tags[$m]);
						$p++;
					}
				}
				if(isset($doc['parent_post_id']))
				{
					$p = 0;
					$pid = $doc['parent_post_id'];
					$data[$i] = PostForm::find()->where(['_id'=>"$pid"])->one();
					$uid = $data[$i]['post_user_id'];
					$shares_array[$p]['parent_post'] = $data[$i];
					$shares_array[$p]['actor'] = UserForm::getUserDetails($uid);
				}
				if(!empty($doc['share_by']))
				{
					$shares_count = substr_count( $doc['share_by'], ",");
				}
				else
				{
					$shares_count = 0;
				}
				// Save post
				$savestatus = new SavePost();
                $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => "$post_id"])->one();
                $savestatuscalue = $savestatus['is_saved'];
				
				//Hide Post
				$hidestatus = new HidePost();
                $hidestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_ids' => "$post_id"])->one();
                if($hidestatus){
					$hidestatuss = "1";
				}else{
					$hidestatuss = "0";
				}
				$frd_id = (string) $doc['post_user_id'];
				// Userexits check
				$userexist = MuteConnect::find()->where(['user_id' => "$user_id"])->one();
				if ($userexist)
				{
					if (strstr($userexist['mute_ids'], "$frd_id"))
					{
						$getnot = 'Unmute notifications';
					}
					else
					{
						$getnot = 'Mute notifications';
					}
				}
				else
				{
					$getnot = 'Mute notifications';
				}
				if($like_array == null)
				{
					$like_array = array();
				}
				
				
				$feedback_array['likers'] = $like_array;
				$feedback_array['comments'] = $comment_array;
				
				$post_array[$i]['main_post'] = $doc; 
				$post_array[$i]['actor'] = UserForm::getUserDetails($doc['post_user_id']);
				$post_array[$i]['feedback'] = $feedback_array;
				$post_array[$i]['savestatus'] = $savestatuscalue;
				$post_array[$i]['hidestatus'] = $hidestatuss;
				$post_array[$i]['mutepostconnect'] = $getnot;
				
				$attachment_array['pictures'] = $pictures_array;
				$attachment_array['videos'] = $videos_array;
				$attachment_array['links'] = $links_array;
				$attachment_array['shares'] = $shares_array;
				$attachment_array['shares_count'] = $shares_count;
				
				$post_array[$i]['attachment'] = $attachment_array;
				$post_array[$i]['tagedactornames'] = $tagedactornames_array;
				$i++;
			}
        }
		else
		{
            $post_array = array();
        }
        $result =  PostForm::find()->with('user')->orderBy(['post_created_date' => SORT_DESC])->all();
        $usrfrd = Connect::getuserConnections($user_id);
		$usrfrdlist = array();
        foreach($usrfrd AS $ud) 
		{
            if(isset($ud['userdata']['fullname']) && $ud['userdata']['fullname'] != '') {
    			$id = (string)$ud['userdata']['_id'];
    			$fbid = isset($ud['userdata']['fb_id']) ? $ud['userdata']['fb_id'] : '';
    			$dp = $this->getimageAPI($ud['userdata']['_id'],'thumb');
    			$nm = $ud['userdata']['fullname'];
    			$usrfrdlist[] = array('id' => $id, 'fbid' => $fbid, 'name' => $nm, 'text' => $nm, 'thumb' => $dp);
            }
        }
		if( count($posts) > 0)
            $count =  count($posts);
        else
            $count = 0;
        // profile view section
        $cntrr = '';
		$view = array();
		$view_i = 0;
		for($x=3; $x>=0; $x--)
		{
			$month = date('M', strtotime(date('Y-m')." -" . $x . " month"));
			$year = date('Y', strtotime(date('Y-m')." -" . $x . " month"));
			$visitor = ProfileVisitor::find()->where(['user_id' => "$user_id",'year' => $year,'month' => $month])->all();
			if($visitor)
			{
				$cnt = count($visitor);
				$cntrr += $cnt; 
			}
			else
			{
				$cnt = 0;
			}
			$view[$view_i]['month'] = $month;
			$view[$view_i]['count'] = $cnt;
			$view_i++;
		}
		$rr = array();
		$rr['totalPage'] = $total_pages;
		$rr['pagenumber'] = $page_number;
		$rr['model'] = $model;
		$rr['posts'] = $post_array;
		$rr['usrfrdlist'] = $usrfrdlist;
		$rr['profile_view_graph'] = $view;
		return $rr;
	}
  
    /* testing method for upload photo */
    public function actionPhototest()
	{
        $imgcount = count($_FILES["photo"]["name"]);
        for ($i = 0; $i < $imgcount; $i++){
            $profile_picture = $_FILES['photo']['name'][$i];
            $photo = 'photo['.$i.']';
            $new = UploadedFile::getInstanceByName( $photo);
            $new->saveAs('../../frontend/web/profile/test/'.$profile_picture);
        }
        return $new;
    }
    
    /* Post new post by user */
    public function actionPostnewfeed() 
	{
        $model = new PostForm();
        
        $email = $_POST['email'];
        $userid = $user_id = $_POST['user_id'];
        $result = LoginForm::find()->where(['_id' => $userid])->one();
        $date = time();
        $_POST['test'] = $_POST['text'];
        if (empty($_POST['test'])) { $_POST['test'] = ''; }
        if (empty($_POST['title'])) { $_POST['title'] = ''; }
        $purifier = new HtmlPurifier();
        $text = HtmlPurifier::process($_POST['test']);

        $post = new PostForm();

        if(($_POST['pagename'] == 'wall') && ($userid != $_POST['tlid']))
        {
            $post->is_timeline = '1';
            $user_id = $_POST['tlid'];
            $post->shared_by = "$userid";
        }

        if (!empty($_POST['link_description']))
        {
            $title = $_POST['link_title'];
            $description = $_POST['link_description'];
            $image = $_POST['link_image'];
            $url = $_POST['link_url'];

            $post->post_type = 'link';
            $post->link_title = ucfirst(strtolower($title));
            $post->image = $image;
            $post->post_text = ucfirst(strtolower($url));
            $post->link_description = $description;
            $post->post_created_date = "$date";
            $post->post_user_id = "$user_id";
            $post->is_deleted = '0';
            $post->post_status = '1';
        }
        else 
        {
            $post->post_status = '1';
            $post->post_text = ucfirst(strtolower($text));
            $post->post_type = 'text';
            $post->post_created_date = "$date";
            $post->post_user_id = "$user_id";

            if(isset($_POST['counter'])){$_POST['counter']=$_POST['counter'];}else{$_POST['counter']=0;}

            if (isset($_FILES) && !empty($_FILES)) {

                $imgcount = count($_FILES["imageFile1"]["name"]);
                $img = '';
                $im = '';
                 for ($i = $_POST['counter']; $i < $imgcount; $i++)
                {
                    if (isset($_FILES["imageFile1"]["name"][$i]) && $_FILES["imageFile1"]["name"][$i] != "") {
                        $url = '../../frontend/web/uploads/';
                        $urls = '/uploads/';
                        
                        if ($text == '') { $post->post_type = 'image'; }
                        else { $post->post_type = 'text and image'; }
                        
                        $image_extn = end(explode('.',$_FILES["imageFile1"]["name"][$i]));
                        $rand = rand(111,999).'_'.time();
                        move_uploaded_file($_FILES["imageFile1"]["tmp_name"][$i], $url.$date.$rand.'.'.$image_extn);
                        
                        $img = $urls.$date.$rand.'.'.$image_extn.',';
                        $im = $im . $img;
                    }
                }
                $post->image = $im;
            }
        }

        if(isset($_POST['current_location']) && !empty($_POST['current_location']) && $_POST['current_location']!='undefined')
        {
            $post->currentlocation = $_POST['current_location'];
        }

        //custom_share', 'custom_notshare', 'anyone_tag'
        $post->custom_share = (isset($_POST['sharewith']) && !empty($_POST['sharewith'])) ? $_POST['sharewith'] : '';
        $post->custom_notshare = (isset($_POST['sharenot']) && !empty($_POST['sharenot'])) ? $_POST['sharenot'] : '';
        $post->anyone_tag = (isset($_POST['customchk']) && !empty($_POST['customchk'])) ? $_POST['customchk'] : '';
        $post->post_tags = (isset($_POST['posttags']) && !empty($_POST['posttags'])) ? $_POST['posttags'] : '';
    
        $post->post_title = ucfirst(strtolower($_POST['title']));
        $post->share_setting = $_POST['share_setting'];
        $post->comment_setting = $_POST['comment_setting'];
        $post->post_privacy = $_POST['post_privacy'];
        $post->is_deleted = '0';
		if($_POST['pagename'] == 'collection')
        {
            $col_id = $_POST['tlid'];
            $post->collection_id = "$col_id";
        }
        $post->insert();

        $last_insert_id =  $post->_id;
        
        if($_POST['post_privacy'] != 'Private')
        {
            // Insert record in notification table also
             $notification =  new Notification();
             $notification->post_id =   "$last_insert_id";
             $notification->user_id = "$user_id";
             $notification->notification_type = 'post';
             $notification->is_deleted = '0';
             $notification->status = '1';
             $notification->created_date = "$date";
             $notification->updated_date = "$date";
             $notification->insert();
        }
        
        if($_POST['posttags'] != 'null')
        {
            // Insert record in notification table also
            //$tag_count = count($_POST['posttags']);
            $tag_connections = explode(',',$_POST['posttags']);
            //print_r($tag_connections);
            $tag_count = count($tag_connections);
            for ($i = 0; $i < $tag_count; $i++)
            {
                $result_security = SecuritySetting::find()->where(['user_id' => "$tag_connections[$i]"])->one();
                if ($result_security)
                {
                    $tag_review_setting = $result_security['review_posts'];
                }
                else
                {
                    $tag_review_setting = 'Disabled';
                }
                $notification =  new Notification();
                $notification->post_id =   "$last_insert_id";
                $notification->user_id = $tag_connections[$i];
                $notification->notification_type = 'tag_connect';
                $notification->review_setting = $tag_review_setting;
                $notification->is_deleted = '0';
                $notification->status = '1';
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                $notification->insert();
            }
        }
       $response = array();
       $response['status'] = true;
       $response['statusMsg'] = "Post added successfully";
       $response['data'] = $last_insert_id;
       
       return $response;
    }
    
    /* Like/Dislike any post  */
    public function actionLikepost()
    { 
        
        $post_id = $_POST['post_id'];
        $uid = $_POST['user_id'];
        $like = new Like();
        $date = time();
        $data = array();
        $already_like = Like::find()->where(['post_id' => $post_id , 'user_id' => (string)$uid])->one();
               
        $status = $already_like['status'];
        if($status == '1'){$status = '0';}else{$status = '1';}
        
        if(!empty($already_like))
        {
            $date = time();
            $already_like->status = $status;
            $already_like->updated_date = $date;
            $already_like->update();
                   
            $data['status'] = $status;
            $data['msg'] = 'updated';
           
        }
        else
        {
        
            $like->post_id = $_POST['post_id'];
            $like->user_id = (string)$uid;
            $like->like_type = 'post';
            $like->status = '1';
            $like->created_date = $date;
            $like->updated_date = $date;
            // $like->ip = Yii::$app->request->userHostAddress();
            $like->insert();
            $data['status'] = '1';
            $data['msg'] = 'inserted';
            $last_insert_id = $like->_id;
        }
        if(isset($last_insert_id))
        {
            if($status == '1')
            {
                // Insert record in notification table also
                $notification =  new Notification();
                if($last_insert_id != '')
                    $notification->like_id =   "$last_insert_id";
                else
                    $notification->like_id = "$already_like->_id";
                $notification->user_id = "$uid";
                $notification->post_id = $_POST['post_id'];
                $notification->notification_type = 'likepost';
                $notification->is_deleted = '0';
                $notification->status = '1'; 
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                $post_details = PostForm::find()->where(['_id' => $_POST['post_id']])->one();
                $notification->post_owner_id = $post_details['post_user_id'];
                if($post_details['post_user_id'] != "$uid" && $post_details['post_privacy'] != "Private")
                {
                    $notification->insert();
                }
            }
        }
        
        
        $likes = Like::find()->where(['post_id' => $post_id ,'status' => '1'])->all();
        $like_names = Like::getLikeUserNames((string)$post_id);
        $data['names'] =  $like_names['names'];
        $data['like_count'] = count($likes);
        return $data;
    }
    
    /* Parent/child comment on post */
    public function actionCommentpost()
    { 
        $post_id = $_POST['post_id'];
        $uid = $userid= $_POST['user_id'];
        $comment = new Comment();
        $date = time();
        $data = array();
        $post = PostForm::find()->where(['_id' => $post_id])->one();
		$result = LoginForm::find()->where(['_id' => $uid])->one();
        $comment_exists = Comment::find()->where(['user_id' => "$uid",'comment'=>$_POST['comment']])->andWhere(['parent_comment_id'=> "0"])->all();
        if($_POST['comment_type'] == 'main_comment')
		{
            if(empty($comment_exists) || count($comment_exists<=0)) 
            {
                $comment->post_id = $_POST['post_id'];
                $comment->user_id = (string)$uid;
                $post_comment = str_replace(array("\n","\r\n","\r"), '', $_POST['comment']);
                $comment->comment = $post_comment;
                $comment->comment_type = 'post';
                $comment->status = '1';
                $comment->parent_comment_id = '0';
                $comment->created_date = $date;
                $comment->updated_date = $date;
				$comment->insert();
                $last_insert_id = $comment->_id;
				// Insert record in notification table also
                $notification =  new Notification();
                $notification->comment_id =   "$last_insert_id";
                $notification->user_id = "$userid";
                $notification->post_id = $_POST['post_id'];
                $notification->notification_type = 'comment';
                $notification->comment_content = $post_comment;
                $notification->is_deleted = '0';
                $notification->status = '1';
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                $post_details = PostForm::find()->where(['_id' => $_POST['post_id']])->one();
                $notification->post_owner_id = $post_details['post_user_id'];
                if($post_details['post_user_id'] != "$userid" && $post_details['post_privacy'] != "Private")
                {
                    $notification->insert();
                }
				$last_comment_data =  Comment::find()->with('user')->with('post')->where(['_id' => "$last_insert_id",'status' => '1'])->one();
				$data['status'] = '1';
				$data['comment_id'] = "$last_insert_id";
				$data['msg'] = 'inserted';
            }
        }
        else if($_POST['comment_type'] == 'reply')
		{
			$comment->post_id = $_POST['post_id'];
            $comment->user_id = (string)$uid;

            $post_reply = str_replace(array("\n","\r\n","\r"), '', $_POST['comment']);
            $comment->comment = $post_reply;
            $comment->comment_type = 'post';
            $comment->status = '1';
            $comment->parent_comment_id = $_POST['comment_id'];
            $comment->created_date = $date;
            $comment->updated_date = $date;
			$comment->insert();
            $last_insert_id = $comment->_id;

            // Insert record in notification table also
            $notification =  new Notification();
            $notification->comment_id = $_POST['comment_id'];
            $notification->reply_comment_id = "$last_insert_id";
            $notification->user_id = "$userid";
            $notification->post_id = $_POST['post_id'];
            $notification->notification_type = 'commentreply';
            $notification->is_deleted = '0';
            $notification->status = '1';
            $notification->created_date = "$date";
            $notification->updated_date = "$date";
            $post_details = PostForm::find()->where(['_id' => $_POST['post_id']])->one();
            $comment_details = Comment::find()->where(['_id' => $_POST['comment_id']])->one();
            $notification->post_owner_id = $comment_details['user_id'];
            if($comment_details['user_id'] != "$userid" && $post_details['post_privacy'] != "Private")
            {
                $notification->insert();
            }
            $data['status'] = '1';
            $data['msg'] = 'inserted';
        }
		else
		{
            $data['status'] = '0';
            $data['msg'] = 'specify comment type';
        }
        return $data;
    }
    
    /* Get User's Detail with about detail & list of all wall display lists */
    public function actionUseraboutprofile()
	{
        $user_id = $_POST['user_id'];
        $result = array();
		// User main data
        $record_user = LoginForm::find()->where(['_id' => $user_id])->one(); 
		// User Credit data
		$user_credit = Credits::usertotalcredits(); 
		// User Personal data
		$record_personal = Personalinfo::find()->where(['user_id' => $user_id])->one();
		// User Settings data for profile		
        $record_setting = UserSetting::find()->where(['user_id' => $user_id])->one(); 
		// user security setting data
		$security_setting = SecuritySetting::find()->where(['user_id' => $user_id])->one();
        // User Connections List
        $usrfrd = Connect::getuserConnections($user_id);
        $usrfrdlist = array();
        foreach($usrfrd AS $ud)
		{
            if(!(isset($ud['userdata']['fullname']) && $ud['userdata']['fullname'] != '')) {
                continue;
            }

            $id = (string)$ud['userdata']['_id'];
            $fbid = isset($ud['userdata']['fb_id']) ? $ud['userdata']['fb_id'] : '';
            $dp = $this->getimageAPI($ud['userdata']['_id'],'thumb');  
            $mutual = Connect::mutualconnectcountAPI($id,$user_id);
			// Unfollow connect check
			$unfollowuser = UnfollowConnect::find()->where(['user_id' => "$user_id"])->andwhere(['like','unfollow_ids',$id])->one();
			if ($unfollowuser)
			{
				$folstatus = 'Unmute connect posts';
			}
			else
			{
				$folstatus = 'Mute connect posts';
			}
			// Userexits check
			$userexist = MuteConnect::find()->where(['user_id' => "$user_id"])->one();
			if ($userexist)
			{
				if (strstr($userexist['mute_ids'], "$id"))
				{
					$getnot = 'Unmute notifications';
				}
				else
				{
					$getnot = 'Mute notifications';
				}
			}
			else
			{
				$getnot = 'Mute notifications';
			}
			//Use Block Check
			$userblock = BlockConnect::find()->where(['user_id' => $user_id])->one();
			if ($userblock)
			{
				if (strstr($userblock['block_ids'], "$id"))
				{
					$getblock = 'Unblock';
				}
				else
				{
					$getblock = 'Block';
				}
			}
			else
			{
				$getblock = 'Block';
			}
			//User connect Array
            $usrfrdlist[] = array('id' => $id,
								'fbid' => $fbid,
								'fname' => $ud['userdata']['fname'],
								'lname' => $ud['userdata']['lname'],
								'gender' => $ud['userdata']['gender'],
								'thumb' => $dp,
								'folstatus' => $folstatus,
								'getnot' => $getnot,
								'getblock' => $getblock,
								'lat' => $ud['userdata']['lat'],
								'longitude' => $ud['userdata']['long'],
								'city' => $ud['userdata']['city'],
								'DOB' => $ud['userdata']['birth_date'],
								'created_date' => $ud['created_date'],'mutual_frnd_count' => $mutual);
        }
		// List of Profile Pic
        $profile_pic = PostForm::getProfilePics($user_id);
        // List of Cover Pic
        $cover_pic = PostForm::getCoverPics($user_id);
        // List of All Pic
		$login_user_id = $_POST['login_user_id'];
        $album_pic = PostForm::getAlbumsAPI($user_id,$login_user_id);
        // List of Occupations
        $record_occupation = Occupation::find()->orderBy(['name'=>SORT_ASC])->all(); 
        // List of Education
        $record_education = Education::find()->orderBy(['name'=>SORT_ASC])->all(); 
        // List of Languages
        $record_speaks = Language::find()->orderBy(['name'=>SORT_ASC])->all();
        // List of Interest
        $record_likes = Interests::find()->orderBy(['name'=>SORT_ASC])->all(); 
        
		// List of User Feed End 
        $result['user_data'] = $record_user;
        $result['user_about'] = $record_personal;
        $result['user_settingdata'] = $record_setting; 
        $result['user_connections'] = $usrfrdlist;
        $result['profession_list'] = $record_occupation;
        $result['studied_list'] = $record_education;
        $result['speaks_list'] = $record_speaks;
        $result['likes_list'] = $record_likes;
        $result['profile_pic_list'] = $profile_pic;
        $result['cover_pic'] = $cover_pic;
        $result['all_album_pic'] = $album_pic;
        $result['user_credit'] = $user_credit;
		$result['security_setting'] = $security_setting;
		
		return $result;
    }
   
    /* share post with different criteria */
    public function actionSharepost() 
	{
        $lmodel = new LoginForm();
        $pmodel = new PostForm();
        $fmodel = new Connect();
        $statusdata = array();
        $user_id = $_POST['user_id'];
        $result_security = SecuritySetting::find()->where(['user_id' => $user_id])->one();
        if ($result_security) {
            $post_privacy = $result_security['my_post_view_status'];
        } else {
            $post_privacy = 'Public';
        }

        if (isset($_POST['spid']) && !empty($_POST['spid'])) {
            $data = array();
            $email = $_POST['email'];
            $user_id = $_POST['user_id'];

            $getpostinfo = PostForm::find()->where(['_id' => $_POST['spid'],])->one();
            if(isset($_POST['current_location']) && !empty($_POST['current_location']))
            {
                $currentlocation = $_POST['current_location'];
            }
            else
            {
                $currentlocation = '';
            }
            if ($getpostinfo) {
                $date = time();
                $sharepost = new PostForm();
               
                if (!empty($getpostinfo['post_type'])) {
                    $sharepost->post_type = $getpostinfo['post_type'];
                }
                if (!empty($_POST['desc'])) {
                    $sharepost->post_text = ucfirst(strtolower($_POST['desc']));
                } else {
                    $sharepost->post_text = '';
                }
                $sharepost->post_status = '1';
                $sharepost->post_created_date = "$date";
                $sharepost->post_tags = (isset($_POST['posttags']) && !empty($_POST['posttags'])) ? $_POST['posttags'] : '';
                if ($_POST['sharewall'] == 'own_wall' || empty($_POST['frndid'])) {
                    $puser = $user_id;
                } else {
                    $puser = $_POST['frndid'];
                }
                if(isset($_POST['post_privacy']) && !empty($_POST['post_privacy']))
                {
                    $postprivacy = $_POST['post_privacy'];
                }
                else
                {
                    $postprivacy = 'Public';
                }
                $sharepost->post_user_id = $puser;

                $sharepost->shared_from = $getpostinfo['post_user_id'];
                $sharepost->currentlocation = $currentlocation;
                $sharepost->post_privacy = $postprivacy;
                if (!empty($getpostinfo['image'])) {
                    $sharepost->image = $getpostinfo['image'];
                }
                if (!empty($getpostinfo['link_title'])) {
                    $sharepost->link_title = ucfirst(strtolower($getpostinfo['link_title']));
                }
                if (!empty($getpostinfo['link_description'])) {
                    $sharepost->link_description = ucfirst(strtolower($getpostinfo['link_description']));
                }
                if (!empty($getpostinfo['album_title'])) {
                    $sharepost->album_title = $getpostinfo['album_title'];
                }
                if (!empty($getpostinfo['album_place'])) {
                    $sharepost->album_place = $getpostinfo['album_place'];
                }
                if (!empty($getpostinfo['album_img_date'])) {
                    $sharepost->album_img_date = $getpostinfo['album_img_date'];
                }
                if (!empty($getpostinfo['is_album'])) {
                    $sharepost->is_album = $getpostinfo['is_album'];
                }
                $posttags = $_POST['posttags'];
                if($posttags != 'null')
                {
                    $gsu_id = $getpostinfo['post_user_id'];
                    $sec_result_set = SecuritySetting::find()->where(['user_id' => "$gsu_id"])->one();
                    if ($sec_result_set)
                    {
                        $tag_review_setting = $sec_result_set['review_tags'];
                    }
                    else
                    {
                        $tag_review_setting = 'Disabled';
                    }
                    if($tag_review_setting == "Enabled")
                    {
                        $review_tags = "1";
                    }
                    else
                    {
                        $review_tags = "0";
                    }
                }
                else
                {
                    $review_tags = "0";
                }
                $sharepost->parent_post_id = $_POST['spid'];
                $sharepost->is_timeline = '1';
                $sharepost->is_deleted = $review_tags;
                $sharepost->shared_by = $user_id;
                $sharepost->share_setting = $_POST['share_setting'];
                $sharepost->comment_setting = $_POST['comment_setting'];
                $sharepost->insert();
                $last_insert_id = $sharepost->_id;
                $result_security = SecuritySetting::find()->where(['user_id' => "$puser"])->one();
                if ($result_security)
                {
                    $tag_review_setting = $result_security['review_posts'];
                }
                else
                {
                    $tag_review_setting = 'Disabled';
                }
                // Insert record in notification table also
                $notification =  new Notification();
                $notification->share_id =   "$last_insert_id";
                $notification->post_id = $_POST['spid'];
                $notification->user_id = $puser;
                $notification->notification_type = 'sharepost';
                $notification->review_setting = $tag_review_setting;
                $notification->is_deleted = '0';
                $notification->status = '1';
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                $post_details = PostForm::find()->where(['_id' => $_POST['spid']])->one();
                $notification->post_owner_id = $post_details['post_user_id'];
                $notification->shared_by = $user_id;
                if($post_details['post_user_id'] != $user_id && $post_details['post_privacy'] != "Private")
                {
                    $notification->insert();
                    $_POST['posttags'] = '';
                    $tag_connections = explode(',',$_POST['posttags']);
                    $tag_count = count($tag_connections);
                    if($posttags != 'null')
                    {
                        for ($i = 0; $i < $tag_count; $i++)
                        {
                            $result_security = SecuritySetting::find()->where(['user_id' => "$tag_connections[$i]"])->one();
                            if ($result_security)
                            {
                                $tag_review_setting = $result_security['review_posts'];
                            }
                            else
                            {
                                $tag_review_setting = 'Disabled';
                            }
                            $notification =  new Notification();
                            $notification->post_id =   "$last_insert_id";
                            $notification->user_id = $tag_connections[$i];
                            $notification->notification_type = 'tag_connect';
                            $notification->review_setting = $tag_review_setting;
                            $notification->is_deleted = $review_tags;
                            $notification->status = '1';
                            $notification->created_date = "$date";
                            $notification->updated_date = "$date";
                            $notification->insert();
                        }
                    }
                }
                
                
               
                if ($last_insert_id) {
                    $sharepost = PostForm::find()->where(['_id' => $_POST['spid'],])->one();
                   
                    $sharepost->share_by = $getpostinfo['share_by'] . $user_id . ',';
                    if ($sharepost->update()) {
                           $sharepost = PostForm::find()->where(['_id' => $_POST['spid'],])->one();
                           $statusdata['statusMsg'] = true;
                           $statusdata['data'] = $sharepost;
                    } else {
                        $statusdata['statusMsg'] = false;
                    }
                } else {
                    $statusdata['statusMsg'] = false;
                }
            } else {
                $statusdata['statusMsg'] = false;
            }
        }
        return $statusdata;
    }
   
    
    /* Like/Dislike post comment */
    public function actionLikecomment()
    { 
        $comment_id = $_POST['comment_id'];
        $uid = $_POST['user_id'];
        $like = new Like();
        $date = time();  
        $data = array();
        $already_like = Like::find()->where(['comment_id' => $comment_id , 'user_id' => (string)$uid])->one();
               
        $status = $already_like['status'];
        if($status == '1'){ $status = '0'; }else{ $status = '1'; }
        
        if(!empty($already_like))
        {
            $date = time();
            $already_like->status = $status;
            $already_like->updated_date = $date;
            $already_like->update();
                   
            $data['status'] = $status;
            $data['msg'] = 'updated';
           
        }
        else
        {
        
            $like->comment_id = $_POST['comment_id'];
            $like->user_id = (string)$uid;
            $like->like_type = 'comment';
            $like->status = '1';
            $like->created_date = $date;
            $like->updated_date = $date;
            $like->insert();
            $last_insert_id = $like->_id;
            $data['status'] = '1';
            $data['msg'] = 'inserted';
        
        }
        if(isset($last_insert_id))
        { 
            if($status == '1') 
            {
                // Insert record in notification table also
                $notification =  new Notification(); 
                if($last_insert_id != '')
                    $notification->like_id =   "$last_insert_id";
                else
                    $notification->like_id = "$already_like->_id";
                $comment_details = Comment::find()->where(['_id' => $_POST['comment_id']])->one();
                $notification->user_id = "$uid";
                $notification->post_id = $comment_details['post_id'];
                $notification->notification_type = 'likecomment';
                $notification->is_deleted = '0';
                $notification->status = '1';
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                $notification->post_owner_id = $comment_details['user_id'];
                if($comment_details['user_id'] != "$uid")
                {
                    $notification->insert();
                }
            }
        }
        
        $likes = Like::find()->where(['comment_id' => $comment_id ,'status' => '1'])->all();

        $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => "$comment_id", 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
        if(!empty($user_ids)) {
            $array = "'".implode("','",$user_ids)."'";
            $array = explode(",", $array);
        }

        $comlikeuseinfo = UserForm::find()
        ->select(['_id','fname', 'lname'])
        ->asArray()
        ->where(['in','_id', $user_ids])
        ->all();

        $usrbox = array();
        foreach ($comlikeuseinfo as $key => $single) {
            $fullnm = $single['fname'] . ' ' . $single['lname'];
            $usrbox[(string)$single['_id']] = $fullnm; 
        }

        $data['like_buddies'] = implode("<br/>", $usrbox);
        $data['like_count'] = count($likes);
        $data['status'] = true;
        return $data;
    }

    /* Insert/Update About information of user */
    public function actionAboutuserpost(){
        $user_id = $_POST['user_id'];
        $record_user = LoginForm::find()->where(['_id' => $user_id])->one();
        $record_personal = Personalinfo::find()->where(['user_id' => $user_id])->one();
        $record_setting = UserSetting::find()->where(['user_id' => $user_id])->one();

        $data = array();
        if (!empty($record_personal)) {

                $record_user->city = $_POST['livesin'];
                $record_user->country = $_POST['country'];
                $record_user->country_code = $_POST['country_code'];
				$record_user->gender = $_POST['gender'];
				$record_user->birth_date = $_POST['birth_date'];
				$record_user->update();
                $record_personal->about = $_POST['about'];
				$record_occupation = ArrayHelper::map(Occupation::find()->all(), 'name', 'name');
				$occ = explode(',', $_POST['occupation']);
				$occupation_differances=array_diff($occ,$record_occupation);
				if(isset($occupation_differances) && !empty($occupation_differances))
				{
					$insert_occupation = new Occupation();
					foreach($occupation_differances as $occupation_differance)                  
					{
						$insert_occupation->name = ucfirst($occupation_differance);
						$insert_occupation->insert();
					}                  
				}
                $record_personal->occupation = ucfirst($_POST['occupation']);
				$record_interests = ArrayHelper::map(Interests::find()->all(), 'name', 'name');
				$int = explode(',', $_POST['interests']);
				$interests_differances=array_diff($int,$record_interests);
				if(isset($interests_differances) && !empty($interests_differances))
				{
					$insert_interests = new Interests();
					foreach($interests_differances as $interests_differance)                  
					{
						$insert_interests->name = ucwords($interests_differance);
						$insert_interests->insert();
					}                  
				}
                $record_personal->interests = ucfirst($_POST['interests']);
				$record_language = ArrayHelper::map(Language::find()->all(), 'name', 'name');
				$lan = explode(",",$_POST['language']);
				$lan_diffs= array_diff($lan,$record_language);
				if(isset($lan_diffs) && !empty($lan_diffs))
				{
					$insert_language = new Language();
					foreach($lan_diffs as $lan_diff)                  
					{
						$insert_language->name = ltrim(ucfirst($lan_diff));
						$insert_language->insert();
					}                  
				}
                $record_personal->language = ucfirst($_POST['language']);
				$record_education = ArrayHelper::map(Education::find()->all(), 'name', 'name');
				$ed = explode(',', $_POST['education']);
				$edu_differances = array_diff($ed,$record_education);
				if(isset($edu_differances) && !empty($edu_differances))
				{
					$insert_education = new Education();
					foreach($edu_differances AS $edu_differance)                  
					{
						$insert_education->name = ltrim(ucwords($edu_differance));
						$insert_education->insert();
					}                  
				}
                $record_personal->education = ucfirst($_POST['education']);
				$record_personal->visited_countries = ucfirst($_POST['visited_countries']);
				$record_personal->lived_countries = ucfirst($_POST['lived_countries']);
				$record_personal->amazing_things = ucfirst($_POST['amazing_things']);
                $record_personal->update();
        }
        else
        {

            $record_personal = new Personalinfo();
            $record_personal->user_id = $user_id;
            $record_personal->about = $_POST['about'];
			$record_occupation = ArrayHelper::map(Occupation::find()->all(), 'name', 'name');
			$occ = explode(',', $_POST['occupation']);
			$occupation_differances=array_diff($occ,$record_occupation);
			if(isset($occupation_differances) && !empty($occupation_differances))
			{
				$insert_occupation = new Occupation();
				foreach($occupation_differances as $occupation_differance)                  
				{
					$insert_occupation->name = ucfirst($occupation_differance);
					$insert_occupation->insert();
				}                  
			}
            $record_personal->occupation = ucfirst($_POST['occupation']);
			$record_interests = ArrayHelper::map(Interests::find()->all(), 'name', 'name');
			$int = explode(',', $_POST['interests']);
			$interests_differances=array_diff($int,$record_interests);
			if(isset($interests_differances) && !empty($interests_differances))
			{
				$insert_interests = new Interests();
				foreach($interests_differances as $interests_differance)                  
				{
					$insert_interests->name = ucwords($interests_differance);
					$insert_interests->insert();
				}                  
			}
            $record_personal->interests = ucfirst($_POST['interests']);
			$record_language = ArrayHelper::map(Language::find()->all(), 'name', 'name');
			$lan = explode(",",$_POST['language']);
			$lan_diffs= array_diff($lan,$record_language);
			if(isset($lan_diffs) && !empty($lan_diffs))
			{
				$insert_language = new Language();
				foreach($lan_diffs as $lan_diff)                  
				{
					$insert_language->name = ltrim(ucfirst($lan_diff));
					$insert_language->insert();
				}                  
			}
            $record_personal->language = ucfirst($_POST['language']);
			$record_education = ArrayHelper::map(Education::find()->all(), 'name', 'name');
			$ed = explode(',', $_POST['education']);
			$edu_differances = array_diff($ed,$record_education);
			if(isset($edu_differances) && !empty($edu_differances))
			{
				$insert_education = new Education();
				foreach($edu_differances AS $edu_differance)                  
				{
					$insert_education->name = ltrim(ucwords($edu_differance));
					$insert_education->insert();
				}                  
			}
            $record_personal->education = ucfirst($_POST['education']);					
            $record_personal->insert();
        }

        $data['status'] = true;
        $data['about'] = $record_personal['about'];
        $data['occupation'] = $record_personal['occupation'];
        $data['interests'] = $record_personal['interests'];
        $data['language'] = $record_personal['language'];
        $data['education'] = $record_personal['education'];
        $data['city'] = $record_user['city'];
        $data['country'] = $record_user['country'];
        $data['country_code'] = $record_user['country_code'];
        return $data;
            
    }
    
    /* Send Connect Request */
    public function actionSendconnectrequest()
    {
        $connect = new Connect();
        $date = time();
        $requestcheckone = Connect::find()->where(['from_id' => $_POST['from_id'] , 'to_id' => $_POST['to_id']])->one();
        $requestchecktwo = Connect::find()->where(['to_id' => $_POST['from_id'] , 'from_id' => $_POST['to_id']])->one();
        $data = array();
        if(!$requestcheckone && !$requestchecktwo)
        {
            $connect->from_id = $_POST['from_id'];
            $connect->to_id = $_POST['to_id'];
            $connect->action_user_id = $_POST['from_id'];
            $connect->status = '0';
            $connect->created_date = $date;
            $connect->updated_date = $date;
            $connect->insert();
            if($connect->_id != '')
            {
                $connect1 = new Connect();
                $date = time();
                $connect1->from_id = $_POST['to_id'];
                $connect1->to_id = $_POST['from_id'];
                $connect1->action_user_id = $_POST['from_id'];
                $connect1->status = '0';
                $connect1->created_date = $date;
                $connect1->updated_date = $date;
                $connect1->insert();
                $data['status'] = true;
                $data['statusMsg'] =  'Connect request sent.';
                
            }
        }
        else
        {
            
            $uid = $_POST['user_id'];
            if($requestcheckone && ($uid == $requestcheckone['action_user_id']))
            {
                $data['status'] = false;
                $data['statusMsg'] = 'Request already sent.';
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = 'Accept Connect Request.';
            }
        }
        return $data;
    }
	
	public function actionCancelconnectrequest()
    {
        $connect = new Connect();
        $date = time();
        $requestcheckone = Connect::find()->where(['from_id' => $_POST['from_id'] , 'to_id' => $_POST['to_id'],'status' => '0' , 'action_user_id' => $_POST['from_id']])->one();
        $requestchecktwo = Connect::find()->where(['to_id' => $_POST['from_id'] , 'from_id' => $_POST['to_id'],'status' => '0' , 'action_user_id' => $_POST['from_id']])->one();
        $data = array();
		
        if($requestcheckone && $requestchecktwo)
        {
            $requestcheckone->delete();
            $requestchecktwo->delete();
			$data['status'] = true;
            $data['statusMsg'] =  'Connect request cancel.';
        }
        else
        {
            $data['status'] = false;
            $data['statusMsg'] = 'Request not found.';
        }
        return $data;
    }
    
    /* get list of security setting of user */
    public function actionGetsecuritysettings()
	{
        $user_id = $_POST['user_id'];
        $data = array();
        $record_security = SecuritySetting::find()->where(['user_id' => $user_id])->one();
        
        $usrfrd = Connect::getuserConnections($user_id);
        $usrfrdlist = array();

        foreach($usrfrd AS $ud) {
            if(isset($ud['userdata']['fullname']) && $ud['userdata']['fullname'] != '') {
                $id = (string)$ud['userdata']['_id'];
                $fbid = isset($ud['userdata']['fb_id']) ? $ud['userdata']['fb_id'] : '';
                $dp = $this->getimageAPI($ud['userdata']['_id'],'thumb');
               $nm = $ud['userdata']['fullname'];
                $usrfrdlist[] = array('id' => $id, 'fbid' => $fbid, 'name' => $nm, 'text' => $nm, 'thumb' => $dp);
            }
        }
        
        $data['security_setting'] = $record_security;
        $data['connect_list'] = $usrfrdlist;
        return $data;
    }
    
    /* get list of security setting of user */
    public function actionUpdatesecuritysettings()
	{
        $user_id = $_POST['user_id'];
        
        $data = array();
        $model = new SecuritySetting();
        $security_model = SecuritySetting::find()->where(['user_id' => $user_id])->one();
        
        if(isset($_POST['securityquestion']) && isset($_POST['securityanswer'])){
            if(!empty($security_model)){
                $security_model->security_questions = $_POST['securityquestion'];
            }else{
                $model->security_questions = $_POST['securityquestion'];
            }
            
            if($_POST['securityquestion'] == 'eml_ans'){
                if(!empty($security_model)){
                    $security_model->eml_ans = $_POST['securityanswer'];
                }else{
                    $model->eml_ans = $_POST['securityanswer'];
                }
            }elseif($_POST['securityquestion'] == 'born_ans'){
                if(!empty($security_model)){
                    $security_model->born_ans = $_POST['securityanswer'];
                }else{
                    $model->born_ans = $_POST['securityanswer'];
                }
            }elseif($_POST['securityquestion'] == 'gf_ans'){
                if(!empty($security_model)){
                    $security_model->gf_ans = $_POST['securityanswer'];
                }else{
                    $model->gf_ans = $_POST['securityanswer'];
                }
            }
        }
        if(isset($_POST['lookupprivacy'])){
            if(!empty($security_model)){
                $security_model->my_view_status = $_POST['lookupprivacy'];
            }else{
                $model->my_view_status = $_POST['lookupprivacy'];
            }
        }
        if(isset($_POST['postsecurity'])){
            if(!empty($security_model)){
                $security_model->my_post_view_status = $_POST['postsecurity'];
            }else{
                $model->my_post_view_status = $_POST['postsecurity'];
            }
        }
        if(isset($_POST['photosecurity'])){
            if(!empty($security_model)){
                $security_model->view_photos = $_POST['photosecurity'];
            }else{
                $model->view_photos = $_POST['photosecurity'];
            }
        }
        if(isset($_POST['contactsecurity'])){
            if(!empty($security_model)){
                $security_model->contact_me = $_POST['contactsecurity'];
            }else{
                $model->contact_me = $_POST['contactsecurity'];
            }
        }
        if(isset($_POST['connectrequestsecurity'])){
            if(!empty($security_model)){
                $security_model->connect_request = $_POST['connectrequestsecurity'];
            }else{
                $model->connect_request = $_POST['connectrequestsecurity'];
            }
        }
        if(isset($_POST['postingpermission'])){
            if(!empty($security_model)){
                $security_model->add_public_wall = $_POST['postingpermission'];
            }else{
                $model->add_public_wall = $_POST['postingpermission'];
            }
        }
        if(isset($_POST['postreview'])){
            if(!empty($security_model)){
                $security_model->review_posts = $_POST['postreview'];
            }else{
                $model->review_posts = $_POST['postreview'];
            }
        }
        if(isset($_POST['taggedpostview'])){
            if(!empty($security_model)){
                $security_model->view_posts_tagged_in = $_POST['taggedpostview'];
            }else{
                $model->view_posts_tagged_in = $_POST['taggedpostview'];
            }
        }
        if(isset($_POST['otherspostview'])){
            if(!empty($security_model)){
                $security_model->view_others_posts_on_mywall = $_POST['otherspostview'];
            }else{
                $model->view_others_posts_on_mywall = $_POST['otherspostview'];
            }
        }
        if(isset($_POST['tagreviews'])){
            if(!empty($security_model)){
                $security_model->review_tags = $_POST['tagreviews'];
            }else{
                $model->review_tags = $_POST['tagreviews'];
            }
        }
        if(isset($_POST['activitypermission'])){
            if(!empty($security_model)){
                $security_model->recent_activities = $_POST['activitypermission'];
            }else{
                $model->recent_activities = $_POST['activitypermission'];
            }
        }
        if(isset($_POST['connectionslist'])){
            if(!empty($security_model)){
                $security_model->connect_list = $_POST['connectionslist'];
            }else{
                $model->connect_list = $_POST['connectionslist'];
            }
        }
        
        if(isset($_POST['restricted_list'])){
            if(!empty($security_model)){
                $security_model->restricted_list = $_POST['restricted_list'];
            }else{
                $model->restricted_list = $_POST['restricted_list'];
            }
        }
        if(isset($_POST['blocked_list'])){
            if(!empty($security_model)){
                $security_model->blocked_list = $_POST['blocked_list'];
            }else{
                $model->blocked_list = $_POST['blocked_list'];
            }
        }
        if(isset($_POST['message_filtering'])){
            if(!empty($security_model)){
                $security_model->message_filtering = $_POST['message_filtering'];
            }else{
                $model->message_filtering = $_POST['message_filtering'];
            }
        }
        
        if(!empty($security_model)){
            $security_model->update();
        }else{
            $model->user_id = $_POST['user_id'];
            $model->insert();
        }
                
        $usrfrd = Connect::getuserConnections($user_id);
        $usrfrdlist = array();

        foreach($usrfrd AS $ud) {
            if(isset($ud['userdata']['fullname']) && $ud['userdata']['fullname'] != '') {
                $id = (string)$ud['userdata']['_id'];
                $fbid = isset($ud['userdata']['fb_id']) ? $ud['userdata']['fb_id'] : '';
                $dp = $this->getimageAPI($ud['userdata']['_id'],'thumb');
                $nm = $ud['userdata']['fullname'];
                $usrfrdlist[] = array('id' => $id, 'fbid' => $fbid, 'name' => $nm, 'text' => $nm, 'thumb' => $dp);
            }
        }
        
        $record_security_new = SecuritySetting::find()->where(['user_id' => $user_id])->one();
        
        $data['security_setting'] = $record_security_new;
        $data['connect_list'] = $usrfrdlist;
        return $data;
    }
    
    /* Get User's all Detail with personal & security by user Id */
    
    public function actionGetbasicinformation()
	{
       // $this->_checkAuth();  /* Only this line will do authentication by header parameters email & password */
       $country = new UserForm();
       
       $user_id = $_GET['user_id'];
       
       $data = array();
       
       $record = UserForm::find()->where(['_id' => $user_id])->one();
       $record_security = SecuritySetting::find()->where(['user_id' => $user_id])->one();
       $record_personal = Personalinfo::find()->where(['user_id' => $user_id])->one();
       $record_privacy = UserSetting::find()->where(['user_id' => $user_id])->one();
       
       $record_occupation = Occupation::find()->orderBy(['name'=>SORT_ASC])->all(); /* List of Occupations */
        
        $record_education = Education::find()->orderBy(['name'=>SORT_ASC])->all(); /* List of Education */
        
        $record_speaks = Language::find()->orderBy(['name'=>SORT_ASC])->all(); /* List of Languages */
        
        $record_likes = Interests::find()->orderBy(['name'=>SORT_ASC])->all(); /* List of Interest */
       
       $data['basic'] = $record;
       $data['security'] = $record_security;
       $data['personal'] = $record_personal;
       $data['privacy'] = $record_privacy;
       $data['occupation'] = $record_occupation;
       $data['education'] = $record_education;
       $data['speaks'] = $record_speaks;
       $data['likes'] = $record_likes;
       
       return $data;
    }
    
    /* Profile Photo upload */
    public function actionUpdateuserprofilephoto()
	{
        $datas = array();
        if(isset($_POST['photo_type']) && $_POST['photo_type'] == "profile")
		{
            $model = new LoginForm();
            $model->scenario = 'profile_picture';
			$email = $_POST['email'];
            $s3 = LoginForm::find()->where(['email' => $email])->one();
            if(!empty($_FILES['photo']))
			{
                $profile_picture = $_FILES['photo']['name'];
				$chars = '0123456789';
                $count = mb_strlen($chars);
				for ($i = 0, $rand = ''; $i < 8; $i++) 
				{
					$index = rand(0, $count - 1);
					$rand .= mb_substr($chars, $index, 1);
                }
				$profile_picture1 = 'photo';
				$models = UploadedFile::getInstanceByName($profile_picture1);
				$models->saveAs('../../frontend/web/profile/'.$profile_picture); 
            }else if(!empty($_POST['fb_photo'])){
                $profile_picture = $_POST['fb_photo'];
            }else if($_POST['remove'] == 'yes'){
                $profile_picture = $s3->gender.".jpg";
            }
            $s3->thumbnail = $profile_picture;
            $s3->photo = $profile_picture;
            $s3->update();
			
			$date = time();
            $post = new PostForm();
            $post->post_status = '1';
            $post->post_type = 'profilepic';
            $post->is_profilepic = '1';
            $post->is_deleted = '0';
            $post->post_privacy = 'Public';
            $post->image = $profile_picture;
            $post->post_created_date = "$date";
            $post->post_user_id = (string)$s3['_id'];
            $post->insert();

            $datas['status'] = true;
            $datas['statusMsg'] = "Profile photo updated successfully";
            $datas['photo'] = $profile_picture;
        }
		else if(isset($_POST['photo_type']) && $_POST['photo_type'] == "cover")
		{
            $model = new LoginForm();
            $post_model = new PostForm();
            $email = $_POST['email'];
            $s3 = LoginForm::find()->where(['email' => $email])->one();
            if(!empty($_FILES['cover_photo'])){
                $cover_photo = $_FILES['cover_photo']['name'];
                $chars = '0123456789';
                $count = mb_strlen($chars);
				for ($i =0, $rand ='';$i < 8;$i++) {
                    $index =rand(0,$count - 1);
                    $rand .= mb_substr($chars,$index, 1);
                }
				$cover_photo = $rand . $cover_photo;
                $cover_photo1 = 'cover_photo';
                $model->cover_photo = UploadedFile::getInstanceByName($cover_photo1);
				$model->cover_photo->saveAs('../../frontend/web/uploads/cover/' . $cover_photo);
            }else if(!empty($_POST['fb_photo'])){
                $cover_photo = $_POST['fb_photo'];
            }
                $s3->cover_photo = $cover_photo;
                $s3->update();
                
				$date = time();
                $post = new PostForm();
                $post->post_status = '1';
                $post->post_type = 'image';
                $post->is_deleted = '0';
                $post->post_privacy = 'Public';
                $post->image = $cover_photo;
                $post->post_created_date = "$date";
                $post->post_user_id = (string) $s3['_id'];
                $post->is_coverpic = '1';
                $post->insert();
            
            $datas['status'] = true;
            $datas['statusMsg'] = "Cover photo updated successfully";
            $datas['photo'] = $cover_photo;
        } else{
            $datas['status'] = false;
            $datas['statusMsg'] = "Select Photo type";
        }      
        return $datas;
    }
    
    /* Update User's all Detail with personal & security by user Id */
    public function actionUpdatebasicinformation()
	{
        
       $user_id = $_POST['user_id'];
       
       $data = array();
       $record_ins = new UserForm();
       $record_personal_ins = new Personalinfo();
       $record_security_ins = new UserSetting();
       $record = UserForm::find()->where(['_id' => $user_id])->one();
       $record_security = UserSetting::find()->where(['user_id' => $user_id])->one();
       $record_personal = Personalinfo::find()->where(['user_id' => $user_id])->one();
       
       if(isset($_POST['newpassword'])){
           $pwd = $_POST['password'];
           $recordpwd = UserForm::find()->where(['_id' => $user_id,'password' => $pwd])->one();
            if(!empty($recordpwd)){
                $recordpwd->password = $_POST['newpassword'];
                $recordpwd->update();
            }else{
                $data['status'] = false;
                $data['statusMsg'] = "password mismatch";
                return $data;
            }
           
       }
       
       if(isset($_POST['firstname'])){
            if(empty($record)){
                $record_ins->fname = $_POST['firstname'];
            }else{
                $record->fname = $_POST['firstname'];
            }
       }
       if(isset($_POST['lastname'])){
            if(empty($record)){
                $record_ins->lname = $_POST['lastname'];
            }else{
                $record->lname = $_POST['lastname'];
            }
       }
       if(isset($_POST['email'])){
            if(empty($record)){
                $record_ins->email = $_POST['email'];
            }else{
                $record->email = $_POST['email'];
            }
       }
       if(isset($_POST['alternatemail'])){
            if(empty($record)){
                $record_ins->alternate_email = $_POST['alternatemail'];
            }else{
                $record->alternate_email = $_POST['alternatemail'];
            }
       }
       if(isset($_POST['city'])){
            if(empty($record)){
                $record_ins->city = $_POST['city'];
            }else{
                $record->city = $_POST['city'];
            }
       }
       if(isset($_POST['country'])){
            if(empty($record)){
                $record_ins->country = $_POST['country'];
            }else{
                $record->country = $_POST['country'];
            }
       }
       if(isset($_POST['isdcode'])){
            if(empty($record)){
                $record_ins->isd_code = $_POST['isdcode'];
            }else{
                $record->isd_code = $_POST['isdcode'];
            }
       }
       if(isset($_POST['mobilenumber'])){
            if(empty($record)){
                $record_ins->phone = $_POST['mobilenumber'];
            }else{
                $record->phone = $_POST['mobilenumber'];
            }
       }
       if(isset($_POST['birthdate'])){
            if(empty($record)){
                $record_ins->birth_date = $_POST['birthdate'];
            }else{
                $record->birth_date = $_POST['birthdate'];
            }
       }
       if(isset($_POST['gender'])){
            if(empty($record)){
                $record_ins->gender = $_POST['gender'];
            }else{
                $record->gender = $_POST['gender'];
            }
       }
       if(isset($_POST['birth_privacy'])){
            if(empty($record_security)){
                $record_security_ins->birth_date_access = $_POST['birth_privacy'];
            }else{
                $record_security->birth_date_access = $_POST['birth_privacy'];
            }
       }
       if(isset($_POST['gender_privacy']))
	   {
            if(empty($record_security))
			{
                $record_security_ins->gender_access = $_POST['gender_privacy'];
            }else{
                $record_security->gender_access = $_POST['gender_privacy'];
            }
       }
       if(isset($_POST['language']))
	   {
		    $record_language = ArrayHelper::map(Language::find()->all(), 'name', 'name');
            $lan = explode(",",$_POST['language']);
            $lan_diffs= array_diff($lan,$record_language);
			if(isset($lan_diffs) && !empty($lan_diffs))
			{
				$insert_language = new Language();
				foreach($lan_diffs as $lan_diff)                  
				{
					$insert_language->name = ltrim(ucfirst($lan_diff));
					$insert_language->insert();
				}                  
			}
			if(empty($record_personal))
			{
                $record_personal_ins->language = $_POST['language'];
            }
			else
			{
                $record_personal->language = $_POST['language'];
            }
       }
       if(isset($_POST['aboutyourself']))
	   {
            if(empty($record_personal))
			{
                $record_personal_ins->about = $_POST['aboutyourself'];
            }
			else
			{
                $record_personal->about = $_POST['aboutyourself'];
            }
       }
       if(isset($_POST['education']))
	   {
            $record_education = ArrayHelper::map(Education::find()->all(), 'name', 'name');
            $ed = explode(',', $_POST['education']);
            $edu_differances = array_diff($ed,$record_education);
            if(isset($edu_differances) && !empty($edu_differances))
			{
				$insert_education = new Education();
				foreach($edu_differances AS $edu_differance)                  
				{
					$insert_education->name = ltrim(ucwords($edu_differance));
					$insert_education->insert();
				}                  
			}
			
			if(empty($record_personal))
			{
                $record_personal_ins->education = $_POST['education'];
            }
			else
			{
                $record_personal->education = $_POST['education'];
            }
       }
       if(isset($_POST['interest']))
	   {
            $record_interests = ArrayHelper::map(Interests::find()->all(), 'name', 'name');
			$int = explode(',', $_POST['interest']);
			$interests_differances=array_diff($int,$record_interests);
			if(isset($interests_differances) && !empty($interests_differances))
			{
				$insert_interests = new Interests();
				foreach($interests_differances as $interests_differance)                  
				{
					$insert_interests->name = ucwords($interests_differance);
					$insert_interests->insert();
				}                  
			}
			if(empty($record_personal))
			{
                $record_personal_ins->interests = $_POST['interest'];
            }
			else
			{
                $record_personal->interests = $_POST['interest'];
            }
       }
       if(isset($_POST['occupation']))
	   {
            $record_occupation = ArrayHelper::map(Occupation::find()->all(), 'name', 'name');
            $occ = explode(',', $_POST['occupation']);
            $occupation_differances=array_diff($occ,$record_occupation);
			if(isset($occupation_differances) && !empty($occupation_differances))
			{
				$insert_occupation = new Occupation();
				foreach($occupation_differances as $occupation_differance)                  
				{
					$insert_occupation->name = ucfirst($occupation_differance);
					$insert_occupation->insert();
				}                  
			}
			
			if(empty($record_personal))
			{
                $record_personal_ins->occupation = $_POST['occupation'];
            }
			else
			{
                $record_personal->occupation = $_POST['occupation'];
            }
       }
       if(!empty($record))
	   {
           $record->update();
           $data['status'] = true;
           $data['statusMsg'] = "Record Updated successfully";
       }
	   else
	   {
           $record_ins->_id = $user_id;
           $record_ins->insert();
           $data['status'] = true;
           $data['statusMsg'] = "Record Updated successfully";
       }
       if(!empty($record_security))
	   {
           $record_security->update();
           $data['status'] = true;
           $data['statusMsg'] = "Record Updated successfully";
       }
	   else
	   {
           $record_security_ins->user_id = $user_id;
           $record_security_ins->insert();
           $data['status'] = true;
           $data['statusMsg'] = "Record Updated successfully";
       }
       if(!empty($record_personal))
	   {
           $record_personal->update();
           $data['status'] = true;
           $data['statusMsg'] = "Record Updated successfully";
       }
	   else
	   {
           $record_personal_ins->user_id = $user_id;
           $record_personal_ins->insert();
           $data['status'] = true;
           $data['statusMsg'] = "Record Updated successfully";
       }
       $records = UserForm::find()->where(['_id' => $user_id])->one();
       $record_securitys = UserSetting::find()->where(['user_id' => $user_id])->one();
       $record_personals = Personalinfo::find()->where(['user_id' => $user_id])->one();
       $data['basic'] = $records;
       $data['security'] = $record_securitys;
       $data['personal'] = $record_personals;
       
       return $data; 
    }
    
    /* set options for feed */
    public function actionSetfeedoptions()
	{
        $loginmodel = new LoginForm();
        $user_id = $_POST['user_id'];
        $post_id = $_POST['post_id'];
        if(isset($post_id) && !empty($post_id)) 
		{
            $data = array();
            if($_POST['option'] == "hide")
			{
                $userexist = HidePost::find()->where(['user_id' => $user_id])->one();
                $unfollow = new HidePost();
                if($userexist)
				{
                    if(strstr($userexist['post_ids'], $post_id))
                    {
                       $data['status'] = true;
                       $data['statusMsg'] = "Post option set successfull";
                    }
                    else
					{
                        $unfollow = HidePost::find()->where(['user_id' => $user_id])->one();
                        $unfollow->post_ids = $userexist['post_ids'].','.$post_id;
                        if($unfollow->update())
                        {
                            $data['status'] = true;
                            $data['statusMsg'] = "Post option set successfull";
                        }
                        else
						{
                            $data['status'] = false;
                            $data['statusMsg'] = "Post option can't set successfull";
                        }
                    }
                }
                else
				{
                    $unfollow->user_id = $user_id;
                    $unfollow->post_ids = $_POST['post_id'];
                    if($unfollow->insert())
                    {
                        $data['status'] = true;
                        $data['statusMsg'] = "Post option set successfull";
                    }
                    else
					{
                            $data['status'] = false;
                            $data['statusMsg'] = "Post option can't set successfull";
                    }
                }
            }
			elseif($_POST['option'] == "save")
			{
                $posttype = $_POST['posttype'];
                $date = time();
                $data = array();
                $userexist = SavePost::find()->where(['user_id' => $user_id,'post_id' => $post_id])->one();
                $savestatus = new SavePost();
                if($userexist)
				{
                    $savestatus = SavePost::find()->where(['user_id' => $user_id,'post_id' => $post_id])->one();
                    $spvalue = $savestatus['is_saved'];
                    if($spvalue == '1')
                    {
                        $savestatus->is_saved = '0';
                        $data['saved'] = '1';
                    }
                    else
                    {
                        $savestatus->is_saved = '1';
                        $data['saved'] = '0';
                    }
                    $savestatus->saved_date = $date;
                    if($savestatus->update())
                    {
                        $data['status'] = true;
                        $data['statusMsg'] = "Post option set successfull";

                    }
                    else
                    {
                        $data['status'] = false;
                        $data['statusMsg'] = "Post option can't set successfull";
                    }
                }
                else
                {
                    $savestatus->user_id = $user_id;
                    $savestatus->post_id = $post_id;
                    $savestatus->post_type = $posttype;
                    $savestatus->is_saved = '1';
                    $savestatus->saved_date = $date;
                    if($savestatus->insert())
                    {
                        $data['saved'] = '1';
						$data['status'] = true;
                        $data['statusMsg'] = "Post option set successfull";
                    }
                    else
					{
						 $data['saved'] = '2';
                        $data['status'] = false;
                        $data['statusMsg'] = "Post option can't set successfull";
                    }
                }
            }
			elseif($_POST['option'] == "mutenotification")
			{
                $data = array();
                $userexist = TurnoffNotification::find()->where(['user_id' => $user_id])->one();
                $ton = new TurnoffNotification();
                if ($userexist)
				{
                    if (strstr($userexist['post_ids'], $post_id))
					{
                        $ton = TurnoffNotification::find()->where(['user_id' => $user_id])->one();
                        $ton->post_ids = str_replace($post_id.',',"",$userexist['post_ids']);
                        $tonids = $ton->post_ids;
                        $ton->update();
                        if(strlen($tonids) == 0)
                        {
                                $ton = TurnoffNotification::find()->where(['user_id' => $user_id])->one();
                                $ton->delete();
                        }
                        $data['status'] = true;
                        $data['statusMsg'] = "Post option set successfull";
                    }
					else
					{
                        $ton = TurnoffNotification::find()->where(['user_id' => $user_id])->one();
                        $ton->post_ids = $userexist['post_ids'].$post_id.',';
                        if ($ton->update())
						{
                            $data['status'] = true;
                            $data['statusMsg'] = "Post option set successfull";
                        }
						else
						{
                            $data['status'] = false;
                            $data['statusMsg'] = "Post option can't set successfull";
                        }
                    }
                }
				else
				{
                    $ton->user_id = $user_id;
                    $ton->post_ids = $post_id.',';
                    if ($ton->insert())
					{
                        $data['status'] = true;
                        $data['statusMsg'] = "Post option set successfull";
                    }
					else
					{
                        $data['status'] = false;
                        $data['statusMsg'] = "Post option can't set successfull";
                    }
                }
            }
			elseif($_POST['option'] == "deletepost")
			{
				$delete = new PostForm();
				$date = time();
				$data = array();
				$delete = PostForm::find()->where(['_id' => $post_id])->one();
				
				$post_owner_id = $delete['post_user_id'];
				
				if($delete['pagepost'] == '1')
				{
					$post_owner_id = $delete['shared_by'];
				}
				$delete->is_deleted = '1';
				if($delete->update()){
					$notification = new Notification();
					Notification::updateAll(['is_deleted' => '1'], ['post_id' => $post_id]);
					$data['status'] = true;
					$data['statusMsg'] = "Post deleted successfull";
				}else{
					$data['status'] = false;
                    $data['statusMsg'] = "Post option can't set successfull";	
				}
            }
        }
        return $data;
    }

    /* get list of notification */
    public function actionGetnotificationslist()
	{
        $userid = $_POST['user_id'];
        $data = array();
        $posts_array = array();
        $i = 0;
        $model_notification = new Notification();
        $notifications = $model_notification->getAllNotificationAPI($userid);
		if($notifications)
		{
			foreach ($notifications as $notification)
			{
				$datastring_image 	= '';
				$datastring_text 	= '';
				$datasmallimage  	= '';
				/*if($notification['notification_type'] == 'sharepost')
				{
					$not_img = $this->getimageAPI($notification['shared_by'],'thumb');
				}
				else
				{
					$not_img = $this->getimageAPI($notification['user']['_id'],'thumb');
				}*/
				if($notification['notification_type'] == 'sharepost')
				{
					$not_img = $this->getimageAPI($notification['shared_by'],'thumb');
				}
				else if($notification['notification_type'] == 'deletepostadmin' || $notification['notification_type'] == 'publishpost' || $notification['notification_type'] == 'deletecollectionadmin' || $notification['notification_type'] == 'publishcollection' || $notification['notification_type'] == 'deletepageadmin' || $notification['notification_type'] == 'publishpage')
				{
					$not_img = $this->getimageAPI('admin','thumb');
				}
				else if($notification['notification_type'] == 'editpostuser')
				{
					$not_img = $this->getimageAPI($notification['post']['post_user_id'],'thumb');
				}
				else if ($notification['notification_type'] == 'editcollectionuser')
				{
					 $not_img = $this->getimageAPI($notification['collection_owner_id'],'thumb');
				}
				else if($notification['notification_type'] == 'onpagewall')
				{
					$not_img = $this->getimageAPI($notification['user_id'],'thumb');
				}
				else
				{
					/*if(isset($notification['page_id']) && !empty($notification['page_id']) && $notification['notification_type'] == 'post')
					{
						if($_SERVER['HTTP_HOST'] == 'localhost')
						{
							$baseUrll = '/iaminaqabacode/frontend/web';
						}
						else
						{
							$baseUrll = '/iaminaqabacode/frontend/web/assets/baf1a2d0';
						}
						$not_img = $this->getpageimage($notification['page_id']);
					}
					else
					{*/
						$not_img = $this->getimageAPI($notification['user']['_id'],'thumb');
					/*}*/
				}
				if(isset($notification['post']['post_text']) && !empty($notification['post']['post_text'])) 
				{
					if(strlen($notification['post']['post_text']) > 20)
					{
						$notification_post_post_text = substr($notification['post']['post_text'],0,20);
						$notification_post_post_text = substr($notification['post']['post_text'], 0, strrpos($notification['post']['post_text'], ' '));
					}
					else
					{
						$notification_post_post_text = $notification['post']['post_text'];
					}
				}
				if(empty($notification['post']['post_text']) && $notification['notification_type'] != 'likereply') 
				{
					if($notification['notification_type']!='connectrequestaccepted' && $notification['notification_type']!='connectrequestdenied')
					{
						$notification_post_post_text = 'View Post';
					}
				}
				
				if($notification['notification_type'] == 'tag_connect')
				{
					$name = 'You';
				}
				else if($notification['notification_type'] == 'deletepostadmin' || $notification['notification_type'] == 'publishpost' || $notification['notification_type'] == 'deletecollectionadmin' || $notification['notification_type'] == 'publishcollection' || $notification['notification_type'] == 'deletepageadmin' || $notification['notification_type'] == 'publishpage')
				{
					$name = 'Iaminaqaba Admin';
				}
				else if($notification['notification_type'] == 'editpostuser')
				{
					$name = $this->getuserdata($notification['post']['post_user_id'],'fullname');
				}
				else if ($notification['notification_type'] == 'editcollectionuser')
				{
					$name = $this->getuserdata($notification['collection_owner_id'],'fullname');
				}
				else if($notification['notification_type'] == 'sharepost')
				{
					$usershare = LoginForm::find()->where(['_id' => $notification['user_id']])->one();
					$usershare_id = $usershare['_id'];
					if($notification['user_id'] == $userid){$user_name = 'Your';}else{ $user_name = $usershare['fullname']; }

					$post_owner_id = LoginForm::find()->where(['_id' => $notification['post_owner_id']])->one();
					$post_owner_id_name_id = $post_owner_id['_id'];
					if($notification['post_owner_id'] == $userid){$post_owner_id_name = 'Your';}else{ $post_owner_id_name = $post_owner_id['fullname'].'\'s'; }

					$shared_by = LoginForm::find()->where(['_id' => $notification['shared_by']])->one();
					$shared_by_name_id = $shared_by['_id'];
					if($notification['shared_by'] == $userid){$shared_by_name = 'You';}else{ $shared_by_name = $shared_by['fullname']; }
					$name = "";
					$name .= "<span class='btext'>";
					$name .= $shared_by_name;
					$name .= "</span> Shared <span class='btext'>";
					$name .= $post_owner_id_name;
					$name .= "</span> Post on <span class='btext'>";
					$name .= $user_name;
					$name .= "</span> Wall: ";
				}
				else
				{
					if(isset($notification['page_id']) && !empty($notification['page_id']) && $notification['notification_type'] == 'post')
					{
						$page_id = Page::Pagedetails($notification['page_id']);
						$name = $page_id['page_name'];
					}
					else
					{
						$name = ucfirst($notification['user']['fname']).' '.ucfirst($notification['user']['lname']);
					}
				}
				
				//Notification time
				$notification_time = Yii::$app->EphocTime->time_elapsed_A(time(),$notification['updated_date']);
				
				//Notification Post Id
				$npostid = $notification['post_id'];

				$datastring_image = $not_img;
				
				
				if($notification['notification_type'] != 'sharepost') 
				{ 
					$datastring_text.= $name;
				}
				if($notification['notification_type']=='likepost' || $notification['notification_type']== 'like')
				{ 
					$datastring_text.= 'Likes your post:';
					$datastring_text.= $notification['post']['post_text'];
				} 
				else if($notification['notification_type']=='likecomment')
				{  
					$datastring_text.= 'Likes your comment: View Post';
				} 
				else if($notification['notification_type'] == 'sharepost')
				{ 
					$datastring_text.= $name;
					$datastring_text.= $notification['post']['post_text'];
				} 
				else if($notification['notification_type'] == 'comment')
				{ 
					$datastring_text.= 'Commented on your post:';
					$datastring_text.= $notification['post']['post_text'];
				}
				else if($notification['notification_type'] == 'tag_connect')
				{  	$datastring_text.= 'Tagged in the post:';
					$datastring_text.= $notification['post']['post_text'];
				}
				else if($notification['notification_type'] == 'post')
				{ 
					if($notification['post']['is_album'] == '1')
					{ 
						$datastring_text.= ' Added new album:';
						$datastring_text.= $notification['post']['album_title'];
					}
					else
					{ 
						$datastring_text.= ' Added new post:';
						$datastring_text.= $notification['post']['post_text'];
					} 
				} 
				else if($notification['notification_type'] == 'commentreply')
				{ 	
					$datastring_text.= 'Replied on your comment:';
					$datastring_text.= $notification['post']['post_text'];
				} 
				else if($notification['notification_type'] == 'connectrequestaccepted')
				{ 
					$datastring_text.= ' Accepted your connect request.';
				} 
				else if($notification['notification_type'] == 'connectrequestdenied')
				{ 	
					$datastring_text.= ' Denied your connect request.';
				} 
				else if($notification['notification_type'] == 'onwall')
				{ 
					$datastring_text.=' Write on your wall.';
				} 
				else if($notification['notification_type'] == 'pageinvitereview')
				{
					$page_info = Page::Pagedetails($npostid);
					$datastring_text.= ' Invited to review'; 
					$datastring_text.= $page_info['page_name'].'page.';
				} 
				else if($notification['notification_type'] == 'pagereview'){
					$page_info = Page::Pagedetails($npostid);
					$datastring_text.= ' Reviewed '.$page_info['page_name'].'page.';
				} 
				else if($notification['notification_type'] == 'pageinvite')
				{
					$page_info = Page::Pagedetails($npostid);
					$datastring_text.= ' Invited to like '.$page_info['page_name'].'page.';
				}
				else if($notification['notification_type'] == 'likepage')
				{
					$page_info = Page::Pagedetails($npostid);
					$datastring_text.= ' Liked '.$page_info['page_name'].' page.';
				} 
				else if($notification['notification_type'] == 'onpagewall'){
					$page_info = Page::Pagedetails($npostid);
					$datastring_text.= ' Write on '.$page_info['page_name'].' page.';
				} 
				else if($notification['notification_type'] == 'deletepostadmin')
				{
					$datastring_text.=' Flaged your post for '.$notification['flag_reason'].'.';
				} 
				else if($notification['notification_type'] == 'deletecollectionadmin')
				{
					$datastring_text.= ' Flaged your collection for '.$notification['flag_reason'].'.';
				}
				else if($notification['notification_type'] == 'deletepageadmin')
				{
					$datastring_text.= ' Flaged your page for '.$notification['flag_reason'].'.';
				}
				else if($notification['notification_type'] == 'editpostuser')
				{
					$datastring_text.=  ' has edited flaged post.';
				}
				else if($notification['notification_type'] == 'editcollectionuser')
				{
					$datastring_text.=  'has edited flaged collection.';
				}
				else if($notification['notification_type'] == 'publishpost')
				{
					$datastring_text.=  ' Approved your post.';
				}
				else if($notification['notification_type'] == 'publishcollection')
				{
					$datastring_text.=  ' Approved your collection.';
				}
				else if($notification['notification_type'] == 'publishpage')
				{
					$datastring_text.=  'Approved your Page.';
				} 
				else if($notification['notification_type'] == 'page_role_type')
				{
					$page_info = Page::Pagedetails($npostid);
					if($notification['status'] == '0')
					{
						$lblrole = 'Removed';
					}
					else
					{
						$lblrole = 'Added';
					}
					$datastring_text.= $lblrole.' you as '.$notification['page_role_type'].' for '.$page_info['page_name'].' page.';
				} 
				else
				{ 
					$datastring_text.=  'Likes post';
				}  
				
				
				// Small Image code start
				if($notification['notification_type']=='likepost' || $notification['notification_type']== 'like' || $notification['notification_type']== 'likepage')
				{ 
					$datasmallimage .= "fa-thumbs-up";
				} 
				else if($notification['notification_type']== 'comment') 
				{
					$datasmallimage .= "fa-comment";
				}
				else if($notification['notification_type'] == 'sharepost')
				{ 
					$datasmallimage .= "fa-share-alt";
				}
				else if($notification['notification_type'] == 'pageinvite')
				{ 
					$datasmallimage .= "fa-thumbs-o-up";
				}
				else if($notification['notification_type'] == 'pageinvitereview')
				{ 
					$datasmallimage .= "fa-pencil"; 
				}
				else if($notification['notification_type'] == 'pagereview')
				{ 
					$datasmallimage .= "fa-pencil-square";
				}
				else 
				{ 
					$datasmallimage .= "fa-globe";
				}
				if($notification['notification_type'] == 'connectrequestaccepted'){
					$npostid = $notification['user_id'];
				}
				
				$data[$i]['post_id']	= $npostid;
				$data[$i]['image'] 		= $datastring_image;
				$data[$i]['text'] 		= $datastring_text;
				$data[$i]['not_type'] 	= $notification['notification_type'];
				$data[$i]['small_img'] 	= $datasmallimage;
				$data[$i]['time'] 		= $notification_time;
				$i++; 
			}
			
		}else{
			$data['status'] = "Notification off";
		}	
		$posts_array['data'] = $data;
        return $posts_array;
    }
    
    /* get all details of feed */
    public function actionGetfeeddetails()
	{
        $post_id = $_POST['post_id'];
        $posts =  PostForm::find()->where(['_id' => $post_id ,'post_status' => '1'])->one();
		
		$post_array = array();
		$like_array = array();
		$comment_array = array();
		$feedback_array = array();
		$attachment_array = array();
		$tagedactornames_array = array();
		$pictures_array = array();
		$videos_array = array();
		$links_array = array();
		$shares_array = array();
		$j = 0;
		$k = 0;
        $likes = Like::find()->where(['post_id' => $post_id ,'status' => '1'])->all();
                   
		foreach($likes as $docLike)
		{
		 $like_array[$j] = UserForm::getUserDetails($docLike['user_id']); 
		 $j++;
		}

		$loaded_comments = Comment::find()->with('user')->with('post')->where(['post_id' => "$post_id",'status' => '1','parent_comment_id' => '0'])->orderBy(['created_date'=>SORT_DESC])->all();
		foreach($loaded_comments as $docComment)
		{
			$comment_array[$k]['main_comment'] = $docComment;
			$comment_array[$k]['actor'] = UserForm::getUserDetails($docComment['user_id']);
			$pcid = (string) $docComment['_id'];
			$loaded_sub_comments = Comment::find()->with('user')->with('post')->where(['parent_comment_id' => "$pcid",'status' => '1'])->orderBy(['created_date'=>SORT_DESC])->all();
			$l = 0;
			$subcomment_array = array();
			foreach($loaded_sub_comments as $docSubComment)
			{
				$subcomment_array[$l]['sub_comment'] = $docSubComment;
				$subcomment_array[$l]['actor'] = UserForm::getUserDetails($docSubComment['user_id']); 
				$l++;
			}
			$comment_array[$k]['subcomment'] = $subcomment_array;
			$k++;
		}
		if(isset($posts['image']) && $posts['image'] != 'No Image')
		{
		   $images = rtrim($posts['image'], ",");
		   $images = explode(',',$images );
		   $p = 0;
		   for($m = 0; $m < count($images); $m++)
		   {
			   $pictures_array[$p]['urls'] = $images[$m];
			   $p++;
		   }
		}
		if($posts['post_type'] == 'link')
		{
		   $p = 0;
		   $links_array[$p]['urls'] = $posts['post_text'];
		}
		if(isset($posts['post_tags']) && $posts['post_tags'] != 'null'){
		   
		   $tags = explode(',', $posts['post_tags']);
		   $p = 0;
		   for($m = 0; $m < count($tags); $m++)
		   {
			   $tagedactornames_array[$p]['tags'] = UserForm::getUserDetails($tags[$m]);
			   $p++;
		   }
		}
		if(isset($posts['parent_post_id']))
		{
		   $p = 0;
		   $pid = $posts['parent_post_id'];
		   $data[$i] = PostForm::find()->where(['_id'=>"$pid"])->one();
		   $uid = $data[$i]['post_user_id'];
		   $shares_array[$p]['parent_post'] = $data[$i];
		   $shares_array[$p]['actor'] = UserForm::getUserDetails($uid);
		}

		$feedback_array['likers'] = $like_array;
		$feedback_array['comments'] = $comment_array;

		$post_array['main_post'] = $posts; 
		$post_array['actor'] = UserForm::getUserDetails($posts['post_user_id']);
		$post_array['feedback'] = $feedback_array;
		$attachment_array['pictures'] = $pictures_array;
		$attachment_array['videos'] = $videos_array;
		$attachment_array['links'] = $links_array;
		$attachment_array['shares'] = $shares_array;
		$post_array['attachment'] = $attachment_array;
		$post_array['tagedactornames'] = $tagedactornames_array;
		return $post_array;
    }
    
    /* get connect request list */
    public function actionGetconnectrequests()
	{
        $user_id = $_POST['user_id'];
        $data = array();
        $posts_array = array();
        $i = 0;
        $pending_requests = Connect::connectPendingRequestsAPI($user_id);
        foreach ($pending_requests as $doc)
		{
            $data[$i]['main_list'] = $doc;
            $data[$i]['actor'] = $post_array[$i]['actor'] = UserForm::getUserDetails($doc['action_user_id']);
            $i++; 
        }
        $posts_array['data'] = $data;
        return $posts_array;
    }
    
    /* Get Notification Setting using user_id of user */
    
    public function actionGetnotificationsettings()
	{
        
        $user_id = $_POST['user_id'];
        $data = array();
        $notification = NotificationSetting::find()->where(['user_id' => $user_id])->one();
        if(!empty($notification))
		{
            $data['status'] = true;
            $data['statusMsg'] = 'Notification Setting List Availablle';
            $data['data'] = $notification;
        }
		else
		{
            $data['status'] = false;
            $data['statusMsg'] = 'Notification Setting List Not Availablle';
        }
        return $data;
    }
    
    /* Update Notification Setting using user_id of user */
    
    public function actionUpdatenotificationsettings()
	{
        
        $user_id = $_POST['user_id'];
        $data = array();
        $notification_ins = new NotificationSetting();
        $notification = NotificationSetting::find()->where(['user_id' => $user_id])->one();
        if(!empty($notification))
		{
            
            if(isset($_POST['connect_activity']))
			{
                $notification->connect_activity = $_POST['connect_activity'];
            }
            if(isset($_POST['email_on_account_issues']))
			{
                $notification->email_on_account_issues = $_POST['email_on_account_issues'];
            }
            if(isset($_POST['member_activity']))
			{
                $notification->member_activity = $_POST['member_activity'];
            }
            if(isset($_POST['connect_activity_on_user_post']))
			{
                $notification->connect_activity_on_user_post = $_POST['connect_activity_on_user_post'];
            }
            if(isset($_POST['non_connect_activity']))
			{
                $notification->non_connect_activity = $_POST['non_connect_activity'];
            }
            if(isset($_POST['connect_request']))
			{
                $notification->connect_request = $_POST['connect_request'];
            }
            if(isset($_POST['e_card']))
			{
                $notification->e_card = $_POST['e_card'];
            }
            if(isset($_POST['member_invite_on_meeting']))
			{
                $notification->member_invite_on_meeting = $_POST['member_invite_on_meeting'];
            }
            if(isset($_POST['question_activity']))
			{
                $notification->question_activity = $_POST['question_activity'];
            }
            if(isset($_POST['credit_activity']))
			{
                $notification->credit_activity = $_POST['credit_activity'];
            }
            if(isset($_POST['sound_on_notification']))
			{
                $notification->sound_on_notification = $_POST['sound_on_notification'];
            }
            if(isset($_POST['sound_on_message']))
			{
                $notification->sound_on_message = $_POST['sound_on_message'];
            }
            $notification->update();
            
            $notification_after = NotificationSetting::find()->where(['user_id' => $user_id])->one();
            $data['status'] = true;
            $data['statusMsg'] = 'Notification Setting Data Availablle';
            $data['data'] = $notification_after;
            
        }
		else
		{
            $notification_ins->user_id = $user_id;
            if(isset($_POST['connect_activity']))
			{
                $notification_ins->connect_activity = $_POST['connect_activity'];
            }
            if(isset($_POST['email_on_account_issues']))
			{
                $notification_ins->email_on_account_issues = $_POST['email_on_account_issues'];
            }
            if(isset($_POST['member_activity']))
			{
                $notification_ins->member_activity = $_POST['member_activity'];
            }
            if(isset($_POST['connect_activity_on_user_post']))
			{
                $notification_ins->connect_activity_on_user_post = $_POST['connect_activity_on_user_post'];
            }
            if(isset($_POST['non_connect_activity']))
			{
                $notification_ins->non_connect_activity = $_POST['non_connect_activity'];
            }
            if(isset($_POST['connect_request']))
			{
                $notification_ins->connect_request = $_POST['connect_request'];
            }
            if(isset($_POST['e_card']))
			{
                $notification_ins->e_card = $_POST['e_card'];
            }
            if(isset($_POST['member_invite_on_meeting']))
			{
                $notification_ins->member_invite_on_meeting = $_POST['member_invite_on_meeting'];
            }
            if(isset($_POST['question_activity']))
			{
                $notification_ins->question_activity = $_POST['question_activity'];
            }
            if(isset($_POST['credit_activity']))
			{
                $notification_ins->credit_activity = $_POST['credit_activity'];
            }
            if(isset($_POST['sound_on_notification']))
			{
                $notification_ins->sound_on_notification = $_POST['sound_on_notification'];
            }
            if(isset($_POST['sound_on_message']))
			{
                $notification_ins->sound_on_message = $_POST['sound_on_message'];
            }
            $notification_ins->insert();
            $notification_after = NotificationSetting::find()->where(['user_id' => $user_id])->one();
            $data['status'] = true;
            $data['statusMsg'] = 'Notification Setting Data Availablle';
            $data['data'] = $notification_after;
        }
        return $data;
    }
    
    /* set the settings for connect setting list*/
    public function actionConnectionssettingoptions()
	{
        $data = array();
        if(isset($_POST['fid'])){ $fid = $_POST['fid']; }else{ $fid = ""; }
        if(isset($_POST['user_id'])){ $user_id = $_POST['user_id']; }else{ $user_id = ""; }
        if(isset($_POST['from_id'])){ $from_id = $_POST['from_id']; }else{ $from_id = ""; }
        if(isset($_POST['to_id'])){ $to_id = $_POST['to_id']; }else{ $to_id = ""; }
        
        /* Mute/Unmute connect posts functionality works in this condition */
        if(isset($_POST['option']) && $_POST['option'] == "mute_connect_post")
		{
            $userexist = UnfollowConnect::find()->where(['user_id' => $user_id])->one();
            $unfollow = new UnfollowConnect();
            if ($userexist) 
            {
                if (strstr($userexist['unfollow_ids'],$fid))
				{
                    $unfollow = UnfollowConnect::find()->where(['user_id' => $user_id])->one();
                    $unfollow->unfollow_ids = str_replace($fid.',',"",$userexist['unfollow_ids']);
                    $unfollowids = $unfollow->unfollow_ids;
                    $unfollow->update();
                    if(strlen($unfollowids) == 0)
                    {
                        $unfollow = UnfollowConnect::find()->where(['user_id' => $user_id])->one();
                        $unfollow->delete();
                    }
                    $data['status'] = true;
                    $data['statusMsg'] = "Data Update successfully";
                }
                else
                {
                    $unfollow = UnfollowConnect::find()->where(['user_id' => $user_id])->one();
                    $unfollow->unfollow_ids = $userexist['unfollow_ids'].$fid.',';
                    if ($unfollow->update())
                    {
                        $data['status'] = true;
                        $data['statusMsg'] = "Data Update successfully";
                    }
                    else
                    {
                        $data['status'] = false;
                        $data['statusMsg'] = "Data Update issue";
                    }
                }
            }
            else
            {
                $unfollow->user_id = $user_id;
                $unfollow->unfollow_ids = $fid.',';
                if ($unfollow->insert())
                {
                    $data['status'] = true;
                    $data['statusMsg'] = "Data insert successfully";
                }
                else
                {
                    $data['status'] = false;
                    $data['statusMsg'] = "Data insert issue";
                }
            }
        }
        /* Mute/Unmute Notifications functionality works in this condition */
        else if(isset($_POST['option']) && $_POST['option'] == "mute_notifications")
		{
            $userexist = MuteConnect::find()->where(['user_id' => $user_id])->one();
            $mute = new MuteConnect();
            if ($userexist)
            {
                if (strstr($userexist['mute_ids'], $fid))
                {
                    $mute = MuteConnect::find()->where(['user_id' => $user_id])->one();
                    $mute->mute_ids = str_replace($fid.',',"",$userexist['mute_ids']);
                    $muteids = $mute->mute_ids;
                    $mute->update();
                    if(strlen($muteids) == 0)
                    {
                        $mute = MuteConnect::find()->where(['user_id' => $user_id])->one();
                        $mute->delete();
                    }
                    $data['status'] = true;
                    $data['statusMsg'] = "Data Update successfully";
                }
                else
                {
                    $mute = MuteConnect::find()->where(['user_id' => $user_id])->one();
                    $mute->mute_ids = $userexist['mute_ids'].$fid.',';
                    if ($mute->update())
                    {
                        $data['status'] = true;
                        $data['statusMsg'] = "Data update successfully";
                    }
                    else
                    {
                        $data['status'] = false;
                        $data['statusMsg'] = "Data update issue";
                    }
                }
            }
            else
            {
                $mute->user_id = $user_id;
                $mute->mute_ids = $fid.',';
                if ($mute->insert())
                {
                    $data['status'] = true;
                    $data['statusMsg'] = "Data insert successfully";
                }
                else
                {
                    $data['status'] = false;
                    $data['statusMsg'] = "Data insert issue";
                }
            }
        }
        /* Suggest connections functionality works in this condition */
        else if(isset($_POST['option']) && $_POST['option'] == "suggest_connections")
		{
            $eml_id = Connect::getuserConnections($user_id);
            $frnd = array();
            $z = 0;
            if (!empty($eml_id))
            {
                foreach ($eml_id as $val) 
				{
                    $guserid = (string) $val['userdata']['_id'];
                    $is_connect = Connect::find()->where(['from_id' => $fid,'to_id' => $guserid,'status' => '1'])->one();
                    if(($fid != $guserid) && !$is_connect)
                    {
                        $frnd[$z]['name'] = $val['userdata']['fullname'];
                        $frnd[$z]['id'] = $guserid;
                    }
                    $z++;
                } 
                $data['status'] = true;
                $data['statusMsg'] = "Suggested connect list";
                $data['data'] = $frnd;
            } 
			else 
			{ 
                    $data['status'] = false;
                    $data['statusMsg'] = "You both have same connections";
            }
        }
        /* unconnections functionality works in this condition */
        else if(isset($_POST['option']) && $_POST['option'] == "unconnect")
		{
            $request_first = Connect::find()->where(['from_id' => $from_id , 'to_id' => $to_id])->one();
        
            if(count($request_first)>0)
                $request_first->delete();

            $request_second = Connect::find()->where(['from_id' => $to_id , 'to_id' => $from_id])->one();
            if(count($request_second)>0)
                $request_second->delete(); 
            
            $data['status'] = true;
            $data['statusMsg'] = "Unconnect successfully";
        }
        /* block connections functionality works in this condition */
        else if(isset($_POST['option']) && $_POST['option'] == "block")
		{
            $userexist = BlockConnect::find()->where(['user_id' => $user_id])->one();
            $mute = new BlockConnect();
            if ($userexist)
            {
                if (strstr($userexist['block_ids'], $fid))
				{
                    $mute = BlockConnect::find()->where(['user_id' => $user_id])->one();
                    $mute->block_ids = str_replace($fid.',',"",$userexist['block_ids']);
                    $muteids = $mute->block_ids;
                    $mute->update();
                    if(strlen($muteids) == 0)
                    {
                            $mute = BlockConnect::find()->where(['user_id' => $user_id])->one();
                            $mute->delete();
                    }
                    $data['status'] = true;
                    $data['statusMsg'] = "Request Execute successfully";
                }
				else
				{
                    $mute = BlockConnect::find()->where(['user_id' => $user_id])->one();
                    $mute->block_ids = $userexist['block_ids'].$fid.',';
                    if ($mute->update())
					{
                        $data['status'] = true;
                        $data['statusMsg'] = "Request Execute successfully";
                    }
					else
					{
                        $data['status'] = false;
                        $data['statusMsg'] = "Request Execute fail";
                    }
                }
            }
			else
			{
                $mute->user_id = $user_id;
                $mute->block_ids = $fid.',';
                if ($mute->insert())
				{
                    $data['status'] = true;
                    $data['statusMsg'] = "Request Execute successfully";
                }
				else
				{
                    $data['status'] = false;
                    $data['statusMsg'] = "Request Execute fail";
                }
            }
        }
		else
		{
            $data['status'] = false;
            $data['statusMsg'] = "Please select Option";
        }
        
        return $data;
    }
   
    /* accept or reject connect request*/  
    public function actionAcceptrejectconnectrequest()
	{
        $data = array();
        $request = Connect::find()->where(['from_id' => $_POST['fid'] , 'to_id' => $_POST['user_id']])->one();
        if(isset($_POST['option']) && $_POST['option'] == 'accept'){   
            if(!empty($request))
            {
                $date = time();
                $request->action_user_id = $_POST['user_id'];
                $request->status = '1';
                $request->updated_date = $date;
                $request->update();

            }

            $request_second = Connect::find()->where(['from_id' => $_POST['user_id'] , 'to_id' => $_POST['fid']])->one();

            if(!empty($request_second))
            {
                $date = time();
                $request_second->action_user_id = $_POST['user_id'];
                $request_second->status = '1';
                $request_second->updated_date = $date;
                $request_second->update();
                $srequest = new SuggestConnect();
                $srequest = SuggestConnect::find()->where(['connect_id' => $_POST['user_id'] , 'suggest_to' => $_POST['fid']])->one();
                if($srequest)
                {
                    $srequest->status = '1';
                    $srequest->update();
                }

            }

            if(!empty($request) && !empty($request_second))
            {
                // Insert record in notification table also
                $notification =  new Notification();
                $notification->from_connect_id = $_POST['fid'];
                $notification->user_id = $_POST['user_id'];
                $notification->notification_type = 'connectrequestaccepted';
                $notification->is_deleted = '0';
                $notification->status = '1';
                $notification->created_date = "$date";
                $notification->updated_date = "$date";
                $notification->insert();
            }
            
            $data['status'] = true;
            $data['statusMsg'] = "Connect request accept successfully";
        }
		else if(isset($_POST['option']) && $_POST['option'] == 'reject')
		{
            $from_id = $_POST['fid'];
            $to_id = $_POST['user_id'];
            $request_first = Connect::find()->where(['from_id' => $from_id , 'to_id' => $to_id])->one();
        
            if(count($request_first)>0)
                $request_first->delete();

            $request_second = Connect::find()->where(['from_id' => $to_id , 'to_id' => $from_id])->one();
            if(count($request_second)>0)
                $request_second->delete();
            
            $data['status'] = true;
            $data['statusMsg'] = "Connect request reject successfully";
        }
		else
		{
            $data['status'] = false;
            $data['statusMsg'] = "Connect request reject successfully";
        }
        return $data;                 
    }
	
	public function actionUserpendingrequest()
	{
		$user_id = $_POST['user_id'];
		$data = array();
        $user_pending = Connect::find()->where(['from_id' => "$user_id" , 'status' => "0"])->andWhere(['not','to_id', "$user_id"])->all();
		$data['status'] = true;
        $data['statusMsg'] = "Pending connect request";
        $data['data'] = $user_pending;
        return $data;
	}
    
    /* suggested user list for connect*/
    public function actionGetsuggesteduserslist()
	{
        $user_id = $_POST['user_id'];
        $fid = $_POST['fid'];
        $eml_id = Connect::getuserConnections($user_id);
        $frnd = array();
        $data = array();
        $posts_array = array();
        $i = 0;
        if (!empty($eml_id))
        {
            foreach ($eml_id as $val) 
			{
                $guserid = (string) $val['userdata']['_id'];
                $is_connect = Connect::find()->where(['from_id' => $fid,'to_id' => $guserid,'status' => '1'])->one();
                if(($fid != $guserid) && !$is_connect)
                {
                    $frnd[$i]['name'] = $val['userdata']['fullname'];
                    $frnd[$i]['id'] = $guserid;
                }else{
                    continue;
                    
                }
                $i++;
            } 
            $posts_array['data'] = $frnd;
            $data['status'] = true;
            $data['statusMsg'] = "Suggested connect list";
            $data['data'] = $posts_array;
        } 
		else 
		{ 
                $data['status'] = false;
                $data['statusMsg'] = "You both have same connections";
        }
        return $data;
    }
    
    /* view all comments of feed */
    public function actionViewallcomments()
	{
        $post_id = $_POST['post_id'];
        $comment_array = array();
        $data = array();
        $k = 0;
        $loaded_comments = Comment::find()->with('user')->with('post')->where(['post_id' => "$post_id",'status' => '1','parent_comment_id' => '0'])->orderBy(['created_date'=>SORT_DESC])->all();
        foreach($loaded_comments as $docComment)
		{
            $comment_array[$k]['main_comment'] = $docComment;
            $comment_array[$k]['actor'] = UserForm::getUserDetails($docComment['user_id']);
            $pcid = (string) $docComment['_id'];
            $loaded_sub_comments = Comment::find()->with('user')->with('post')->where(['parent_comment_id' => "$pcid",'status' => '1'])->orderBy(['created_date'=>SORT_DESC])->all();
            $l = 0;
            $subcomment_array = array();
            foreach($loaded_sub_comments as $docSubComment)
			{
                $subcomment_array[$l]['sub_comment'] = $docSubComment;
                $subcomment_array[$l]['actor'] = UserForm::getUserDetails($docSubComment['user_id']); 
                $l++;
            }
            $comment_array[$k]['subcomment'] = $subcomment_array;
			$k++;
        } 
        $data['status'] = true;
        $data['statusMsg'] = "Commments found";
        $data['data'] = $comment_array;
        return $data;
    }
    
    /* global serch of user */
    public function actionGlobalsearchtext()
	{
        $suserid = $_GET['user_id'];
        if (isset($_GET['key']) && !empty($_GET['key'])) 
		{
            $email = $_GET['key'];
            $eml_id = LoginForm::find()->where(['like','fname',$email])
                    ->orwhere(['like','lname',$email])
                    ->orwhere(['like','fullname',$email])
                    ->andwhere(['status'=>'1'])
                    ->orderBy([$email => SORT_ASC])
                    ->limit(7)
                    ->all();
           //echo "<pre>";print_r($eml_id);exit;
            $main_data = array();
            $data = array();
            
            if (!empty($eml_id)) 
			{ 
				$i = 0;
                foreach ($eml_id as $val) 
				{
                    $guserid = (string)$val->_id;

                    $block = BlockConnect::find()->where(['user_id' => $guserid])->andwhere(['like','block_ids',$suserid])->one();
                    if(!$block)
                    {
                        $result_security = SecuritySetting::find()->where(['user_id' => $guserid])->one();
                        if($result_security)
                        {
                            $lookup_settings = $result_security['my_view_status'];
                        }
                        else
                        {
                            $lookup_settings = 'Public';
                        }
                        $is_connect = Connect::find()->where(['from_id' => $guserid,'to_id' => $suserid,'status' => '1'])->one();
                        if(($lookup_settings == 'Public') || ($lookup_settings == 'Connections' && ($is_connect || $guserid == $suserid)) || ($lookup_settings == 'Private' && $guserid == $suserid)) {
                            $main_data[$i]['id'] = $val['_id'];
                            $main_data[$i]['photo'] = $this->getimageAPI($val['_id'],'thumb');
                            if(empty($val->city)){
                                $val->city = '&nbsp;';
                            }
                            $main_data[$i]['fname'] = $val->fname; 
                            $main_data[$i]['lname'] = $val->lname; 
                            $main_data[$i]['city'] = $val->city; 
                        }
                    }
					else
					{
                        continue;
                    } 
                    $i++;
                } 
				$data['status'] = true;
                $data['statusMsg'] = "Global search found";
                $data['data'] = $main_data;
            } 
			else 
			{
                $data['status'] = false;
                $data['statusMsg'] = "No data found";
            }
        }
		else 
		{
                $data['status'] = false;
                $data['statusMsg'] = "Key not found";
        }
        return $data;
    }
    
    /* mutual connect list */
    public function actionGetmutualconnectlist()
	{
        
        $user_id = $_POST['user_id'];
        $fid = $_POST['fid'];
        $data = array();
        $mutual = Connect::mutualconnect($fid,$user_id);
        
        $data['status'] = true;
        $data['statusMsg'] = "Mutual connect data found";
        $data['data'] = $mutual;
        return $data;
    }
    
    /* Pin specific photo */
    public function actionPinphotos()
    {		
        $model = new PinImage();
        $user_id = $_POST['user_id'];
        $imagename = $_POST['iname'];
        $date = time();
        $data = array();
        $pinit = new PinImage();		
        $pinit = PinImage::find()->where(['user_id' => $user_id,'imagename' => $imagename])->one();

        if($pinit)
        {
            $pinit = new PinImage();
            $pinit = PinImage::find()->where(['user_id' => $user_id,'imagename' => $imagename])->one();
            if($pinit['is_saved'] == '1')
			{
				$pinvalue = '0';
			}
			else
			{
				$pinvalue = '1';
			}
            $pinit->is_saved = $pinvalue;
            $pinit->pinned_at = "$date";
            if($pinit->update())
            {
                $data['status'] = true;
				$data['pinvalue'] = $pinvalue;
                $data['statusMsg'] = "Pinimage update successfully";
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Pinimage update fail";
            }
        }
        else
        {
            $pinit = new PinImage();
            $pinit->user_id = $user_id;
            $pinit->imagename = $imagename;
            $pinit->is_saved = '1';
            $pinit->pinned_at = "$date";
            if($pinit->insert())
            {
                $data['status'] = true;
                $data['statusMsg'] = "Pinimage insert successfully";
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Pinimage insert fail";
            }	
        }
		return $data;	
    }
    
    /* get pinned photos of user */
    public function actionGetpinnedphotos()
	{
        $data = array();
        $gallary_data = array();
        $user_id = $_POST['user_id'];
        $wall_user_id = $_POST['wall_user_id'];
        $k = $i = $l = 0;
        $comment_array = array();
		$like_array = array();
        $gallery = PinImage::getPinnedImages($wall_user_id);
        $galleryuser = LoginForm::find()->where(['_id' => $wall_user_id])->one();
        $loginuser = LoginForm::find()->where(['_id' => "$user_id"])->one();
        $gallery_img = $this->getimageAPI($loginuser['_id'],'photo');
        $main_data = array();
        if(count($gallery) > 0)
        {
            foreach($gallery as $gallery_item)
            {
                $eximg = '/uploads/'.$gallery_item['imagename'];
                $fileinfo = pathinfo($eximg);
                $uniq_id = $fileinfo['filename'] .'_'. $gallery_item['_id'];
                $like_count = Like::getLikeCount((string)$uniq_id);
				$like_array[$l]['count']= $like_count;
				$like_names = Like::getLikeUserNames((string)$uniq_id);
				$like_array[$l]['names'] =  $like_names['names'];
                $comments = Comment::getAllPostLike((string)$uniq_id);
                foreach($comments as $docComment)
				{
                    $comment_array[$k]['main_comment'] = $docComment;
                    $comment_array[$k]['actor'] = UserForm::getUserDetails($docComment['user_id']);
                    
					$pcid = (string) $docComment['_id'];
					$loaded_sub_comments = Comment::find()->with('user')->with('post')->where(['parent_comment_id' => "$pcid",'status' => '1'])->orderBy(['created_date'=>SORT_DESC])->all();
					$l = 0;
					$subcomment_array = array();
					foreach($loaded_sub_comments as $docSubComment)
					{
						$subcomment_array[$l]['sub_comment'] = $docSubComment;
						$subcomment_array[$l]['actor'] = UserForm::getUserDetails($docSubComment['user_id']); 
						$l++;
					}
                    $comment_array[$k]['subcomment'] = $subcomment_array;
                    $k++;
                }
                if(file_exists('../../frontend/web'.$eximg))
                {
                    $galimname = $gallery_item['imagename'];
                    $postdetails = PostForm::find()->where(['like','image',$galimname])->one();
                    $time = Yii::$app->EphocTime->comment_time(time(),$postdetails['post_created_date']);
					$puserid = (string)$postdetails['post_user_id'];
                    $curloc = $postdetails['currentlocation'];
                    $puserdetails = LoginForm::find()->where(['_id' => $puserid])->one();
                    if($puserid != $user_id)
                    {
                        $photo_add_user = ucfirst($puserdetails['fname']) . ' ' . ucfirst($puserdetails['lname']);
                    }
                    else
                    {
                        $photo_add_user = 'You';
                    }
                    $like_buddies = Like::getLikeUser((string)$fileinfo['filename'] .'_'. $gallery_item['_id']);
                    $newlike_buddies = array();
                    foreach($like_buddies as $like_buddy)
                    {
                        $newlike_buddies[] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
                    } 
                }
				else
				{
                    
                } 
                $gallary_data[$i]['Photo_add_user'] = $photo_add_user;
                $gallary_data[$i]['Photo_add_time'] = $time;
                $gallary_data[$i]['Photo_add_image'] = $eximg;
                $gallary_data[$i]['Photo_add_location'] = $curloc;
                $gallary_data[$i]['Photo_like_count'] = $like_array;
                $gallary_data[$i]['Photo_comment_count'] = $comment_array;
                $i++;
            }
            $main_data['data'] = $gallary_data;
            $data['status'] = true;
            $data['statusMsg'] = "Pin Image Found";
            $data['data'] = $main_data;
        }
		else
		{
                $data['status'] = false;
                $data['statusMsg'] = "No Pin Image Found";
        }
        return $data;
    }
    
    /* get saved post of user */
    public function actionGetsavedpost()
	{
        $user_id = $_POST['user_id'];
        $data = array();
        $SavePost = new SavePost();
        $savedposts = SavePost::find()->where(['user_id' => "$user_id",'is_saved' => '1'])->orderBy(['saved_date'=>SORT_DESC])->all();
     
        if(!empty($savedposts))
		{
            $data['status'] = true;
            $data['statusMsg'] = "Saved post available";
            $data['data'] = $savedposts;
        }
		else
		{
            $data['status'] = false;
            $data['statusMsg'] = "No Saved post available";
        }
        return $data;
    }
    
    /* set album privacy */
    public function actionAlbumprivacy()
    {
        $albumid = isset($_POST['albumid']) ? $_POST['albumid'] : '';
        $album_value = isset($_POST['albumvalue']) ? $_POST['albumvalue'] : '';
        $data = array();
        if($albumid != '' && $album_value != '' )
        {
            $editalbumprivacy1 = new PostForm();
            $editalbumprivacy = PostForm::find()->where(['_id' => new \MongoId($albumid)])->one();
            if($editalbumprivacy)
            {
                $editalbumprivacy->post_privacy = $album_value;
                if($editalbumprivacy->update())
                {
                    $data['status'] = true;
                    $data['statusMsg'] = "Privacy set successfully";
                }else{
                    $data['status'] = false;
                    $data['statusMsg'] = "Privacy can't set";
                }
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Privacy can't set";
            }
        }
        else
        {
            $data['status'] = false;
            $data['statusMsg'] = "Privacy can't set";
        }
        return $data;
    }
    
    /* delete user's album*/
    public function actionDeletealbum()
    {
        $album_id = isset($_POST['albumid']) ? $_POST['albumid'] : '';
        $data = array();
        if($album_id != '')
        {
            $delimage = new PostForm();
            $delimage = PostForm::find()->where(['_id' => $album_id])->one();
            if($delimage)
            {
                $delimage->is_album = '0';
                $delimage->is_deleted = '1';
                if($delimage->update())
                {
                    $data['status'] = true;
                    $data['statusMsg'] = "Album deleted successfully";
                }
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Album can't delete";
            }
        }
        else
        {
            $data['status'] = false;
            $data['statusMsg'] = "Album can't delete";
        }
        return $data;
    }
    
    /* set album cover */
    public function actionAlbumcover()
    {
        $image_name = $_POST['image_name'];
        $post_id = $_POST['post_id'];
        $data = array();
        if(isset($image_name) && !empty($image_name) && isset($post_id) && !empty($post_id))
        {
            $updatealbumcover = new PostForm();
            $updatealbumcover = PostForm::find()->where(['_id' => $post_id])->one();
            if($updatealbumcover)
            {
                $imagevalue = $updatealbumcover['image'];
                $eximgs = explode(',',$imagevalue,-1);
                $totalimgs = count($eximgs);
                if($totalimgs == '1')
                {
                    $data['status'] = true;
                    $data['statusMsg'] = "Album cover set successfully";
                }
                else
                {
                    $imagepath = $image_name.',';
                    $updatedimagevalue = str_replace($imagepath,"",$imagevalue);
                    $updatealbumcover->image = $image_name.','.$updatedimagevalue;
                    if($updatealbumcover->update())
                    {
                        $data['status'] = true;
                        $data['statusMsg'] = "Album cover set successfully";
                    }
                    else
                    {
                        $data['status'] = false;
                        $data['statusMsg'] = "Album cover can't set";
                    }
                }
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Album cover can't set";
            }
        }
        else
        {
            $data['status'] = false;
            $data['statusMsg'] = "Album cover can't set";
        }
        return $data;
    }
    
    /* delete photo from album */
    public function actionDeletephoto()
    {
        $image_name = isset($_POST['image_name']) ? $_POST['image_name'] : '';
        $post_id =  isset($_POST['post_id']) ? $_POST['post_id'] : '';
        $data = array();
        if($image_name != '' && $post_id != '')
        {
            $delimage = new PostForm();
            $delimage = PostForm::find()->where(['_id' => $post_id])->one();
            if($delimage)
            {
                $imagevalue = $delimage['image'];
                $imagepath = $image_name.',';
                $updatedimagevalue = str_replace($imagepath,"",$imagevalue);
                if(strlen($updatedimagevalue) < 3)
                {
                    $delimage->post_type = 'text';
                    if($delimage->is_album == '1') { $delimage->is_album = '0'; $delimage->is_deleted = '1';}
                }
                $delimage->image = $updatedimagevalue;
                if($delimage->update())
                {
                    $data['status'] = true;
                    $data['statusMsg'] = "Photo deleted";
                }
                else
                {
                    $data['status'] = false;
                    $data['statusMsg'] = "Photo can't deleted";
                }
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Photo can't deleted";
            }
        }
        else
        {
            $data['status'] = false;
            $data['statusMsg'] = "Photo can't deleted";
        }
        return $data;
    }
    
    /* get album photos */
    public function actionGetalbumphotos()
	{
        $user_id = $_POST['user_id'];
        $data = array();
        $json_data = array();
        $imagedata = array();
        $i = 0;
        if (isset($_POST['post_id']) && !empty($_POST['post_id']))
        {
            $getalbumdetails = PostForm::find()->where(['_id' => $_POST['post_id']])->one();
            $result = UserForm::find()->where(['_id' => (string)$getalbumdetails['post_user_id']])->one();
            $fullname = $result['fname'].' '.$result['lname'];
            if ($getalbumdetails)
            {
                $imgcontent = '';
                if(isset($getalbumdetails['image']) && !empty($getalbumdetails['image']))
                {
                    $eximgs = explode(',',$getalbumdetails['image'],-1);
                    $total_eximgs = count($eximgs); 
                
                    foreach ($eximgs as $eximg) 
					{
                        $fileinfo = pathinfo($eximg);
                        $uniq_id = $fileinfo['filename'] .'_'. $getalbumdetails['_id'];
                        $like_count = Like::getLikeCount((string)$uniq_id);
                        $like_names = Like::getLikeUserNames((string)$fileinfo['filename'] .'_'. $getalbumdetails['_id']);
                        $like_buddies = Like::getLikeUser((string)$fileinfo['filename'] .'_'. $getalbumdetails['_id']);
                        $is_like = Like::find()->where(['user_id'=>$user_id,'post_id'=>$uniq_id,'status'=>'1'])->one();
                        if($is_like){$ls = 'Liked';}else{$ls = 'Like';}
                        $newlike_buddies = array();
						$j = 0;
						foreach($like_buddies as $like_buddy)
                        {
                            $newlike_buddies[$j]['name'] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
                            $newlike_buddies[$j]['id'] = ucfirst($like_buddy['user']['_id']);
                            $j++;
                        }
						$imagedata[$i]['image_url'] = $eximg;
						$imagedata[$i]['image_id'] = (string)$getalbumdetails['_id'];
                        $imagedata[$i]['like_count'] = $like_count;
                        $imagedata[$i]['newlike_buddies'] = $newlike_buddies;
                        $i++;
                    }  
                } 
                $json_data['user_name'] = $fullname;
                $json_data['album_name'] = $getalbumdetails['album_title'];
                $json_data['total_image'] = $total_eximgs;
                $json_data['image_data'] = $imagedata;
                
                $data['status'] = true;
                $data['statusMsg'] = "Album Photos detail";
                $data['data'] = $json_data;
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Album details unavailable";
            }
        }
        else
        {
            $data['status'] = false;
            $data['statusMsg'] = "Select post id";
        }
		return $data;
    }
	
	/* get All photos */
    public function actionAllphotos()
	{
        $user_id = $_POST['user_id'];
        $data = array();
        $json_data = array(); 
        $imagedata = array();
        $i = 0;
        $gallery = PostForm::getUserPhotos($user_id);
		if($gallery)
		{
			foreach($gallery as $getalbumdetails)
            {
				$album_title = $getalbumdetails['album_title'];
				$eximgs = explode(',',$getalbumdetails['image'],-1);
				$imgcontent = '';
				foreach ($eximgs as $eximg) 
				{
					$fileinfo = pathinfo($eximg);
					$uniq_id = $fileinfo['filename'] .'_'. $getalbumdetails['_id'];
					$like_count = Like::getLikeCount((string)$uniq_id);
					$like_names = Like::getLikeUserNames((string)$fileinfo['filename'] .'_'. $getalbumdetails['_id']);
					$like_buddies = Like::getLikeUser((string)$fileinfo['filename'] .'_'. $getalbumdetails['_id']);
					$is_like = Like::find()->where(['user_id'=>$user_id,'post_id'=>$uniq_id,'status'=>'1'])->one();
					if($is_like){$ls = 'Liked';}else{$ls = 'Like';}
					$newlike_buddies = array();
					$j = 0;
					foreach($like_buddies as $like_buddy)
					{
						$newlike_buddies[$j]['name'] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
						$newlike_buddies[$j]['id'] = ucfirst($like_buddy['user']['_id']);
						$j++;
					}
					$imagedata[$i]['image_url'] = $eximg;
					$imagedata[$i]['image_id'] = (string)$getalbumdetails['_id'];
					$imagedata[$i]['like_count'] = $like_count;
					$imagedata[$i]['newlike_buddies'] = $newlike_buddies;
					$imagedata[$i]['album_title'] = $album_title;
					
					$i++;
				}
					$json_data['image_data'] = $imagedata;
					$data['status'] = true;
					$data['data'] = $json_data;
			}
		}
		else
		{
			$data['status'] = false;
		}	
        return $data;
    }
    
    /* Edit album detail */
    public function actionAlbumedit()
    {
        $album_id = isset($_POST['post_id']) ? $_POST['post_id'] : '';
        $data = array();
        if($album_id != '')
        {
            $editalbum = new PostForm();
            $editalbum = PostForm::find()->where(['_id' => $album_id])->one();
            if($editalbum)
            {
                $editalbum->album_title = $_POST['edit_title'];
                $editalbum->post_text = $_POST['edit_desc'];
                $editalbum->album_place = $_POST['edit_place'];
                if($editalbum->update())
                {
                    $data['status'] = true;
                    $data['statusMsg'] = "Album details update successfully";
                }
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Album details can't edit";
            }
        }
        else
        {
            $data['status'] = false;
            $data['statusMsg'] = "Album Id unavailable";
        }
        return $data;
    }
    
    /* move photo to another album */
    public function actionMovephotofromalbum()
    {
        $image_name = $_POST['image_name'];
        $from_post_id = $_POST['from_post_id'];
        $to_post_id = $_POST['to_post_id'];
        $data = array();
        if(isset($image_name) && !empty($image_name) && isset($from_post_id) && !empty($from_post_id) && isset($to_post_id) && !empty($to_post_id))
        {
            $removealbumimage = new PostForm();
            $removealbumimage = PostForm::find()->where(['_id' => $from_post_id])->one();
            $imagevalue = $removealbumimage['image'];
            $imagepath = $image_name.',';
            $removealbumimagee = str_replace($imagepath,"",$imagevalue);
            if(strlen($removealbumimagee) < 3)
            {
                $removealbumimage->post_type = 'text';
                if($removealbumimage->is_album == '1') 
				{ 
					$removealbumimage->is_album = '0';
					$removealbumimage->is_deleted = '1';
				}
            }
            $removealbumimage->image = $removealbumimagee;

            $addalbumimage = new PostForm();
            $addalbumimage = PostForm::find()->where(['_id' => $to_post_id])->one();
            $addimagevalue = $addalbumimage['image'];
            $addalbumimage->image = $addimagevalue.$image_name.',';

            if($removealbumimage->update() && $addalbumimage->update())
            {
                $data['status'] = true;
                $data['statusMsg'] = "Photo move to another album successfully";
            }
            else
            {
                $data['status'] = false;
                $data['statusMsg'] = "Photo move to another album fails";
            }
        }
        else
        {
            $data['status'] = false;
            $data['statusMsg'] = "requirement parameters are not proper";
        }
        return $data;
    }
    
    
    /* create new album */
    public function actionCreatealbum()
    {
        $userid = $_POST['login_user_id'];
        $result_security = SecuritySetting::find()->where(['user_id' => (string)$userid])->one();
        if($result_security)
        {
            $album_privacy = $result_security['view_photos'];
        }
        else
        {
            $album_privacy = 'Public';
        }
        $model = new LoginForm();
        $post = new PostForm();
        $date = time();
        if($_POST['login_email'])
        {
            if(isset($_POST) && !empty($_POST))
			{
                $album_title = $_POST['album_title'];
                if(empty($album_title))
				{
                    $album_title = 'Untitled Album';
                }
                $album_description = $_POST['album_description'];
                $album_place = $_POST['album_place'];
                $album_img_date = '';

                $post->post_status = '1'; 
                $post->album_title = ucfirst(strtolower($album_title));
                $post->post_text = $album_description;
                $post->album_place = $album_place;
                $post->album_img_date = $album_img_date;
                $post->post_type = 'text and image'; 
                $post->is_album = '1';
                $post->post_privacy = $album_privacy;
                $post->post_created_date = "$date";
                $post->is_deleted = '0';
                
                $post->post_user_id = (string)$userid;
                $post->post_ip = $_POST['REMOTE_ADDR_IP'];

                if(isset($_FILES) && !empty($_FILES))
				{
                    $imgcount = count($_FILES["imageFile1"]["name"]);
                    $img = '';
                    $im = '';
                    for($i=0; $i<$imgcount; $i++)
                    {
                        if(isset($_FILES["imageFile1"]["name"][$i]) && $_FILES["imageFile1"]["name"][$i] != "") 
                        {
                           $url = '../../frontend/web/uploads/';
                           $urls = '/uploads/';
                           $image_extn = end(explode('.',$_FILES["imageFile1"]["name"][$i]));
                           $rand = rand(111,999);
                           $img = $urls.$date.$rand.'.'.$image_extn.',';
                           move_uploaded_file($_FILES["imageFile1"]["tmp_name"][$i], $url.$date.$rand.'.'.$image_extn);
                           $im = $im . $img;
                        }
                    }
                    $post->image = $im;
                }
                $post->insert();
                $last_ablum_id = $post->_id;

                if($album_privacy != 'Public')
                {
                    $notification = new Notification();
                    $notification->post_id = "$last_ablum_id";
                    $notification->user_id = "$userid";
                    $notification->notification_type = 'post';
                    $notification->is_deleted = '0';
                    $notification->status = '1';
                    $notification->created_date = "$date";
                    $notification->updated_date = "$date";
                    $notification->insert();
                }
                $data['status'] = true;
                $data['statusMsg'] = "Album created successfuly";
                $data['album_id'] = $last_ablum_id;
            }
			else
			{
                $data['status'] = false;
                $data['statusMsg'] = "Required parameters unavailable";
            }
        }
		else
		{
                $data['status'] = false;
                $data['statusMsg'] = "Email is required";
        }
        return $data;
    }
	
	public function actionAddimages()
    {
        $pid = $_POST['album_id'];
        $update = new PostForm();
        $update = PostForm::find()->where(['_id' => $pid,'is_deleted' => '0','is_album' => '1'])->one();
        $image = $update['image'];
        $date = time();
		$data = array();
		if (isset($_FILES) && !empty($_FILES))
        {
            $imgcount = count($_FILES["imageFile1"]["name"]);
            $img = '';
            $im =  '';
            for ($i = 0;$i < $imgcount;$i++)
            {
                $name = $_FILES["imageFile1"]["name"][$i];
                $tmp_name = $_FILES["imageFile1"]["tmp_name"][$i];
                if (isset($name) && $name != "")
                {
                    $url = '../../frontend/web/uploads/';
                    $urls = '/uploads/';
                    $image_extn = end(explode('.',$_FILES["imageFile1"]["name"][$i]));
                    $rand = rand(111,999);
                    $img = $urls.$date.$rand.'.'.$image_extn.',';
                    move_uploaded_file($_FILES["imageFile1"]["tmp_name"][$i], $url.$date.$rand.'.'.$image_extn);
                    $im = $im . $img;
                } 
            }
            $ims = $image . $im;
            $update->image = $ims;
            if($update->update())
			{	
				$data['image'] = $im;
				$data['status'] = true;
				$data['statusMsg'] = "Image add successfully";
			} else {
				$data['status'] = false;
				$data['statusMsg'] = "Select Album";
			}
        }
		return $data;
    }
	
	
    /* get feed details */
    public function getfeeddetails($post_id)
	{
		$posts =  PostForm::find()->where(['_id' => $post_id ,'post_status' => '1'])->one();
		$post_array = array();
		$like_array = array();
		$comment_array = array();
		$feedback_array = array();
		$attachment_array = array();
		$tagedactornames_array = array();
		$pictures_array = array();
		$videos_array = array();
		$links_array = array();
		$shares_array = array();
		$j = 0;
		$k = 0;
		$i = 0;
		$likes = Like::find()->where(['post_id' => $post_id ,'status' => '1'])->all();

		foreach($likes as $docLike)
		{
			$like_array[$j] = UserForm::getUserDetails($docLike['user_id']); 
			$j++;
		}
        $loaded_comments = Comment::find()->with('user')->with('post')->where(['post_id' => "$post_id",'status' => '1','parent_comment_id' => '0'])->orderBy(['created_date'=>SORT_DESC])->all();
        foreach($loaded_comments as $docComment)
		{
            $comment_array[$k]['main_comment'] = $docComment;
            $comment_array[$k]['actor'] = UserForm::getUserDetails($docComment['user_id']);
            $pcid = (string) $docComment['_id'];
			$loaded_sub_comments = Comment::find()->with('user')->with('post')->where(['parent_comment_id' => "$pcid",'status' => '1'])->orderBy(['created_date'=>SORT_DESC])->all();
			$l = 0;
			$subcomment_array = array();
			foreach($loaded_sub_comments as $docSubComment)
			{
			$subcomment_array[$l]['sub_comment'] = $docSubComment;
			$subcomment_array[$l]['actor'] = UserForm::getUserDetails($docSubComment['user_id']); 
			$l++;
			}
			$comment_array[$k]['subcomment'] = $subcomment_array;
            $k++;
        }
		if(isset($posts['image']) && $posts['image'] != 'No Image')
		{
		   $images = rtrim($posts['image'], ",");
		   $images = explode(',',$images );
		   $p = 0;
		   for($m = 0; $m < count($images); $m++)
		   {
			   $pictures_array[$p]['urls'] = $images[$m];
			   $p++;
		    }
		}
		if($posts['post_type'] == 'link')
		{
		   $p = 0;
		   $links_array[$p]['urls'] = $posts['post_text'];
		}
		if(isset($posts['post_tags']) && $posts['post_tags'] != 'null')
		{
		   $tags = explode(',', $posts['post_tags']);
		   $p = 0;
		   for($m = 0; $m < count($tags); $m++)
		   {
			   $tagedactornames_array[$p]['tags'] = UserForm::getUserDetails($tags[$m]);
			   $p++;
		   }
		}
		if(isset($posts['parent_post_id']))
		{
		   $p = 0;
		   $pid = $posts['parent_post_id'];
		   $data[$i] = PostForm::find()->where(['_id'=>"$pid"])->one();
		   $uid = $data[$i]['post_user_id'];
		   $shares_array[$p]['parent_post'] = $data[$i];
		   $shares_array[$p]['actor'] = UserForm::getUserDetails($uid);
		}
                   
		$feedback_array['likers'] = $like_array;
		$feedback_array['comments'] = $comment_array;

		$post_array['main_post'] = $posts; 
		$post_array['actor'] = UserForm::getUserDetails($posts['post_user_id']);
		$post_array['feedback'] = $feedback_array;
		$attachment_array['pictures'] = $pictures_array;
		$attachment_array['videos'] = $videos_array;
		$attachment_array['links'] = $links_array;
		$attachment_array['shares'] = $shares_array;
		$post_array['attachment'] = $attachment_array;
		$post_array['tagedactornames'] = $tagedactornames_array;
        return $post_array;
    }
    
    /* edit feed */
    public function actionEditfeed() 
	{
		$model = new LoginForm();
        $data = array();
        $pid = isset($_POST['pid']) ? $_POST['pid'] : '';
        if ($pid != '') 
		{
            $date = time();
			$update = new PostForm();
            $update = PostForm::find()->where(['_id' => $pid])->one();
            $text = $update['post_text'];
            $image = $update['image'];

            if (isset($_FILES) && !empty($_FILES)) 
			{
                $imgcount = count($_FILES["imageFilepost"]["name"]);
                $img = '';
                $im =  '';
                for ($i =0;$i < $imgcount;$i++)
				{
                    $name = $_FILES["imageFilepost"]["name"][$i];
                    $tmp_name = $_FILES["imageFilepost"]["tmp_name"][$i];
                    if (isset($name) && $name != "") 
					{
                        $url = '../../frontend/web/uploads/';
                        $urls = '/uploads/';
                        move_uploaded_file($tmp_name, $url . $date . $name);
                        $img .= $urls . $date . $name . ',';
                    } 
                }
                $im = $image . $im . $img;
                $update->image = $im;
                if (isset($_POST['desc']) && !empty($_POST['desc'])) 
				{
                    $update->post_text = ucfirst(strtolower($_POST['desc']));
                    $update->post_type = 'text and image';
                }
				else if (isset($text) && !empty($text)) 
				{
                    $update->post_type = 'text and image';
                } 
				else
				{
                    $update->post_type = 'image';
                }
            } 
			else 
			{
                if (isset($_POST['desc']) && !empty($_POST['desc'])) 
				{
                    $update->post_text = ucfirst(strtolower($_POST['desc']));
                    if (isset($image) && !empty($image)) 
					{
                        $update->post_type = 'text and image';
                    }
					else 
					{
                        $update->post_type = 'text';
                    }
                }
            }
            $update->post_title = isset($_POST['title']) ? ucfirst(strtolower($_POST['title'])) : '';
            $update->post_tags = isset($_POST['posttags']) ? $_POST['posttags'] : '';
            $update->post_privacy = isset($_POST['post_privacy']) ? $_POST['post_privacy'] : '';
            $update->currentlocation = isset($_POST['edit_current_location']) ? $_POST['edit_current_location'] : '';
            $update->share_setting = isset($_POST['share_setting']) ? $_POST['share_setting'] : '';
            $update->comment_setting = isset($_POST['comment_setting']) ? $_POST['comment_setting'] : '';
            $update->post_created_date = "$date";
            $update->update();
            
            $data['status'] = true;
            $data['statusMsg'] = "Post updated successfully";
            $data['data'] = $this->getfeeddetails($pid);
        } 
		else 
		{
            $data['status'] = false;
            $data['statusMsg'] = "Enter post id";
        }
        return $data;
    }
    
    /* get album list*/
    public function actionGetalbumlist()
	{
        $wall_user_id = $_POST['user_id'];
        $user_id = $_POST['login_user_id'];
        $data = array();
        $albums = PostForm::getAlbumsAPI($wall_user_id,$user_id);
        if($albums)
		{
            $data['status'] = true;
            $data['statusMsg'] = "Album list available";
            $data['data'] = $albums;
        }
		else
		{
            $data['status'] = false;
            $data['statusMsg'] = "Album not available";
        }
        return $data;
    }
    
    /* edit comment of post */
    public function actionEditcomment()
    {
        $comment_id = $_POST['comment_id'];
        $edit_comment = str_replace(array("\n","\r\n","\r"), '', $_POST['edit_comment']);
        $data = array();
        if(!empty($comment_id) && isset($comment_id) && !empty($edit_comment) && isset($edit_comment))
        {
            $user_id = $_POST['user_id'];
            $date = time();
            $model = new Comment();
            $query = Comment::find()->where(['_id' => $comment_id, 'user_id' => $user_id])->one();
            if($query)
			{
                $query->comment = $edit_comment;
                $query->updated_date = $date;
                if($query->update())
                {
                    $data['status'] = true;
                    $data['statusMsg'] = "comment uppdated successfully";
                }
				else
				{
                    $data['status'] = false;
                    $data['statusMsg'] = "comment can't update";
                }
            }
			else
			{
                $data['status'] = false;
                $data['statusMsg'] = "no data found for update";
            }
        }
		else
		{
            $data['status'] = false;
            $data['statusMsg'] = "comment id not selected";
        }
        return $data;
    }
    
    /* hide & delete post comment */
   public function actionSetcommentoption() 
   {
        $data = array();
        if (isset($_POST['comment_id']) && !empty($_POST['comment_id'])) 
		{
            $comment_id = $_POST["comment_id"];
            $user_id = $_POST['user_id'];
            if($_POST['option'] == 'hide')
			{
                $hidecomment = new HideComment();
                $userexist = HideComment::find()->where(['user_id' => $user_id])->one();
                if ($userexist) 
				{
                    if (strstr($userexist['comment_ids'],$comment_id)) 
					{

                    } 
					else 
					{
                        $hidecomment = HideComment::find()->where(['user_id' => $user_id])->one();
                        $hidecomment->comment_ids = $userexist['comment_ids'] . ',' . $comment_id;
                        if ($hidecomment->update()) 
						{
                            $data['status'] = true;
                            $data['statusMsg'] = "comment hide";
                        } 
						else
						{
                            $data['status'] = false;
                            $data['statusMsg'] = "comment can't hide";
                        }
                    }
                } 
				else 
				{
                    $hidecomment->user_id = $user_id;
                    $hidecomment->comment_ids = $comment_id;
                    if ($hidecomment->insert()) 
					{
                        $data['status'] = true;
                        $data['statusMsg'] = "comment hide";
                    } 
					else 
					{
                        $data['status'] = false;
                        $data['statusMsg'] = "comment can't hide";
                    }
                }
            }
			else if($_POST['option'] == 'delete')
			{
                $model = new Comment();
                $query = Comment::find()->where(['_id' => $comment_id])->one();
                if ($query) 
				{
                    $deletecomment = new Comment();
                    $deletecomment = Comment::find()->where(['_id' => $comment_id])->one();

                    $deletecomment->status = '0';
                    if ($deletecomment->update()) 
					{
                        $data['status'] = true;
                        $data['statusMsg'] = "comment deleted";
                    } 
					else 
					{
                        $data['status'] = false;
                        $data['statusMsg'] = "comment can't deleted";
                    }
                } 
				else 
				{
                    $data['status'] = false;
                    $data['statusMsg'] = "comment can't deleted";
                }
            }
        }
		return $data;
    }
	
	//Send suggest Connect request
	public function actionSuggestconnect()
    {
		$fid = (string) $_POST['connectid'];
		$sto = (string) $_POST['suggestto'];
		$user_id = (string) $_POST['user_id'];
		if (isset($fid) && !empty($fid) && isset($sto) && !empty($sto))
		{
			$data = array();
			$suggest = new SuggestConnect();
			$suggestexist = SuggestConnect::find()->where(['user_id' => $user_id,'connect_id' => $fid,'suggest_to' => $sto,'status' => '0'])->one();
			if (!$suggestexist)
			{
				$date = time();
				$suggest->user_id = $user_id;
				$suggest->connect_id = $fid;
				$suggest->suggest_to = $sto;
				$suggest->created_at = "$date";
				$suggest->status = '0';
				$suggest->insert();
				$data['status'] = '1';
				$data['msg'] = 'Suggetion sent';
			}
			else
			{
				$data['status'] = '2';
				$data['msg'] = 'Already sent';
			}
		}
		else
		{
			$data['status'] = '0';
			$data['msg'] = 'Suggetion failed';
		}
        return $data;
    }
	
	//connect list to suggest connect
	public function actionSuggestconnectlist()
    {
        $suserid = (string)$_POST['user_id'];
        $fid = (string)$_POST['connectid'];
        $eml_id = Connect::getuserConnections($suserid);
		$data = array();
		$suggest_connect = array();
        if (!empty($eml_id))
        {
			$z = 0;
			foreach ($eml_id as $val) 
			{
				$guserid = (string)$val['userdata']['_id'];
				$suggestdp = $this->getimageAPI($guserid,'photo');
				$is_connect = Connect::find()->where(['from_id' => $fid,'to_id' => $guserid,'status' => '1'])->one();
				if(($fid != $guserid) && !$is_connect)
				{
					if($is_connect){ $fri = "Connect";}else{ $fri = "Suggest Connect";}
					$suggest_connect[$z]['id'] = $guserid;
					$suggest_connect[$z]['image'] = $suggestdp;
					$suggest_connect[$z]['fullname'] = $val['userdata']['fullname'];
					$suggest_connect[$z]['button'] = $fri;
					$z++;
				}
			}
			$data['connect'] = $suggest_connect;
			$data['status'] = '1';
			$data['msg'] = "Suggetion connections.";
		} else { 
			$data['connect'] = $suggest_connect;
			$data['status'] = '0';
			$data['msg'] = "You both have same connections.";
        }
		return $data;
    }
	
	//Suggest connect list
	public function actionGetsuggestconnect()
	{
		$user_id = $_POST['user_id'];
		$suggetion_requests = SuggestConnect::find()->where(['suggest_to' => "$user_id",'status' => '0'])->all();
		$data = array();
		$suggest_connections = array();
		if(!empty($suggetion_requests) && count($suggetion_requests)>0) 
		{
			$i =0;
			foreach($suggetion_requests as $suggetion_request)
			{ 
				$sugby = LoginForm::find()->where(['_id' => $suggetion_request['user_id']])->one();
                $frdetails = LoginForm::find()->where(['_id' => $suggetion_request['connect_id']])->one();
                $frnd_img = $this->getimageAPI($frdetails['_id'],'thumb');
				$mutual_ctr = Connect::mutualconnectcountAPI($frdetails['_id'],$user_id);
				$suggest_connections[$i]['id'] = (string)$sugby['_id'];
				$suggest_connections[$i]['image'] = $frnd_img;
				$suggest_connections[$i]['mutual'] = $mutual_ctr;
				$suggest_connections[$i]['fullname'] = $frdetails['fullname'];
				$suggest_connections[$i]['suggestname'] = $sugby['fullname'];
				$suggest_connections[$i]['city'] =	$frdetails['city'];
				$i++;
            }
			$data['suggest_connect'] = $suggest_connections;
			$data['status'] = '1';
			$data['msg'] = "Suggetion connections.";	
		} else {
			$data['suggest_connect'] = $suggest_connections;
			$data['status'] = '0';
			$data['msg'] = "No any connections";
        }
		return $data;	
	}
	
	public function actionUsermainfeed()
	{
        $user_id = $_POST['user_id'];
		$result = array();
        if(!isset($_POST['pagenumber']))
		{
            $page_number = 1;
        }
		else
		{
            $page_number = $_POST['pagenumber'];
        }
		$count_post = PostForm::getUserConnectionsPostsAPICount($user_id);
		$num_rec_per_page=20;
		$start_from = ($page_number-1) * $num_rec_per_page;
		$total_pages = ceil($count_post / $num_rec_per_page); 
		$posts =  PostForm::getUserPostAPI($user_id,$start_from,$num_rec_per_page);
		if(count($posts) > 0)
		{
			$post_array = array();
			$i = 0;
			foreach ($posts as $doc)
			{
				$like_array = array();
				$comment_array = array();
				$feedback_array = array();
				$attachment_array = array();
				$tagedactornames_array = array();
				$pictures_array = array();
				$videos_array = array();
				$links_array = array();
				$shares_array = array();
				$j = 0;
				$k = 0;
			    $post_id = (string) $doc['_id'];
			    $likes = Like::find()->where(['post_id' => $post_id ,'status' => '1'])->all();
				foreach($likes as $docLike)
			    {
					$like_array[$j] = UserForm::getUserDetails($docLike['user_id']); 
					$j++;
				}
				$loaded_comments = Comment::find()->with('user')->with('post')->where(['post_id' => "$post_id",'status' => '1','parent_comment_id' => '0'])->orderBy(['created_date'=>SORT_DESC])->all();
				foreach($loaded_comments as $docComment)
				{
					$comment_array[$k]['main_comment'] = $docComment;
					$comment_array[$k]['actor'] = UserForm::getUserDetails($docComment['user_id']);
					$pcid = (string) $docComment['_id'];
					$loaded_sub_comments = Comment::find()->with('user')->with('post')->where(['parent_comment_id' => "$pcid",'status' => '1'])->orderBy(['created_date'=>SORT_DESC])->all();
					$l = 0;
					$subcomment_array = array();
					foreach($loaded_sub_comments as $docSubComment)
					{
						$subcomment_array[$l]['sub_comment'] = $docSubComment;
						$subcomment_array[$l]['actor'] = UserForm::getUserDetails($docSubComment['user_id']); 
						$l++;
					}
					$comment_array[$k]['subcomment'] = $subcomment_array;
					$k++;
				}
				if(isset($doc['image']) && $doc['image'] != 'No Image')
				{
					$images = rtrim($doc['image'], ",");
					$images = explode(',',$images );
					$p = 0;
					for($m = 0; $m < count($images); $m++){
					   $pictures_array[$p]['urls'] = $images[$m];
					   $p++;
					}
				}
				if($doc['post_type'] == 'link')
				{
				   $p = 0;
				   $links_array[$p]['urls'] = $doc['post_text'];
				}
				if(isset($doc['post_tags']) && $doc['post_tags'] != 'null')
				{
					$tags = explode(',', $doc['post_tags']);
					$p = 0;
					for($m = 0; $m < count($tags); $m++)
					{
					   $tagedactornames_array[$p]['tags'] = UserForm::getUserDetails($tags[$m]);
					   $p++;
					}
				}
				if(isset($doc['parent_post_id']))
				{
					$p = 0;
					$pid = $doc['parent_post_id'];
					$data[$i] = PostForm::find()->where(['_id'=>"$pid"])->one();
					$uid = $data[$i]['post_user_id'];
					$shares_array[$p]['parent_post'] = $data[$i];
					$shares_array[$p]['actor'] = UserForm::getUserDetails($uid);
				}
				if(!empty($doc['share_by']))
				{
					$shares_count = substr_count( $doc['share_by'], ",");
				}
				else
				{
					$shares_count = 0;
				}
				
				// Save post
				$savestatus = new SavePost();
                $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => "$post_id"])->one();
                $savestatuscalue = $savestatus['is_saved'];
				
				//Hide Post
				$hidestatus = new HidePost();
                $hidestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_ids' => "$post_id"])->one();
                if($hidestatus)
				{
					$hidestatuss = "1";
				}
				else
				{
					$hidestatuss = "0";
				}
				$feedback_array['likers'] = $like_array;
				$feedback_array['comments'] = $comment_array;
				$attachment_array['pictures'] = $pictures_array;
				$attachment_array['videos'] = $videos_array;
				$attachment_array['links'] = $links_array;
				$attachment_array['shares'] = $shares_array;
				$attachment_array['shares_count'] = $shares_count;
				$post_array[$i]['feedback'] = $feedback_array;
				$post_array[$i]['attachment'] = $attachment_array;
				$post_array[$i]['main_post'] = $doc; 
				$post_array[$i]['actor'] = UserForm::getUserDetails($doc['post_user_id']);
				$post_array[$i]['tagedactornames'] = $tagedactornames_array;
				$post_array[$i]['savestatus'] = $savestatuscalue;
				$post_array[$i]['hidestatus'] = $hidestatuss;
				$i++;
			}
		}
		else
		{
			$post_array = array();
		}
        // List of User Feed End 
        $result['pagenumber'] = $page_number;
		$result['total_pages'] = $total_pages;
		$result['feed_list'] = $post_array;
		return $result;
    }
	
	/*Recently join*/
	
	public function actionRecentlyjoined()
	{
		$data = array();
		$user_data  = array();
		$users = LoginForm::getrecentlyjoined();
		$z = 0;
		foreach ($users as $user) 
		{
			$userid = (string)$user['_id'];
			$dp = $this->getimageAPI($userid,'photo');
			$user_data[$z]['id'] = $userid;
			$user_data[$z]['image'] = $dp;
			$user_data[$z]['fullname'] = $user['fullname'];
			$z++;
		}
		$data['users'] = $user_data; 
		return $data;
	}
	
	public function actionPeopleyouknow()
	{
		$data = array();
		$user_data  = array();
		$user_id = $_POST['user_id'];
		$users = Connect::userlistFirstfive($user_id);
		$z = 0;
		foreach ($users as $user) 
		{
			$userid = (string)$user['_id'];
			$dp = $this->getimageAPI($userid,'photo');
			$user_data[$z]['id'] = $userid;
			$user_data[$z]['image'] = $dp;
			$user_data[$z]['fullname'] = $user['fullname'];
			$z++;
		}
		$data['users'] = $user_data; 
		return $data;
		
	}
}