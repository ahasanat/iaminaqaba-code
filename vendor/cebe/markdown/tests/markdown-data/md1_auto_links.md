Link: <https://example.com/>.

With an ampersand: <https://example.com/?foo=1&bar=2>

* In a list?
* <https://example.com/>
* It should.

> Blockquoted: <https://example.com/>

Auto-links should not occur here: `<https://example.com/>`

	or here: <https://example.com/>