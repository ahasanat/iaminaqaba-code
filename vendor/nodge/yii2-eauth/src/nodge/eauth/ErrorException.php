<?php
/**
 * ErrorException class file.
 *
 * @author Maxim Zemskov <nodge@yandex.ru>
 * @link https://github.com/Nodge/yii2-eauth/
 * @license https://www.opensource.org/licenses/bsd-license.php
 */

namespace nodge\eauth;

class ErrorException extends \yii\base\ErrorException {

}