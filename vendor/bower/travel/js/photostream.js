$(document).ready(function() {
	justifiedGalleryinitialize();
	lightGalleryinitialize();
});

$(window).bind("load", function() {
   $travellers = $('.allow-gallery').length;
   if($travellers>0) {
      $('.lt').html('('+$travellers+')');
   } else {
      $('.lt').html('');
   }
});

		/* Noor JS */
$(document).ready(function(){ 
   $w = $(window).width();
   if ( $w > 739) {      
      $(".places-tabs .sub-tabs li a").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".tabs.icon-menu.tabsnew li a").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".mbl-tabnav").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
      $(".clicable.viewall-link").click(function(){
         $("body").removeClass("remove_scroller");
      }); 
   } else {
      $(".places-tabs .sub-tabs li a").click(function(){
         $("body").addClass("remove_scroller");
      }); 
      $(".clicable.viewall-link").click(function(){
         $("body").addClass("remove_scroller");
      });         
      $(".tabs.icon-menu.tabsnew li a").click(function(){
         $("body").addClass("remove_scroller");
      }); 
      $(".mbl-tabnav").click(function(){
         $("body").removeClass("remove_scroller");
      });
   }

   $(".header-icon-tabs .tabsnew .tab a").click(function(){
      $(".bottom_tabs").hide();
   });

   $(".places-tabs .tab a").click(function(){
      $(".top_tabs").hide();
   });

   $('.footer-section').css('left', '0');
   $w = $(window).width();
   if($w <= 768) {
      $('.main-footer').css({
         'width': '100%',
         'left': '0'
      });
   } else {
      var $_I = $('.places-content.places-all').width();
      var $__I = $('.places-content.places-all').find('.container').width();

      var $half = parseInt($_I) - parseInt($__I);
      $half = parseInt($half) / 2;

      $('.main-footer').css({
         'width': $_I+'px',
         'left': '-'+$half+'px'
      });
   }
});

$(window).resize(function() {
   // footer work for places home page only
   if($('#places-all').hasClass('active')) {
      $('.footer-section').css('left', '0');
      $w = $(window).width();
      if($w <= 768) {
         $('.main-footer').css({
            'width': '100%',
            'left': '0'
         });
      } else {
         var $_I = $('.places-content.places-all').width();
         var $__I = $('.places-content.places-all').find('.container').width();

         var $half = parseInt($_I) - parseInt($__I);
         $half = parseInt($half) / 2;

         $('.main-footer').css({
            'width': $_I+'px',
            'left': '-'+$half+'px'
         });
      }
   }
});

$(document).on('click', '.tablist .tab a', function(e) {
   $href = $(this).attr('href');
   $href = $href.replace('#', '');
   $('.places-content').removeClass().addClass('places-content '+$href);
   $this = $(this);
});
