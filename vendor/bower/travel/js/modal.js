var $post_privacy_array = ['Private', 'Connections','Custom', 'Public', 'Enabled', 'Disabled'];
var person = [];
var personSearch = [];
var testArray = [];
var addUserForTag = [];
var addUserForShareWith = [];
var userwall_tagged_users = [];
var userwall_tagged_usersTemp = [];
var addUserForShareWithTemp = [];
var whoisopeninblock = '';
var map;
var pos;
var location_name = "";
var lname = "";
var t = 0;
var google;
var $isMapLocationId = '';
var $locationLabel = '';
var isNormalpost = 'no';
var isAlbumphotobox = 'no';
var selectedpostid = '';
var $lazyhelpcountuploadstuff = 0;
var isnewwithmodal = 'no';
var selectStartPoint = '';
var starsArray = ['1', '2', '3', '4', '5', 1, 2, 3, 4, 5];
var photoUpload = [];

$(document).on('click', '#compose_uploadphotomodal .check-image', function () {
    setTimeout(function(){
        $odknds = $('#compose_uploadphotomodal').find('.selected_photo_text');
        $totalcheckedimages = $('#compose_uploadphotomodal').find('.check-image.active-class').length;
        if($totalcheckedimages <=0) {
            $($odknds).html('0 photos selected');
        } else if ($totalcheckedimages == 1) {
            $($odknds).html('1 photo selected');
        } else {
            $($odknds).html($totalcheckedimages+' photos selected');
        }
    },600);
});
$(document).ready(function () {

    setModelMaxHeight();
    setTimeout(function(){
        gridBoxImgUINew('.gridBox127');
    },400);
    datepikerfndestoy();

    $(document).on('click', '.compose_visitcountryAction', function () {
        $('#compose_visitcountry').modal('open');
    });

    $(document).on('click', '.compose_iwasinCountryAction', function () {
        $('#compose_iwasincountry').modal('open');
    });

    // js for sticky navbar
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 280) {
            $('.index-page .sticky-nav').css('display', 'block');
            $('.index-page .header-section .header-themebar').css('box-shadow', 'none');
        }
        else {
            $('.index-page .sticky-nav').css('display', 'none');
            $('.index-page .header-section .header-themebar').css('box-shadow', '0 1px 8px rgba(0,0,0,.3)');
        }
    });

    // js for header box-shadow
    $("li.rem-shadow-none").click(function(){
        $(".wallpage .header-section .header-themebar.innerpage").addClass("box-shadow-none");
    });
    
    $(".wall-tabs .tabs li").not( document.getElementsByClassName("rem-shadow-none")).click(function(){
      $(".wallpage .header-section .header-themebar.innerpage").removeClass("box-shadow-none");
    });

    // js to add class 'waves-effect' and 'waves-effect waves-light' elements
    $("ul.dropdown-content li, a.pa-like, a.pa-share, a.pa-comment, a.pa-publish, a.done_btn, a.btn-invite-close, a.cross, #account_setting li a").addClass("waves-effect");
    $("a.popup-window, a.btn-floating, a.homebtn").addClass("waves-effect waves-light");

    // js for active class to check upload images
    $(document).on('click', '.check-image', function() {
        $(this).toggleClass('active-class');
        return false;
    });

    // js for dropdown initialization
    $('.dropdown .dropdown-button, .public_dropdown_container').dropdown({
      inDuration: 500,
      outDuration: 225,
      constrainWidth: true, // Does not change width of dropdown to that of the activator
      hover: false, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'right', // Displays dropdown with edge aligned to the left of button
      stopPropagation: true // Stops event propagation
    });
    
    $('.collapsible').collapsible(); 

    $('.dropdown .nothover').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: true, // Does not change width of dropdown to that of the activator
        hover: false, // Activate on hover
        gutter: 0, // Spacing from edge
        belowOrigin: false, // Displays dropdown below the button
        alignment: 'right', // Displays dropdown with edge aligned to the left of button
        //stopPropagation: true // Stops event propagation
    }); 

    $('.timepicker').pickatime({
        default: 'now', // Set default time: 'now', '1:30AM', '16:30'
        fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
        twelvehour: true, // Use AM/PM or 24-hour format
        donetext: 'OK', // text for done-button
        cleartext: 'Clear', // text for clear-button
        canceltext: 'Cancel', // Text for cancel-button
        autoclose: false, // automatic close timepicker
        ampmclickable: true, // make AM PM clickable
        container:'body',
        aftershow: function(){} //Function for after opening timepicker
    });
 
    //**************FUN for modal initialization **************/
    
    //FUN for post comment modal
    $('#comment_modal_xs').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
        },
        complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close

    });

    $('.compose_tool_box').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',    
        ready: function (modal, trigger) {
            addUserForTag = [];
            addUserForTagTemp = [];
            addUserForShareWithTemp = [];
            $('select').material_select();   
            initDropdown();

            $("body").addClass("modal_open");
            $(".selected_person_text").html(addUserForTag.length+" person selected");
            
            setTimeout(function(){
                initNiceScroll(".nice-scroll");
                setModelMaxHeight();                
            },400);            
        },
        complete: function (modal) { 
            $("body").removeClass("modal_open");
            addUserForTag = [];
            addUserForTagTemp = [];
            addUserForShareWithTemp = [];
            $(".selected_person_text").html(addUserForTag.length+" person selected");
            if(modal[0]['id'] == 'composeeditpostmodal') {
                $('#composeeditpostmodal').html('');
            }
        }
    });   
    
    //modal for share post
    $('.sharepost_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            shareongroupwallArrayTemp = [];
            shareongroupwallArray = [];
            customArray = [];
            customArrayTemp = [];
            $("body").addClass("modal_open");
            setTimeout(function(){
                fixImageUI("popup-images");
                initNiceScroll(".nice-scroll");                     
                //var modal_height = $(".modal.open").visibleHeight();
                setModelMaxHeight();        
                
                initDropdown();
            },400);
        },
        complete: function () { 
            shareongroupwallArrayTemp = [];
            shareongroupwallArray = [];
            customArray = [];
            customArrayTemp = [];
            $("body").removeClass("modal_open"); 
            $('#sharepostmodal').html('');
            setTimeout(function(){
                initDropdown();
            },400);
        }
    });
    //modal for preference
    $('.preference_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
            setTimeout(function(){
                initDropdown();
            },400);         
        },
        complete: function () { 
            $("body").removeClass("modal_open");
        } // Callback for Modal close
    });
    
    //login modal
    $('#login_modal').modal({
        dismissible: true,
        opacity: .5,
        inDuration: 500,
        outDuration: 400,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
        },
        complete: function () { $("body").removeClass("modal_open"); }  // Callback for Modal close
    });

    $('#receiveCallBox').modal({
        dismissible: true,
        opacity: .5,
        inDuration: 500,
        outDuration: 400,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
        },
        complete: function () { $("body").removeClass("modal_open"); }  // Callback for Modal close
    });

    // gift list modal
    $('.gift_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
            setModelMaxHeight();
        },
        complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close

    });
    // use gift modal
    $('.usegift_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
            setModelMaxHeight();
        },
        complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close

    });
    
    //fUN for custom md modal
    $('.custom_md_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            setTimeout(function(){
                initNiceScroll(".nice-scroll");                     
                //var modal_height = $(".modal.open").visibleHeight();
                setModelMaxHeight();   
                customArray = [];
                customArrayTemp = [];             
            },400);  
        },
        complete: function (modal, trigger) {
            customArray = [];
            customArrayTemp = [];
            if(modal.attr('id') == 'add-photo-popup') {
                storedFiles = [];
                $('#add-photo-popup').find('.img-row').html('<div class="img-box"> <div class="custom-file addimg-box"> <div class="addimg-icon"> <i class="zmdi zmdi-plus zmdi-hc-lg"></i> </div> <input class="upload custom-upload remove-custom-upload" title="Choose a file to upload" required="" data-class=".post-photos .img-row" multiple="true" type="file"> </div> </div>');
            }
        }
    });

    
    // add person modal
    $('#compose_addpersonAction_as_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',    
        ready: function (modal, trigger) {
            addUserForAccountSettings();
            $('#compose_addpersonAction_as_modal').find('#search_box').val('');
            addUserForAccountSettingsArrayTemp = addUserForAccountSettingsArray.slice();
        },
        complete: function () { 
            $('#compose_addpersonAction_as_modal').find('.person_box').html('');                   
        }
    });

   

     //modal for compose tool box
    $('.tbpost_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
            initDropdown();
            
            if(modal[0]['id'] == 'upload-gallery-popup') {
                userwall_tagged_users = [];
                userwall_tagged_usersTemp = [];
                storedFiles = [];
                storedFilesExsting = [];
                
                $('select').material_select();   
                initDropdown();

                $("body").addClass("modal_open");
                $(".selected_person_text").html(userwall_tagged_users.length+" person selected");

                setTimeout(function(){
                    initNiceScroll(".nice-scroll");
                    setModelMaxHeight();                
                },400);
            }
        },
        complete: function (modal) { 
            $("body").removeClass("modal_open");
            totalPerson=[]; 
            $(".selected_person_text").html(totalPerson.length+" person selected");

            if(modal[0]['id'] == 'upload-gallery-popup') {
                userwall_tagged_users = [];
                userwall_tagged_usersTemp = [];
                storedFiles = [];
                storedFilesExsting = [];
                customArray = [];
                $(".selected_person_text").html(userwall_tagged_users.length+" person selected");
            }

            if($('.lg-outer').length) {
                $("body").css("overflow-y", "hidden");
            }
        }
    });

    $('#Complete_loged').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%'
    });

    /*$(function() {
      $(window).scroll(function() {
        var mass = Math.max(2, 4-0.01*$(this).scrollTop()) + 'rem';
        $('.expandable-text').css({'font-size': mass, 'line-height': mass});
      });
    });*/

    function addUserForAccountSettings() {
        $.ajax({ 
            url: "?r=site/add-user-for-account-settings", 
            type: 'POST',
            data: {ids: addUserForAccountSettingsArray},
            success: function (data) {
                var result = $.parseJSON(data);
                if(result.status != undefined && result.status == true) {
                    var content = result.html;
                    var userCount = addUserForAccountSettingsArray.length;
                    if(userCount>1) {
                        var label = userCount + ' users selected';
                        $('#compose_addpersonAction_as_modal').find('#chk_person_done_ss').removeClass('focoutTRV03');
                    } else {
                        var label = userCount + ' user selected';
                        $('#compose_addpersonAction_as_modal').find('#chk_person_done_ss').addClass('focoutTRV03');
                    }
                    $('#compose_addpersonAction_as_modal').find('.selected_photo_text').html(label);
                    $('#compose_addpersonAction_as_modal').find('.person_box').html(content);
                } else {
                    $('#compose_addpersonAction_as_modal').find('.selected_photo_text').html('0 user selected.');
                    $('#compose_addpersonAction_as_modal').find('#chk_person_done_ss').addClass('focoutTRV03');
                    getnolistfound('norecordfoundtag');
                }
            }
        });
    }
    
    // add person modal - share with
    $('#compose_addperson_sharewith').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',    
        ready: function (modal, trigger) {
            addUserForShareWithfun();
            addUserForShareWithTemp = addUserForShareWith.slice();
        },
        complete: function () { 
            addUserForShareWithTemp = [];
            $('#compose_addperson_sharewith').find('.selected_photo_text').html('0 user selected');                   
            $('#compose_addperson_sharewith').find('#search_box').val('');                   
            $('#compose_addperson_sharewith').find('.person_box').html('');                   
        }
    });
    
    //fUN for custom md modal
    $('.item_members').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
        },
        complete: function () {}
    });
    //FUN for manage modal
    $('.manage-modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
        },
        complete: function () { $("body").removeClass("modal_open"); } // Callback for Modal close
    });
    //modal for report abuse
    $('.reportabuse_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open");
            setTimeout(function(){
                setModelMaxHeight();                
            },400);
        },
        complete: function () { 
            $("body").removeClass("modal_open");
            
        } // Callback for Modal close
    });
    
    //discard modal
    $('.discard_md_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 400,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            if($(".main-content").hasClass("general-page") && !$(".main-content").hasClass("generaldetails-page")){
                $('.discard_md_modal').removeAttr("data-id");
            }
            $("body").addClass("modal_open");           
        },
        complete: function () { 
            $("body").removeClass("modal_open"); 
            $('#compose_discard_modal').removeClass('notallclose');
            if($('.lg-outer').length) {
                $("body").css("overflow-y", "hidden");
            }
        } 
    });
 
    $('#payment-popup').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 400,
        startingTop: '50%',
        endingTop: '50%',
        ready: function (modal, trigger) {
            $("body").addClass("modal_open GENPAY_popupbody");
            setModelMaxHeight();
        },
        complete: function () { $("body").removeClass("modal_open GENPAY_popupbody"); } // Callback for Modal close
    });
    
    // add person modal
    $('#compose_addperson').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',    
        ready: function (modal, trigger) {
            addUserForTagfun();
            $('#compose_addperson').find('.selected_photo_text').html('0 user selected');                   
            $('#compose_addperson').find('#search_box').val('');                   
            addUserForTagTemp = addUserForTag.slice();
        },
        complete: function () { 
            addUserForTagTemp = [];
            $('#compose_addperson').find('.person_box').html('');                   
        }
    });

        //inner compose tool box modal
    $('.compose_inner_modal').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700, 
        startingTop: '50%', 
        endingTop: '50%',    
        ready: function (modal) {
            if (modal.hasClass('compose_uploadphotomodal')) {
                fillcheckedfromuploadstuff();
                calluploadstuffmore();
            }

            if (modal.hasClass('map_modal')) {
                var $value = $('#compose_mapmodal').find('#pac-input').val();
                $custSelector = $('.modal.open').find('id').prevObject[0]['id'];
                if($value != '') {
                    if($('#'+$custSelector).find('#selectedlocation').find('.tagged_location_name').length) {
                        $('#'+$custSelector).find('#selectedlocation').find('.tagged_location_name').html($value);
                    } else if($('#'+$custSelector).find('#placelocationsearch').length) {
                        $('#'+$custSelector).find('#placelocationsearch').val($value);
                    } else {
                        $html = '<label id="selectedlocation"><div class="location_div compose_mapmodalAction"><span class="tagged_location"></span><span class="tagged_location_name">'+$value+'</span></div><a href="javascript:void(0)" class="removelocation"><i class="mdi mdi-close"></i></a></label>';
                        $('#'+$custSelector).find('#selectedlocation').html($html);
                    }
                } else {
                    $('#'+$custSelector).find('#selectedlocation').html('');
                }

                $('.search_container .close_span').show();
                //$('.search_container .done_btn').show();
                $('.search_container .back_arrow').hide();
                $('.search_span').show();
                $('.empty_input').hide();
            }
        },
        complete: function (modal) { 
            if (modal.hasClass('compose_uploadphotomodal')) {
                mainFeedPostBtn();
            }
        }
    });

    $('#userwall_tagged_users').modal({
        dismissible: false,
        opacity: .5,
        inDuration: 500,
        outDuration: 700,
        startingTop: '50%',
        endingTop: '50%',    
        ready: function (modal, trigger) {
            userwall_tagged_users_fn();
            $('#userwall_tagged_users').find('.selected_photo_text').html('0 user selected');                   
            $('#userwall_tagged_users').find('.search_box').val('');                   
            userwall_tagged_usersTemp = userwall_tagged_users.slice();
        },
        complete: function () { 
            userwall_tagged_usersTemp = [];
            $('#userwall_tagged_users').find('.person_box').html('');                   
        }
    });
    //**************FUN for material dropdown**************/
    initDropdown();

    //share it popup - events/groups
    $(document).on("click",".share-it",function (event) {
        shareInfoPost();
    });
    
    //keep button
    $(document).on("click",".modal_keep",function (event) {
        $(this).parents(".modal.open").modal("close");
        removeDisClasses();
    });
    //discard button 
    $(document).on("click",".modal_discard",function (event) {
        if($(this).parents(".modal").hasClass("dis_createitem"))
            CollectionClearPost();
        if($(this).parents(".modal").hasClass("dis_postpopup"))
            clearPost();
        $allclose = true;
        if($('.modal.open').hasClass('notallclose')) {
            $allclose = false;
        }

        $(this).parents(".modal.open").modal("close");
        setTimeout(function() {
            if($allclose) {
                $(".modal.open").modal("close");      
            }      
        },400);
        removeDisClasses();
    });
    
    //compose box checkbox for 3dots
    /*$(document).on("click",".settings-icon .dropdown-button + .dropdown-content",function (event) {
        event.stopPropagation();
    });
*/ 
    // bind setPostBtnStatus
    $(document).on("keypress keyup",".comment_textarea",function () {
        setPostBtnStatus();
    });
    $(document).on("click",".comment_modal_xs .cancel_poup",function () {
        $('.comment_modal_xs').modal('close');
    });
        
    $(document).on("click",".readlink",function () {        
        var sparent=$(this).parents(".para-section");
        sparent.find(".para").toggleClass("opened");
        
        var thisObj = $(this);
        var itext = thisObj.html();
        if(itext == "Read More"){ 
            itext="Read Less";
        }else{ 
            itext="Read More";
        }
        setTimeout(function(){                      
            thisObj.html(itext);            
        },200);
    });
    
    $(document).on('click', '#composetoolboxAction,.composetoolboxAction', function () {
        if($(this).hasClass('checkuserauthclassnv')) {
            checkuserauthclassnv();
        } else if($(this).hasClass('checkuserauthclassg')) {
            checkuserauthclassg();
        } else {
            $.ajax({
                url: '?r=site/new-post-html', 
                success: function(data){
                    $('#compose_tool_box').html(data);
                    $('#compose_tool_box').modal('open');
                }
            });
        }
    }); 
        
    $(document).on('click', '#new_post_btn', function () {
        $.ajax({
            url: '?r=site/new-post-html',
            success: function(data){
                $('#compose_tool_box').html(data);
                $('#compose_tool_box').modal('open');
            }
        });
    });

/*    $(document).on('click', '.suggest-connections', function () {
        $('#suggest-connections').modal('open');
    });*/
    
    $(document).on('click', '.location_div', function () {
        currentlocation();
        $('#compose_mapmodal').modal('open');
        var select_val = $('#selectedlocation').find('.tagged_location_name').text();
        $('#pac-input').val(select_val);
    });
     
    $(document).on('click', '.compose_mapmodalAction1', function () {
        currentlocation();
        $('#compose_mapmodal1').modal('open');
    });
 
    $(document).on('click', '.edit_button_business', function () {
        var pageid = $(this).attr('data-pageid');
        if(pageid) {
            $.ajax({
                url: '?r=page/fetchbusinessbutton',
                type: 'POST',
                data:{pageid},
                success: function(data){
                    $('#edit_button_business').html(data);
                    setTimeout(function() {
                        setAdditionalInfo(); 
                        $('#edit_button_business').modal('open');
                        initDropdown();
                    }, 400);
                }
            });
        }
    });

    $(document).on('click', '.payment_popup_ads', function () {
        setVars();
    });

    $(document).on('change', '.compose_camera', function () {
        $('#compose_camera').modal('open');
    });

    //Serch location in compose tool box modal
    $(document).on('keypress', '#pac-input', function (e) {
        currentlocation();
    });
    //FUN for location modal in compose tool box modal
    $(document).on('click focus', '.compose_mapmodalAction', function () {
        if($(this).parents('.modal.open').find('#selectedlocation').find('.tagged_location_name').length) {
            var location = $(this).parents('.modal.open').find('#selectedlocation').find('.tagged_location_name').html();
            $('#compose_mapmodal').find('#pac-input').val(location);
            currentlocation(location);

        } else if($(this).parents('.modal.open').find('input#placelocationsearch').length) {
            var location = $(this).parents('.modal.open').find('input#placelocationsearch').val();
            $('#compose_mapmodal').find('#pac-input').val(location);
            currentlocation(location);
        } else {
            currentlocation();
            $('#compose_mapmodal').find('#pac-input').val('');
        }

        $('#compose_mapmodal').modal('open');
    });
    
    //FUN for upload photo modal in compose tool box modal
    $(document).on('click', '#compose_uploadphotomodalAction', function () {
        if(!$('#compose_uploadphotomodal').hasClass('isloaded')) {
            getuploadstuff();
        } else {
            $('#compose_uploadphotomodal').modal('open');
        }
    });
    
    $(document).on("click",".modal .close_span,.modal .custom_close,.modal .close_modal",function () {
        var disText = $(".discard_md_modal .discard_modal_msg");
        var btnKeep = $(".discard_md_modal .modal_keep");
        var btnDiscard = $(".discard_md_modal .modal_discard");

        if($(this).find(".compose_discard_popup").length > 0 || $(this).hasClass("open_discard_modal")) {
            //$('#compose_discard_modal').modal('open');            
            if($(this).parents(".modal").hasClass("add-item-popup")) {
                $(".discard_md_modal").addClass('dis_createitem');
                $(".discard_md_modal").css("z-index", "2150");
                $(".discard_md_modal").modal("open");
            } else if($(this).parents(".modal").hasClass("post-popup")) {
                disText.html("Discard Changes?");
                btnKeep.html("Keep");
                btnDiscard.html("Discard");
                btnDiscard.removeAttr("onclick");
                $(".discard_md_modal").addClass('dis_postpopup'); 
                $(".discard_md_modal").css("z-index", "2150");
                $(".discard_md_modal").modal("open");
            } else {
                disText.html("Discard Changes?");
                btnKeep.html("Keep");
                btnDiscard.html("Discard");
                btnDiscard.removeAttr("onclick");
                $(".discard_md_modal").css("z-index", "2150");
                $(".discard_md_modal").modal("open");
            }
        } else {  
            if($(this).hasClass("post_btn")){
                var modalID = $(this).parents(".modal").attr("id");
                if($(this).hasClass("active_post_btn")){
                    if($(".discard_md_modal").hasClass("open")){
                        $(".discard_md_modal").modal("close");
                        setTimeout(function(){
                            $(".modal.open").modal("close");            
                        },400);
                    }else{                      
                        $('#'+modalID+'').modal('close');
                    }
                }
            }else{              
                var modalID = $(this).parents(".modal").attr("id");
                if($(".discard_md_modal").hasClass("open")){
                    $(".discard_md_modal").modal("close");
                    setTimeout(function(){
                        $(".modal.open").modal("close");            
                    },400);
                }else{                      
                    $('#'+modalID+'').modal('close');
                }
            }
        }
    });
    
    //compose add person modal js - //share on connections wall - //share as message
    $(document).on('click', '.compose_addpersonAction', function () {
        $('#compose_addperson').find('.person_box').html('');
        $('#compose_addperson').modal('open');
    });

    $(document).on('click', '.compose_addpersonAction_as,#compose_addpersonAction_as', function () {
        $('#compose_addpersonAction_as_modal').find('.person_box').html('');
        $('#compose_addpersonAction_as_modal').modal('open');
    });
    
    $(document).on('click', '.compose_addpersonActionShareWith', function () {
        //$('#compose_addperson_sharewith').find('.person_box').html('');
        //$('#compose_addperson_sharewith').modal('open');
    });

    //FUN of search input
    $(document).on("click",".search_span",function () {
        $('#pac-input').focus();
    });
    $(document).on("click",".back_arrow",function () {
        $('.search_container .close_span').show();
        $('.search_container .back_arrow').hide();
        $('.search_span').show();
        $('.empty_input').hide();
        $('#pac-input').val("");
    });
    $(document).on("click","#pac-input",function () {
        $('.search_container .close_span').hide();
        $('.search_container .back_arrow').show();
        $('#pac-input').addClass('pac_input_new');
        $('.search_span').hide();
        $('.empty_input').show();
    });

    $(document).on("click",".empty_input",function () {
        $('#pac-input').val('')
    });
    //end of search input

    //post title for compose tool box
    $(document).on("click", ".compose_titleAction", function () {
        $(this).parents(".modal").find(".title_post_container").slideToggle(500);
        $(this).parents(".modal").find(".title_post_container").focus();
        
        var scrollDiv = $(this).parents(".modal").find('.scroll_div');
        scrollDiv.animate({
            scrollTop: 0},
        'slow');
    
    });
    
    //compose modal privacy select dropdown
    $(document).on("click",".dropdown-content li",function () {
        $(this).parent().siblings('.dropdown_text').find("span").html($(this).find('a').html());
    });
    
    //compose upload photo modal js for edit tool box
    $(document).on('click', '#compose_edituploadphotomodalAction', function () {
        if(!$('#compose_uploadphotomodal').hasClass('isloaded')) {
            getuploadstuff();
        } else {
            $('#compose_uploadphotomodal').modal('open');
        }
    });
    
    //post title for edit tool box
    $(document).on('click', '#compose_edittitleAction', function () {
        var $seletor = $(this).parents(".modal").find(".title_post_container");
        $seletor.find('.title').slideToggle(500).focus();
        $seletor.offset().top;
        $(this).parents(".modal").find('.scroll_div').animate({
            scrollTop: $seletor},
        'slow');
    });
    
    $(document).on('click', '.show_all_btn',function () {
        $('.show_all_btn').hide();
        $('.collpase_btn').show();
    });
    $(document).on('click', '.collpase_btn',function () {
        $('.collpase_btn').hide();
        $('.show_all_btn').show();
    });
    //share info from share post modal
    $(document).on('click', '.show_all_btn',function () {
        $('.share_info_container').slideDown();
    });
    $(document).on('click', '.collpase_btn',function () {
        $('.share_info_container').slideUp();
    });
    
    //FUN for tag person modal in compose tool box modal


     $('#chk_person_done_new').click(function (e) {
        if($(this).hasClass('focoutTRV03')) {
            return false;
        }

        addUserForTag = addUserForTagTemp.slice();
        $('#compose_addperson').modal('close');
        $custSelector = $('.modal.open').find('id').prevObject[0]['id'];
        if(addUserForTag.length) {
            $.ajax({ 
                url: "?r=site/combileidwithname", 
                type: 'POST',
                data: {ids: addUserForTagTemp},
                success: function (data) {
                    var result = $.parseJSON(data);
                    if(result.status != undefined && result.status == true) {
                        var $html = result.html;
                        $('#'+$custSelector).find('#tag_person').html($html);
                    }
                }
            });
        } else {
            $('#'+$custSelector).find('#tag_person').html('');
        }
    });

    $('#chk_person_done_ss').click(function () {
        if($(this).hasClass('focoutTRV03')) {
            return false;
        }

        addUserForAccountSettingsArray = addUserForAccountSettingsArrayTemp.slice();
        $('#compose_addpersonAction_as_modal').modal('close');
        $custSelector = $('.modal.open').find('id').prevObject[0]['id'];
        if(addUserForAccountSettingsArray.length) {
            $.ajax({
                url: "?r=site/get-html-content-for-block", 
                type: 'POST',
                data: {$label: whoisopeninblock, ids: addUserForAccountSettingsArrayTemp},
                success: function (data) {
                    var result = $.parseJSON(data);
                    if(result.status != undefined && result.status == true) {
                        var $html = result.returnlabel;
                        $('#'+whoisopeninblock).html($html);
                    }
                }
            });
        } else {
            $('#'+$custSelector).find('#tag_person').html('');
        }

        if(whoisopeninblock == 'restricted_list_label') {
            $restricted_list_label = addUserForAccountSettingsArray;
        } else if(whoisopeninblock == 'blocked_list_label') {
            $blocked_list_label = addUserForAccountSettingsArray;
        } else if(whoisopeninblock == 'blocked_event_invites_label') {
            $blocked_event_invites_label = addUserForAccountSettingsArray;
        } else if(whoisopeninblock == 'message_filter_label') {
            $message_filter_label = addUserForAccountSettingsArray;
        } else if(whoisopeninblock == 'request_filter_label') {
            $request_filter_label = addUserForAccountSettingsArray;
        } else if(whoisopeninblock == 'page_restricted_list') {
            $page_restricted_list = addUserForAccountSettingsArray;
        } else if(whoisopeninblock == 'page_block_list') {
            $page_block_list = addUserForAccountSettingsArray;
        } else if(whoisopeninblock == 'page_block_message_list') {
            $page_block_message_list = addUserForAccountSettingsArray;
        }   
    });
    
    $(document).on('click', '#chk_person_done_sharewith', function () {
        addUserForShareWith = addUserForShareWithTemp.slice();
        $('#compose_addperson_sharewith').modal('close');
        $custSelector = $('.modal.open').find('id').prevObject[0]['id'];
        
        if(addUserForShareWith.length) {
            $.ajax({
                url: "?r=site/combileidwithname", 
                type: 'POST',
                data: {ids: addUserForShareWithTemp, for: 'share'},
                success: function (data) {
                    var result = $.parseJSON(data);
                    if(result.status != undefined && result.status == true) {
                        var $html = result.html;
                        $('#usegift-popup').find('.share-connections').find(".input-holder").html($html);
                    }
                }
            });
        } else {
            $('#'+$custSelector).find('.share-connections').find(".input-holder").html('');
        }
    });
    
    $(document).on("click", ".removelocation", function () {
        $(".main_modal.open").find(".location_parent").find("label").text("");
        $('.location_content').removeClass('location_parent')
    });
    
    $(document).on('click', '.map_location_container', function (e) {
        try {
            if ($("#selectedlocation").text() == "") {
                $('.post_active_btn').prop('disabled', false).addClass('active_post_btn');
            }
            
            var n = $(this).attr('data-id');
            location_name = $(this).find("#dev" + n).text();

            if($('#compose_mapmodal').hasClass('map_modalUniq')) {
                $('#compose_mapmodal').find('#pac-input').val(location_name);
                $('#compose_mapmodal').modal('close');
                $('#'+$isMapLocationId).val(location_name);
            } else {
                $locationLabel = location_name;
                $('#compose_mapmodal').find('#pac-input').val(location_name);
                $('#compose_mapmodal').modal('close');
                $('.location_content').addClass('location_parent');
                $('.modal.open').find("#selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location_name + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                $('.modal.open').find("#edit_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location_name + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                $('.modal.open').find("#share_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location_name + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
            }

        } catch (err) {
        }
    });
    
    $(document).on("keyup", "#compose_addperson #search_box", function () {
        var $key = $("#compose_addperson").find("#search_box").val().trim();
            $.ajax({ 
                url: "?r=site/add-user-for-tag-search", 
                type: 'POST', 
                data: {$key, addUserForShareWithTemp},
                success: function (data) {
                    var result = $.parseJSON(data);
                    if(result.status != undefined && result.status == true) {
                        var content = result.html;
                        var userCount = addUserForTagTemp.length;
                        if(userCount>1) {
                            var label = userCount + ' users selected';
                        } else {
                            var label = userCount + ' user selected';
                        }
                        $('#compose_addperson').find('.selected_photo_text').html(label);
                        if(content != '') {
                            $('#compose_addperson').find('.person_box').html(content);
                        } else {
                            getnolistfound('norecordfoundaddperson');
                        }
                    } else {
                        getnolistfound('norecordfoundaddperson');
                    }
                }
            });
        //}
    }); 

    $(document).on("keyup", "#compose_addpersonAction_as_modal #search_box", function () {
        var $key = $("#compose_addpersonAction_as_modal").find("#search_box").val().trim();
            $.ajax({
                url: "?r=site/add-user-for-tag-search", 
                type: 'POST',
                data: {$key, addUserForAccountSettingsArrayTemp},
                success: function (data) {
                    var result = $.parseJSON(data);
                    if(result.status != undefined && result.status == true) {
                        var content = result.html;
                        var userCount = addUserForAccountSettingsArrayTemp.length;
                        if(userCount>1) {
                            var label = userCount + ' users selected';
                        } else {
                            var label = userCount + ' user selected';
                        }
                        $('#compose_addpersonAction_as_modal').find('.selected_photo_text').html(label);
                        if(content != '') {
                            $('#compose_addpersonAction_as_modal').find('.person_box').html(content);
                        } else {
                            getnolistfound('norecordfoundaddperson');
                        }
                    } else {
                        getnolistfound('norecordfoundaddperson');
                    }
                }
            });
        //}
    }); 
    $(document).on("keyup", "#compose_addperson_sharewith #search_box", function () {
        var $key = $("#compose_addperson_sharewith").find("#search_box").val().trim();
            $.ajax({
                url: "?r=site/add-user-for-tag-search", 
                type: 'POST',
                data: {$key, addUserForShareWithTemp},
                success: function (data) {
                    var result = $.parseJSON(data);
                    if(result.status != undefined && result.status == true) {
                        var content = result.html;
                        var userCount = addUserForShareWithTemp.length;
                        if(userCount>1) {
                            var label = userCount + ' users selected';
                        } else {
                            var label = userCount + ' user selected';
                        }
                        $('#compose_addperson_sharewith').find('.selected_photo_text').html(label);
                        if(content != '') {
                            $('#compose_addperson_sharewith').find('.person_box').html(content);
                        } else {
                            getnolistfound('norecordfoundaddpersonshare');
                        }
                    } else {
                       getnolistfound('norecordfoundaddpersonshare'); 
                    }
                }
            });
        //}
    }); 
    
    //Fun add connect
    $(document).on('click', '.add_connect',function () {
        $(this).parents('.desc-holder').find('.add_connect').hide();
        $(this).parents('.desc-holder').find('.remove_connect').show();
    });
    $(document).on('click', '.remove_connect',function () {
        $(this).parents('.desc-holder').find('.add_connect').show();
        $(this).parents('.desc-holder').find('.remove_connect').hide();
    });
    //end of Fun add connect
    
    //FUN of height of compose_tool_box modal
    $(window).resize(function () {
        setModelMaxHeight();
        datepikerfndestoy();
        dropdown929Ivalidation();
        setTimeout(function(){
            gridBoxImgUINew('.gridBox127');        
        },400);

    });
    
    $(document).on('click', '.datepickerinput', function() {
        $this = $(this);
        $query = $($this).attr('data-query');
        datepickerfn($this, $query);
    });

    $(document).on('click', '.datepickerinputtemp', function() {
        $this = $(this);
        $query = $($this).attr('data-query');
        datepickerfn($this, $query, true);
    });


    //Fun for tag person 
    var totalPerson = [];
    $(document).on("click", ".chk_person", function (e) {
        var checkbox = $(this);
        var pn = $(this).parents(".person_name_container").find(".person_name").text()  
        
        if (checkbox[0].checked == true) {
            // checkbox.attr("checked", "true");
            testArray.push(pn);
            totalPerson.push(pn);
            //checkbox.removeAttr("checked");
            
        } else {
            testArray1 = testArray.filter(function (value) {
                return value == pn;
            });
            
            if (testArray1.length > 0) {

                var index = testArray.indexOf(testArray1[0]);
                testArray.splice(index, 1);
            }
            if(totalPerson.length > 0){
                var i = totalPerson.indexOf(pn);            
                totalPerson.splice(i, 1);
            }       
        }

        if (totalPerson.length > 1) {
            $(".selected_person_text").html(totalPerson.length+" persons selected");
        }else{
            $(".selected_person_text").html(totalPerson.length+" person selected");
        }
        
        if (testArray.length == 1) {
            $('.done_btn').prop('disabled', false).addClass('active_check');

        }
        else{ 
            if (testArray.length == 0) {
                $('.done_btn').prop('disabled', true).removeClass('active_check');
            }
        }
    });
    
    $(document).on("click", "#saveimg",function () {
        $('#upload_img_box').modal('close');
    });
    
    $(document).on('click', '.damagedropdownli', function(){
        if($(this).parents('.damagedropdown').length >0) {
            var thisvalue = $(this).text().trim();
            $(this).parents('.modal').find('.post_privacy_label').html(thisvalue);
        }
    });    

    $(document).on('click', '#compose_addperson .person_detail_container', function () {
        $(this).find(".chk_person").click();
        var $thisvalue = $(this).find(".chk_person").val();    

        if($(this).find(".chk_person").prop('checked') == true){
            if($.inArray($thisvalue, addUserForTagTemp) !== -1) {
            } else {
                addUserForTagTemp.push($thisvalue);
            }
        } else {
            addUserForTagTemp = $.grep(addUserForTagTemp, function(value) {
              return value != $thisvalue;
            });
        }

        if(addUserForTagTemp.length >0) {
            $label = addUserForTagTemp.length + ' users selected.';
            $('#compose_addperson').find('#chk_person_done_new').removeClass('focoutTRV03');
        } else {
            $label = '0 user selected.';            
            $('#compose_addperson').find('#chk_person_done_new').addClass('focoutTRV03');
        }
        $('#compose_addperson').find('.selected_photo_text').html($label);
    });    

    $(document).on('click', '#compose_addpersonAction_as_modal .person_detail_container', function () {
        $(this).find(".chk_person").click();
        var $thisvalue = $(this).find(".chk_person").val();    

        if($(this).find(".chk_person").prop('checked') == true){
            if($.inArray($thisvalue, addUserForAccountSettingsArrayTemp) !== -1) {
            } else {
                addUserForAccountSettingsArrayTemp.push($thisvalue);
            }
        } else {
            addUserForAccountSettingsArrayTemp = $.grep(addUserForAccountSettingsArrayTemp, function(value) {
              return value != $thisvalue;
            });
        }

        if(addUserForAccountSettingsArrayTemp.length >0) {
            $label = addUserForAccountSettingsArrayTemp.length + ' users selected.';
            $('#compose_addpersonAction_as_modal').find('#chk_person_done_ss').removeClass('focoutTRV03');
        } else {
            $label = '0 user selected.';            
            $('#compose_addpersonAction_as_modal').find('#chk_person_done_ss').addClass('focoutTRV03');
        }
        $('#compose_addpersonAction_as_modal').find('.selected_photo_text').html($label);
    });    
    
    $(document).on('click', '#compose_addperson_sharewith .person_detail_div', function () {
        $(this).find(".chk_person").click();
        var $thisvalue = $(this).find(".chk_person").val();    

        if($(this).find(".chk_person").prop('checked') == true){
            if($.inArray($thisvalue, addUserForShareWithTemp) !== -1) {
            } else {
                addUserForShareWithTemp.push($thisvalue);
            }
        } else {
            addUserForShareWithTemp = $.grep(addUserForShareWithTemp, function(value) {
              return value != $thisvalue;
            });
        }

        if(addUserForShareWithTemp.length >0) {
            $label = addUserForShareWithTemp.length + ' users selected.';
        } else {
            $label = '0 user selected.';            
        }
        $('#compose_addperson_sharewith').find('.selected_photo_text').html($label);
    });

    //**************collection page js**************/
    
    $(document).on('click', '#upload_img_action',function () {
        //$("#fileinput").click();
        //$(".cropit-preview").css('display', 'block')
    });

    $('.messagesearch-xs-btn').hide();
    
    var public_trip_height = $('.public_trip_modal').height();
    if ($(window).height() <= 568) {
        $('.public_trip_content').css('height', public_trip_height - 55);
    } else {
        $('.public_trip_content').css('height', public_trip_height - 110);
    }
    
    /*** logout ***/
    $(document).on("click",".modal.dis_logout .modal_discard",function (event) {
        $.ajax({
            type: 'POST',
            url: '?r=site/user-logout',
            success: function(data){
            if(data == "1") {
             window.location.href='?r=site/index';
            }
            }
        });
    });

    $(document).on("click", ".closedatepicker", function () {
        if($('#datepickerDropdown').hasClass('open')) {
            $('#datepickerDropdown').modal('close');
        }

        if($('.profile-settingblock2').length) {
            $('.profile-settingblock2').hide();
            $('.profile-settingblock1').show();
        }
    });

    $(document).on("click", ".resetdatepicker", function () {
        $('[data-toggle="datepicker"]').datepicker('reset');
        if($('.profile-settingblock2').length){
            $('.profile-settingblock2').hide();
            $('.profile-settingblock1').show();
        }
    });
});


function calluploadstuffmore() {
    $("#compose_uploadphotomodal .uploadphotomodal_container").bind('scroll', function () {
        if( $('.lazyloadscrolluploadstuff').is_on_screen() ) { // if target element is visible on screen after DOM loaded
            $('.lazyloadscrolluploadstuff').removeClass("lazyloadscrolluploadstuff");
            getuploadstufflazyload();
        }
    });
}

function getuploadstuff() {
    $.ajax({  
        url: '?r=site/getuploadstuff', 
        success: function(data) {
            storedFiles = [];
            storedFilesExsting = [];
            $('#compose_tool_box').find('.post-photos').find('.img-row').html('');

            $('#compose_uploadphotomodal').html(data);

            setTimeout(function() {
                $('#compose_uploadphotomodal').modal('open');
                $('#compose_uploadphotomodal').addClass('isloaded');

                $selectore = $('#compose_uploadphotomodal').find('.getuploadstuffbox');
                if($selectore.length) {
                    $('#compose_uploadphotomodal').find('.getuploadstuffbox').last().addClass('lazyloadscrolluploadstuff');
                } else  {
                    $lazyhelpcountuploadstuff = $lazyhelpcountuploadstuff + 1;
                    getuploadstufflazyload();
                }
            },400);
        },
    });
}

function getuploadstufflazyload() {
    $.ajax({  
        type: 'POST',
        url: '?r=site/getuploadstufflazyload', 
        data: {$lazyhelpcountuploadstuff},
        success: function(data) {
            var result = $.parseJSON(data);

            $lazyhelpcountuploadstuff = $lazyhelpcountuploadstuff + 1;
            $html = result.html;
            
            $('#compose_uploadphotomodal').find('.upload-images-gallery').append($html);
            
            setTimeout(function(){ 
                $('#compose_uploadphotomodal').find('.getuploadstuffbox').last().addClass('lazyloadscrolluploadstuff');

                if( $('.lazyloadscrolluploadstuff').is_on_screen() ) { // if target element is visible on screen after DOM loaded
                    $('.lazyloadscrolluploadstuff').removeClass("lazyloadscrolluploadstuff");
                    getuploadstufflazyload();
                }
            },400);
        },
    });
}

function fillcheckedfromuploadstuff() {
    $tempo = [];
    $(storedFilesExsting).each(function(i, v) {
        $name = v.name;
        $tempo.push($name);
    });

    $selectore = $('.uploadphotomodal_container .getuploadstuffbox');
    $($selectore).each(function(i, v) {
        $src = $(v).find('img').attr('src');
        if($.inArray($src, $tempo) !== -1) {
            $(v).find('.check-image').addClass('active-class');
        } else {
            $(v).find('.check-image').removeClass('active-class');
        }
    });

    printcheckedtotalheaderlable();
}

function printcheckedtotalheaderlable() {
    //$totalcheckedimages = $(storedFilesExsting).length;
    $totalcheckedimages = $('.check-image.active-class').length;
    $odknds = $('#compose_uploadphotomodal').find('.selected_photo_text');
    if($totalcheckedimages <=0) {
        $($odknds).html('0 photos selected');
    } else if ($totalcheckedimages == 1) {
        $($odknds).html('1 photo selected');
    } else {
        $($odknds).html($totalcheckedimages+' photos selected');
    }
}

function doLogout(isPermission=false) {
    if(isPermission) {
        window.location.href='?r=site/user-logout';
    } else { 
        var disText = $(".discard_md_modal .discard_modal_msg");
        var btnKeep = $(".discard_md_modal .modal_keep");
        var btnDiscard = $(".discard_md_modal .modal_discard");
        disText.html("Logging out?");
        btnKeep.html("Stay");
        btnDiscard.html("Logout");
        btnDiscard.attr('onclick', 'doLogout(true)');
        $(".discard_md_modal").modal("open");
    }
}

//FUN for collection share post  modal
function shareInfoPost() {
    $('#sharepostmodal').modal('open');
}

function openAddItemModal() {
    $('.dropdown-button').dropdown("close");
    $('.add-item-popup').modal('open');
    setTimeout(function(){
        initDropdown();
    },400);

    setTimeout(function(){
    //getprivacydata();
    },1000);
}
//FUN for opening add item modal

//discard modal 
function discardModel() {
    $('.discard_md_modal').modal('open');
}

//coollection clear post
function CollectionClearPost() {
    $('#Collection_title').val('');
    $('#Collection_title').blur();
    $('#Collection_tagline').val('');
    $('.dropdown-button').dropdown('close');
}

//clear data in  compose tool box 
function clearPost() {
    $('.title').val("");
    $('.comment_textarea').val("");
    $('.share_comment_textarea').val("");
    $("#post_edittitle").val("");
    $("#selectedlocation").text("");
    $("#edit_selectedlocation").text("");
    $("#share_selectedlocation").text("");
    $("#tag_person").text("");
    $("#edit_tag_person").text("");
    $("#share_tag_person").text("");
    testArray = [];
    $(".img-row").children().remove();
    $(".imgfile-count").val(0);
    $('.location_content').removeClass('location_parent');
    $('.dropdown-button').dropdown('close');
}

//disable btn when clear data in compose tool box
function stoppedTyping() {
    if ($(".new_post_comment").val().length == 0) {
        $('.post_active_btn').prop('disabled', true).removeClass('active_post_btn');

    } else {
        $('.post_active_btn').prop('disabled', false).addClass('active_post_btn');
    }
}
function setPostBtnStatus() {
    var $selectore = $('.main_modal.open').length;  
    if($selectore >0) {
        var $custId = $('.main_modal.open').attr("id");     
        if($('#'+$custId).find('.post-mcontent').find('.comment_textarea').length) {
            var $textValue = $('#'+$custId).find('.post-mcontent').find('.comment_textarea').val().trim();
            if($textValue.length == 0) {
                var $imagesCount = $('#'+$custId).find('.post-photos').find('img').length;
                if($imagesCount <=0) {
                    $('#'+$custId+' .post_btn').prop('disabled', true).removeClass('active_post_btn').css('cursor', 'not-allowed');
                } else {
                    $('#'+$custId+' .post_btn').prop('disabled', false).addClass('active_post_btn').css('cursor', 'pointer');
                }
            } else {
                $('#'+$custId+' .post_btn').prop('disabled', false).addClass('active_post_btn').css('cursor', 'pointer');
            }   
        }
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageid')
                .attr('src', e.target.result)

        };

        reader.readAsDataURL(input.files[0]);
    }
}

//Fun for croppit

$("#file").change(function () {
    loadImage(this);
});

function loadImage(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        canvas = null;
        reader.onload = function (e) {
            image = new Image();
            image.onload = validateImage;
            image.src = e.target.result;
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    // Exporting cropped image 
    $(document).on("click", ".saveimg", function () {
        if($(this).parents('.crop-holder').length) {
            $selector = $(this).parents('.crop-holder');
            var imageData = $selector.cropit('export');
            
            //$selector = $(this).parents('.preview-part');
            $selector.find(".main-img1").find('img').attr('src', imageData);

            $('.cropit-preview-image').css('transform', 'unset');

            $selector.find(".cropit-preview").removeClass("class-show");
            $selector.find(".cropit-preview").addClass("class-hide");

            $selector.find(".btn-save").removeClass("class-show");
            $selector.find(".btn-save").addClass("class-hide");

            $selector.find(".main-img1").removeClass("class-hide");
            $selector.find(".main-img1").addClass("class-show");

            $selector.find(".main-img").removeClass("class-show");
            $selector.find(".main-img").addClass("class-hide");

            $selector.find(".removeimg").removeClass("class-show");
            $selector.find(".removeimg").addClass("class-hide");
        }

    });

    $(document).on("click", "#removeimg", function () {
        if($(this).parents('.crop-holder').length) {
            $selector = $(this).parents('.crop-holder');

            $selector.find(".main-img").removeClass("class-hide");
            $selector.find(".main-img").addClass("class-show");

            $selector.find(".cropit-preview").removeClass("class-show");
            $selector.find(".cropit-preview").addClass("class-hide");

            $selector.find(".main-img1").removeClass("class-show");
            $selector.find(".main-img1").addClass("class-hide");

            $selector.find(".btn-save").removeClass("class-show");
            $selector.find(".btn-save").addClass("class-hide");

            $selector.find(".removeimg").removeClass("class-show");
            $selector.find(".removeimg").addClass("class-hide");
        }

    });
});

/* fun of designer */

function openLogoutDialog(){    
    $('#logout_modal').modal('open');
}
function initDropdown(){
    //custom js for MD dropdown 
    $('select').material_select();
    $('.dropdown-button').dropdown({
        alignment: 'right'
    });
    $('.dropdown-button-left').dropdown({
        alignment: 'left'
    });
    $('.dropdown_right .dropdown-button').dropdown({
        alignment: 'right'
    });
    $('.dropdown-button-top').dropdown({
        alignment: 'top'
    });

    $('.dropdown .dropdown-button, .public_dropdown_container').dropdown({
        inDuration: 300,
        outDuration: 225,
        constrainWidth: true,
        hover: false,
        gutter: 0,
        belowOrigin: false,
        alignment: 'right',
        stopPropagation: false 
        //stopPropagation: true
    });    

    $('ul.tabs').tabs(); 
}
function setModelMaxHeight(){
    var modal_height = $(window).height() * 0.7;
    if($(window).width() > 767)
        $('.modal.open .scroll_div').css('max-height', modal_height - 180 + 'px');
    else if($(window).width() > 420)
        $('.modal.open .scroll_div').css('max-height', modal_height - 150 + 'px');
    else{           
        $('.modal.open .scroll_div').css('max-height', $(window).height() - 210 + 'px');
    }
}

function removeDisClasses(){
    
    var classes = $(".discard_md_modal").attr("class").split(' ');
    $.each(classes, function(i, c) {
        if (c.indexOf("dis_") == 0) {                    
            $(".discard_md_modal").removeClass(c);
        }
    }); 
}

/* fun by dev */

function setDiscardId(dataid){
    $(".discard_md_modal .modal_discard").attr('data-id', ''+dataid+'');
}

function addUserForTagfun() { 
    $.ajax({
        url: "?r=site/add-user-for-tag", 
        type: 'POST',
        data: {ids: addUserForTag},
        success: function (data) {
            var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var content = result.html;
                var userCount = addUserForTag.length;
                if(userCount>1) {
                    var label = userCount + ' users selected';
                } else {
                    var label = userCount + ' user selected';
                }
                $('#compose_addperson').find('.selected_photo_text').html(label);
                $('#compose_addperson').find('.person_box').html(content);
            } else {
                $('#compose_addperson').find('.selected_photo_text').html('0 user selected.');
                getnolistfound('norecordfoundtag');
            }
        }
    });
}

function addUserForShareWithfun() {
    $.ajax({
        url: "?r=site/add-user-for-tag", 
        type: 'POST',
        data: {ids: addUserForShareWith},
        success: function (data) {
            var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var content = result.html;
                var userCount = addUserForShareWith.length;
                if(userCount>1) {
                    var label = userCount + ' users selected';
                } else {
                    var label = userCount + ' user selected';
                }
                $('#compose_addperson_sharewith').find('.selected_photo_text').html(label);
                $('#compose_addperson_sharewith').find('.person_box').html(content);
            } else {
                $('#compose_addperson_sharewith').find('.selected_photo_text').html('0 user selected.');
                getnolistfound('norecordfoundaddpersonshare');
            }
        }
    });
}

function setaddUserForTag(editpostid) {
    if(editpostid != undefined && editpostid != null && editpostid != '') {
        $.ajax({
            type: 'POST', 
            url: '?r=site/setadduserfortag', 
            data:{'editpostid' : '5c0579159fb00fc43700002b'}, 
            success: function(data) {
                var result = $.parseJSON(data);
                if(result.status == true && result.ids != undefined) {
                    var $ids = result.ids;
                    addUserForTag = $ids;
                } else {
                    addUserForTag = [];
                }
            },
        });   
    }
}

function filderMapLocationModal(obj) {
    $('.pac-container').remove();  
    $query = $(obj).attr('data-query');
    $isMapLocationId = $(obj).attr('id');
      
    if($query != undefined && $query != null && $query != '') {
        if($query == 'all') {
            currentlocation();
            $('#pac-input').val("");
            $('#compose_mapmodal').modal('open');
        } else if($query == 'none') {
            geolocate();
        } else if($query == 'MT') {
            $width = $( window ).width();
            if($width >1024) {      
                geolocate();
            } else {
                currentlocation();
                $('#pac-input').val("");
                $('#compose_mapmodal').modal('open');
            }
        } else if($query == 'M') {
            $width = $( window ).width(); 
            if($width >480) {
                geolocate();
            } else {
                currentlocation();
                $('#pac-input').val("");
                $('#compose_mapmodal').modal('open');
            }
        }
            
    }
}

function failGetLatLong($location='') {
  if($location == '' || $location == undefined || $location == null) {
    $location = '"Amman"';
  } else if($location.code == 1 && $location.message == 'User denied Geolocation') {
    $location = '"Amman"';
  }

  var geocoder =  new google.maps.Geocoder();
  geocoder.geocode( { 'address': 'Amman' }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
        var lat = results[0].geometry.location.lat();
        var lng = results[0].geometry.location.lng();
        var exPos = {lat, lng};
        currentlocation2(exPos);
    }
  });
}

function getLatLong(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    var exPos = {lat, lng};
    currentlocation2(exPos);
}

function currentlocation() {  
    var place_name = "";
    if($('#'+$isMapLocationId).length) {
        var location = $('#'+$isMapLocationId).val();
         
        if(location == '') {
            if(navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(getLatLong, failGetLatLong);
            } else {
              failGetLatLong();
            }
        } else {
            var geocoder =  new google.maps.Geocoder();
            geocoder.geocode( { 'address': location }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var lng = results[0].geometry.location.lng();
                var exPos = {lat, lng};
                currentlocation2(exPos);
            }   
          }); 
        }
   } 
   else if($isMapLocationId == undefined || $isMapLocationId == '') {
        if(navigator.geolocation) {
            if($locationLabel != '') {
                failGetLatLong($locationLabel);
            } else {
                navigator.geolocation.getCurrentPosition(getLatLong, failGetLatLong);   
            }
        } else {
            if($locationLabel != '') {
                failGetLatLong($locationLabel);
            } else {
                failGetLatLong();
            }
        }
   }
}

function currentlocation2(pos) {
    var geocoder =  new google.maps.Geocoder();
    geocoder.geocode({'location': pos}, function(results, status) {
        if (status === 'OK') {

            map = new google.maps.Map(document.getElementById('map'), {
                center: pos,
                zoom: 9
            });

            var options = {
                types: ['(cities)'],
            };

            var markers = [];

            markers.forEach(function (marker) {
                marker.setMap(null);
            });

            markers = [];
            markers.push(new google.maps.Marker({
                map: map,
                position: pos
            }));

            var service = new google.maps.places.PlacesService(map);
                var div = "";
                $(".map_container").html('');
                for (var i = 0; i < results.length; i++) {

                    var address_components = results[i].address_components;
                    var country = '';

                    $.each(address_components, function (i, address_component) {
                        if (address_component.types[0] == "administrative_area_level_3"){ 
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "locality"){
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "country"){ 
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }

                        if (address_component.types[0] == "postal_code_prefix") {
                            if(country != '') {
                                country = country + ', ' + address_component.long_name;
                            } else {
                                country = address_component.long_name;
                            }
                        }
                    });
                    div = "<div class='map_location_container' data-id='" + i + "'>" +
                        "<div class='location_container'>" +
                        "<span class='map_icon'>" +
                        "<i class='zmdi zmdi-hc-2x zmdi-pin'></i>" +
                        "</span>" +
                        "<div class='location'>" +
                        "<p class='location_name' id='dev" + i + "' >" + results[i].formatted_address + "</p>" +
                        "<p class='place_address'>" + country + "</p>" +
                        "</div>" +
                        "</div>" +
                        "</div>";
                    
                    $(".map_container").append(div);
                }
                setTimeout(function(){ 
                    initNiceScroll(".nice-scroll");                     
                },400)

            google.maps.event.addListener(markers[0], 'click', function () {
                $('#compose_mapmodal').modal('close');
                if($('#compose_mapmodal').hasClass('map_modalUniq')) {
                    var location = $('#compose_mapmodal').find("#pac-input").val();
                    if($.trim(location) == '') {
                        location = 'Amman';
                    }
                    $('#'+$isMapLocationId).focus().val(location);
                } else {
                    var location = $("#pac-input").val();
                    $('.location_content').addClass('location_parent');
                    $("#selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                    $("#edit_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                    $("#share_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + place_name + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
                }
            });

        }
    });
}

$(document).on("focus keyup","#compose_mapmodal #pac-input", function() {
    initAutocompletenew();
});

$(document).on('mouseover', '.dropdown782 ul.select-dropdown', function() {
    $width = $(window).width();
    $dummy = null;
    if($width<480) {
       $fill = $(this).parent('div').find('select').data('fill');
       $action = $(this).parent('div').find('select').data('action');
       $selectore = $(this).parent('div').find('select').data('selectore');
       if($fill == 'y') {
            $dummy = $('select.'+$selectore).val();
       }
       dropdown929I($action, $fill, $selectore, $dummy);
    } else {
        $('#custom_dropdown_modal').modal('close');
    }
});

$(document).on('mouseover', '.dropdown782 ul.select-dropdown', function() {
    $(this).css('overflow-y', 'auto');
    removeNiceScroll($(this));
});

function dropdownadjustment0328921() {

}

function datepikerfndestoy() {
    $('[data-toggle="datepicker"]').datepicker('destroy');
    if($('#datepickerDropdown').hasClass('open')) {
        $('#datepickerDropdown').modal('close');
    }

    if($('.profile-settingblock2').length) {
        $('.profile-settingblock2').hide();
        $('.profile-settingblock1').show();
    }
}

function datepickerfn($obj, $device, $isNew=false) {
    $('[data-toggle="datepicker"]').datepicker('destroy');
    $w = $(window).width();
    $isOpen = 'normal';

    if($device == 'M') {
        if($w <= 480) {
            $isOpen = 'modal';
        }
    } else if($device == 'MT') {
        if($w <= 1024) {
            $isOpen = 'modal';
        }
    } else if($device == 'all') {
        $isOpen = 'modal';
    } else if($device == 'none') {
        $isOpen = 'normal';
    }

    if($isNew) {
        if($w <= 420) {
            $isNew = false;
        }
    }

    if($isNew) {
        $($obj).datepicker({
            format: 'yyyy-mm-dd',
            container: $('#datepickerBlocktemp'),
            inline: true,
            autoShow: true,
            autoHide: false
        });
        $('.profile-settingblock1').hide();
        $('.profile-settingblock2').show();
    } else {
        if($isOpen == 'modal') {
            $('#datepickerDropdown').modal('open');
            $($obj).datepicker({
                format: 'yyyy-mm-dd',
                container: $('#datepickerBlock'),
                inline: true,
                autoShow: true,
                autoHide: false,
                pick:function() {
                    //$('#datepickerDropdown').modal('close');
                }
            });
        } else {
            if($('#datepickerDropdown').hasClass('open')) {
                $('#datepickerDropdown').modal('close');
            }
            $($obj).datepicker({
                format: 'yyyy-mm-dd',
                autoShow: true,
                autoHide: true,
                autoHide: false,
                zIndex: 2000
            });
        }
    }
}

function dropdown929Ivalidation() {
    if($('#custom_dropdown_modal').length) {
        if($('#custom_dropdown_modal').hasClass('open')) {
            $('#custom_dropdown_modal').modal('close');                                                       
        }
    }
}

function dropdown929I($action, $fill, $selectore, $dummy) {
    $.ajax({
        type: 'POST', 
        url: "?r=site/dropdownfilter",
        data: {$action, $fill, $selectore, $dummy},
        success: function (data) {
            $('#custom_dropdown_modal').html(data);
            setTimeout(function() { 
                $('#custom_dropdown_modal').modal('open');
            },400);
        }
    });
} 

function savedrpfilterdatachk($lbl) {
    $checkedArray = [];
    $("input:checkbox[class=filterdrpchk]:checked").each(function () {
        $thisValue = $(this).val();
        $checkedArray.push($thisValue);
    });
    
    $('#custom_dropdown_modal').modal('close');
    $($lbl).material_select('destroy');
    $($lbl).val($checkedArray).material_select();
}

function savedrpfilterdatardo($lbl) {
    $checkedArray = [];
    $sdata = $("input[name='filterdrpchk']:checked").val();
    $checkedArray.push($sdata);
    $('#custom_dropdown_modal').modal('close');
    $($lbl).material_select('destroy');
    $($lbl).val($checkedArray).material_select();
}

function initAutocompletenew() {
   
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);

    //search_places
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();
        if ($("#selectedlocation").text() == "") {
            $('.post_active_btn').prop('disabled', false).addClass('active_post_btn');
        }
        
        if($('#compose_mapmodal').hasClass('map_modalUniq')) {
            var location = $('#compose_mapmodal').find("#pac-input").val();
            $('#'+$isMapLocationId).focus().val(location);
        } else {
            var location = $("#pac-input").val();
            $locationLabel = location;
            $('.location_content').addClass('location_parent');
            $("#selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
            $("#edit_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
            $("#share_selectedlocation").html("<div class='location_div'><span class='tagged_location'></span><span class='tagged_location_name'>" + location + "</span></div><a href='javascript:void(0)'  class='removelocation'><i class='mdi mdi-close'></i></a>");
        }

        $('#compose_mapmodal').modal('close');
        if (places.length == 0) {
            return;
        }
    });
}
