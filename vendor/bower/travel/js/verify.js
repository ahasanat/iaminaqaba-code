function verifyme()
{
	var selected_verify_plan = $('#selected_verify_plan').val();
	if(selected_verify_plan == '')
	{
		Materialize.toast('Select the verify plan first.', 2000, 'red');
		return false;
	} else {
		 $.ajax({
			url: '?r=site/checkverify', 
			type: 'POST',
			async: false,
			data: 'selected_verify_plan=' + selected_verify_plan,
			success: function (data) {
				if(data == '0'){
					document.getElementById("myformsubmit").submit();
				}
				else{
				}	
			}
		});
	}	
}
/* End Verify Fuction For Become a Verify Member */


/* Start Payment Via Card For Become a Verify Member */
			
$( "#paymentForm" ).submit(function( event ) {
	var selected_verify_plan = $('#selected_verify_plan').val();
		if(selected_verify_plan == '')
		{
			Materialize.toast('Select the verify plan first.', 2000, 'red');
			return false;
		}
		else
		{
			var $is_stop = true;
			$.ajax({
				url: '?r=site/checkverify', 
				type: 'POST',
				async: false,
				data: 'selected_verify_plan=' + selected_verify_plan,
				success: function (data) {
					
					if(data == '1'){
						$is_stop = false;
					}
					else
					{
						var name_on_card = $('#name_on_card').val();
						var card_number = $('#card_number').val();
						var expiry_month = $('#expiry_month').val();
						var expiry_year = $('#expiry_year').val();
						var cvv = $('#cvv').val();
						var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
						var password = $('#password').val();
						var reg_nm = /^[a-zA-Z\s]+$/;
						var spc = /^\s+$/;	
						var one_space = /^[A-Za-z]+\s+[A-Za-z]+$/;
						
						if(name_on_card == '')
						{
							Materialize.toast('Enter full name of the card holder.', 2000, 'red');
							$("#name_on_card").focus();
							$is_stop = false;
						}
						else if(!one_space.test(name_on_card))
						{
							Materialize.toast('Enter first and last name of the card holder.', 2000, 'red');
							$("#name_on_card").focus();
							$is_stop = false;
						}
						else if(card_number == '')
						{
							Materialize.toast('Enter card number.', 2000, 'red');
							$("#card_number").focus();
							$is_stop = false;
						}
						else if(expiry_month == '')
						{
							Materialize.toast('Enter expiry month.', 2000, 'red');
							$("#expiry_month").focus();
							$is_stop = false;
						}
						else if(expiry_year == '')
						{
							Materialize.toast('Enter expiry year.', 2000, 'red');
							$("#expiry_year").focus();
							$is_stop = false;
						}
						else if(cvv == '')
						{
							Materialize.toast('Enter CVV number.', 2000, 'red');
							$("#cvv").focus();
							$is_stop = false;
						}
					}
				}
			});
			if($is_stop == true)
			{
				$('#cardSubmitBtn').attr("disabled",true);
			}
			return $is_stop;
		}
});

/* End Payment Via Card For Become a Verify Member */