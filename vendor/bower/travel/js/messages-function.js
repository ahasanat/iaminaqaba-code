var getuserlabel;
$(window).load(function() {
  loadNotificationCount();
});

$(window).resize(function() {
  $w = $(window).width();
  if($w > 799) {
    $('.messages-wrapper').addClass('bigscreen');
  } else {
    $('.messages-wrapper').removeClass('bigscreen');
  }
  closeattachmentbox();
  setMsgTextArea();
  generalCaseStudy(); 
});

$(document).on('click', '#search_messages_Action', function() {
  openCustomSearchMain();
  /*$('.search_messages_modal').removeClass('showsidemodal_right');
  $('.search_messages_modal').addClass('hidesidemodal_right');*/
});

$(".not-icons.desktop .not-messages").click(function() {
  $(this).find('.messages_notification').html('');
  $(this).find('.messages_notification').css('display','none');
}); 

$(document).on('change', '#communication_label, #is_received_message_tone_on, #is_new_message_display_preview_on, #show_away, #is_send_message_on_enter', function() {
  communicationMessage();
});

$(document).on('change', '#do_mute_message', function() {
  domutemessage();
});

$(document).on('change', '#do_block_message', function() {
  doblockmessage();
});

$(document).on('keyup change paste', '#new_chat_search', function(e) {
  $searchkey = $('#new_chat_search').val().trim();
  if($searchkey != '') {
    personSearchSlide($searchkey);
  } else {
    personSlide(0);
  }
});

$(document).on('click', '.moveNextCarousel', function(e) {
  e.preventDefault();
  e.stopPropagation();
  $('.carousel').carousel('next');
});

$(document).on('click', '.movePrevCarousel', function(e) {
  e.preventDefault();
  e.stopPropagation();
  $('.carousel').carousel('prev');
});

$(document).on('change', '.selectionchecked:checkbox', function() {
  $checkedCount = $('.current-messages').find('.mainli.active').find('.selectionchecked:checkbox:checked');

  if($('.selected_msg_number').length) {
    if($checkedCount.length > 0) {
      $('.selected_msg_number').html('<span>'+$checkedCount.length+'</span> selected'); 
    } else {
      $('.selected_msg_number').html('<span>0</span> selected'); 
    }
  }

  $isImagePost = false;
  $.each($checkedCount, function(b, d) {
    $type = $(d).attr('data-msgtype');
    if($type == 'image' || $type == 'text and image') {
      $isImagePost = true;
      return false;
    }
  });

  if($isImagePost) {
    if($('.selected_messages_box').find('.downloadicon').length) {
      $('.selected_messages_box').find('.downloadicon').show();
    } else {
      $('.selected_messages_box').find('.downloadicon').hide();
    }
  } else {
    $('.selected_messages_box').find('.downloadicon').hide();
  }
});

$(document).on('input keyup keydown', '.keywordMarkHighlighter', function() {
  var $value = $(this).val();
  if($value != undefined || $value != null || $value != '') {
     var $this = $(this).parents('li.mainli');
     if($this.length) {
        var $id = $this.attr('id');
        if($id != undefined || $id != null || $id == '') {
          $selector = $('.chat-section').find('.chat-window').find('.mainul');
          if($selector.length) {
            $selector.find('li#'+$id);
            if($selector != undefined) {
              $content = $selector.find(".chat-conversation");
              var options = {'debug': false, 'diacritics': false, 'separateWordSearch': false};
                $content.unmark({
                  done: function() {
                    $content.mark($value, options);
                  }
                });
            }
          }
        }
     }   
  }
});

/*$(document).on("focusout",".messagewallsearch",function() {
  $('.messagewallsearch').val('');
  getRecentMessagsUsers('0', 'yes'); 
});*/

$(document).on('click', '.slide_out_btn', function() {
  $('.left_side_modal').removeClass('hidesidemodal');
  $('.left_side_modal').addClass('showsidemodal');
});

$(document).on('click', '.messagebox', function() {
    $selectore = $('.not-messages').find('ul.msg-listing');
    if (!$($selectore).hasClass("isloaded")) {
        notReadMessageUserIds = [];
        if(data2.id != undefined) {
            $.ajax({ 
            url: '?r=messages/get-recent-messages-users',
            beforeSend: function() {                    
                var sparent=$(".msg-list.not-area").find(".tab-pane#recent-messages");
                setLoaderImg(sparent);                  
                setOpacity(sparent.find('.loading-holder'),0);
            },
            success: function(data) {
                var sparent=$(".msg-list.not-area").find(".tab-pane#recent-messages");
                removeLoaderImg(sparent);                   
                setOpacity(sparent.find('.loading-holder'),1);

                var users = JSON.parse(data);
                $selectore.addClass('isloaded');
                if(users.length === 0) {
                    $liBox = $('<li class="norecord label-notice"><div class="valign-wrapper"><h5 class="center-align">No messages found.</h5></div></li>');
                    $selectore.html($liBox);
                } else {
                  $selectore.html('');
                  $.each(users, function(i, item) {
                      var $isreadcls = '';
                      var $notificationMsg='';
                      var msgid = item._id['$id'];
                      var $type=item.type;
                      var $from_id = item.from_id;
                      var $id = item.from_id;
                      var $fullname = item.fullname;
                      var $thumbnail = item.thumbnail;
                      var $msg = item.reply;
                      var $status = item.status;
                      var $is_read = item.is_read;
                      var $category = item.category;
                      var $to_id = item.to_id;
                     
                      if($type == 'image') {
                          if($from_id == data2.id) { 
                              $notificationMsg = "<i class='zmdi zmdi-image'></i> you sent a photo.";
                          } else {
                              $notificationMsg = "<i class='zmdi zmdi-image'></i> you receive a photo.";
                          }
                      } else if($type == 'gift') {
                          if($from_id == data2.id) { 
                              $notificationMsg = "<i class='mdi mdi-gift'></i> you sent a gift.";
                          } else {
                              $notificationMsg = "<i class='mdi mdi-gift'></i> you receive a gift.";
                          }
                      } else {
                        if($msg != undefined && $msg != null) {
                          $notificationMsg = $.emoticons.replace($msg);
                        } else {
                          $notificationMsg = '';
                        }
                      }
                      

                      var $setUnread = '<i class="zmdi zmdi-dot-circle"></i>';
                      if($id == data2.id) {
                          $id = item.to_id;
                          $isreadcls = 'read';
                          $setUnread = '';
                      }
                      if($setUnread != '' && $is_read == 1) {
                          $isreadcls = '';
                      } else if($setUnread != '' && $is_read == 0) {
                          $isreadcls = 'read';
                      }

                      if($category == 'page')
                      {
                        var $entity_link = 'index.php?r=page/index&id='+$to_id;
                        var $entity_click = '';
                      }
                      else
                      {
                        var $entity_link = 'javascript:void(0)';
                        var $entity_click = 'openChatbox(this);';
                      }

                      $liBox = $('<li class="'+$isreadcls+' mainli recid'+$id+'" data-msgid='+msgid+'><div class="msg-holder"><a href='+$entity_link+' onclick="'+$entity_click+'" data-id="chat_'+$id+'" id="'+$id+'"><span class="img-holder"><img class="img-responsive" src="'+$thumbnail+'"></span><span class="desc-holder"><span class="uname">'+$fullname+'</span><span class="desc">'+$notificationMsg+'</span><span class="time-stamp">11:40 pm</span></span></a><a class="readicon" onclick="setReadUnread(this),markRead(this);" title="Mark as read" href="javascript:void(0)">'+$setUnread+'</a><div class="clear"></div></div></li>');
                      $selectore.prepend($liBox);                                 
                  });
                }
                
                fixImageUI('not-messages');
            },
            complete: function() {                  
                var sparent=$(".msg-list.not-area").find(".tab-pane#recent-messages");
                removeLoaderImg(sparent);                   
                setOpacity(sparent.find('.loading-holder'),1);
            }
        });
        }

      /*  socket.emit('callRecentMsgForNotificationBox', {
            id: data2.id
        });*/
    }
});

$(document).on('change', '#chatWallFileUpload', function(e) {       
});

$(document).on('change', '#chatPageFileUpload', function(e) {
    var $this = $('.chat-add').find('textarea#inputPageWall');
    var $sendTo = $($this).attr('data-id');
    if($sendTo) {
    $sendTo = $sendTo.replace('li-', '');
         $.ajax({ 
                url: '?r=messages/get-category',
                type: 'POST',
                data: {id: $sendTo},
                success: function(result) {
                    var resultFilter = JSON.parse(result);
                    if(resultFilter.status) {
                             var file = e.originalEvent.target.files[0],
                                reader = new FileReader();
                            reader.onload = function(evt) {
                                var base64image = evt.target.result;
                                var imageData = base64image.split(',');
                                
                                if($sendTo) {                
                                    var sendData = {
                                        'fileName' : file.name,
                                        'base64image' : base64image,
                                        'to_id' : $sendTo,
                                        'from_id' : data2.id,
                                        'category' : 'page' 
                                    }
                                    socket.emit('userImage', sendData);
                                }
                            };
                            reader.readAsDataURL(file);
                      } else if(resultFilter.status == false && resultFilter.reason != undefined) {
                            if(resultFilter.reason == 'block') {
                                var by = resultFilter.by;
                                if(by == 'self') {
                                    $this = '<span>Unblock Messages</span>';
                                                    manageBlockConverasion($this);
                                } else {
                                }
                            }
                        }
                }
            });            
    }       
});

$('body').on('click', '.callRecentMessagesUsers', function() {
    getChatUsersForRecent();        
});

$(document).on("keypress", "#profile_name_add", function(e) {
  if(e.which == 13) {
    if($('.right-section').find('.current-messages').find('.mainli.active').length) {
      $id = $('.right-section').find('.current-messages').find('.mainli.active').attr('id');
      $id = $id.replace("li_msg_","");
      if($id) {
        $sentence = $('#profile_name_add').val();
        $.ajax({ 
          url: '?r=messages/upt-usr-sentence',
          type: 'POST',
          data: {id: $id, sentence: $sentence},
          success: function(result) {
            $('.profile_setting_box').find('.user_status_setting').html($sentence);
            $('.setting_profile_name').slideUp();
            $('.setting_profile_name input').focus();
          }
        });
      }
    }
  }
});

$('body').on("keypress", "textarea#inputChatWall", function(e) {
    if(e.which == 13) {
        // For Des.. START
        var $msg = $(this).val().trim();
        if($msg) {
            var id = $(this).attr('data-id');
            if(id) {
                $.ajax({ 
                    url: '?r=messages/get-category', // check user is block, mmute or connect or not then set category like inbox and others
                    type: 'POST',
                    data: {id: id},
                    success: function(result) {
                        var resultFilter = JSON.parse(result);
                        if(resultFilter.status) {
                            var category = resultFilter.category;
                            var SendMsgArray= {
                                'from_id': data2.id,
                                'to_id': id,
                                'reply': $msg,
                                'type': 'text',
                                'category': category
                            };
                            socket.emit('sendMessage', SendMsgArray);
                        } else if(resultFilter.status == false && resultFilter.reason != undefined) {
                            if(resultFilter.reason == 'block') {
                                var by = resultFilter.by;
                                if(by == 'self') {
                                    $this = '<span>Unblock Messages</span>';
                                    manageBlockConverasion($this);
                                } else {
                                }
                            }
                        }            
                    }
                });
             }
        $(this).val('').change();
        }
    } 
});

$('body').on("keypress", "textarea#inputPageWall", function(e) {
        if(e.which == 13) {
            var $msg = $(this).val().trim();
              if($msg) {
                if($pageID) {
                    $.ajax({ 
                        url: '?r=messages/get-category',
                        type: 'POST',
                        data: {id: $pageID},
                        success: function(result) {
                            var resultFilter = JSON.parse(result);
                            if(resultFilter.status) {
                                var category = resultFilter.category;
                                var SendMsgArray= {
                                    'from_id': data2.id,
                                    'to_id': $pageID,
                                    'pageOTHER': $pageOWNER,
                                    'pageSELF': data2.id,
                                    'reply': $msg,
                                    'type': 'text',
                                    'category': 'page'
                                };
              
                                $('textarea#inputPageWall').val('');
                                socket.emit('sendMessage', SendMsgArray);
                            } else if(resultFilter.status == false && resultFilter.reason != undefined) {
                                if(resultFilter.reason == 'block') {
                                    var by = resultFilter.by;
                                    if(by == 'self') {
                                        $this = '<span>Unblock Messages</span>';
                                        manageBlockConverasion($this);
                                    } else {
                                    }
                                   
                                }
                            }           
                        }
                    });
                }
            }
        }
});

$('body').on("keypress", "textarea#inputPageWallReply", function(e) {
  if(e.which == 13) {
      var pageId = getParameterByName('id');
      if(pageId != undefined || pageId != null || pageId != '') {     
        $.ajax({
          type: 'POST', 
          url: '?r=page/check-page-id', 
          data:{pageId},
          success: function(data) {
            var result = $.parseJSON(data);
            if(result.status != undefined && result.status == true) {
                var id = $(this).attr('data-id');
                var $msg = $(this).val().trim();
                if(id != undefined && $pageId != undefined && $pageId != '') {
                          if($msg) {
                              $.ajax({ 
                                  url: '?r=messages/get-category',
                                  type: 'POST',
                                  data: {id: id},
                                  success: function(result) {
                                      var resultFilter = JSON.parse(result);
                                      if(resultFilter.status) {
                                          var category = resultFilter.category;
                                          var SendMsgArray= {
                                              'from_id': $pageId,
                                              'to_id': id,
                                              'pageSELF' : data2.id,
                                              'pageOTHER' : id,
                                              'reply': $msg,
                                              'type': 'text',
                                              'category': 'page'
                                          };
                        
                                          $('textarea#inputPageWallReply').val('');
                    socket.emit('sendMessage', SendMsgArray);
                                      } else if(resultFilter.status == false && resultFilter.reason != undefined) {
                                          if(resultFilter.reason == 'block') {
                                              var by = resultFilter.by;
                                              if(by == 'self') {
                                                  $this = '<span>Unblock Messages</span>';
                                                  manageBlockConverasion($this);
                                              } else {
                                              }
                                             
                                          }
                                      }           
                                  }
                              });
                          }
                      }
                  }                  
            }
          
        });
      }
    }
});

$('body').on("keyup", "input.chatwallsearch", function(e) {
    var $key = $(this).val().trim();
    if($key) {
        var $searchFor = $('.left-section .nav').find('.active').attr('data-cls');
        //if($searchFor) {
            var data= {
          //      'category': $searchFor,
                'key': $key
            };

            $.ajax({ 
            url: '?r=messages/search-from-messages',
            type: 'POST',
            data: data,
            success: function(result) {
                var resultFilter = JSON.parse(result);
                if(resultFilter.length == 0) {
                    var $selectorAll = $('.chat-tabs .tab-content').find('#chat-search');
                    $selectorAll.html('<span class="ctitle">Search</span><div class="nice-scroll recentchat-scroll" tabindex="8" style="overflow-y: hidden; outline: none;"><ul class="chat-ul chat-label-notice"><li class="label-notice"><div class="no-listcontent">No record found.</div></li></ul></div>');
                } else {
                    socket.emit('searchUserStatus', data2.id, resultFilter);
                }
            },
            complete: function (result) {
                initNiceScroll(".nice-scroll");
                scrollMessageBottom();
                initGalleryImageSlider();
            }
        });
        //}
    } else {
        var $selectorAll = $('.chat-tabs .tab-content').find('#chat-search');
        $selectorAll.html('');
    }
});

$(document).on('click', '.contact_info_action', function() {
  if($('.right-section').find('.current-messages').find('.mainli.active').length) {
    $id = $('.right-section').find('.current-messages').find('.mainli.active').attr('id');
    $id = $id.replace("li_msg_","");
    if($id) {
      $.ajax({ 
        url: '?r=messages/getcontactinfo',
        type: 'POST',
        data: {$id},
        success: function(result) {
          $('.contact_info_modal').html(result);
          setTimeout(function() {
            $('.contact_info_modal').removeClass('showsidemodal_right');
            $('.contact_info_modal').addClass('hidesidemodal_right'); 

            $person_status = $('.person_status').html().trim();
            $usercountry = $('.usercountry').html().trim();
            $usertime = $('.usertime').html().trim();
            $timeerLabel = $person_status+'|&nbsp;'+$usercountry+'&nbsp;|&nbsp;'+$usertime;

            $('.contact_info_modal').find('.contact_user_lastseen').html($timeerLabel);
          },400);
        }
      });
    }  
  }
});

$(document).ready(function () {
    $w = $(window).width();
    if($w > 799) {
      $('.messages-wrapper').addClass('bigscreen');
    } else {
      $('.messages-wrapper').removeClass('bigscreen');
    }
    
    getRecentMessagsUsers('0', 'no');
    setMsgTextArea();

    $('.contact_info_action').click(function() {
      $('.contact_info_modal').removeClass('showsidemodal_right');
      $('.contact_info_modal').addClass('hidesidemodal_right');
    });

    manageMessagePage();        
    $(".btn-msg-send").hide();
    $('.pac-container:nth-child(2)').css('width', '270px');

});

$(document).on('click', '.emotion-list a', function() {
    $('.emotion-holder.gifticonclick').find('i').trigger('click');
    $obj = $('#inputMessageWall');
    sendICON($obj);
});

$(document).on("focus change paste keyup", "#inputMessageWall", function(e){
    $obj = $(this);
    sendICON($obj);
});

$('body').on("keyup", "input.messagewallsearch", function(e) {
    var $key = $(this).val().trim();
    if($key != '') {
        var data= {
          'category': 'inbox',
          'key': $key
        };

        $.ajax({ 
          url: '?r=messages/search-from-messages',
          type: 'POST', 
          data: data,
          success: function(result) { 
              console.log(result);
              var resultFilter = JSON.parse(result);
              if(resultFilter.length>0) {
                socket.emit('fillwithlabel', data2.id, resultFilter);
              } else {
                $htmlblock = '<li class="label-notice"><div class="no-listcontent">No record Found.</div></li>';
                var $selectorAll = $('#messages-inbox').find('.users-display');
                $selectorAll.html($htmlblock);
              }
          },
          complete: function (result) {
              initNiceScroll(".nice-scroll");
              scrollMessageBottom();
              initGalleryImageSlider();
          }
        });
    } else {
      $('.messagewallsearch').val('');
      getRecentMessagsUsers('0', 'no');
    }
});

$('body').on("keyup", "input.messagewallsearchforpage", function(e) {
    var $key = $(this).val().trim();
    if($key != '') {
        var $pageID = getParameterByName('id');
        if($pageID != undefined || $pageID != '') {
            $.ajax({ 
              url: '?r=messages/search-from-messages-for-page',
              type: 'POST',
              data: {
                key: $key,
                pageID: $pageID
              },
            success: function(result) {
                var resultFilter = JSON.parse(result);
                if(resultFilter.status != undefined && resultFilter.status == true) {
                var $selectorAll = $('.left-section').find('.tab-content').find('ul.users-display');
                $selectorAll.html('');
                resultFilterRes = resultFilter.data;
                $.each(resultFilterRes, function(i, item) {
                    var fullname = item.fullname;
                    var id = item.id;
                    var thumbnail = item.thumb;
                    var cls = '';
                    if(i == 0) {
                        cls='active';
                    }
                    $liBox = $('<li><a href="javascript:void(0)" class="'+cls+'" id="msg_'+id+'" onclick="openMessage(this);"><span class="muser-holder"> <span class="imgholder"><img src="'+thumbnail+'"></span><span class="descholder"><h5>'+fullname+'</h5><span class="timestamp">4:45 pm</span><span class="newmsg-count"></span></span></span></a></li>');
                    $selectorAll.append($liBox);

                    if(i == 0) {
                        isReadMessage(id);
                        $liBox = $('<li class="mainli parentli-'+id+' active" id="li_msg_'+id+'"><div class="msgdetail-list nice-scroll"><div class="msglist-holder images-container"><ul class="outer"></ul></div></li>');
                        $(".right-section .current-messages").html('');
                        $(".right-section .current-messages").html($liBox);
                            $('textarea#inputMessageWall').attr('data-id', id);
                            var requstdata = {
                                from_id: id,
                                to_id: data2.id,
                                category: 'page',
                                start: 0,
                                limit: 20
                            }
                            callHistoryForMessageWall(requstdata);
                    }
                });  
                }              
            },
            complete: function (result) {
                initNiceScroll(".nice-scroll");
                scrollMessageBottom();
                initGalleryImageSlider();
            }
        });
      }
    } else {
      getRecentMessagsUsers('0', 'no');
    }
});
  
$(document).on('change', '#attach_file_add', function(e) {
    var liCls = $('.left-section .tab-content').find('.active').attr('id');
        liCls = liCls.replace('messages-', '');
    var $this = $('textarea#inputMessageWall');
    var $sendTo = $($this).attr('data-id');
    if($sendTo) {
         $.ajax({ 
                url: '?r=messages/get-category',
                type: 'POST',
                data: {id: $sendTo},
                success: function(result) {
                    var resultFilter = JSON.parse(result);
                    if(resultFilter.status) {

                         var file = e.originalEvent.target.files[0],
                                reader = new FileReader();
                            reader.onload = function(evt) {
                                var base64image = evt.target.result;
                                var imageData = base64image.split(',');
                                
                                if($sendTo) {              
                                    var sendData = {
                                        'fileName' : file.name,
                                        'base64image' : base64image,
                                        'to_id' : $sendTo,
                                        'from_id' : data2.id,
                                        'category' : liCls 
                                    }

                                    socket.emit('userImage', sendData);
                                     //initNiceScroll(".nice-scroll");
                                    scrollChatBottom();
                                    scrollMessageBottom();
                                }
                            };
                            reader.readAsDataURL(file);
                      } else if(resultFilter.status == false && resultFilter.reason != undefined) {
                            if(resultFilter.reason == 'block') {
                                var by = resultFilter.by;
                                if(by == 'self') {
                                    $label = result.label;
                                    var disText = $(".discard_md_modal .discard_modal_msg");
                                    var btnKeep = $(".discard_md_modal .modal_keep");
                                    var btnDiscard = $(".discard_md_modal .modal_discard");
                                    disText.html($label);
                                    btnKeep.html("Unblock Messages");
                                    btnDiscard.html("Ok");
                                    btnKeep.attr('onclick', 'manageBlockConverasion("<span>Unblock Messages</span>")');
                                    $(".discard_md_modal").modal("open");
                                } else {
                                    Materialize.toast('This person isn\'t receiving messages from you at the moment.', 2000, 'red');
                                }
                            }
                        }
                }
            });            
    }
});

$(document).on('change', '#messageWallFileUploadPage', function(e) {
    var $parent = $(this).parents('.addnew-msg').find('textarea#inputPageWallReply');
    if($parent.length) {
       var POID = $($parent).attr('data-id');
       var PID = getParameterByName('id');
       if(POID != undefined && PID != undefined) {
         $.ajax({ 
                url: '?r=messages/get-category',
                type: 'POST',
                data: {id: PID},
                success: function(result) {
                    var resultFilter = JSON.parse(result);
                    if(resultFilter.status) {
                       var file = e.originalEvent.target.files[0],
                            reader = new FileReader();
                            reader.onload = function(evt) {
                              var base64image = evt.target.result;
                              var imageData = base64image.split(',');
                              var sendData = {
                                'from_id': PID,
                                'to_id': POID,
                                'pageOTHER': POID,
                                'pageSELF' : data2.id,
                                'fileName' : file.name,
                                'base64image' : base64image,
                                'category' : 'page' 
                              }
                              socket.emit('userImage', sendData);
                               //initNiceScroll(".nice-scroll");
                              scrollChatBottom();
                              scrollMessageBottom();
                            };
                            reader.readAsDataURL(file);
                      } else if(resultFilter.status == false && resultFilter.reason != undefined) {
                            if(resultFilter.reason == 'block') {
                                var by = resultFilter.by;
                                if(by == 'self') {
                                    $this = '<span>Unblock Messages</span>';
                                    manageBlockConverasion($this);
                                } else {
                                }
                            }
                        }
                 }
            });            
        }
    }
});   

$('body').on("keypress", "#inputMessageWall", function(e) {
  if(e.which == 13) {
      var $this = $(this); 
      var id = $this.attr('data-id');
      var $msg = $this.val().trim();
      if(id) {
          if($msg) {
              $.ajax({ 
                  url: '?r=messages/get-category',
                  type: 'POST',
                  data: {id: id}, 
                  success: function(result) {
                      var resultFilter = JSON.parse(result);
                      if(resultFilter.isOnHitEnter != undefined && resultFilter.isOnHitEnter == 'yes') {
                        if(resultFilter.status) {
                            var category = resultFilter.category;
                            var SendMsgArray= {
                                'from_id': data2.id,
                                'to_id': id,
                                'reply': $msg,
                                'type': 'text',
                                'category': category
                            };
                            $this.val('');
                            sendICON($this);
                            socket.emit('sendMessage', SendMsgArray);
                        } else if(resultFilter.status == false && resultFilter.reason != undefined) {
                            if(resultFilter.reason == 'block') {
                                var by = resultFilter.by;
                                if(by == 'self') {
                                  $label = result.label;
                                  var disText = $(".discard_md_modal .discard_modal_msg");
                                  var btnKeep = $(".discard_md_modal .modal_keep");
                                  var btnDiscard = $(".discard_md_modal .modal_discard");
                                  disText.html($label);
                                  btnKeep.html("Unblock Messages");
                                  btnDiscard.html("Ok");
                                  btnKeep.attr('onclick', 'manageBlockConverasion("<span>Unblock Messages</span>")');
                                  $(".discard_md_modal").modal("open");
                                } else {
                                  var disText = $(".discard_md_modal .discard_modal_msg");
                                  var btnDiscard = $(".discard_md_modal .modal_discard");
                                  disText.html("This person isn\'t receiving messages from you at the moment.");
                                  btnDiscard.html("Ok");
                                  $(".discard_md_modal").modal("open");
                                }
                            }
                        }
                      }  else {
                        $(".write-msg.nice-scroll").getNiceScroll().resize();
                      }         
                  }
              });
          }
      }
  }
}); 

$('body').on('click', '.deleteSocketConversation', function() {
    var ActiveLiFromMsg = $('.right-section ul.current-messages').find('li.active');
    var id = ActiveLiFromMsg[0].id;
    id = id.replace('li_msg_', '');
    if(id != undefined || id != '') {
    var activecls =  $('.left-section').find('ul.nav-tabs').find('.active').attr('data-cls');
    $.ajax({
        type: 'POST',
        url: '?r=messages/delete-socket-conversation',
        data: { id: id, category: activecls },
        success: function(responce) {
            if(responce) {
                // remvoe message history
                ActiveLiFromMsg.find('ul.outer').html('');

                // remvoe message user from message list
                var user = $(".left-section .tab-content").find(".tab-pane.active").find("ul.users-display").find("li").find("a#msg_"+id);
                user.parent('li').remove();
                
                // remvoe chat from chat box if it exists..
                if($('.chat-window ul.mainul').find('li#li-'+id).length) {
                    $('.chat-window ul.mainul').find('li#li-'+id).find('chat-conversation').find('ul.outer').html('');
                }
            }
        }
    });
  }
});

$('body').on('click', '.deleteSigMessage', function() {
  var ids = [];
  $this = $(this);
  if($this.parents('.msg-handle').length) {
    $.each($this.parents('.msg-handle').find('.message_block'), function(b, h) {
        $id = $(h).attr('data-id');
        ids.push($id);
    });
  }

  if (typeof ids !== 'undefined' && ids.length > 0) {
    $.ajax({
      url: '?r=messages/delete-selected-message',
      type: 'POST',
      data: {
        id: ids
      },
      success: function(data) {
        if (data) {
          if($this.parents('.msg-handle').length) {
            $this.parents('.msg-handle').remove();
            Materialize.toast('Deleted.', 2000, 'green');
          }
        }
      }
    });
  } else {
    Materialize.toast('no message box is checked for delete.', 2000, 'red');
  }
});

$('body').on('click', '.addstarmessage', function() {
  var ids = [];
  $this = $(this);
  if($this.parents('.msg-handle').length) {
    $.each($this.parents('.msg-handle').find('.message_block'), function(b, h) {
        $id = $(h).attr('data-id');
        ids.push($id);
    });
  }

  
  if (typeof ids !== 'undefined' && ids.length > 0) {
    $.ajax({
      url: '?r=messages/addstarmessage',
      type: 'POST',
      data: {
        id: ids
      },
      success: function(response) {
        var $result = JSON.parse(response);
        if($result.status != undefined && $result.status == true) {
            $label = $result.label;
            Materialize.toast($label, 2000, 'green');
        }
      }
    });
  } else {
    Materialize.toast('no message box is checked for delete.', 2000, 'red');
  }
});

$('body').on('click', '.deleteConvHistory', function() {
    $selector = $('.right-section ul.current-messages').find('li.active');
    if($selector.length) { 
      $id = $selector.attr('id');
      $id = $id.replace('li_msg_', '');
      if($id != undefined || $id != '') {
        $.ajax({
          type: 'POST',
          url: '?r=messages/delete-socket-conversation', 
          data: { id: $id},
          success: function(responce) {
              if(responce) {
                if (window.location.href.indexOf("messages") > -1) {
                  // removed right box
                  $rightSelection = $('.right-section').find('.current-messages').find('li#li_msg_'+$id);
                  if($rightSelection.length) {
                    $rightSelection.remove();
                    if($rightSelection.hasClass('active')) {
                      $('.msgwindow-name.curtusrbasicinfo').html('');
                    }
                  }

                  // removed left box
                  $leftSelection = $('.left-section').find('.users-display').find('li');
                  if($leftSelection.find('a#msg_'+$id).length) {
                    $leftSelection.find('a#msg_'+$id).parents('li').remove();
                    if($('.left-section').find('.users-display').find('li').length) {
                      $('.left-section').find('.users-display').find('li:first').find('a').trigger('click');
                    } else {
                      $liblock = '<li class="label-notice"><div class="no-listcontent">No record Found.</div></li>';        
                      $('.left-section').find('.users-display').html($liblock);
                    }
                  }
                } else {

                }
              }
            }
        });    
      }
    }

});

function getTime() {
    var now = new Date();
    var hh = now.getHours();
    var min = now.getMinutes();
            
    var ampm = (hh>=12)?'pm':'am';
    hh = hh%12;
    hh = hh?hh:12;
    hh = hh<10?'0'+hh:hh;
    min = min<10?'0'+min:min;
            
    var time = hh+":"+min+" "+ampm;
    return time;
}

function STOPgetuserlabel() {
  clearInterval(getuserlabel);
}

function STARTgetuserlabel() {
  $selector = $('.current-messages').find('.mainli.active')
  if($selector.length) {
    getuserlabel = setInterval(function(){
        $id = $selector.attr('id');
        $id = $id.replace('li_msg_', '');
        if($id) {
          $codeKey = 'AHBSAJ';
          socket.emit('getuserlabel', $id, data2.id, $codeKey);
        }
    }, 10000);
  } else {
    STARTgetuserlabel();
  }
}

function SINGLEFIREgetuserlabel() {
  $selector = $('.current-messages').find('.mainli.active')
  if($selector.length) {
    $id = $selector.attr('id');
    $id = $id.replace('li_msg_', '');
    if($id) {
      $codeKey = 'AHBSAJ';
      socket.emit('getuserlabel', $id, data2.id, $codeKey);
    }
  }
}

function msgToneActive() {
  var audioElement = document.createElement('audio');
  audioElement.setAttribute("src", "sounds/new_notification.mp3");
  audioElement.play();
}

function sendICON($obj) {
  $obj = $($obj);
  $.ajax({  
    url: '?r=messages/sendiconstatus',
    success: function(data) {
      if(data == 'No') {
        if($obj.val().length>0) {
          $(".btn-msg-send").show("100");
        } else {
          $(".btn-msg-send").hide("300");
        }
      } else {
         $(".btn-msg-send").hide();        
      }
    }
  });

}

function dstrToUTC(ds) {
    var dsarr = ds.split("/");
    var mm = parseInt(dsarr[0],10);
    var dd = parseInt(dsarr[1],10); 
    var yy = parseInt(dsarr[2],10);
    return Date.UTC(yy,mm-1,dd,0,0,0); 
}

function datediff(ds1,ds2) {
     var d1 = dstrToUTC(ds1);
     var d2 = dstrToUTC(ds2);
     var oneday = 86400000;
     return (d2-d1) / oneday;    
}

function getDayName(date) {
    // Name of the days as Array
    var dayNameArr = new Array ("Sunday", "Monday", "Tuesday", "Wednesday", "Thrusday", "Friday", "Saturday");
    var dateArr = date.split('/'); // split the date into array using the date seperator
    var month = eval(dateArr[0]); 
    var day = eval(dateArr[1]);
    var year = eval(dateArr[2]);
    // Calculate the total number of days, with taking care of leapyear 
    var totalDays = day + (2*month) + parseInt(3*(month+1)/5) + year + parseInt(year/4) - parseInt(year/100) + parseInt(year/400) + 2;
    // Mod of the total number of days with 7 gives us the day number
    var dayNo = (totalDays%7);
    // if the resultant mod of 7 is 0 then its Saturday so assign the dayNo to 7
    if(dayNo == 0){
    dayNo = 7;
    }
   return dayNameArr[dayNo-1]; // return the repective Day Name from the array
}
 
function getTimeWithAMPM(date) { 
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = ('0' + hours).slice(-2) + ':' + ('0' + minutes).slice(-2) + ' ' + ampm;
    return strTime;
}

function isReadMessage(id) {
    if(id) {
        $.ajax({ 
            url: '?r=messages/is-read-message',
            type: 'POST',
            data: {id: id},
            success: function(data) {
                var result = JSON.parse(data);
                if(result.status == true) {
                    var count = result.count;
                    if(count == '') {
                        $('.not-messages').find('#msg_budge').html('');
                        $('.not-messages').find('#msg_budge').hide();
                    } else {
                        $('.not-messages').find('#msg_budge').show();
                        $('.not-messages').find('#msg_budge').html(count);
                    }
                }

                var getli = $('.not-messages').find('.msg-listing').find('li.recid'+id);
                if(getli.length) {
                    getli.addClass('read');
                }
            }
        });     
   }
}

function setReadUnread(obj) {
   var li = $(obj).parents('li.mainli');
   var msgid = li.attr('data-msgid');
   if(msgid) {
      $.ajax({ 
          url: '?r=messages/set-is-read-message',
          type: 'POST',
          data: {msgid},
          success: function(data) {
              if(data) {
                  //
              }
          }
      });     
   }
}  

function loadNotificationCount() {
    $.ajax({ 
        url: '?r=messages/get-unread-msg',
        success: function(data) {
            var getli = $(".not-messages div.dropdown").find('span.badge');
            if(data>0) {
                getli.css('display', 'block');
                getli.html(data);
            } else {
                getli.css('display', 'none');
                getli.html('');
            }
        }
    });    
}

function setreadall() {
  $.ajax({ 
    url: '?r=messages/set-read-all',
    success: function(data) {
      $('.not-messages').find('.msg-listing').find('li').not( ".read" ).each(function() {
          $(this).addClass('read');
      })
    }
  });  
}

function messagegifttextCounter(obj) {
  var tt = $(obj).val().length;
  if(tt<=40){
    var charLeft=40 - tt;
    $('#usegift-popup').find('.tt-counter').html(charLeft);
  } else {
    var newvalue = $(obj).val();
    var oldvalue = newvalue.substring(0, 40);
    $(obj).val(oldvalue);
  }
} 

function sendmessagewithgift($isPricePermission=false) {
  if(window.location.href.indexOf("messages") > -1) {
    var addmoreto = $('.current-messages').find('li.active').attr('id');
  } else {
    var addmoreto = $('.chat-section').find('.mainul').find('li.active').attr('id');
  }

  if(addmoreto) {
    addmoreto = addmoreto.replace('li_msg_', '');
    addmoreto = addmoreto.replace('li-', '');
    var $imgsrc = $('#usegift-popup').find('.carousel_position').find('.carousel-item.active');
    if($imgsrc.length) {
      $imgsrc = $($imgsrc).attr('data-class');

      goarray = addUserForShareWith.slice();
      goarray.push(addmoreto);

      if($imgsrc) {
        var $message = $('#usegift-popup').find('textarea#message').val();
     
        if($isPricePermission) {
          $imgsrc = $imgsrc +'|||||||'+$message;

          var SendMsgArray= {
              'from_id': data2.id,
              'to_id': goarray,
              'reply': $imgsrc,
              'type': 'gift',
              'category': 'inbox'
          };
          socket.emit('sendMessagewithgift', SendMsgArray);
          setTimeout(function () { 
            getRecentMessagsUsers('0', 'no');
          }, 1000);
        } else {
          $.ajax({ 
              url: '?r=messages/get-category-with-gift',
              type: 'POST', 
              data: {goarray},
              success: function(result) {
                  var disText = $(".discard_md_modal .discard_modal_msg");
                  var btnKeep = $(".discard_md_modal .modal_keep");
                  var btnDiscard = $(".discard_md_modal .modal_discard");
                  disText.html(result);
                  btnKeep.html("Cancel");
                  btnDiscard.html("Continue");
                  btnDiscard.attr('onclick', 'sendmessagewithgift(true)');
                  $(".discard_md_modal").modal("open");
              }
          });
        }
      }
    }
  } 
}

function callBuddies() {
  socket.emit('callBuddies', data2.id);
}

function callOnlineUsers() {
  socket.emit('callOnlineUsers', data2.id);
}

function getChatUsersForRecent() {
    $.ajax({
        url: '?r=messages/recent-chat-user-list',
        success: function(data) {
            var $result = JSON.parse(data);
            if($result.length) {
                socket.emit('getChatUsersForRecentStatus', $result, data2.id);
            } else {
                $selectore = $('.chat-tabs').find('.tab-content').find('#chat-recent').find('.nice-scroll');
                if($selectore.length) {
                    $li = '<ul class="chat-ul chat-label-notice"><li class="label-notice"><div class="no-listcontent">No memeber is online</div></li></ul>';
                    $selectore.html($li);
                }
            }
        }
    });         
}

function chatLiClicked(obj) {
    var id = $(obj).attr('data-id');
    id=id.replace("chat_","");
    isReadMessage(id);
    var requstdata = {
        from_id: id,
        to_id: data2.id,
        start: 0,
        limit: 20
    }
    callHistoryForChatWall(requstdata);
}

function callHistoryForChatWall(data) {

    if(data) {
        $.ajax({ 
            url: '?r=messages/load-history-chat',
            type: 'POST',
            data: data,
            success: function(result) {
                var resultFilter = JSON.parse(result);
                // Set Empty
                $('.chat-window ul.mainul').find('li#li-'+resultFilter.to_id).find('ul.outer').html('');
                
                $.each(resultFilter, function(i, v) {
                    var d = new Date();
                    var msgid = v._id;
                    var $from_id = v.from_id;
                    var $fullname = v.fullname;
                    var $msg = v.reply;
                    var $thumb = v.thumb;
                    var $to_id = v.to_id;
                    var $type = v.type; 
                    var $datetime = v.created_at.sec;

                    var $datetimedisplay = new Date(1000*$datetime);
                    var $time = getTimeWithAMPM($datetimedisplay);
                    var year = $datetimedisplay.getFullYear();

                    var existingDate = ('0' + ($datetimedisplay.getMonth()+1)).slice(-2) + "/" +  ('0' + $datetimedisplay.getDate()).slice(-2) + "/" + $datetimedisplay.getFullYear();


                    var label = '';
                    var previousDate = '';
                    if(resultFilter[i + 1] != undefined) {
                        var previous = resultFilter[i + 1].created_at.sec;
                        previous = new Date(1000*previous);
                        previousDate = ('0' + (previous.getMonth()+1)).slice(-2) + "/" +  ('0' + previous.getDate()).slice(-2) + "/" + previous.getFullYear();
                    }

                    if(previousDate != existingDate) {
                        var currentDate = ('0' + (d.getMonth()+1)).slice(-2) + "/" +  ('0' + d.getDate()).slice(-2) + "/" + d.getFullYear();
                        if(currentDate != undefined && existingDate != undefined) {
                            var days = datediff(existingDate, currentDate);
                            if(days <7) {
                                if(days <2) {
                                    if(days == 0) {
                                        label = 'Today';
                                    } else if(days == 1) {
                                        label = 'Yesterday';
                                    }   

                                } else {
                                   var dayName = getDayName(existingDate);
                                   label = dayName;
                                }
                            } else {
                                var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

                                monthName = monthNames[('0' + ($datetimedisplay.getMonth()+1)).slice(-2)];

                                var tempexistingDate = ('0' + $datetimedisplay.getDate()).slice(-2) + " " + monthName + " " + $datetimedisplay.getFullYear();
                                label = tempexistingDate;
                            }

                            label = '<li class="date-divider"><span>'+label+'</span></li>';
                        }
                    }



                    var fileuploadcls = '';
                    
                    if($type == 'image') {
                        fileuploadcls = 'fileholder';
                        $msg = '<div class="msg-img"><div class="imgfigure"> <figure data-pswp-uid="1"><a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><img src="'+$msg+'"></a></figure></div><a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a></div>';
                    } else if($type == 'gift') {
                        $msgsplit = $msg.toString().split('|||||||');
                        $code = '';
                        $text = '';
                        if($msgsplit[0] != undefined) {
                            $code = $msgsplit[0];
                        }

                        if($msgsplit[1] != undefined && $msgsplit[1] != null) {
                            $text = $.emoticons.replace($msgsplit[1]);
                        } else {
                          $text = '';
                        }

                        $msg = '<div class="gift-img"><p>'+$text+'</p> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i></a></div>';
                    } else {
                      if($msg != undefined && $msg != null) {
                        $msg = $.emoticons.replace($msg);
                      } else {
                        $msg = '';
                      }
                    }

                    if($from_id == data2.id) {
                        if($('.chat-window ul.mainul').find('li#li-'+$to_id).find('ul.outer').find('li').length) {
                            if( $('.chat-window ul.mainul').find('li#li-'+$to_id).find('ul.outer').find('li:first').hasClass('received')) {
                                $chatBox = $(label+'<li class="chatli"><ul class="submsg"><li><div class="msgholder '+fileuploadcls+'">'+$msg+'</div> <div class="timestamp">'+$time+'</div></li></ul> </li');
                                $('.chat-window ul.mainul').find('li#li-'+$to_id).find('ul.outer').prepend($chatBox);
                            } else  {
                                $chatBox = $(label+'<li><div class="msgholder '+fileuploadcls+'">'+$msg+'</div> <div class="timestamp">'+$time+'</div></li>');
                                $('.chat-window ul.mainul').find('li#li-'+$to_id).find('ul.outer').find('li:first').find('ul.submsg').prepend($chatBox); 
                            } 
                        } else {
                            $chatBox = $(label+'<li class="chatli"><ul class="submsg"><li><div class="msgholder '+fileuploadcls+'">'+$msg+'</div> <div class="timestamp">'+$time+'</div></li></ul> </li');
                            $('.chat-window ul.mainul').find('li#li-'+$to_id).find('ul.outer').prepend($chatBox);
                        }
                    } else { 
                        if( $('.chat-window ul.mainul').find('li#li-'+$from_id).find('ul.outer').find('li:first').hasClass('received')) {
                            $chatBox = $(label+'<li><div class="msgholder '+fileuploadcls+'">'+$msg+'</div> <div class="timestamp">'+$time+'</div></li>');
                            $('.chat-window ul.mainul').find('li#li-'+$from_id).find('ul.outer').find('li:first').find('ul.submsg').prepend($chatBox); 
                        } else  {
                            $chatBox = $(label+'<li class="chatli received msg-income"><div class="imgholder"><img src="'+$thumb+'"></div><ul class="submsg"><li><div class="msgholder '+fileuploadcls+'">'+$msg+'</div> <div class="timestamp">'+$time+'</div></li></ul> </li');
                            $('.chat-window ul.mainul').find('li#li-'+$from_id).find('ul.outer').prepend($chatBox);
                        }
                    }
                });
                //initNiceScroll(".nice-scroll");
                scrollChatBottom();
                initGalleryImageSlider();
            }
        });
    }
}

function messageSendFromMessage() {
    var $this = $('textarea#inputMessageWall');
    var id = $($this).attr('data-id');
    var $msg = $this.val().trim();
    if(id) {
        if($msg) {
            $.ajax({ 
                url: '?r=messages/get-category',
                type: 'POST',
                data: {id: id},
                success: function(result) {
                    var result = JSON.parse(result);
                    if(result.status) {
                        var category = result.category;
                        var SendMsgArray= {
                            'from_id': data2.id,
                            'to_id': id,
                            'reply': $msg,
                            'type': 'text',
                            'category': category
                        };

                        $this.val('');
                        sendICON($this);
                        socket.emit('sendMessage', SendMsgArray);
                    } else if(result.status == false && result.reason != undefined) {
                        if(result.reason == 'block') {
                            var by = result.by;
                            if(by == 'self') {
                              $label = result.label;
                              var disText = $(".discard_md_modal .discard_modal_msg");
                              var btnKeep = $(".discard_md_modal .modal_keep");
                              var btnDiscard = $(".discard_md_modal .modal_discard");
                              disText.html($label);
                              btnKeep.html("Unblock Messages");
                              btnDiscard.html("Ok");
                              btnKeep.attr('onclick', 'manageBlockConverasion("<span>Unblock Messages</span>")');
                              $(".discard_md_modal").modal("open");
                            } else {
                              var disText = $(".discard_md_modal .discard_modal_msg");
                              var btnDiscard = $(".discard_md_modal .modal_discard");
                              disText.html("This person isn\'t receiving messages from you at the moment.");
                              btnDiscard.html("Ok");
                              $(".discard_md_modal").modal("open");
                            }
                        }
                    }             
                }
            });
        }
    }
}

function messageSendFromMessagePage(obj) {
    var $parent = $(obj).parents('.addnew-msg').find('textarea#inputPageWallReply');
    if($parent.length) {
       var POID = $($parent).attr('data-id');
       var PID = getParameterByName('id');
       if(POID != undefined && PID != undefined) {
          var $msg = $($parent).val().trim();
          if($msg != undefined) {
              $.ajax({ 
                url: '?r=messages/get-category',
                type: 'POST',
                data: {id: PID},
                success: function(result) {
                    var result = JSON.parse(result);
                    if(result.status) {
                        var category = result.category;
                        var SendMsgArray= {
                            'from_id': PID,
                            'to_id': POID,
                            'pageOTHER': POID,
                            'pageSELF' : data2.id,
                            'reply': $msg,
                            'type': 'text',
                            'category': 'page'
                        };
                        

                        $($parent).val('');

                        socket.emit('sendMessage', SendMsgArray);
                    } else if(result.status == false && result.reason != undefined) {
                        if(result.reason == 'block') {
                            var by = result.by;
                            if(by == 'self') {
                                $this = '<span>Unblock Messages</span>';
                                manageBlockConverasion($this);
                            } else {
                            }
                        }
                    }             
                }
            });
          }
       }
    }
}

function messageSendFromChat() {
  var $this = $('.chat-window .mainul').find('li.active').find('textarea#inputChatWall');
  var pagename = '';
  pagename = $('#pagename').val();
  if(pagename == 'page'){
    var $this = $('.chat-window .mainul').find('li.active').find('textarea#inputPageWall');
  }
  var $msg = $this.val().trim();
      if($msg != '') {
          var id = $($this).attr('data-id');
  if(pagename == 'page'){
    id = id.replace('li-','');
  }

  if(id) {
      $.ajax({ 
          url: '?r=messages/get-category',
          type: 'POST',
          data: {id: id},
          success: function(result) {
              var resultFilter = JSON.parse(result);              
              if(resultFilter.status) {
                  var category = resultFilter.category;
                  var ownerid = '';
                  if(category == 'page') {
                    ownerid = $($this).attr('data-ownerid');
                  }
                  var SendMsgArray= {
                      'from_id': data2.id,
                      'to_id': id,
                      'pageOTHER': ownerid,
                      'pageSELF' : data2.id,
                      'reply': $msg,
                      'type': 'text',
                      'category': category
                  };
                  $this.val('');
                  socket.emit('sendMessage', SendMsgArray);
              } else if(resultFilter.status == false && resultFilter.reason != undefined) {
                  if(resultFilter.reason == 'block') {
                      var by = resultFilter.by;
                      if(by == 'self') {
                          $this = '<span>Unblock Messages</span>';
                          manageBlockConverasion($this);
                      } else {
                      }
                      
                  }
              }             
          }
      });
    }
  }
}  

function getRecentMessagsUsers($isStart=0, $isStop='no') { 
  $.ajax({ 
      type: 'POST',
      url: '?r=messages/recent-messages-user-list',
      success: function(data) { 
          var $dataFilter = JSON.parse(data);
          var $category = 'inbox'; 
          $selector = $('#messages-inbox').find('.message-userlist').find('ul');
          if($isStart == 0 || $isStart == '') {
            $selector.html('');
          }
 
          if($dataFilter.result != undefined && $dataFilter.result.length >0) {
            $result = $dataFilter.result;
            socket.emit('recentuserlistfillwithlabel', data2.id, $result, $isStop);
          } else {  
            $li = $('<li class="label-notice"><div class="no-listcontent">No record Found.</div></li>');
            $selector.html($li);

            if($('.msgwindow-name.curtusrbasicinfo').length) {
              $('.msgwindow-name.curtusrbasicinfo').html('');
            }
          }

          var sparent=$(".messages-list .left-section #messages-"+$category);
          removeLoaderImg(sparent);
          setOpacity(sparent.find('.loading-holder'),1);
      },
      complete: function($dataFilter) {
          //var $dataFilter = JSON.parse(data);
          var $category = $dataFilter.category;
          var sparent=$(".messages-list .left-section #messages-"+$category);
          removeLoaderImg(sparent);
          setOpacity(sparent.find('.loading-holder'),1);
      }
  });
}

function messageLiClicked(obj) {
  var id = $(obj).attr('id');
  id=id.replace("msg_","");
  $(obj).removeClass('unread-msg');
  $(obj).find('.descholder').find('.newmsg-count').remove();
  isReadMessage(id);

  if($(obj).parents('li').hasClass('unreadmsgli')) {
    $(obj).parents('li').removeClass('unreadmsgli');
  }

  var category = $('.left-section .nav').find('li.active').attr('data-cls');
  if(category == undefined) {
      category = 'inbox';
  }

  $('textarea#inputMessageWall').attr('data-id', id);
  if(category == 'page') {
    $('.right-section .addnew-msg').find('textarea#inputPageWallReply').attr('data-id', id);
  }

  //check li exist or not...
  if(id != undefined) {
      var firstLi = $('.right-section ul.current-messages').find('li#li_msg_'+id);
      if(!firstLi.length) {
          $tempSel = $('.right-section').find('ul.current-messages');
          $li = '<li class="mainli" id="li_msg_'+id+'"> <div class="msgdetail-list nice-scroll" tabindex="18" style="opacity: 1; outline: none;"><div class="msglist-holder images-container"><ul class="outer"></ul></div></div></li>';
          $($tempSel).append($li)
      }

      var requstdata = {
          from_id: id,
          to_id: data2.id,
          category: category,
          start: 0,
          limit: 20
      }

      callHistoryForMessageWall(requstdata);
  }
}

function callHistoryForMessageWall(data) {
  if(data) {   
        $.ajax({ 
            url: '?r=messages/load-history-message',
            type: 'POST',
            data: data,
            beforeSend: function() {
              $IJASJKA = $('.right-section').find('.current-messages').find('.mainli.active');
              if($IJASJKA.length) {
                $activeUserId = $IJASJKA.attr('id');
                $activeUserId = $activeUserId.replace('li_msg_', '');
                $.ajax({
                  type: 'POST',
                  url: '?r=messages/getmsgsetoptions', 
                  data: { id: $activeUserId},
                  success: function(result) {
                    $('#setting_messages').html(result);
                    $('#setting_messages_xs').html(result);
                  }
                });
              }
            },
            success: function(result) {
              var resultFilter = JSON.parse(result);
                if(data.from_id == data2.id) { 
                  otherId = data.to_id; 
                } else { 
                  otherId = data.from_id; 
                }
              
                $('.right-section ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').html('');
                var oldLabel = '';
                var $isCheckForFullDateTimeLabel = true;
                var previousDate = '';
                var previousTime = '';
                var previousType = '';

                if(resultFilter.length >0) {
                    $.each(resultFilter, function(i, v) {
                        var today = new Date();
                        var currentDate = new Date();
                        var msgid = v._id;
                        var $fullname = v.fullname;
                        var $msg = v.reply;
                        var $thumb = v.thumb;
                        var $type = v.type;
                        var $from_id = v.from_id;
                        var lazyloadcls = '';
                        var $datetime = v.created_at.sec;
                        var $datetimedisplay = new Date(1000*$datetime);
                        var $time = getTimeWithAMPM($datetimedisplay);
                        var year = $datetimedisplay.getFullYear();
                        var seconds =  (previousTime - $datetime);
                        var uniqID = msgid+'_'+$datetime;

                        if(year = today.getFullYear) {
                            var $datetimedisplays = ('0' + ($datetimedisplay.getMonth()+1)).slice(-2) + "/" + ('0' + $datetimedisplay.getDate()).slice(-2) + ' ' +$time;
                            year = '';
                        } else {
                            var $datetimedisplays = ('0' + ($datetimedisplay.getMonth()+1)).slice(-2) + "/" + ('0' + $datetimedisplay.getDate()).slice(-2) + "/" + year + ' ' +$time;
                        }

                        var existingDate = ('0' + ($datetimedisplay.getMonth()+1)).slice(-2) + "/" +  ('0' + $datetimedisplay.getDate()).slice(-2) + "/" + $datetimedisplay.getFullYear();
                        var label = '';
                        if(previousDate != '' && previousDate != existingDate) {
                            var currentDate = ('0' + (today.getMonth()+1)).slice(-2) + "/" +  ('0' + today.getDate()).slice(-2) + "/" + today.getFullYear();
                            if(currentDate != undefined && existingDate != undefined) {
                                var days = datediff(existingDate, currentDate);
                                if(days <7) {
                                    if(days <2) {
                                        if(days == 0) {
                                            label = 'Today';
                                        } else if(days == 1) {
                                            label = 'Yesterday';
                                        }   

                                    } else {
                                       var dayName = getDayName(existingDate);
                                       label = dayName;
                                    }
                                } else {
                                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];
                                    monthName = monthNames[($datetimedisplay.getMonth()+1)];
                                    var tempexistingDate = ('0' + $datetimedisplay.getDate()).slice(-2) + " " + monthName + " " + $datetimedisplay.getFullYear();
                                    label = tempexistingDate;
                                    $isCheckForFullDateTimeLabe = false;
                                }
                                label = '<li class="date-divider"><span>'+label+'</span></li>';
                            }
                        }

                        if($isCheckForFullDateTimeLabel) {
                          var currentDate = ('0' + (today.getMonth()+1)).slice(-2) + "/" +  ('0' + today.getDate()).slice(-2) + "/" + today.getFullYear();
                          if(currentDate != undefined && existingDate != undefined) {
                              var days = datediff(existingDate, currentDate);
                              if(days <7) {
                                $datetimedisplays = $time;  
                              }
                          }
                        }

                        if($type == 'image') {
                            $readIcon = '';
                            if($from_id == data2.id) {
                              $readIcon = '<i class="mdi mdi-check-all msg-status"></i>';
                            }

                            if(previousDate == existingDate && previousSender == $from_id) {
                                $JIDSL = '<div class="msg-handle">'+$readIcon+'<span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$datetimedisplays+'</span> </figure> </span><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div>';

                               $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').find('li.msgli:first').find('.descholder').prepend($JIDSL);
                            } else if($from_id == data2.id) {
                              $JIDSL = label+'<li class="msgli msg-outgoing"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle"> <i class="mdi mdi-check-all msg-status"></i> <span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></a></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$datetimedisplays+'</span> </figure> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';
                               $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').prepend($JIDSL);
                            } else {
                              $JIDSL = label+'<li class="msgli received msg-income"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"><span class="message_block" data-id="'+msgid+'"> <img src="'+$thumb+'"></span> </div> <div class="descholder"> <div class="msg-handle"> <i class="mdi mdi-check-all msg-status"></i> <span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"> <img src="'+$msg+'"></a> <a href="'+$msg+'" class="download" download> <i class="mdi mdi-download"></i> </a> <span class="timestamp">'+$datetimedisplays+'</span> </figure> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';


                              $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').prepend($JIDSL);
                            }
                        } else if($type == 'gift') {
                            $msgsplit = $msg.toString().split('|||||||');
                            $code = '';
                            $text = '';
                            if($msgsplit[0] != undefined) {
                                $code = $msgsplit[0];
                            }

                            if($msgsplit[1] != undefined && $msgsplit[1] != 'undefined' && $msgsplit[1] != '' && $msgsplit[1] != null) {
                              $text = $.emoticons.replace($msgsplit[1]);
                              $text = '<p><span class="message_block" data-id="'+msgid+'">'+$text+'</span><span class="timestamp">'+$datetimedisplays+'</span></p>';
                            } else {
                              $text = '<span class="message_block" data-id="'+msgid+'"></span>';
                            }

                              $readIcon = '';
                            if(previousDate == existingDate && previousSender == $from_id) {
                              if($from_id == data2.id) {
                                $readIcon = '<i class="mdi mdi-check-all msg-status"></i>';
                              }
                              
                                $JIDSL = '<div class="msg-handle">'+$readIcon+' '+$text+' <span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                    $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').find('li.msgli:first').find('.descholder').prepend($JIDSL);
                            } else if($from_id == data2.id) {
                                $JIDSL = label+'<li class="msgli msg-outgoing"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle"><i class="mdi mdi-check-all msg-status"></i>'+$text+' <span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';
                                $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').prepend($JIDSL);
                            } else {
                                $JIDSL = label+'<li class="msgli received msg-income"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"> <img src="'+$thumb+'"> </div> <div class="descholder"> <div class="msg-handle">'+$text+'<span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';
                                $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').prepend($JIDSL);
                            }
                        } else {
                            if($msg != undefined && $msg != null) {
                              $msg = $.emoticons.replace($msg);
                            } else {
                              $msg = '';
                            }

                            if(previousDate == existingDate && previousSender == $from_id) {
                              if(seconds <60) {
                                $JIDSL = '<span class="message_block" data-id="'+msgid+'">'+$msg+'</span><br/>';
                                if(previousType == 'text') {
                                  $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').find('li.msgli:first').find('.descholder').find('.msg-handle:first').find('p:first').prepend($JIDSL);
                                } else {
                                  $JIDSL = '<div class="msg-handle" data-time="'+$datetime+'"><p><span class="message_block" data-id="'+msgid+'">'+$msg+'</span><span class="timestamp">'+$datetimedisplays+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"><div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>'; 
                                  $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').find('li.msgli:first').find('.descholder').prepend($JIDSL);
                                }
                              } else {
                                $JIDSL = '<div class="msg-handle" data-time="'+$datetime+'"><p><span class="message_block" data-id="'+msgid+'">'+$msg+'</span><span class="timestamp">'+$datetimedisplays+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;" ><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>'; 
                                $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').find('li.msgli:first').find('.descholder').prepend($JIDSL);
                              }
                            } else if($from_id == data2.id) {
                                 $JIDSL = label+'<li class="msgli msg-outgoing time"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle" data-time="'+$datetime+'"> <p class="onw"><span class="message_block" data-id="'+msgid+'">'+$msg+'</span><span class="timestamp">'+$datetimedisplays+'</span></p> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div> </div> </div> </li>'
                                 $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').prepend($JIDSL);
                            } else {
                               $JIDSL = label+'<li class="msgli received msg-income time"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"> <img src="'+$thumb+'"> </div> <div class="descholder"> <div class="msg-handle" data-time="'+$datetime+'"> <p class="two"><span class="message_block" data-id="'+msgid+'">'+$msg+'</span><span class="timestamp">'+$datetimedisplays+'</span></p> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';
                                $('.right-section').find('ul.current-messages').find('li#li_msg_'+otherId).find('ul.outer').prepend($JIDSL);  
                            }
                        }

                        oldLabel = existingDate;
                        previousDate = existingDate;
                        previousTime = $datetime;
                        previousSender = $from_id;
                        previousType = $type;
                    });
                }

            },
            complete: function (data) {
                //initNiceScroll(".nice-scroll");
                scrolltobottomMSG();
                scrollMessageBottom();
                initGalleryImageSlider();
                initDropdown();
               
                $('.right-section ul.current-messages').find('.msgdetail-list').css('opacity', 1);
            }
        });
    }
}
 
function messageOptionsClk() {
  var $selector = $(".allmsgs-holder").find("ul.current-messages").find('li.active');
  var $selectorLength = $selector.length;
  if($selectorLength) {
    var $id = $($selector).attr('id');
    $id = $id.replace("li_msg_", "");
    if($id) {
      $.ajax({
        type: 'POST',
        url: '?r=messages/message-user-status',
        data: {
          id: $id
        },
        success: function(response) {
          var $result = JSON.parse(response);
          if($result.status != undefined && $result.status == true) {
            $('.connections-search').find('ul').removeClass('dropdown-menu');
            $('.connections-search').find('ul').addClass('dropdown-menu');
            $('.messageOptionsClkoptionshtml').html($result.lis);
          }
        }
      });
    }
  } else {
    $('.connections-search').find('.dropdown-menu').removeClass('dropdown-menu');
  }
}

/* FUN manage mute conversation */ 
function manageMuteConverasion() {
  $selector = $('.right-section ul.current-messages').find('li.active');
  if($selector.length) {
    $id = $selector.attr('id');
    $id = $id.replace('li_msg_', '');
    if($id != undefined || $id != '') {
      $.ajax({   
        type: 'POST',
        url: '?r=messages/mute-user',
        data: {$id}, 
        success: function(response) {
          var $type=response.trim();  
          if($type=="mute") {         
            if($(".block-notice").css("display")=="none") {
              $(".mute-notice").slideDown().delay(3000).slideUp();
              var settingObj=$(".mute-setting");
              if(settingObj!=undefined) {
                settingObj.html("Unmute Coversation");
              }
            }
          } else {      
            if($(".block-notice").css("display")=="none") {
              $(".mute-notice").hide();
              var settingObj=$(".mute-setting");
              if(settingObj!=undefined){
                settingObj.html("Mute Coversation");
              }
            }
          }
        }
      });
    }
  }
}

function archiveChat() {
  var $selector = $(".allmsgs-holder").find("ul.current-messages").find('li.active');
  var $selectorLength = $selector.length;
  if($selectorLength) {
    var $id = $($selector).attr('id');
    $id = $id.replace("li_msg_", "");
    if($id) {
      $.ajax({
        type: 'POST', 
        url: '?r=messages/add-archive',
        data: { 
          id: $id
        },
        success: function(response) {
          if(response) {
            Materialize.toast('Archived .', 2000, 'green');
          } else {
            Materialize.toast('Unarchived .', 2000, 'green');
          }
          getRecentMessagsUsers('0', 'no');
        }
      });
    }
  }
}

function manageBlockConverasion(obj) {
 $text = $(obj).text();
 if($text) {
  var $selector = $(".allmsgs-holder").find("ul.current-messages").find('li.active');
                var $selectorLength = $selector.length;
                if($selectorLength) {
                  var $id = $($selector).attr('id');
                  $id = $id.replace("li_msg_", "");
                  if($id) {
                    $.ajax({
                      type: 'POST',
                      url: '?r=messages/block-user',
                      data: {
                        id: $id
                      },
                      success: function(response) {
                        var $type=response.trim();  
                        if($type=="block"){     
                          $(".block-notice").slideDown();
                          var settingObj=$(".block-setting");
                          if(settingObj!=undefined){
                            settingObj.html("Unblock Messages");
                            settingObj.attr("onclick","manageBlockConverasion(this)");
                          }
                        } else {      
                          $(".block-notice").hide();
                          var settingObj=$(".block-setting");
                          if(settingObj!=undefined){
                            settingObj.html("Block Messages");
                            settingObj.attr("onclick","manageBlockConverasion(this)");
                          }
                        }
                      }
                    });
                  }
                }    
  }
}

function deleteselectedmessage() {
  var ids = [];
  $.each($('.mainli.active').find('.selectionchecked'), function() {
     if(this.checked) {
        if($(this).parents('.msg-handle').length) {
          $.each($(this).parents('.msg-handle').find('.message_block'), function() {
              $id = $(this).attr('data-id');
              ids.push($id);
          });
        }
     }
  });
  
  if (typeof ids !== 'undefined' && ids.length > 0) {
    $.ajax({
      url: '?r=messages/delete-selected-message',
      type: 'POST',
      data: {
        id: ids
      },
      success: function(data) {
        if (data) {
          $.each($('.mainli.active').find('.selectionchecked'), function() {
            if(this.checked) {
              if($(this).parents('.msg-handle').length) {
                $(this).parents('.msg-handle').remove();
                $('.selected_msg_number').html('<span>0</span> selected'); 
                Materialize.toast('Deleted.', 2000, 'green');
              }
            }
          });
        }
      }
    });
  } else {
    Materialize.toast('no message box is checked for delete.', 2000, 'red');
  }
}

function genmsgoptionsPhtThd() {
  //showMsgPhotos
  var $selector = $('.current-messages').find('li.active');
  var $length = $selector.length;   
  if($length) {
    var $id = $selector.attr('id');
    if($id) {
      
      $id = $id.replace('li_msg_', '');

      if($id) {
        $.ajax({
                  url: '?r=messages/get-thread',
                  type: 'POST',
          data: {
            id: $id
          },
                  success: function(data) {
                    var data = JSON.parse(data);
                    if (typeof data !== 'undefined' && data.length > 0) {
              $selector = $('.main-msgwindow').find('.albums-grid').find('.row');
              if($selector) {
                $selector.html('');
                $.each(data, function(i, v) {
                  var $image = '<div class="grid-box"> <div class="photo-box"> <div class="imgholder"><figure data-pswp-uid="'+i+'"><a href="'+v+'" data-size="1600x1600" data-med="'+v+'" data-med-size="1024x1024" data-author="Folkert Gorter" class="himg-box"> <img class="himg" src="'+v+'"></a></figure></div></div></div>';
                  $selector.append($image);
                });
                showMsgPhotos();
                initGalleryImageSlider(); 
                fixImageUI('main-msgwindow');
                fixMessageImagesPopup();
              } 
                    }
                  }
              });
      }

      /*var $selector = $('.tab-content').find('#chat-connections').find('ul.chatuserid_'+$id);
      var $length = $selector.length;
      if($length) {
        var $a = $selector.find('a#'+$id);
        if($a) {
          $($a).trigger( "click" );
        }
      }*/
    };
  }
}

function genmsgoptionsOpnCht() {
  var $selector = $('.current-messages').find('li.active');
  var $length = $selector.length;   
  if($length) {
    var $id = $selector.attr('id');
    if($id) {
      $id = $id.replace('li_msg_', '');
      var $selector = $('.tab-content').find('#chat-connections').find('ul.chatuserid_'+$id);
      var $length = $selector.length;
      if($length) {
        var $a = $selector.find('a#'+$id);
        if($a) {
          $($a).trigger( "click" );
        }
      }
    };
  }
} 

function firstDefaultLiClicked() {
  setTimeout(function() {
    var $selector = $('.left-section').find('.tab-content').find('ul.users-display').find('li:first');
    if($selector.length) {
      $selector.find('a.active').trigger('click');
    }
  }, 1000);
}

function messagereporttoabuse($id) {
      if($id != undefined && $id != null && $id != '') {

              bootbox.prompt({
                  title: "Report This Offer!",
                  inputType: 'checkbox',
                  inputOptions: [
                      {
                          text: "It's annoying or not interesting",
                          value: '1',
                      },
                      {
                          text: "I'm in this photo and I don't like it",
                          value: '2',
                      },
                      {
                          text: "I think it shouldn't be on Facebook",
                          value: '3',
                      },
                      {
                          text: "It's spam",
                          value: '4',
                      }
                  ],
                  callback: function (result) {
                      if(result == null) {
                        bootbox.hideAll();
                      } 
                      
                      if(result.length) {
                          $.ajax({
                              url: '?r=messages/report-offer',
                             type: 'POST',
                              data : {
                                  $id : $id,
                                  $type : 'message',
                                  $reason : result 
                              },
                              success: function(data) {
                                  if(data == true) {
                                      //  
                                  }
                              }, complete: function(data) {
                              }
                          });
                      }
                  }
              });
      }
}

      
function openGiftTabs(){
  if($(".giftbox").css("display")=="none"){
    $(".giftbox").slideDown(200);     
  }else{
    $(".giftbox").slideUp(200);
  }
}

function resetAllMessageLink(){
  $(".message-userlist ul > li").each(function(){
    var atag=$(this).find("a");
    if(atag.hasClass("active")) atag.removeClass("active");
  });
}

function openThread(obj){
  var userid=$(obj).attr("data-id");
  PersonBoxClose();
  openMessage("#"+userid);
}

function resetMessageHolder(){
  $(".current-messages li").removeClass("active");
}

function openArchivedMessage(obj){
  $id = $(obj).attr('data-id');
    if($id) {
      var exists=false;
      $(".current-messages li.mainli").each(function(){
          var li_id=$(this).attr("id");
          li_id=li_id.replace("li_","");  
          if(li_id==$id){
              exists=true;        
          }
      });

      if(exists) {
          resetMessageHolder();
          $(".current-messages li#li_"+$id).addClass("active");
      } else {
          resetMessageHolder();
          $(".current-messages").append("<li class='mainli active' id='li_msg_"+$id+"'> <div class='msgdetail-list nice-scroll'><div class='msglist-holder images-container'><ul class='outer'></li></ul></div></div></li>");
      }

      $thumb = $(obj).find('.participants_profile').find('img').attr('src');
      
      isReadMessage($id);
      category = 'inbox';

      $('textarea#inputMessageWall').attr('data-id', $id);

      if($id != undefined) {
          var firstLi = $('.right-section ul.current-messages').find('li#li_msg_'+$id);
          if(!firstLi.length) {
              $tempSel = $('.right-section').find('ul.current-messages');
              $li = '<li class="mainli" id="li_msg_'+$id+'"> <div class="msgdetail-list nice-scroll" tabindex="18" style="opacity: 1; outline: none;"><div class="msglist-holder images-container"><ul class="outer"></ul></div></div></li>';
              $($tempSel).append($li)
          }

          var requstdata = {
              from_id: $id,
              to_id: data2.id,
              category: category,
              start: 0,
              limit: 20
          }
          callHistoryForMessageWall(requstdata);
      }

      socket.emit('getBasicInfoOfUser', $id, data2.id);

      $('.left_side_modal').removeClass('hidesidemodal');
      $('.left_side_modal').addClass('showsidemodal');

      scrollMessageBottom();
      initDropdown();
  }
}

function openMessage(obj) {
  $w = $(window).width(); 
  if ($w <= 799) {
    $('.search-xs-btn').hide();
    $('.globel_setting').hide();
    $('.messagesearch-xs-btn').show();
    $('.person_dropdown').show();
  }

  if($('.person_search_slide_xs').length) {
    if($('.person_search_slide_xs').hasClass('hidesidemodal')) {
      $('.person_search_slide_xs').removeClass('hidesidemodal').addClass('showsidemodal');
    } 
  }

  resetAllMessageLink(); 
  $(obj).addClass("active");
  var userid=$(obj).attr("id");
  var useridFilter = userid.replace("msg_","");

  /* AJAX call to replace messages part */
  var exists=false;
  $(".current-messages li.mainli").each(function(){
    var li_id=$(this).attr("id");
    li_id=li_id.replace("li_","");
    
    if(li_id==userid){
      exists=true;        
    }
  });

  if(exists) {
    resetMessageHolder();
    $(".current-messages li#li_"+userid).addClass("active");
  } else {
    resetMessageHolder();

    if(userid=="ad") {
      $(".right-section .topstuff .msgwindow-name .desc-holder").html("Puma Shoes <span>Sponsored Ad</span>");
      $(".allmsgs-holder").parents(".right-section").addClass("inbox-advert");      
    } else {
      $(".right-section .topstuff .msgwindow-name .desc-holder").html($(obj).find("h6").html());
      var chatimg = $(obj).find(".imgholder").html();
      $(".right-section .topstuff .msgwindow-name .imgholder").html(chatimg);
      
      if($(".allmsgs-holder").parents(".right-section").hasClass("inbox-advert"))
        $(".allmsgs-holder").parents(".right-section").removeClass("inbox-advert");
    }

    $(".current-messages").append("<li class='mainli active' id='li_"+userid+"'> <div class='msgdetail-list nice-scroll'><div class='msglist-holder images-container'><ul class='outer'></li></ul></div></div></li>");
  } 

  // call load history function...
  $('#inputMessageWall').attr('data-id', useridFilter);
  messageLiClicked(obj);

  $thumb = $(obj).find('.imgholder').find('img').attr('src');
  socket.emit('getBasicInfoOfUser', useridFilter, data2.id);
  
  var isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
  if(isMblMessagePage){
    $(".messages-list").find(".left-section").addClass("hidden");
    if($(".messages-list").find(".right-section").hasClass("hidden")){
      $(".messages-list").find(".right-section").removeClass("hidden");
      $(".messages-list").find(".right-section").addClass("shown");
      
      var getusername = $('.msgwindow-name').find('.desc-holder').html();
              
      $userimg = $('.msgwindow-name').find('img').attr('src');
      $username = $('.msgwindow-name').find('.desc-holder').html();
      $person_status = $('.msgwindow-name').find('.person_status').html();
      $usercountry = $('.msgwindow-name').find('.usercountry').html();
      $usertime = $('.msgwindow-name').find('.usertime').html();

      $(".header-themebar .mbl-innerhead").show();
      $htmlBlock = '<span class="top_img"><a href="index.php?r=userwall%2Findex&id='+useridFilter+'"><img src="'+$userimg+'"></a></span> <a href="index.php?r=userwall%2Findex&id='+useridFilter+'" class="mbl-logo page-name">'+$username+'</a> <div class="top_message_status"> <span class="">'+$person_status+'</span><span class="">'+$usercountry+'&nbsp;</span> <span class="">&nbsp;'+$usertime+'&nbsp;</span></div>';
      $('.mbl-innerhead').find('.logo-holder').html($htmlBlock);
      $('.mobile-footer').hide();
      $(".mblMessagePage").addClass("innerpage");

      generalCaseStudy();
    }
  }
  
  
  var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
  if(isBusinessPage){
    var isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
    if(isMblMessageTab){
      $(".messages-list").find(".left-section").addClass("hidden");
      if($(".messages-list").find(".right-section").hasClass("hidden")){
        $(".messages-list").find(".right-section").removeClass("hidden");
        $(".messages-list").find(".right-section").addClass("shown");
        var getusername = $('.msgwindow-name').find('.desc-holder').html();
        
        $(".header-themebar .mbl-innerhead").show();
        $(".header-themebar .mbl-innerhead").find('.page-name').html(getusername);
        $('.mobile-footer').hide();
        $(".mblMessagePage").addClass("innerpage");       
      }
    }
  }
  
  scrollMessageBottom();
  initDropdown();
  setMsgTextArea();

  STARTgetuserlabel();
}

function openSupportTeam(obj) {
  $w = $(window).width(); 
  if ($w <= 799) {
    $('.search-xs-btn').hide();
    $('.globel_setting').hide();
    $('.messagesearch-xs-btn').show();
    $('.person_dropdown').show();
  }

  if($('.person_search_slide_xs').length) {
    if($('.person_search_slide_xs').hasClass('hidesidemodal')) {
      $('.person_search_slide_xs').removeClass('hidesidemodal').addClass('showsidemodal');
    } 
  }

  resetAllMessageLink(); 
  $(obj).addClass("active");
  var userid=$(obj).attr("id");
  var useridFilter = userid.replace("msg_","");

  /* AJAX call to replace messages part */
  var exists=false;
  $(".current-messages li.mainli").each(function(){
    var li_id=$(this).attr("id");
    li_id=li_id.replace("li_","");
    
    if(li_id==userid){
      exists=true;        
    }
  });

  if(exists) {
    resetMessageHolder();
    $(".current-messages li#li_"+userid).addClass("active");
  } else {
    resetMessageHolder();

    if(userid=="ad") {
      $(".right-section .topstuff .msgwindow-name .desc-holder").html("Puma Shoes <span>Sponsored Ad</span>");
      $(".allmsgs-holder").parents(".right-section").addClass("inbox-advert");      
    } else {
      $(".right-section .topstuff .msgwindow-name .desc-holder").html($(obj).find("h6").html());
      var chatimg = $(obj).find(".imgholder").html();
      $(".right-section .topstuff .msgwindow-name .imgholder").html(chatimg);
      
      if($(".allmsgs-holder").parents(".right-section").hasClass("inbox-advert"))
        $(".allmsgs-holder").parents(".right-section").removeClass("inbox-advert");
    }

    $(".current-messages").append("<li class='mainli active' id='li_"+userid+"'> <div class='msgdetail-list nice-scroll'><div class='msglist-holder images-container'><ul class='outer'></li></ul></div></div></li>");
  } 

  // call load history function...
  $('#inputMessageWall').attr('data-id', useridFilter);
  messageLiClicked(obj);

  $thumb = $(obj).find('.imgholder').find('img').attr('src');

  $SPImage = $('#supportteamLI').find('.imgholder').find('img').attr('src');
  $SPName = $('#supportteamLI').find('.descholder').find('h5').html();

  $MSS = '<div class="imgholder dot_offline"> <img src="'+$SPImage+'"> </div> <span class="desc-holder">'+$SPName+'</span> <span class="person_status">&nbsp;</span> <span class="usercountry">&nbsp;</span> <span class="usertime"></span>';
  $('.topstuff .msgwindow-name').html($MSS);

  var isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
  if(isMblMessagePage){
    $(".messages-list").find(".left-section").addClass("hidden");
    if($(".messages-list").find(".right-section").hasClass("hidden")){
      $(".messages-list").find(".right-section").removeClass("hidden");
      $(".messages-list").find(".right-section").addClass("shown");
      
      var getusername = $('.msgwindow-name').find('.desc-holder').html();
              
      $userimg = $('.msgwindow-name').find('img').attr('src');
      $username = $('.msgwindow-name').find('.desc-holder').html();
      $person_status = $('.msgwindow-name').find('.person_status').html();
      $usercountry = $('.msgwindow-name').find('.usercountry').html();
      $usertime = $('.msgwindow-name').find('.usertime').html();

      $(".header-themebar .mbl-innerhead").show();
      $htmlBlock = '<span class="top_img"><a href="index.php?r=userwall%2Findex&id='+useridFilter+'"><img src="'+$userimg+'"></a></span> <a href="index.php?r=userwall%2Findex&id='+useridFilter+'" class="mbl-logo page-name">'+$username+'</a> <div class="top_message_status"> <span class="">'+$person_status+'</span><span class="">'+$usercountry+'&nbsp;</span> <span class="">&nbsp;'+$usertime+'&nbsp;</span></div>';
      $('.mbl-innerhead').find('.logo-holder').html($htmlBlock);
      $('.mobile-footer').hide();
      $(".mblMessagePage").addClass("innerpage");

      generalCaseStudy();
    }
  }
  
  
  var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
  if(isBusinessPage){
    var isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
    if(isMblMessageTab){
      $(".messages-list").find(".left-section").addClass("hidden");
      if($(".messages-list").find(".right-section").hasClass("hidden")){
        $(".messages-list").find(".right-section").removeClass("hidden");
        $(".messages-list").find(".right-section").addClass("shown");
        var getusername = $('.msgwindow-name').find('.desc-holder').html();
        
        $(".header-themebar .mbl-innerhead").show();
        $(".header-themebar .mbl-innerhead").find('.page-name').html(getusername);
        $('.mobile-footer').hide();
        $(".mblMessagePage").addClass("innerpage");       
      }
    }
  }
  
  scrollMessageBottom();
  initDropdown();
  setMsgTextArea();

  //STARTgetuserlabel();
}

function openNewMessagemobile(obj){   

  resetAllMessageLink();   
  resetMessageHolder();
  
  $(".allmsgs-holder .newmessage").show();
  
  var isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
  if(isMblMessagePage){
    $(".messages-list").find(".left-section").addClass("hidden");
    if($(".messages-list").find(".right-section").hasClass("hidden")){
      $(".messages-list").find(".right-section").removeClass("hidden");
      $(".messages-list").find(".right-section").addClass("shown");
      
      $(".header-themebar .mbl-innerhead").show();
      $(".header-themebar .mbl-innerhead").find('.page-name').html("New Message");
      
      $('.mobile-footer').hide();
      $(".mblMessagePage").addClass("innerpage");
    }
  }
  $('.messages-page').find('.messages-list').addClass("newmsg-mode");
  
  if(isBusinessPage){
    var isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
    if(isMblMessageTab){
      $(".messages-list").find(".left-section").addClass("hidden");
      if($(".messages-list").find(".right-section").hasClass("hidden")){
        $(".messages-list").find(".right-section").removeClass("hidden");
        $(".messages-list").find(".right-section").addClass("shown");
        //var getusername = $('.msgwindow-name').find('.desc-holder').html();
        
        $(".header-themebar .mbl-innerhead").show();
        $(".header-themebar .mbl-innerhead").find('.page-name').html("New Message");
        
        $('.mobile-footer').hide();
        $(".mblMessagePage").addClass("innerpage");
      }
    }
  }
  
  
}

function closeMessage(obj){
  
  var isMblMessagePage;
  var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
  if(isBusinessPage){
    isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
  }else{
    isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
  }
  
  if(isMblMessagePage){
    $(".messages-list").find(".right-section").addClass("hidden");
    if($(".messages-list").find(".left-section").hasClass("hidden")){
      $(".messages-list").find(".left-section").removeClass("hidden");
      $(".messages-list").find(".left-section").addClass("shown");
      
      $('.page-name').html('Home');
      $('.gotohome').find('a').html('<i class="mdi mdi-menu"></i>');
      $('.gotohome').find('a').removeAttr('onclick');
      $('.gotohome').find('a').attr('href', 'messages.html');
      
      if(!isBusinessPage)
        $('.mobile-footer').show();
    }     
  }   
} 

function markRead(obj){
  var sparent=$(obj).parents("li");
  if(sparent.hasClass("read")){
    sparent.removeClass("read");
    $(obj).attr("title","Mark as read");
  }
  else{
    sparent.addClass("read");
    $(obj).attr("title","Mark as unread");
  }
}

function showAllMsgRead(){
  $(".msg-listing li").each(function(){
    $(this).addClass("read");
    $(this).find("a.readicon").attr("title","Mark as unread");
  });
}

function showMsgCheckbox(){
  $(".main-msgwindow").addClass("have_checkbox");
}

function hideMsgCheckbox(){
  $(".main-msgwindow").removeClass("have_checkbox");
}

function showMsgPhotos(){
  $(".main-msgwindow").addClass("have_photos");
}

function hideMsgPhotos(){
  $(".main-msgwindow").removeClass("have_photos");
}

function changeEmoCategory(obj){
  var esCat=$(obj).val();
  manageEmoStickersBox(esCat);
}

function manageEmoStickersBox(esCat, $parent){
  $(".giftlist-popup").find(".nice-scroll.emostickers").niceScroll({horizrailenabled:false,cursorcolor:"#bbb",cursorwidth:"8px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)"});
  $(".giftlist-popup").find(".nice-scroll.emostickers").getNiceScroll(0).rail.addClass('popupScroller');

  var catname = esCat;
  var sparent=$(".giftlist-popup").find(".emotion-holder");
  initNiceScroll(".nice-scroll");

  //ajax call.... fire and get emotions for particular category..
  var $faces="";
  $faces = ['(wine)','(icecream)','(coffee)','(heart)','(flower)','(cake)','(handshake)','(gift)','(goodmorning)','(goodnight)','(backpack)','(parasailing)','(train)','(flipflop)','(airplane)','(sunbed)','(happyhalloween)','(merrychristmas)','(eidmubarak)','(happyyear)','(happymothers)','(happyfathers)','(happyaniversary)','(happybirthday)'];
  
  $(".emostickers-box ul.emostickers-list").html("");
  if($faces.length!=0){
    
    $.each($faces, function(i, v){
      
      var faces = $.emostickers.replace(v);     
      $(".giftlist-popup").find(".emostickers-box").find("ul.emostickers-list").append('<li><a href="javascript:void(0)" data-class="'+v+'" onclick="useGift(\''+ v + '\',\''+catname+'\',\'editmode\', \''+$parent+'\', this);">'+faces+'</a></li>');
    });
    
  }else{
    getnolistfound('nostikersfound');
  }

  var winw = $(window).width();
  if(winw < 1024){
    removeNiceScroll(".nice-scroll");
  }
  
}

function resetEmoCarousels(catName,emovalue,mode){    
  if(mode=="editmode") {
    if($('.right-section').find('.current-messages').find('.mainli.active').length) {
      $id = $('.right-section').find('.current-messages').find('.mainli.active').attr('id');
      $id = $id.replace("li_msg_","");
      if($id) {
        $("#usegift-popup").html('');
        $.ajax({
          url: '?r=messages/getallgifts',
          type: 'POST', 
          data: {
            catName: catName,
            emovalue: emovalue,
            mode: mode,
            id: $id
          },

          success: function(result) {
            if(result != '') {
              $("#usegift-popup").html(result);
              $('.carousel.carousel-slider').carousel({
                fullWidth: true
              });
            }
          }
        });
      }
    }
  }
  else{   
    var htmltext = $.emostickers.replace(emovalue);
    $("#usegift-popup").find(".emostickers-holder").html(htmltext);
  }     
}

function useGift(emovalue, catName, mode, $parent, obj) {   
  $("#usegift-popup").find(".emostickers-slider").find(".loading-holder").html("");
  $("#usegift-popup").find(".emostickers-holder").find(".loading-holder").html("");
  
  if(mode=="editmode") {
    $("#usegift-popup").find(".preview-emosticker").hide();
    $("#usegift-popup").find(".edit-emosticker").show();  
  } else {
    $("#usegift-popup").find(".edit-emosticker").hide();
    $fullname = $('.right-section').find('.topstuff').find('.desc-holder').html();
    if($(obj).parents('li.msgli').hasClass('msg-outgoing')) {
      $content = '<div class="custom_message_modal_header"><p> You send a gift to <span>'+$fullname+'</span></p><button class="close_modal_icon close_modal"> <i class="mdi mdi-close"></i> </button> </div> <div class="usergift_content"> <div> <div class="popup-content"> <div class="emostickers-holder data-loading"> <div class="loading-holder"> </div> </div> <div class="btn-holder"> </div> </div> </div></div></div>';
    } else {
      $content = '<div class="custom_message_modal_header"><p> You got a gift from <span>'+$fullname+'</span></p><button class="close_modal_icon close_modal"> <i class="mdi mdi-close"></i> </button> </div> <div class="usergift_content"> <div> <div class="popup-content"> <div class="emostickers-holder data-loading"> <div class="loading-holder"> </div> </div> <div class="btn-holder"> </div> </div> </div> </div> <div class="modal-footer"> <div class="gift_send"> <a href="javascript:void(0)" class="left close_modal" onclick="saythankyou()">Say thank you</a> <a href="javascript:void(0)" class="right close_modal" data-dismiss="modal"  onclick="giftModalAction()">Reply with a gift</a> </div> </div>';;
    }

    $("#usegift-popup").find(".preview-emosticker").html($content);
    $("#usegift-popup").find(".preview-emosticker").show();
  }

  $('.usegift_modal').modal('open');
  
  if($parent == 'chat') {
    var selector = $('.chat-window').find('ul.mainul').find('li.active');
    if(selector.length) {
      var id  = $(selector).attr('id');
      id = id.replace('li-', '');
      //$("#sendgiftusers").select2().val(id).trigger("change");

      var name = $(selector).find('.userinfo').find('h6').html();
      name = $.trim(name);  

      $('#usegift-popup').find('.edit-emosticker').find('.to_gift').html(name);
    }
  } else if($parent == 'message') {
    var selector = $('#messages-inbox').find('ul.users-display').find('li').find('a.active');
    if(selector.length) {
      var id  = $(selector).attr('id');
      id = id.replace('msg_', '');
      //$("#sendgiftusers").select2().val(id).trigger("change");

      var name = $(selector).find('.descholder').find('h6').html();
      name = $.trim(name);  

      $('#usegift-popup').find('.edit-emosticker').find('.to_gift').html(name);
    }
  }

  setTimeout(function() {
    resetEmoCarousels(catName,emovalue,mode);
    if(mode=="editmode"){         
      $("#usegift-popup").find(".emostickers-slider").find(".loading-holder").find('.carousel#emoCarousel').carousel({ interval: false });
      $("#usegift-popup").find(".emostickers-slider.data-loading").addClass("data-loaded");
    } else {
      $("#usegift-popup").find(".emostickers-holder.data-loading").addClass("data-loaded");
    }       
  },400);

  $('.carousel.carousel-slider').carousel({
    fullWidth: true
  });
}

function usegift_modal() {
  $('.usegift_modal').modal('close');
}

function scrolltobottomMSG() { // scroll to bottom in message page message list box.
  $scrollSelectore = $('.mainli.active').find('.msgdetail-list');
  if($scrollSelectore.length) {
      $divHeight = $scrollSelectore.find('ul.outer').height();
      $scrollSelectore.scrollTop(898);
  }
}

function selectMessageBox() {
    $('.selected_messages_box').show();
    $('.select_msg_checkbox').show();
    $('.addnew-msg').hide();
    $('.attachment_add_icon').hide();
    $('.msgdetail-box .imgholder img').hide();
}

function closeSelectedMessage() {
  $('.selected_messages_box').hide();
  $('.select_msg_checkbox').hide();
  $('.addnew-msg').show();
  $('.attachment_add_icon').show();
  $('.msgdetail-box .imgholder img').show();
  $('.msgdetail-box .descholder p').css('background-color', '#f3f3f3');
}

function contactInfo() {
  $('.contact_info_modal').addClass('showsidemodal_right');
  $('.contact_info_modal').removeClass('hidesidemodal_right');
}

function closeMessageSearchSlide() {
  $('.search_messages_modal').removeClass('hidesidemodal_right');
  $('.search_messages_modal').addClass('showsidemodal_right');
}

function sepmsgopt(obj) {
  var $ids = [];
  $this = $(obj);
  if($this.parents('.msg-handle').length) {
    $.each($this.parents('.msg-handle').find('.message_block'), function(i, v) {
        $id = $(v).attr('data-id');
        $ids.push($id);
    });
  }
  
  $.ajax({
    type: 'POST',
    url: '?r=messages/sepmsgopt', 
    data: { id: $ids},
    success: function(result) {
      $this.parents('.settings-icon').find('ul').html(result);
    }
  });
}

function selectedmessagesaved() {
  var ids = [];
  $.each($('.mainli.active').find('.selectionchecked'), function() {
     if(this.checked) {
        if($(this).parents('.msg-handle').length) {
          $.each($(this).parents('.msg-handle').find('.message_block'), function() {
              $id = $(this).attr('data-id');
              ids.push($id);
          });
        }
     }
  });

  if (typeof ids !== 'undefined' && ids.length > 0) {
    $.ajax({
      url: '?r=messages/addstarmessagebulk',
      type: 'POST',
      data: {
        id: ids
      },
      success: function(response) {
        var $result = JSON.parse(response);
        if($result.status != undefined && $result.status == true) {
            $label = $result.label;
            Materialize.toast($label, 2000, 'green');
            $('.selectionchecked').removeAttr('checked');
            $('.selected_msg_number').html('<span>0</span> selected'); 
        }
      }
    });
  } else {
    Materialize.toast('no message box is checked for delete.', 2000, 'red');
  }
}

function getallsavedmsg() {
  $('.message_save_modal').removeClass('showsidemodal');
  $('.message_save_modal').addClass('hidesidemodal');

  $.ajax({ 
    url: '?r=messages/getallsavedmsg',
    success: function(data) {
      $('.message_save_modal').find('.saved_message_container').html(data);

    }, 
    complete: function() {
      initDropdown();
    }
  });
}

function ProfileNameChange() {
  $('.setting_profile_name').slideDown();
  $('.setting_profile_name input').focus();
}

function getparticularusersavedmsg() {
  if($('.right-section').find('.current-messages').find('.mainli.active').length) {
    $id = $('.right-section').find('.current-messages').find('.mainli.active').attr('id');
    $id = $id.replace("li_msg_","");
    if($id) {
      $('.person_message_save_modal').removeClass('showsidemodal_right');
      $('.person_message_save_modal').addClass('hidesidemodal_right');

      $.ajax({ 
        url: '?r=messages/getparticularusersavedmsg',
        type: 'POST',
        data: {id: $id},
        success: function(data) {
          $('.person_message_save_modal').find('.saved_message_container').html(data);
        }, 
        complete: function() {
          initDropdown();
        }
      });
    }
  }
}

function closeInnerRightSlide() {
  $('.person_message_save_modal').removeClass('hidesidemodal_right');
  $('.person_message_save_modal').addClass('showsidemodal_right');
}

function getallarchivedusers() {
  $('.archieved_chat_modal').removeClass('showsidemodal');
  $('.archieved_chat_modal').addClass('hidesidemodal');

  $.ajax({ 
    url: '?r=messages/getallarchivedusers',
    success: function(data) {
      $('.archieved_chat_modal').find('.archeive_container').html(data);
    }, 
    complete: function() {
      initDropdown();
    }
  });
}

function getallblockusers() {
  $('.blocked_contact_modal').removeClass('showsidemodal');
  $('.blocked_contact_modal').addClass('hidesidemodal');

  $.ajax({ 
    url: '?r=messages/getallblockusers',
    success: function(data) {
      $('.blocked_contact_modal').find('.block_contact_container').html(data);
    }, 
    complete: function() {
      initDropdown();
    }
  });
}

function unsavedmessage(obj) {
  $this = $(obj);
  $id = $this.attr('data-id');
  if($id) {
    $.ajax({
      url: '?r=messages/unsavedmessage',
      type: 'POST',
      data: {
        id: $id
      },
      success: function(response) {
        var $result = JSON.parse(response);
        if($result.status != undefined && $result.status == true) {
            Materialize.toast('Unsaved', 2000, 'green');
            if($this.parents('.msg_division').length) {
              $this.parents('.msg_division').remove();
            }
        }
      }
    });
  }
}

function unblockuser(obj) {
  $this = $(obj);
  $id = $this.attr('data-id');
  if($id) {
    $.ajax({
      url: '?r=messages/unblockuser',
      type: 'POST',
      data: {
        id: $id
      },
      success: function(response) {
        var $result = JSON.parse(response);
        if($result.status != undefined && $result.status == true) {
            Materialize.toast('Unblock', 2000, 'green');
            if($this.parents('.blocked_person_box').length) {
              $this.parents('.blocked_person_box').remove();
            }
        }
      }
    });
  }
}

function unarchiveuser(obj) {
  $this = $(obj);
  $id = $this.attr('data-id');
  if($id) {
    $.ajax({
      url: '?r=messages/unarchiveuser',
      type: 'POST',
      data: {
        id: $id
      },
      success: function(response) {
        var $result = JSON.parse(response);
        if($result.status != undefined && $result.status == true) {
          Materialize.toast('Unarchive', 2000, 'green');
          if($this.parents('.archeive_info').length) {
            $this.parents('.archeive_info').remove();
          }

          if(!$('.archieved_chat_modal').find('.archeive_container').find('.archeive_info').length) {
            $('.archieved_chat_modal').find('.archeive_container').html('<span class="no_archeive_chat_icon"> <i class="zmdi zmdi-archive"></i> </span> <p class="archeived_msg">No archeived chat</p>');
          }

          getRecentMessagsUsers('0', 'no');
        }
      }
    });
  }
}

function deletemessage_sm(obj) {
  $this = $(obj);
  $id = $this.attr('data-id');
  if($id) {
    $.ajax({
      url: '?r=messages/deletemessage_sm',
      type: 'POST',
      data: {
        id: $id
      },
      success: function(response) {
        var $result = JSON.parse(response);
        if($result.status != undefined && $result.status == true) {
            Materialize.toast('Deleted', 2000, 'green');
            if($this.parents('.msg_division').length) {
              $this.parents('.msg_division').remove();
            }
        }
      }
    });
  }
}

function saythankyou() {
  if($('.right-section').find('.current-messages').find('.mainli.active').length) {
    $id = $('.right-section').find('.current-messages').find('.mainli.active').attr('id');
    $id = $id.replace("li_msg_","");

    if($id) {
        $.ajax({ 
            url: '?r=messages/get-category',
            type: 'POST',
            data: {id: $id},
            success: function(result) {
                var result = JSON.parse(result);
                if(result.status) {
                    var category = result.category;
                    var SendMsgArray= {
                        'from_id': data2.id,
                        'to_id': $id,
                        'reply': 'Thank you for your really nice gift.',
                        'type': 'text',
                        'category': category
                    };

                    $('textarea#inputMessageWall').val('');
                    socket.emit('sendMessage', SendMsgArray);
                } else if(result.status == false && result.reason != undefined) {
                    if(result.reason == 'block') {
                        var by = result.by;
                        if(by == 'self') {
                          $label = result.label;
                          var disText = $(".discard_md_modal .discard_modal_msg");
                          var btnKeep = $(".discard_md_modal .modal_keep");
                          var btnDiscard = $(".discard_md_modal .modal_discard");
                          disText.html($label);
                          btnKeep.html("Unblock Messages");
                          btnDiscard.html("Ok");
                          btnKeep.attr('onclick', 'manageBlockConverasion("<span>Unblock Messages</span>")');
                          $(".discard_md_modal").modal("open");
                        } else {
                          var disText = $(".discard_md_modal .discard_modal_msg");
                          var btnDiscard = $(".discard_md_modal .modal_discard");
                          disText.html("This person isn\'t receiving messages from you at the moment.");
                          btnDiscard.html("Ok");
                          $(".discard_md_modal").modal("open");
                        }
                    }
                }             
            }
        });
    }
  }
}

function clearmessages() {
  // cleared message history...
  if($('.right-section').find('.current-messages').find('.mainli.active').length) {
    $id = $('.right-section').find('.current-messages').find('.mainli.active').attr('id');
    $id = $id.replace("li_msg_","");
    if($id) {
      $.ajax({ 
        url: '?r=messages/clearmessages',
        type: 'POST',
        data: {id: $id},
        success: function(data) {
          if(data) {
            if($('.right-section').find('.current-messages').find('.mainli.active').length) {
               $('.right-section').find('.current-messages').find('.mainli.active').find('.outer').html('');
            }

            if($('.left-section').find('.users-display').find('li').find('a.active').length) {
              $('.left-section').find('.users-display').find('li').find('a.active').find('.descholder').find('p').remove();
            }
            
            Materialize.toast('Deleted messages', 2000, 'green');
          }
        }, 
        complete: function() {
          initDropdown();
        }
      });
    }
  }
    
}

function blocked_contact_action() {
  $('.blocked_contact_modal').removeClass('showsidemodal');
  $('.blocked_contact_modal').addClass('hidesidemodal');
}

function new_group_action() {
    $('.new_group_modal').removeClass('showsidemodal');
    $('.new_group_modal').addClass('hidesidemodal');
}

function MessageRequest() {
    $('.message_request_modal').removeClass('showsidemodal');
    $('.message_request_modal').addClass('hidesidemodal');
}

function getmessageleftsetting() {
  $('.msg_setting_modal').removeClass('showsidemodal');
  $('.msg_setting_modal').addClass('hidesidemodal');

  $.ajax({  
    url: '?r=messages/getmessageleftsetting',
    success: function(data) {
      $('.msg_setting_modal').find('.side_modal_container').html(data);
    }, 
    complete: function() {
      initDropdown();

      if($('.contact_info_switch_sound').length) {
        $isSound = $('.contact_info_switch_sound').attr('data-issound');
        if($isSound == 'on') {
          $('.contact_info_switch_sound').find('input').attr('checked', 'checked');
        } else {
          $('.contact_info_switch_sound').find('input').attr('checked', false);
        }
      }

      if($('.contact_info_switch_displaypreview').length) {
        $isDisplayPreview = $('.contact_info_switch_displaypreview').attr('data-isdisplaypreview');
        if($isDisplayPreview == 'on') {
          $('.contact_info_switch_displaypreview').find('input').attr('checked', 'checked');
        } else {
          $('.contact_info_switch_displaypreview').find('input').attr('checked', false);
        }
      }

    }
  });
}
    
function communicationMessage()
{
  var is_received_message_tone_on = 'off';
  var is_new_message_display_preview_on = 'off';
  //var communication_label = '';
  var show_away = 'off';
  var is_send_message_on_enter = 'off';

  if($("#is_received_message_tone_on").prop('checked') == true) {
    is_received_message_tone_on = 'on';
  }
  
  if($("#is_new_message_display_preview_on").prop('checked') == true) {
    is_new_message_display_preview_on = 'on';
  }

  //communication_label = $('#communication_label').val();

  if($("#show_away").prop('checked') == true) {
    show_away = 'on';
  }
  
  if($("#is_send_message_on_enter").prop('checked') == true) {
    is_send_message_on_enter = 'on';
  }

  $.ajax({  
    type: 'POST',
    url: '?r=messages/communication-settings-message',
    data: {
      'is_received_message_tone_on' : is_received_message_tone_on,
      'is_new_message_display_preview_on' : is_new_message_display_preview_on,
      'show_away' : show_away,
      'is_send_message_on_enter' : is_send_message_on_enter
    },
    success: function(data){
      if(data == '1'){
        Materialize.toast('Saved', 2000, 'green');
      }
    }
  });
}

function domutemessage()
{
  var do_mute_message = 'off';
  
  if($("#do_mute_message").prop('checked') == true) {
    do_mute_message = 'on';
  }
  
  if($('.right-section').find('.current-messages').find('.mainli.active').length) {
    $id = $('.right-section').find('.current-messages').find('.mainli.active').attr('id');
    $id = $id.replace("li_msg_","");
    if($id) {
      $.ajax({ 
        type: 'POST', 
        url: '?r=messages/domutemessage',
        data: {
          'do_mute_message' : do_mute_message,
          'id' : $id
        },
        success: function(data){
          var result = JSON.parse(data);
          if(result.status == true) {
            $label = result.label;
            $isdone = result.isdone;
            Materialize.toast($label, 2000, 'green');
          }
        },
        complete: function() {
          SINGLEFIREgetuserlabel();
        }
      });
    }
  }     
}

function doblockmessage()
{
  var do_block_message = 'off';

  if($("#do_block_message").prop('checked') == true) {
    do_block_message = 'on';
  }

  if($('.right-section').find('.current-messages').find('.mainli.active').length) {
    $id = $('.right-section').find('.current-messages').find('.mainli.active').attr('id');
    $id = $id.replace("li_msg_","");
    if($id) {
      $.ajax({ 
        type: 'POST', 
        url: '?r=messages/doblockmessage',
        data: {
          'do_block_message' : do_block_message,
          'id' : $id
        },
        success: function(data){
          var result = JSON.parse(data);
          if(result.status == true) {
            $label = result.label;
            $isdone = result.isdone;
            Materialize.toast($label, 2000, 'green');
          }
        },
        complete: function() {
          SINGLEFIREgetuserlabel();
        }
      });
    }
  }     
}

function giftModalAction() {
  $('.gift_modal').modal('open');
  closeattachmentbox();    
}

function giftModalActionFromCtIfo() {
  $('.contact_info_modal').addClass('showsidemodal_right');
  $('.contact_info_modal').removeClass('hidesidemodal_right');
  $('.gift_modal').modal('open');
}

function attach_file_add() {
  $('#attach_file_add').trigger('click');
  closeattachmentbox();    
}

function gift_modal() {
  $('.gift_modal').modal('close');
}

function closeattachmentbox() {
  $custW = $(window).width();
  if($custW <=799) {
          $('.hidden-attachment-box').removeClass('shown').addClass('hidden');
      } else {
        $('.hidden-attachment-box').hide();
      }
  $('.fixed-action-btn.docustomize').removeClass('active'); 
}

function setMsgTextArea() {
  $w = $(window).width();
  $left = $('.left-section');
  $right = $('.right-section');
  $textarea = $('.inputMessageWall');
  $textareaBlock = $('.addnew-msg');
  $messagespagefixed = $('.messages-page-fixed').width();
  
  if($w < 800) {
    $SPACE = 118;
    $SDJSDI = $w;
  } else {
    $SPACE = 140;
    $SDJSDI = $right.width();
  }

  $textareaBlock.css('width', $SDJSDI+'px');
  $textareaSpace = $textareaBlock.width() - $SPACE;
  $textarea.css('width', $textareaSpace+'px');
}

function generalCaseStudy() {
  $w = $(window).width();

  if(window.location.href.indexOf("messages") > -1) {
    if($w > 799) {
      $('.mbl-innerhead').hide();
      $('.hidemenu-wrapper').find('.header-section').find('.header-themebar').find('.mobile-menu').css('display', 'block');
      $('.hidemenu-wrapper').find('.header-section').find('.header-themebar').find('.mobile-menu.topicon').css('display', 'none');
    } else {
        $('.hidemenu-wrapper').find('.header-section').find('.header-themebar').find('.mobile-menu.topicon').css('display', 'block');
    }
    
    if($w < 800) {
      $('.page-name.mainpage-name').show();
    } else {  
      $('.page-name.mainpage-name').hide();
    }
  }

  if($('.mblMessagePage').length) {
    if($('.mblMessagePage').hasClass('innerpage')) {
      if ($w <= 799) {
        $('.messages-wrapper').find('.header-section').find('.header-themebar').find('.mbl-innerhead').find('.logo-holder').css('margin-top', '-38px');
        $('.hidemenu-wrapper').find('.header-section').find('.header-themebar').find('.mobile-menu').css('display', 'none');
        $('.messagesearch-xs-btn').show();
      } else {
        $('.messagesearch-xs-btn').hide();
      }
    } else {
      $('.messagesearch-xs-btn').hide();
    } 
  }
}

function personSearchSlide($searchkey='') {
  $('.person_search_slide_xs').removeClass('showsidemodal');
  $('.person_search_slide_xs').addClass('hidesidemodal');

  $('.person_search_slide_xs').find('.suggested_person_addto_group').html('');
  
  $.ajax({ 
    url: '?r=messages/getfrdandfrdoffrdsearch',
    type: 'POST',
    async: false,
    cache: false, 
    data: {
      searchkey: $searchkey
    },
    beforeSend: function() { 
      $loaderblock = '<div class="loaderblock" style="clear: both;"> <center><div class="preloader-wrapper active"> <div class="spinner-layer"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div></center></div>';
      $('.person_search_slide_xs').find('.suggested_person_addto_group').html($loaderblock);
    },
    success: function(result) {
      if($('.person_search_slide_xs').length) {
        if($('.person_search_slide_xs').hasClass('hidesidemodal')) {
          $('.person_search_slide_xs').find('.loaderblock').remove();
          $sk = $('#new_chat_search').val().trim();
          $('.person_search_slide_xs').find('.suggested_person_addto_group').html(result);
        }
      }
    }
  });  
}

function openpersonSlide() {
  $('.person_search_slide_xs').removeClass('showsidemodal');
  $('.person_search_slide_xs').addClass('hidesidemodal');

  if($('.person_search_slide_xs ').find('#new_chat_search').length) {
    $('.person_search_slide_xs ').find('#new_chat_search').val('');
  }
  personSlide();
}

function personSlide(itretion=0) {
  $.ajax({ 
    url: '?r=messages/getfrdandfrdoffrd',
    type: 'POST',
    //async: false,
    //cache: false, 
    data: {
      itretion: itretion
    },
    beforeSend: function() {
      $loaderblock = '<div class="loaderblock" style="clear: both;"> <center><div class="preloader-wrapper active"> <div class="spinner-layer"> <div class="circle-clipper left"> <div class="circle"></div> </div><div class="gap-patch"> <div class="circle"></div> </div><div class="circle-clipper right"> <div class="circle"></div> </div> </div> </div></center></div>';
      $('.person_search_slide_xs').find('.suggested_person_addto_group').append($loaderblock);
    },
    success: function(result) {
      if(result != 'FINISH') {
        $sk = $('#new_chat_search').val().trim();
        if($sk == '') {
          if(itretion === 0) {
            $('.person_search_slide_xs').find('.suggested_person_addto_group').html('');
          }
       
          if($('.person_search_slide_xs').length) {
            if($('.person_search_slide_xs').hasClass('hidesidemodal')) {
              $('.person_search_slide_xs').find('.loaderblock').remove();
              $('.person_search_slide_xs').find('.suggested_person_addto_group').append(result);
              
              $itretion = itretion + 1;
              personSlide($itretion);
            }
          }
        }
      } else {
        $('.person_search_slide_xs').find('.loaderblock').remove();
      }
    }
  });  
}

function messageSearchSlide() {
  $('.msgsearch_messages_modal').removeClass('showsidemodal');
  $('.msgsearch_messages_modal').addClass('hidesidemodal');
}

function openCustomSearchMain() {
  if($('.main-msgwindow').length) {
    if($('.main-msgwindow').hasClass('customsearchmainactive')) {
      $('.main-msgwindow').removeClass('customsearchmainactive');
    } else {
      $('.main-msgwindow').addClass('customsearchmainactive');
    }
  }
}

function getSTli(){  
  $.ajax({
    url: '?r=messages/supportteam',
    success: function (data) {
      $('.left-section').find('.users-display').remove('#supportteamLI');
      $('.left-section').find('.users-display').append(data);
    }
  });
}