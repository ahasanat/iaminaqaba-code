/* page load event */
$(document).ready(function() { 
  var city = getQueryVariable('city');
  if(city != undefined && city != null && city != '') {
    var name = getQueryVariable('name');
    if(name != undefined && name != null && name != '') {
      /* listing all hotels list with particular location*/
      displayhotelswithparticular(city, name);
    } else {
      window.location.href='';
    }
  } else {
      /* listing hotels */
      displayhotels();
      //displayhotelsUniq();
  }

	  $.fn.is_on_screen = function(){
		var win = $(window);
		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();
		 
		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();
		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
	};
	if( $('.lazyloadscrolltour').length > 0 ) { // if target element exists in DOM
		if( $('.lazyloadscrolltour').is_on_screen() ) { // if target element is visible on screen after DOM loaded
			displayhotels(postNumber);
		}
	} 

	$('body').bind('touchmove', function(e) { 
		if( $('.lazyloadscrolltour').length > 0 ) { // if target element exists in DOM
			if( $('.lazyloadscrolltour').is_on_screen() ) { // if target element is visible on screen after DOM loaded
				displayhotels(postNumber);
			}
		}
	});
	$(window).scroll(function(){ // bind window scroll event 
		console.log('HIDS');
		if( $('.lazyloadscrolltour').length > 0 ) { // if target element exists in DOM
			console.log('HIDS MISD');
			if( $('.lazyloadscrolltour').is_on_screen() ) { // if target element is visible on screen after DOM loaded
				console.log('HIDS MISD nkdn kfdskf');
				displayhotels(postNumber);
			}
		}
	});

	aboutplace(3);
});

/* check parameter exist in URL*/
function getQueryVariable(variable) {
   var query = decodeURIComponent(window.location.search.substring(1));
   var vars = query.split("&");
   for (var i=0;i<vars.length;i++) {
       var pair = vars[i].split("=");
       if(pair[0] == variable){
        $pair = pair[1].replace(/\+/g, ' ');
        return $pair;
       }
   }
   return(false);
}

/* listing hotel with particular location*/
function displayhotelswithparticular(city, name) {
	if(city != undefined && name != undefined) {
	    $.ajax({
	        type: 'POST',
	        url: '?r=tours/getlistwithparticular', 
	        data: {
	        	'country' : city,
	        	'name' : name    
	        },
	        success: function (data)
	        {
	        	$("#tours-list").html(data);
	        }
	    });
	}
}

/* select city */
function selectcity() {
    var country = $("#chooseCountry").val();
    $.ajax({
        type: 'POST',
        url: '?r=tours/getcity', 
        data: {country},
        success: function (data) {
            $("#chooseCity").html(data);
        }
    });
}

/* display hotels list */
function displayhotels($lazyhelpcount=0) {
	console.log('$lazyhelpcount '+ $lazyhelpcount);
    var country = 'Aqaba';
	$('#tours-list').find('.lazyloadscrolltour').removeClass('lazyloadscrolltour');
    $.ajax({
        type: 'POST',
        url: '?r=tours/getlist', 
        data: {country, $lazyhelpcount},
        success: function (data) {
        	if(data !=  '') {
				$("#tours-list").find('ul').append(data);
				postNumber = $lazyhelpcount + 1;
				$('#tours-list').find('.tourbox').last().addClass('lazyloadscrolltour');
			}					
        }
    });
}


function displayhotelsUniq() {
    $.ajax({
        url: '?r=tours/getlist', 
        success: function (data) {
        	if(data != '') {
				$("#tours-list").html(data);
			}					
        }
    });
}

/* FUN manage expand map */
function expandMap(obj,target) {		
		var sparent=$(obj).parents(".map-holder");
	sparent.find(".overlay").hide();
	sparent.find("a.closelink").show();
	sparent.addClass("expanded");	
	var orgScroll = $('body').scrollTop()-30;
	if(($(target).offset().top)>100 && !$(obj).parents(".subtab").length>0) {
		$('body').animate({
			scrollTop: (eval($(target).offset().top-30))+orgScroll
		}, 1000);
	}
	$(".places-mapview").addClass("showMapView");
	setTimeout(function(){initNiceScroll(".places-mapview .nice-scroll");},500);
	$('body').animate({
		scrollTop:0
	}, 1000);
}

function shrinkMap(obj) {
	var sparent=$(obj).parents(".map-holder");
	sparent.find(".overlay").show();
	sparent.find("a.closelink").hide();
	sparent.removeClass("expanded");	
	if($(".places-mapview").hasClass("showMapView")){
		$(".places-mapview").removeClass("showMapView");
	}
}
/* FUN end manage expand map */
