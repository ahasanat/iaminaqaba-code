var current = 0;	
var interval;

/* start animate css plugin code for moving card bottom to up slowly */
$.fn.extend({
  animateCss: function(animationName, callback) {
    var animationEnd = (function(el) {
      var animations = {
        animation: 'animationend',
        OAnimation: 'oAnimationEnd',
        MozAnimation: 'mozAnimationEnd',
        WebkitAnimation: 'webkitAnimationEnd',
      };

      for (var t in animations) {
        if (el.style[t] !== undefined) {
          return animations[t];
        }
      }
    })(document.createElement('div'));

    this.addClass('animated ' + animationName).one(animationEnd, function() {
      $(this).removeClass('animated ' + animationName);

      if (typeof callback === 'function') callback();
    });

    return this;
  },
});
/* end animate css plugin code for moving card bottom to up slowly */
	$(document).on('click', '.notificationfrdrqt', function() {
		$('#frdrqtdebugger').click();
	});
	
	$(document).on('click', '.footer_booking', function() {
		$('.footer_alert').removeClass('active');
		$('.master_alert').removeClass('active');
	}); 
	
	$(".action-icon.activity-link").click(function(){
		$(".activity-content").addClass("showactivity");
		$(".tab-pane.main-tabpane.active").hide();
		$(".custom-wall-links .tab a").removeClass("active");
	});

/* new post options */	
	$("body").on("click", "ul.echeck-list > li", function(e){
		var isSelected=$(this).hasClass("selected");
		if(isSelected)
			$(this).removeClass("selected");
		else
			$(this).addClass("selected");

		e.stopPropagation();
	});
	/* end new post options */

/* document ready */
	$(document).ready(function() {
		var textAreas = [].slice.call(document.querySelectorAll('textarea[data-adaptheight]'));
		textAreas.forEach(function(el) {
			el.style.boxSizing = el.style.mozBoxSizing = 'border-box';
			el.style.overflowY = 'hidden';
			var minHeight = el.scrollHeight;
			el.addEventListener('input', function() {
				console.log('input');
				adjustHeight(el, minHeight);
			});
			/*el.addEventListener('focus', function() {
				console.log('focus');
				adjustHeight(el, minHeight);
			});*/
			window.addEventListener('resize', function() {
				console.log('resize');
				adjustHeight(el, minHeight);
			});
			adjustHeight(el, minHeight);
		});

		var winw=$(window).width();
		$headernav = $('.header-nav').width();
		$__s = parseInt(winw) - parseInt($headernav);
		$__s = parseInt($__s) / 2;
		$('.sticky-nav').find('.col').css('width', $headernav+'px');
		$('.sticky-nav').find('.col').css('margin-left', $__s+'px');

		/* Lazy Loading Handler */
		$.fn.is_on_screen = function(){
			var win = $(window);
			var viewport = {
				top : win.scrollTop(),
				left : win.scrollLeft()
			};
			viewport.right = viewport.left + win.width();
			viewport.bottom = viewport.top + win.height();
			 
			var bounds = this.offset();
			bounds.right = bounds.left + this.outerWidth();
			bounds.bottom = bounds.top + this.outerHeight();
			return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
		};
		if( $('.lazyloadscrolluploadstuff').length > 0 ) { // if target element exists in DOM
			if( $('.lazyloadscrolluploadstuff').is_on_screen() ) { // if target element is visible on screen after DOM loaded
				$('.lazyloadscrolluploadstuff').removeClass("lazyloadscrolluploadstuff");
				fetchLoadPost(postNumber);
			}
		} 
			
		$('body').bind('touchmove', function(e) { 
			if( $('.lazyloadscrolluploadstuff').length > 0 ) { // if target element exists in DOM
				if( $('.lazyloadscrolluploadstuff').is_on_screen() ) { // if target element is visible on screen after DOM loaded
					$('.lazyloadscrolluploadstuff').removeClass("lazyloadscrolluploadstuff");
					fetchLoadPost(postNumber);
				}
			}
		});
		
		$(window).scroll(function(){ // bind window scroll event	
			if( $('.lazyloadscrolluploadstuff').length > 0 ) { // if target element exists in DOM
				if( $('.lazyloadscrolluploadstuff').is_on_screen() ) { // if target element is visible on screen after DOM loaded
					$('.lazyloadscrolluploadstuff').removeClass("lazyloadscrolluploadstuff");
					fetchLoadPost(postNumber);
				}
			}
		});
		/* END Lazy Loading Handler */

		/*var urlid = getQueryVariable('r');
		if(urlid != undefined && urlid != 'undefined' && urlid != null && urlid != '' && urlid != 'site/complete-profile') {
			Isuncompleteprofile();
		}*/

		/* Start Display Post Comment in Mobile Screen Post-comment Popup */
		$(document).on('click', '.pa-comment', function(e) {
			if($(this).hasClass('checkuserauthclassnv')) {
				checkuserauthclassnv();
			} else if($(this).hasClass('checkuserauthclassg')) {
				checkuserauthclassg();
			} else {
				if($('#gallery-content').length) {
					if($('#gallery-content').hasClass('active')) {
						return false;
					}
				}

				if ($(window).width() >= 569) {					 
					$(this).parents('.post-data').find(".comments-section").slideToggle("slow");
				} else {

					var postid = $(this).data('id');
					var modalID = $(this).parents(".modal").attr("id");
					if(modalID == "postopenmodal") {
						$(".modal.open").find(".addnew-comment").find("#comment_txt_"+postid).focus();
						setTimeout(function(){
							$(".modal.open").find(".addnew-comment").find(".sliding-middle-out").trigger( "click" );
						},300);
					} else {
						var postid = $(this).data('id');
						var from_ctr = $('#from_ctr_' + postid).val('3');
						$.ajax({
							type: 'POST',
							url: '?r=site/mobile-post', 
							data:{postid},
							success: function(data) {
								$('#postopenmodal').modal('open');
								$("#postopenmodal").html(data);		
								$("#postopenmodal .comments-section").addClass("open");
								
								setTimeout(function(){fixImageUI();
									$("#postopenmodal .overlay-link.postdetail-link").remove();
									$("#postopenmodal .overlay-link.comments-popup").remove();
									initDropdown();
								},400);
							},
						});

						/*var from_ctr = $('#from_ctr_' + postid).val('3');
						$.ajax({
							type: 'POST',
							url: '?r=site/mobile-comment', 
							data:{postid},
							success: function(data) {
								$("#comment_modal_xs").html(data);		
								$('#comment_modal_xs').modal('open');
							},
						 }).done(function(){
							   setTimeout(function(){initDropdown();},500);
						  });*/		
					}
				}
			}
		});	
		/* End Display Post Comment in Mobile Screen Post-comment Popup */
	
		/************ chat functions ***********/
		setChatHeight();
		setFloatChat();
		manageChatbox();
		/************ end chat functions ***********/
		/************ ESSENTIALS FUNCTIONS ************/
			 
			/*======= START Needed only login user's ============*/
			$.ajax({
				url: '?r=site/checkloginstatus', 
				success: function (data) {
					if(data) {
						countingCall();
						setInterval(function () {
							countingCall();
					   	}, 5000);
					}
				}
			});
			/*======= END Needed only login user's ============*/

		   	var win_w=$(window).width();
			var fromTop1=$(window).scrollTop();
			if($(".page-wrapper").hasClass("transheadereffect")) {
				if(fromTop1 > 0){
					if($(".page-wrapper").hasClass("transheadereffectall")) {
						$(".page-wrapper").addClass('page-scrolled');
						$(".page-wrapper").removeClass('JIS3829');
					} else if(win_w<=568) {
						$(".page-wrapper").addClass('page-scrolled');
					}
				} else {
					if($(".page-wrapper").hasClass("transheadereffectall")) {
						$(".page-wrapper").removeClass('page-scrolled');
						$(".page-wrapper").addClass('JIS3829');
					} else if(win_w<=568){
						$(".page-wrapper").removeClass('page-scrolled');
					}
				}
			}
			
			/* nice scroll */		
			var win_w=$(window).width();
			if(win_w>2000){
				var isHome=$(".home-page.bodydup").length;				
				if(isHome){
					initNiceScroll("homebody");
				}
				initNiceScroll(".nice-scroll");
			}
			else{
				$(".sidemenu").css("opacity",1);
				removeNiceScroll(".nice-scroll");			
			}
			/* end nice scroll */		
			
			var win_w=$(window).width();		
			if(win_w>767){
				if($('body').hasClass("hideHeader")){
					$('body').removeClass("hideHeader");
				}
			}
			
			/* set new post bubble */			
			var top=$(window).scrollTop();
			var winw=$(window).width();
			
			if(!$(".home-page.bodydup").length) {
				if(winw > 767){
					top=$(window).scrollTop();			
				}else{
					if ($(".page-wrapper")[0]){
						top = $('.page-wrapper').offset().top;
					} else if ($(".home-wrapper")[0]){
						top = $('.home-wrapper').offset().top;
					}
				}
				setNewPostBubble(winw,top,'docready');
			}
			/* end set new post bubble */

			if($('#place_loc').length) {
				$("#getplaceinfo").click(function() {
					var p = $('#place_loc').val();
					if(p != '') {
						window.location.href = "?r=places&p="+p;
					}
				});

				$('body').on( "keypress","#place_loc",function (e) {
					if (e.which == 13) {
					  var p = $('#place_loc').val();
						if(p != '') {
							window.location.href = "?r=places&p="+p;
						}
					}
				});
			}
			
			/* manage left menu */
			setLeftMenu();
			/* end manage left menu */
									
			/* fix centering image */
			setTimeout(function(){fixImageUI();},400);
			/* end fix centering image */
			
			/* popup */
			
			// Open Magnific popup
			$(document.body).on('click', "a.popup-modal", function(e){
				
				var win_w = $(window).width();
				if(win_w >= 768){
					$("#newpost-popup").find('.post-bcontent').hide();
				}
				else
				{
					$("#newpost-popup").find('.post-bcontent').show();
				}
				e.preventDefault();       
			});
			// END Open Magnific popup 
			$(document).on('click', '.close-popup', function (e) {
				if($(this).hasClass("notpopup")) {
					var col = $(this).closest('form').attr('id');
					var getparntcls = $("#"+col).parent().attr('id');
					if(getparntcls) {
					resetNewPost("#"+getparntcls);
					} else {
					resetNewPost();
					}
				} else {
				
					var col = $('.close-popup').closest('form').attr('id');
					var getparntcls = $("#"+col).parent().attr('id');
					resetNewPost("#"+getparntcls);
					
					var modalDiv = $(this).parents(".popup-area");
					var modalId=modalDiv.attr("id");
					if(modalId=="newpost-popup"){
						e.preventDefault();
					}
					else if(modalId=="privacy-popup"){
						e.preventDefault();
					}
					else if(modalId=="change-cover" || modalId=="change-profile"){
						e.preventDefault();					
						resetAllPopupSection("cover");
						resetAllPopupSection("profile");
					}
					else{
						e.preventDefault();
					}
				}
				//resetNewPost("#"+getparntcls);
			});
			/* end popup */
			 
			/* mobile screen : manage innerpage and header with page name */
				/*$(document).on('click', '.open-innerpage .tabs li a',function(){
				
				var win_w = $(window).width();
				if(win_w < 768){
					
					var getusername=$(this).attr("pagename");
					
					$(".header-themebar .mbl-innerhead").show();
					$(".header-themebar .mbl-innerhead").find('.page-name').html(getusername);
					
					$(this).parents(".open-innerpage").find(".tabs-detail").removeClass("gone");
				}
			});*/
			/* end mobile screen : manage innerpage and header with page name */
			
			/* side icons visibility */
			$(".scrollup-float").hide();
			/* end side icons visibility */
									
			/* side icons visibility */
			$(".scrollup-float").click(function(){ 
				$('html,body').animate({ scrollTop: 0 }, 'slow');
				return false; 
			});
			/* end side icons visibility */
			
			/* percentage page loader */	
			runLoad();
			/* end percentage page loader */
			
			/* search result */	
			$(".search-btn").click(function (e) {		
				manageSearch(this);		 
			});
			$(".search-input").keyup(function() {
			  var textValue = $(this).val();
			  var isSearchResult=$(".search-result").css("display");
			  
			  if(isSearchResult=="none"){
				$(".search-result").slideDown();
			  }
			});

			$(".search-input-connections").keyup(function() {
			    var textValue = $(this).val();
			    var isSearchResult=$(".search-connections-result").css("display");
			  
			    if(isSearchResult=="none"){
					$(".search-connections-result").slideDown();
			    }
			});
			/* end search result */
			
			/* side float icons */
			$(".btnbox").click(function(){
				var cls=$(this).attr("class");
				cls=cls.replace("box-open","").trim();		
				closeAllSideDrawer(cls);
				$(this).toggleClass("box-open");
				
			});
			/* end side float icons */
			
			/* home page layout */
			setTimeout(function() {				
				var isHome=$(".home-page.bodydup").length;
				if(isHome){
					$("body").on('input propertychange', '#lemail',validate_lemail);					
					$('.home-wrapper').fadeIn(500);					
				}
			},200);				
			/* end home page layout */			
			
			/* start login enter key check */
			$("#lemail").keypress(function (e) {
				check_enter(this, e);
			});
			$("#lpassword").keypress(function (e) {
				check_enter(this, e);
			});
			/* end login enter key check */
			
			/* user authentication */	
			$(document).on('click', '.directcheckuserauthclass', function(obj) {
				if($(this).hasClass('checkuserauthclassnv')) {
					checkuserauthclassnv();
				} else if($(this).hasClass('checkuserauthclassg')) {
					checkuserauthclassg();
				}
			});
			/* end user authentication */
					
			/* Facebook Login */        
			var signinWin;
			var popupWidth=600;
			var popupHeight=250;
			var xPosition=($(window).width()-popupWidth)/2;
			var yPosition=($(window).height()-popupHeight)/2;

			$(document).on('click', '#FacebookBtn', function(obj) {				
			   signinWin = window.open("?r=site/auth&authclient=facebook", "SignIn", "width="+popupWidth+",height="+popupHeight+",toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left="+xPosition+",top="+yPosition);
			   signinWin.focus();
			   return false;
			}); 

			$(document).on('click', '#GoogleBtn', function(obj) {				
			   signinWin = window.open("?r=google/auth&authclient=google", "SignIn", "width="+popupWidth+",height="+popupHeight+",toolbar=0,scrollbars=0,status=0,resizable=0,location=0,menuBar=0,left="+xPosition+",top="+yPosition);
			   signinWin.focus();
			   return false;
			}); 
			/* Facebook Login */ 

			$(document).on('click', '#filled-in-box', function() {
				if($("#filled-in-box").prop('checked') == true) {
					$("#filled-in-box2").removeAttr("disabled");
					$("#filled-in-box2").attr('checked', false);
				} else {
					$("#filled-in-box").attr('checked', false);
					$("#filled-in-box2").attr('checked', false);
					$("#filled-in-box2").attr("disabled", true);
				}
			});
		/************ END ESSENTIALS FUNCTIONS ************/
		
		/************ GENERAL PAGE FUNCTIONS ************/
		
			/* settings up newpost bubble - general page */
			$("body").on("click", ".general-page .right-tabs.pagetabs .tabs li a", function(e){resetGeneralPageTabs(this);	});
			$("body").on("click", ".general-page .right-tabs.pagetabs .tabs li a", function(e){resetGeneralPageTabs(this);	});
			/* end settings up newpost bubble - general page */
			
			/* show general-details page summery expandable */
			reset_gdetails_expandable();
			/* end general-details page summery expandable */
				
		/************ END GENERAL PAGE FUNCTIONS ************/
				
		/************ COMMON FUNCTIONS ************/
			
			/* search input text entered */
			$("body").on('keyup', '.search-input',searchStringEntered);
			/* end search input text entered */
			
			/* custom tooltip */
			$('.custom-tooltip').tooltipster({
				contentAsHTML: true,
				side: ['top', 'bottom'],theme: ['tooltipster-borderless'],
				contentCloning: true,
				interactive:true
			});
			/* end custom tooltip */
			
			/* view likes and comments of an album photo */
			/*$("body").on("click", ".view-likes", function(){
				var tid = $(this).attr('data-id');
				var tsec = $(this).attr('data-section');
			
				if(tid && tsec)
					openLikePopup(tid,tsec);
				
			});			*/
			$("body").on("click", ".view-comments", function(){
				var tid = $(this).attr('data-id');
				var tsec = $(this).attr('data-section');
			});	
			/* end view likes and comments of an album photo */		
						
			/* set security option dropdown */		
			$("body").on("click", ".custom-drop .dropdown-menu li a", function(){setDropVal(this);});
			$("body").on("click", ".setDropVal .dropdown-menu li a", function(){setDropVal(this);});		
			/* end set security option dropdown */		
			
			/* invite section scrollbar */
			$(".suggest-connections .cbox-desc").niceScroll({horizrailenabled:true,cursorcolor:"#bbb",cursorwidth:"8px",cursorborderradius:"0",cursorborder:"0px solid #fff",background:"rgba(255,255,255,0.6)",autohidemode:"false"});
			/* end invite section scrollbar */
			
			/* search closable */
			$(document).on('click', '.closable-search a', function (e) {	
				$(this).parents('.fsearch-form').toggleClass('open');
			});
			/* end search closable */
		
			/* range slider initalization */
			if($("#distance-slider").length>0)
				initSlidernew("distance-slider",0,100,1,'km');
			if($("#price-slider").length>0)
				initSlidernew("price-slider",500,10000,2500,'$');
			/* end range slider initalization */
			
			/* manage expandable -photo slider : hotels/places : more info */			
			$(".subtab-menu > li").click(function(e){initSubtabSlider(this);});
			/* end manage expandable -photo slider : hotels/places : more info */
			
			// reset split page : mbl-fiter-icon
			resetMblFilterIcon();
			
			/* credit plan selection */
			$('input[name="credit-add"]').click(function () {
				$('input:not(:checked)').parent('li').removeClass("active");
				$('input:checked').parent('li').addClass("active");
				var credit = $('input:checked').parent('li').data('crplan');
				$("#buy_credits").val(credit);
				
				/* dynamic amount of credit plans*/
				$.ajax({
					url: '?r=site/creditplanamount', 
					type: 'POST',
					async: false,
					data: 'selected_credit_plan=' + credit,
					success: function (data) {
						$("#credit_amount").val(data);
					}
				});
				/* dynamic amount of credit plans*/				
			}); 
			/* end credit plan selection */
			
			/* init image gallery slider for hotels page */
			if(!$(".main-page").hasClass("hotels-page") && !$(".main-page").hasClass("places-page")){			
				initGalleryImageSlider(); //temporary comment out
			}
			/* end init image gallery slider for hotels page */
			
			/* autogrow textarea */
			//autosize(document.getElementsByClassName("autogrow-tt"));	
			/* end autogrow textarea */
			
			/* add photos to new post */
			$(document.body).on("change", ".custom-upload", function () {
				var getid = $(this).attr('id');
				var cls = $(this).data("class");
				if(cls==undefined || cls==null) return false;
				var newcls = cls;
				applypostloader('SHOW');
				changePhotoFileInput(newcls, this, true);        
			});
			
			$(document.body).on("change", ".hidden_uploader", function () {
				storedFiles=[];
				resetAddPhotoPopup();
				var cls = $(this).data("class");

				if($(this).hasClass('wall')) {
					$type = 'wall';
				} else if($(this).hasClass('page')) { 
					$type = 'page';
				} else {
					$type = 'other';
				}
				var myThis = $(this)[0];
				$.ajax({
					type: "POST",
					url: "?r=userwall/add-photo-popup", 
					data: {
						type: $type
					},
					success: function (data) {
						$('#add-photo-popup').html(data);
						$('#add-photo-popup').modal('open');
						changePhotoFileInput(cls, myThis, true); 
					}
				});
			}); 

					
			/* dropdown resist click */
			$("body").on("click", ".dropdown.resist a.dropdown-toggle", function(e){			
				$(this).parent().toggleClass('open');
			});
			/* end dropdown resist click */
			
			/* text input animation */				
				$("body").on("click", ".sliding-middle-out", function(e){titleUnderline(this);});
			/* end text input animation */	 
			
			if($('.timeText').length>0) {
				$('.timeText').timepicker({ 'step': 5 });
			}
			
			/* Profile ToolTip Initialize */
			$('body').on('mouseenter', '.profile-tooltip:not(.tooltipstered)', function(){
				var id = $(this).attr('id');
				if(id) {
					$('#dispatchcontent'+id).css('display', 'block');
					$(this)
					.tooltipster({
						content: $('#dispatchcontent'+id).detach(),
						multiple: false,
						side: ['top', 'bottom'],
						theme: 'tooltipster-profiletip',
						contentCloning: true,
						interactive: true,
						//trigger: 'click'
					})
						.tooltipster('open');
				}
			});
			
			$('body').on('mouseenter', '.connect-tooltip:not(.tooltipstered)', function(){
				var id = $(this).attr('id');
				if(id) {
					$('#connectprofiletip_'+id).css('display', 'block');
					$(this)
						.tooltipster({
							content: $('#connectprofiletip_'+id).detach(),
							multiple: false,
							side: ['top','bottom'],
							theme: 'tooltipster-profiletip',
							contentCloning: true,
							interactive: true
						})
						.tooltipster('open');
				}
			});
			/* END Prifile ToolTip Initialize */
			
			/* like tooltip */
			$('body').on('mouseenter', '.liveliketooltip', function(){
				var likecon = $(this).attr('data-title');
				if(likecon) {
					$(this).tooltipster({ 
							contentAsHTML: true,
							theme: 'tooltipster-borderless',
							contentCloning: true
						   })
						.tooltipster('content', likecon)
						.tooltipster('open');
				
				} else {
						//$(this).tooltipster('destroy'); 
				}
			});
			$('body').on('mouseenter', '.livetooltip:not(.tooltipstered)', function(){
			  $(this)
				  .tooltipster({ 
					contentAsHTML: true,
					theme: 'tooltipster-borderless'
				   })
				  .tooltipster('open');
			});
			/* end like tooltip */
			
			// try to make general
			$( document ).on( 'keyup', '.inviteconnect', function(e) {
				var baseurl = $("#baseurl").val();
				var id = $(this).attr('data-id');
				var textValue = $(this).val().trim();
				data = 'key='+textValue+'&baseurl='+baseurl;
				$.ajax({
					url: "?r=page/inviteconnect", 
					type: 'GET',
					data: data,
					async: false,
					success: function (data) {
						
					}
				});
			});

			/* Start Invite Connect Search From Event*/
			$( document ).on( 'keyup keypress paste click', '.invite_connect_event', function(e) {
				var id = $(this).attr('data-id');
				var textValue = $.trim($(this).val()); 
				var baseurl = $("#baseurl").val();
				var data = 'key='+textValue+'&baseurl='+baseurl;
				$.ajax({
					url: "?r=event/invite-connections-event", 
					type: 'GET',
					data: data, 
					async: false,
					success: function (data) {	 
						$(".block"+id).html(data).show();
					}
				});
			});
			/* End Invite Connect Search From Event*/

			/* Start Invite Connect Search From Event*/
			$(document).on( 'keyup keypress paste click', '.cretranXHIL213 #connect_name', function(e) {
				var key = $.trim($(this).val());
				$.ajax({
					url: "?r=event/search-connect", 
					type: 'GET', 
					data: {key}, 
					success: function (data) {	  
						$("#transfercreditsUI").html(data);
					}
				});
			});

			
			$(document).on('click', '#transfercreditsUI li', function() {
				$name = $(this).find('.unm').text();
				$id = $(this).attr('data-uid');
				if($name) {
					$('.search-connections-result').hide();
					$('.cretranXHIL213').find('#connect_name').val($name);
					$('.cretranXHIL213').find('#connect_name').attr('data-uid', $id);
				}
			});
			/* End Invite Connect Search From Event*/


			/* Start Invite Connect Search From Page*/
			 $( document ).on( 'keyup', '.invite_connect_review', function(e) {
				var id = $(this).attr('data-id');
				var textValue = $(this).val();
				var baseurl = $("#baseurl").val();
				data = 'key='+textValue+'&baseurl='+baseurl;
				$.ajax({
					url: "?r=page/invite-connections-review", 
					type: 'GET',
					data: data,
					success: function (data) {				
						$(".block"+id).html(data).show();
					}
				});
			 });
			 /* End Invite Connect Search From Page*/
			    
			 /* Start Invite Connect Search From Page*/
			 $( document ).on('keyup', '.invite_connect_search', function(e) {
				var id = $(this).attr('data-id');
				var textValue = $(this).val();
				var baseurl = $("#baseurl").val();
				data = 'key='+textValue+'&baseurl='+baseurl;
				$.ajax({
					url: "?r=page/invite-connections", 
					type: 'GET',
					data: data,
					async: false,
					success: function (data) {
						$(".block"+id).html(data).show();
					}
				});
			 });
			/* End Invite Connect Search From Page*/				
		/************ END COMMON FUNCTIONS ************/
				
		/************ VIP/CREDIT/VERIFY FUNCTIONS ************/
					
			/* select plans */
			$('ul.vip_plan li').click(function() { 
				var vip = $(this).attr('data-viplan');
				$("#selected_vip_plan").val(vip);
				
				var selected_vip_plan = $('#selected_vip_plan').val();
				
				/* dynamic amount*/
				$.ajax({
					url: '?r=vip/amount', 
					type: 'POST',
					async: false,
					data: 'selected_vip_plan=' + selected_vip_plan,
					success: function (data) {
						$(".vip_plan_amount").val(data);
					}
				});
				/* dynamic amount*/
			});
			/* end select plans */
			
			/* plan verification */		
			$('ul.verify_plan li').click(function() { 
				var verify = $(this).attr('data-verifyplan');
				$("#selected_verify_plan").val(verify);
				
				var selected_verify_plan = $('#selected_verify_plan').val();
				
				/* dynamic amount*/
				$.ajax({
					url: '?r=vip/amount-verify', 
					type: 'POST',
					async: false,
					data: 'selected_verify_plan=' + selected_verify_plan,
					success: function (data) {
						//$("#vip_plan_amount").val(data);
						$(".verify_plan_amount").val(data);
					}
				});
				/* dynamic amount*/
				
			});
			/* end plan verification */		
			
			/* Credit Plans amount*/
			$('ul.credit_plan li').click(function(){ 
				var credit = $(this).attr('data-creditplan');
				$("#buy_credits").val(credit);
				
				var selected_credit_plan = $('#buy_credits').val();
				
				
				$.ajax({
					url: '?r=site/creditplanamount', 
					type: 'POST',
					async: false,
					data: 'selected_credit_plan=' + credit,
					success: function (data) {
						$(".credit_amount").val(data);
						$(".credit_plan_amount").val(data);
					}
				});
				
				
			});
			/* Credit Plans amount*/
		
		/************ END VIP/CREDIT/VERIFY FUNCTIONS ************/
		
		$(document).on('click', '.toggleSlider', function (e) {		
			var detailMode=$(this).parents(".mode-holder").find(".detail-mode");
			if(detailMode.css("display")=="none"){
				$(this).parents(".mode-holder").find(".normal-mode").fadeOut();
			}else{				
				$(this).parents(".mode-holder").find(".normal-mode").fadeIn();
			}
			detailMode.toggle();			
		});

		initNiceScroll(".nice-scroll");
		fixPostImages();
		lightGalleryinitialize();
		lightGalleryinitializenormal();
	});
/* end document ready */

/************ ESSENTIALS FUNCTIONS ************/

/* window scrolled */
		
	$(window).scroll(function() {
		
		var fromTop=$(window).scrollTop();
		var winh=$(window).height();
		var top=$(window).scrollTop();
		var winw=$(window).width();
		var win_w = $(window).width();
		
		setNewPostBubble(winw,top,'window');
		
		if(fromTop > 100 && winw > 767){
			$(".scrollup-float").show();					
			if(!$(".floating .chat-button").parent().hasClass("chat-open"))
				$(".floating .chat-button").addClass("chathide");			
		}
		else{
			$(".scrollup-float").hide();		
			if(!$(".floating .chat-button").parent().hasClass("chat-open"))
				$(".floating .chat-button").removeClass("chathide");			
		}
		

		/* fixed header */
		if(fromTop > 0){
			if($(".home-page.bodydup").length){
				$("header").addClass("fixed-header");
			}
			if(winw>1024){				
				$(".header-section").addClass("fixed-header");
				$(".page-wrapper").addClass('fixed-wrapper');			
			}
			
			if(winw<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").show();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").addClass('page-scrolled');
				$(".page-wrapper").removeClass('JIS3829');
			} else if(winw<=568){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").addClass('page-scrolled');
				}
			}
		} else {
			if($(".home-page.bodydup").length) {
				$("header").removeClass("fixed-header");
			}
			$(".header-section").removeClass("fixed-header");
			$(".page-wrapper").removeClass('fixed-wrapper');
			
			if(winw<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").hide();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").removeClass('page-scrolled');
				$(".page-wrapper").addClass('JIS3829');
			} else if(winw<=568){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").removeClass('page-scrolled');
				}
			}
		}                                  
		/* end fixed header */                            
		                                                           
		/* bottom of the page reached */		                                               
		if($(window).scrollTop() + $(window).height() == $(document).height()) {		   
		   $(".page-wrapper").addClass('page-bottom');
	    }else{
		   $(".page-wrapper").removeClass('page-bottom');
	    }
		/* end bottom of the page reached */
		
		/* colored header */
		if(fromTop > 0){
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("transheader-wrapper"))
				$(".header-section").addClass("colored");
		}else{
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("transheader-wrapper"))
				$(".header-section").removeClass("colored");				
		}
		/* end colored header */
		
		/* fixed header chat */		
		if(fromTop > 0){
			if($(".float-chat").hasClass("chat-open")){
				$(".float-chat").addClass("stickToTop");				
			}
		} else {
			if($(".float-chat").hasClass("stickToTop")){			
				$(".float-chat").removeClass("stickToTop");
			}
		}
		/* end fixed header chat */
		
		/* fixed header */		
		if(fromTop > 78){
			if(win_w<1170){								
				$(".float-chat").addClass("fixed-chat");
			}
			if(win_w <= 992 && win_w > 767){
				
				if($(".main-page").hasClass("places-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
		}
		else if(fromTop > 50){
			if(win_w<1170){
				$(".sidemenu-holder").addClass("fixed-menu");				
			}
			if(win_w <= 992){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
			if(win_w <= 800){
				$(".gdetails-summery .search-area").addClass("fixed-filter");
			}
		}
		else if(fromTop > 5){
			
			if(win_w <= 767){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}				
			}
			
		}else {			
			$(".sidemenu-holder").removeClass("fixed-menu");
			
			$(".float-chat").removeClass("fixed-chat");
			
			$(".gdetails-summery .search-area").removeClass("fixed-filter");
			
			if($(".main-page").hasClass("hotels-page") || $(".main-page").hasClass("places-page")){
				$(".search-area").removeClass("fixed-filter");
			}
		}
		/* end fixed header */
		
		var winw=$(window).width();
		if(winw>1024){
		   if($(window).scrollTop() + $(window).height() > $(document).height() - 40) {
			   $(".chat-window").addClass('stick-to-footer');
		   }else{
			   $(".chat-window").removeClass('stick-to-footer');		   
		   } 
		}else{
			if($(".chat-window").hasClass('stick-to-footer')) $(".chat-window").removeClass('stick-to-footer');
		}

	});
	
/* end window scrolled */

/* fixed layout scrolled */

	$(".fixed-layout").scroll(function() {
		
		var top = $('.page-wrapper').offset().top;
		var win_w = $(window).width();
		
		setNewPostBubble(win_w,top,'fixed-layout');
		
		var fromTop=$('.fixed-layout').scrollTop();
		
		/* fixed header */				
	
		if(fromTop > 0){
			if(win_w>1024){				
				$(".header-section").addClass("fixed-header");
				$(".page-wrapper").addClass('fixed-wrapper');			
			}
			
			if(win_w<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").show();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").addClass('page-scrolled');
				$(".page-wrapper").removeClass('JIS3829');
			} else if(winw<=568){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").addClass('page-scrolled');
				}
			}
		} else {
			$(".header-section").removeClass("fixed-header");
			$(".page-wrapper").removeClass('fixed-wrapper');
			
			if(win_w<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").hide();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").removeClass('page-scrolled');
				$(".page-wrapper").addClass('JIS3829');
			} else if(winw<=568){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").removeClass('page-scrolled');
				}
			}
		}
		/* end fixed header */
		
		/* bottom of the page reached */		
		if($(window).scrollTop() + $(window).height() == $(document).height()) {		   
		   $(".page-wrapper").addClass('page-bottom');
	    }else{
		   $(".page-wrapper").removeClass('page-bottom');
	    }
		/* end bottom of the page reached */
		
		/* fixed header */		
		if(fromTop > 78){
			if(win_w<1170){								
				$(".float-chat").addClass("fixed-chat");
			}
			if(win_w <= 992 && win_w > 767){
				
				if($(".main-page").hasClass("places-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
		}
		else if(fromTop > 50){
			if(win_w<1170){
				$(".sidemenu-holder").addClass("fixed-menu");				
			}
			if(win_w <= 992){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}			
			if(win_w <= 800){
				$(".gdetails-summery .search-area").addClass("fixed-filter");
			}
		}
		else if(fromTop > 5){
			
			if(win_w <= 767){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}				
			}
			
		}else {			
			$(".sidemenu-holder").removeClass("fixed-menu");
			
			$(".float-chat").removeClass("fixed-chat");
			
			$(".gdetails-summery .search-area").removeClass("fixed-filter");
			
			if($(".main-page").hasClass("hotels-page") || $(".main-page").hasClass("places-page")){
				$(".search-area").removeClass("fixed-filter");
			}
		}
		/* end fixed header */
		
	});
	
/* end fixed layout scrolled */

/* body scrolled */

	$('body').scroll(function() {
	
		var top = $('.page-wrapper').offset().top;
		var win_w = $(window).width();
		
		setNewPostBubble(win_w,top,'body');
		
		var fromTop=$('body').scrollTop();

		/* fixed header */				
		if(fromTop > 0 || top < 0){
			if(win_w>1024){				
				$(".header-section").addClass("fixed-header");
				$(".page-wrapper").addClass('fixed-wrapper');			
			}
			if(win_w<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").show();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").addClass('page-scrolled');
				$(".page-wrapper").removeClass('JIS3829');
			} else if(winw<=568){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").addClass('page-scrolled');
				}
			}
		} else {
			$(".header-section").removeClass("fixed-header");
			$(".page-wrapper").removeClass('fixed-wrapper');
			
			if(win_w<=568){
				if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
					$(".header-section .mainpage-name").hide();
				}
			}

			if($(".page-wrapper").hasClass("transheadereffectall")) {
				$(".page-wrapper").removeClass('page-scrolled');
				$(".page-wrapper").addClass('JIS3829');
			} else if(winw<=568){
				if($(".page-wrapper").hasClass("transheadereffect")){
					$(".page-wrapper").removeClass('page-scrolled');
				}
			}
		}
		/* end fixed header */
		
		/* colored header */
		if(fromTop > 0){
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("transheader-wrapper"))
				$(".header-section").addClass("colored");
		}else{
			if($(".page-wrapper").hasClass("place-wrapper") || $(".page-wrapper").hasClass("transheader-wrapper"))
				$(".header-section").removeClass("colored");				
		}
		/* end colored header */
		
		/* bottom of the page reached */		
		if($(window).scrollTop() + $(window).height() == $(document).height()) {		   
		   $(".page-wrapper").addClass('page-bottom');
	    }else{
		   $(".page-wrapper").removeClass('page-bottom');
	    }
		/* end bottom of the page reached */
		
		/* fixed header */		
		if(fromTop > 78) {
			if(win_w<1170) {								
				$(".float-chat").addClass("fixed-chat");				
			}
			if(win_w <= 992 && win_w > 767){
				
				if($(".main-page").hasClass("places-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}
		}
		else if(fromTop > 50){
			if(win_w<1170){
				$(".sidemenu-holder").addClass("fixed-menu");				
			}
			if(win_w <= 992){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}
			if(win_w <= 800){
				$(".gdetails-summery .search-area").addClass("fixed-filter");
			}
		}
		else if(fromTop > 5){
			
			if(win_w <= 767){
				
				if($(".main-page").hasClass("hotels-page")){
					$(".search-area").addClass("fixed-filter");
				}
			}
			
		}else {
			$(".sidemenu-holder").removeClass("fixed-menu");
			$(".float-chat").removeClass("fixed-chat");
			$(".gdetails-summery .search-area").removeClass("fixed-filter");
			
			if($(".main-page").hasClass("hotels-page") || $(".main-page").hasClass("places-page")){
				$(".search-area").removeClass("fixed-filter");
			}
		}
		/* end fixed header */
		
		if($(".footer-section").isVisible()) {
			$(".chat-window").addClass('chat-with-footer');
			var chatH = $('.chat-conversation .chat-area').height();
			chatHU = chatH - 40;
			$('.chat-conversation .chat-area').height(chatHU);
		} else {
			if($('.chat-window').hasClass('chat-with-footer')){
				var chatH = $('.chat-conversation .chat-area').height();
				chatHU = chatH + 40;
				$('.chat-conversation .chat-area').height(chatHU);
			}
			$(".chat-window").removeClass('chat-with-footer');
			
		}
	});

/* end body scrolled */

/* document click */

	$(document).click(function(e) { 

		if(!$(e.target).closest('.btnbox').length) {
			if($('.btnbox').is(":visible")) {
				$('.btnbox').removeClass('box-open');
			}
		}		
		closeSearchSection(e);				
		$('.carousel-albums .carousel-inner').css("overflow","hidden");
	
		/* close resist dropdrown */
		var rtrgt = $('.dropdown.resist');
		var rotrgt = $('.dropdown.resist.open');	

		if ((!rtrgt.is(e.target) & rtrgt.has(e.target).length === 0) ||
			(!rotrgt.is(e.target) & rotrgt.has(e.target).length === 0) ||
			$(e.target).parents("li").hasClass("cancel_post")
		){
			$('.dropdown.resist').removeClass('open');
		}
		/* end close resist dropdrown */
		
		if (!$('.sliding-middle-out').is(e.target) & $('.sliding-middle-out').has(e.target).length === 0 & !$('.pa-comment').is(e.target))
			clearUnderline();

		var isSettings=$(".settings-page").length;
		var winw = $(window).width();
		if(isSettings>0 && winw < 1024){
			/* Settings Side Menu */
			if(e.target.className == 'mdi mdi-menu' || $(e.target).parents(".sidemenu-holder").length > 0) {
				
			   if(e.target.className == 'mdi mdi-menu') {
				   setMobileMenu('alt');			  
			   }else if($(e.target).parents("a").attr("class")=="closemenu" || $(e.target).parents("ul").hasClass('submenu')){
				   setMobileMenu('close');			
			   } else {
					setMobileMenu('open');			
			   }
			} 
			else {
				   setMobileMenu('close');			
			}
			/* End Settings Side Menu */
		}else{
			if(e.target.className == 'mdi mdi-menu' || e.target.className == ".sidemenu-holder") {

			   if(e.target.className == 'mdi mdi-menu') {
				   setMobileMenu('alt');			  
			   } else {
					setMobileMenu('open');			
			   }
			} else {			
				   setMobileMenu('close');			
			}
		}
		
		/* close emotion-box */
		
		/*var emotrgt = $('.emotion-box');
				
		if(e.target.className != 'emotion-box' && e.target.className != 'mdi mdi-emoticon' && emotrgt.has(e.target).length === 0){			
			$(".emotion-box").hide();
		}*/	
		
		/* end close emotion-box */
		
  });

/* end document click */ 

/* window resize */
	var resizeId;
	$(window).resize(function() {		
		clearTimeout(resizeId);
		resizeId = setTimeout(doneResizing, 300);
		fixPostImages();

		var winw=$(window).width();
		$headernav = $('.header-nav').width();
		$__s = parseInt(winw) - parseInt($headernav);
		$__s = parseInt($__s) / 2;
		$('.sticky-nav').find('.col').css('width', $headernav+'px');
		$('.sticky-nav').find('.col').css('margin-left', $__s+'px');
	});	
/* end window resize */

/* percentage page loader function */
	function runLoad(){		
		interval = setInterval(increment,100);
	}

	function increment(){	
		current++;
		if($('.hcontent-holder.banner-section').is(':visible') || current >= 35) { 
			hideLoader();
		}
	}
/* end percentage page loader function */

/************ END ESSENTIALS FUNCTIONS ************/



$(document).on('mouseover', '.justified-gallery .allow-gallery', function() {
	$(this).find('.removeicon').show();
});

$(document).on('mouseout', '.justified-gallery .allow-gallery', function() {
	$(this).find('.removeicon').hide();
});


$(document).on('mouseover', '.photobox', function() {
	$(this).find('.photosdelete').css('visibility', 'visible');
});

$(document).on('mouseout', '.photobox', function() {
	$(this).find('.photosdelete').css('visibility', 'hidden');
});