$(function() {
    // General function 
    var notReadMessageUserIds = [];
    idleTimer = null;
    idleState = false;
    idleWait = 50000; 
    (function ($) {  
        $(document).ready(function () {
            $('*').bind('mousemove keydown scroll', function () {  
                clearTimeout(idleTimer);
                if (idleState == true) {
                    socket.emit('setOnline', data2.id);
                }  
                 
                idleState = false;
                idleTimer = setTimeout(function () { 
                    socket.emit('setAway', data2.id);
                    idleState = true; 
                }, idleWait); 
            });
            $("body").trigger("mousemove");
        });
    }) (jQuery) 
 
    socket.on('setBasicInfoOfUser', function($data) { 
        $id = $data.id;
        $('textarea#inputMessageWall').attr('data-id', $id);
        
        $.ajax({  
            url: '?r=messages/calculatediff',
            type: 'POST',
            data: {$data},
            async: true,
            success: function(response)  {
                $('.topstuff .msgwindow-name').html(response);
                $time = '&nbsp;|&nbsp;'+getTime();
                $('.usertime').html($time);
                SINGLEFIREgetuserlabel();
            } 
        });
    }); 

    socket.on('getBasicInfoOfUserDJSUHSFS', function($data) { 
        $id = $data.id;
        $('textarea#inputMessageWall').attr('data-id', $id);
        $.ajax({  
            url: '?r=messages/calculatediffnew',
            type: 'POST',
            data: {$data},
            async: true,
            success: function(response)  {
                $('.mbl-innerhead').html(response);
            } 
        });
    });

    if(window.location.href.indexOf("messages") > -1) {
        socket.emit('callOnlineUsersids', data2.id);
    }

    socket.on('callOnlineUsersids', function($data) { 
        $getrecentmessageusersid = $data; 
    });

    socket.on('sendMessageToSelf', function($data) {
        if(!$.isEmptyObject($data)) {
            $id = $data.from_id;
            
            if($id == data2.id) {
                $id = $data.to_id;
            } 
            
            if($id != undefined) {
                $.ajax({  
                    url: '?r=messages/user-basic-info',
                    type: 'POST', 
                    data: {id: $id}, 
                    async: true,
                    success: function(response) {
                        var $result = $.parseJSON(response);
                        if($result.result != undefined && $result.result == true) {
                            var $fullname = $result.fullname;
                            var $thumb = $result.thumbnail
                            var $userstatus = $result.status;
                            var $isSameDate = $result.isSameDate;
                            var $isSameSender = $result.isSameSender;
                            var $difftime = $result.difftime;
                            var msgid = $data._id;
                            var $type = $data.type;
                            var $category = $data.category;
                            var $to_id = $data.to_id;
                            var $from_id = $data.from_id;
                            var $msg = $data.reply;
                            var $datetime = $data.created_at;
                            var $datetimedisplay = new Date($datetime);
                            var $time = getTimeWithAMPM($datetimedisplay);
                            var $getDate = $datetimedisplay.getDate();
                            var $getMonth = $datetimedisplay.getMonth();
                            var $getFullYear = $datetimedisplay.getFullYear();
                            var $currentDateTime = new Date();
                            var $preview='';
                            var $notificationMsg='';
                            var uniqID = msgid+'_'+$datetime;
                            
                            // check Today Label Exist.....
                            $this1 = $('.general-portion').find('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id);
                            var isTodayLabelExist = $this1.find('ul.outer').find('li.date-divider:last');
                            var label = '';
                            if( isTodayLabelExist.length ) {
                                var existlabel = isTodayLabelExist.find('span').html().trim();
                                if(existlabel == 'Today') {
                                    label = '';
                                } else {
                                    label = '<li class="date-divider"><span>Today</span></li>';
                                }
                            } else {
                                label = '<li class="date-divider"><span>Today</span></li>';
                            }

                            //check message page is open or not............
                            if (window.location.href.indexOf("messages") > -1) {
                                if($type == 'image') {
                                    $readIcon = '<br/>';
                                    if($from_id == data2.id) {
                                      $readIcon = '<br/><i class="mdi mdi-check-all msg-status"></i>';
                                    }

                                    if($isSameDate == true && $isSameSender == true) {
                                        $JISDK = '<div class="msg-handle">'+$readIcon+'<span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg27_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg27_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($JISDK);
                                    } else if($from_id == data2.id) {
                                        $JISDK = label+'<li class="msgli msg-outgoing time "> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle"> <i class="mdi mdi-check-all msg-status"></i> <span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"> <span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span> </a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" href="javascript:void(0)" onclick="sepmsgopt(this)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';
                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    } else {
                                        $JISDK = label+'<li class="msgli received msg-income"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox">  <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"><label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"><img src="'+$thumb+'"></div> <div class="descholder"><div class="msg-handle"><span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div></div> </div> </li>';
                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    }

                                    if($('.left-section').find('.users-display').find('.label-notice').length) {
                                        $('.left-section').find('.users-display').find('.label-notice').remove();
                                    }

                                    if($('.left-section').find('.users-display').find('li').find('a#msg_'+$to_id).length) {
                                        $leftSideBlock = '<h5>'+$fullname+'</h5> <p><i class="msg-status mdi mdi-check"></i><i class="zmdi zmdi-image"></i> You sent a photo</p> <span class="timestamp">'+$time+'</span>';
                                        $innerThis = $('.left-section').find('.users-display').find('li').find('a#msg_'+$to_id);
                                        $innerThis.parent('li').find('.descholder').html($leftSideBlock);
                                    } else {
                                        $li ='<li><a href="javascript:void(0)" class="active" id="msg_'+$to_id+'" onclick="openMessage(this);"><span class="muser-holder"> <span class="imgholder"><img src="'+$thumb+'"></span><span class="descholder"><h5>'+$fullname+'</h5> <p><i class="msg-status mdi mdi-check"></i><i class="zmdi zmdi-image"></i> You sent a photo</p> <span class="timestamp">'+$time+'</span></span></span></a></li>';
                                        $('.left-section').find('.users-display').prepend($li);
                                    }
                                } else if($type == 'gift') {
                                    $msgsplit = $msg.toString().split('|||||||');
                                    $code = '';
                                    $text = '';
                                    if($msgsplit[0] != undefined) {
                                        $code = $msgsplit[0];
                                    }

                                    if($msgsplit[1] != undefined && $msgsplit[1] != null) {
                                        $text = $.emoticons.replace($msgsplit[1]);
                                        $text ='<p><span class="message_block" data-id="'+msgid+'">'+$text+'</span><span class="timestamp">'+$time+'</span>';
                                    } else {
                                        $text = '<span class="message_block" data-id="'+msgid+'"></span>';
                                    }


                                    if($isSameDate == true && $isSameSender == true) {
                                      $readIcon = '';
                                      if($from_id == data2.id) {
                                        $readIcon = '<i class="mdi mdi-check-all msg-status"></i>';
                                      }

                                        $JISDK = '<div class="msg-handle">'+$text+' '+$readIcon+'<span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                       $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($JISDK);
                                    } else if($from_id == data2.id) {
                                        $JISDK = label+'<li class="msgli msg-outgoing"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle"> <i class="mdi mdi-check-all msg-status"></i>'+$text+'<span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';

                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    } else {
                                        $JISDK = label+'<li class="msgli received msg-income"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"> <img src="'+$thumb+'"> </div> <div class="descholder"> <div class="msg-handle">'+$text+'<span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';

                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    }

                                    if($('.left-section').find('.users-display').find('.label-notice').length) {
                                        $('.left-section').find('.users-display').find('.label-notice').remove();
                                    }

                                    if($('.left-section').find('.users-display').find('li').find('a#msg_'+$to_id).length) {
                                        $leftSideBlock = '<h5>'+$fullname+'</h5> <p><i class="msg-status mdi mdi-check"></i><i class="mdi mdi-gift"></i> You sent a gift</p> <span class="timestamp">'+$time+'</span>';
                                        $innerThis = $('.left-section').find('.users-display').find('li').find('a#msg_'+$to_id);
                                        $innerThis.parent('li').find('.descholder').html($leftSideBlock);
                                    } else {
                                        $li ='<li><a href="javascript:void(0)" class="active" id="msg_'+$to_id+'" onclick="openMessage(this);"><span class="muser-holder"> <span class="imgholder"><img src="'+$thumb+'"></span><span class="descholder"><h5>'+$fullname+'</h5> <p><i class="msg-status mdi mdi-check"></i><i class="mdi mdi-gift"></i> You sent a gift</p> <span class="timestamp">'+$time+'</span></span></span></a></li>';
                                        $('.left-section').find('.users-display').prepend($li);
                                    }
                                } else {
                                    if($msg != undefined && $msg != null) {
                                        $filterMsg = $.emoticons.replace($msg);
                                    } else {
                                        $filterMsg = '';
                                    }
                                    if($isSameDate == true && $isSameSender == true) {
                                      if($difftime <1) {
                                        $inner_I = '<br/><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span>'; 
                                        if($('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').find('.msg-handle:last').find('p').length) {
                                            $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').find('.msg-handle:last').find('p:last').append($inner_I)
                                        } else {
                                            $HIDSJDS = '<div class="msg-handle" data-time="'+$datetime+'"><p>'+$inner_I+'<span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg7_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg7_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                            $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($HIDSJDS);
                                        }
                                      } else {
                                        $HIDSJDS = '<div class="msg-handle" data-time="'+$datetime+'"><p><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg7_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg7_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($HIDSJDS);
                                      }
                                    } else if($from_id == data2.id) {
                                         $HIDSJDS = label+'<li class="msgli msg-outgoing time"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"><div class="msg-handle" data-time="'+$datetime+'"><p class="onw"><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div></div> </div> </li>';
                                         $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').append($HIDSJDS);
                                    } else {
                                        $HIDSJDS = label+'<li class="msgli received msg-income time"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"><img src="'+$thumb+'"></div> <div class="descholder"> <div class="msg-handle" data-time="'+$datetime+'"><p class="two"><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div></div> </div> </li>';
                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').append($HIDSJDS);  
                                    }

                                    if($('.left-section').find('.users-display').find('.label-notice').length) {
                                        $('.left-section').find('.users-display').find('.label-notice').remove();
                                    }
                                    
                                    if($('.left-section').find('.users-display').find('li').find('a#msg_'+$to_id).length) {
                                        $leftSideBlock = '<h5>'+$fullname+'</h5> <p><i class="msg-status mdi mdi-check"></i>'+$filterMsg+'</p> <span class="timestamp">'+$time+'</span>';
                                        $innerThis = $('.left-section').find('.users-display').find('li').find('a#msg_'+$to_id);
                                        $innerThis.parent('li').find('.descholder').html($leftSideBlock);
                                    } else {
                                        $li ='<li><a href="javascript:void(0)" class="active" id="msg_'+$to_id+'" onclick="openMessage(this);"><span class="muser-holder"> <span class="imgholder"><img src="'+$thumb+'"></span><span class="descholder"><h5>'+$fullname+'</h5><p><i class="msg-status mdi mdi-check"></i>'+$filterMsg+'</p><span class="timestamp">'+$time+'</span></span></span></a></li>';
                                        $('.left-section').find('.users-display').prepend($li);
                                    }
                                }
                                initDropdown();
                                scrolltobottomMSG();
                            } else {
                                if($type == 'image') {
                                    $readIcon = '<br/>';
                                    if($from_id == data2.id) {
                                      $readIcon = '<br/><i class="mdi mdi-check-all msg-status"></i>';
                                    }

                                    if($isSameDate == true && $isSameSender == true) {
                                        $JISDK = '<div class="msg-handle">'+$readIcon+'<span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg27_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg27_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                        $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($JISDK);
                                    } else if($from_id == data2.id) {
                                      $JISDK = label+'<li class="msgli msg-outgoing time "> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle"> <i class="mdi mdi-check-all msg-status"></i> <span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"> <img src="'+$msg+'"> </span></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';
                                      $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    } else {
                                      $JISDK = label+'<li class="msgli received msg-income"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox">  <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"><label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"><img src="'+$thumb+'"></div> <div class="descholder"><div class="msg-handle"><span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div></div> </div> </li>';
                                      $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    }
                                } else if($type == 'gift') {
                                    $msgsplit = $msg.toString().split('|||||||');
                                    $code = '';
                                    $text = '';
                                    if($msgsplit[0] != undefined) {
                                        $code = $msgsplit[0];
                                    }

                                    if($msgsplit[1] != undefined && $msgsplit[1] != null) {
                                        $text = $.emoticons.replace($msgsplit[1]);
                                        $text ='<p><span class="message_block" data-id="'+msgid+'">'+$text+'</span><span class="timestamp">'+$time+'</span>';
                                    } else {
                                        $text = '<span class="message_block" data-id="'+msgid+'"></span>';
                                    }


                                    if($isSameDate == true && $isSameSender == true) {
                                      $readIcon = '';
                                      if($from_id == data2.id) {
                                        $readIcon = '<i class="mdi mdi-check-all msg-status"></i>';
                                      }

                                        $JISDK = '<div class="msg-handle">'+$text+' '+$readIcon+'<span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                       $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($JISDK);
                                    } else if($from_id == data2.id) {
                                        $JISDK = label+'<li class="msgli msg-outgoing"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle"> <i class="mdi mdi-check-all msg-status"></i>'+$text+'<span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';

                                        $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    } else {
                                        $JISDK = label+'<li class="msgli received msg-income"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"> <img src="'+$thumb+'"> </div> <div class="descholder"> <div class="msg-handle">'+$text+'<span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';

                                        $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    }
                                } else {
                                    if($msg != undefined && $msg != null) {
                                        $filterMsg = $.emoticons.replace($msg);
                                    } else {
                                        $filterMsg = '';
                                    }

                                    if($isSameDate == true && $isSameSender == true) {
                                      if($difftime <1) {
                                        $inner_I = '<br/><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span>'; 
                                        if($('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').find('.msg-handle:last').find('p').length) {
                                            $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').find('.msg-handle:last').find('p:last').append($inner_I)
                                        } else {

                                            $JISDK = '<div class="msg-handle" data-time="'+$datetime+'"><p><span class="message_block" data-id="'+msgid+'">'+$inner_I+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg7_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg7_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                            $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($JISDK);
                                        }
                                      } else {
                                        $JISDK = '<div class="msg-handle" data-time="'+$datetime+'"><p><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg7_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg7_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                        $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($JISDK);
                                      }
                                    } else if($from_id == data2.id) {
                                         $JISDK = label+'<li class="msgli msg-outgoing time"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"><div class="msg-handle" data-time="'+$datetime+'"><p class="onw"><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div></div> </div> </li>';
                                         $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);
                                    } else {
                                        $JISDK = label+'<li class="msgli received msg-income time"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"><img src="'+$thumb+'"></div> <div class="descholder"> <div class="msg-handle" data-time="'+$datetime+'"><p class="two"><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div></div> </div> </li>';
                                        $('.chat-section').find('ul.mainul').find('li#li_msg_'+$to_id).find('ul.outer').append($JISDK);  
                                    }
                                }
                            }
                          
                            if($to_id) {
                                $.ajax({ 
                                    url: '?r=messages/get-info-for-single',
                                    type: 'POST',
                                    data: {id: $to_id},
                                    async: true,
                                    success: function(response) {
                                        var $response = $.parseJSON(response);
                                        if($response.length >0) {
                                            var $otherid = $response._id;
                                            var $otherthumb = $response.thumbnail;
                                            var $othername = $response.fullname;

                                            if($category == 'page')
                                            {
                                              var $entity_link = 'index.php?r=page/index&id='+$otherid;
                                              var $entity_click = '';
                                            }
                                            else
                                            {
                                              var $entity_link = 'javascript:void(0)';
                                              var $entity_click = 'openChatbox(this);';
                                            }

                                            $('.not-messages').find('.msg-listing').find('li.recid'+$otherid).remove();
                                            var $liBox = $('<li class="read mainli recid'+$otherid+'"> <div class="msg-holder"> <a href="'+$entity_link+'" onclick="'+$entity_click+'" data-id="chat_'+$otherid+'" id="'+$otherid+'"> <span class="img-holder"> <img class="img-responsive" src="'+$otherthumb+'"> </span> <span class="desc-holder"><span class="uname">'+$othername+'</span><span class="desc">'+$notificationMsg+'</span><span class="time-stamp">'+$time+'</span> </span></a>'+$preview+'<a class="readicon" onclick="markRead(this)" title="Mark as read" href="javascript:void(0)"></a><div class="clear"></div></li>'); 
                                            
                                           $('.not-messages').find('.msg-listing').prepend($liBox);
                                           fixImageUI('not-messages');
                                           initNiceScroll(".nice-scroll"); 
                                        }
                                    }
                                });     
                               }
                          
                            
                            scrollChatBottom();
                            scrollMessageBottom();
                            initGalleryImageSlider();                                   
                        }
                    }, 
                    complete: function() {                  
                        initDropdown();
                    }
                });     
            }
        }
    });

    socket.on('sendMessageToOther', function($result) {
        if(!($.isEmptyObject($result))) {
            $id = $result.from_id;
            if($id == data2.id) {
                $id = $result.to_id;
            }
            var is_display = true;           
            $.ajax({
                type: 'POST',
                url: '?r=messages/check-is-mute', 
                data: {id: $id},
                success: function(data) {
                    result = JSON.parse(data);                    
                    if(result.isMute != undefined && result.isMute == true) {
                        is_display = false;
                    }
                    var $data = JSON.parse(JSON.stringify($result));
                    var msgid = $data._id;
                    var $from_id = $data.from_id;
                    var $to_id = $data.to_id;
                    var $thumb = result.thumbnail;
                    var $fullname = result.fullname;
                    var $userstatus = result.status;
                    var $isSameDate = result.isSameDate;
                    var $isSameSender = result.isSameSender;
                    var $difftime = result.difftime;
                    var $unreadmsgcount = result.unreadmsgcount;
                    var $msg = $data.reply;
                    var $type = $data.type;
                    var $category = $data.category;
                    var $preview='';
                    var $notificationMsg='';
                    var $datetime = $data.created_at;
                    var $datetimedisplay = new Date($datetime);
                    var $time = getTimeWithAMPM($datetimedisplay);
                    var $currentDateTime = new Date();
                    var uniqID = msgid+'_'+$datetime;
                    var msgTone = msgTone;
                    if(is_display == false) {
                        isReadMessage($data.from_id);
                    }

                    //check message page is open or not............
                    if (window.location.href.indexOf("messages") > -1) {
                        // check right side tab is open or not.................
                        $this = $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id);
                        if($this.length) {
                            if($this.hasClass('active')) {
                                var isTodayLabelExist = $this.find('ul.outer').find('li.date-divider:last');
                                var label = '';
                                if( isTodayLabelExist.length ) {
                                    var existlabel = isTodayLabelExist.find('span').html().trim();
                                    if(existlabel == 'Today') {
                                        label = '';
                                    } else {
                                        label = '<li class="date-divider"><span>Today</span></li>';
                                    }
                                } else {
                                    label = '<li class="date-divider"><span>Today</span></li>';
                                }

                                if($type == 'image') {
                                    $readIcon = '<br/>';
                                    if($from_id == data2.id) {
                                      $readIcon = '<br/><i class="mdi mdi-check-all msg-status"></i>';
                                    }

                                    if($isSameDate == true && $isSameSender == true) {
                                        $jdsia = '<div class="msg-handle">'+$readIcon+'<span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg27_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg27_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                       $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($jdsia);
                                    } else if($from_id == data2.id) {
                                      $jdsia = label+'<li class="msgli msg-outgoing time "> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle"> <i class="mdi mdi-check-all msg-status"></i> <span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"> <span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span> </a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';
                                      $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').append($jdsia);
                                    } else {
                                      $jdsia = label+'<li class="msgli received msg-income"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox">  <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"><label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"><img src="'+$thumb+'"></div> <div class="descholder"><div class="msg-handle"><span class="msg-img"> <figure data-pswp-uid="1"> <a href="'+$msg+'" data-size="1600x1600" data-med="'+$msg+'" data-med-size="1024x1024" data-author="Folkert Gorter"><span class="message_block" data-id="'+msgid+'"><img src="'+$msg+'"></span></a> <a href="'+$msg+'" class="download" download><i class="mdi mdi-download"></i></a> <span class="timestamp">'+$time+'</span> </figure> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div></div> </div> </li>';
                                      $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').append($jdsia);
                                    }


                                } else if($type == 'gift') {
                                    $msgsplit = $msg.toString().split('|||||||');
                                    $code = '';
                                    $text = '';
                                    if($msgsplit[0] != undefined) {
                                        $code = $msgsplit[0];
                                    }

                                    if($msgsplit[1] != undefined && $msgsplit[1] != null) {
                                        $text = $.emoticons.replace($msgsplit[1]);
                                        $text = '<p><span class="message_block" data-id="'+msgid+'">'+$text+'</span><span class="timestamp">'+$time+'</span></p>';
                                    } else {
                                        $text = '<span class="message_block" data-id="'+msgid+'"></span>';
                                    }


                                    if($isSameDate == true && $isSameSender == true) {
                                      $readIcon = '';
                                      if($from_id == data2.id) {
                                        $readIcon = '<i class="mdi mdi-check-all msg-status"></i>';
                                      }

                                        $jdsia = '<div class="msg-handle">'+$text+' '+$readIcon+'<span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                       $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($jdsia);
                                    } else if($from_id == data2.id) {
                                        $jdsia = label+'<li class="msgli msg-outgoing"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"> <div class="msg-handle"> <i class="mdi mdi-check-all msg-status"></i>'+$text+' <span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';

                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').append($jdsia);
                                    } else {
                                        $jdsia = label+'<li class="msgli received msg-income"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"> <img src="'+$thumb+'"> </div> <div class="descholder"> <div class="msg-handle">'+$text+' <span class="gift-img"> <a href="javascript:void(0)" onclick="useGift(\''+$code+'\', \'popular\', \'preview\', \'chat\', this)"> <i class="mdi mdi-gift"></i> </a> </span> <span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div> </div> </div> </li>';

                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').append($jdsia);
                                    }
                                } else {
                                    if($msg != undefined && $msg != null) {
                                        $filterMsg = $.emoticons.replace($msg);
                                    } else {
                                        $filterMsg = '';
                                    }

                                    if($isSameDate == true && $isSameSender == true) {
                                      if($difftime <1) {
                                        $inner_I = '<br/><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span>'; 
                                        if($('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').find('.msg-handle:last').find('p').length) {
                                            $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').find('li.msgli:last').find('.descholder').find('.msg-handle:last').find('p:last').append($inner_I)
                                        } else {
                                            $jdsia = '<div class="msg-handle" data-time="'+$datetime+'"><p><span class="message_block" data-id="'+msgid+'">'+$inner_I+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg7_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg7_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                            $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($jdsia);
                                        }
                                      } else {
                                        $jdsia = '<div class="msg-handle" data-time="'+$datetime+'"><p><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="chatmsg7_setting"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="chatmsg7_setting" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div>';
                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').find('li.msgli:last').find('.descholder').append($jdsia);
                                      }
                                    } else if($from_id == data2.id) {
                                         $jdsia = label+'<li class="msgli msg-outgoing time"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="descholder"><div class="msg-handle" data-time="'+$datetime+'"><p class="onw"><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span> </div></div> </div> </li>';
                                         $('.right-section').find('ul.current-messages').find('li#li_msg_'+$to_id).find('ul.outer').append($jdsia);
                                    } else {
                                        $jdsia = label+'<li class="msgli received msg-income time"> <div class="checkbox-holder"> <div class="h-checkbox entertosend msg-checkbox"> <input type="checkbox" name="deleteselectedmsg" value="'+msgid+'"> <label>&nbsp;</label> </div> </div> <div class="msgdetail-box"> <div class="imgholder"><img src="'+$thumb+'"></div> <div class="descholder"> <div class="msg-handle" data-time="'+$datetime+'"><p class="two"><span class="message_block" data-id="'+msgid+'">'+$filterMsg+'</span><span class="timestamp">'+$time+'</span></p><span class="select_msg_checkbox"> <input type="checkbox" class="selectionchecked" data-msgtype="'+$type+'" id="'+uniqID+'" /> <label for="'+uniqID+'"></label> </span> <span class="settings-icon"> <a class="dropdown-button more_btn" onclick="sepmsgopt(this)" href="javascript:void(0)" data-activates="'+$datetime+'"> <i class="mdi mdi-dots-horizontal"></i> </a> <ul id="'+$datetime+'" class="dropdown-content custom_dropdown individiual_chat_setting" style="opacity: 1; left: -204px; position: absolute; top: 9px; display: none;"><div class="lds-css ng-scope"> <div style="width:100%;height:100%" class="lds-rolling-crntmsgpsndrp"> <div class="lststg"></div> </div> </div></ul> </span></div></div> </div> </li>';
                                        $('.right-section').find('ul.current-messages').find('li#li_msg_'+$from_id).find('ul.outer').append($jdsia);  
                                    }
                                }
                            }
                        }

                        if($type == 'image') {
                            $messagePoint = '<i class="msg-status mdi mdi-check"></i><i class="zmdi zmdi-image"></i> You received a photo';
                        } else if($type == 'gift') {
                            $messagePoint = '<i class="msg-status mdi mdi-check"></i><i class="mdi mdi-gift"></i> You received a gift';
                        } else {
                            if($msg != undefined && $msg != null) {
                                $messagePoint = $.emoticons.replace($msg);
                            } else {
                                $messagePoint = '';
                            }
                        }

                        // check lest side lable avaialbe if not then create new one..........
                        $this = $('.left-section').find('.tab-content').find('#messages-inbox');
                        if($this.hasClass('active')) {
                            $innerThis = $this.find('.users-display').find('li').find('a#msg_'+$from_id);
                            if($innerThis.length) {
                                if($innerThis.hasClass('active')) {
                                    $leftSideBlock = '<h5>'+$fullname+'</h5> <p>'+$messagePoint+'</p> <span class="timestamp">'+$time+'</span>';
                                    $innerThis.parent('li').find('.imgholder').addClass('dot_online');
                                    $innerThis.parent('li').find('.descholder').html($leftSideBlock);
                                } else {
                                    $unreadmsgcountBLOCK = '';
                                    if($unreadmsgcount > 0) {
                                        $unreadmsgcountBLOCK = '<span class="newmsg-count">'+$unreadmsgcount+'</span>';
                                    }

                                    $leftSideBlock = '<h5>'+$fullname+'</h5> <p>'+$messagePoint+'</p> <span class="timestamp">'+$time+'</span>'+$unreadmsgcountBLOCK;
                                    $innerThis.parents('li').addClass('unreadmsgli');
                                    $innerThis.parent('li').find('.imgholder').addClass('dot_online');
                                    $innerThis.parent('li').find('.descholder').html($leftSideBlock);
                                    if(msgTone == 'Yes') {
                                        msgToneActive();
                                    }
                                }   
                            } else {

                                if($this.find('.users-display').find('li:first').hasClass('label-notice')) {
                                    $leftSideBlock = '<li><a href="javascript:void(0)" id="msg_'+$from_id+'" onclick="openMessage(this)"> <span class="muser-holder"> <span class="imgholder dot_online"><img src='+$thumb+'></span> <span class="descholder"> <h5>'+$fullname+'</h5> <p>'+$messagePoint+'</p> <span class="timestamp">'+$time+'</span> <span class="newmsg-count"></span> </span> </span> </a> </li>';
                                    
                                    $this.find('.users-display').html('');
                                    $this.find('.users-display').append($leftSideBlock);
                                    $this.find('.users-display').find('li:first').find('a').trigger('click');
                                } else {
                                    $unreadmsgcountBLOCK = '';
                                    if($unreadmsgcount > 0) {
                                        $unreadmsgcountBLOCK = '<span class="newmsg-count">'+$unreadmsgcount+'</span>';
                                    }
                                    $leftSideBlock = '<li class="unreadmsgli"><a href="javascript:void(0)" id="msg_'+$from_id+'" onclick="openMessage(this)"> <span class="muser-holder"> <span class="imgholder dot_online"><img src='+$thumb+'></span> <span class="descholder"> <h5>'+$fullname+'</h5> <p>'+$messagePoint+'</p> <span class="timestamp">'+$time+'</span>'+$unreadmsgcountBLOCK+'</span> </span> </a> </li>';
                                    
                                    $this.find('.users-display').append($leftSideBlock);
                                }

                                if(msgTone == 'Yes') {
                                    msgToneActive();
                                }
                            }

                            // Move to top position......
                            $settle =  $this.find('.users-display');          
                            $moverLI = $settle.find('li').find('a#msg_'+$from_id).parent('li');
                            $settle.prepend($moverLI);
                        }
                    }

                    //initNiceScroll(".nice-scroll");
                    scrollChatBottom();
                    scrollMessageBottom();
                    initGalleryImageSlider();
                    fixImageUI('not-messages');
                }, 
                complete: function() {                  
                    initDropdown();
                }
            });            
        }
    });  

    // Chat Area functions

    socket.on('getSelfUserList', function(userList) {
        if(userList.length != 0) {
            $.ajax({
                url: '?r=messages/for-self',
                type: 'POST',
                data: {userList}, 
                async:false,
                success: function(data) {
                    var $users = JSON.parse(data);
                    $frdtab = $("#chat-connections").find('.nice-scroll');
                    $frdtab.html('');
                    
                    // Remove Loader.......
                    var sparent=$(".float-chat .data-loading");
                    removeLoaderImg(sparent);
                    setOpacity(sparent.find('.loading-holder'),1);             
                    
                    if($users.length === 0) {
                        $liBox = $("<ul class='chat-ul'><li class='label-notice'><div class='no-listcontent'>No record found.</div></li></ul>");
                        $frdtab.html($liBox);
                    } else {
                        $.each($users, function(k, item) {       
                            var $id = item.id;
                            var $fullname = item.fullname;
                            var $status = item.status;
                            var $thumbnail = item.thumbnail;
                            $liBox = $('<ul class="chat-ul chat-'+$status+' chatuserid_'+$id+'"><li class="chatuser-'+$id+'"><div class="chat-summery"><a href="javascript:void(0)" onclick="openChatbox(this);" data-id="chat_'+$id+'" id="'+$id+'" class="atag"><span class="img-holder"><img src="'+$thumbnail+'"></span><span class="desc-holder"><span class="uname">'+$fullname+'</span><span class="badge">3</span></span></a></div></li></ul>');
                            $frdtab.append($liBox);
                        });
                    }
                }
            });
        } else {
            // Remove Loaader....
            var sparent=$(".float-chat .data-loading");
            removeLoaderImg(sparent);
            setOpacity(sparent.find('.loading-holder'),1);      

            // Put Nor Record Found Label..........
            $frdtab = $("#chat-connections").find('.nice-scroll');
            $liBox = $("<ul class='chat-ul'><li class='label-notice'><div class='no-listcontent'>No record found.</div></li></ul>");
            $frdtab.html($liBox);
        }
    });

    socket.on('getAnotherUserList', function(userList) {
        if(!($.isEmptyObject(userList))) {
            $.ajax({
                url: '?r=messages/for-all',
                type: 'POST', 
                data: {userList},
                success: function(data) {
                    var user = JSON.parse(data);
                    if(user.length != 0) {
                        if (window.location.href.indexOf("messages") > -1) {
                            var id = user.id;
                            var status = user.status;

                            $selector = $('#messages-inbox').find('.users-display').find('li').find('a#msg_'+id);
                            if($selector.length) {
                                if($selector.hasClass('active')) {
                                    $('.topstuff').find('.msgwindow-name.curtusrbasicinfo').find('.imgholder').removeClass().addClass('imgholder dot_'+status);
                                    $('.topstuff').find('.msgwindow-name.curtusrbasicinfo').find('.person_status').html(status);
                                }
                                $selector.find('.imgholder').removeClass().addClass('imgholder dot_'+status);
                                generalCaseStudy();
                            }
                        } else  {
                            var whichActive = $(".chat-tabs").find(".tab-content").find('.fade.active');
                            if(whichActive.length) {
                                var activeId = whichActive.attr('id');
                                var id = user.id;
                                var status = user.status;
                                var cls = 'chat-'+status;
                                if(activeId == 'chat-connections' || activeId == 'chat-recent' || activeId == 'chat-search') {
                                    whichActive.find('ul.chatuserid_'+id).removeClass().addClass('chat-ul '+ cls +' chatuserid_'+id);                              
                                } else if(activeId == 'chat-online') {
                                    var current = whichActive.find('ul.chatuserid_'+id);
                                    if(current.length) {
                                        current.remove();
                                        if(!whichActive.find('ul.chat-ul').length) {
                                            $liBox = $("<ul class='chat-ul chat-label-notice'><li class='label-notice'><div class='no-listcontent'>No memeber is online</div></li></ul>");
                                            whichActive.find('.nice-scroll').html($liBox);
                                        }
                                    } else {
                                        var fullname = user.fullname;
                                        var thumbnail = user.thumbnail;
                                        $liBox = $('<ul class="chat-ul chat-online chatuserid_'+id+'" chat-ul"><li class="chatuser-'+id+'"><div class="chat-summery"><a href="javascript:void(0)" onclick="openChatbox(this);" data-id="chat_'+id+'" id="'+id+'" class="atag"><span class="img-holder"><img src="'+thumbnail+'"></span><span class="desc-holder"><span class="uname">'+fullname+'</span></span></a></div></li></ul>');
                                        if(whichActive.find('.nice-scroll').find('ul.chat-label-notice').length) {
                                            whichActive.find('.nice-scroll').find('ul.chat-label-notice').remove();
                                        }
                                        whichActive.find('.nice-scroll').append($liBox);
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }    
    });

    socket.on('getOnlineUsers', function(userList) {
        if(userList.length) {
            $.ajax({
                url: '?r=messages/for-self',
                type: 'POST',
                data: {userList},
                async:false,
                success: function(data) {
                    var users = JSON.parse(data);
                    $frdtab = $(".chat-tabs").find(".tab-content").find("#chat-online").find('.nice-scroll');
                    $frdtab.html('');
                    if(users.length === 0) {
                        $liBox = $("<ul class='chat-ul chat-label-notice'><li class='label-notice'><div class='no-listcontent'>No memeber is online</div></li></ul>");
                        $frdtab.html($liBox);
                    } else {
                        var sparent=$(".float-chat .data-loading");
                        removeLoaderImg(sparent);
                        setOpacity(sparent.find('.loading-holder'),1); 

                        $.each(users, function(k, item) {       
                            var id = item.id;
                            var fullname = item.fullname;
                            var status = item.status;
                            var thumbnail = item.thumbnail;
                            $liBox = $('<ul class="chat-ul chat-'+status+' chatuserid_'+id+'" chat-ul"><li class="chatuser-'+id+'"><div class="chat-summery"><a href="javascript:void(0)" onclick="openChatbox(this);" data-id="chat_'+id+'" id="'+id+'" class="atag"><span class="img-holder"><img src="'+thumbnail+'"></span><span class="desc-holder"><span class="uname">'+fullname+'</span></span></a></div></li></ul>');
                            $frdtab.append($liBox);
                        });
                    }
                }
            });
        } else {
            var sparent=$(".float-chat .data-loading");
            removeLoaderImg(sparent);
            setOpacity(sparent.find('.loading-holder'),1);    

            $frdtab = $(".chat-tabs").find(".tab-content").find("#chat-online").find('.nice-scroll');
            $liBox = $("<ul class='chat-ul chat-label-notice'><li class='label-notice'><div class='no-listcontent'>No memeber is online</div></li></ul>");
            $frdtab.html($liBox);
        } 
    });
    
    socket.on('getOnlineUsersquniq', function(userList,wall_user_id,baseUrl) {
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=userwall/connect-content',  //Server script to process data
                type: 'POST',
                data: {
                    id: wall_user_id,
                    baseUrl: baseUrl,
                    userList: userList
                },
                success: function(data)
                {
                    if(data == 'checkuserauthclassnv') {
                        checkuserauthclassnv();
                    } 
                    else if(data == 'checkuserauthclassg') {
                        checkuserauthclassg();
                    } 
                    else {
                        $("#connections-content").html(data);
                        setTimeout(function(){ initDropdown(); $('.tabs').tabs(); },400);
                    }
                        
                }
            });
        }
    });

    socket.on('chatboxsearchresult', function(result) {
        var result = JSON.parse(JSON.stringify(result));
        if(result) {
            var $selectorAll = $('.chat-tabs .tab-content').find('#chat-search');
            $selectorAll.html('');

            $selectorAll.append('<span class="ctitle">Recent</span><div class="nice-scroll recentchat-scroll"></div>');

            $.each(result, function(i, item) {
                var fullname = item.fullname;
                var id = item.id;
                var thumbnail = item.thumb;
                var status = item.status;

                $liBox = $('<ul class="chat-'+status+' chat-ul chatuserid_'+id+'"><li class="chatuser-'+id+'"> <div class="chat-summery"> <a href="javascript:void(0)" onclick="openChatbox(this);" data-id="chat_'+id+'" id="chat-'+id+'" class="atag"><span class="img-holder"><img src="'+thumbnail+'"></span> <span class="desc-holder"><span class="uname">'+fullname+'</span></span> </a> </div> </li> </ul>');
                $selectorAll.append($liBox);
            }); 
        }
    });
 
    socket.on('recentuserlistfillwithlabel', function(result, $isStop) {
        var $result = JSON.parse(JSON.stringify(result));
        var $category = 'index';

        $resultLength = $result.length;
        if($resultLength>0) {
            $stop_itration = parseInt($resultLength) - 1;
            $.each($result, function(i, v) {                              
                $id = v.user_id;
                $onlinehtml = '';
                if($.inArray($id, $getrecentmessageusersid) !== -1) {
                  $onlinehtml = '<span class="online-dot"></span>';
                }

                $thumb = v.thumb;
                $fullname = v.fullname;
                $country = v.country;
                $type = v.type;
                $message = v.reply;
                $from_id = v.from_id;
                $status = v.status;
                $to_id = v.to_id;
                $isMute = v.isMute;
                $isBlock = v.isBlock;
                $created_at =v.created_at.sec;
                $created_at = new Date(1000*$created_at);
                $time = getTimeWithAMPM($created_at);
                $sender = false;
                $unreadmsgcount = v.unreadmsgcount;
                $unreadmsgcountBLOCK = '';
                $blockicon_img = v.blockicon_img;
                $blockprofile_img = v.blockprofile_img;

                if($isMute == 'yes') {
                    $status = 'mute';
                }

                if($isBlock == 'yes') {
                    $status = 'block';
                }

                if($from_id == data2.id) { 
                    $sender = true; 
                }

                if($type == 'image') {
                    if($sender) { 
                        $box = '<i class="zmdi zmdi-image"></i> You sent a photo'; 
                    } else {
                        $box = '<i class="zmdi zmdi-image"></i> You received a photo'; 
                    }
                } else if($type == 'gift'){
                    if($sender) { 
                        $box = '<i class="mdi mdi-gift"></i> You sent a gift'; 
                    } else { 
                        $box = '<i class="mdi mdi-gift"></i> You received a gift'; 
                    }
                } else {
                    if($message != undefined && $message != null) {
                      $box = $.emoticons.replace($message);
                    } else {
                      $box = '';
                    }
                }

                var cls ='';
                
                if(i == 0 && $isStop == 'no') {
                    cls = 'active';
                    isReadMessage($id);
                    
                    var firstLi = $('.right-section ul.current-messages').find('li#li_msg_'+$id);
                    if(!firstLi.length) {
                        $tempSel = $('.right-section').find('ul.current-messages');
                        $li = '<li class="mainli active" id="li_msg_'+$id+'"> <div class="msgdetail-list nice-scroll" tabindex="18" style="opacity: 1; outline: none;"><div class="msglist-holder images-container"><ul class="outer"></ul></div></div></li>';
                        $($tempSel).append($li)
                    } else { firstLi.addClass('active'); } 

                    socket.emit('getBasicInfoOfUser', $id, data2.id);

                    //$('.right-section').find('.msgwindow-name').find('h4').html($fullname);
                    var requstdata = {
                        from_id: $from_id,
                        to_id: $to_id,
                        category: $category,
                        start: 0,
                        limit: 20
                    }
                      
                    callHistoryForMessageWall(requstdata);
                }


                if($unreadmsgcount > 0  && cls != 'active') {
                    $unreadmsgcountBLOCK = '<span class="newmsg-count">'+$unreadmsgcount+'</span>';
                }
                $li ='<li><a href="javascript:void(0)" class="'+cls+'" id="msg_'+$id+'" onclick="openMessage(this);"><span class="muser-holder"> <span class="imgholder dot_'+$status+'"><img src="'+$thumb+'" class="'+$blockprofile_img+'">'+$blockicon_img+'</span>'+$onlinehtml+'<span class="descholder"><h5>'+$fullname+'</h5><p><i class="msg-status mdi mdi-check"></i>'+$box+'</p><span class="timestamp">'+$time+'</span>'+$unreadmsgcountBLOCK+'</span></span></a></li>';
                $('.left-section').find('.users-display').append($li);

                if(i == $stop_itration) {
                    getSTli();
                }
            });  
        } else {
            getSTli();
        }

        var sparent=$(".messages-list .left-section #messages-"+$category);
        removeLoaderImg(sparent);
        setOpacity(sparent.find('.loading-holder'),1); 
    });

    socket.on('returnwithlabel', function(result) {
        var result = JSON.parse(JSON.stringify(result));
        if(result) {
            var $selector = $('#messages-inbox').find('.users-display');
            $selector.html('');
            $resultLength = result.length;
            if($resultLength>0) {
                $stop_itration = parseInt($resultLength) - 1;
                $.each(result, function(i, item) {
                    var fullname = item.fullname;
                    var id = item.id;
                    var thumbnail = item.thumb;
                    var status = item.status;
                    var cls = '';
                    if(i == 0) {
                        cls='active';
                    }

                    var $isMute = item.isMute;
                    var $isBlock = item.isBlock;
                    var $blockicon_img = item.blockicon_img;
                    var $blockprofile_img = item.blockprofile_img;
                    if($isMute == 'yes') {
                        status = 'mute';
                    }

                    if($isBlock == 'yes') {
                        status = 'block';
                    }

                    $liBox = $('<li><a href="javascript:void(0)" class="'+cls+'" id="msg_'+id+'" onclick="openMessage(this);"><span class="muser-holder"> <span class="imgholder dot_'+status+'"><img src="'+thumbnail+'" class="'+$blockprofile_img+'">'+$blockicon_img+'</span><span class="descholder"><h5>'+fullname+'</h5><span class="timestamp"></span><span class="newmsg-count"></span></span></span></a></li>');
                    $selector.append($liBox);

                    if(i == $stop_itration) {
                        getSTli();
                    }
                });

            } else  {
                getSTli();
            }

            var sparent=$(".messages-list .left-section #messages-inbox");
            removeLoaderImg(sparent);
            setOpacity(sparent.find('.loading-holder'),1); 
        }
    });

    socket.on('getChatUsersForRecentStatus', function(data) {
        var $result = JSON.parse(JSON.stringify(data));
        if($result) {
            $selectore = $('.chat-tabs').find('.tab-content').find('#chat-recent').find('.nice-scroll');
            if($selectore.length) {
                $selectore.html('');
                $.each($result, function(i, v) {
                    var $fullname = v.fullname,
                        $id = v.id,
                        $thumb = v.thumb,
                        $status = v.status,
                        $country = v.country;

                    $li = '<ul class="chat-ul chat-'+$status+' chatuserid_'+$id+'"><li class="chatuser-'+$id+'"><div class="chat-summery"><a href="javascript:void(0)" onclick="openChatbox(this);" data-id="chat_'+$id+'" id="'+$id+'" class="atag"><span class="img-holder"><img src="'+$thumb+'"></span><span class="desc-holder"><span class="uname">'+$fullname+'</span><span class="info">'+$country+'</span></span></a></div></li></ul>';
                    $selectore.append($li);
                });
            }
        }
    });

    socket.on('getuserlabel', function(result) {
        var result = JSON.parse(JSON.stringify(result));
        $.ajax({  
            url: '?r=messages/getuserlabel',
            type: 'POST',
            data: {result},
            async: true,
            success: function(response)  { 
                var $ISJ = $.parseJSON(response);
                $person_status = $ISJ.person_status;
                $time = '&nbsp;|&nbsp;'+getTime();
                $usertime = $ISJ.usertime;
                $imageclass = $ISJ.imageclass;
                $blockprofile_img = $ISJ.blockprofile_img;
                $blockicon_img = $ISJ.blockicon_img;
                
                $selectoreI = $('.topstuff .msgwindow-name');
                $selectoreI.find('.imgholder').removeClass().addClass($imageclass);
                $selectoreI.find('.imgholder').find('img').removeClass('blockprofile_img').addClass($blockprofile_img);
                $selectoreI.find('.imgholder').find('.blockicon_img').remove();
                $selectoreI.find('.imgholder').append($blockicon_img);
                $selectoreI.find('.person_status').html($person_status);
                $selectoreI.find('.usertime').html($time);

                $id = $('.right-section').find('.current-messages').find('li.mainli.active').attr('id');
                $id = $id.replace('li_msg_', '');

                $selectoreII = $('#messages-inbox').find('.users-display').find('li').find('a#msg_'+$id);
                if($selectoreII.length) {
                    $selectoreII.find('.imgholder').removeClass().addClass($imageclass);
                    $selectoreII.find('.imgholder').find('img').removeClass('blockprofile_img').addClass($blockprofile_img);
                    $selectoreII.find('.imgholder').find('.blockicon_img').remove();
                    $selectoreII.find('.imgholder').append($blockicon_img);
                }
            } 
        });
    });
});   

    
 


    
    