$(function() {
  var $input = $(".customsearchmain.input"),
    $clearBtn = $("button[data-search='clear']"),
    $prevBtn = $("button[data-search='prev']"),
    $nextBtn = $("button[data-search='next']"),
    $context = $('.current-messages').find('.mainli.active'),
    $results,
    currentClass = "current",
    animateClass = "animate",
    offsetTop = 50,
    currentIndex = 0;

  function jumpTo() {
    if ($results.length) {
      var position, $current = $results.eq(currentIndex);
      $results.removeClass(currentClass);
      if ($current.length) {
        $current.addClass(currentClass);
        position = $current.offset().top - offsetTop;
        window.scrollTo(0, position);
      }
    }
  }
    
  $input.on("input", function() {
    var searchVal = this.value;
    $context.unmark({
      done: function() {
        $context.mark(searchVal, {
          separateWordSearch: true,
          separateWordSearch : true,
          diacritics : false,
          debug : false,
          done: function() {
            $results = $context.find("mark");
            currentIndex = 0;
            jumpTo();
          }
        });
      }
    });
  });

  $clearBtn.on("click", function() {
    $context.unmark();
    $input.val("").focus();
  });

  $input.on("keypress", function(e) {
      if(e.which == 13) {
        if ($results.length) {
          currentIndex += -1;
          if (currentIndex < 0) {
            currentIndex = $results.length - 1;
          }
          if (currentIndex > $results.length - 1) {
            currentIndex = 0;
          }
          jumpTo();
        }    
      }
  });

  $nextBtn.add($prevBtn).on("click", function() {
    if ($results.length) {
      currentIndex += $(this).is($prevBtn) ? -1 : 1;
      if (currentIndex < 0) {
        currentIndex = $results.length - 1;
      }
      if (currentIndex > $results.length - 1) {
        currentIndex = 0;
      }
      jumpTo();
    }
  });
});