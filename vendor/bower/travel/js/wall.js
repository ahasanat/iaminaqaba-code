 	$(document).ready(function() {
		/************ WALL FUNCTIONS ************/
			$(document).on('click', '.wall-info span.directeditmode', function() {
				$('.aboutclk a').click();

				setTimeout(function(){
					$('.editicon').click();
					//$("HTML, BODY").animate({ scrollTop: 690 }, 500)
				}, 400);
			});

			$('#image-cropper.mian-crop').cropit({
		        height: 300,
		        width: 1005,
		        smallImage: "allow",
		        onImageLoaded: function (x) {           
		            $(".cropit-preview").removeClass("class-hide");
		            $(".cropit-preview").addClass("class-show");
		            $(".btn-save").removeClass("class-hide");
		            $(".btn-save").addClass("class-show");
		            $("#removeimg").removeClass("class-hide");
		            $("#removeimg").addClass("class-show");
		        }
		    });
		    
			/* popup tag change - change cover/photo */
			$("body").on("click", ".pc-section .nav li", pcSectionTabChange);				
			/* end popup tag change - change cover/photo */		
						
			var isWall=$(".page-wrapper").hasClass("wallpage");                        
			if(isWall){	

				$('#albumCarousel').carousel({
					interval: false //changes the speed
				});
				
				/* album slider */
					initPhotoCarousel("wallpage");
				/* end album slider */                                               
			}
			
			/* reset photos - photos page : second tab */
			$("body").on("click", ".photos-content .album-tab", resetAlbumsTab);
			/* end reset photos - photos page : second tab */
			
			/* open saved post */
			$("body").on("click", ".saved-post .s_postlink", openSavedPost);		
			/* end open saved post */

			/* album carousel initalization */
			$("body").on("click", ".carousel-albums .descholder .dropdown-toggle",function(){			
				$('.carousel-albums .carousel-inner').css("overflow","visible");
			});
			/* emd album carousel initalization */
			
			/* open activity post */
			fixImageUI("activity");
			$("body").on("click", ".activity-holder .a_postlink", openActivityPost);		
			/* end open activity post */
			
			/* swich wall main tabs */	
			$(".link-menu li a").click(function(){
				if($(this).hasClass("activity-link")){
					if($(".page-wrapper").hasClass("wallpage")){				
						if($("#about-content").find(".mode-holder").hasClass("opened")){$("#about-content").find(".mode-holder").removeClass("opened");}
						$("#about-content").removeClass("active in");
					}
					resetTabs(".custom-wall-links");
					resetTabs(".sub-tabs");
					
					$(".fixed-layout").addClass("hide-addflow");
					  
					setTimeout(function(){
						fixImageUI("activity");
					},400);
					
					var sparent = $(".main-content");
					if(sparent.hasClass("gone")){
						sparent.removeClass("gone");
					}
					setTimeout(function(){				
						resetInnerPage("wall","hide"); // hide wall menu in mobile
					},600);
				}
			});

			$(".custom-wall-links li a").click(function(){	 
				var initScroll = 190;
				var win_w = $(window).width();
				if(win_w < 767) {
					initScroll = 0;
				}
				$('html, body').animate({ scrollTop: initScroll }, 'slow');

				wallTabClicked(this,"custom-walllinks");
				var tabname = $(this).attr("tabname");

				if($('.adslistingblock').length) {
					if(tabname == 'Wall') {
						$('.adslistingblock').show();
					} else {
						$('.adslistingblock').hide();
					}
				}
		
				resetTabs(".link-menu");
			});
			/* end swich wall main tabs */ 
			                                                                             
			/* swich wall sub tabs */	
			$(".sub-tabs li a").click(function(){	
				var initScroll = 190;
				var win_w = $(window).width();
				if(win_w < 767) {
					initScroll = 0;
				}
				$('html, body').animate({ scrollTop: initScroll }, 'slow');
					
				resetInnerPage("wall","hide");
				resetTabs(".link-menu");
				wallTabClicked(this,"subtabs");
			});
			/* end swich wall sub tabs */
			
			/* default banner selected */	
			$(".carousel_master#carousel li").each(function(){
				
				$(this).on("click",function(){
					var name = $(this).find("img").attr("src");
					var covername = name.split("/").splice(-1);
					                                         
					$.ajax({  
					   type: 'POST',
					   url: '?r=site/user-cover',
					   data: 'covername='+covername,
					   success: function(data){
							if(data == '1')
							{
								var name1 = name.replace("-thumb", '');
								var name2 = name1.replace("thumbs/", '');
								closeDefaultSlider();
								$(".wall-banner").css("background","url("+name2+")");
								$(".wall-banner").css("background-repeat","no-repeat");
								$(".wall-banner").css("background-position","top center");
							}
					   }
				   });

					
				});		
			});
			/* end default banner selected */
				
			/* referral */
			$("body").on("click", ".fb_positive", function(){
				$(this).siblings( '#ref_point' ).val("Positive");
			})
			
			$("body").on("click", ".fb_neutral", function(){
				$(this).siblings( '#ref_point' ).val("Neutral");
			})
			
			$("body").on("click", ".fb_negative", function(){
				$(this).siblings( '#ref_point' ).val("Negative");
			})	
			/* end referral */
			
			/* album security */
			$("body").on("click", ".album-private", function(){
				var albumid = $(this).attr('data-album');           
				var albumvalue = 'Private';
				setAlbumPrivacy(albumid, albumvalue, this);
			})		
			$("body").on("click", ".album-connections", function(){
				var albumid = $(this).attr('data-album');
				var albumvalue = 'Connections';
				setAlbumPrivacy(albumid, albumvalue, this);
			})		                                                     
			$("body").on("click", ".album-public", function(){                     
				var albumid = $(this).attr('data-album');
				var albumvalue = 'Public';
				setAlbumPrivacy(albumid, albumvalue, this);
			})
			/* end album security */
			
			/* Connect Search From Userwall*/
			$( document ).on( 'keyup', '#connect_search', function() {
				setTimeout(function(){ 
					var key = $('#connect_search').val();
					$.ajax({
						url: "?r=site/search-connections", 
						type: 'POST', 
						data: {key},
						success: function (data) {
							$(".connect_search").html(data).show();
						}, 
						complete: function () {
							$('#connect_search').focus();
						}
					});
				},400);
			});
			/* End Connect Search From Userwall*/
									 
			/* set dropdown for icondrop */
			$("body").on("click", ".custom-icondrop .dropdown-menu li a", setIcondropValue,update_connect_list_setting);
			/* end set dropdown for icondrop */
			 
		/************ END WALL FUNCTIONS ************/
	});
/* end document ready */

/************ START Design Code ************/
/* FUN initalize photo carousel */ 
	function resetPhotoCarousel(parentClass){
		$(parentClass+" .item div[class*='cloneditem-']").remove();		
	}

	function initPhotoCarousel(which){
		
		var parentClass="";
		if(which == "wallpage"){
			parentClass=".photos-content .carousel-albums";			
		}
		if(which == "topattractions"){
			parentClass=".topattractions-section .carousel-albums";			
		}
		if(which == "topplaces"){
			parentClass=".topplaces-section .carousel-albums";			
		}
		
		resetPhotoCarousel(parentClass);
		$(parentClass).carousel({interval:false});
		
		var win_w=$(window).width();
		
		var ci_count = $(parentClass+" .item").length;
		var ci_limit=3;
		
		if(win_w>=600){
			ci_limit=3;		
		}else if(win_w>=450 && win_w<600){
			ci_limit=2;
		}else{
			ci_limit=1;
		}
			 
	   if(ci_count >ci_limit ){
		   
		   if($(parentClass).hasClass("no-carousel"))
				$(parentClass).removeClass("no-carousel");
			
		   $(parentClass+" .carousel-control").show();  

		   $(parentClass+' .item').each(function(){
		   
				var itemToClone = $(this);

				for (var i=1;i<ci_limit;i++) {
				  itemToClone = itemToClone.next();

				  // wrap around if at end of item collection
				  if (!itemToClone.length) {
				 itemToClone = $(this).siblings(':first');
				  }

				  // grab item, clone, add marker class, add to collection
				  itemToClone.children(':first-child').clone()
				 .addClass("cloneditem-"+(i))
				 .appendTo($(this));
				}
		   });
	   }
	   else{
		$(parentClass).addClass("no-carousel");
		$(parentClass+" .carousel-control").hide();
	   }
	}
/* FUN end initalize photo carousel */

/* FUN reset tabs */
	function resetWallTabs(linktext,toWhich){
		var sparent;
		
		if(toWhich=="custom-walllinks"){
			sparent=".custom-wall-links";
		} else {
			sparent=".sub-tabs";			
			if($('#settings-content').length) {
				$('#settings-content').removeClass('active in');			
				$('#settings-content').hide();		
			}	
		}

		//setTimeout(function(){
			$(sparent+" ul li").find('a').removeClass('active');		
			$(sparent+" ul li").removeClass('active');		

			if($(sparent+" ul li").find("a[href$='"+linktext+"']").length) {
				$(sparent+" ul li").find("a[href$='"+linktext+"']").addClass('active');
			}		
		//}, 1000); 
	} 
	function wallTabClicked(obj,fromWhere){
		var ahref= $(obj).attr("href");
		var tabname = $(obj).attr("tabname");
		
		if(tabname!="Wall" && tabname!="Post"){
			var sparent = $(".main-content");
			if(sparent.hasClass("gone")){
				sparent.removeClass("gone");
			}
			resetInnerPage("wall","hide");
			setTimeout(function(){	
				resetInnerPage("wall","hide");
			},400);
		}

		if(fromWhere=="subtabs"){
			resetWallTabs(ahref,"custom-walllinks");
		} else {
			resetWallTabs(ahref,"subtabs");			
		}   

		if(tabname!="Wall" && tabname!="Refers" && tabname!="References" && tabname!="Reviews"){		
			//closeAboutSection();
			manageNewPostBubble("hideit");			
		}else{
			
			if($(obj).hasClass("disabled"))
				manageNewPostBubble("hideit");
			else
				manageNewPostBubble("showit");
		}
		
		$('.user-photos').removeClass('photoshow');
						
		if(tabname=="Contribution"){
			setTimeout(function(){
				$('.gauge2').gauge({
					values: {
						0 : '0',
						20: '25 - 75',
						100: '100'
					},                                             
					colors: {
						0 : '#ED1C24',
						20 : '#39B54A',
						80: '#FDD914'
					},                 
					angles: [  
						180,
						360
					],
					lineWidth: 20,
					arrowWidth: 10,
					arrowColor: '#ccc',
					inset:true,

					value: 65
				});
			},400);
		}
		
		$("body").getNiceScroll().resize();	                            	
		
		setTimeout(function(){fixImageUI();},400);
		
		
		var initScroll = 190;
		var win_w = $(window).width();
		
		if(win_w < 767)
			initScroll = 0;
		
		
		$('html, body').animate({ scrollTop: initScroll }, 'slow');
	}	
/* FUN end reset tabs */

/* FUN change cover/photo */
	function resetAllPopupSection(fromwhere){               
		if(fromwhere=="cover"){
			$(".popup-area#change-cover .pc-section").hide();
			$(".popup-area#change-cover .pc-title").hide();
		}
		else{
			$(".popup-area#change-profile .pc-section").hide();
			$(".popup-area#change-profile .pc-title").hide();
		}
	}
	function changePopup(fromwhere, which){	
		resetAllPopupSection(fromwhere);
		$(".popup-area#change-profile").removeClass("uploadphoto-popup");
		if(fromwhere=="cover"){			
			$(".popup-area#change-cover ."+which).show();

			if(which=="choosephoto"){
				setTimeout(function(){fixImageUIPopup();},400);
			}
		}
		else{
			$(".popup-area#change-profile ."+which).show();
			if(which=="uploadphoto"){
				$(".popup-area#change-profile").addClass("uploadphoto-popup");
			}
		}
	}
	function openDefaultSlider() {
        $(".cover-slider").addClass("openDrawer");
        $('.carousel').carousel();
		/*$.ajax({
            type: 'GET',
            url: '?r=userwall/getsliders',
            success: function (data) {
            	setTimeout(function(){
            		$('#carousel').carousel();
            		$('#carousel').carousel({
					    indicators: true
					  });
            	}, 400);
            }
        });*/
	}
	
	function closeDefaultSlider(){
		$(".cover-slider").removeClass("openDrawer");
	}
/* FUN end change cover/photo */    

/* FUN imagefix class - manage in popup tab */
	function pcSectionTabChange(){
		
		if(!$(this).hasClass("visited")){			
			setTimeout(function(){
				fixImageUIPopup();
				var tabpan_id=($(this).find("a").attr("href")).replace("#","");			
				alert(tabpan_id);
				$(".pic-change-popup").find("#"+tabpan_id).addClass("visited");
			},100);
			$(this).addClass("visited");
		}
	}
/* FUN end imagefix class - manage in popup tab */
 
/* FUN open album images */
	function openAlbumImagesTab(obj,album_id,album_name='') { 
		if (album_id != '') {
            $.ajax({
                type: 'POST',
                url: '?r=userwall/viewalbumpics',
                data: "post_id="+album_id,
                success: function (data) {
					if($('#ordinaorytabli').length) { 
						$('#ordinaorytabli').remove();
					}

					if($('#ordinaorytablicontent').length) {
						$('#ordinaorytablicontent').remove();
					}
					
					$lidesign = '<li class="tab" id="ordinaorytabli"><a href="#ordinaorytablicontent"><span id="ordinaorytablititle">'+album_name+' </span><span class="photos_count" id="ordinaorytablicount">()</span></a></li>';
					$('#photos-content').find('.fake-title-area').find('.tabs').find('li:eq(0)').after($lidesign);
					$('#photos-content').find('.tab-content').append('<div class="tab-pane in active" id="ordinaorytablicontent" data-albumid="'+album_id+'"><div class="photos-area"></div></div>');		
					$("#ordinaorytablicontent .photos-area").html(data);
					$('ul#photosareatab').tabs('select_tab', 'ordinaorytablicontent');
					$totalphotos = $("#ordinaorytablicontent").find(".images-container").find('.grid-box').length;
					$("#ordinaorytablicount").html('('+$totalphotos+')');
					initGalleryImageSlider();
					initDropdown(); 

					justifiedGalleryinitialize();
					lightGalleryinitialize();
                }
            });
		}
	}

	function openAlbumImages(obj,album_id,album_name){
		var super_parent=$(obj).parents(".tab-pane");
		var tabpane=$(obj).parents(".tab-pane").attr("id");

		if(tabpane=="pc-albums"){
			if (album_id != '') {
			$(".add_image_id").val(album_id);
			$(".album_title_name").html(album_name);
            $.ajax({
                type: 'POST',
                url: '?r=userwall/viewalbumpics',
                data: "post_id="+album_id,
                success: function (data) {
					$("#"+tabpane+" .photos-area").html(data);
					initGalleryImageSlider();
                }
            });
			}
			$("#"+tabpane).find(".photos-area").show();
			$("#"+tabpane).find(".albums-area").hide();
		}
		if(tabpane=="pc-all"){
			if (album_id != '') {
			$(".add_image_id").val(album_id);
			$(".album_title_name").html(album_name);
            $.ajax({
                type: 'POST',
                url: '?r=userwall/viewalbumpics',
                data: "post_id="+album_id,
                success: function (data) {
					$("#"+tabpane+" .photos-area").html(data);
					initGalleryImageSlider();
					
                }
            });
			}
			$("#"+tabpane).find(".photos-area").show();
		}
		
	}
/* FUN end open album images */

/* FUN hide all saved post detail */
	function hideAllPostDetails(){
		$(".saved-post .saved-post-detail").each(function(){			
			$(this).hide();
		});
	}
/* FUN end hide all saved post detail */

/* FUN open saved post */
	function openSavedPost(){
	
		var super_parent=$(this).parents(".main-li");
		if(super_parent.find(".saved-post-detail").css("display")=="none"){
			hideAllPostDetails();
			super_parent.find(".saved-post-detail").slideDown();
			setTimeout(function(){			
				fixImageUI("openSavedPost");
			},300);
		}
		else{
			super_parent.find(".saved-post-detail").slideUp();			
		}
	
	}
/* FUN end open saved post */

/* FUN reset albums tab */
	function resetAlbumsTab(){
		$("#pc-albums").find(".photos-area").hide();
		$("#pc-albums").find(".photos-area").html();
		$("#pc-albums").find(".albums-area").show();
	}
/* FUN end reset albums tab */

/* manage wall tab scroll */
	function manageWallSubTabScroll(){
			
		var win_w=$(window).width();		
		
		if(win_w > 650){
			$(".tab-scroll").getNiceScroll().remove();
			$(".tab-scroll").css({"width":"auto","overflow":"initial"});
		}
		else{
			//$(".tab-scroll").getNiceScroll().resize();
			$(".tab-scroll").niceScroll();
		}
	}
/* end manage wall tab scroll */

/* FUN open upload cover popup */
	function openCoverUpload(dataUrl){
		var croppicContainerPreloadOptions;
		croppicContainerPreloadOptions = {
			cropUrl:'?r=site/cover-image-crop',
			loadPicture: dataUrl,
			enableMousescroll:true,
			onAfterImgCrop: function(){ 
				location.reload();
			}
		}
		return false;
	}	
/* FUN end open upload cover popup */

/* FUN set wall stuff edit mode textarea */
	function setWallEditTextarea(obj){
		
		var nearestMainParent=$(obj).parents(".swapping-parent");
		
		var tt=nearestMainParent.find(".edit-mode").find("textarea");
		//setAutoGrowTextarea(tt);
		setTimeout(function(){
			setGeneralThings();
		},600);
	}
/* FUN end set wall stuff edit mode textarea */

/* FUN open about section of wall */
	function openAboutSection(state){
		
		if($(".page-wrapper").hasClass("wallpage")){
			if($(".page-wrapper").hasClass("businesspage")){
				closePageSettings();
			}
			var findli = $(".sub-tabs .tabs li a[href='#about-content']");		
			findli.trigger("click");
			if(state=="normal"){
				
				if($("#about-content").find(".mode-holder").hasClass("opened")){
					$("#about-content").find(".mode-holder").removeClass("opened");
				}
			}else{
				$("#about-content").find(".mode-holder").addClass("opened");			
			}
		}
	}
	function openWallTabInternally(which){
		if($(".page-wrapper").hasClass("wallpage")){
			if($(".page-wrapper").hasClass("businesspage")){
				closePageSettings();
			}
			var findli = $(".sub-tabs .tabs li a[href='#"+which+"']");		
			setTimeout(function(){			
				//findli.trigger("click");
				findli.click();
			}, 800);
		}
	}
/* FUN end open about section of wall */

/* FUN open send message drawer - wall page */
	function showMsg(obj){
		
		var win_w = $(window).width();
		if(win_w >= 768){
			var sparent = $(obj).parents(".wall-header");
			var msgObj = sparent.find('.sendMessage');
			if(msgObj.hasClass('open')){
				msgObj.hide();
				msgObj.removeClass('open');
			}
			else{
				msgObj.show();
				msgObj.addClass('open');
			}
		}else{
			activemessageblock();	
			/*var msgObj = $("#newmsg-popup").find('.sendMessage');
			msgObj.show();*/
		}
	}
/* FUN end open send message drawer - wall page */


/* FUN wall tab dropdown */
	function wallTabDropdown(name, evt) {
		if(evt.params.data.id=="gallery")		
			$('.desk-menu a[href="#gallery-content"]').trigger('click');
		
		if(evt.params.data.id=="saved")		
			$('.desk-menu a[href="#saved-content"]').trigger('click');
		
		if(evt.params.data.id=="contribution")		
			$('.desk-menu a[href="#contribution-content"]').trigger('click');
		
		if(evt.params.data.id=="wall")		
			$('.desk-menu a[href="#wall-content"]').trigger('click');
	}
/* FUN end wall tab dropdown */

/* wall refers functions */
	function replyEditMode(obj){
		
		var sparent=$(obj).parents(".feedback-reply");
		
		sparent.find(".normal-mode").hide();
		sparent.find(".edit-mode").show();
	}
	function replyNormalMode(obj){
		
		var sparent=$(obj).parents(".feedback-reply");
		
		sparent.find(".normal-mode").show();
		sparent.find(".edit-mode").hide();
	}
	/*function replyDelete(obj){
		
		var mparent=$(obj).parents(".refer-post");
		var sparent=$(obj).parents(".feedback-reply");
		
		if(mparent.hasClass("replied")){
			mparent.removeClass("replied");
			sparent.remove();
		}
	}*/
/* end wall refers functions */

/* FUN open activity post */
	function openActivityPost(){		
		var super_parent=$(this).parents(".main-li");
		if(super_parent.find(".activity-post-detail").css("display")=="none"){
			hideAllPostDetails();
			super_parent.find(".activity-post-detail").slideDown();
			setTimeout(function(){			
				fixImageUI("openActivityPost");
			},300);
		}
		else{
			super_parent.find(".activity-post-detail").slideUp();			
		}
	
	}
/* FUN end open activity post */

/* manage resizable holder */
	function openResizable(obj){
		var holder=$(obj).parents(".resizable-holder");
		holder.addClass("expanded");
	}
/* end manage resizable holder */

/* destinations listing */
	function destListing(){
		$("#destigrid").css("visibility","hidden");
		setTimeout(function(){
			$("#destigrid").css("visibility","visible");$(".gloader").hide();
			initNiceScroll(".nice-scroll");
		},700);			
	}
/* end destinations listing */

/* destinations listing */
	function box(){
		$("#box").css("visibility","hidden");
		setTimeout(function(){
			$("#box").css("visibility","visible");$(".gloader").hide();
			initNiceScroll(".nice-scroll");
		},700);			
	}
/* end destinations listing */

/* wall page - open settings tabs in mobile */
	function backToMain(fromWhere){
		
		if(fromWhere == "businesspage"){
			var contentID = $(".main-content .main-tabpane.active").attr("id");
			
			if(contentID == "messages-content"){
				closeAddNewMsg(); // back to messages screen
			}
			if(contentID == "settings-content"){
				getBack(); // back to settings screen
			}
		}
		
	}
	function getBack(){		
		$(".settings-content .open-innerpage").find(".tabs-detail").addClass("gone");		
		
		$(".header-themebar .mbl-innerhead").hide();
		$(".header-themebar .mbl-innerhead").find('.page-name').html("");		
	}
/* end wall page - open settings tabs in mobile */

/* add album */
	function addAlbum(){
		applypostloader('SHOW');
		var status = $("#user_status").val();
		if(status == 0){
			applypostloader('HIDE');
			return false;
		}
		
		var fileselectore = $('#addAlbumContentPopup').find('.upload');
		var album_title = $('#addAlbumContentPopup').find('#album_title').val();
		if(album_title == ''){
			$('#album_title').focus();
			Materialize.toast('Add title to the album.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}
		if(storedFiles.length <= 0){
			Materialize.toast('Add photos to the album.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}
		else
		{
			$('input[name="imageFile1"]').val('');
			var formdata;
			formdata = new FormData($('form')[1]);
			for(var i=0, len=storedFiles.length; i<len; i++)
			{
				formdata.append('imageFile1[]', storedFiles[i]); 
			}
			var album_title = $("#album_title").val();
			var album_description = $("#album_description").val();
			var album_place = $("#album_place").val();
			formdata.append('album_title', album_title);
			formdata.append('album_description', album_description);
			formdata.append('album_place', album_place);
			managePostButton('HIDE');
			$.ajax({ 
				url: '?r=userwall/photos',
				type: 'POST',
				data:formdata,
				processData: false,
				contentType: false,
				success: function(data) {
					//applypostloader('HIDE');
					if(data == 'isreview') {
						Materialize.toast('Post in review.', 2000, 'green');
					} 

					$('#addAlbumContentPopup').modal('close');
					$('#photos-content').hide(); 
					photosContent();
					$('#addAlbumContentPopup').find('form')[0].reset();
					//$(".add_image_id").val(data);
					//$('.single').append($("<option></option>").attr("value",data).text(album_title));
					$(".album_title_name").html(album_title);
					lastModified = [];
					storedFiles = [];
					storedFiles.length = 0;
					managePostButton('SHOW');
					setTimeout(function(){ 
						initDropdown(); 
						$('.tabs').tabs(); 
						$('#photos-content ul.tabs').tabs('select_tab', 'pc-albums');
						$('#photos-content').show(); 
					},400);
				}
			});
		}
	}
/* end add album */

/* edit album */
	function editAlbum(album_id){
		if (album_id != '') {
			$.ajax({
				type: 'POST',
				url: '?r=userwall/editalbum',
				data: "album_id="+album_id,
				success: function (data) {
					$("#edit-album-popup").html(data);
					$("#edit-album-popup").modal('open');
					$(".sliding-middle-out").click(function(){			
						titleUnderline(this);			
					});
				}
			});
		}
	}
/* end edit album */

/* edit album details */
	function editAlbumDetails(album_id){
		if (album_id != '') {
			var edit_title = $(".edit_title").val();
			var edit_desc = $(".edit_desc").val();
			var edit_place = $(".edit_place").val();
			$.ajax({
				type: 'POST',
				url: '?r=userwall/editalbumdetails',
				data: "album_id="+album_id+"&edit_title="+edit_title+"&edit_desc="+edit_desc+"&edit_place="+edit_place,
				success: function (data) {
					$('#edit-album-popup').modal('close');
					Materialize.toast('Saved', 2000, 'green');
					var result = $.parseJSON(data);
					if(result['value'] == '1')
					{
						photosContent();
					}
					lightGalleryinitialize();
				}
			});
		}
	}
/* end edit album details */

/* delete album */
	function deleteAlbum(album_id, isPermission=false){
		if (album_id !== '') {
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=userwall/delete-album',
					data: "album_id=" + album_id,
					success: function (data)
					{
						$(".discard_md_modal").modal("close");
						var result = $.parseJSON(data);
						if(result['value'] === '1')
						{
							photosContent();
							Materialize.toast('Deleted', 2000, 'green');
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
				var btnKeep = $(".discard_md_modal .modal_keep");
				var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Delete album.");
				btnKeep.html("Keep");                                                   
				btnDiscard.html("Delete");
				btnDiscard.attr('onclick', 'deleteAlbum(\''+album_id+'\', true)');
				$(".discard_md_modal").modal("open");
			}
		}
	}
/* end delete album */

/* add image to album */
	function addAlbumImages() {
		applypostloader('SHOW');
		if(storedFiles.length <= 0) {
			Materialize.toast('Add photos to the album.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		} else {
			$('input[name="imageFile1"]').val('');
			var formdata;
			formdata = new FormData($('form')[1]);
			for(var i=0, len=storedFiles.length; i<len; i++) {
				formdata.append('imageFile1[]', storedFiles[i]); 
			}

			if($('#ordinaorytablicontent').hasClass('active')) {
				var add_image_id = $("#ordinaorytablicontent").attr('data-albumid');
			} else if($('#ablumnamedropdown').length) {
				var add_image_id = $("#ablumnamedropdown").val();
			} else {
				//var add_image_id = $(".add_image_id").val();
			}
			
			formdata.append('add_image_id', add_image_id);
			if(add_image_id != '') {
				$.ajax({ 
					url: '?r=userwall/addimages',  
					type: 'POST', 
					data:formdata,
					processData: false,
					contentType: false,
					success: function(data) {
						//applypostloader('HIDE');
						if(data == 'isreview') {
							Materialize.toast('Photos in review.', 2000, 'green');
						} 
						
						$('#add-photo-popup').modal('close');
						lastModified = [];
						storedFiles = [];
						storedFiles.length = 0;
						$(".imgfile-count").val('0');
						$(".counter").val('0');
						if($('#ordinaorytabli').find('a').hasClass('active')) {
							$title = $('#ordinaorytablititle').text();
							openAlbumImagesTab('this', add_image_id, $title);
							photoscontentsplit();
						} else {
							photoscontentsplit();
						}
					}
				});
			}
		}
	}
/* end add image to album */

/* delete image from album */
	function deleteImage(imagename, image_name, post_id, isPermission=false){
		if (image_name !== '' && post_id !== '') {
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=userwall/delete-image',
					data: "image_name=" + image_name + "&post_id=" + post_id,
					success: function (data)
					{
						$(".discard_md_modal").modal("close");
						var result = $.parseJSON(data);
						if(result['value'] === '1')
						{

							imagename = imagename.replace('.','\\.');

							if($('#ordinaorytablicontent').length) {
								$('#ordinaorytablicontent').find('#albumimg_'+imagename).remove();
								$ordinaorytabbox = $('#ordinaorytablicontent').find('.images-container').find('.grid-box').length;
								$('#ordinaorytablicount').html('('+$ordinaorytabbox+')');
								$('#ordinaorytablicontent').find('.section-title').find('.info').html('('+$ordinaorytabbox+')');


							}
							$("#pc-photos .albums-grid #albumimg_"+imagename).remove();
							$("#pc-all .albums-grid #albumimg_"+imagename).remove();
							Materialize.toast('Deleted', 2000, 'green');
							
							albumscontentsplit();
							photoscontentsplit();
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
				var btnKeep = $(".discard_md_modal .modal_keep");
				var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Delete a photo from this post.");
				btnKeep.html("Keep");                                                   
				btnDiscard.html("Delete");
				btnDiscard.attr('onclick', 'deleteImage(\''+imagename+'\', \''+image_name+'\', \''+post_id+'\', true)');
				$(".discard_md_modal").modal("open");
			}
		}
	}
/* end delete image from album */

/* move image between albums */
	function moveImage(iname,album_id){
		if (iname != '' && album_id != '') {  
			$.ajax({
				type: 'POST', 
				url: '?r=userwall/moveimagedisplay',
				data: "iname="+iname+"&album_id="+album_id,
				success: function (data) {
					$("#moveImageToAlbum").html(data);
					$("#moveImageToAlbum").modal('open');
				}
			});
		}
	}
/* end move image between albums */

/* move images between albums */
	function moveAlbumImage(image_name, from_post_id, isPermission=false){
		var to_post_id = $('#toalbum').val();
		if (image_name !== '' && from_post_id !== '' && to_post_id !== '' ) {
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=userwall/move-album-image',
					data: "image_name=" + image_name + "&from_post_id=" + from_post_id + "&to_post_id=" + to_post_id,
					success: function (data)
					{
						$(".discard_md_modal").modal("close");
						var result = $.parseJSON(data);
						if(result['value'] === '1')
						{
							if($('#ordinaorytabli').find('a').hasClass('active')) {
								$('#ordinaorytablicontent').find('.images-container').find('.grid-box').length;
								image_name = image_name.replace('.','\\.');
								image_name = image_name.replace('/uploads/','');
								$('#ordinaorytablicontent').find('#albumimg_'+image_name).remove();
								$ordinaorytabbox = $('#ordinaorytablicontent').find('.images-container').find('.grid-box').length;
								$('#ordinaorytablicount').html('('+$ordinaorytabbox+')');
								$('#ordinaorytablicontent').find('.section-title').find('.info').html('('+$ordinaorytabbox+')');
							}
							Materialize.toast('Deleted', 2000, 'green');
							albumscontentsplit();
							photoscontentsplit();
							//photosContent();
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
				var btnKeep = $(".discard_md_modal .modal_keep");
				var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Move photo.");
				btnKeep.html("Keep");                                                   
				btnDiscard.html("Move");
				btnDiscard.attr('onclick', 'moveAlbumImage(\''+image_name+'\', \''+from_post_id+'\', true)');
				$(".discard_md_modal").modal("open");
			}
		}
	}
/* end end move images between albums */

/* set album cover */
	function albumCover(uid, image_name, post_id, isPermission=false){
		if (image_name !== '' && post_id !== ''){
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=userwall/album-cover',
					data: "image_name=" + image_name + "&post_id=" + post_id,
					success: function (data)
					{
						$(".discard_md_modal").modal("close");
						var result = $.parseJSON(data);
						if(result['value'] === '1')
						{
							//photosContent();
							Materialize.toast('Cover is set.', 2000, 'green');
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
				var btnKeep = $(".discard_md_modal .modal_keep");
				var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Make this photo album cover.");
				btnKeep.html("Discard");                                                   
				btnDiscard.html("Set");
				btnDiscard.attr('onclick', 'albumCover(\''+uid+'\', \''+image_name+'\', \''+post_id+'\', true)');
				$(".discard_md_modal").modal("open");
			}
		}
	}
/* end set album cover */

/* like gallery photos */
	function doLikeAlbumbPhotos(pid){ 
		if(pid) {
			$.ajax({
				url: '?r=like/like-post', 
				type: 'POST',
				data: 'post_id=' + pid,
				success: function (data) {
					var result = $.parseJSON(data);

					if(result.auth == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} 
					else if(result.auth == 'checkuserauthclassg') {
						checkuserauthclassg();
					} 
					else {
					
						var title =  result.buddies;
						var count =  result.like_count;
						var ls =  result.status;
						var lsc = '';
						if(ls =='1'){
							lsc = 'mdi-thumb-up';
						}
						else
						{
							lsc = 'mdi-thumb-up-outline';
						}
						var cou = '';
						if(count >0) {
						cou = ' '+count+' ';
						}

						$(".ls_"+pid).removeClass('mdi-thumb-up-outline mdi-thumb-up');
						$(".ls_"+pid).addClass(lsc);
						$(".likecount_"+pid).html(cou);
						$('.liketitle_'+pid).attr("data-title", title);
					}
				}
			});
		}
    }
/* end like gallery photos */

/* FUN set album privacy */ 
function setAlbumPrivacy(albumid, albumvalue, obj, isdirect='no', selectore=''){
	if(albumid != '' && albumvalue != '') {
		$.ajax({
			type: 'POST',
			url: '?r=userwall/albumprivacy',
			data: "albumid="+albumid+"&albumvalue="+albumvalue,
			success: function (data) {
				if (albumvalue == 'Private') {
                    $post_dropdown_class = 'lock';
                } else if (albumvalue == 'Connections') {
                    $post_dropdown_class = 'account';
                } else {
                    $post_dropdown_class = 'earth';
                }

                if(isdirect == 'yes') {
					$(selectore).find('i').removeAttr('class');
					$(selectore).find('i').addClass('mdi mdi-'+$post_dropdown_class+' mdi-16px');
                } else {
					$(obj).parents('.dropdown').find('a.dropdown-button').find('i').removeAttr('class');
					$(obj).parents('.dropdown').find('a.dropdown-button').find('i').addClass('mdi mdi-'+$post_dropdown_class+' mdi-16px');
				}
				
				Materialize.toast('Saved.', 2000, 'green');

			}
		});
	} else {
		if($('.modal.open').length>0) {
			$('.modal.open').each(function(i, v) {
			});
		}
	}
}
/* FUN end set album privacy */ 

/* userwall - setting update */	
	function update_basicinfo(){
		var about = $("#about").val();
		var walloccupation = $("#walloccupation").val();
		var wallinterests = $("#wallinterests").val();
		var livesin = $("#lives_in").val();
		var country = $("#country").val();
		var country_code = $("#country_code").val();
		var walllanguage = $("#walllanguage").val();
		var walleducation = $("#walleducation").val();
		
		if(livesin == ''){
			Materialize.toast('Add lives in.', 2000, 'red');
			return false;
		}
		else{
			$.ajax({
			   type: 'POST',
			   url: '?r=site/accountsettings',
			   data: 'about='+about+'&walloccupation='+walloccupation+'&wallinterests='+wallinterests+'&walllanguage='+walllanguage+'&walleducation='+walleducation+'&livesin='+livesin+'&country='+country+'&country_code='+country_code,
			   success: function(data){
					var result = $.parseJSON(data);
					if(result[0] == 'null' || result[0] == '')
					{
						$(".about_rs").html('<a onclick="swapMode(this)" href="javascript:void(0)">Tell other about you</a>');
					}
					else{
						$(".about_rs").html("<span class='darktext'>Personally, </span>"+result[0]);
						$(".about_me").html(result[0]);
					}
					
					if(result[1] == 'null' || result[1] == ''){
						$(".occupation_rs").html('<a onclick="swapMode(this)" href="javascript:void(0)">What your profession</a>');
					}
					else
					{
						$(".occupation_rs").html("<span class='darktext'>Profession</span> "+result[1].replace(/,/g , ', '));
						$(".ocu_me").html(result[1].replace(/,/g , ', '));
					}
					
					if(result[2] == 'null' || result[2] == '')
					{
						$(".interests_rs").html('<a onclick="swapMode(this)" href="javascript:void(0)">What are your interest</a>');
					}
					else
					{
						$(".interests_rs").html("<span class='darktext'>Likes</span> "+result[2].replace(/,/g , ', '));
						$(".inte_me").html(+result[2].replace(/,/g , ', '));
					}
					if(result[3] == 'null' || result[3] == '')
					{
						$(".language_rs").html('<a onclick="swapMode(this)" href="javascript:void(0)">What languages you speak</a>');
					}
					else
					{
						$(".language_rs").html("<span class='darktext'>Speaks</span> "+result[3].replace(/,/g , ', '));
						$(".lang_me").html(result[3].replace(/,/g , ', '));
					}
					if(result[4] == 'null' || result[4] == '')
					{
						$(".education_rs").html('<a onclick="swapMode(this)" href="javascript:void(0)">What is your highest degree</a>');
					}
					else
					{
						$(".education_rs").html("<span class='darktext'>Studied</span> "+result[4].replace(/,/g , ', '));
						$(".edu_me").html(result[4].replace(/,/g , ', '));
					}
					
					$(".lives_rs").html(result[5]);
					$(".city_me").html(result[5]);
					
					if(result[6]) {
						var country  = result[6];
					} else {
						var country = 'Not Added';
					}
					
					$(".loc-info").html('<span class="btext">Currently in:</span>'+country+'<img src="'+$assetsPath+'/images/blank.gif" id="cngcountryflag" class="flag '+result[7] + ' fnone">');
			   }
		   });
		}
	}
/* end userwall - setting update */	

/* Connect List Setting*/
	function update_connect_list_setting(){
		var fs = $(this).parents('li').attr('data-val');
		if(fs) {
			var cls = '';
			if(fs=='private') {
				cls = 'mdi mdi-lock';
			}
			else if(fs=='connections') {
				cls = 'mdi mdi-account';
			}
			else if(fs=='public') {
				cls = 'mdi mdi-earth';
			}
			
			if(cls!='') {
				$('.custom-icondrop .dropdown-toggle i').removeClass();
				$('.custom-icondrop .dropdown-toggle i').addClass(cls);				
			}
		}
		$.ajax({
			   type: 'POST',
			   url: '?r=connect/connectlist-setting',
			   data: 'fs='+fs,
			   success: function(data){
			   }
		   });
	}
/* End Connect List Setting*/

/* view profile pics */
	function viewprofilepics(u_id){
		if (u_id != '') {
			$.ajax({
				type: 'POST',
				url: '?r=userwall/viewprofilepics',
				data: "u_id="+u_id,
				success: function (data) {
					$("#pc-all .photos-area").html(data);
					initGalleryImageSlider();
				}
			});
		}
	}
/* end view profile pics */

/* view cover pics */
	function viewcoverpics(u_id){
		if (u_id != '') {
			$.ajax({
				type: 'POST',
				url: '?r=userwall/viewcoverpics',
				data: "u_id="+u_id,
				success: function (data) {
					$("#pc-all .photos-area").html(data);
					initGalleryImageSlider();
				}
			});
		} 
	}
/* end view cover pics */ 

/* view gallery comment */
	function galary_comment(post_id){
		$.ajax({ 
			url: '?r=comment/view-galary-comments', 
			type: 'POST', 
			data: 'post_id=' + post_id,
			success: function (data){
				$("#compose_Comment_Action").html(data);
				
				setTimeout(function(){
					$("#compose_Comment_Action").modal('open');
					initDropdown(); 
				},1000);
			}
		}); 
	}
/* end view gallery comment */
 	
 	function section_aboutgenderbirthdate(){
 		$.ajax({
			url: '?r=userwall/sectionaboutgenderbirthdate', 
			success: function (data) {
				var result = $.parseJSON(data);
				$var1 = result.birth_date_privacy_custom;
				$var2 = result.gender_privacy_custom;
				birth_date_privacy_custom = $var1;
				gender_privacy_custom = $var2;
			}
		});
	}

/* wall - about content */
	function section_about(wall_user_id){
		$.ajax({
			url: '?r=userwall/section-about', 
			type: 'POST',
			data: 'wall_user_id=' + wall_user_id,
			success: function (data) {
				$("#section_about").html(data);
				setTimeout(function(){ 
					getCountries();
					initDropdown(); 
					$('.tabs').tabs();
					$("#about-content").show();
					section_aboutgenderbirthdate();
				},400);	
			}
		});
	}
/* end wall - about content */

/* wall : add new image to slider */
	function Add_Slider_Cover(user_id,image_name,image_path,image_id){
		$.ajax({
			type: 'POST',
			url: '?r=userwall/add-slider-cover',
			data: "user_id="+user_id+"&image_name="+image_name+"&image_path="+image_path+"&image_id="+image_id,
			success: function (data) {
				
				if(data == '1')
				{					
					if($("#photos-content").find(".user-photos").length > 0){
					
						var win_w = $(window).width();
						var settime = 400;
						if(win_w < 768)
							settime = 700;
						
							
							setTimeout(function(){					
								$('.user-photos').addClass('photoshow');
							},settime);
					
						setTimeout(function(){
							fixImageUI("wall-photoslider");
							
						},400);	
					}
					//photosContent();
				}
			}
		});	
	}
/* end wall : add new image to slider */
	
/* wall : add new album to slider */
	function Add_Slider_Cover_Album(e){
		var $AlbumId = $(e).attr('data-albumid');
		if($AlbumId != undefined || $AlbumId != 'undefined' || $AlbumId != null || $AlbumId != '') {
			$.ajax({
				type: 'POST',
				url: '?r=userwall/add-slider-cover-album',
				data: {$AlbumId},
				success: function (data) {
					if(data == true){
						
						if($("#photos-content").find(".user-photos").length > 0){
						
						var win_w = $(window).width();
						var settime = 400;
						if(win_w < 768)
							settime = 700;
							
						setTimeout(function(){					
							var slider = $('#content-slider').lightSlider({
								slideMargin:4,
								slideWidth:120,
								item:5,
								pager:false,
								loop:true,
								responsive : [
									{
										breakpoint:800,
										settings: {
											item:3,
											slideMove:1,
											slideMargin:6,
										  }
									},
									{
										breakpoint:480,
										settings: {
											item:2,
											slideMove:1
										}
									}
								]
							});
							slider.refresh();
															
							$('.user-photos').addClass('photoshow');
						},settime);
						
						
						setTimeout(function(){
							fixImageUI("wall-photoslider");
							
						},400);	
					}
						
						photosContent();
					}
				}
			});		
		}
	}
/* end wall : add new album to slider */

/* wall : remove album to slider */
	function Remove_Slider_Cover_Album(e){
		var $AlbumId = $(e).attr('data-albumid');
		if($AlbumId != undefined || $AlbumId != 'undefined' || $AlbumId != null || $AlbumId != '') {
			$.ajax({
				type: 'POST',
				url: '?r=userwall/remove-slider-cover-album',
				data: {$AlbumId},
				success: function (data) {
					if(data == true)
					{
						if($("#photos-content").find(".user-photos").length > 0){
						
						var win_w = $(window).width();
						var settime = 400;
						if(win_w < 768)
							settime = 700;
					
							
							setTimeout(function(){					
								var slider = $('#content-slider').lightSlider({
									slideMargin:4,
									slideWidth:120,
									item:5,
									pager:false,
									loop:true,
									responsive : [
										{
											breakpoint:800,
											settings: {
												item:3,
												slideMove:1,
												slideMargin:6,
											  }
										},
										{
											breakpoint:480,
											settings: {
												item:2,
												slideMove:1
											}
										}
									]
								});
								slider.refresh();
								
								
								$('.user-photos').addClass('photoshow');
							},settime);
						
						setTimeout(function(){
							fixImageUI("wall-photoslider");
							
						},400);	
					}
						//LoadSlider();
						photosContent();
					}
				}
			});		
		}
	}
/* end wall : remove album to slider */
	
/* wall : remove image to slider */
	function Remove_Slider_Cover(image_name){
		$.ajax({
			type: 'POST',
			url: '?r=userwall/remove-slider-cover',
			data: "image_name="+image_name,
			success: function (data) {
				if(data == true)
				{
					if($("#photos-content").find(".user-photos").length > 0){
					
					var win_w = $(window).width();
					var settime = 400;
					if(win_w < 768)
						settime = 700;
					
						setTimeout(function(){					
							var slider = $('#content-slider').lightSlider({
								slideMargin:4,
								slideWidth:120,
								item:5,
								pager:false,
								loop:true,
								responsive : [
									{
										breakpoint:800,
										settings: {
											item:3,
											slideMove:1,
											slideMargin:6,
										  }
									},
									{
										breakpoint:480,
										settings: {
											item:2,
											slideMove:1
										}
									}
								]
							});
							slider.refresh();
														
							$('.user-photos').addClass('photoshow');
						},settime);
					
					setTimeout(function(){
						fixImageUI("wall-photoslider");
						
					},400);	
				}
					
					photosContent();
				}
			}
		});
	
	}
/* end wall : remove image to slider */
	
/* wall : load slider */
	function LoadSlider() {
		$.ajax({
			type: 'POST',
			url: '?r=userwall/load-slider',
			success: function (data) {
			}
		});		
	}
/* end wall : load slider 

/************ END Design Code ************/