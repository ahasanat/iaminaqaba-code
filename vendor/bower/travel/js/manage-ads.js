$(document).ready(function(){
	adlist();

	$(document).on('click', '.select-dropdown', function() {
		$('ul.select-dropdown').css('overflow', 'scroll');
	})

	$('.change_head').keyup(function() {
   	 $('.travad-title').html($(this).val());
    });  
   $('.sub_title_you_ad').keyup(function() {
   $('.travad-subtitle').html($(this).val());
   }); 
   $('.catch_phase').keyup(function() {
   $('.change_ad_word').html($(this).val());
   });   
   $('.aware_head').keyup(function() {
   $('.change_awar_head').html($(this).val());
   });  
   $('.aware_text').keyup(function() {
   $('.change_awar_text').html($(this).val());
   }); 
   $('.advert_title').keyup(function() {
   $('.lead_title').html($(this).val());
   }); 
   $('.advert_phrase').keyup(function() {
   $('.lead_phrase').html($(this).val());
   }); 
   $('.advert_head').keyup(function() {
   $('.lead_head').html($(this).val());
   }); 
   $('.advert_text').keyup(function() {
   $('.lead_text').html($(this).val());
   }); 
   $('.advert_url').keyup(function() {
   $('.lead_url').html($(this).val());
   }); 
   $('.conversion_title').keyup(function() {
   $('.con_create_title').html($(this).val());
   }); 
   $('.conversion_phrase').keyup(function() {
   $('.con_create_phrase').html($(this).val());
   }); 
   $('.conversion_head').keyup(function() {
   $('.con_create_head').html($(this).val());
   }); 
   $('.conversion_text').keyup(function() {
   $('.con_create_text').html($(this).val());
   });
   $('.inbox_title').keyup(function() {
   $('.inbox_create_title').html($(this).val());
   });
   $('.inbox_main_catch').keyup(function() {
   $('.inbox_create_main').html($(this).val());
   });
   $('.inbox_sub_catch').keyup(function() {
   $('.inbox_create_sub').html($(this).val());
   });
   $('.inbox_head').keyup(function() {
   $('.inbox_create_head').html($(this).val());
   });
   $('.inbox_text').keyup(function() {
   $('.inbox_create_text').html($(this).val());
   });
   $('.endorsement_phrase').keyup(function() {
   $('.endorsement_create_phrase').html($(this).val());
   });
   $('.endorsement_head').keyup(function() {
   $('.endorsement_create_head').html($(this).val());
   });
   $('.endorsement_text').keyup(function() {
   $('.endorsement_create_text').html($(this).val());
   });       
      
});

function main_change_button(x) {    
	$('.con_create_button').text(x);
}      
function main_inbox_button(y) {    
	$('.inbox_create_button').text(y);
}    
function pageendorse_change(z) {    
	$('.endorsement_create_value').text(z);
}
function main_title_change() {
	var x = document.getElementById("mySelect").selectedIndex;
	$('.head_chtitle').text(document.getElementsByTagName("option")[x].value);
}

/* Start Showing All Ads of User Function */
function adlist()
{
	$.ajax({
		type: 'POST',
		url: '?r=ads/myads',
		data: 'baseUrl='+baseUrl,
		success: function(data)
		{
			$("#ads-list").html(data);
			$('.changeswitch').on('click', function (e)
			{
				var adid = $('.changeswitch').attr("data-adid");
				$.ajax({
					type: 'POST',
					url: '?r=ads/switchad',
					data: 'adid='+adid,
					success: function(data)
					{
						var result = $.parseJSON(data);
						if(result['msg'] == 'success')
						{
							var revst = result['status'];
							$("#ad_value_"+adid).val(revst);
						}
					}   
				});
			});
		}
	});
}
/* End Showing All Ads of User Function */

/* Start Edit Particular Ad Function */
function editAd(adid)
{
	$(".edit-travad").html('');
	$.ajax({
		type: 'POST',
		url: '?r=ads/editad',
		data: 'baseUrl='+baseUrl+'&adid='+adid,
		success: function(data)
		{
			$(".edit-travad").html(data);
			setTimeout(function(){
				initNiceScroll(".nice-scroll"); 
				$('.collapsible').collapsible();

				initDropdown();
				
				var slider = document.getElementById('test-slider');
				  noUiSlider.create(slider, {
				   start: [0, 100],
				   connect: true,
				   step: 1,
				   orientation: 'horizontal', // 'horizontal' or 'vertical'
				   range: {
				     'min': 0,
				     'max': 100
				   },
				   format: wNumb({
				     decimals: 0
				   })
				  });
				 
				  var slider1 = document.getElementById('test-slider1');
				  noUiSlider.create(slider1, {
				   start: [0, 100],
				   connect: true,
				   step: 1,
				   orientation: 'horizontal', // 'horizontal' or 'vertical'
				   range: {
				     'min': 0,
				     'max': 100
				   },
				   format: wNumb({
				     decimals: 0
				   })
				  });
				  	
				  	slider1.noUiSlider.on('change', function(){
				      change_audience_guage();
				   });

				    var rand = Math.floor((Math.random() * 100) + 1);
					gauge = new Gauge($('.gauge2'), {
						values: {
							0 : 'Specific',
							20: 'Good',
							100: 'Broad'
						},
						colors: {
							0 : '#ED1C24',
							20 : '#39B54A',
							80: '#FDD914'
						},
						angles: [
							180,
							360
						],
						lineWidth: 20,
						arrowWidth: 10,
						arrowColor: '#ccc',
						inset:true,

						value: rand
					});

					$('.gauge2').gauge({
						values: {
							0 : 'Specific',
							20: 'Good',
							100: 'Broad'
						},
						colors: {
							0 : '#ED1C24',
							20 : '#39B54A',
							80: '#FDD914'
						},
						angles: [
							180,
							360
						],
						lineWidth: 20,
						arrowWidth: 10,
						arrowColor: '#ccc',
						inset:true,

						value: 10
					}); 

				  $('.audience-form').find('li.disabled').find('label').remove();
				}, 600);
		}
	});
}
/* End Edit Particular Ad Function */