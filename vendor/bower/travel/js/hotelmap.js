  jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "//maps.googleapis.com/maps/api/js?key=AIzaSyDvxNXW5zgo6LcbupPQiTuGvgiwRRuiA7A&callback=initialize";
    document.body.appendChild(script);
  });

  function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
      mapTypeId: 'roadmap'
  };
                  
  // Display a map on the page
  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
  map.setTilt(45);
      
  // Multiple Markers
  var markers = [
    ['', 30.3233, 35.4671],
    ['', 30.3256, 35.4694],
    ['', 30.3263, 35.4691]
  ];
                      
  // Info Window Content
  var infoWindowContent = [
    ['<div class="maps-popups">' +
      '<div class="card">' +
        '<div class="card-image">' +
          '<img src="images/hotel1.png">' +
        '</div>' +
        '<div class="card-content">' +
          '<span class="card-title">Aqaba Guest House</span>' +
          '<span class="checks-holder">' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<label>Excellent - 88/100</label>' +
           '</span>' +
           '<span class="price">JOD 184*</span>' +
           '<span class="vendor-name">Booking.com</span>' +
        '</div>' +
      '</div>' +
    '</div>'],
    ['<div class="maps-popups">' +
      '<div class="card">' +
        '<div class="card-image">' +
          '<img src="images/hotel2.png">' +
        '</div>' +
        '<div class="card-content">' +
          '<span class="card-title">Mövenpick Resort Aqaba</span>' +
          '<span class="checks-holder">' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<label>Excellent - 88/100</label>' +
           '</span>' +
           '<span class="price">JOD 184*</span>' +
           '<span class="vendor-name">Booking.com</span>' +
        '</div>' +
      '</div>' +
    '</div>'],
    ['<div class="maps-popups">' +
      '<div class="card">' +
        '<div class="card-image">' +
          '<img src="images/hotel3.png">' +
        '</div>' +
        '<div class="card-content">' +
          '<span class="card-title">Aqaba Moon Hotel</span>' +
          '<span class="checks-holder">' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<i class="mdi mdi-radiobox-marked active"></i>' +
              '<label>Excellent - 88/100</label>' +
           '</span>' +
           '<span class="price">JOD 184*</span>' +
           '<span class="vendor-name">Booking.com</span>' +
        '</div>' +
      '</div>' +
    '</div>']
  ];
      
  // Display multiple markers on a map
  var infoWindow = new google.maps.InfoWindow(), marker, i;
  
  // Loop through our array of markers & place each one on the map  
  for( i = 0; i < markers.length; i++ ) {
    var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
    bounds.extend(position);
    marker = new google.maps.Marker({
      position: position,
      map: map,
      title: markers[i][0]
    });
    
    // Allow each marker to have an info window    
    google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            infoWindow.setContent(infoWindowContent[i][0]);
            infoWindow.open(map, marker);
        }
    })(marker, i));

    // Automatically center the map fitting all markers on the screen
    map.fitBounds(bounds);
  }

  // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
  var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
    this.setZoom(17);
    google.maps.event.removeListener(boundsListener);
  });
  
}