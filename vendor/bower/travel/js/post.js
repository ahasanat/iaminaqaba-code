/* document ready */

	$(document).ready(function() {
		/************ POST FUNCTIONS ************/
			$(document).on('click', '.directprivacydropdown li', function() {
				var $this = $(this);
				var $privacy = $this.attr('data-cls');
				$privacy = $privacy.replace("post-", "");
				var $privacyArray = ['private', 'connections', 'connect-of-connect', 'custom', 'public'];
				
				if($.inArray($privacy, $privacyArray) !== -1) {
					if($this.parents('.col').length) {
						$id = $this.parents('.col').attr('data-postid');
					} else if($this.parents('.bborder.post-holder').length) {
						$id = $this.parents('.bborder.post-holder').attr('data-postid');
					}

					if($id) {
						$.ajax({
							type: 'POST',  
							url: '?r=site/directprivacy', 
							data: {$privacy, $id},
							success: function(data){
								var result = $.parseJSON(data);
								if(result.status != undefined && result.status == 'yes') {
									Materialize.toast('Saved', 2000, 'green');
									var $content = result.content;
									$this.parents('.timestamp').find('a.dropdown-button').html($content);
								}
							}
						});
					}
				}
			});

			$('.added-tags').tooltipster({ contentAsHTML: true });
		    
		    $("body").on('input propertychange', '#textInput', function()
			{
				mobiletoggle();
			});
			$("body").on('input propertychange', '#title', function()
			{
				mobiletoggle();
			});

			/* new post btn disabled on off if content is exist or not */
		    $(document).on('keyup', '#compose_tool_box textarea, #composeeditpostmodal textarea', function() {
		    	mainFeedPostBtn();
		    });			

			/* readmore click */
			$("body").on("click", ".readmore", openReadMore);
			/* end readmore click */
			
			/* remove video link */
			$("body").on("click", ".remove-vlink", removeVideoLink);
			/* end remove video link */
			 
			/* remove added photo */
			$("body").on("click", ".removePhotoFile", function(e){
				
				if($(this).attr("data-class")=="ep-delpic"){
					openStrapPopup("epost-imgdel",this);				
				}
				else{  
					removePhotoFile(this);
				}
			});
			/* end remove added photo */
			
			/* edit post popup pre-setup */
			$("body").on("click", ".editpost-link", preEditPostSetup);
			/* end edit post popup pre-setup */
			
			/* share post popup iamge fix */
			//$("body").on("click", ".pa-share", fixImageSharePopup);
			/* end share post popup iamge fix */
			
			/* show fullpost click */
			$("body").on("click", ".show-fullpost", showFullPost);		
			/* end show fullpost click */

			/* open reply comment textarea */
			$("body").on("click", ".reply-comment", openReplyComment);
			/* end open reply comment textarea */
			
			/* open edit comment textarea */
			$("body").on("click", ".edit-comment", openEditComment);
			/* end open edit comment textarea */
			
			/* enter on edit comment textarea */
			$("body").on("keyup", "textarea.editcomment-tt", saveEditComment);
			/* end enter on edit comment textarea */
			
			/* enter on edit comment textarea */
			$("body").on("click", ".editcomment-cancel", cancelEditComment);
			/* enter on edit comment textarea */
			
			/* like clicked */
			$("body").on("click", ".pa-like", likeClicked);
			/* end like clicked */		
					
			/* share to connections */
				$("body").on("click", ".share-to-connections", openShareToConnections);		
			/* end share to connections */
			
			/* share to group wall */
				$("body").on("click", ".share-to-groupwall", openShareToGroupwall);		
			/* end share to group wall */
			
			/* share as message */
				$("body").on("click", ".share-as-message", openShareAsMessage);		
			/* end share as message */
			
			/* share privacy clicked */
				$("body").on("click", ".share-privacy", sharePrivacyClicked);				
			/* end share privacy clicked */
					
			/* comment icon clicked */
			//$("body").on("click", ".pa-comment", commentFocus);
			/* end comment icon clicked */
			
			/* load more comments */
				$(".view-morec").click(function(){
					$(this).parents(".post-holder").find(".pcomment-earlier").slideDown();
					$(this).fadeOut();
				});
			/* end load more comments */

			/* new post clicked */
			$(document).on('click keypress keydown', '.new-post', function (e) {
				var win_w = $(window).width();
				if(win_w >= 768){
					if(e.target.className == 'close-popup notpopup' || e.target.className == 'btn btn-primary postbtn' || e.target.className == 'btn btn-primary postbtn') {
						//$(e).removeClass("active");
						//$(e).find(".post-bcontent").slideUp();
					} else {
						expandNewpost(this);
					}
				}
				else{	
					if($(this).parents().hasClass('post-pages')){
						return true;
					}
					else{
						//window.location.href = "newpost.html";
						if($(this).hasClass("refers-newpost")){
							$("#referpost-popup").find('.post-bcontent').show();
						}else if($(this).hasClass('reviews-column')){
							$("#reviewpost-popup").find('.post-bcontent').show();
						}else{
							$("#newpost-popup").find('.post-bcontent').show();
						}
					}
				}
			
			});
			/* end new post clicked */
			
			/* open post location */
				$("body").on("click", ".add-location", function(){
					$(this).parents(".npost-content").find(".post-location").toggle("slow");
				});
			/* end open post location */

			/* open post photos */
				$("body").on("click", ".add-photos",function(){	
					$(this).parents(".npost-content").find(".post-photos").slideDown();
				});
			/* end open post photos */

			/* open post tag */
				$("body").on("click",".add-tag",function(){	
					$(this).parents(".npost-content").find(".post-tag").toggle("slow");
				});
			/* end open post tag */

			/* open post title */
				$("body").on("click", ".add-title",function(e){
					openTitle(this);
				});
			/* end open post title */
			
			/* Start Display Content For Sharing Functionality in Share Popup For The Post */
			$(document.body).on('click', '.sharepostmodalAction', function(e) {
				var editpostid = $(this).data('sharepostid');
				selectedpostid = editpostid;
				$.ajax({
					type: 'POST',  
					url: '?r=site/share-post-pre-set',
					data: 'pid='+editpostid,
					success: function(data){
						if(data) {
							if(data == 'checkuserauthclassnv') {
								checkuserauthclassnv();
							} else if(data == 'checkuserauthclassg') { 
								checkuserauthclassg();
							} else {
								$('#sharepostmodal').addClass("sharepost-popup-"+editpostid);
								$('#sharepostmodal').html(data);
								
								$('#sharepostmodal').modal('open');
								addUserForTag = [];
								
								e.preventDefault();
							}	
							setTimeout(function() {
								setGeneralThings();
							},400);
						}
					}
				});        	
			});
			/* End Display Content For Sharing Functionality in Share Popup For The Post*/

			/* Start Display Content For Edit Functionality in Edit Popup For The Post */	
			$(document.body).on('click', '.composeeditpostAction', function(e) {
				var editpostid = $(this).data('editpostid');
				selectedpostid = editpostid;				
				if(editpostid != '') {
					var fullidnm = '#editpost-popup-'+editpostid;
				}
 
				$.ajax({
					type: 'POST', 
					url: '?r=site/edit-post-pre-set', 
					data:'editpostid='+editpostid,
					success: function(data) {
						if(data) {
							if(data == 'checkuserauthclassnv') {
								checkuserauthclassnv();
							} else if(data == 'checkuserauthclassg') {
								checkuserauthclassg();
							} else {
								$('#composeeditpostmodal').addClass("editpost-popup-"+editpostid);
								$('#composeeditpostmodal').html(data);

								$('#composeeditpostmodal').modal('open');	

								if(window.location.href.indexOf("page/index") > -1) {
							       if($('.modal.open').find('.mdi.mdi-star').length) {
										$rating = $('.modal.open').find('.mdi.mdi-star.active').length;
										if($.inArray($rating, starsArray) !== -1) {
											selectStartPoint = $rating;
										} else {
											selectStartPoint = '';
										}
									}
								}

								setTimeout(function(){ 
									setaddUserForTag(editpostid);  
								},500);
							}
							
							setTimeout(function(){
								setGeneralThings();
								fixImageUI("popup-images");
							},400);
						}
					}
				});        	
			});
			/* End Display Content For Edit Functionality in Edit Popup For The Post */	
			
			/* Start Display Content For Report Functionality in Report Popup For The Post */
			$(document.body).on('click', '.customreportpopup-modal', function(e) {
				var editpostid = $(this).data('reportpostid');
				if(editpostid != '') {
					var fullidnm = '#reportpost-popup-'+editpostid;
				}

				$.ajax({
					type: 'POST',
					url: '?r=site/report-post-pre-set', 
					data:'reportpostid='+editpostid,
					success: function(data) {
						if(data) {
							$('#reportpost-popup-'+editpostid).html(data);
							e.preventDefault();
						}
					}
				});        	
			});
			/* End Display Content For Report Functionality in Report Popup For The Post */	
			
			/* Start Display Content For Edit-Ask Functionality in Edit-Ask Popup For The Post */	
			$(document.body).on('click', '.customeditpopup-modal-ask', function(e) {
				var editpostid = $(this).data('editpostid');
				if(editpostid != '') {
					$.ajax({
						type: 'POST', 
						url: '?r=site/edit-post-pre-set-ask', 
						data: {editpostid},
						success: function(data) {
							if(data) {
								if(data == 'checkuserauthclassnv') {
									checkuserauthclassnv();
								} 
								else if(data == 'checkuserauthclassg') {
									checkuserauthclassg();
								} 
								else {
									$('#compose_newask').html(data);
									setTimeout(function(){
										$('#compose_newask').modal('open');
										setPostBtnStatus();
									},400);
									e.preventDefault();
								}
								setTimeout(function(){
									initDropdown();
									setGeneralThings();
								},400);
							}
						}
					});   
				}      	
			});
			/* End Display Content For Edit-Ask Functionality in Edit-Ask Popup For The Post */	
			
			/* Start Display Content For Edit-Tip Functionality in Edit-Tip Popup For The Post */
			$(document.body).on('click', '.customeditpopup-modal-tip', function(e) {
				var editpostid = $(this).data('editpostid');
				if(editpostid != '') {
					$.ajax({
						type: 'POST',
						url: '?r=site/edit-post-pre-set-tip', 
						data: {editpostid},
						success: function(data) {
							if(data) {
								if(data == 'checkuserauthclassnv') {
									checkuserauthclassnv();
								} 
								else if(data == 'checkuserauthclassg') {
									checkuserauthclassg();
								} 
								else {
									$('#compose_newtrip').html(data);
									setTimeout(function(){
										$('#compose_newtrip').modal('open');
										setPostBtnStatus();
									},400);
									e.preventDefault();
								}
								setTimeout(function(){
									initDropdown();
									setGeneralThings();
								},400);
							}
						}
					}); 
				}       	
			});
			/* End Display Content For Edit-Tip Functionality in Edit-Tip Popup For The Post */
			
			/* Start Display Content For Edit-Place-Review Functionality in Edit-Tip Popup For The Post */
			$(document.body).on('click', '.customeditpopup-modal-place-review', function(e) {
				var editpostid = $(this).data('editpostid');
				selectedpostid = editpostid;
				$.ajax({
					type: 'POST',
					url: '?r=site/edit-post-pre-set-place-review', 
					data:'editpostid='+editpostid,
					success: function(data) {
						if(data) {
							if(data == 'checkuserauthclassnv') {
								checkuserauthclassnv();
							} else if(data == 'checkuserauthclassg') {
								checkuserauthclassg();
							} else {
								$('#compose_newreview').html(data);
								setTimeout(function(){
									$('#compose_newreview').modal('open');
									setPostBtnStatus();
								},400);
								e.preventDefault();
							}
							setTimeout(function(){
								initDropdown();
								setGeneralThings();
								setaddUserForTag(editpostid);
							},800);
						}
					}
				});        	
			});
			/* End Display Content For Edit-Place-Review Functionality in Edit-Tip Popup For The Post */
 
			// insert comment 
			$('body').on( "keypress",".comment_class",function (e) {
				if (e.which == 13)
				{
				  comment_event(this, e);
				  setTimeout(function(){
					setGeneralThings();

					if($("#compose_Comment_Action").length) {
						$("#compose_Comment_Action").animate({ scrollTop: $('#compose_Comment_Action').prop("scrollHeight")}, 1000);
					}

				  },600);
				  return false;
				}
			});
			
			/* Post button disable */
			$("body").on('input propertychange', '.npinput', function() {
				var id = $(this).parents('form').data('id');
				toggleAbilityPostButton();        
			});

			$("body").on('input propertychange', '.textInput', function() {
				var id = $(this).parents('form').data('id');
				
				$(".post-bcontent").show();
				
				toggleAbilityPostButton();         
			});		    
			/* End Post Button Disable*/
			
			// posting
			$("body").on("click", ".postbtn", function(){
				var col = $(this).closest('form').attr('id');
				var getparntcls = $("#"+col).parent().attr('id');
				if(getparntcls) {
					addNewPost("#"+getparntcls);
				}
			});
			$("body").on("click", ".popup-postbtn", function(){			
				var col = $(this).parents('.popup-content');
				var getparntcls = col.find(".new-post").attr('id');
				if(getparntcls) {
					addNewPost("#"+getparntcls);
				}
			});
			/* post security settings */
			$("body").on("click", ".post-private", function(){
				$(this).siblings( '#post_privacy' ).val("Private");
			})
			
			$("body").on("click", ".post-connections", function(){
				$(this).siblings( '#post_privacy' ).val("Connections");
			})
			
			$("body").on("click", ".post-public", function(){
				$(this).siblings( '#post_privacy' ).val("Public");
			})
			/* end post security settings */
			
			// comment reply
			$(document).on("keypress", ".reply_class", function (e) {
				if (e.which == 13)
				{
					replyEvent(this, e);
				
					setTimeout(function(){
						setGeneralThings();
				    },600);
					return false;
				}
			});
			// end comment reply
			
			/* disable share and comment */
			$("body").on("click", ".disable_share", function(e){
				var select = $(this).parents('form');
				if(select.length) {
					if($('input[name="share_setting"]').val() == 'Enable')
					{
						select.find($('input[name="share_setting"]')).val('Disable');
					} else {
						select.find($('input[name="share_setting"]')).val('Enable');
					}
				}
			});

			$("body").on("click", ".disable_comment", function(e){
				var select = $(this).parents('form');
				if(select.length) {
					if($('input[name="comment_setting"]').val() == 'Enable')
					{
						select.find($('input[name="comment_setting"]')).val('Disable');
					} else {
						select.find($('input[name="comment_setting"]')).val('Enable');
					}
				}
			});
			/* end disable share and comment */
			
			/* video link preview */
			$(document.body).on('input propertychange', '#textInput', function() {
				
				var s_parent=$(this).parents(".new-post");
														
				$("#frmnewpost .pvideo-holder").remove();
				var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
				var regex = new RegExp(expression);
				var url = $("#textInput").val();
				if (url.match(regex)) {
				
					urls = "?r=site/curl_fetch";
					link = "url=" + $('#textInput').val();
					managePostButton('HIDE');
					$.ajax({
						url: urls, 
						type: 'POST',
						data: link,
						success: function (data) {
			
							var result = $.parseJSON(data);
						
							if(! (result['title'] && result['desc']) ) {
							return false;
						}
							$("#link_description").val(result['desc']);
							$("#link_title").val(result['title']);
							$("#link_image").val(result['image']);
							$("#link_url").val(result['url']);
							$("#newPost").hide();

							selector = result['title'];
							
							s_parent.find(".desc").find(".pvideo-holder").remove();
							if(result['image'] != 'No Image')
							{
								s_parent.find(".desc").after('<div class="pvideo-holder"><div class="img-holder"><img src="' + result['image'] + '"/></div><div class="desc-holder"> <h4><a href="javascript:void(0)">' + result['title'] + '</a></h4><p>' + result['desc'] + '</p></div><a class="remove-vlink" href="javascript:void(0)"><span class="glyphicon glyphicon-remove"></span></a></div>');
							}
							else
							{
								
								s_parent.find(".desc").after('<div class="pvideo-holder"><h4><a href="javascript:void(0)">' + result['title'] + '</a></h4><p>' + result['desc'] + '</p></div>');
							
							}
							managePostButton('SHOW');
						}
					});
				} else {
					 $('#load').hide();
					$("div.preview_wrap").remove();
					return false;

				}
			});
			/* end video link preview */
			
			/* Start Display Post Detail in Mobile Screen Post-Detail Popup */
			$(document).on('click', '.postdetail-link', function(e) {
				var postid = $(this).data('postid');
				var from_ctr = $('#from_ctr_' + postid).val('3');
				$.ajax({
					type: 'POST',
					url: '?r=site/mobile-post', 
					data:{postid},
					success: function(data) { 
						$('#postopenmodal').modal('open');
						
						$("#postopenmodal").html(data);		
						$("#postopenmodal .comments-section").addClass("open");
						
						setTimeout(function(){fixImageUI();
							$("#postopenmodal .overlay-link.postdetail-link").remove();
							$("#postopenmodal .overlay-link.comments-popup").remove();
							initDropdown();

							lightGalleryinitialize();
							lazyloadvariableisonscreen();
							fixPostImages();
						},400);
					},
				});
				
			});	
			/* End Display Post Detail in Mobile Screen Post-Detail Popup */
			
			$(document.body).on("input propertychange", ".capitalize", function () {
				 // store current positions in variables
				var start = this.selectionStart,
				end = this.selectionEnd;

				var str = $(this).val();
				$(this).val(capitalize(str));
			
				// restore from variables...
				this.setSelectionRange(start, end);
			});
			 
					
		/************ END POST FUNCTIONS ************/
	});
/* end document ready */
	
	$(window).on('load', function() {
		$('.post-list').find('.post-holder').last().addClass('lazyloadscrolluploadstuff lazyloadscroll lazycounthelp');
		lazyloadvariableisonscreen();
	});

/************ Functions **********/
	function openStrapPopup(fromWhere, mainObj, isPermission=false) {
		if(fromWhere=="logout") {
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=site/user-logout',
					success: function(data){
					if(data == "1") {
						Materialize.toast('Logout', 2000, 'green');
						window.location.href='?r=site/index';
					}
					}
				}); 
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
				var btnKeep = $(".discard_md_modal .modal_keep");
				var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Logout?.");
				btnKeep.html("Keep");
				btnDiscard.html("Logout");
				btnDiscard.attr('onclick', 'openStrapPopup(\''+fromWhere+'\', this, true)');
				$(".discard_md_modal").modal("open");
			}
		}
		if(fromWhere=="alert-savedpost") {
			Materialize.toast('Already saved.', 2000, 'red');
			return false;
		}  
    }
    
/* FUN post button manage */
	function managePostButton(action){
		if(action == "SHOW"){
			$(postBtnEle).attr("disabled",false);
		}else{
			$(postBtnEle).attr("disabled",true);
		}
	}
/* FUN end post button manage */

/* FUN post photos input file */
	function setCurrClass(name){curr_cls=name;}	 
	function getCurrClass(){return curr_cls;}
	
	function removePhotoFile(e) {
		// remove from photoUpload array if photo tab is active in places....
		if($('#upload-gallery-popup').length) {
			if($('#upload-gallery-popup').hasClass('open')) {
				if($('.photos.tab').length) {
					if($('.photos.tab').find('a').hasClass('active')) {
						$selector = $(e).parents('.img-box');
						$eq = $('#upload-gallery-popup.open').find('.img-row').find($selector).index();
						if(photoUpload[$eq] != undefined) {
				            photoUpload.splice($eq,1);

				            //reset form.....
				            $('#layeredform')[0].reset();
				            userwall_tagged_users = [];
        					userwall_tagged_usersTemp = [];
        					$('#upload-gallery-popup').find('.userwall_tagged_users').html('Add people to tagged connections');

				            //check 0 image availabel
				            if($('#upload-gallery-popup.open').find('.img-row').find('.img-box').length >0) {
								$selectore = $('#upload-gallery-popup.open').find('.img-row').find('.img-box:first');
								$('.img-row.layered').find('.img-box').removeClass('activelayered');
								$selectore.addClass('activelayered');
								if(typeof photoUpload[0] === 'undefined') {
								} else {
								    $storeduploadpopupJIDSphototitle = photoUpload[0].$uploadpopupJIDSphototitle;
								    $storeduploadpopupJIDSdescription = photoUpload[0].$uploadpopupJIDSdescription;
								    $storeduploadpopupJIDSlocation = photoUpload[0].$uploadpopupJIDSlocation;
								    $storeduploadpopupJIDSvisibleto = photoUpload[0].$uploadpopupJIDSvisibleto
								    $storeduploadpopupJIDStaggedconnections = photoUpload[0].$uploadpopupJIDStaggedconnections;

								    $('.upload-popupJIDS-phototitle').val($storeduploadpopupJIDSphototitle);
								    $('.upload-popupJIDS-description').val($storeduploadpopupJIDSdescription);
								    $('.upload-popupJIDS-location').val($storeduploadpopupJIDSlocation);
								    $('.upload-popupJIDS-visibleto').html($storeduploadpopupJIDSvisibleto);
								    
								    userwall_tagged_users = $storeduploadpopupJIDStaggedconnections;
								    userwall_tagged_usersTemp = $storeduploadpopupJIDStaggedconnections;

								    if(userwall_tagged_users.length) {
								        $.ajax({
								            url: "?r=site/combileidwithname", 
								            type: 'POST',
								            data: {ids: userwall_tagged_users, for: 'tagged_users'},
								            success: function (data) {
								                var result = $.parseJSON(data);
								                if(result.status != undefined && result.status == true) {
								                    var $html = result.html;
								                    $('#upload-gallery-popup').find('.userwall_tagged_usersblock').html($html);
								                }
								            }
								        });
								    } else {
								        $("#upload-gallery-popup").find(".userwall_tagged_usersblock").html("<span class='tagged_person_name userwall_tagged_users'>Add people to tagged connections</span>");
								    }
								}
				            }
				        }
					}
				}
			}
		}

	    var code = $(e).data("code");
	   		
	    for(var i=0;i<storedFiles.length;i++) {
	    	str = storedFiles[i].lastModified;
	        if(str == code) {
	            storedFiles.splice(i,1);
	            break;
	        }
	    }


	    for(var i=0;i<storedFilesExsting.length;i++) {
	    	str = storedFilesExsting[i].lastModified;
	        if(str == code) {
	            storedFilesExsting.splice(i,1);
	            break;
	        }
	    }

	    $(e).parent().remove(); 
		$(e).closest('div').remove();
		if(storedFiles.length == 0 && storedFilesExsting.length == 0)
		{
			//toggleAbilityPostButton();
			mobilepostbutton("hide");
		}
		
		setPostBtnStatus();
		mainFeedPostBtn();


		// Remove upload image input if 3 images is exists or uploaded....
		if($('.modal.open').find('.upldimg').length >= 3) {
			$('.modal.open').find('.custom-file').parents('.img-box').remove();
		} else {
			if(!$('.modal.open').find('.custom-file').length) {
				$uploadBox = '<div class="img-box"> <div class="custom-file addimg-box add-photo ablum-add"> <span class="icont">+</span> <br><span class="">Update photo</span> <input class="upload custom-upload remove-custom-upload edit-collections-file-upload" id="edit-collections-file-upload" title="Choose a file to upload" data-class=".post-photos .img-row" type="file"> </div> </div>';
				if($('.modal.open').find('.img-row').append($uploadBox));
			}	
		}
	}
/* FUN end post photos input file */

/* FUN remove video link */	
	function removeVideoLink(e){
		$(this).parents(".pvideo-holder").remove();
		resetNewPost();
	}
/* FUN end remove video link */

/* FUN pre-edit post */	
	function hidePostExtras(areaid){		
		$("#"+areaid).find(".post-photos").hide();
		$("#"+areaid).find(".npost-title").hide();
		$("#"+areaid).find(".post-location").hide();
		if($(".main-content").hasClass("travelstore-page")){		
			$("#"+areaid).find(".post-category").hide();
			$("#"+areaid).find(".post-price").hide();
		}
	}
	function preEditPostSetup(e){

		$(".post-bcontent").css("display", "block");
		 
		// hide all post extras : not input value inside - parameter = ID of super parent
		hidePostExtras("editpost-popup");
		
		if($(".main-content").hasClass("travelstore-page")){
			var hasPrice=$("#editpost-popup").find(".post-price").find(".pprice").val().trim();
			if(hasPrice)
				$("#editpost-popup").find(".post-price").show();
			
			$("#editpost-popup").find(".post-category").show();
		}
		
		var hasImages=$(".editpost-popup").find(".post-photos").find(".img-row").length;
		if(hasImages)
			$(".editpost-popup").find(".post-photos").show();
		
		var hasTitle=$(".editpost-popup").find(".npost-title").find(".title").val();
		if(hasTitle == undefined) {
			hasTitle = $.trim(hasTitle);
		}

		if(hasTitle!="")
			$(".editpost-popup").find(".npost-title").show();
		
		var hasLocation=$(".editpost-popup").find(".post-location").find(".plocation").val();
		if(hasLocation == undefined) {
			hasLocation = $.trim(hasLocation);
		}

		if(hasLocation!="")
			$(".editpost-popup").find(".post-location").show();
		
		var hasTag=$(".editpost-popup").find(".post-tag").find(".ptag").val();
		if(hasTag == undefined) {
			hasTag = $.trim(hasTag);
		}

		if(hasTag!="")
			$(".editpost-popup").find(".post-tag").show();
		
		setTimeout(function(){
			setGeneralThings();
		},600);
		
		$(".editpost-popup").find(".desc").removeClass("more");
	}
/* FUN end pre-edit post */

/* FUN open read more */	
	function openReadMore(e){	
		
		var descDiv = $(this).parents(".post-mcontent").find(".desc");				
        descDiv.addClass("more");
		$(this).hide();		
	}
/* FUN end open read more */	

/* FUN open full post */	
	function showFullPost(e){	
		
		var getText=$(this).html();
		if(getText.indexOf("Show All") >= 0){
			$(this).html("Collapse <span class='glyphicon glyphicon-arrow-up'></span>");
			//$(this).parents(".org-post").addClass("open-fullpost");
			$(this).parents(".org-post").toggleClass("open-fullpost");
			$(this).parents(".scroll_div").animate({scrollTop: 100}, "slow");
		}
		else{
			$(this).html("Show All <span class='glyphicon glyphicon-arrow-down'></span>");
			//$(this).parents(".org-post").removeClass("open-fullpost");
			$(this).parents(".org-post").toggleClass("open-fullpost");
			$(this).parents(".scroll_div").animate({scrollTop: 0}, "slow");
		}
	}
/* FUN end open full post */	

/* FUN open reply comment textarea*/
	function resetAllReplyComments(obj){
		
		$(".comment-reply-holder.comment-addreply.currOpen").each(function(){			
			
			if(!$(obj).hasClass("currOpen")){				
				$(this).removeClass("currOpen");								
				$(this).find(".addnew-comment").slideUp(200);
			}
		});
	}
	function openReplyComment(e){		
		resetAllReplyComments($(this).parents(".pcomment-holder").find(".comment-addreply"));
		$(this).parents(".pcomment-holder").find(".comment-addreply").find(".addnew-comment").slideDown(200);	
		$(this).parents(".pcomment-holder").find(".comment-addreply").addClass("currOpen");
		
		setTimeout(function(){
			setGeneralThings();
		},400);
	}
/* FUN end open reply comment textarea*/

/* FUN open edit comment textarea*/
	function openEditComment(e){		
		$(this).parents(".pcomment").find(".normal-mode").slideUp();
		$(this).parents(".pcomment").find(".edit-mode").slideDown();
		
		var tt=$(this).parents(".pcomment").find(".edit-mode").find("textarea");
		//setAutoGrowTextarea(tt);
		setTimeout(function(){
			setGeneralThings();
		},600);
	}
/* FUN end open edit comment textarea*/

/* FUN cancel edit comment textarea*/
	function cancelEditComment(e){		
		$(this).parents(".pcomment").find(".normal-mode").slideDown();
		$(this).parents(".pcomment").find(".edit-mode").slideUp();
	}
/* FUN end cancel edit comment textarea*/

/* FUN save edited comment */
	function saveEditComment(e){		
		if((e.keyCode || e.which) == 13) { //Enter keycode		  
			$this = $(this);
			var id = $this.attr("data-id");
			var comment = $this.val().trim();
			if(comment.length >0) {
				$.ajax({
					url: '?r=comment/edit-reply-comment',  
					type: 'POST',
					data:'comment_id='+id+'&edit_comment='+comment,
					success: function(data)
					{
						$this.parents('.desc-holder').find('.normal-mode').find('.desc').find("p#text_"+id).html(comment).change();
						$("#edit_comment_"+id).val(comment);
						
						$this.parents(".pcomment").find(".normal-mode").slideDown();
						$this.parents(".pcomment").find(".edit-mode").slideUp();
					}
	            });
	        }
		}
	}
/* FUN end save edited comment */

	function clearUnderline(){
		if($(".sliding-middle-out").length) {
			$(".sliding-middle-out").each(function() {
			  if($(this).hasClass("focused")){
			  	$(this).removeClass("focused");
			  }
			});
		}
	}

/* FUN like clicked */
	function likeClicked(e){		
		var likeid=$(this).parents(".likeholder").attr("id");
		//alert(likeid);
	}
/* FUN end like clicked */

/* FUN open share to connections */
	function openShareToConnections(){
		$(".share-connections").slideDown("slow");
	}
/* FUN end open share to connections */

/* FUN open group wall */
	function openShareToGroupwall(){
		$(".share-groupwall").slideDown("slow");
	}
/* FUN end open group wall */

/* FUN open message receipent */
	function openShareAsMessage(){
		$(".share-message").slideDown("slow");
	}
/* FUN end open message receipent */

/* FUN share privacy clicked */
	function sharePrivacyClicked(){
		if(!$(this).hasClass("share-to-connections"))
			$(".share-connections").slideUp("slow");
		if(!$(this).hasClass("share-to-groupwall"))
			$(".share-groupwall").slideUp("slow");
		if(!$(this).hasClass("share-as-message"))
			$(".share-message").slideUp("slow");
		
		if($("#sharewall").val() =='own_wall') { $("#sharewall").val('connect_wall'); }
		else { $("#sharewall").val('own_wall'); }
	}
/* FUN end share privacy clicked */

/* FUN pre-share post */	
	function hideShareExtras(){		
		$(".share-connections").hide();
		$(".share-groupwall").hide();
		$(".share-message").hide();
	}
	function preSharePostSetup(e){
		//alert("pre-share");
		// hide all post extras : not input value inside - parameter = ID of super parent
		hideShareExtras();	
	}
/* FUN end pre-share post */

/* FUN open comment replies */
	function openReplies(obj){  
		$(obj).parents(".comments-reply-summery").slideUp();
		$(obj).parents(".comment-reply-holder").find(".comments-reply-details").slideDown();
	}
/* FUN end open comment replies */

/* FUN expand new post */
	function expandNewpost(obj){		
		if($(obj).parents(".reviews-column").length){		
			
			if($(obj).hasClass("expandReview")){
				$(obj).addClass("active");
				$(obj).find(".post-bcontent").show();
				$(obj).find(".expandable").slideDown();				
			}
		}
	}
/* FUN end expand new post */

/* FUN open post category */
	function openAdCategory(obj){
		$(obj).find(".post-category").show("slow");
	}
/* FUN end open post category */

/* open view map inside post */
	function openViewMap(obj){
		var sparent=$(obj).parents(".post-content");
		var mapholder=sparent.find(".map-holder");
		
		if(mapholder.css("display")=="block"){
			mapholder.slideUp();
			$(obj).html("View on map");
		}else{
			mapholder.slideDown();
			$(obj).html("Close map");
		}
	}
/* end open view map inside post */

/* FUN open post title */
	function openTitle(obj){
		
		var getParent=$(obj).parents(".new-post");			
		getParent.find('.npost-title').toggle("slow");
		
		var slideOut=getParent.find(".sliding-middle-out");
				
		setTimeout(function(){titleUnderline(slideOut);},800);
		
	}
/* FUN end open post title */

/* reply to comment */
	function replyEvent(obj, e){
		if (e.which == 13){
			var pid = $(obj).data('postid');
			var cid = $(obj).data('commentid');
			
			if($(".modal.open").length) {
				var modalID = $(".modal.open").attr("id");
				var reply = $('.modal#'+modalID+' #reply_txt_' + cid).val();				
			} else {
				var reply = $('#reply_txt_' + cid).val();				
			}

			if($.trim(reply) == undefined || $.trim(reply) == null || $.trim(reply) == ""){	
			e.preventDefault();
			   return false;
			}
			else{			
				$("#reply_txt_" + pid).css('display', 'inline');
				$("#user_img_" + pid).css('display', 'block');
				$("#reply_txt_" + cid).focus();
				$("#commmets_" + pid).show();
				$("#init_comment_display_" + pid).show();
				
			   var formdata;
				formdata = new FormData();
				formdata.append("post_id", pid);
				formdata.append("reply", reply);
				formdata.append("comment_id", cid);
				
				$.ajax({
					url: '?r=comment/reply-comment', 
					type: 'POST',				
					data: formdata,
					processData: false,
					contentType: false,
					success: function (data) {
						if(data == 'checkuserauthclassnv') {
							checkuserauthclassnv();
						} 
						else if(data == 'checkuserauthclassg') {
							checkuserauthclassg();
						} 
						else {	
							if($(".modal.open").length) {
								var modalID = $(".modal.open").attr("id");
								$("#"+modalID+" #reply_txt_" + cid).val('');
								$(obj).parents('.post-comments').animate({scrollTop: $(obj).parents('.post-comments').find('.pcomments').height()});
							} else {
								$("#reply_txt_" + cid).val('');
							}

							$('#display_reply_' + cid).hide();
							$(".reply_comments_" + cid).append(data);					
						}

						var txtArs = $('#reply_txt_'+cid);
						txtArs.get(0).style.boxSizing = txtArs.get(0).style.mozBoxSizing = 'border-box';
						txtArs.get(0).style.overflowY = 'hidden';    
						txtArs.get(0).style.height = '35px';
					}
				}).done(function(){
				   setTimeout(function(){initDropdown();},500);
			  });
			}
		}
	}
/* end reply to comment */

/* Start Like on Comment Function */
	function likeComment(cid, reply, parentid){	
		$.ajax({
			url: '?r=like/comment-like', 
			type: 'POST',
			data: 'comment_id=' + cid,
			success: function (data) {
				var result = $.parseJSON(data);
				var title =  result.like_buddies;
				var count =  result.like_count;
				var cou = '';
				if (result['status'] == '1'){
					$(".commentcounttitle_"+cid).addClass("active");
					
					if(count >0) {
						cou = count;
					}				
					if(!reply) {
						//$("#comment_"+cid+" .commentcount_"+cid).html(cou);
						$("#comment_"+cid+" .commentcounttitle_"+cid).attr("data-title", title);
					} else {
						//$("#comment_"+parentid+" .commentcount_"+cid).html(cou);
						$("#comment_"+parentid+" .commentcounttitle_"+cid).attr("data-title", title);
					}
				}
				else
				{
					$(".commentcounttitle_"+cid).removeClass("active");	
					$("#comment_"+cid+" .commentcounttitle_"+cid).attr("data-title", title);
				}
			}
		});
	}
/* End Like on Comment Function */

/* Start Show Previous Comments For Perticular Post */
	function showPreviousComments(pid){
		$(".recent").remove();
		if ($(window).width() >= 569) {
			$display = $('.sub_post_comment_'+pid).find('.pcomment-holder').length;
			if($display) {
				var from_ctr = $('#from_ctr_' + pid).val();
			} else {
				var from_ctr = '0';
			}
		}
		else
		{
			var modalID = $(".modal.open").attr("id");
			$display = $('.modal#'+modalID+' .sub_post_comment_'+pid).find('.pcomment-holder').length;
			if($display) {
				var from_ctr = $('#from_ctr_' + pid).val();
			} else {
				var from_ctr = '0';
			}
		}	

		var to_ctr = 5;
		$.ajax({
			url: '?r=comment/load-comment', 
			type: 'POST',
			data: 'from_ctr=' + from_ctr + '&to_ctr=' + to_ctr + '&pid=' + pid,
			success: function (data) {
				if ($(window).width() >= 569) {
					$(".sub_post_comment_" + pid).append(data);
				}
				else
				{
					var modalID = $(".modal.open").attr("id");
					//var comment = $('.modal#'+modalID+' #comment_txt_' + pid).val();
					$(".modal#"+modalID+" .sub_post_comment_" + pid).append(data);
				}

				from_ctr = parseInt($('#from_ctr_' + pid).val()) + parseInt(to_ctr);
				$('#from_ctr_' + pid).val(from_ctr);
				var to_cntr = $('#to_ctr_' + pid).val();

				if (parseInt($('#to_ctr_' + pid).val(), 10) < parseInt($('#from_ctr_' + pid).val(), 10))
				{
					$(".commment-ctr-" + pid).remove('');
					$(".view-morec-" + pid).remove('');
				} else {				
					$('.commment-ctr-'+pid).html('<font class="ctrdis_'+pid+'">'+from_ctr+'</font> of <font class="countdisplay_'+pid+'">'+to_cntr+'</font>');
				}
				if (from_ctr == $('#to_ctr_' + pid).val())
				{
					$(".view-morec-" + pid).remove('');
					$(".commment-ctr-" + pid).remove('');
				}
				$(".tarrow1 .alink1").on("click", function (e) {
						showDrawer(this, e);
				});
			}
		}).done(function(){
				    setTimeout(function(){
					   initDropdown();
					},500);
			  });
	}
/* End Show Previous Comments For Perticular Post */

/* Start Delete Comments Function */
	function deleteComment(pcid,pid){
		
			$.ajax({
				url: '?r=site/delete-post-comment', 
				type: 'POST',
				data: 'comment_id=' + pcid,
				success: function (data)
				{
					var result = $.parseJSON(data);
					var getid = result['post_id'];
					var c_id = result['comment_id'];
					
					var listed_cmnt = $('.ctrdis_'+getid).html();
					var all_cmnt = $('.countdisplay_'+getid).html();
					var listed_cmnt = parseInt(listed_cmnt)-1;
					var all_cmnt = parseInt(all_cmnt)-1;
					
					if(!$(".commentcountdisplay_"+getid).length) {
						$(".pa-comment[data-id='"+getid+"']").html('<i class="zmdi zmdi-comment"></i><span class="lcount commentcountdisplay_'+getid+'"></span>');
					}
					
					if(result['ctr'] != 0)
					{
						$(".commentcountdisplay_"+getid).html(result['ctr']);
						$('.commment-ctr-'+getid).html('<font class="ctrdis_'+getid+'">'+listed_cmnt+'</font> of <font class="countdisplay_'+getid+'">'+all_cmnt+'</font>');
						
							
					}
					else
					{
						$(".commentcountdisplay_"+getid).html('');
						$('.commment-ctr-'+getid).remove();
					}
					if(result['ctr'] < 4)
					{
						$(".view-morec-"+pid).remove();
						$(".commment-ctr-"+pid).remove();
						var from_ctr = $('#from_ctr_' + getid).val('0');
						showPreviousComments(getid);
					}
					if ($(window).width() >= 569) {
						$('#comment_'+c_id).remove();	
					}
					else 
					{
						var modalID = $(".modal.open").attr("id");
						$('.modal#'+modalID+' #comment_' + c_id).remove();
					}	
				}
			});
					
	}
/* End Delete Comments Function */

/* Start Hide Comments Function */
	function hideComment(pcid){
		$.ajax({
			url: '?r=site/hide-post-comment', 
			type: 'POST',
			data: 'comment_id=' + pcid,
			success: function (data)
			{
				if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else {	
					$("#comment_"+pcid).hide();
				}
			}
		});
	}
/* End Hide Comments Function */

/* Start Like Function for The Post */
	function doLike(pid){ 	
		if(pid){
			$.ajax({
			  url: '?r=like/like-post', 
			  type: 'POST',
			  data: 'post_id=' + pid,
			  success: function (data) {
					var result = $.parseJSON(data);
					if(result.auth == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} 
					else if(result.auth == 'checkuserauthclassg') {
						checkuserauthclassg();
					} else {
						var title =  result.buddies;
						var count =  result.like_count;
						var like_status =  result.status;
						var cou = '';
						if(count >0) {
							cou = '<span class="tooltip_content lcount likecount_'+pid+'">'+count+'</span>';
						}

						if(like_status == '1') {
							$(".liketitle_"+pid).addClass("active");
						} else {
							$(".liketitle_"+pid).removeClass("active");	
						}

						$('.liketitle_'+pid).attr("data-title", title);
						if($(".liketitle_"+pid).parents('.like-tooltip').find('.likecount_'+pid).length) {
							$(".liketitle_"+pid).parents('.like-tooltip').find('.likecount_'+pid).remove();	
						}
						$(cou).insertAfter(".liketitle_"+pid);
					}
				}
			});
		}
    }
/* End Like Function for The Post */


/* Start Insert Comments In To DataBase Function */
	function comment_event(obj, e){ 
	  if (e.which == 13)
	  {
		var pid = $(obj).data('postid');

		if($(".modal.open").length) {
			var modalID = $(".modal.open").attr("id");
			var comment = $('#'+modalID+' #comment_txt_' + pid).val();					
		} else {
			var comment = $('#comment_txt_' + pid).val();
		}

		if(($.trim(comment) == undefined || $.trim(comment) == null || $.trim(comment) == ""))
		{
		  e.preventDefault();
		  return false;
		}
		else
		{
		  $("#comment_txt_" + pid).css('display', 'inline');
		  $("#user_img_" + pid).css('display', 'block');
		  $("#comment_txt_" + pid).focus();
		  $("#comment_display_" + pid).show();
		  $('#init_commmets_' + pid).show();

		  var formdata; 
		  formdata = new FormData();
		  formdata.append("post_id", pid);
		  formdata.append("comment", comment);
		  $.ajax({
			url: '?r=comment/comment-post', 
			type: 'POST',
			//data: 'post_id=' + pid + '&comment=' + comment+'&comment_file='+comment_file,
			data:formdata, 
			async: false,
			processData: false,
			contentType: false,
			success: function (data) 
			{ 
				if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else {
				  	var cls = '.post_comment_'+pid +' .sub_post_comment_'+pid;
				  	$(cls).append(data);
					var comcount = $("#hide_"+pid).find('.pcomments').children('.pcomment-holder').last().attr('data-commentcount');
				  	
					if(!$(".commentcountdisplay_"+pid).length) {
						$('<span class="comment-lcount commentcountdisplay_'+pid+'"></span>').insertAfter(".pa-comment[data-id='"+pid+"']");
					}

				if(comcount == 'undefined' || comcount == 'null' || comcount == '' || comcount == undefined)
				{
					if($(".commentcountdisplay_"+pid).length > -1 ){
						var comment_counter = $(".commentcountdisplay_"+pid).html();
						var comment_counter = comment_counter.trim();
						if(comment_counter !='')
						{
							var comment_counter = comment_counter.replace('(', '');
							var comment_counter = comment_counter.replace(')', '');
							var comment_counter = parseInt(comment_counter)+1;
							$(".commentcountdisplay_"+pid).html(comment_counter);
						}
						else
						{

							$(".commentcountdisplay_"+pid).html('1');
						}
					}else{
						$('<span class="comment-lcount commentcountdisplay_'+pid+'">1</span>').insertAfter(".pa-comment[data-id='"+pid+"']");
					}
					
				} else {					
					var listed_cmnt_count = $(".ctrdis_"+pid).html();
					var listed_cmnt_count = parseInt(listed_cmnt_count)+1;
								
					if(comcount>0) {
						$(".commentcountdisplay_"+pid).html(comcount);
						if(comcount>=1 && comcount<6){
						$(".ctrdis_"+pid).html(listed_cmnt_count);
						} else if(comcount>=6){
						$(".ctrdis_"+pid).html(listed_cmnt_count);
						}
						$(".countdisplay_"+pid).html(''+comcount+'');
					} else {
						$(".commentcountdisplay_"+pid).html(''); 
						$(".countdisplay_"+pid).html('');
						$(".ctrdis_"+pid).html('');
					}  
				}

				if($(".modal.open").length) {
					var modalID = $(".modal.open").attr("id");
					$("#"+modalID+" #comment_txt_" + pid).val('');
					$(obj).parents('.post-comments').animate({scrollTop: $(obj).parents('.post-comments').find('.pcomments').height()});
				} else {
				  	$("#comment_txt_" + pid).val('');
				}  
				
					
				/* $(".reply_class").on("keypress", function (e) {
					if (e.which == 13) {
						replyEvent(this, e);
					}
				 }); */
				  var cid = $('#last_comment_id').val();
				  if(cid != '')
				  {
					$('#comment_count').val(parseInt($('#comment_count').val())+parseInt(1));
				  }
				  $("#comment_ctr_" + pid).html($('#comment_count_'+pid).val());
				  $('#image-holder-comment-'+pid).css('display','none');
				  
				  $(".tarrow_"+cid+" .alink_"+cid).on("click", function (e) {
					showDrawer(this, e);
				  });
				   $("#edit_textarea_"+cid).on("keypress", function (e) {
					replace_event(this, e,cid);
				  });
				  
				  $(".imgReply").on("change", function (e) {
					readURL(this,cid,$('#replyimg_'+cid));
				  });
				  $('#imageComment_'+pid).val('');
				  $('#last_comment_id').remove();
				  $('#comment_count_'+pid).remove();	

					var txtArs = $('#comment_txt_'+pid);
					txtArs.get(0).style.boxSizing = txtArs.get(0).style.mozBoxSizing = 'border-box';
					txtArs.get(0).style.overflowY = 'hidden';    
					txtArs.get(0).style.height = '38px';
				}
			}
			
			
		  }).done(function(){
			   setTimeout(function(){initDropdown(); },500);
		  });
		}
	  }
	}
/* End Insert Comments In To DataBase Function */

/* Start Hide Post Function */
	function hidePost(pid, isPermission=false){
		if(pid) {
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=like/hide-post',
					data: "post_id=" + pid,  
					success: function (data) {
						if (data) {
							$('#hide_'+pid).remove();

							$('.places-tabs').find('.tablist').find('ul.li').find('a.active').attr('href');

							if($('.places-tabs').find('.tablist').find('ul li a.active').length) {
								$hrefId = $('.places-tabs').find('.tablist').find('ul li a.active').attr('href');
								if($hrefId) {
									var $totalbox = $($hrefId).find('.bborder.post-holder').length;
									if($totalbox <= 0) {
										$($hrefId).find('.cbox-title').find('span.lt').html('(0)');	
										$($hrefId).find('.content-box').find('.cbox-desc').html('<div class="post-holder bshadow"><div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>No record found.</p> </div></div>');
									} else {
										$($hrefId).find('.cbox-title').find('span.lt').html('('+$totalbox+')');	
									}
								}
							}
							Materialize.toast('Post hide.', 2000, 'green');
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Hide post?");
		        btnKeep.html("Keep");
		        btnDiscard.html("Hide");
		        btnDiscard.attr('onclick', 'hidePost(\''+pid+'\', true)');
		        $(".discard_md_modal").modal("open");
			}
		}
	}
/* End Hide Post Function */

/* Start Un-Follow Connect Function */
	function unfollowConnect(fid, isPermission=false){
		if (fid != '') {
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=site/unfollowconnect',
					data: "fid=" + fid,
					success: function (data) {
						$(".discard_md_modal").modal("close");
						if (data)
						{
							$('.post_user_id_'+fid).hide();
							Materialize.toast('Unfollowed', 2000, 'green');
							$.ajax({
								type: 'POST',
								url: '?r=site/muteconnect',
								data: "fid=" + fid									
							});
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
				var btnKeep = $(".discard_md_modal .modal_keep");
				var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Mute connect posts.");
				btnKeep.html("Keep");
				btnDiscard.html("Unfollow");
				btnDiscard.attr('onclick', 'unfollowConnect(\''+fid+'\', true)');
				$(".discard_md_modal").modal("open");
			}
		}
	}
/* End Un-Follow Connect Function */

/* Start Save Post Function */
	function savePost(postid, posttype, savestatus,type){
		var savestatus = $(".tbsp_" + postid).val();
		if(savestatus=='Save'){var save = 'Saving the selected post?';var savecncl='Cancel';}
		if(savestatus=='Unsave'){var save = 'Unsaving the selected post?';var savecncl='Keep';}
		$.ajax({
			type: 'POST',
			url: '?r=like/save-post',
			data: "postid=" + postid + "&posttype=" + posttype + "&type=" + type,
			success: function (data) {
				var result = $.parseJSON(data);
				if (result['status'] === 'true')
				{
					if (result['saved'] === '1')
					{
						$("#save_post_" + postid).html('Save post');
						$("#save_content_" + postid).hide();
						$(".tbsp_" + postid).val('Save');
						Materialize.toast('Unaved', 2000, 'green');
					} else
					{
						$("#save_post_" + postid).html('Unsave post');
						$(".tbsp_" + postid).val('Unsave');
						Materialize.toast('Saved', 2000, 'green');
					}
				}							
			}
		});
	}
/* End Save Post Function */

/* Start Report Post Function */
	function reportpost(pid, isPermission=false){
		if(pid != '') {
			var desc = $("#textInput").val();
			if(desc == '')
			{	
				Materialize.toast('Enter reporting reason.', 2000, 'green');
				return false;			
			}				
			
			if(isPermission) {
				$.ajax({
					type: 'POST',
					url: '?r=site/reportpost',
					data: "pid=" + pid + "&desc=" + desc,
					success: function (data) {
						$(".discard_md_modal").modal("close");
						if (data)
						{
							$('#hide_'+pid).hide();
							Materialize.toast('Reported', 2000, 'green');
						}
					}
				});
			} else {
				var disText = $(".discard_md_modal .discard_modal_msg");
				var btnKeep = $(".discard_md_modal .modal_keep");
				var btnDiscard = $(".discard_md_modal .modal_discard");
				disText.html("Report this.");
				btnKeep.html("Keep");
				btnDiscard.html("Report");
				btnDiscard.attr('onclick', 'reportpost(\''+pid+'\', true)');
				$(".discard_md_modal").modal("open");
			}
		}
	}
/* End Report Post Function */

/* Start Delete Post Function */
	function deletePost(p_uid, pid, isPermission=false){
		if(isPermission) {
			$.ajax({
				url: '?r=site/delete-post', 
				type: 'POST',
				data: 'post_user_id=' + p_uid + "&pid=" + pid,
				success: function (data){
					if(data != '1'){
						var share_post_id = data; 
						var share_counter = $("#shareid_"+share_post_id).html();
						if(share_counter !='') {
							if(share_counter != undefined && share_counter != null) {
								if(share_counter.match('(')) {
								/*if(share_counter.indexOf('(') != -1){*/
									var share_counter = share_counter.replace('(', '');
								}

								if(share_counter.match(')')) {
									var share_counter = share_counter.replace(')', '');
								}
							}

							var share_counter = parseInt(share_counter)-1;
							if(share_counter >= 1){
								$("#shareid_"+share_post_id).html(share_counter);
							}
							else{
								$("#shareid_"+share_post_id).html('');
							}
						}
						else{
							$("#shareid_"+share_post_id).html('1');
						}
					}							
					$('#hide_'+pid).hide();
					Materialize.toast('Deleted', 2000, 'green');
				}
			});
		} else {
			$('.dropdown-button').dropdown("close");
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard"); 
		    disText.html("Delete this post?");
            btnKeep.html("Cancel");
            btnDiscard.html("Delete");
            btnDiscard.attr("onclick", "deletePost(\""+p_uid+"\",\""+pid+"\", true)");
            $(".discard_md_modal").modal("open");
		}
	}
/* End Delete Post Function */

/* Start Flag Post By Front-End Admin Function */
	function flagPost(p_uid, pid, isPermission=false){
		var desc = $("#textInput").val();
		if(desc == '')
		{	
			Materialize.toast('Enter Flaging reason.', 2000, 'red');
			return false;			
		}

		if(isPermission) {
			$.ajax({
				url: '?r=site/flag-post', 
				type: 'POST',
				data: 'post_user_id=' + p_uid + "&pid=" + pid + "&desc=" + desc,
				success: function (data){
					$(".discard_md_modal").modal("close");
					$('#hide_'+pid).hide();
					Materialize.toast('Flaged', 2000, 'green');
				}
			});
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Flag this.");
			btnKeep.html("Keep");
			btnDiscard.html("Flag");
			btnDiscard.attr('onclick', 'flagPost(\''+p_uid+'\', \''+pid+'\', true)');
			$(".discard_md_modal").modal("open");
		}	
	}
/* End Flag Post By Front-End Admin Function */

/* Start Turn-off-Notification Function */
	function turn_off_notification(pid, label='')	{
		$.ajax({
			type: 'POST',
			url: '?r=site/turnoff-notification',
			data: "pid=" + pid,
			success: function (data) {
				if (data) {
					if(label == '') {
						label = 'post';
					}

					if(data == 2) {
						$(".turnnotpost_"+pid).html('Unmute this '+label+' notification');
						Materialize.toast('Unmute notification.', 2000, 'green');
					}
					if(data == 1) {
						$(".turnnotpost_"+pid).html('Mute this '+label+' notification');
						Materialize.toast('Mute notification.', 2000, 'green');
					}
				}
			}
		});		
	}
/* End Turn-off-Notification Function */

/* Start Share Post Function */
	function sharePost(isPermission=false) {

		if(!(window.location.href.indexOf("collection") > -1)) {
			isPermission = true;			
		}

		if(isPermission) {
			$('#sharepostmodal').modal('close');
			Materialize.toast('Sharing..', 2000, 'green');
			
			var sharewall = $("#sharewall").val();
			//var frndid = $("#frndid").val();
			 var frndid = addUserForShareWith;
			var desc = $("#share_desc").val();
			var spid = $("#spid").val();
			var spid_loader = '#sharepost-popup-'+spid;
			//var post_privacy = $("#post_privacy").val();
			var share_current_location = $('#sharepostmodal').find('#selectedlocation').find('.tagged_location_name').text().trim();

			var $post_privacy = $('#sharepostmodal').find('#post_privacy').text().trim();
			if($.inArray($post_privacy, $post_privacy_array) > -1) {
			} else {
				$post_privacy = 'Public';
			}

			var share_setting = 'Enable';
			var comment_setting = 'Enable';
			if($('#sharepostmodal').find('.toolbox_disable_sharing').is(':checked')) {
				share_setting = 'Disable';
			}
			if($('#sharepostmodal').find('.toolbox_disable_comments').is(':checked')) {
				comment_setting = 'Disable';
			}

			if (desc === "" && frndid === "undefined")
			{
				$("#desc").focus(); 
				return false;
			}
			else
			{
				$.ajax({
					type: 'POST', 
					url: '?r=site/sharenowwithconnections',
					data: {
						posttags: addUserForTag,
						sharewall: sharewall,
						spid: spid,
						frndid: frndid,
						desc: desc,
						post_privacy: $post_privacy,
						current_location: share_current_location,
						share_setting: share_setting,
						comment_setting: comment_setting,
						shareongroupwallids: shareongroupwallArray,
						customids: customArrayTemp
					},
					success: function (data) {
						$(".discard_md_modal").modal("close");
						if (data)
						{
							if($('#pagename').val() == "tripexperience")
							{
								$(data).hide().prependTo(".post-list").fadeIn(3000);
							}
							else
							{
								$(".post-list").find('.row').prepend(data);
							}
							load_last_post(data);
							$("#frndid").val('');
							$(".frnduname").val('');
							$('#sharepostmodal').modal('close');
						} else
						{
							$('#sharepostmodal').modal('close');
						}
						Materialize.toast('Shared', 2000, 'green');	
						
						var share_counter = $("#shareid_"+spid).html();
						if(typeof value === "undefined")
						{
							$("#shareid_"+spid).html('1');
						}
						else
						{ 
							var share_counter = share_counter.replace('(', '');
							var share_counter = share_counter.replace(')', '');
							var share_counter = parseInt(share_counter)+1;
							$("#shareid_"+spid).html(share_counter);
						}
						
					  		
					},
					complete: function(){
					  $('#sharepostmodal').modal('close');
					}
				}).done(function(){
					    setTimeout(function(){
						   fixImageUI('newpost');
						   initDropdown();
						},500);
				  });
			}
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Sharing your wall.");
			btnKeep.html("Discard");
			btnDiscard.html("Share");
			btnDiscard.attr('onclick', 'sharePost(true)');
			$(".discard_md_modal").modal("open");			
		}
				
	}
/* End Share Post Function */

/* Start Share Entity Function */
	function shareEntity(entity, isPermission=false){
		applypostloader('SHOW');
		$('#sharepostmodal').modal('close');
		Materialize.toast('Sharing..', 2000, 'green');

		if(isPermission) {
			var sharewall = $("#sharewall").val();
			var frndid = $("#frndid").val();
			var desc = $("#share_desc").val();
			var spid = $("#spid").val();
			var post_privacy = $("#post_privacy").val();
			var share_current_location = $('#sharepostmodal').find('#selectedlocation').find('.tagged_location_name').text().trim();
			var share_setting = $('#share_setting').val();
			var comment_setting = $('#comment_setting').val();
			if (desc === "" && frndid === "undefined") {
				$("#desc").focus(); 
				$(".discard_md_modal").modal("close");
				applypostloader('HIDE');
				return false;
			} else {
				$.ajax({
					type: 'POST',
					url: '?r=site/shareentity',
					data: "posttags=" + addUserForTag + "&sharewall=" + sharewall + "&spid=" + spid + "&frndid=" + frndid + "&desc=" + desc + "&post_privacy=" + post_privacy + "&current_location=" + share_current_location + "&share_setting=" + share_setting + "&comment_setting=" + comment_setting,
					success: function (data) {
						//applypostloader('HIDE');
						window.location.href="";
						if (data) {
							$("#frndid").val('');
							$(".frnduname").val('');
						}
						Materialize.toast('Shared', 2000, 'green');	
					}
				}).done(function(){
					   setTimeout(function(){fixImageUI('newpost');},500);
				  });
			}
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Share this "+entity+" on my wall.");
			btnKeep.html("Keep");
			btnDiscard.html("Share");
			btnDiscard.attr('onclick', 'shareEntity(\''+entity+'\', true)');
			$(".discard_md_modal").modal("open");
		}
	}
/* Start Share Entity Function */

/* Post Submit button disable */
	function toggleAbilityPostButton() {
		if($("#title, #textInput").val().trim().length > 0 || storedFiles.length > 0){
			$(".postbtn").attr("disabled",false);
		}   
		else if($("#title, #textInput").val().trim().length == 0 && storedFiles.length == 0){
			$(".postbtn").attr("disabled",true);
		}
		else{
			$(".postbtn").attr("disabled",true);
		}
    }
	function toggleAbilityPostBtn(action){
		if(action == "SHOW"){
			$(postBtnEle).attr("disabled",false);
			$(".post_loadin_img").css("display","none");
		}else{
			$(postBtnEle).attr("disabled",true);
			$(".post_loadin_img").css("display","inline-block");
		}
	}	
	function toggleAbilityPageButton() {

      if($("#ptitle").val().trim().length > 0 || $("#ptextInput").val().trim().length > 0 || storedFiles.length > 0){
        $(".postreview").attr("disabled",false);
      }   
      else if($("#ptitle").val().trim().length == 0 && $("#ptextInput").val().trim().length == 0 && storedFiles.length == 0){
        $(".postreview").attr("disabled",true);
      }
      else{
        $(".postreview").attr("disabled",true);
      }
    }
/* END Post Submit button disable */

/* Start Delete Image Function */
	function delete_image(imagename, image_name, post_id, isPermission=false){
		if(isPermission) {
			$.ajax({
				type: 'POST',
				url: '?r=userwall/delete-image',
				data: "image_name=" + image_name + "&post_id=" + post_id,
				success: function (data)
				{
					$(".discard_md_modal").modal("close");
					var result = $.parseJSON(data);
					if(result['value'] === '1')
					{
						$("#imgbox_" + imagename).remove();
						Materialize.toast('Deleted', 2000, 'green');
						removePhotoFile();
					}
				}
			});
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Deleting a photo from this post.");
			btnKeep.html("Keep");
			btnDiscard.html("Delete");
			btnDiscard.attr('onclick', 'delete_image(\''+imagename+'\', \''+image_name+'\', \''+post_id+'\', true)');
			$('#compose_discard_modal').addClass('notallclose');
			$(".discard_md_modal").modal("open");
		}
    } 
/* End Delete Image Function */

/* Start Edit Post Function */
    function edit_post(pid) {
		if (pid != '') {
			applypostloader('SHOW');
			var $w = $(window).width();
			var formdata;
			var parid = '#composeeditpostmodal';

			if($w > 568) {
				var $post_privacy = $('#composeeditpostmodal').find('#post_privacy').text().trim();
			} else {
				var $post_privacy = $('#composeeditpostmodal').find('#post_privacy2').text().trim();
			}

			if($.inArray($post_privacy, $post_privacy_array) > -1) {
			} else {
				$post_privacy = 'Public';
			}
			var title = $(parid +" .title").val();
			var text = $(parid +" textarea.desc").val();
			
			
			var imageExist = $('#composeeditpostmodal').find('.post-photos').find('img').length;
			var $location =  $('#composeeditpostmodal').find('#selectedlocation').find('.tagged_location_name').text().trim();
			
			if(imageExist <= 0 && text.length <=0) {
				applypostloader('HIDE');
				return false;
			}
		
			if($(parid +" #placereviewrate"+pid).val() != null){
				var placereviewrate = $(parid +" #placereviewrate"+pid).val();
			}
			if($("#edit_trav_price_"+pid).val() != null){
				var edit_trav_price = $("#edit_trav_price_"+pid).val();
			}
			if($("#edit_trav_cat_"+pid).val() != null){
				if(title == ''){
					$(parid +" .title").focus();
					applypostloader('HIDE');
					return false;
				}
				var edit_trav_cat = $("#edit_trav_cat_"+pid).html();
			}
			if ($('#pagename').val() == "tripexperience"){
				if(title == ''){
					Materialize.toast('Add title of trip experience.', 2000, 'red');
					$(parid +" .title").focus();
					applypostloader('HIDE');
					return false;  
				}
				if($location == ''){
					Materialize.toast('Add location of trip experience.', 2000, 'red');
					$($location).focus();
					applypostloader('HIDE');
					return false;
				}
			}

			formdata = new FormData($('form')[1]);
			for(var i=0, len=storedFiles.length; i<len; i++) {
				formdata.append('imageFilepost[]', storedFiles[i]); 
			}

			for(var i=0, len=storedFilesExsting.length; i<len; i++) {
	        	$sadks = storedFilesExsting[i].name;
				formdata.append('imageFile2[]', $sadks); 
	        }

			var $share_setting = 'Enable';
	        var $comment_setting = 'Enable';
	        if($('#composeeditpostmodal').find('.toolbox_disable_sharing').is(':checked')) {
	        	$share_setting = 'Disable';
	        }
	        if($('#composeeditpostmodal').find('.toolbox_disable_comments').is(':checked')) {
	        	$comment_setting = 'Disable';
	        }

			formdata.append("desc", text);
			formdata.append("pid", pid);
			formdata.append("post_privacy", $post_privacy);
			formdata.append("edit_current_location", $location);
			formdata.append("share_setting", $share_setting);
			formdata.append("comment_setting", $comment_setting);
			formdata.append("title", title);
			formdata.append("posttags", addUserForTag);
			formdata.append("customids", customArray);

			pagereviewrate = '';
			if(window.location.href.indexOf("page/index") > -1) {
				if($('.modal.open').find('.mdi.mdi-star').length) {
					$rating = $('.modal.open').find('.mdi.mdi-star.active').length;
					if($.inArray($rating, starsArray) !== -1) {
						pagereviewrate = $rating;
					}
				}
			}

			formdata.append("pagereviewrate", pagereviewrate);

			if($(parid +" #placereviewrate"+pid).val() != null)
			{
				formdata.append("placereviewrate", placereviewrate);
			}
			if($("#edit_trav_price_"+pid).val() != null)
			{
				formdata.append("edit_trav_price", edit_trav_price);
			}
			if($("#edit_trav_cat_"+pid).val() != null)
			{
				formdata.append("edit_trav_cat", edit_trav_cat);
			}
			Materialize.toast('Saving...', 2000, 'green');
			$.ajax({
				type: 'POST',
				url: '?r=site/editpost',
				data: formdata,
				processData: false,
				contentType: false,
				success: function (data) {
					if (data)
					{
						//applypostloader('HIDE');
						$ispagereview = false;
						if(window.location.href.indexOf("page/index") > -1) {
					        if($('.modal.open').find('.mdi.mdi-star').length) {
								$ispagereview = true;
							}
						}

						$('#composeeditpostmodal').modal('close')
						newct = 0;
						lastModified = [];
						storedFiles = [];
						storedFiles.length = 0;
						storedFilesExsting = [];
						storedFilesExsting.length = 0;
					  	$('#hide_'+pid).parent('.col').remove();
					  	if($('#pagename').val() == "place")
						{
							var pt = $('#placetype').val();
							displayplaceposts('all',pt);
						}
						else if($('#pagename').val() == "tripexperience")
						{
							$(data).hide().prependTo(".post-list").fadeIn(3000);
						}
						else
						{
							if($ispagereview) {
								$('#reviews-content').find(".post-list").prepend(data);
							} else {
								$(".post-list").find('.row').prepend(data);
							}
						}
						
						lightGalleryinitialize();
						      
					  	Materialize.toast('Saved', 2000, 'green');
					} else {
						return false;
					}
				},
				complete: function(){
				}
			}).done(function(){
			   	setTimeout(function(){
			   		lightGalleryinitialize();
				   	fixImageUI('newpost');
				  	initDropdown();
				},500);
			});
			} else {
			$('#share_fail').css('display', 'inline-block').fadeIn(3000).delay(3000).fadeOut(3000);
		}
	}
	/* End Edit Post Function */
	
	/* Start Add New Post Function */
    function addNewPost(id) {
    	applypostloader('SHOW');
    	Materialize.toast('Posting...', 2000, 'green');
    	
    	var $w = $(window).width();
		var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
        var title = $("#title").val();

        chk = 'off';
        if($('#customchk:checkbox:checked').length > 0) {
            chk = 'on';
        }
        
        var status = document.getElementById('textInput').value;
        
        if (status != "" && reg.test(status) == true) {
             status = status.replace(reg, " ");
        }
        if (title != "" && reg.test(title) == true) {
             title = title.replace(reg, " ");
        }

        var count = document.getElementById('hiddenCount').value;
        var formdata;
        formdata = new FormData($('form')[1]);
        var isImages = 'no';
        for(var i=0, len=storedFiles.length; i<len; i++) {
			formdata.append('imageFile1[]', storedFiles[i]); 
			isImages = 'yes';
        }

        for(var i=0, len=storedFilesExsting.length; i<len; i++) {
        	$sadks = storedFilesExsting[i].name;
			formdata.append('imageFile2[]', $sadks); 
			isImages = 'yes';
        }


        if (/^\s+$/.test(status) && isImages == 'no' )
        {
            toggleAbilityPostButton(HIDE);
            $('#textInput').val("");
            $("#textInput").focus();
            applypostloader('HIDE');
            return false;
        }  
        else if (status == "" && isImages == 'no')
        {
        	$("#textInput").focus();
        	applypostloader('HIDE');
            return false;
        } else if (status != "" && isImages == 'no')
        {
            formdata.append("test", status);
            formdata.append("imageFile1", "");

        } else if (isImages == 'yes' && status == "")
        {
            var ofile = document.getElementById('imageFile1').files;
            formdata.append("imageFile1", ofile);
            formdata.append("test", "");

        } else if (isImages == 'yes' && status != "")
        {
            var ofile = document.getElementById('imageFile1').files;
            formdata.append("imageFile1", ofile);
            formdata.append("test", status);
        }

        if($w > 568) {
        	$post_privacy = $('#post_privacy').text().trim();
		} else {
			$post_privacy = $('#post_privacy2').text().trim();
		}

        if($.inArray($post_privacy, $post_privacy_array) > -1) {
		} else {
			$post_privacy = 'Public';
		}
        var $share_setting = 'Enable';
        var $comment_setting = 'Enable';
        if($('#sharing_setting_btns').find('.toolbox_disable_sharing').is(':checked')) {
        	$share_setting = 'Disable';
        }
        if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
        	$comment_setting = 'Disable';
        }

    	var $location = $('#selectedlocation').find('.tagged_location_name').text().trim();
    	$(".post_loadin_img").css("display","inline-block");
		formdata.append("sharewith",$('#customin1').val());
		formdata.append("sharenot",$('#customin3').val());
		formdata.append("customchk", chk);
		formdata.append("posttags", addUserForTag);
		formdata.append("current_location", $location);
		formdata.append("post_privacy", $post_privacy);
		formdata.append("link_title",$('#link_title').val());
		formdata.append("link_url",$('#link_url').val());
		formdata.append("link_description",$('#link_description').val());
		formdata.append("link_image",$('#link_image').val());
		formdata.append("title", title);
		formdata.append("share_setting",$share_setting);
		formdata.append("comment_setting",$comment_setting);
		formdata.append("pagename",$('#pagename').val());
		formdata.append("tlid",$('#tlid').val());
		formdata.append("eventownerid",$('#eventownerid').val());
		formdata.append("groupownerid",$('#groupownerid').val());
		formdata.append('custom', customArray);
		if ($('#pagename').val() == "tripexperience" && $('#title').val() == "")
		{	
			$('#frmnewpost').find('.npost-title').show().find('.title').focus();
			$('#frmnewpost').find('.post-location').show();
			Materialize.toast('Add trip title.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}
		if ($('#pagename').val() == "tripexperience" && $location == "")
		{
			$("#selectedlocation").focus();
			Materialize.toast('Add trip location.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}
		if ($('#pagename').val() == "tripstory" && $('#title').val() == "")
		{	
			$('#frmnewpost').find('.npost-title').show().find('.title').focus();
			$('#frmnewpost').find('.post-location').show();
			Materialize.toast('Add trip title.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}
		if ($('#pagename').val() == "tripstory" && $location == "")
		{
			$("#selectedlocation").focus();
			Materialize.toast('Add trip location.', 2000, 'red');
			applypostloader('HIDE');
			return false;
		}
		if ($('#pagename').val() == "place")
		{
			formdata.append("current_location",$('#placename').val());
			formdata.append("placetype",$('#placetype').val());
			formdata.append("placetitlepost",$('#placetitlepost').val());
			if($('#pagereviewrate').val() == 0 && $('#placetype').val() == 'reviews')
			{
				Materialize.toast('Rate for the place.', 2000, 'red');
				applypostloader('HIDE');
				return false;
			}
			formdata.append("placereview",$('#pagereviewrate').val());
		}

		$.ajax({ 
            url: '?r=site/upload',  
            type: 'POST', 
            data:formdata,
            async:false,        
            processData: false,
            contentType: false,
            success: function(data) {
            	//applypostloader('HIDE');
            	if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else {
					$('#compose_tool_box').modal('close');
					newct = 0;
	                lastModified = [];
	                storedFiles = [];
	                storedFilesExsting = [];
	                storedFiles.length = 0;
	                storedFilesExsting.length = 0;
	                $("#taginput").val('').trigger('change');
	                if($('#pagename').val() == "place")
					{
						var pt = $('#placetype').val();
						displayplaceposts('all',pt);
					}
					else if($('#pagename').val() == "tripstory")
					{
						$(data).hide().prependTo(".post-list").fadeIn(3000);
					}
					else if($('#pagename').val() == "tripexperience")
					{
						$(data).hide().prependTo(".post-list").fadeIn(3000);
					}
					else
					{
						$(data).hide().prependTo(".post-list .row").fadeIn(3000);
					}

					if($('.post-list').parents('.post-column').find('.joined-tb').length) {
						$('.post-list').parents('.post-column').find('.joined-tb').parents('.post-holder').remove();
					}
	                setTimeout(function() { 
						managePostButton('SHOW');
		                load_last_post(data);        	
						resetNewPost(id);
	                },500);
					$("#textInput").blur(); 
					Materialize.toast('Posted', 2000, 'green');
				}	
            }
        }).done(function(){
           setTimeout(function(){ 
	           	lightGalleryinitialize();
	           	fixImageUI('newpost'); 
	           	initDropdown(); 
           },500);
  		});

   		return true;
    }
	/* End Add New Post Function */

	/* Start Display  Last Post After Add New Post */
 	function load_last_post(data){

		resetNewPost();
		// Custom input
		
		$("#post-status-list").fadeOut(300);
		$(".profiletooltip_content_last").hide();
		$("#newPost").hide();
		$('#textInput').val("");
		$('.getplacelocation').val("");
		$("#imageFile1").val("");
		$("#image-holder").hide();
		$('#displayImag').html("");
		$('#displayImag').hide();
		$(".previewLoading").hide();
		$(".previewImage").hide();
		$(".post-tag").hide();
		$(".previewDesc").hide();
		$('#link_title').val("");
		$('#link_url').val("");
		$('#link_description').val("");
		$('#link_image').val("");
		$('#area').hide();
		$("#title").val("");
		applypostloader('HIDE');
		$("#cur_loc").val("");
		$(".tarrow").removeClass("tarrow1");
		$(".alink").removeClass("alink1");
		$(".addpost-location").hide();
		$('#share_setting').val("Enable");
		$('#comment_setting').val("Enable");     
		$("#imgfilecount").val(0);
		//toggleAbilityPostButton(); 
		setTimeout(function () {
				
			 $('#post-status-list').prepend(data);
			 setTimeout(function(){ initDropdown(); $('.tabs').tabs(); },400);

			 var last_pid = $('#last_inserted').val();

			 $("#comment_txt_" + last_pid).on("keypress", function (e) {
				if (e.which == 13) {
					comment_event(this, e);
					return false;
			 	}
			 });
			 
			 $(".tarrow1 .alink1").on("click", function (e) {
				 showDrawer(this, e);
			 });
			 $(".frnduname").on("keyup", function (e) {
				 connectnames(this, e);
			 });
			 $(".add-loc .add-loc1").on("click", function (e) {
				openLocPost(this, e);
			 });
			 $(".add tag .add-tag1").on("click", function (e) {
				openLocTag(this, e);
			 });
			 $(".on-sel-phone").click(function()
			 {
				$("#frndid").val('');
				$(".frnduname").val('');
				document.getElementById('connectlist').style.display = "none";
			 });
			 $(".on-sel-connections").click(function()
			 {
				document.getElementById('connectlist').style.display = "block";
			 });
			 $(".on-sel-connect").click(function()
			 {
				$("#frndid").val('');
				$(".frnduname").val('');
				document.getElementById('connectlist').style.display = "none";
			 });
			 $(".on-sel-connect-facebook").click(function()
			 {
				$("#frndid").val('');
				$(".frnduname").val('');
				document.getElementById('connectlist').style.display = "none";
			 });
			 $(".frnduname").keyup(function (e) {
				connectnames(this, e);
			 });
			 $('.profilelink').hover(

				function () {
					   var pp=$(this).parents(".tb-panel-head").children(".profiletip-holder");                               
					   pp.show();                 
				}, 

				function (e) {

					  var pp=$(this).parents(".tb-panel-head").children(".profiletip-holder");              

					  if(!pp.hasClass("pp-hovered")){
							  $(".profiletip-holder").hide();
					  }

				}
			);	
			$('.profiletip-holder').hover(

				function () {
					   $(this).addClass("pp-hovered");
					   $(this).show();                
				}, 

				function () {                                
					  $(this).removeClass("pp-hovered");
					  $(this).hide();
				}
			 );
					
			$(".frnduname").on("keyup", function (e) {
				connectnames(this, e);

			});

		   $(".imgComment").on("change", function (e) {
			   comment_reply_stuffs(this,'comment');
		   });

		   $(".imgReply").on("change", function (e) {
			  comment_reply_stuffs(this,'reply');
		   });

		   $("#post-status-list").fadeIn(300);
		   $(".post_loadin_img").fadeOut(300);
								
			// START EDit Post Link for tag 
			$(document.body).on('click', '.popup-modal', function() {
				var editpostid = $(this).data('editpostid');
				if(editpostid) {
						var taginput = $("#edittaginput"+editpostid).data("taginput");
						if (taginput != undefined || taginput != null || taginput != '') {
							var newtaginput = taginput.split(',');
						}
				}
			});
				
		}, 300);
	 }
	/* End Display  Last Post After Add New Post */

	/* Start Delete Photo Function */
	function deletePhoto(pid, isPermission=false){
		if(isPermission) {
			$.ajax({
				url: '?r=site/delete-photo', 
				type: 'POST',
				data: 'pid='+pid,
				success: function (data)
				{
					$(".discard_md_modal").modal("close");
					if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} else {
						photosContent();
						Materialize.toast('Deleted', 2000, 'green');
					}
				}
			});
		} else {
			var disText = $(".discard_md_modal .discard_modal_msg");
			var btnKeep = $(".discard_md_modal .modal_keep");
			var btnDiscard = $(".discard_md_modal .modal_discard");
			disText.html("Photo will be deleted permanently.");
			btnKeep.html("Keep");
			btnDiscard.html("Delete");
			btnDiscard.attr('onclick', 'deletePhoto(\''+pid+'\', true)');
			$(".discard_md_modal").modal("open");
		}
	}
	/* End Delete Photo Function */
	


/* FUN to focus on comment area */
	function commentFocus(){  
		 //clearUnderline();
		 var comid=$(this).attr("data-id");
		 var id = $("#comment_txt_"+comid);
		 //alert($(id).parent().attr("class"));
		 titleUnderline($(id).parent());
		 //$(id).parent().addClass("focused");	 
		 $(id).focus();
		 
		 if($(this).parents(".reviewpost-holder")){
			$(this).parents(".post-data").find(".comments-area").slideDown();
		}
	}
/* FUN to focus on comment area */
	/* Start Function For Publish The Post In Which User is Tagged */
	function publish_tag_post(post_id)
	{
		$.ajax({
			type: 'POST',
			url: '?r=site/publish-tag-post', 
			data: "post_id="+post_id,
			success: function(data) {
				if(data == '1')
				{
					Materialize.toast('Posted on your wall.', 2000, 'green');
					$("#publishposticon").remove();
				}	
			},
		});
	}
	/* End Function For Publish The Post In Which User is Tagged */

/* LazyLoading Content */ 
	function fetchLoadPost(lazyhelpcount=0){
		var pagename = $('#pagename').val();
		var tlid = $('#tlid').val();
		var totalPost = $('.post-list').find('.row').find('.s12.m12').length;

		$.ajax({   
			url: '?r=site/lazyloadpost',  
			type: 'POST',
			data: {
				'lazyhelpcount' : lazyhelpcount,
				'pagename' : pagename,
				'tlid' : tlid,
				'totalPost' : totalPost
			},
			beforeSend: function() {
				$(".post-loader.text-center.postfeed-loader").css("display", "block");
			},
			success: function(data) {
				if(data == '') {
					$(".postfeed-loader").remove();
				}
				
				$('.main-content .post-list').find('.row').append(data); 
				postNumber = lazyhelpcount + 1;
				$('.post-list').find('.post-holder').last().addClass('lazyloadscrolluploadstuff');
			}, 
			complete: function() {
				$(".post-loader.text-center.postfeed-loader").css("display", "none");
				if(window.location.href.indexOf("userwall") > -1) {
				} else {
					initDropdown();
				}
			}
		}).done(function(){
			lightGalleryinitialize();
			lazyloadvariableisonscreen();
			fixPostImages();
		});
	}
/* END LazyLoading Content */

/* like album image */
	function doLikeAlbumbImages(pid){ 
		if(pid) {
			$.ajax({
				url: '?r=like/like-post', 
				type: 'POST',
				data: 'post_id=' + pid,
				success: function (data) {
					var result = $.parseJSON(data);

					if(result.auth == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} 
					else if(result.auth == 'checkuserauthclassg') {
						checkuserauthclassg();
					} 
					else {
						var title =  result.buddies;
						var count =  result.like_count;
						var like_status =  result.status;
						
						if($('.likecount_'+pid).length) {
							if(count >0) {
								$(".likecount_"+pid).html(count);
							} else {
								$(".likecount_"+pid).html('');
							}
						}
						
						if(like_status == '1') {
							$(".liketitle_"+pid).addClass("active");
						} else {
							$(".liketitle_"+pid).removeClass("active");	
						}

						$('.liketitle_'+pid).attr("data-title", title);
					}
				} 
			});
		}
    }
/* end like album image */
