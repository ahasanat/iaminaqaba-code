var urlid = getQueryVariable('id');
var address = getQueryVariable('address');
var yr=new Date().getFullYear();
var activities = ["Site Seeing", "Hiking", "Hot Air Balloon", "Photography", "Zoo","Amuesement Park", "Business Events","Museums","Hanging Out", "Golf", "Skiing", "Snowboarding", "Giving Tours", "Beach", "Dinner", "Movie", "Outdoors", "Biking" ,"Picnic","Shopping","Coffee House","Introduce you to people"];
var rates = ["$35", "$40", "$45", "$50", "$55", "$60", "$65", "$70", "$75", "$80", "$90", "$100"];

$(window).resize(function() {
	// footer work for places home page only
	if($('#places-all').hasClass('active')) {
		$('.footer-section').css('left', '0');
		$w = $(window).width();
		if($w <= 768) {
		   $('.main-footer').css({
		      'width': '100%',
		      'left': '0'
		   });
		} else {
		   var $_I = $('.places-content.places-all').width();
		   var $__I = $('.places-content.places-all').find('.container').width();

		   var $half = parseInt($_I) - parseInt($__I);
		   $half = parseInt($half) / 2;

		   $('.main-footer').css({
		      'width': $_I+'px',
		      'left': '-'+$half+'px'
		   });
		}
	}
});

$(document).ready(function() {
    justifiedGalleryinitialize();
    $('.collection-card-body').find('img').css('opacity', '1');
    $('.collectiondetail-page').find('#placebox').removeClass('dis-none');
    lightGalleryinitialize();
    initDropdown();
});

$(document).on('click', '.tablist .tab a', function(e) {
	$href = $(this).attr('href');
	$href = $href.replace('#', '');
	$('.places-content').removeClass().addClass('places-content '+$href);
	$this = $(this);
});

$(document).ready(function() {
	$w = $(window).width();
	if ( $w > 739) {      
		$(".places-tabs .sub-tabs li a").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".tabs.icon-menu.tabsnew li a").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".mbl-tabnav").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
		$(".clicable.viewall-link").click(function(){
		   $("body").removeClass("remove_scroller");
		}); 
	} else {
		$(".places-tabs .sub-tabs li a").click(function(){
		   $("body").addClass("remove_scroller");
		}); 
		$(".clicable.viewall-link").click(function(){
		   $("body").addClass("remove_scroller");
		});         
		$(".tabs.icon-menu.tabsnew li a").click(function(){
		   $("body").addClass("remove_scroller");
		}); 
		$(".mbl-tabnav").click(function(){
		   $("body").removeClass("remove_scroller");
		});
	}

	$(".header-icon-tabs .tabsnew .tab a").click(function(){
		$(".bottom_tabs").hide();
	});

	$(".places-tabs .tab a").click(function(){
		$(".top_tabs").hide();
	});



	// footer work for places home page only
	$('.footer-section').css('left', '0');
	$w = $(window).width();
	if($w <= 768) {
		$('.main-footer').css({
		   'width': '100%',
		   'left': '0'
		});
	} else {
		var $_I = $('.places-content.places-all').width();
		var $__I = $('.places-content.places-all').find('.container').width();

		var $half = parseInt($_I) - parseInt($__I);
		$half = parseInt($half) / 2;

		$('.main-footer').css({
		   'width': $_I+'px',
		   'left': '-'+$half+'px'
		});
	}

	var slider = document.getElementById('age-slider');
    if(slider){
        noUiSlider.create(slider, {
                start: [0, 100],
                connect: true,
                step: 1,
                orientation: 'horizontal', // 'horizontal' or 'vertical'
                range: {
                 'min': 0,
                 'max': 100
            },
            format: wNumb({
             decimals: 0
            })
        });     
    }

    $('#languagedropdown').mouseenter(function() {	
		if(!$('#languagedropdown').hasClass('langloaded')) {
			$.ajax({
				type: 'GET',
				url: '?r=localdriver/fetchlanguages',
				success: function(data){
					var result = $.parseJSON(data);

					$.each(result, function(i, v) {
						$('#chooseLanguage').append($("<option></option>") .attr("value", v) .text(v)); 
					});
					$('select').material_select();  
					$('#languagedropdown').addClass('langloaded');
					setTimeout(function() { 
						$('.disabled').find('input').remove()
					},400);  
				}
			});
		} else {
			$('.disabled').find('input').remove()
		}
	});

	$('select').material_select();
    $('.test-input > .select-wrapper > .select-dropdown').prepend('<li class="toggle selectall"><span><label></label>Select all</span></li>');
    $('.test-input > .select-wrapper > .select-dropdown').prepend('<li style="display:none" class="toggle selectnone"><span><label></label>Select none</span></li>');
    $('.test-input > .select-wrapper > .select-dropdown .selectall').on('click', function() {
        selectAll();
        $('.test-input > .select-wrapper > .select-dropdown .toggle').toggle();
    });
    $('.test-input > .select-wrapper > .select-dropdown .selectnone').on('click', function() {
        selectNone();
        $('.test-input > .select-wrapper > .select-dropdown .toggle').toggle();
    });

	var activateOption = function(collection, newOption) {
	    if (newOption) {
	        collection.find('li.selected').removeClass('selected');
	 
	        var option = $(newOption); 
	        option.addClass('selected');
	    } 
	};
	
	google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
    });

    $(document).on('click', '.composeTbPostActionlocaldriver', function () {
    	if($(this).hasClass('checkuserauthclassnv')) {
			checkuserauthclassnv();
		} else if($(this).hasClass('checkuserauthclassg')) {
			checkuserauthclassg();
		} else {
		   $.ajax({
	            url: '?r=localdriver/create-event-prepare',
	            success: function(data){
	            	$('#createLocalDriverModal').html(data);
	                setTimeout(function() { 
	                    initDropdown();
	                    $('#createLocalDriverModal').modal('open'); 
	                }, 400);
	            }
	        });
	    }
    });
	
	/************ LOCAL GUIDE FUNCTIONS ************/
	$( ".activity-checkboxes .subcheck" ).bind( "click", function() {
		if($( ".activity-checkboxes .check-all").is(':checked')){
			$( ".activity-checkboxes .check-all").prop("checked",false);
		}
	});
	$( ".activity-checkboxes .check-all").bind( "click", function() {	
		if ($(this).is(':checked')){
			$('.activity-checkboxes .h-checkbox').each(function(){
				$(this).find("input[type='checkbox']").prop("checked",true);
			});				
		}else{
			$('.activity-checkboxes .h-checkbox').each(function(){
				$(this).find("input[type='checkbox']").prop("checked",false);
			});		
		}
	});
	/************ END LOCAL GUIDE FUNCTIONS ************/

	justifiedGalleryinitialize();
	lightGalleryinitialize();
})
/************ START Design Code ************/
	
	/* reset LOCAL guide form */
		function resetLocalGuideForm(fromWhere){
			var sparent;
			if(fromWhere == "desktop"){
				sparent = $(".main-page");
			}else{
				sparent = $(".popup-area.editpost-popup");			
			}
			var formobj = sparent.find(".localdriverevent-form");
			
			formobj.find(".descinput").val("");
			formobj.find(".activity-checkboxes input[type='checkbox']").each(function(){
				$(this).attr('checked', false);
			});
			
			setTimeout(function(){
				$(".createprofilehg").find(".feeinput").select().val("Negotiable").trigger("change");  	
			},400);
		}
	/* end reset LOCAL guide forms */
/************ END Design Code ************/	
/* textarea autogrow */
$(document).on('click', 'textarea#message.materialize-textarea', function() {
	var $value = $(this).val();
	if($value != undefined && $value != null && $value != '') {
		$(this).val('');
	}
});

/* post location search */
$("#evtlocationforsearch").keypress(function(e) {
    if(e.which == 13) {
   		searchClkLocalguide();	
    }
});

if(urlid != undefined && urlid != null && urlid != '') {
	if(address != undefined && address != null && address != '') {
		localdriverSelectedRecord(urlid, address);
	} else {
		//actionRecentLocalguidePosts();
	}
} else {
	//actionRecentLocalguidePosts();
}

$(document).on('click', '.localdriverreview', function (e) {
	if($(this).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(this).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		$.ajax({
	        url: '?r=localdriver/review',
	        success: function(data){
	        	$('#localdriver_review').html(data);
	            setTimeout(function() { 
	                initDropdown();
	                $('.tabs').tabs();
	                $('#localdriver_review').modal('open');
	            }, 400);
	        }
	    });
	}
});


function callmyfun(id)
{
	var objDiv = document.getElementById(id);
     objDiv.scrollTop = objDiv.scrollHeight;
}

function checkedEverything(obj) {
	if($('.modal.open').find('#check_all_everything').prop("checked")) {
	    $('.modal.open').find('.localdrivereventname').find('select option:not(:disabled)').not(':selected').prop('selected', true);
	    $('.modal.open').find('.localdrivereventname').find('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:not(:checked)').not(':disabled').prop('checked', 'checked');
	    var values = $('.modal.open').find('.localdrivereventname').find('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').parent().map(function() {
	        return $(this).text();
	    }).get();
	    $('.modal.open').find('.localdrivereventname').find('input.select-dropdown').val(values.join(', '));
	} else {
	    $('.modal.open').find('.localdrivereventname').find('select option:selected').not(':disabled').prop('selected', false);
	    $('.modal.open').find('.localdrivereventname').find('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').prop('checked', '');
	    var values = $('.modal.open').find('.localdrivereventname').find('.dropdown-content.multiple-select-dropdown input[type="checkbox"]:disabled').parent().text();
	    $('.modal.open').find('.localdrivereventname').find('input.select-dropdown').val(values);
	}
}

function checkedEverything1(obj) {
	if($('#edit_check_all_everything').prop("checked")) {
	    $('.custom-localdriver1 select option:not(:disabled)').not(':selected').prop('selected', true);
	    $('.custom-localdriver1 .dropdown-content.multiple-select-dropdown input[type="checkbox"]:not(:checked)').not(':disabled').prop('checked', 'checked');
	    var values = $('.custom-localdriver1 .dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').parent().map(function() {
	        return $(this).text();
	    }).get();
	    $('.custom-localdriver1 input.select-dropdown').val(values.join(', '));
	} else {
	    $('.custom-localdriver1 select option:selected').not(':disabled').prop('selected', false);
	    $('.custom-localdriver1 .dropdown-content.multiple-select-dropdown input[type="checkbox"]:checked').not(':disabled').prop('checked', '');
	    var values = $('.custom-localdriver1 .dropdown-content.multiple-select-dropdown input[type="checkbox"]:disabled').parent().text();
	    $('.custom-localdriver1 input.select-dropdown').val(values);
	}
}

function checkallselected() {
	if($('#checkallsearch').prop("checked")) {
		 $('#checkallsearchdiv input[type="checkbox"]:not(:checked)').not(':disabled').prop('checked', 'checked');
	} else {
		$('#checkallsearchdiv input[type="checkbox"]:checked').not(':disabled').prop('checked', '');
	}
}

function selectNone() {
}

function selectAll() {
}

/* selected record posts listing */
function localdriverSelectedRecord($id, $address) { 
	if($id != undefined && $id != null && $id != '') {
		$.ajax({
            url: '?r=localdriver/selected-record',
            type: 'POST',
            data: {$id, $address},
            success: function(data) {
            	if(data == '') {
					window.address='?r=localdriver/index';
            	} else {
        			$('.localdriver-page').find('#localdriver-recent').find('.post-list').html(data);
        		}
        	}
        });
    } 
}

/* recent posts listing */
/*function actionRecentLocalguidePosts() {  
	var urlid = getQueryVariable('id');
	if(urlid != undefined && urlid != null && urlid != '') {
		window.location.href='?r=localdriver/index';
	}
	var address = getQueryVariable('address');
	if(address != undefined && address != null && address != '') {
		window.location.href='?r=localdriver/index';
	}
	$.ajax({
        url: '?r=localdriver/recent-localdriver-posts',
        beforeSend: function() {
        	$('#localdriver-recent').find('.post-list').html($loader);
		},
        success: function(data) {
    		$('.localdriver-page').find('#localdriver-recent').find('.post-list').html(data);
    		setTimeout(function() { 
    			initDropdown(); 
    		},400);
    	}
    });
}*/

/* my posts listing */
function myposts() {  
	$.ajax({
        url: '?r=localdriver/myposts',
        beforeSend: function() {
        	$(".localdriver-yours").html($loader);
		},
        success: function(data) {
    		$('.localdriver-yours').html(data);
    		setTimeout(function(){ initDropdown(); },400);
    	}
    });
}

/* edit post popup open */
function editpostpopupopen(id, obj) {
	if($(obj).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(obj).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		if(id != undefined && id != '') {  
			$.ajax({ 
	            url: '?r=localdriver/editpostpopupopen',
				type: 'POST', 
	            data: {id},
	            success: function(data) {
	            	$('.dropdown-button').dropdown("close");
	        		$('#editLocalDriverModal').html(data);
	                setTimeout(function() { 
	                    initDropdown();
	                    $('#editLocalDriverModal').modal('open'); 
	                }, 400);
	        	}
	        });
	    }
	}
}

function uploadphotoslocaldriver(id, obj) {
	if($(obj).hasClass('checkuserauthclassnv')) {
		checkuserauthclassnv();
	} else if($(obj).hasClass('checkuserauthclassg')) {
		checkuserauthclassg();
	} else {
		if(id != undefined && id != '') {  
			$.ajax({ 
	            url: '?r=localdriver/uploadphotoslocaldriver',
				type: 'POST', 
	            data: {id},
	            success: function(data) {
	            	$('.dropdown-button').dropdown("close");
	        		$('#uploadphotoslocaldriver').html(data);
	                setTimeout(function() { 
	                    initDropdown();
	                    $('#uploadphotoslocaldriver').modal('open'); 
	                }, 400);
	        	}
	        });
	    }
	}
}

function actioncreatepost() {  
	applypostloader('SHOW');
	var validate = true;
	var localdriver_vehicletype = $('#localdriver_vehicletype').val();
	var localdriver_onboard = $('#localdriver_onboard').val();
	var localdriver_vehiclecapacity = $('#localdriver_vehiclecapacity').val();
	var localdriver_restriction = $('#localdriver_restriction').val();
	var localdriver_describeyourtalent = $('#localdriver_describeyourtalent').val();
	var localdriver_activity = $('#localdriver_activity').val();	
	var localdriver_rate = $('#localdriver_rate').val();	
    
	if (localdriver_vehicletype == null || localdriver_vehicletype == undefined || localdriver_vehicletype == '') {
		validate = false;
		Materialize.toast('Enter vehicle type.', 2000, 'red');
		$('#localdriver_vehicletype').focus();
		applypostloader('HIDE');
		return false;
	}
		
	if (localdriver_describeyourtalent == null || localdriver_describeyourtalent == undefined || localdriver_describeyourtalent == '') {
		validate = false;
		Materialize.toast('Enter meet your driver.', 2000, 'red');
		$('#localdriver_describeyourtalent').focus();	
		applypostloader('HIDE');
		return false;
	}

	if(rates.indexOf(localdriver_rate) !== -1){
	} else {
		validate = false;
		Materialize.toast('Select rate.', 2000, 'red');
		$('#localdriver_rate').focus();	
		applypostloader('HIDE');
		return false;    
	}


    if (storedFiles.length < 3) {
		validate = false;
        Materialize.toast('Please upload three cover photos.', 2000, 'red');
        applypostloader('HIDE');
        return false;
    }

	if(validate) {
		var fd;
	    fd = new FormData();
	   	for(var i=0, len=storedFiles.length; i<len; i++) { 
	        fd.append('localdriver_images[]', storedFiles[i]);
	    }
	    fd.append('localdriver_vehicletype', localdriver_vehicletype);
	    fd.append('localdriver_onboard', localdriver_onboard);
	    fd.append('localdriver_vehiclecapacity', localdriver_vehiclecapacity);
	    fd.append('localdriver_restriction', localdriver_restriction);
	    fd.append('localdriver_describeyourtalent', localdriver_describeyourtalent);
	    fd.append('localdriver_activity', localdriver_activity);
	    fd.append('localdriver_rate', localdriver_rate);

		$.ajax({
            url: '?r=localdriver/createpost',
            type: 'POST',
			data: fd,
			contentType: 'multipart/form-data',
			processData: false,
			contentType: false,
			success: function(result) {
				//applypostloader('HIDE');
				$result = JSON.parse(result);
				if($result.status != undefined && $result.status == true) {
					$('#createLocalDriverModal').modal('close'); 
            		Materialize.toast('Profile created.', 2000, 'green');
            		window.location.href="";
				}
            }
        });
	}
}

function editpostsave(id) {
	applypostloader('SHOW');
	var validate = true;
	var localdriver_vehicletype = $('#editlocaldriver_vehicletype').val();
	var localdriver_onboard = $('#editlocaldriver_onboard').val();
	var localdriver_vehiclecapacity = $('#editlocaldriver_vehiclecapacity').val();
	var localdriver_restriction = $('#editlocaldriver_restriction').val();
	var localdriver_describeyourtalent = $('#editlocaldriver_describeyourtalent').val();
	var localdriver_activity = $('#editlocaldriver_activity').val();	
	var localdriver_rate = $('#editlocaldriver_rate').val();	
    
	if (localdriver_vehicletype == null || localdriver_vehicletype == undefined || localdriver_vehicletype == '') {
		validate = false;
		Materialize.toast('Enter vehicle type.', 2000, 'red');
		$('#editlocaldriver_vehicletype').focus();
		applypostloader('HIDE');
		return false;
	}
		
	if (localdriver_describeyourtalent == null || localdriver_describeyourtalent == undefined || localdriver_describeyourtalent == '') {
		validate = false;
		Materialize.toast('Enter meet your driver.', 2000, 'red');
		$('#editlocaldriver_describeyourtalent').focus();	
		applypostloader('HIDE');
		return false;
	}

	if(rates.indexOf(localdriver_rate) !== -1){
	} else {
		validate = false;
		Materialize.toast('Select rate.', 2000, 'red');
		$('#editlocaldriver_rate').focus();	
		applypostloader('HIDE');
		return false;    
	}


	if ($('#editLocalDriverModal').find('.custom-file').length) {
		validate = false;
        Materialize.toast('Please upload three cover photos.', 2000, 'red');
        applypostloader('HIDE');
        return false;
	} else if ($('#editLocalDriverModal').find('.post-photos').find('.img-row').find('.img-box').length != 3) {
		validate = false;
        Materialize.toast('Please upload three cover photos.', 2000, 'red');
        applypostloader('HIDE');
        return false;
    }

	if(validate) {
		var fd;
	    fd = new FormData();
	   	for(var i=0, len=storedFiles.length; i<len; i++) { 
	        fd.append('localdriver_images[]', storedFiles[i]);
	    }
	    fd.append('localdriver_vehicletype', localdriver_vehicletype);
	    fd.append('localdriver_onboard', localdriver_onboard);
	    fd.append('localdriver_vehiclecapacity', localdriver_vehiclecapacity);
	    fd.append('localdriver_restriction', localdriver_restriction);
	    fd.append('localdriver_describeyourtalent', localdriver_describeyourtalent);
	    fd.append('localdriver_activity', localdriver_activity);
	    fd.append('localdriver_rate', localdriver_rate);
	    fd.append('id', id);

		$.ajax({
			url: '?r=localdriver/editpost',
            type: 'POST',
			data: fd,
			contentType: 'multipart/form-data',
			processData: false,
			contentType: false,
			success: function(result) {
				//applypostloader('HIDE');
				$result = JSON.parse(result);
				if($result.status != undefined && $result.status == true) {
					$('#editLocalDriverModal').modal('close'); 
            		Materialize.toast('Profile updated.', 2000, 'green');
            		window.location.href="";
				}
            }
        });
	}
}

function uploadphotoslocaldriversave(id) {
	var validate = true;
	var fd;
	applypostloader('SHOW');
    fd = new FormData();
   	for(var i=0, len=storedFiles.length; i<len; i++) {
        fd.append('localdriver_images[]', storedFiles[i]);
    }
    fd.append('id', id);

    if (!$('#uploadphotoslocaldriver').find('.post-photos').find('.img-row').find('.img-box').length) {
		validate = false;
        Materialize.toast('Upload photos.', 2000, 'red');
        applypostloader('HIDE');
        return false;
    }

	if(validate) {
		$.ajax({
            url: '?r=localdriver/uploadphotoslocaldriversave',
            type: 'POST',
			data: fd,
			contentType: 'multipart/form-data',
			processData: false,
			contentType: false,
			success: function(result) {
				//applypostloader('HIDE');
				$result = JSON.parse(result);
				if($result.status != undefined && $result.status == true) {
					$('#uploadphotoslocaldriver').modal('close'); 
            		Materialize.toast('Profile updated.', 2000, 'green');
            		window.location.href="";
				}
            }
        });
	}	
}

/* send invite post */
function sendinvitepost($id, $obj) {
	if($id) {
		var $parent = $('.post-list').find('.postid_'+$id);
		if($parent.length) {
			$arrival = $($parent).find('.formpostid_'+$id).find('#arrival_date').val().trim();
			$departure = $($parent).find('.formpostid_'+$id).find('#departune_date').val().trim();
			$message = $($parent).find('.formpostid_'+$id).find('#message').val().trim(); 
			var $validate = true;
			if($arrival == '') {
				$validate = false;
				$($parent).find('.formpostid_'+$id).find('#arrival_date').focus();
				Materialize.toast('Select arrival date.', 2000, 'red');
				return false;
			} else if($departure == '') {
				$validate = false;
				$($parent).find('.formpostid_'+$id).find('#departune_date').focus();
				Materialize.toast('Select departure date.', 2000, 'red');
				return false;
			} else if(new Date($arrival) > new Date($departure)) {
	    		$validate = false;
				$($parent).find('.formpostid_'+$id).find('#message').focus();
				Materialize.toast('Departure date is always greater or equal to arrival date.', 2000, 'red');
				return false;
			} else if($message.length <=0) {
				$validate = false;
				$($parent).find('.formpostid_'+$id).find('#message').focus();
				Materialize.toast('Insert message.', 2000, 'red');
				return false;
			}

			if($validate) {
				$.ajax({
	                url: '?r=localdriver/send-invite-post',
	                type: 'POST',
	                data: { 
	                	id: $id, 
	                	arrival: $arrival, 
	                	departure: $departure, 
	                	message: $message
	                },
	                success: function(data) {
	                	$result = JSON.parse(data);
	                	if($result.status != undefined || $result.status == true) {
	                		$htmlBlk = '<a href="javascript:void(0)" class="cborder-btn green-styled-btn" style="color: #a1a1a1 !important;">Invite</a>';	            			
	                		if($($obj).parents('.content-holder').find('.normal-mode').find('.cborder-btn').length) {
	                			$($obj).parents('.content-holder').find('.normal-mode').find('.cborder-btn').remove();
	                			$($obj).parents('.content-holder').find('.normal-mode').append($htmlBlk);
	                		}
	                		close_detail($obj);
	                		Materialize.toast('Invitation sent.', 2000, 'green');
						} else {
							Materialize.toast('some thing wont wrong, try after some time.', 2000, 'red');
	                	}
	                }
	            });
			}
		}
	}
}

/* save post  */
function actionSavePost($id, $obj) {
	if($id) {
		$.ajax({
            url: '?r=localdriver/save-post',
            type: 'POST',
            data: {$id},
            success: function(data) {
            	if(data) {
            		$($obj).closest('li').remove();
            		Materialize.toast('Successfully saved post', 2000, 'green');
            	}
            }
        });
	}
}

function localdriversaveevents(obj, $id) {
    $('.dropdown-button').dropdown('close');
	if($id) {
		$.ajax({
            url: '?r=localdriver/save-event',
            type: 'POST',
            data: {$id},
            success: function(data) {
            	$result = JSON.parse(data);
            	if($result.status != undefined && $result.status == true) {
            		$label = $result.label;
          		    Materialize.toast('Localguide '+$label+'.', 2000, 'green');

            		if($('#checkisactivesave').hasClass('active')) {
            			$(obj).parents('.post-holder').remove();
            			if($('#localdriver-saved').find('.post-list').find('.post-holder').length <=0) {
            				$('#localdriver-saved').find('.post-list').html('<div class="post-holder bshadow"> <div class="joined-tb"> <i class="mdi mdi-file-outline"></i> <p>No hangout found</p> </div> </div>');
            			}
            		} else {
	            		if($label == 'Save') {
	            			$label = 'Unsave';
	            		} else {
	            			$label = 'Save';
	            		}
	        			$(obj).html("<a href='javascript:void(0)'>"+$label+" gudie profile</a>");
	        		}
            	}
            }
        });
	}
}

/* delete post */
function actionDeletePost($id, $obj, isPermission=false) {
	if($id) {
		if(isPermission) {
			$.ajax({
                url: '?r=localdriver/delete-post',
                type: 'POST',
                data: {$id},
                success: function(data) {
                	$(".discard_md_modal").modal("close");
                	if(data) {
                		var selectore = $('#localdriver-recent').find('.postid_'+$id);
                		if(selectore.length) {
                			selectore.remove();
                			Materialize.toast('Event hide.', 2000, 'green');
                			var selectore = $('#localdriver-recent').find('.post-list').find('.post-holder.localdriver-post').length;
                			if(!selectore) {
                				getnolistfound('nolocaldriverfound');
                			}
                		}
                	}
                }
            });
		} else {
			$('.dropdown-button').dropdown("close");
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
	    	disText.html("Hide event.");
            btnKeep.html("Keep");
            btnDiscard.html("Hide");
            btnDiscard.attr('onclick', 'actionDeletePost(\''+$id+'\', this, true)');
            $(".discard_md_modal").modal("open");
		}
	}
}

/* delete post from my list */
function actionDeleteMyEventFromList($id, obj, isPermission=false) {
	if(obj) {
		if($id) {
			if(isPermission) {
				$.ajax({
	                url: '?r=localdriver/delete-my-post',
	                type: 'POST',
	                data: {$id}, 
	                success: function(data) {
	                	$(".discard_md_modal").modal("close");
	                	if(data) {
	                		if($('#recent-menu').hasClass('active')) {
	                			if($('#localdriver-recent').find('.postid_'+$id).length) {
		                			$('#localdriver-recent').find('.postid_'+$id).remove();
		                			Materialize.toast('Event deleted.', 2000, 'green');
				        			if(!($('#localdriver-recent').find('.post-list').find('.post-holder').length))	{
				        				getnolistfound('nolocaldriverpostfound');
								    }
								}
	                		} else {
		                		if($('#localdriver-yours').find('.postid_'+$id).length) {
		                			$('#localdriver-yours').find('.postid_'+$id).remove();
		                			Materialize.toast('Event deleted.', 2000, 'green');
				        			if(!($('#localdriver-yours').find('.post-list').find('.post-holder').length))	{
				        				getnolistfound('nolocaldriverpostfound');
								    }
								} 
							}
						}
	                }
	            });
			} else {
				$('.dropdown-button').dropdown("close");
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
		    	disText.html("Delete your event.");
	            btnKeep.html("Keep");
	            btnDiscard.html("Delete");
	            btnDiscard.attr('onclick', 'actionDeleteMyEventFromList(\''+$id+'\', this, true)');
	            $(".discard_md_modal").modal("open");
			}
		}
	}
}

/* block user for post */
function actionBlockUserforPost($id, $obj, isPermission=false) {
    if($id) {
    	if(isPermission) {	
    		$.ajax({
                url: '?r=localdriver/block-user-post',
                type: 'POST',
                data: {$id},
                success: function(data) {
                    if(data) {
                      	if($('#recent-menu').hasClass('active')) {
                        	//actionRecentLocalguidePosts();
                    	} else {
                    		actionRequestsList();
                    	}
                    }
                }
            });
    	} else {
    		$('.dropdown-button').dropdown("close");
    		var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
	    	disText.html("Block this post user.");
            btnKeep.html("Keep");
            btnDiscard.html("Block");
            btnDiscard.attr('onclick', 'actionBlockUserforPost(\''+$id+'\', this, true)');
            $(".discard_md_modal").modal("open");
    	}
    }
}

/* save post list */
function actionSavedEventList() {
	$.ajax({
        url: '?r=localdriver/saved-event-list',
        beforeSend: function() {
        	$('#localdriver-saved').find('.post-list').html($loader);
		},
        success: function(data) {
        	$('#localdriver-saved').find('.post-list').html('');
    		$('#localdriver-saved').find('.post-list').html(data);
    		initDropdown();
        }
    });
}

/* unsave post */
function actionUnSavePost($id, $obj) {
	if($id) {
		$.ajax({
            url: '?r=localdriver/unsave-post',
            type: 'POST',
            data: {$id},
            success: function(data) {
            	if(data) {
            		var selectore = $($obj).parents('.postid_'+$id);
            		if(selectore.length) {
            			selectore.remove();
            			var selectore = $('#localdriver-saved').find('.post-list').find('.post-holder.localdriver-post').length;
            			if(!selectore) {
            				getnolistfound('nolocalaguidsavedfound');
            			}
            		}
            		Materialize.toast('Successfully unsaved event', 2000, 'green');
            	}
            }
        });
	}
}

/* post search */
function searchClkLocalguide() {
	$('.combined-column').find('.cbox-title').find('.nav-tabs').find('li.active').removeClass('active');
	$('.combined-column').find('.post-column').find('.tab-content').find('.tab-pane.active').removeClass('active in');
	$('.combined-column').find('.cbox-title').find('.nav-tabs').find('li:first').addClass('active');
	$('.combined-column').find('.post-column').find('.tab-content').find('.tab-pane:first').addClass('active in');
	var recommandDest = $('#evtlocationforsearch').val().trim();
	var localdriverInfo = new Array();
	$("input:checkbox[name=localdriverInfo]:checked").each(function() {
	   localdriverInfo.push($(this).val());
	});
	var lastLogin = $('#lastLogin').val();
	var joinDate = $('#joindate').val();
	var language = new Array();
	$("select#chooseLanguage option:selected").each(function()
	{
	      var optionValue = $(this).val();
	      language.push(optionValue);
	});

	var minValue = $('.noUi-handle-lower').find('.noUi-tooltip').find('span').html().trim() * 1;
	var maxValue = $('.noUi-handle-upper').find('.noUi-tooltip').find('span').html().trim() * 1;
	if(minValue<0 || minValue > 100){
		minValue  = 0;
	}
	if(maxValue<0 || maxValue >100){
		maxValue  = 100;
	}
	var gender = new Array();
	$("input:checkbox[name=gendername]:checked").each(function() {
	   gender.push($(this).val());
	});
	$.ajax({
		url: '?r=localdriver/search',
		type: 'POST',
		data: {
			recommandDest: recommandDest,
			localdriverInfo: localdriverInfo,
			lastLogin: lastLogin,
			joinDate: joinDate,
			language: language,
			age: {
				minValue: minValue,
				maxValue: maxValue
			},
			gender: gender
		},
		success: function(data) {
			if($('#searcharea1').hasClass('expanded')) {
				$('#searcharea1').removeClass('expanded');
			}
			$('#localdriver-recent').find('.post-list').html(data);
			$('.pagetabs').find('li.active').removeClass('active');
			$('.pagetabs').find('ul.nav-tabs').find('li:first').addClass('active');
			$('html, body').animate({scrollTop : 0},500);
			setTimeout(function() { 
    			initDropdown(); 
    		},400);
		}
	});
}

/* post requests listing  */
function actionRequestsList() {
	$htmlBlock = '<div class="content-desc bshadow"> <div class="offer-ul"> <div class="data-container"> <ul> <center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center> </ul></div> <div id="pagination-demo1" class="pagination-holder"></div> </div> <div class="offer-details"> <div class="post-loader text-center"> </div> </div> </div>';
	$.ajax({
		beforeSend: function() {
        	$("#localdriver-requests").html($htmlBlock);
		},
        url: '?r=localdriver/requests-list',
        success: function(data) {
        	$data = JSON.parse(data);
        	var selectore = $("#localdriver-requests").find('.offer-ul').find('.data-container').find('ul');
        	if($data.length != 0) {
        		selectore.html('');
        		$.each($data, function(i, v) {
        			selectore.append(v);
        		});
        	} else {
				getnolistfound('nolocaldriverrequestfound');
        	}
        	initDropdown();
        }
	});
}

/* open post offer */
function openOffer($id, $obj){
	if($id != undefined && $id != '') {
		$.ajax({
            type: 'POST',
			url: '?r=localdriver/invitation-detail',
            data: {$id},
            success: function(data) {
            	$data = JSON.parse(data);
            	if($data.status != undefined && $data.status == true) {
            		$search = $data.search;
            		$detail = $data.detail;
            		if($search != undefined && $detail != undefined) {
            			var fparent=$($obj).parents(".offer-ul");
						var sparent=$($obj).parents(".offer-tab");
						
						fparent.hide();
						sparent.find(".offer-details").show();

						$($obj).parents(".main-page").find(".side-area.main-search").hide();
						$($obj).parents(".main-page").find(".side-area.offer-profile").show();
						$('.offer-profile').html('');
	            		$('.offer-profile').html($search);
	            		$('.offer-details').html('');
	            		$('.offer-details').html($detail);
	            		fetchmsghistory($id);
	            	}
            	}
            }
		});
	}
	var win_w=$(window).width();	
	if(win_w<=800){
		$(".mbl-filter-icon.main-icon").hide();
		$(".mbl-filter-icon.offer-profile-icon").show();
	}
}

/* update post invitation read */
function invitationisread($id, obj) {
	if($id) {
		$.ajax({
		    type: 'POST',
			url: '?r=localdriver/invitation-isread',
		    data: {$id},
		    success: function(data) {
		    	if(data == true) {
		   			$(obj).addClass('read');
		   		}
		    }
		});
	}
}

/* fetch history */
function fetchmsghistory($id) {
	if($id) {
		$.ajax({
            type: 'POST', 
			url: '?r=localdriver/fetch-msg-history',
            data: {$id},
            success: function(data) {
            	$data = JSON.parse(data);
            	if($data.status != undefined && $data.status == true) {
            		$result = $data.data;  
            		$('.localdriver-page').find('.messagelisting').prepend($result);
            	}
            	setTimeout(function() { 
	    			initDropdown(); 
	    		},400);
            }
		});
	}
}

/* send message */
function sendmessagelocaldriver($id) {
	if($id) {
		message  = $('#localdriverdetailmessage').val().trim();
		if(message.length >0) {
			$.ajax({
	            type: 'POST',
				url: '?r=localdriver/send-message',
	            data: {
	            	message: message,
	            	invitationId: $id 
	            },
	            success: function(data) {
	           		$data = JSON.parse(data);
	           		if($data.status != undefined  && $data.status == true) {
	            		$('.localdriver-page').find('#localdriverdetailmessage').val('');
	           			if($data.status != undefined && $data.status == true) {
		           			$result = $data.data;
		           			$('.localdriver-page').find(".messagelisting").prepend($result);
	           			}
	           		} else if($data.status == false  && $data.reason != undefined) {
	           			alert('This person isn\'t receiving messages from you at the moment.');
	           		}
	           		setTimeout(function() { 
		    			initDropdown(); 
		    		},400);
	            }
			});
		}
	}
}

/* delete offer */
function deleteofferlocaldriver($id, obj, isPermission=false) {
	if($id != undefined || $id != '') {
		if(isPermission) {
			$.ajax({
	            type: 'POST',
				url: '?r=localdriver/delete-offer',
	            data: {$id},
	            success: function(data) {
	           		if(data) {
	           			$(".discard_md_modal").modal("close");
	           			if($('.offer-ul').find('.postrequest_'+$id).length) {
	           				$('.offer-ul').find('.postrequest_'+$id).remove();
	           				Materialize.toast('Request deleted.', 2000, 'green');
	           				if(!($('.offer-ul').find('ul.li').length)) {
	           					getnolistfound('nolocaldriverrequestfound');
	           				}
	           			}
	           		}
	            }
			});
		} else {
			$('.dropdown-button').dropdown("close");
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
	    	disText.html("Delete request.");
            btnKeep.html("Keep");
            btnDiscard.html("Delete");
            btnDiscard.attr('onclick', 'deleteofferlocaldriver(\''+$id+'\', this, true)');
            $(".discard_md_modal").modal("open");
		}
	}
}

function deletemessage($id, isPermission=false) {
	if($id != undefined || $id != '') {
		if(isPermission) {
			$.ajax({
	            type: 'POST',
				url: '?r=localdriver/delete-message',
	            data: {$id},
	            success: function(data) {
	           		if(data) {
	           			$(".discard_md_modal").modal("close");
	           			if($("#message_"+$id).length) {
	           				$("#message_"+$id).remove();
	           				Materialize.toast('Message deleted.', 2000, 'green');
	           			}
	           		}
	            }
			});
		} else {
			$('.dropdown-button').dropdown("close");
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
	    	disText.html("Delete message.");
            btnKeep.html("Keep");
            btnDiscard.html("Delete");
            btnDiscard.attr('onclick', 'deletemessage(\''+$id+'\', true)');
            $(".discard_md_modal").modal("open");
		}
	}
}
 
/* reset area */
function resetSearchArea() {
	$("#localdriversearchform")[0].reset();
	$('.range-slider1').find('.ui-slider-range').css({'left' : '0%', 'width' : '100%'});
	$('.range-slider1').find('.ui-slider-handle.ui-state-default:eq( 0 )').css('left', '0%');
	$('.range-slider1').find('.ui-slider-handle.ui-state-default:eq( 1 )').css('left', '100%');
	$('.range-slider1').find('.min-value').css('left', '0%');
	$('.range-slider1').find('.min-value').html('0');
	$('.range-slider1').find('.max-value').css('left', '100%');
	$('.range-slider1').find('.max-value').html('100');
}

/* get last message */
function getLstMsg($id) {
	if($id) {
		$.ajax({
			url: '?r=localdriver/get-lst-msg',
			type: 'POST',
			data: {$id},
			success: function(data) {
				$data = JSON.parse(data);
            	if($data.status != undefined && $data.status == true) {
            		var $message = $data.message;
            		var $is_read = $data.is_read;
            		var $self = $data.self;
        			$selectore = $('#localdriver-requests').find('.offer-ul').find('ul').find('li.postrequest_'+$id);
        			if($selectore.length) {
        				$selectore.find('.msg-holder').find('.msg-bbl').find('p').html($message);
        				if($self == false) {
            				if($is_read == true) {
            					$selectore.removeClass('read');
            					$selectore.addClass('read');
            				} else {
            					$selectore.removeClass('read');
            				}
        					$selectore.removeClass('sent');
            			} else {
            				if($is_read == true) {
            					$selectore.removeClass('read');
            					$selectore.addClass('read');
            				} else {
        						$selectore.removeClass('read');
            				}
            				$selectore.removeClass('sent');
            				$selectore.addClass('sent');
            			}
        			}
        		}
			}
		});
	}
}

function addLocaldriverReview() { 
	applypostloader('SHOW');
	var $select = $('#localdriver_review');

	var $w = $(window).width();
	var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
    var title = $select.find("#title").val();
    var status = $select.find('#textInput').val();
    
    if (status != "" && reg.test(status) == true) {
         status = status.replace(reg, " ");
    }
    if (title != "" && reg.test(title) == true) {
         title = title.replace(reg, " ");
    }

    var formdata;
    formdata = new FormData($('form')[1]);

    formdata.append("test", status);
    
    if($w > 568) {
    	$post_privacy = $select.find('#post_privacy').text().trim();
	} else {
		$post_privacy = $select.find('#post_privacy2').text().trim();
	}

    if($.inArray($post_privacy, $post_privacy_array) > -1) {
	} else {
		$post_privacy = 'Public';
	}
    var $share_setting = 'Enable';
    var $comment_setting = 'Enable';
    if($('#sharing_setting_btns').find('.toolbox_disable_sharing').is(':checked')) {
    	$share_setting = 'Disable';
    }
    if($('#sharing_setting_btns').find('.toolbox_disable_comments').is(':checked')) {
    	$comment_setting = 'Disable';
    }

    $link_title = $('#link_title').val();
    $link_url = $('#link_url').val();
    $link_description = $('#link_description').val();
    $link_image = $('#link_image').val();
   
   	var $totalStartFill = $select.find('.rating-stars').find('i.active').length;
	if($totalStartFill <=0) {
		Materialize.toast('Rate your self.', 2000, 'red');
		applypostloader('HIDE');
		return false;
	}

	$(".post_loadin_img").css("display","inline-block");
	
	formdata.append("posttags", addUserForTag);
	formdata.append("post_privacy", $post_privacy);
	formdata.append("link_title",$link_title);
	formdata.append("link_url",$link_url);
	formdata.append("link_description",$link_description);
	formdata.append("link_image",$link_image);
	formdata.append("title", title);
	formdata.append("share_setting",$share_setting);
	formdata.append("comment_setting",$comment_setting);
	formdata.append('custom', customArray);
	//formdata.append("current_location",place); 
	formdata.append("placereview", $totalStartFill);
	
	$.ajax({ 
		url: '?r=localdriver/addreview',
		type: 'POST',
		data:formdata,
		async:false,        
		processData: false,
		contentType: false,
		success: function(data) {
			//applypostloader('HIDE');
			$select.modal('close');
			if(data == 'checkuserauthclassnv') {
				checkuserauthclassnv();
			} else if(data == 'checkuserauthclassg') {
				checkuserauthclassg(); 
			} else {
				Materialize.toast('Published.', 2000, 'green');
				newct = 0;
				lastModified = [];
                storedFiles = []; 
                storedFilesExsting = [];
                storedFiles.length = 0;
                storedFilesExsting.length = 0;
				$(data).hide().prependTo(".collection").fadeIn(3000);
				setTimeout(function() { 
		            initDropdown();
		        }, 400);
			}	
		}
	}).done(function(){
		lightGalleryinitialize();
	   	//resizefixPostImages();
  	});
	return true;
}


function removepiclocaldriver_modal($id, obj)
{   
    $this = $(obj);
    var $src = $this.parents('.img-box').find('img').attr('src');
    if($id) {   
        $.ajax({
            type: 'POST', 
            url: '?r=localdriver/removepic',  
            data: {$id, $src},
            success: function(data) {
            	var result = $.parseJSON(data);
                if(result.success != undefined && result.success == true) {
                    Materialize.toast('Deleted', 2000, 'green');
                    $this.parents('.img-box').remove();

                    // Remove upload image input if 3 images is exists or uploaded....
                    if($('.modal.open').find('.upldimg').length >= 3) {
                        $('.modal.open').find('.custom-file').parents('.img-box').remove();
                    } else {
                        if(!$('.modal.open').find('.custom-file').length) {
                            $uploadBox = '<div class="img-box"> <div class="custom-file addimg-box add-photo ablum-add"> <span class="icont">+</span> <br><span class="">Update photo</span> <input class="upload custom-upload remove-custom-upload edit-collections-file-upload" id="edit-collections-file-upload" title="Choose a file to upload" data-class=".post-photos .img-row" type="file"> </div> </div>';
                            if($('.modal.open').find('.img-row').append($uploadBox));
                        }   
                    }
                }
            }
        });
    }
}

function manageSlidingPan(obj,which){
	var super_parent=$(obj).parents(".slidingpan-holder");
	if(super_parent.find(".sliding-pan."+which).hasClass("openSlide"))
		super_parent.find(".sliding-pan."+which).removeClass("openSlide");
	else		
		super_parent.find(".sliding-pan."+which).addClass("openSlide");		
}