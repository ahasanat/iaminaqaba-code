/* Start Function For Ad Start date and End Date calculation on Document Ready of Create ads Page */
	$(document).on('mouseover', '#languagedropdown ul.select-dropdown', function() {
		$(this).css('overflow-y', 'scroll');
	});

	$(document).on('mouseover', '#interestsdropdown ul.select-dropdown', function() {
		$(this).css('overflow-y', 'scroll');
	});

	$(document).on('mouseover', '#educationdropdown ul.select-dropdown', function() {
		$(this).css('overflow-y', 'scroll');
	});

	$(document).on('mouseover', '#occupationdropdown ul.select-dropdown', function() {
		$(this).css('overflow-y', 'scroll');
	});

	$(document).on('mouseover', '.enterpagelistingarea ul.select-dropdown', function() {
		$(this).css('overflow-y', 'scroll');
	});

	/*$(document).on('change', '.cropit-image-input', function(e) {
		$id = $(this).parents('.crop-holder').attr('id');
		if($id.length) {
			$('#'+$id).cropit({
		        height: 190,
		        width: 366,
		        smallImage: "allow",
		        onImageLoaded: function (x) {           
		        	$(".cropit-preview").removeClass("class-hide");
		            $(".cropit-preview").addClass("class-show");

		            $(".btn-save").removeClass("class-hide");
		            $(".btn-save").addClass("class-show");
		            $("#removeimg").removeClass("class-hide");
		            $("#removeimg").addClass("class-show");
		        }
		    });	
		}
		$(this).attr('id');
	});*/

	$(document).ready(function(){
		countrycodedropdown();
		interestsdropdown();
		occupationdropdown();
		
		$(document).on("click", ".image_save_btn",function () {
			$('#upload_img_box').modal('close');
	    });

		$('.image-cropperGeneral').cropit({
	        height: 188,
	        width: 355,
	        smallImage: "allow",
	        onImageLoaded: function (x) {
	            if($('.mainhead.active').length) {
	                var openBlockId = $('.mainhead.active').attr('id');
	                if(openBlockId) {
	                    openBlockId = openBlockId.replace('_block', '');
	                    $openPreviewBox = $('.'+openBlockId).find('.preview-part').find('.preview-accordion').find('li.active');   
	                    $openPreviewBox.find('.cropit-preview').removeClass("class-hide");
	                    $openPreviewBox.find('.cropit-preview').addClass("class-show");
	                    $openPreviewBox.find('.saveimg').removeClass("class-hide");
	                    $openPreviewBox.find('.saveimg').addClass("class-show");
	                    $openPreviewBox.find('.removeimg').removeClass("class-hide");
	                    $openPreviewBox.find('.removeimg').addClass("class-show");
	                }
	            }
	        }
	    });

	    $('.image-cropperGeneralRCDsk').cropit({
	        height: 188,
	        width: 278,
	        smallImage: "allow",
	        onImageLoaded: function (x) {
	            if($('.mainhead.active').length) {
	                var openBlockId = $('.mainhead.active').attr('id');
	                if(openBlockId) {
	                    openBlockId = openBlockId.replace('_block', '');
	                    $openPreviewBox = $('.'+openBlockId).find('.preview-part').find('.preview-accordion').find('li.active');   
	                    $openPreviewBox.find('.cropit-preview').removeClass("class-hide");
	                    $openPreviewBox.find('.cropit-preview').addClass("class-show");
	                    $openPreviewBox.find('.saveimg').removeClass("class-hide");
	                    $openPreviewBox.find('.saveimg').addClass("class-show");
	                    $openPreviewBox.find('.removeimg').removeClass("class-hide");
	                    $openPreviewBox.find('.removeimg').addClass("class-show");
	                }
	            }
	        }
	    });

	    $('.image-cropperGeneralMbl').cropit({
	        height: 188,
	        width: 318,
	        smallImage: "allow",
	        onImageLoaded: function (x) {
	            if($('.mainhead.active').length) {
	                var openBlockId = $('.mainhead.active').attr('id');
	                if(openBlockId) {
	                    openBlockId = openBlockId.replace('_block', '');
	                    $openPreviewBox = $('.'+openBlockId).find('.preview-part').find('.preview-accordion').find('li.active');   
	                    $openPreviewBox.find('.cropit-preview').removeClass("class-hide");
	                    $openPreviewBox.find('.cropit-preview').addClass("class-show");
	                    $openPreviewBox.find('.saveimg').removeClass("class-hide");
	                    $openPreviewBox.find('.saveimg').addClass("class-show");
	                    $openPreviewBox.find('.removeimg').removeClass("class-hide");
	                    $openPreviewBox.find('.removeimg').addClass("class-show");
	                }
	            }
	        }
	    });

	    $('.image-cropperGeneralDsk').cropit({
	        height: 188,
	        width: 400,
	        smallImage: "allow",
	        onImageLoaded: function (x) {
	            if($('.mainhead.active').length) {
	                var openBlockId = $('.mainhead.active').attr('id');
	                if(openBlockId) {
	                    openBlockId = openBlockId.replace('_block', '');
	                    $openPreviewBox = $('.'+openBlockId).find('.preview-part').find('.preview-accordion').find('li.active');   
	                    $openPreviewBox.find('.cropit-preview').removeClass("class-hide");
	                    $openPreviewBox.find('.cropit-preview').addClass("class-show");
	                    $openPreviewBox.find('.saveimg').removeClass("class-hide");
	                    $openPreviewBox.find('.saveimg').addClass("class-show");
	                    $openPreviewBox.find('.removeimg').removeClass("class-hide");
	                    $openPreviewBox.find('.removeimg').addClass("class-show");
	                }
	            }
	        }
	    });
		
		$('#image-cropper.mian-crop1').cropit({
	        height: 190,
	        width: 366,
	        smallImage: "allow",
	        onImageLoaded: function (x) {           
	        	$id = $('.travad-accordion').find('h3.active').attr('id'); 
	        	$id = $id.replace('_block', '');

				$src = $('.travad-detailbox.'+$id).find('.cropit-preview-image').attr('src');
	        	$('.travad-detailbox.'+$id).find('.'+$id).attr('src', $src);
	        	
	            $(".cropit-preview").removeClass("class-hide");
	            $(".cropit-preview").addClass("class-show");
	            $(".btn-save").removeClass("class-hide");
	            $(".btn-save").addClass("class-show");
	            $(".removeimg").removeClass("class-hide");
	            $(".removeimg").addClass("class-show");
	        }
	    });

	    $('#image-cropper').cropit({
	        height: 188,
	        width: 355,
	        smallImage: "allow",
	        onImageLoaded: function (x) {
	            
	            $(".cropit-preview").removeClass("class-hide");
	            $(".cropit-preview").addClass("class-show");

	            $(".btn-save").removeClass("class-hide");
	            $(".btn-save").addClass("class-show");
	            $(".removeimg").removeClass("class-hide");
	            $(".removeimg").addClass("class-show");
	        }
	    });
	    
		$('.change_head').keyup(function() {
		    $('.travad-title').html($(this).val());
		});  
		$('.sub_title_you_ad').keyup(function() {
		    $('.travad-subtitle').html($(this).val());
		}); 
		$('.catch_phase').keyup(function() {
		    $('.change_ad_word').html($(this).val());
		});   
		$('.aware_head').keyup(function() {
		    $('.change_awar_head').html($(this).val());
		});  
		$('.aware_text').keyup(function() {
		    $('.change_awar_text').html($(this).val());
		}); 
		$('.advert_title').keyup(function() {
		    $('.lead_title').html($(this).val());
		}); 
		$('.advert_phrase').keyup(function() {
		    $('.lead_phrase').html($(this).val());
		}); 
		$('.advert_head').keyup(function() {
		    $('.lead_head').html($(this).val());
		}); 
		$('.advert_text').keyup(function() {
		    $('.lead_text').html($(this).val());
		}); 
		$('.advert_url').keyup(function() {
		    $('.lead_url').html($(this).val());
		}); 
		$('.conversion_title').keyup(function() {
		    $('.con_create_title').html($(this).val());
		}); 
		$('.conversion_phrase').keyup(function() {
		    $('.con_create_phrase').html($(this).val());
		}); 
		$('.conversion_head').keyup(function() {
		    $('.con_create_head').html($(this).val());
		}); 
		$('.conversion_text').keyup(function() {
		    $('.con_create_text').html($(this).val());
		});
		$('.inbox_title').keyup(function() {
		    $('.inbox_create_title').html($(this).val());
		});
		$('.inbox_main_catch').keyup(function() {
		    $('.inbox_create_main').html($(this).val());
		});
		$('.inbox_sub_catch').keyup(function() {
		    $('.inbox_create_sub').html($(this).val());
		});
		$('.inbox_head').keyup(function() {
		    $('.inbox_create_head').html($(this).val());
		});
		$('.inbox_text').keyup(function() {
		    $('.inbox_create_text').html($(this).val());
		});
		$('.endorsement_phrase').keyup(function() {
		    $('.endorsement_create_phrase').html($(this).val());
		});
		$('.endorsement_head').keyup(function() {
		    $('.endorsement_create_head').html($(this).val());
		});
		$('.endorsement_text').keyup(function() {
		    $('.endorsement_create_text').html($(this).val());
		});

		$(".pos_rel .image_save_btn.image_save").click(function(){
			$(".pos_rel .collection_image_trash.image_trash").removeClass("class-show");
		});

		setTimeout(function(){ 
			$('.audience-form').find('li.disabled').find('label').remove();
		},500);

		$(document).on('change', '.pagenamechange', function() {
			$obj = $(this);
			$selected_id = $obj.val();
			if($selected_id) {
				$page_name = $obj.find('option:selected').text();
				$obj.parents('.travad-detailbox').find('.pagelikenames').html($page_name);
				$.ajax({
					type: 'POST',
					url: '?r=ads/getpagelogo',
					data: {
						id: $selected_id
					},
					success: function(data)
					{
						var result = $.parseJSON(data);
						if(result.status != undefined && result.status == true) {
							$src = result.src;
							$obj.parents('.travad-detailbox').find('.pagelikesimage').attr("src", $src);
						}
					}
				});
			}
		});

		var slider = document.getElementById('test-slider');
		noUiSlider.create(slider, {
		   start: [0, 100],
		   connect: true,
		   step: 1,
		   orientation: 'horizontal', // 'horizontal' or 'vertical'
		   range: {
		     'min': 0,
		     'max': 100
		   },
		   format: wNumb({
		     decimals: 0
		   })
		});

		slider.noUiSlider.on('change', function(){
		  	change_audience_guage();
		});  
		  
		var slider1 = document.getElementById('test-slider1');
		noUiSlider.create(slider1, {
		   start: [0, 100],
		   connect: true,
		   step: 1,
		   orientation: 'horizontal', // 'horizontal' or 'vertical'
		   range: {
		     'min': 0,
		     'max': 100
		   },
		   format: wNumb({
		     decimals: 0
		   })
		});

		var select = function() {
		var d1 = $('#startdate').datepicker('getDate');
		var d2 = $('#enddate').datepicker('getDate');
		var diff = 0;
		if (d1 && d2) {
		  diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
		}
		$('#calculated').val(diff);
	  }
		$('.simple-tooltip').tooltipster({
			contentAsHTML: true,
			trigger:'click',
			theme: ['tooltipster-borderless']
		});

		$(".range-slider2").find(".slider-range").slider({
		  change: function() {
		  	var minage = $(".range-slider2").find('.noUi-handle-lower').find('.noUi-tooltip').find('span').html().trim() * 1;
			var maxage = $(".range-slider2").find('.noUi-handle-upper').find('.noUi-tooltip').find('span').html().trim() * 1;
			if(minage<0 || minage > 100){
				minage  = 0;
			}
			if(maxage<0 || maxage >100){
				maxage  = 100;
			}
			change_audience_guage();
		  }
		});

	});
/* End Function For Ad Start date and End Date calculation on Document Ready of Create ads Page */

/* Start Function Of Radio Button Selection  for Audiance */
	$('input.All').on('change', function() {
		$('input.Male').not(this).prop('checked', false);  
		$('input.Female').not(this).prop('checked', false);
		change_audience();
	});

	$('input.Male').on('change', function() {
		$('input.All').not(this).prop('checked', false); 
		$('input.Female').not(this).prop('checked', false);
		change_audience();
	});

	$('input.Female').on('change', function() {
		$('input.All').not(this).prop('checked', false); 
		 $('input.Male').not(this).prop('checked', false);  
		change_audience();
	}); 

	function countrycodedropdown() { 
		if(!$('#countrycodedropdown').hasClass('langloaded')) {
			$.ajax({
				type: 'GET',
				url: '?r=userwall/fetchlocations',
				success: function(data) {
					var result = $.parseJSON(data);

					$.each(result, function(i, v) {
						$('#ads_loc').append($("<option></option>") .attr("value", v) .text(v)); 
					});
					$('select').material_select();  
					$('#countrycodedropdown').addClass('langloaded');
					$('#countrycodedropdown').find('ul.select-dropdown').css('overflow-y', 'scroll');
					$('.disabled').find('input').remove();
				}
			});
		}
	}

	function interestsdropdown() { 
		if(!$('#interestsdropdown').hasClass('langloaded')) {
			$.ajax({
				type: 'GET',
				url: '?r=userwall/fetchinterests',
				success: function(data) {
					var result = $.parseJSON(data);

					$.each(result, function(i, v) {
						$('#ads_int').append($("<option></option>") .attr("value", v) .text(v)); 
					});
					$('select').material_select();  
					$('#interestsdropdown').addClass('langloaded');
					$('#interestsdropdown').find('ul.select-dropdown').css('overflow-y', 'scroll');
					$('.disabled').find('input').remove();

				}
			});
		}
	}

	function occupationdropdown() {
		if(!$('#occupationdropdown').hasClass('langloaded')) {
			$.ajax({
				type: 'GET',
				url: '?r=userwall/fetchoccupation',
				success: function(data) {
					var result = $.parseJSON(data);

					$.each(result, function(i, v) {
						$('#ads_pro').append($("<option></option>") .attr("value", v) .text(v)); 
					});
					$('select').material_select();  
					$('#occupationdropdown').addClass('langloaded');
					$('#occupationdropdown').find('ul.select-dropdown').css('overflow-y', 'scroll');
					$('.disabled').find('input').remove();
				}
			});
		}
	}
	
	function main_change_button(x) {    
    	$('.con_create_button').text(x);
	}      
	function main_inbox_button(y) {    
	    $('.inbox_create_button').text(y);
	}    
	function pageendorse_change(z) {    
	    $('.endorsement_create_value').text(z);
	}
	function main_title_change() {
	    var x = document.getElementById("mySelect").selectedIndex;
	    $('.head_chtitle').text(document.getElementsByTagName("option")[x].value);
	}   

	/* End Function Of Radio Button Selection  for Audiance */
	function change_audience()
	{
		change_audience_guage();
	}

	function change_gender()
	{
		change_audience_guage();
	}
	/* Start Function For Filtering Audiance As User Selects Relavent Filter Options */
	function change_audience_guage()
	{
		var genderArray = ['Male', 'Female', 'All'];
		var location = []; 
		$('#ads_loc :selected').each(function(i, selected){ 
		  if($(selected).val().trim() != '') {
			  location.push($(selected).text()); 
			}
		});

		var language = [];  
		$('#ads_lang :selected').each(function(i, selected){ 
			if($(selected).val().trim() != '') {
		  		language.push($(selected).text()); 
			}
		});

		var minage = $('.noUi-handle-lower').find('.noUi-tooltip').find('span').html().trim() * 1;
		var maxage = $('.noUi-handle-upper').find('.noUi-tooltip').find('span').html().trim() * 1;
		if(minage<0 || minage > 100){
			minage  = 0;
		}
		if(maxage<0 || maxage >100){
			maxage  = 100;
		}

		var gender = $('input[name=adGender]:checked').val();
        if($.inArray(gender, genderArray) !== -1) {
        } else {
        	Materialize.toast('Select gender', 2000, 'red');
        }

		var proficient  = []; 
		$('#ads_pro :selected').each(function(i, selected){ 
			if($(selected).val().trim() != '') {
		  		proficient.push($(selected).text()); 
		  	}
		});

		var interest = []; 
		$('#ads_int :selected').each(function(i, selected){ 
			if($(selected).val().trim() != '') {
			  interest.push($(selected).text());
			}
		});

		var formdata;
		formdata = new FormData();
		formdata.append("location", location);
		formdata.append("minage", minage);
		formdata.append("maxage", maxage);
		formdata.append("language", language);
		formdata.append("gender", gender);
		formdata.append("proficient", proficient);
		formdata.append("interest", interest);

		$.ajax({
			url: '?r=ads/checkaudience', 
			type: 'POST', 
			data: formdata,
			processData: false,
			contentType: false,
			success: function (data)
			{
				var result = $.parseJSON(data);
				var data = result['meter'];
				if(data<0) { data  = 0; }
				if(data >100) {data  = 100; }
				gauge = new Gauge($('.gauge2'), {
				values: {
					0 : 'Specific',
					20: 'Good',
					100: 'Broad'
				},
				colors: {
					0 : '#ED1C24',
					20 : '#39B54A',
					80: '#FDD914'
				},
				angles: [
					180,
					360
				],
				lineWidth: 20,
				arrowWidth: 10,
				arrowColor: '#ccc',
				inset:true,
				value: data});
				var datacount = result['count'];
				var slider = document.getElementById('test-slider1');
				slider.noUiSlider.set([0, datacount]);
				$("#tgt_aud_cnt").html(datacount);
				$('#eta_location').html(result['locationLabel']);
				$('#eta_age').html(minage+' - '+maxage);
				$('#eta_language').html(result['languageLabel']);
				$('#eta_gender').html(gender);

				$(".fixmin-slider").find(".slider-range").slider({
					range: "min",
					value: datacount,
					min: 0,
					max: datacount,
					slide: function( event, ui ) {
						$( ".max-amount" ).html( ui.value + " people on Iaminaqaba");
					}
				});
			}
		});
	}
	/* End Function For Filtering Audiance As User Selects Relavent Filter Options */
	
	/* Start  Function For Ad calculation min budget */
	function ad_summary()
	{
			var min_budget = $('#min_budget').val();
			var advert_name = $('#advert_name').val();
			var ads_amount = $('#ads_amount').val();
			$("#ad_name").html(advert_name);
			$("#daily_budget").html('$'+min_budget);
			$(".ads_amount").val(min_budget);
	}
	/* End Function For Ad calculation min budget */

	/* Start  Function For Payment On Paypal For New Ad */
		function buy_ads()
		{
			$("#myformsubmit").submit();
		}
	/* End Function For Payment On Paypal For New Ad */

/* Start Function For Manual Publish if User is Not Liable To Pay and Has Already Enough Credits For New ad */
		function buy_ads2()
		{
			var dif_amount = $("#dif_amount").val();
			$.ajax({
			type: 'POST',
			url: '?r=ads/manualpublish',
			data: "dif_amount="+dif_amount+"&benifit_amount="+benifit_amount,
			success: function(data)
			{
				window.location.href = "?r=ads/manage";
			}
			});
		}
/* End Function For Manual Publish if User is Not Liable To Pay and Has Already Enough Credits For New ad */

/* Start Function For Ads Payment Via Card */
	$( "#paymentForm" ).submit(function( event ) {
		var min_budget = $('#min_budget').val();
		$('#ads_amount').val(min_budget);
		if(ads_amount == ''){
				Materialize.toast('Select the credit plan first.', 2000, 'red');
				return false;
			}
			else
			{
				var $is_stop = true;
				
				var name_on_card = $('#name_on_card').val();
				var card_number = $('#card_number').val();
				var expiry_month = $('#expiry_month').val();
				var expiry_year = $('#expiry_year').val();
				var cvv = $('#cvv').val();
				var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
				var password = $('#password').val();
				var reg_nm = /^[a-zA-Z\s]+$/;
				var spc = /^\s+$/;	
				var one_space = /^[A-Za-z]+\s+[A-Za-z]+$/;
				
				if(name_on_card == '')
				{
					Materialize.toast('Enter full name of the card holder.', 2000, 'red');
					$("#name_on_card").focus();
					$is_stop = false;
				}
				else if(!one_space.test(name_on_card))
				{
					Materialize.toast('Enter first and last name of the card holder.', 2000, 'red');
					$("#name_on_card").focus();
					$is_stop = false;
				}
				else if(card_number == '')
				{
					Materialize.toast('Enter card number.', 2000, 'red');
					$("#card_number").focus();
					$is_stop = false;
				}
				else if(expiry_month == '')
				{
					Materialize.toast('Enter expiry month.', 2000, 'red');
					$("#expiry_month").focus();
					$is_stop = false;
				}
				else if(expiry_year == '')
				{
					Materialize.toast('Enter expiry year.', 2000, 'red');
					$("#expiry_year").focus();
					$is_stop = false;
				}
				else if(cvv == '')
				{
					Materialize.toast('Enter CVV number.', 2000, 'red');
					$("#cvv").focus();
					$is_stop = false;
				}
				
				else
				{
					$is_stop = true;
				}	 
			}
			
			if($is_stop == true)
			{
				$('#cardSubmitBtn').attr("disabled",true);
			}
			return $is_stop;
	});
/* End Function For Ads Payment Via Card */

function AudianceExTones()
{
	var location = []; 
	$('#ads_loc :selected').each(function(i, selected){ 
	  location.push($(selected).text()); 
	});

	var language = []; 
	$('#ads_lang :selected').each(function(i, selected){ 
	  language.push($(selected).text()); 
	});

	var minage = $("#minage").html();
	var maxage = $("#maxage").html();
	if(minage<0 || minage > 100)
	{
		minage  = 0;
	}
	if(maxage<0 || maxage >100)
	{
		maxage  = 100;
	}

	var gender = [];
	$("input:checkbox[name=gender]:checked").each(function(){
		var fixGender = ['Male', 'Female'];

		var genval = $(this).val();
		if($.inArray(genval, fixGender) > -1) {
			gender.push(genval);
		}
	});

	var proficient  = []; 
	$('#ads_pro :selected').each(function(i, selected){ 
	  proficient.push($(selected).text()); 
	});

	var interest = []; 
	$('#ads_int :selected').each(function(i, selected){ 
	  interest.push($(selected).text());
	});

	var formdata;
	formdata = new FormData();
	formdata.append("location", location);
	formdata.append("minage", minage);
	formdata.append("maxage", maxage);
	formdata.append("language", language);
	formdata.append("gender", gender);
	formdata.append("proficient", proficient);
	formdata.append("interest", interest);
}

function closeMessage(obj){		
	var isMblMessagePage;
	var isBusinessPage=$(".page-wrapper").hasClass("businesspage");
	if(isBusinessPage){
		isMblMessageTab=$(".messages-content").hasClass("mblMessagePage");
	}else{
		isMblMessagePage=$(".main-content").hasClass("mblMessagePage");
	}
	
	if(isMblMessagePage){
		$(".messages-list").find(".right-section").addClass("hidden");
		if($(".messages-list").find(".left-section").hasClass("hidden")){
			$(".messages-list").find(".left-section").removeClass("hidden");
			$(".messages-list").find(".left-section").addClass("shown");
			
			$('.page-name').html('Home');
			$('.gotohome').find('a').html('<i class="mdi mdi-menu"></i>');
			$('.gotohome').find('a').removeAttr('onclick');
			$('.gotohome').find('a').attr('href', 'messages.html');
			
			if(!isBusinessPage)
				$('.mobile-footer').show();
		}			
	}		
}

$('body').on( "focusout","#min_budget",function (e) {
		var a = $(this).val();
		var b = parseFloat(a).toFixed(2);
		if(b != 'NaN')
		{
			$('#min_budget').val(b);
		}
		else
		{
			$('#min_budget').val('');
		}
	 	
	});