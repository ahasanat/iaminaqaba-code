var data1 = '';
/* Start Function For Check User is Vip Member or Not */			
$( "#myformsubmit" ).submit(function( event ) {
	var selected_vip_plan = $('#selected_vip_plan').val();
		if(selected_vip_plan == ''){
			return false;
		}
		else
		{
			var $is_stop = true;
			$.ajax({
				url: '?r=vip/checkvip', 
				type: 'POST',
				async: false,
				data: 'selected_vip_plan=' + selected_vip_plan,
				success: function (data) {
					
					if(data == '1'){
						$is_stop = true;
					}
					else if(data == '2'){
						$is_stop = false;
					}
					else{
						$is_stop = false;
					}	
				}
			});
			
			return $is_stop;
		}
});
/* End Function For Check User is Vip Member or Not */

/* Start Function For Become a VIP Member Payment Via Card */

$( "#paymentForm" ).submit(function( event ) {
	var selected_vip_plan = $('#selected_vip_plan').val();
		if(selected_vip_plan == ''){
			Materialize.toast('Select the VIP plan first.', 2000, 'red');
			return false;
		}
		else
		{
			var $is_stop = true;
			$.ajax({
				url: '?r=vip/checkvip', 
				type: 'POST',
				async: false,
				data: 'selected_vip_plan=' + selected_vip_plan,
				success: function (data) {
					if(data == '1'){
						
						var name_on_card = $('#name_on_card').val();
						var card_number = $('#card_number').val();
						var expiry_month = $('#expiry_month').val();
						var expiry_year = $('#expiry_year').val();
						var cvv = $('#cvv').val();
						var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
						var password = $('#password').val();
						var reg_nm = /^[a-zA-Z\s]+$/;
						var spc = /^\s+$/;	
						var one_space = /^[A-Za-z]+\s+[A-Za-z]+$/;
						
						if(name_on_card == '')
						{
							Materialize.toast('Enter full name of the card holder.', 2000, 'red');
							$("#name_on_card").focus();
							$is_stop = false;
						}
						else if(!one_space.test(name_on_card))
						{
							Materialize.toast('Enter first and last name of the card holder.', 2000, 'red');
							$("#name_on_card").focus();
							$is_stop = false;
						}
						else if(card_number == '')
						{
							Materialize.toast('Enter card number.', 2000, 'red');
							$("#card_number").focus();
							$is_stop = false;
						}
						else if(expiry_month == '')
						{
							Materialize.toast('Enter expiry month.', 2000, 'red');
							$("#expiry_month").focus();
							$is_stop = false;
						}
						else if(expiry_year == '')
						{
							Materialize.toast('Enter expiry year.', 2000, 'red');
							$("#expiry_year").focus();
							$is_stop = false;
						}
						else if(cvv == '')
						{
							Materialize.toast('Enter CVV number.', 2000, 'red');
							$("#cvv").focus();
							$is_stop = false;
						}
					}
					else if(data == '2'){
						$is_stop = false;
					}
					else{
						$is_stop = false;
					}	
				}
			});
			
			if($is_stop == true)
			{
				$('#cardSubmitBtn').attr("disabled",true);
			}
			return $is_stop;
		}
});

/* End Function For Become a VIP Member Payment Via Card */