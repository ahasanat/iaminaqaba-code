var $page_restricted_list = [];
var $page_block_list = [];
var $page_block_message_list = [];

var $ispage_restricted_list = 'no';
var $ispage_block_list = 'no';
var $ispage_block_message_list = 'no';

$(window).resize(function() {
	var winw = $(window).width();

	if(winw <= 767) {
		if($('#pagesettings-general').hasClass('active')) {
			$('#pagesettings-general .editicon1').css('display', 'none');
			$('#pagesettings-general .editicon2').css('display', 'block');
		}

		if($('#pagesettings-blocking').hasClass('active')) {
			$('#pagesettings-blocking .editicon1').css('display', 'none');
			$('#pagesettings-blocking .editicon2').css('display', 'block');
		}
	} else {
		if($('#pagesettings-general').hasClass('active')) {
			$('#pagesettings-general .editicon1').css('display', 'block');
			$('#pagesettings-general .editicon2').css('display', 'none');
		}

		if($('#pagesettings-blocking').hasClass('active')) {
			$('#pagesettings-blocking .editicon1').css('display', 'block');
			$('#pagesettings-blocking .editicon2').css('display', 'none');
		}
	}
});

$(document).on('click', '.page_restricted_list, .page_block_list, .page_block_message_list', function() {
	if($(this).hasClass('page_restricted_list')) {
		addUserForAccountSettingsArrayTemp = $page_restricted_list;
		addUserForAccountSettingsArray = $page_restricted_list;

		$ispage_restricted_list = 'yes';
		$ispage_block_list = 'no';
		$ispage_block_message_list = 'no';

		whoisopeninblock = 'page_restricted_list';
	}

	if($(this).hasClass('page_block_list')) {
		addUserForAccountSettingsArrayTemp = $page_block_list;
		addUserForAccountSettingsArray = $page_block_list;

		$ispage_restricted_list = 'no';
		$ispage_block_list = 'yes';
		$ispage_block_message_list = 'no';

		whoisopeninblock = 'page_block_list';
	}

	if($(this).hasClass('page_block_message_list')) {
		addUserForAccountSettingsArrayTemp = $page_block_message_list;
		addUserForAccountSettingsArray = $page_block_message_list;

		$ispage_restricted_list = 'no';
		$ispage_block_list = 'no';
		$ispage_block_message_list = 'yes';

		whoisopeninblock = 'page_block_message_list';
	}
});

function pageblockingsomeparams(){
	$.ajax({
		url: '?r=page/pageblockingsomeparams', 
		success: function (data) {
			var result = $.parseJSON(data);
			
			$page_restricted_list = result.page_restricted_list;
			$page_block_list = result.page_block_list;
			$page_block_message_list = result.page_block_message_list;
		}
	});
}


/************ START Design Code ************/
	function zhijsdxhixsl() {
		var winw = $(window).width();
		$('#pc-albums .grid-box').each(function() {
			if($(this).find('.descholder').length) {
				if($(this).find('.descholder').find('ul').length) {
					$selectore = $(this).find('.descholder');
					$a = $(this).find('.descholder').find('a.dropdown-button.more_btn');
					$ul = $(this).find('.descholder').find('ul.dropdown-content.custom_dropdown');
					if(winw <= 480) {
						$a.attr('onclick', 'privacymodal(this)');
						$a.attr('data-activates', '');
						$ul.addClass('forcefullyhide');
					} else {
						$data_activities = $ul.attr('id');
						$a.attr('data-activates', $data_activities);
						$a.attr('onclick', '');
						$ul.removeClass('forcefullyhide');
						$ul.hide();

					}
				}
			}
		});

		$('.dropdown-button').dropdown();
	}

/* FUN edit/normal mode settings page */
	function close_all_edit(){
	   var count=0;
	   $(".settings-ul .settings-group").each(function(){
			
		   var editmode=$(this).children(".edit-mode").css("display");		
		
		   if(editmode!="none"){
				
				$(this).children(".normal-mode").slideDown(300);
				$(this).children(".edit-mode").slideUp(300);
			}
	   });
	}

	function open_edit(obj){
		addUserForAccountSettingsArray = [];
		addUserForAccountSettingsArrayTemp = [];
		close_all_edit();

		var obj=$(obj).parents(".settings-group");
				
		var editmode=obj.children(".edit-mode").css("display");

		if(editmode=="none"){
			obj.children(".normal-mode").slideUp(300);
			obj.children(".edit-mode").slideDown(300); 
			obj.children(".edit-mode").css('overflow', 'visible'); 
		}
		else{
			obj.children(".normal-mode").slideDown(300);
			obj.children(".edit-mode").slideUp(300);
		}
		
		initDropdown();
		/*setTimeout(function(){
			setGeneralThings();
		},400);*/
	}
	function close_edit(obj) {
		if($(obj).parents('li').find('.getvalue').length) {
		   var name = $(obj).data('nm');
		   if(name != undefined && name != '') {
			var lispan = $(obj).parents('li').find('.getvalue').html().trim();
			$('.'+name+'li .edit-mode #'+name).val(lispan);
			setting(); 
			$('.'+name+'li .normal-mode .row .security-setting.'+name+'display').html(lispan);
		   }
		  }


		var objParent=$(obj).parents(".settings-group");
		var emode=objParent.children(".edit-mode");
		var nmode=objParent.children(".normal-mode");
		var editmode=emode.css("display");

		if(editmode=="none") {
		nmode.slideUp(300);
		emode.slideDown(300);
		} else {
		nmode.slideDown(300);
		emode.slideUp(300);
		}
	}   
/* FUN end edit/normal mode settings page */
	
/* FUN manage settings section of business page */
	function getParameterByName(name) {
	    var url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}
	
	function hideTabContents(){		             
		if($(".page-wrapper").hasClass("businesspage")){
			$(".main-tabs .tabs > li").each(function(){
				if($(this).hasClass("active")) $(this).removeClass("active");
			});
			$(".sub-tabs .tabs > li").each(function(){
				if($(this).hasClass("active")) $(this).removeClass("active");
			});
			$(".main-tab-content .outer-tab").each(function(){
				if($(this).hasClass("active in")) $(this).removeClass("active in");
			});
		}
	}   
	function closePageSettings(){
		if($(".page-wrapper").hasClass("businesspage")){			
			$("#wall-content").addClass("active in");
			$(".main-tabs .tabs > li").each(function(){
				if($(this).find("a").html()=="Wall"){
					$(this).addClass("active");
				}
			});
			$("#settings-content").removeClass("active in");
		}
	}          
	function openPageSettings(){
		//closeAboutSection();
		if($(".page-wrapper").hasClass("businesspage")){                
			hideTabContents();
			$("#wall-content").removeClass("active in");
			$("#wall-content").css("display", "none");
			$("#settings-content").addClass("active in");			
			$("#settings-content").css("display", "block");			
		}
	}	     
/* FUN end manage settings section of business page */

/* FUN manaage info text - business page settings : page role */
	function resetInfoText(obj,what){
		var msg="";
		if(what=="Editor"){msg="Can edit the page, send messages and publish as the page, create ads, see which admin created a post or comment, and view insights.";}
		if(what=="Supporter"){msg="Can moderate the page, send messages and publish as the page, create ads, see which admin created a post or comment, and view insights.";}
		if(what=="Admin"){msg="Can admin the page, send messages and publish as the page, create ads, see which admin created a post or comment, and view insights.";}
		$("#roletype").val(what);
		$(".infotext").html(msg);
	}	
/* FUN end manaage info text - business page settings : page role */

/* FUN remove page role - business page settings : page role */
	function removePageRole(obj,id){
		$.ajax({
			type: 'POST',
			url: '?r=page/deletepagerole',
			data: 'id='+id,
			success: function(data){
				var result = $.parseJSON(data);
				if(result['msg'] == 'success')
				{ 
					Materialize.toast(result['status'], 2000, 'green');
					$(obj).parents(".pagerole-box").remove();
				}
				if(result['msg'] == 'fail')
				{
					Materialize.toast(result['status'], 2000, 'red');
					return false;
				}
		   }
		});
	}
/* FUN end remove page role - business page settings : page role */
  
/* FUN add page role - business page settings : page role */
	function resetAddRoleBox(){		
		$(".addrole-name").val("");
		$(".chooseRole").html('Admin <span class="caret"></span>');		
	}
	function addPageRole(){
		var whichRole = $("#roletype").val();
		var roleId = $("#roleid").val();
		if(whichRole != '' && roleId != '')
		{
			var formdata;
			formdata = new FormData();
			formdata.append("whichRole", whichRole);
			formdata.append("roleId", roleId);
			$.ajax({
				type: 'POST',
				url: '?r=page/addpagerole',
				data: formdata,
				processData: false,
				contentType: false,
				success: function(data){
					var result = $.parseJSON(data);
					if(result['msg'] == 'success')
					{
						var parentDiv="";
						if(whichRole=="Admin")
						{
							parentDiv="adminrole";
						}
						else if(whichRole=="Editor")
						{
							parentDiv="editorrole";
						}
						else
						{
							parentDiv="supporterrole";
						}
						Materialize.toast(result['status'], 2000, 'green');
						$(".pagerole-holder."+parentDiv).append(result['content']);
					}
					if(result['msg'] == 'exist')
					{
						Materialize.toast(result['status'], 2000, 'green');
						return false;
					}
					if(result['msg'] == 'fail')
					{
						Materialize.toast(result['status'], 2000, 'red');
						return false;
					}
			   }
			});
		}
	}
/* FUN end add page role - business page settings : page role */

/* FUN hide post from review list */
	function hideReviewPost(obj,status,pid){
		if(pid){
			// code for adding the post to main list
			$.ajax({
				url: '?r=page/reviewdpost', 
				type: 'POST',
				data: 'pid='+pid+'&status='+status,
				success: function (data)
				{
					$(obj).parents(".mainli").remove();
				}
			})
		}
	}

	function aprvReviewPhoto(obj, imgNm){
		var aprvid = $(obj).parents('.countgrid-box').attr('id');
		aprvid = aprvid.replace('reviewphoto_','');

		if(aprvid) {
			$.ajax({
				url: '?r=page/aprvreviewphoto', 
				type: 'POST',
				data: {aprvid, imgNm},
				success: function (data) {
					if(data) {
						$(obj).parents('.countgrid-box').remove();
						$isBox = $('#pagesettings-reviewphotos').find('.grid-box.countgrid-box').length;

						if($isBox == 0) {
							$block = '<div class="no-listcontent">No photos exist for review now.</div>';
							$('#pagesettings-reviewphotos').find('.row').html($block);
						} else {
							Materialize.toast('Approved.', 2000, 'green');
						}
					}
				}
			})
		}
	}

	function rjctReviewPhoto(obj, imgNm){
		var rjctid = $(obj).parents('.countgrid-box').attr('id');
		rjctid = rjctid.replace('reviewphoto_','');

		if(rjctid) {
			$.ajax({
				url: '?r=page/rjctreviewphoto', 
				type: 'POST',
				data: {rjctid, imgNm},
				success: function (data) {
					if(data) {
						$(obj).parents('.countgrid-box').remove();

						$isBox = $('#pagesettings-reviewphotos').find('.grid-box.countgrid-box').length;

						if($isBox == 0) {
							$block = '<div class="no-listcontent">No photos exist for review now.</div>';
							$('#pagesettings-reviewphotos').find('.row').html($block);
						} else {
							Materialize.toast('Rejected.', 2000, 'green');
						}
					}
				}
			})
		}
	}
/* FUN end hide post from review list */

/* FUN test business button - business page */
	function resetAdditionalInfo(){
		$(".additional-info").find(".info").hide();
	}
	function testBusinessButton(){
		if($(".hidden-businessbtn").find(".simple-tooltip").length>0){
			$(".simple-tooltip").trigger("click");
		}
		if($(".hidden-businessbtn").find(".simple-link").length>0){
			var href = $('.simple-link').attr('href');
			window.open(href, '_blank');
		}
	}
	function setAdditionalInfo(){
		
		resetAdditionalInfo();
		var getType=$("#businessbtn-type").val();
		if(getType=="Call Now" || getType=="Contact Us"){
			$(".additional-info").find(".info.phone-info").show();
		}
		else if(getType=="Send Email" || getType=="Send Message"){			
			$(".additional-info").find(".info.mail-info").show();
		}
		else{
			$(".additional-info").find(".info.url-info").show();
		}
	}
	function deleteBusinessButton(){
		$.ajax({
		url: '?r=page/deletebsnesbtn', 
		type: 'POST',
		success: function (data) {
			if(data)
			{
				$(".hidden-businessbtn").html("");
				$(".businessbtn-caption").html("Add action button");
				$(".businessbtn-caption").parents(".dropdown").addClass("grayed-out");
			}
		}
		});
	}
	function updateBusinessButton(){
		var getValue="";
		var getType=$("#businessbtn-type").val();
		if(getType=="Call Now" || getType=="Contact Us"){
			getValue=$("#phone-infox").val().trim();;
			if(getValue == '')
			{
				Materialize.toast('Enter phone.', 2000, 'red');
				$("#phone-infox").focus();
				return false;
			}

			/*18005551234
			1 800 555 1234
			+1 800 555-1234
			+86 800 555 1234
			1-800-555-1234
			1 (800) 555-1234
			(800)555-1234
			(800) 555-1234
			(800)5551234
			800-555-1234
			800.555.1234
			800 555 1234x5678
			8005551234 x5678
			1    800    555-1234
			1----800----555-1234*/

			var ptn = /\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/;
			if(getValue != '' && !ptn.test(getValue))
			{
				Materialize.toast('Enter valid phone.', 2000, 'red');
				$("#phone-infox").focus();
				return false;
			}
		}
		else if(getType=="Send Email" || getType=="Send Message"){
			var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
			getValue=$("#mail-infox").val().trim();
			if(getValue == '')
			{
				Materialize.toast('Enter email.', 2000, 'red');
				$("#mail-infox").focus();
				return false;
			}
			if(getValue != '' && !pattern.test(getValue))
			{
				Materialize.toast('Enter valid email.', 2000, 'red');
				$("#mail-infox").focus();
				return false;
			}
		}
		else{
			var urlverify = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
			getValue=$("#url-infox").val().trim();;
			if(getValue == '')
			{
				Materialize.toast('Enter site url.', 2000, 'red');
				$("#url-infox").focus();
				return false;
			}
			if(getValue != '' && !urlverify.test(getValue))
			{
				Materialize.toast('Enter valid url.', 2000, 'red');
				$("#url-infox").focus();
				return false;
			}
		}		
		$.ajax({
			url: '?r=page/addbsnesbtn', 
			type: 'POST',
			data: 'getType='+getType+'&getValue='+getValue,
			success: function (data)
			{
				var result = $.parseJSON(data);
				if(result['msg'] == 'success')
				{ 
					$('#edit_button_business').modal('close');
					getValue = result['status'];
					$(".hidden-businessbtn").html("");
					if(getType=="Call Now" || getType=="Contact Us")
					{
						$(".hidden-businessbtn").html('<a href="javascript:void(0)" class="simple-tooltip" title="'+getValue+'">'+getType+'</a>');
						$('.simple-tooltip').tooltipster({
								contentAsHTML: true,
								trigger:'click',
								theme: ['tooltipster-borderless']
						});			
					}
					else if(getType=="Send Email" || getType=="Send Message")
					{
						$(".hidden-businessbtn").html('<a href="mailto:'+getValue+'" class="simple-link">Link</a>');
					}
					else
					{	
						$(".hidden-businessbtn").html('<a href="'+getValue+'" target="blank" class="simple-link">Link</a>');
					}
					$(".businessbtn-caption").html(getType);
					if($(".hidden-businessbtn").parents(".dropdown").hasClass("grayed-out"))
					{
						$(".hidden-businessbtn").parents(".dropdown").removeClass("grayed-out");
					}
				}
				else
				{
					Materialize.toast('Please try late.', 2000, 'red');
				}
			}
		});
	}

	function updateBusinessButtonNew(obj) {
		var getValue="";
		var getType=$("#walleditbustype").val();
		var getAbout=$("#walleditabout").val();
		var getEmail=$("#mail-info").val();
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		var getUrl=$("#url-info").val();
		var urlverify = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		
		if(getType==""){
			Materialize.toast('Select business type.', 2000, 'red');
			return false;
		} else if(getEmail == '') {
			Materialize.toast('Enter email.', 2000, 'red');
			$("#mail-info").focus();  
			return false;
		} else if(getValue != '' && !pattern.test(getValue)) {
			Materialize.toast('Enter valid email.', 2000, 'red');
			$("#mail-info").focus();
			return false;
		} else if(getUrl == '') {
			Materialize.toast('Enter site url.', 2000, 'red');
			$("#url-info").focus();
			return false;
		} else if(getUrl != '' && !urlverify.test(getUrl)) {
			Materialize.toast('Enter valid url.', 2000, 'red');
			$("#url-info").focus();
			return false;
		} 

		$.ajax({
			url: '?r=page/wallabouteditdirect', 
			type: 'POST',
			data: {getAbout, getType, getEmail, getUrl},
			success: function (data)
			{
				Materialize.toast('Successfully update.', 2000, 'green');
			}
		});
}
/* FUN end test business button - business page */

/* FUN manage endorsement */
	function manageEndorsement(obj,img,pe){
		var doWhat="";
		var sparent=$(obj).parents(".servicedetail");
		var mparent=$(obj).parents(".service-box");
		
		$.ajax({
			type: 'POST',
			url: '?r=page/manageendorse',
			data: "endorse="+pe,
			success: function(data)
			{
				if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else {
					
					mparent.find(".count").html(data);
					
					var getText=$(obj).html();
					if(getText=='<i class="mdi mdi-plus"></i>'){doWhat="add";}
					else{doWhat="remove";}
					
					if(doWhat=="add"){
					$(obj).addClass("remove");
					$(obj).html('<i class="mdi mdi-minus"></i>');
					if(sparent.find("ul").find("li:eq(0)").length)
					{
						sparent.find("ul").find("li:eq(0)").before('<li class="active"><a href="javascript:void(0)"><img src="'+img+'"/></a></li>');
					}	
					else
					{
						sparent.find("ul").append('<li class="active"><a href="javascript:void(0)"><img src="'+img+'"/></a></li>');
					}
				}else{
					$(obj).removeClass("remove");
					$(obj).html('<i class="mdi mdi-plus"></i>');
					sparent.find("ul li:first-child").remove();
				}
					
				}
			}
		});
		
		
	}
/* FUN end manage endorsement */

/* FUN start update page details */
	function update_pagedetails(){
		var buscat = $("#buscat").val();
		var about = $("#about").val();
		var email = $("#email").val();
		var website = $("#website").val();
		var tlid = $("#tlid").val();

		var pagesitere = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;

		if(email == '') {
			Materialize.toast('Add business email.', 2000, 'red');
		} else if(email != '' && ! pattern.test(email)) {
			Materialize.toast('Enter valid business email.', 2000, 'red');
		} else if(website != '' && !pagesitere.test(website)) {
			Materialize.toast('Add business site.', 2000, 'red');
		} else {
			$.ajax({
			   type: 'POST',
			   url: '?r=page/pageupdate',
			   data: 'buscat='+buscat+'&about='+about+'&email='+email+'&website='+website+'&pageid='+tlid,
			   success: function(data){
					var result = $.parseJSON(data);
					if(result['updatestatus'] == 'Success')
					{
						$(".buscat").html(result['buscat']);
						$(".about").html(result['shrtdesc']);
						$(".email").html(result['email']);
						$("a.maillink").attr("href", result['emaillink']);
						$(".website").html(result['site']);
						$("a.sitelink").attr("href", result['sitelink']);
					}
			   }
			});
		}
	}
/* FUN end update page details */

/* FUN start page invite for review functions */
	function cancelinvitereview(fid){
		$('.invite_'+fid).hide();
	}
	function sendinvitereview(fid,pid){
		var status = $("#page_status").val();
		if(status == 0){
			return false;
		}
		if (fid != '' && pid != '')
		{
			$.ajax({
				type: 'POST',
				url: '?r=page/sendinvitereview',
				data: "fid="+fid+"&pid="+pid,
				success: function (data) {
					if (data)
					{
						if(data == 'checkuserauthclassnv') {
							checkuserauthclassnv();
						} 
						else if(data == 'checkuserauthclassg') {
							checkuserauthclassg();
						} 
						else {
							$('.events_'+fid).hide();
							$('.sendinvitation_'+fid).show();
						}
					}
				}
			});
		}
	}
/* FUN end page invite for review functions */
  
function addReviewNew(id) {
	applypostloader('SHOW');
	Materialize.toast('Posting...', 2000, 'green');
	
	var $w = $(window).width();
	var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
    var title = $("#title").val();

    chk = 'off';
    if($('#customchk:checkbox:checked').length > 0) {
        chk = 'on';
    }
    
    var status = document.getElementById('textInput').value;
    
    if (status != "" && reg.test(status) == true) {
         status = status.replace(reg, " ");
    }
    if (title != "" && reg.test(title) == true) {
         title = title.replace(reg, " ");
    }
  
    var count = document.getElementById('hiddenCount').value;
    var formdata;
    formdata = new FormData($('form')[1]);
    
    if (/^\s+$/.test(status))
    {
        toggleAbilityPostButton(HIDE);
        $('#textInput').val("");
        $("#textInput").focus();
        applypostloader('HIDE');
        return false;
    }  
    else if (status == "")
    {
        $("#textInput").focus();
        applypostloader('HIDE');
        return false;
    } else if (status != "")
    {
        formdata.append("test", status);
    }

    if($('#pagereviewrate').val() == 0 && $('#placetype').val() == 'reviews')
	{
		Materialize.toast('Rate for the place.', 2000, 'red');
		applypostloader('HIDE');
		return false;
	}

	if($w > 568) {
    	$post_privacy = $('#post_privacy').text().trim();
	} else {
		$post_privacy = $('#post_privacy2').text().trim();
	}

    if($.inArray($post_privacy, $post_privacy_array) > -1) {
	} else {
		$post_privacy = 'Public';
	}

	$rating = $('#compose_newreview').find('.mdi.mdi-star.active').length;
	if($.inArray($rating, starsArray) !== -1) {
	} else {
		$rating = '';
	}

	var $location = $('#selectedlocation').find('.tagged_location_name').text().trim();
	$(".post_loadin_img").css("display","inline-block");

	formdata.append("sharewith",$('#customin1').val());
	formdata.append("sharenot",$('#customin3').val());
	formdata.append("customchk", chk);
	formdata.append("posttags", addUserForTag);
	formdata.append("current_location", $location);
	formdata.append("post_privacy", $post_privacy);
	formdata.append("link_title",$('#link_title').val());
	formdata.append("link_url",$('#link_url').val());
	formdata.append("link_description",$('#link_description').val());
	formdata.append("link_image",$('#link_image').val());
	formdata.append("title", title);
	formdata.append("pagename",$('#pagename').val());
	formdata.append("tlid",$('#tlid').val());
	formdata.append('custom', customArray);	
	formdata.append("pagereviewrate" , $rating);
 
	$.ajax({  
        url: '?r=page/givereview',  
        type: 'POST', 
        data:formdata,
        async:false,        
        processData: false,
        contentType: false,
        beforeSend: function() {
        },
        success: function(data) {
        	//applypostloader('HIDE');
        	if(data == 'checkuserauthclassnv') {
				checkuserauthclassnv();
			} 
			else if(data == 'checkuserauthclassg') {
				checkuserauthclassg();
			} 
			else {
				$('#compose_newreview').modal('close');
				newct = 0;
                lastModified = [];
                storedFiles = [];
                storedFilesExsting = [];
                storedFiles.length = 0;
                storedFilesExsting.length = 0;
                $('#reviews-content').find('.post-list').prepend(data); 
				Materialize.toast('Posted', 2000, 'green');
			}	
        }
    }).done(function(){
       setTimeout(function(){ fixImageUI('newpost'); initDropdown(); },500);
	});

	return true;
}

/* FUN add page review functions */
	function addReview(id) {
		applypostloader('SHOW');
		var status = $("#page_status").val();
		if(status == 0){
			applypostloader('HIDE');
			return false;
		}
		//var reg = /<(.|\n)*?>/g;
		var reg = /<\s*\/\s*\w\s*.*?>|<\s*br\s*>/g;
		var title = $("#title").val();

		chk = 'off';
		if($('#customchk:checkbox:checked').length > 0) {
			chk = 'on';
		}
		
		var status = document.getElementById('ptextInput').value;
		
		if (status != "" && reg.test(status) == true)
		{
			 status = status.replace(reg, " ");
		}
		if (title != "" && reg.test(title) == true)
		{
			 title = title.replace(reg, " ");
		}
		var count = document.getElementById('hiddenCount').value;
		var formdata;
		formdata = new FormData($('form')[1]);
		for(var i=0, len=storedFiles.length; i<len; i++) {
			formdata.append('imageFile1[]', storedFiles[i]); 
		}
		if (/^\s+$/.test(status) && document.getElementById('imageFile1').value == "" )
		{
			toggleAbilityPostButton(HIDE);
			$('#ptextInput').val("");
			$("#ptextInput").focus();
			applypostloader('HIDE');
			return false;
		}
		else if (status == "" && document.getElementById('imageFile1').value == "")
		{
			$("#ptextInput").focus();
			applypostloader('HIDE');
			return false;
		} else if (status != "" && document.getElementById('imageFile1').value == "")
		{
			formdata.append("test", status);
			formdata.append("imageFile1", "");

		} else if (document.getElementById('imageFile1').value != "" && status == "")
		{
			var ofile = document.getElementById('imageFile1').files;
			formdata.append("imageFile1", ofile);
			formdata.append("test", "");

		} else if (document.getElementById('imageFile1').value != "" && status != "")
		{
			var ofile = document.getElementById('imageFile1').files;
			formdata.append("imageFile1", ofile);
			formdata.append("test", status);
		} else
		{
			applypostloader('HIDE');
			return false;

		}
		$(".post_loadin_img").css("display","inline-block");

		  formdata.append("pagereviewrate" ,$('#pagereviewrate').val());
		  formdata.append("posttags" ,$('#taginputt').val());
		  formdata.append("current_location",$('#pr_location').val());
		  formdata.append("post_privacy",$('#post_privacy').val());
		  formdata.append("title",$('#ptitle').val());
		  formdata.append("pagename",$('#pagename').val());
		  formdata.append("tlid",$('#tlid').val());
		  managePostButton('HIDE');
		  
		$.ajax({
			url: '?r=page/givereview',  
			type: 'POST',
			data:formdata,
			async:false,        
			processData: false,
			contentType: false,
			success: function(data) {
				//applypostloader('HIDE');
				if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else {
					newct = 0;
					lastModified = [];
					storedFiles = [];
					storedFiles.length = 0;
					reviewsContent();
				}
			},
			complete: function(){
			   resetNewPost(id);
			}
		}).done(function(){
		   setTimeout(function(){fixImageUI('newpost');},500);
	  });

		return true;
	} 
/* FUN end page review functions */

/* FUN add page verify functions */
	function pageVerify(){
		$(".notice .post_loadin_img").css("display","inline-block");

		var email = $("#page_email").val();
		var pageid = $("#tlid").val();

		if(email != '' && pageid != '')
		{
		$.ajax({
			type: 'POST',
			url: '?r=page/verifyemail',
			data: "verifyemailid="+email+"&pageid="+pageid,
			success: function(data)
			{               
			}
			});
		}
	}
/* FUN end page verify functions */

/* FUN start page endorse functions */
	function saveEndorse(obj){
		var pgendorse = '0';
		var pgmail = '0';
		if(document.getElementById("pgendorse").checked)
		{
			var pgendorse = $("#pgendorse").val();
		}
		if(document.getElementById("pgmail").checked) 
		{
			var pgmail = $("#pgmail").val();	
		}
		$.ajax({
			type: 'POST',
			url: '?r=page/setendorse',
			data: "pgendorse="+pgendorse+"&pgmail="+pgmail,
			success: function(data)
			{
				Materialize.toast('Successfully updated.', 2000, 'green');
			}
		});
		mng_expandable(obj);
	}
/* FUN end page endorse functions */

/* FUN start page add endorse functions */
	function addEndorse(){
		var endorse = $("#addendorse").val();
		if(endorse == '')
		{
			Materialize.toast('Add endorsement skill.', 2000, 'red');
			return false;
		}
		else
		{
			$.ajax({
				type: 'POST',
				url: '?r=page/addendorse',
				data: "endorse="+endorse,
				success: function(data)
				{
					if(data)
					{
						if(data == 'checkuserauthclassnv') {
							checkuserauthclassnv();
						} 
						else if(data == 'checkuserauthclassg') {
							checkuserauthclassg();
						} 
						else {
						$("#addendorse").val('');
						endorseContent();
						}
					}
					else
					{
						Materialize.toast('This endorsement skill is already exist.', 2000, 'red');
						return false;
					}
				}
			});
		}
	}
/* FUN end page add endorse functions */

/* FUN end page delete endorse functions */
	function removeEndorse(endorse,end){
		$.ajax({
			type: 'POST',
			url: '?r=page/deleteendorse',
			data: "endorse="+endorse,
			success: function(data)
			{
				if(data)
				{
					$('.delete_'+end).hide();
				}
				else
				{
				}
			}
		});
	}
/* FUN end page delete endorse functions */

/* FUN start user list page endorse in popup */
	function listUserEndorse(endorse,baseurl){
		$('#endorsepeople-popup').html('');
		$.ajax({
		type: 'POST',
		url: '?r=page/listuserendorse',
		data: "endorse="+endorse+"&baseurl="+baseurl,
		success: function(data)
		{
			$('#endorsepeople-popup').html(data);
		}
	});
	}
/* FUN start user list page endorse in popup */

/* Start Delete Page function */
	function pagedelete(pid, isPermission=false){
		if(pid) {
			if(isPermission) {
				$.ajax({
				  url: '?r=page/delete-page', 
				  type: 'POST',
				  data: 'page_id=' + pid,
				  success: function (data) {
					if(data){
						window.location.href='?r=site/travpage';
					}
					else{
						$(".discard_md_modal").modal("close");
					}
				  }
				});
			} else {
				$('.dropdown-button').dropdown('close');
				var disText = $(".discard_md_modal .discard_modal_msg");
			    var btnKeep = $(".discard_md_modal .modal_keep");
			    var btnDiscard = $(".discard_md_modal .modal_discard");
	    		disText.html("Are you sure to Delete this page ?");
	            btnKeep.html("Keep");
	            btnDiscard.html("Delete");
	            btnDiscard.attr('onclick', 'pagedelete(\''+pid+'\', true)');
	            $(".discard_md_modal").modal("open");
			}
		}
	}
/* End Delete page function */

/* Start Flag Page function */
	function flag_page(page_id,p_uid,uid, isPermission=false){	
		var desc = $("#textInput").val();
		
		if(desc == '') {
			Materialize.toast('Enter Flaging reason.', 2000, 'red');
			return false;			
		}
		
		if(isPermission) {
			$.ajax({
			  url: '?r=page/flag-page', 
			  type: 'POST',
			  data: 'page_id=' + page_id+'&p_uid='+p_uid+'&uid='+uid+'&desc='+desc,
			  success: function (data) {
				if(data) {
					window.location.href='?r=site/travpage';
				} else {
					$(".discard_md_modal").modal("close");
				}
			  }
			});
		} else {
			$('.dropdown-button').dropdown('close');
			var disText = $(".discard_md_modal .discard_modal_msg");
		    var btnKeep = $(".discard_md_modal .modal_keep");
		    var btnDiscard = $(".discard_md_modal .modal_discard");
    		disText.html("Canceling request?");
            btnKeep.html("Keep");
            btnDiscard.html("Flag");
            btnDiscard.attr('onclick', 'flag_page(\''+page_id+'\', \''+p_uid+'\', \''+uid+'\', true)');
            $(".discard_md_modal").modal("open");
		}
	}
/* End Flag page function */

/* FUN start update page details */
	function update_page_about(obj){
		var pagename = $("#buspagename").val();
		var buscat = $("#buscat").val();
		var pageshort = $("#pageshort").val();
		var busaddress = $("#busaddress").val();
		var bustown = $("#autocomplete").val();
		var buscode = $("#buscode").val();
		var email = $("#email").val();
		var website = $("#website").val();
		var busphone = $("#busphone").val();
		var tlid = $("#tlid").val();
		var country_code = $("#country_code").val();

		var pagesitere = /^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/;
		var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
		var ptn = /([0-9\s\-]{1,})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;

		if(pagename == '')               
		{
			Materialize.toast('Enter page name.', 2000, 'red');
			$("#pagename").focus();
			return false;
		}
		if(busaddress == '')
		{
			Materialize.toast('Enter business address.', 2000, 'red');
			$("#busaddress").focus();
			return false;
		}
		if(bustown == '')
		{
			Materialize.toast('Enter business city.', 2000, 'red');
			$("#bustown").focus();
			return false;
		}
		if(email == '')
		{
			Materialize.toast('Add business email.', 2000, 'red');
			$("#email").focus();
			return false;
		}
		if(email != '' && ! pattern.test(email))
		{
			Materialize.toast('Enter valid business email.', 2000, 'red');
			$("#email").focus();
			return false;
		}
		if(website != '' && !pagesitere.test(website))
		{
			Materialize.toast('Add business site.', 2000, 'red');
			$("#website").focus();
			return false;
		}
		if(busphone == '')
		{
			Materialize.toast('Enter business phone.', 2000, 'red');
			$("#busphone").focus();
			return false;
		}
		if(busphone != '' && ! ptn.test(busphone))
		{
			Materialize.toast('Enter valid business phone.', 2000, 'red');
			$("#busphone").focus();
			return false;
		}
		else
		{
			var formdata;
			formdata = new FormData();
			formdata.append("pagename", pagename);
			formdata.append("buscat", buscat);
			formdata.append("pageshort", pageshort);
			formdata.append("busaddress", busaddress);
			formdata.append("bustown", bustown);
			formdata.append("buscode", buscode);
			formdata.append("email", email);
			formdata.append("website", website);
			formdata.append("busphone", busphone);
			formdata.append("pageid", tlid);
			formdata.append("country_code", country_code);

			$.ajax({
				type: 'POST',  
				url: '?r=page/pageupdateabout',
				data: formdata,
				processData: false,
				contentType: false,
				success: function(data){
					var result = $.parseJSON(data);
					if(result['updatestatus'] == 'Success')
					{
						$(".busname").html(result['busname']);
						$(".buscat").html(result['buscat']);
						$(".about").html(result['shrtdesc']);
						$(".busaddress").html(result['address']);
						$(".bustown").html(result['bustown']);
						$(".buscode").html(result['buscode']);
						$(".busphone").html(result['busphone']);
						$(".email").html(result['email']);
						$("a.maillink").attr("href", result['emaillink']);
						$(".website").html(result['site']);
						$("a.sitelink").attr("href", result['sitelink']);
						close_detail(obj);
					}
			   }
			});
		}
	}
/* FUN end update page details */

/************ END Design Code ************/
$(window).resize(function() {		
	$width = $(window).width();
	if($width >767) {
		$('.mainpage-name').hide();
		$('.innerpage-name').hide();
		//$('.gotohome').hide();
	} else {
		$('.mainpage-name').hide();
		$('.innerpage-name').show();
		//$('.gotohome').show();
	}

	if($('#photos-content').hasClass('active')) {
		if($('#photos-content').find('#pc-albums').hasClass('active')) {
			zhijsdxhixsl();
		}
	}
});

$(".action-icon.activity-link").click(function(){
	setTimeout(function() { 
		$width = $(window).width();
		if($width <767) {
			$('.mainpage-name').hide();
			$('.innerpage-name').show().html('Page');
			$('.mobile-menu.topicon').find('.gotohome').show()
			$('.mobile-menu.topicon').find('.mbl-menuicon1').hide();
			$('.mobile-menu.topicon').find('.gotohome').find('i').removeClass().addClass('mdi mdi-arrow-left');
		} else {
			$('.innerpage-name').hide();
			$('.mainpage-name').hide();
			$('.mobile-menu.topicon').find('.mbl-menuicon1').show();
			$('.mobile-menu.topicon').find('.gotohome').hide();
			$('.tabs-detail').removeClass('gone');
			$('.tabs-detail').addClass('gone');
		}
    }, 400);
});

$(document).on('click', '.ksjdsikosadsa', function() {
	$boxid = $(this).attr('data-boxid');
	isAlbumphotobox = 'yes';
	selectedpostid = $boxid;
	$.ajax({
        url: '?r=userwall/fetchalbumids',
        type: "post",
	    data: {$boxid},
        success: function(data){
        	var result = $.parseJSON(data);
      		customArray = result;
      		customArrayTemp = result;
        }
    });
});

$(document).on('click', '.xmdjkv li.tab', function() {
	$width = $(window).width();
	if($width < 767) {  
		$labelNameArray = ['General', 'Notifications', 'Page Admins', 'Block List', 'Activity Log', 'Review Posts', 'Review Photos'];
		$labelName = $(this).find('a').text().trim();
		
		if($.inArray($labelName, $labelNameArray) !== -1) {
			$specId = $(this).find('a').attr('href');
			if($width <767) {
				$('.innerpage-name').show().html($labelName);
				$('.mainpage-name').hide();
			} else {
				$('.innerpage-name').hide();
				$('.mainpage-name').show();
			}
			$('.mobile-menu.topicon').find('.mbl-menuicon1').hide();
			$('.profile-top').hide();
			$('.search-holder.main-sholder').hide();

			$('.mobile-menu.topicon').find('.gotohome').show();
			$('.mobile-menu.topicon').find('.gotohome').find('i').removeClass().addClass('mdi mdi-arrow-left');
			$('.mobile-menu.topicon').find('.gotohome').find('a').attr("onclick", "openPageSettings()");
			$('.mobile-menu.topicon').find('.gotohome').find('a').addClass('gobacksetting');						
			$('.mobile-menu.topicon').find('.gotohome').find('a').attr("href", "javascript:void(0)")
			
			$('.tabs-detail').removeClass('gone').show();
			$('.tabs-detail').find('.tab-content').find('.tab-pane.fade').removeClass('active in');
			
			$($specId).addClass('active in');

			$('.gobacksetting').on('click', function() {
				if($('.action-icon.activity-link').length) {
					$width = $(window).width();
					if($width <767) {
						$('.innerpage-name').show();
						$('.mainpage-name').hide();
					} else {
						$('.innerpage-name').hide();
						$('.mainpage-name').show();
					}
					$('#settings-content').find('.tabs-list').show();
					$('#settings-content').find('.tabs-detail').hide();

					$('.mobile-menu.topicon').find('.gotohome').find('a').removeClass('gobacksetting');						
					$('.mobile-menu.topicon').find('.gotohome').find('a').attr("onclick", "location.reload();");
				}
			});
		}
	}
});

$(document).ready(function() {
	if($('#photos-content').hasClass('active')) {
		if($('#photos-content').find('#pc-albums').hasClass('active')) {
			zhijsdxhixsl();
		}
	}

	$(document).on('click', '.windowload', function() {
		window.location.href='';
	});
	
	$(document).on('click', '.innerdiv li.tab', function() {

		$width = $(window).width();
		if($width >767) {
			$('.mainpage-name').hide();
			$('.innerpage-name').hide();
			//$('.gotohome').hide();
		} else {
			$('.mainpage-name').hide();
			$('.innerpage-name').show();
			//$('.gotohome').show();
		}
		
		$isFirst = $(this).is( ":first-child" );
		if(!$isFirst) {
			if($('.gotohome').length) {
				$('.gotohome').css('display', 'block');
				$('.gotohome').html('<a href="javascript:void(0)" onclick="backtomaintab()"><i class="mdi mdi-arrow-left"></i></a>');
			}
		}

	});

	$(window).scroll(function() {
		var newfromTop=$(window).scrollTop();
		var newwinw=$(window).width();
		
		if($('#firstliclicked').hasClass('active')) {
			/* fixed header */
			var $pagename = $('.getpgname').html();
			$('.innerpage-name').html($pagename);
			
			if(newfromTop > 0){
				if(newwinw<=767){
					if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
						$(".header-section .innerpage-name").show();
						$(".header-section .innerpage-name").html($pagename);
					}
				}
			} else { 
				if(newwinw<=767){
					if($(".page-wrapper").hasClass("menuhideicons-wrapper")){
						$(".header-section .innerpage-name").hide();
						$(".header-section .innerpage-name").html($pagename);
					}                                     
				}                                      
			}                                     
			/* end fixed header */    
		}
	});

	$width = $(window).width();
	if($width >767) {
		$('.mainpage-name').hide();
		$('.innerpage-name').hide();
		//$('.gotohome').hide();
	} else {
		$('.mainpage-name').hide();
		$('.innerpage-name').show();
		//$('.gotohome').show();
	}

	$("#upload_img_action").click(function(){
  		$(".cover-slider").removeClass("openDrawer");			
  	});

  	$(document).on('click', '#carousel .carousel-item', function(e) {
		if($(this).find('img').length) {
			$imgSrc = $(this).find('img').attr('src');
			if($imgSrc != '') {
				$.ajax({  
					url: '?r=page/directsetcover', 
					type: 'POST',
					data: {$imgSrc},
					success: function (data) {
						Materialize.toast('Profile cover changed.', 2000, 'green');
						window.location.href='';
					}
				});
			}
		}
	});	

	var cropper = $('.cropper'); 
	if (cropper.length) {
	  $.each(cropper, function(i, val) {
	    var uploadCrop; 

	    var readFile = function(input) {
	      if (input.files && input.files[i]) {
	        var reader = new FileReader();

	        reader.onload = function(e) {
	          uploadCrop.croppie('bind', {
	            url: e.target.result
	          });
	        };

	        reader.readAsDataURL(input.files[i]);
	      } else {
	        alert("Sorry - you're browser doesn't support the FileReader API");
	      }
	    };

	    uploadCrop = $('.js-cropping').croppie({ // TODO: fix so its selects right element
	      viewport: {
	        width: 200,
	        height: 200
	      },
	      boundary: {
	        width: 350,
	        height: 355
	      },
	      enableOrientation: true
	    });

	    $('.js-cropper-upload').on('change', function() {
	      $('.crop').show(); // TODO: fix so its selects right element
	      readFile(this);
	    });
	    
	    $('.js-cropper-rotate--btn').on('click', function(ev) {
	      uploadCrop.croppie('rotate', parseInt($(this).data('deg')));
	    });

	    $('.img-cancel-btn').on('click', function(ev) {
	      $('#compose_camera').modal('close');
	    });

	    $('.js-cropper-result--btn').on('click', function(ev) {
	      uploadCrop.croppie('result', 'canvas').then(function(resp) {
	        popupResult({
	          src: resp
	        });
	      });
	    });

	    var popupResult = function(result) {
	    	if(result.src != undefined && result.src != '') {
				$.ajax({
				    type: 'POST', 
				    url: "?r=page/pageprofileupload",
				    data: {
				    	file: result.src
				   	},
				    success: function (data) {
				    	window.location.href='';
				    }
				});
			}
	    };
	  });
	}
	
	$(document).on('click', '.covercropbtn', function() {
		var images = $('#image-cropper').cropit('export');
		if(images != '') {
			$.ajax({
				url: '?r=page/pagecoverupload', 
				type: 'POST',
				data: {images},
				success: function (data) {
					window.location.href='';
				}
			});
		}  

	});

	var pageId = getParameterByName('id');
	if(pageId != undefined || pageId != null || pageId != '') {    	
		$.ajax({
			type: 'POST', 
			url: '?r=page/check-page-id',
			data:{pageId},
			success: function(data) {
				var result = $.parseJSON(data);
				if(result.status != undefined && result.status == true) {
					var $pageId = pageId;
					var $pageOWNER = data2.id; 
				} else {
					$pageId = '';
					$pageOWNER = ''; 
				}
			}
		});
	}

	$(document).on('click', '.addAlbumContent', function () {
         $.ajax({
            url: '?r=userwall/addalbumcontent',
            success: function(data){
                $('#addAlbumContentPopup').html(data);
                setTimeout(function() { 
                    initDropdown();
                    $('.tabs').tabs();
                    $('#addAlbumContentPopup').modal('open'); 
                }, 400);
            }
        });
    });

	justifiedGalleryinitialize();
	lightGalleryinitialize();

});

	$(".pagemessages").click(function(){
        messagesContent();
    });

    function getQueryVariable(variable)
	{
	       var query = decodeURIComponent(window.location.search.substring(1));
	       var vars = query.split("&");
	       for (var i=0;i<vars.length;i++) {
	               var pair = vars[i].split("=");
	               if(pair[0] == variable){
	               	$pair = pair[1].replace(/\+/g, ' ');
	               	return $pair;
	               }
	       }
	       return(false);
	}

	function backtomaintab() {
		if($('.gotohome').length) {
			var id = getQueryVariable('id');
			if(id != undefined && id != null && id != '') {
				window.location.href='?r=page/index&id='+id;
			}
		}
	}


	/* Start Display Message Content In Page Wall Function */
    function messagesContent()
    {
		$("#recently_joined").hide();
		$("#week_photo").hide();
		$("#get_verified").hide();
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/messages-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    $("#messages-content").html(data);
					var span = $('<span data-cls="page"/>');
					getMessagesUsers(span);
					
					$("#messages-pages").addClass('active');
                }
            });
        }
    }
	
	/* End Display Message Content In Page Wall Function */
	
    $(".pagenotifications").click(function(){
        notificationsContent();
    });
	
	/* Start Display Notification Content In Page Wall Function */
    function notificationsContent()
    {
		$("#recently_joined").hide();
		$("#week_photo").hide();
		$("#get_verified").hide();
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/notifications-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                	$('.tbpagelikes').show(); 
                    $("#notifications-content").html(data);
                    setTimeout(function(){ 
						initDropdown(); 
						$('.tabs').tabs(); 
                    	$("#notifications-content").show();
					},400);
                }
            });
        }
    }
	/* End Display Notification Content In Page Wall Function */
    
    $(".pagepromote").click(function(){
        promoteContent();
    });
	
	/* Start Display Page Promotion Function */
    function promoteContent()
    {
		$("#recently_joined").hide();
		$("#week_photo").hide();
		$("#get_verified").hide();
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/promote-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    $('.tbpagelikes').hide();
                    $("#insights-content").html(data);
                    setTimeout(function(){ 
						initDropdown(); 
						$('.tabs').tabs(); 
                    	$("#insights-content").show();
					},400);
                }
            });
        }
    }
    /* End Display Page Promotion Function */
	
    $(".pagewall").click(function(){
        $('.tbpagelikes').show();
    });

    $(".pagelike").click(function(){
        likesContent();
    });
	
	/* Start Display Page Like Content Function */
    function likesContent()
    {
    	$("#recently_joined").hide();
		$("#week_photo").hide();
		$("#get_verified").hide();

        if(wall_user_id != '' && baseUrl != '')
        {
        	$.ajax({
                url: '?r=page/likes-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                	$('.tbpagelikes').hide();
                    $("#likes-content").html(data);
                    setTimeout(function(){ 
						initDropdown(); 
						$('.tabs').tabs(); 
                    	$("#likes-content").show();
					},400);
                    initNiceScroll(".nice-scroll.invite_connect_search");
			    }
            });
        }
    }
	/* Start Display Page Like Content Function */
	
    $(".pagephotos").click(function(){
        photosContent();
    });

    function download(obj) {
	    $.ajax({
	        url: "?r=page/download", 
	        success: function (data) {
	        	/*var result = $.parseJSON(data);
	            if(result.status != undefined && result.status == true) {
	                var returnlabel = result.returnlabel;
	                var ids = result.ids;
	                $('#'+$label).html(returnlabel);
	                addUserForAccountSettingsArray = ids.slice();
	            }*/
	        }
	    });
	}


	/* Start Display Photo Content in Page Wall Seciton Function */	
    function photosContent()
    { 
		$("#recently_joined").hide();
		$("#week_photo").hide();
		$("#get_verified").hide();
		var type = 'page'; 
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({ 
                url: '?r=userwall/photos-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl+"&type="+type+"&page_owner="+page_owner,
                success: function(data)
                {
                	$('.tbpagelikes').show();
                    $("#photos-content").html(data); 
                    photoscontentsplit();
   
                    setTimeout(function() { 
						initDropdown(); 
						$('.tabs').tabs(); 
                    	$("#photos-content").show();
                    	var boxes = $('#pc-albums').find('.grid-box.countgrid-box').length;
                    	$('.getAgainAlbumscount').html('('+boxes+')');

                    	var boxes = $('#pc-photos').find('.grid-box.countgrid-box').length;
                    	$('.getAgainPhotoscount').html('('+boxes+')'); 
					},400); 

                    initPhotoCarousel("wallpage");			          
                    /* end album slider */
                    setTimeout(function() {
						initGalleryImageSlider();

						$('.carousel_master .carousel').carousel();

						var owl = $('.owl-carousel');
							owl.owlCarousel({
								margin: 10,
								loop: false,		
								dots: true,			
								nav: true,
								responsive: {
								  0: {
									items: 1
								  },
								  600: {
									items: 2
								  },
								  1000: {
									items: 3
								  }
							}
						});
						
						if($("#photos-content").find(".user-photos").length > 0){
							
							var win_w = $(window).width();
							var settime = 400;
							if(win_w < 768)
								settime = 700;
								
								setTimeout(function(){					
									$('.user-photos').addClass('photoshow');
								},settime);
							
							setTimeout(function(){
								fixImageUI("wall-photoslider");
								
							},400);	
						}  
                    },700);
                }
            });
        }
    }
    /* End Display Photo Content in Page Wall Seciton Function */
	
    $(".pageevents").click(function(){
        eventsContent();
    });

	/* Start Display Event Content in Page Wall Seciton Function */	
    function eventsContent()
    {
		$("#recently_joined").hide();
		$("#week_photo").hide();
		$("#get_verified").hide();
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/events-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    $('.tbpagelikes').show();
                    $("#events-content").html(data);
                    setTimeout(function(){ 
						initDropdown(); 
						$('.tabs').tabs(); 
                    	$("#events-content").show();
					},400);
                    $( function() {
                    	var croppicContainerPreloadOptions;
					    croppicContainerPreloadOptions = {
							cropUrl:'?r=event/event-image-crop',
							loadPicture: $assetsPath+'/images/night.jpg',
							enableMousescroll:true,
							
							onAfterImgCrop:function(response){
									var image ='<img src="'+response.url+'">';
									$('.main-img').html(image);				
									$('#images').val(response.url);
							}
						}
                    } );
                }
            });
        }
    }
    /* End Display Event Content in Page Wall Seciton Function */	
	
    $(".pagereviews").click(function(){
        reviewsContent();
    });

	/* Start Display Review Content in Page Wall Seciton Function */	
    function reviewsContent()
    {
		$("#recently_joined").hide();
		$("#week_photo").hide();
		$("#get_verified").hide();
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({ 
                url: '?r=page/reviews-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    $('.tbpagelikes').show();
                    $("#reviews-content").html(data);
                    setTimeout(function(){ 
						initDropdown(); 
						$('.tabs').tabs(); 
                    	$("#reviews-content").show();
					},400);
                    $(".new-post").click(function(){
                        expandNewpost(this);
                    });
                    $("body").on('input propertychange', '#ptextInput', function()
                    {
                        var id = $(this).parents('form').data('id');
                        $(".post-bcontent").show();
                        toggleAbilityPageButton();
                    });
                    setTimeout(function(){setGeneralThings();},400);
                }
            });
        }
    }
    /* Start Display Review Content in Page Wall Seciton Function */
	
    $(".pageendorse").click(function(){
        endorseContent();
    });

	/* Start Display Endorement Content in Page Wall Seciton Function */	
    function endorseContent()
    {
		$("#recently_joined").hide();
		$("#week_photo").hide();
		$("#get_verified").hide();
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/endorse-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    if(data == 'checkuserauthclassnv') {
						checkuserauthclassnv();
					} 
					else if(data == 'checkuserauthclassg') {
						checkuserauthclassg();
					} 
					else {
						$('.tbpagelikes').show();
						$("#endorsement-content").html(data);
						setTimeout(function(){ 
							initDropdown(); 
							$('.tabs').tabs(); 
							$("#endorsement-content").show();
						},400);
					}
                }
            });
        }
    }
	/* End Display Endorement Content in Page Wall Seciton Function */
	
   $(document.body).on('click', "a.popup-modal", function(e){
            e.preventDefault();       
        });
	
	
	$(document).ready(function()
    {
	 	$('.simple-tooltip').tooltipster({
			contentAsHTML: true,
			trigger:'click',
			theme: ['tooltipster-borderless']
		});
		
		$('.inline-tooltip').tooltipster({
	        contentAsHTML: true,
	        theme: ['tooltipster-borderless']
	    });
	});
	
    $(".pagesettings-activity").click(function(){
        activityContent();
    });
    function activityContent()
    {
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/activity-content-new',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    $("#pagesettings-activity").html(data);
                    setTimeout(function(){
						initDropdown();
						setGeneralThings();

						$(".activity-content .activity-box").each(function() {
							if($(this).find(".imgpreview").length > 0 ) {				
								$(this).find(".pimg-holder").each(function(e, v) {
									var cont_width=$(v).width();
									var cont_height=$(v).height();
									var img_width=$(v).find("img").width();
									var img_height=$(v).find("img").height();
									
									if(img_width>cont_width) {
										$(v).find("img").css('width', '100%');
									}
								});
							}
						});
					},400);
                }
            });
        }
    }
    $(".pagesettings-reviewposts").click(function(){
        reviewpostsContent('posts');
    });
    $(".pagesettings-reviewphotos").click(function(){
        reviewpostsContent('photos');
    });
	/* Start Display Review Post Content Function */
    function reviewpostsContent(type)
    {
        
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/reviewposts-content',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl+"&type="+type,
                success: function(data)
                {
                	$("#pagesettings-review"+type).html(data);

                	setTimeout(function() {
						initGalleryImageSlider();
						initDropdown(); 
                	}, 400)
                }
            });
        }
    }
	/* Start Display Review Post Content Function */
	
    $(".pagesettings-pageroles").click(function(){
        rolesContent();
    });
	
	/* Start Display Page Roles Content Function */
    function rolesContent()
    {
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/pageroles',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    $("#pagesettings-pageroles").html(data);
                }
            });
        }
    }
	/* End Display Page Roles Content Function */
	
    $(".pagesettings-messaging").click(function(){
        msgSetContent();
    });
	
	/* Start Message Set Function */
    function msgSetContent()
    {
       if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/getmsgset',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    $("#pagesettings-messaging").html(data);
                    $('#msg_use_key').click(function()
                    {
                        var msg_use_key_value = $("#msg_use_key_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'msg_use_key='+msg_use_key_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Use the return key to send messages turned on';
                                    if(revst == 'off'){publabel = 'Use the return key to send messages turned off';}
                                    $("#msg_use_key_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                    $('#send_instant').click(function()
                    {
                        var send_instant_value = $("#send_instant_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'send_instant='+send_instant_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Send instant replies to anyone who messages your page turned on';
                                    if(revst == 'off'){publabel = 'Send instant replies to anyone who messages your page turned off';}
                                    $("#send_instant_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                    $('#show_greeting').click(function()
                    {
                        var show_greeting_value = $("#show_greeting_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'show_greeting='+show_greeting_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Show a message greeting when people start a conversation with you on messanger turned on';
                                    if(revst == 'off'){publabel = 'Show a message greeting when people start a conversation with you on messanger turned off';}
                                    $("#show_greeting_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                }
            });
        }
    }
	/* End Message Set Function */
	
	/* Start Send Instant Message in Page-Settings Function */
    function send_instant_msg(obj)
    {
        var send_instant_msg = $("#send_instant_msg").val();
        $.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: 'send_instant_msg='+send_instant_msg,
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var revst = '"'+result['status']+'"';
                    $(".send_instant_msg_value").html(revst);
                    close_edit(obj);
                    Materialize.toast('Instant message updated successfully.', 2000, 'green');
                }
                else
                {
                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }
	/* End Send Instant Message in Page-Settings Function */
	
    function show_greeting_msg(obj)
    {
        var show_greeting_msg = $("#show_greeting_msg").val();
        $.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: 'show_greeting_msg='+show_greeting_msg,
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var revst = '"'+result['status']+'"';
                    $(".show_greeting_msg_value").html(revst);
                    close_edit(obj);
                    Materialize.toast('Greeting message updated successfully.', 2000, 'green');
                }
                else
                {
                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }
	
    $(".pagesettings-notifications").click(function(){
        notSetContent();
    });
	
    function notSetContent()
    {
        if(wall_user_id != '' && baseUrl != '')
        {
            $.ajax({
                url: '?r=page/getnotset',  
                type: 'POST',
                data: "id="+wall_user_id+"&baseUrl="+baseUrl,
                success: function(data)
                {
                    $("#pagesettings-notifications").html(data);
                    $('#not_add_post').click(function()
                    {
                        var not_add_post_value = $("#not_add_post_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'not_add_post='+not_add_post_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Notification when there is a new post added to the page turned on';
                                    if(revst == 'off'){publabel = 'Notification when there is a new post added to the page turned off';}
                                    $("#not_add_post_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                    $('#not_add_comment').click(function()
                    {
                        var not_add_comment_value = $("#not_add_comment_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'not_add_comment='+not_add_comment_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Notification when there is a new comment to the page post turned on';
                                    if(revst == 'off'){publabel = 'Notification when there is a new comment to the page post turned off';}
                                    $("#not_add_comment_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                    $('#not_like_page').click(function()
                    {
                        var not_like_page_value = $("#not_like_page_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'not_like_page='+not_like_page_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Notification when there is a new like to the page turned on';
                                    if(revst == 'off'){publabel = 'Notification when there is a new like to the page turned off';}
                                    $("#not_like_page_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                    $('#not_like_post').click(function()
                    {
                        var not_like_post_value = $("#not_like_post_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'not_like_post='+not_like_post_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Notification when there is a new like on page post turned on';
                                    if(revst == 'off'){publabel = 'Notification when there is a new like on page post turned off';}
                                    $("#not_like_post_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                    $('#not_post_edited').click(function()
                    {
                        var not_post_edited_value = $("#not_post_edited_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'not_post_edited='+not_post_edited_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Notification when page post gets edited turned on';
                                    if(revst == 'off'){publabel = 'Notification when page post gets edited turned off';}
                                    $("#not_post_edited_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
					
                    $('#not_get_review').click(function()
                    {
                        var not_get_review_value = $("#not_get_review_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'not_get_review='+not_get_review_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Notification each time the page gets a review turned on';
                                    if(revst == 'off'){publabel = 'Notification each time the page gets a review turned off';}
                                    $("#not_get_review_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                    $('#not_msg_rcv').click(function()
                    {
                        var not_msg_rcv_value = $("#not_msg_rcv_value").val();
                        $.ajax({
                            type: 'POST',
                            url: '?r=page/pagesettings',
                            data: 'not_msg_rcv='+not_msg_rcv_value,
                            success: function(data)
                            {
                                var result = $.parseJSON(data);
                                if(result['msg'] == 'success')
                                {
                                    var revst = result['status'];
                                    var publabel = 'Notification each time your page receives a message turned on';
                                    if(revst == 'off'){publabel = 'Notification each time your page receives a message turned off';}
                                    $("#not_msg_rcv_value").val(revst);
                                    Materialize.toast(publabel, 2000, 'green');
                                }
                                else
                                {
                                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                                }
                            }   
                        });
                    });
                }
            });
        }
    }
	
    function page_publish(obj)
    {
        var page_pub_set_val = $("#page_pub_set_val").val();
		$.ajax({
		type: 'POST',
		url: '?r=page/pagesettings',
		data: 'page_pub_set_val='+page_pub_set_val,
		success: function(data)
		{
			var result = $.parseJSON(data);
			if(result['msg'] == 'success')
			{
				var publabel = 'Page published';
				if(result['status'] == 0){publabel = 'Page unpublished';}
				$("#gen_publish").html(publabel);
				close_edit(obj);
				Materialize.toast(publabel+' for others.', 2000, 'green');
			}
			else
			{
				close_edit(obj);
				Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
			}
		}   
	});
    }
	
    function page_post(obj) 
    {
        var pagepost = $('input[name="radioPosts"]:checked').val();
        var pagepostreview = 'off';
        if($('input[name=reviewPost]').is(':checked')) {
            pagepostreview = 'on';
        }
        $.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: 'pagepost='+pagepost+'&pagepostreview='+pagepostreview,
            success: function(data)
            {
            	var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var revst = result['status'];
                    var publabel = ' Public can add posts to the page';
                    if(revst == 'denyPost'){publabel = 'Public can\'t add posts to the page';}
                    $("#pagepost_value").html(publabel);
                    close_edit(obj);
                    Materialize.toast(publabel, 2000, 'green');
                }
                else
                {
				   close_edit(obj);
				   Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }
	
    function page_photos(obj)
    {
        var pagephotos = $('input[name="radioPhotos"]:checked').val();
        var pagephotoreview = 'off';
        if(document.getElementById("reviewPhotos").checked)
        {
            pagephotoreview = 'on';
        }
			$.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: 'pagephotos='+pagephotos+'&pagephotoreview='+pagephotoreview,
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var revst = result['status'];
                    var publabel = ' Public can add photos to the page';
                    if(revst == 'denyPhotos'){publabel = 'Public can\'t add photos to the page';}
                    $("#pagephotos_value").html(publabel);
                    close_edit(obj);
                    Materialize.toast(publabel, 2000, 'green');
                }
                else
                {
                    close_edit(obj);
                    Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }
    $('#rwbtn_switch').click(function()
    {
        var review_switch_value = $("#review_switch_value").val();
        $.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: 'review_switch_value='+review_switch_value,
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var revst = result['status'];
                    var publabel = 'Reviews are turned on';
                    if(revst == 'off'){publabel = 'Reviews are turned off';}
                    $("#review_switch_value").val(revst);
                    $("#revstatus").html(revst);
                    Materialize.toast(publabel, 2000, 'green');
                }
                else
                {
                	Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    });
	
    function msgfltr(obj)
    {
        var msgfltr = $("#msgfltr").val();
		$.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: 'msgfltr='+msgfltr,
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var publabel = 'List updated successfully';
                    close_edit(obj);
                    Materialize.toast(publabel, 2000, 'green');
                }
                else
                {
                    close_edit(obj);
                    Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }
    function pgfltr(obj)
    {
        var pgfltr = $("#pgfltr").val();
		$.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: 'pgfltr='+pgfltr,
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var publabel = 'List updated successfully';
                    close_edit(obj);
                    Materialize.toast(publabel, 2000, 'green');
                }
                else
                {
                    close_edit(obj);
                    Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }
    function restrictDrop(obj)
    {
        var restrictDrop = $("#restrictDrop").val();
		$.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: {
            	'restrictDrop' : addUserForAccountSettingsArrayTemp
            },
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var publabel = 'Restricted list updated successfully';
                    close_edit(obj);
                    Materialize.toast(publabel, 2000, 'green');
                }
                else
                {
                    close_edit(obj);
                    Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }
    function blockedDrop(obj)
    {
        var blockedDrop = $("#blockedDrop").val();
		$.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: {
            	'blockedDrop' : addUserForAccountSettingsArrayTemp
            },
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var publabel = 'Blocked list updated successfully';
                    close_edit(obj);
                    Materialize.toast(publabel, 2000, 'green');
                }
                else
                {
                    close_edit(obj);
                    Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }

    function blk_msg_filtering(obj)
    {
		$.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: {
            	'blk_msg_filtering' : addUserForAccountSettingsArrayTemp
            },
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var publabel = 'Message filter list updated successfully';
                    close_edit(obj);
                    Materialize.toast(publabel, 2000, 'green');
                }
                else
                {
                    close_edit(obj);
                    Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }

    function eventDrop(obj)
    {
        var eventDrop = $("#eventDrop").val();
		$.ajax({
            type: 'POST',
            url: '?r=page/pagesettings',
            data: 'eventDrop='+eventDrop,
            success: function(data)
            {
                var result = $.parseJSON(data);
                if(result['msg'] == 'success')
                {
                    var publabel = ' Restrict event engagement list updated successfully';
                    close_edit(obj);
                    Materialize.toast(publabel, 2000, 'red');
                }
                else
                {
                    close_edit(obj);
                    Materialize.toast('Something went wrong. Please try later.', 2000, 'red');
                }
            }   
        });
    }
	
	/* Start Setting Menu For Slider in Photo Section of Page Wall */
	function fetchslidercoversettingmenu($AlbumId) {
		if($AlbumId != undefined || $AlbumId != null || $AlbumId != '') {
			$.ajax({
				url: '?r=userwall/fetchslidercoversettingmenu',  
				type: 'POST',
				data: {$AlbumId},
				success: function(data)
				{
					$resultFilter = JSON.parse(data);
					if($resultFilter.status != undefined && $resultFilter.status == true) {
						$li = $resultFilter.li;
						var $selectore = $('.fetchslidercoversettingmenu').find('ul.dropdown-menu');
						if($selectore.length) {
							$selectore.html($li);
						}
					}
				}
			});
		}
	}
	/* End Setting Menu For Slider in Photo Section of Page Wall */
	
	/* Start Like Page From Wall Thumb Function */
	function likepagefromwallthumb(pid) {
		$.ajax({
		  url: '?r=page/like-page', 
		  type: 'POST',
		  data: 'page_id=' + pid,
		  success: function (data) 
			{
				if(data == 'checkuserauthclassnv') {
					checkuserauthclassnv();
				} 
				else if(data == 'checkuserauthclassg') {
					checkuserauthclassg();
				} 
				else {
						var result = $.parseJSON(data);
						var likestatus =  result.status;
						if(likestatus == 1)
						{
							$('.likeaction').attr('title','Liked');
							$('.likeaction').addClass('active');
						}
						if(likestatus == 0)
						{
							$('.likeaction').attr('title','Like');
							$('.likeaction').removeClass('active');
						}
				}	
			}	
		});
	}
	/* End Like Page From Wall Thumb Function */	
	
		
function open_edit_bp_general(obj) {
	$('#pagesettings-general').find('.editicon1').hide();
	$('#pagesettings-general').find('.editicon1').addClass('dis-none');
	$('#pagesettings-general').find('.editicon1').css('display', 'none');

	$('#pagesettings-general').find('.editicon2').hide();
	$('#pagesettings-general').find('.editicon2').addClass('dis-none');
	$('#pagesettings-general').find('.editicon2').css('display', 'none');

	$.ajax({
        url: "?r=page/getpagegeneraledit",
        beforeSend: function() {
        },
        success: function (data) {
        	$('#pagesettings-general').find('.normal-part').addClass('dis-none');
			$('#pagesettings-general').find('.normal-part').hide()
			$('#pagesettings-general').find('.edit-part').removeClass('dis-none');
			$('#pagesettings-general').find('.edit-part').show();
			$('#pagesettings-general').find('.edit-part').html(data);
			$('#pagesettings-general').find('.edit-part').find('.edit-mode').show();
			$('select').material_select();

			$(".tabs-detail").scrollTop(0);

			$('html, body').animate({
			    scrollTop: $(".tabs-detail").offset().top - 100
			}, 400);
        }
    });
}

function open_edit_bp_general_cl(obj, dsik) {
	var w = $(window).width();
	if(w<=767) { 
		$('#pagesettings-general').find('.editicon2').show();
		$('#pagesettings-general').find('.editicon2').removeClass('dis-none');

		$('#pagesettings-general').find('.editicon1').hide();
		$('#pagesettings-general').find('.editicon1').addClass('dis-none');
	} else {
		$('#pagesettings-general').find('.editicon1').show();
		$('#pagesettings-general').find('.editicon1').removeClass('dis-none');

		$('#pagesettings-general').find('.editicon2').hide();
		$('#pagesettings-general').find('.editicon2').addClass('dis-none');
	}

	if(dsik) {
		var page_pub_set_val = $("#page_pub_set_val").val();
		var pagepost = $('input[name="radioPosts"]:checked').val();
		var pagepostreview = 'off';
		if($('input[name=reviewPost]').is(':checked')) {
		    pagepostreview = 'on';
		}

		var pagephotos = $('input[name="radioPhotos"]:checked').val();
		var pagephotoreview = 'off';
		if(document.getElementById("reviewPhotos").checked) {
		    pagephotoreview = 'on';
		}

		var pgfltr = $("#pgfltr").val();

		var review_switch_value = $("#review_switch_value").val();

		$.ajax({
			type: 'POST',   
			url: '?r=page/pagesettingsgeneralsave',
			data: {
				page_pub_set_val : page_pub_set_val,
				pagepost: pagepost,
				pagepostreview : pagepostreview,
				pagephotos : pagephotos,
				pagephotoreview : pagephotoreview,
				pgfltr : pgfltr,
				review_switch_value : review_switch_value
			},
	        beforeSend: function() {
	        },
	        success: function (data) {
	        	var result = $.parseJSON(data);
	        	if(result.success != undefined && (result.success == 'false' || result.success == false)) {
	        		if(result.msg != undefined && (result.msg != '')) {
	        			$msg = result.msg;
	        			Materialize.toast($msg, 2000, 'red');
	        		}
	        	} else {
	        		Materialize.toast('Saved.', 2000, 'green');

	        		$.ajax({
				        url: "?r=page/getpagegeneralnormal",
				        beforeSend: function() {
				        },
				        success: function (data) {
							$('#pagesettings-general').find('.edit-part').addClass('dis-none');
							$('#pagesettings-general').find('.edit-part').hide();
							$('#pagesettings-general').find('.edit-part').find('.edit-mode').hide();
							$('#pagesettings-general').find('.normal-part').removeClass('dis-none');
							$('#pagesettings-general').find('.normal-part').show().html(data);

							$(".tabs-detail").scrollTop(0);

							$('html, body').animate({
							    scrollTop: $(".tabs-detail").offset().top - 100
							}, 400);
				        }
				    });
				}
	        }
	    });
	} else {
		$.ajax({
	        url: "?r=page/getpagegeneralnormal",
	        beforeSend: function() {
	        },
	        success: function (data) {
				$('#pagesettings-general').find('.edit-part').addClass('dis-none');
				$('#pagesettings-general').find('.edit-part').hide();
				$('#pagesettings-general').find('.edit-part').find('.edit-mode').hide();
				$('#pagesettings-general').find('.normal-part').removeClass('dis-none');
				$('#pagesettings-general').find('.normal-part').show().html(data);

				$(".tabs-detail").scrollTop(0);

				$('html, body').animate({
				    scrollTop: $(".tabs-detail").offset().top - 100
				}, 400);
	        }
	    });
	}
}	

function open_edit_bp_blocking(obj) {
	$('#pagesettings-blocking').find('.editicon1').hide();
	$('#pagesettings-blocking').find('.editicon1').addClass('dis-none');
	$('#pagesettings-blocking').find('.editicon1').css('display', 'none');

	$('#pagesettings-blocking').find('.editicon2').hide();
	$('#pagesettings-blocking').find('.editicon2').addClass('dis-none');
	$('#pagesettings-blocking').find('.editicon2').css('display', 'none');

	$.ajax({
        url: "?r=page/getpageblockingedit",
        beforeSend: function() {
        },
        success: function (data) {
        	$('#pagesettings-blocking').find('.normal-part').addClass('dis-none');
			$('#pagesettings-blocking').find('.normal-part').hide()
			$('#pagesettings-blocking').find('.edit-part').removeClass('dis-none');
			$('#pagesettings-blocking').find('.edit-part').show();
			$('#pagesettings-blocking').find('.edit-part').html(data);
			$('#pagesettings-blocking').find('.edit-part').find('.edit-mode').show();
			$('select').material_select();

			$(".tabs-detail").scrollTop(0);

			$('html, body').animate({
			    scrollTop: $(".tabs-detail").offset().top - 100
			}, 400);

			pageblockingsomeparams();

        }
    });
}

function open_edit_bp_blocking_cl(obj, dsik) {
	var w = $(window).width();
	if(w<=767) {
		$('#pagesettings-blocking').find('.editicon2').show();
		$('#pagesettings-blocking').find('.editicon2').removeClass('dis-none');

		$('#pagesettings-blocking').find('.editicon1').hide();
		$('#pagesettings-blocking').find('.editicon1').addClass('dis-none');
	} else {
		$('#pagesettings-blocking').find('.editicon1').show();
		$('#pagesettings-blocking').find('.editicon1').removeClass('dis-none');

		$('#pagesettings-blocking').find('.editicon2').hide();
		$('#pagesettings-blocking').find('.editicon2').addClass('dis-none');
	}

	if(dsik) {
		$.ajax({
			type: 'POST',  
			url: "?r=page/pagesettingsblockingsave",
			data: {
				page_restricted_list: $page_restricted_list,
				page_block_list: $page_block_list,
				page_block_message_list: $page_block_message_list
			},
	        beforeSend: function() {
	        },
	        success: function (data) {
	        	var result = $.parseJSON(data);
	        	if(result.success != undefined && (result.success == 'false' || result.success == false)) {
	        		if(result.msg != undefined && (result.msg != '')) {
	        			$msg = result.msg;
	        			Materialize.toast($msg, 2000, 'red');
	        		}
	        	} else {
	        		Materialize.toast('Saved.', 2000, 'green');

	        		$.ajax({
				        url: "?r=page/getpageblockingnormal",
				        beforeSend: function() {
				        },
				        success: function (data) {
							$('#pagesettings-blocking').find('.edit-part').addClass('dis-none');
							$('#pagesettings-blocking').find('.edit-part').hide();
							$('#pagesettings-blocking').find('.edit-part').find('.edit-mode').hide();
							$('#pagesettings-blocking').find('.normal-part').removeClass('dis-none');
							$('#pagesettings-blocking').find('.normal-part').show().html(data);

							$(".tabs-detail").scrollTop(0);

							$('html, body').animate({
							    scrollTop: $(".tabs-detail").offset().top - 100
							}, 400);
				        }
				    });


				}
	        }
	    });
	} else {
		$.ajax({
	        url: "?r=page/getpageblockingnormal",
	        beforeSend: function() {
	        },
	        success: function (data) {
				$('#pagesettings-blocking').find('.edit-part').addClass('dis-none');
				$('#pagesettings-blocking').find('.edit-part').hide();
				$('#pagesettings-blocking').find('.edit-part').find('.edit-mode').hide();
				$('#pagesettings-blocking').find('.normal-part').removeClass('dis-none');
				$('#pagesettings-blocking').find('.normal-part').show().html(data);

				$(".tabs-detail").scrollTop(0);

				$('html, body').animate({
				    scrollTop: $(".tabs-detail").offset().top - 100
				}, 400);
	        }
	    });
	}
}	
		
		