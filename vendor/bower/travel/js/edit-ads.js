 /* Start Function For Ad Start date and End Date calculation on Document Ready of Edit ads Page */
	$(document).ready(function(){

		$(document).on('mouseover', 'ul.select-dropdown', function() {
			$(this).css('overflow-y', 'scroll');
		});

		
		var select = function() {
		var d1 = $('#startdate').datepicker('getDate', true);
		var d2 = $('#enddate').datepicker('getDate', true);
		var diff = 0;
		if (d1 && d2) {
		  diff = Math.floor((d2.getTime() - d1.getTime()) / 86400000); // ms per day
		}
		$('#calculated').val(diff);
	  }
		
        $('.simple-tooltip').tooltipster({
            contentAsHTML: true,
            trigger:'click',
            theme: ['tooltipster-borderless']
        });

        $('.image-cropperGeneralDsk').cropit({
	        height: 188,
	        width: 400,
	        smallImage: "allow",
	        onImageLoaded: function (x) {
	            if($('.mainhead.active').length) {
	                var openBlockId = $('.mainhead.active').attr('id');
	                if(openBlockId) {
	                    openBlockId = openBlockId.replace('_block', '');
	                    $openPreviewBox = $('.'+openBlockId).find('.preview-part').find('.preview-accordion').find('li.active');   
	                    $openPreviewBox.find('.cropit-preview').removeClass("class-hide");
	                    $openPreviewBox.find('.cropit-preview').addClass("class-show");
	                    $openPreviewBox.find('.saveimg').removeClass("class-hide");
	                    $openPreviewBox.find('.saveimg').addClass("class-show");
	                    $openPreviewBox.find('.removeimg').removeClass("class-hide");
	                    $openPreviewBox.find('.removeimg').addClass("class-show");
	                }
	            }
	        }
	    });
    });
/* End Function For Ad Start date and End Date calculation on Document Ready of Edit ads Page */

	$('input.All').on('change', function() {
		$('input.Male').not(this).prop('checked', false);  
		$('input.Female').not(this).prop('checked', false);
		change_audience();
	});

	$('input.Male').on('change', function() {
		$('input.All').not(this).prop('checked', false); 
		$('input.Female').not(this).prop('checked', false);
		change_audience();
	});

	$('input.Female').on('change', function() {
		$('input.All').not(this).prop('checked', false); 
		 $('input.Male').not(this).prop('checked', false);  
		change_audience();
	}); 

	function change_audience()
	{
		change_audience_guage();
	}
	
	function change_age_audience()
    {
    	var minage = $('.noUi-handle-lower').find('.noUi-tooltip').find('span').html().trim() * 1;
		var maxage = $('.noUi-handle-upper').find('.noUi-tooltip').find('span').html().trim() * 1;
		if(minage<0 || minage > 100){
			minage  = 0;
		}
		if(maxage<0 || maxage >100){
			maxage  = 100;
		}
        change_audience_guage();
    }
	
	function change_gender()
    {
        change_audience_guage();
    }
	
	/* Start Function For Filtering Audiance As User Selects Relavent Filter Options */

	function change_audience_guage() {
        var location = []; 
        var genderArray = ['Male', 'Female', 'All'];
		$('#ads_loc :selected').each(function(i, selected){ 
			if($(selected).val().trim() != '') {
		  		location.push($(selected).text()); 
		  	}
		});

        var language = []; 
		$('#ads_lang :selected').each(function(i, selected){ 
			if($(selected).val().trim() != '') {
			  language.push($(selected).text()); 
			}
		});

        var minage = $('.noUi-handle-lower').find('.noUi-tooltip').find('span').html().trim() * 1;
		var maxage = $('.noUi-handle-upper').find('.noUi-tooltip').find('span').html().trim() * 1;
		if(minage<0 || minage > 100){
			minage  = 0;
		}
		if(maxage<0 || maxage >100){
			maxage  = 100;
		}
        
        var gender = $('input[name=adGender]:checked').val();
        if($.inArray(gender, genderArray) !== -1) {
        } else {
        	Materialize.toast('Select gender', 2000, 'red');
        }

        var proficient  = []; 
		$('#ads_pro :selected').each(function(i, selected){ 
			if($(selected).val().trim() != '') {
		  		proficient.push($(selected).text()); 
		  	}
		});

		var interest = []; 
		$('#ads_int :selected').each(function(i, selected){ 
			if($(selected).val().trim() != '') {
		  		interest.push($(selected).text());
		  	}
		});

        var formdata;
        formdata = new FormData();
        formdata.append("location", location);
        formdata.append("minage", minage);
        formdata.append("maxage", maxage);
        formdata.append("language", language);
        formdata.append("gender", gender);
        formdata.append("proficient", proficient);
        formdata.append("interest", interest);

        $.ajax({
            url: '?r=ads/checkaudience',  
            type: 'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function (data)
            {
                var result = $.parseJSON(data);
                var data = result['meter'];
                if(data<0)
                {
                    data  = 0;
                }
                if(data >100)
                {
                    data  = 100;
                }
                gauge = new Gauge($('.gauge2'), {
                values: {
                    0 : 'Specific',
                    20: 'Good',
                    100: 'Broad'
                },
                colors: {
                    0 : '#ED1C24',
                    20 : '#39B54A',
                    80: '#FDD914'
                },
                angles: [
                    180,
                    360
                ],
                lineWidth: 20,
                arrowWidth: 10,
                arrowColor: '#ccc',
                inset:true,
                value: data});
                var datacount = result['count'];
                var slider = document.getElementById('test-slider1');
				slider.noUiSlider.set([0, datacount]);
                $("#tgt_aud_cnt").html(datacount);
				$('#eta_location').find('p').html(result['locationLabel']);
				$('#eta_age').find('p').html(minage+' - '+maxage);
				$('#eta_language').find('p').html(result['languageLabel']);
				$('#eta_gender').find('p').html(gender);
                $(".fixmin-slider").find(".slider-range").slider({
                    range: "min",
                    value: datacount,
                    min: 0,
                    max: datacount,
                    slide: function( event, ui ) {
                        $( ".max-amount" ).html( ui.value + " people on Iaminaqaba");
                    }
                });
            }
        });
    }
	/* End Function For Filtering Audiance As User Selects Relavent Filter Options */
	
	/* Start  Function For Ad calculation min budget */
	function ad_summary()
	{
			var min_budget = $('#min_budget').val();
			var advert_name = $('#advert_name').val();
			var ads_amount = $('#ads_amount').val();
			$("#ad_name").html(advert_name);
			$("#daily_budget").html('$'+min_budget);
			$(".ads_amount").val(min_budget);
	}
	/* End  Function For Ad calculation min budget */

	/* Start  Function For Payment On Paypal For New Edited Ad */	
	function buy_ads()
	{
		$("#myformsubmit").submit();
	}
	/* End  Function For Payment On Paypal For New Edited Ad */
	
	/* Start Function For Manual Publish if User is Not Liable To Pay and Has Already Enough Credits For Edited ad */
	function buy_ads2()
	{
		var dif_amount = $("#dif_amount").val();
		var benifit_amount = $("#benifit_amount").val();
		$.ajax({
		type: 'POST',
		url: '?r=ads/manualpublish',
		data: "dif_amount="+dif_amount+"&benifit_amount="+benifit_amount,
		success: function(data)
		{
			window.location.href = "?r=ads/manage";
		}
		});
	}
	/* End Function For Manual Publish if User is Not Liable To Pay and Has Already Enough Credits For Edited ad */

	
	/* Start Function For Edited Ads Payment Via Card */	
	$( "#paymentForm" ).submit(function( event ) {
		var min_budget = $('#min_budget').val();
		$('#ads_amount').val(min_budget);
		if(ads_amount == ''){
				Materialize.toast('Select the credit plan first.', 2000, 'red');
				return false;
			}
			else
			{
				var $is_stop = true;
				
				var name_on_card = $('#name_on_card').val();
				var card_number = $('#card_number').val();
				var expiry_month = $('#expiry_month').val();
				var expiry_year = $('#expiry_year').val();
				var cvv = $('#cvv').val();
				var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
				var password = $('#password').val();
				var reg_nm = /^[a-zA-Z\s]+$/;
				var spc = /^\s+$/;	
				var one_space = /^[A-Za-z]+\s+[A-Za-z]+$/;
				
				if(name_on_card == '')
				{
					Materialize.toast('Enter full name of the card holder.', 2000, 'red');
					$("#name_on_card").focus();
					$is_stop = false;
				}
				else if(!one_space.test(name_on_card))
				{
					Materialize.toast('Enter first and last name of the card holder.', 2000, 'red');
					$("#name_on_card").focus();
					$is_stop = false;
				}
				else if(card_number == '')
				{
					Materialize.toast('Enter card number.', 2000, 'red');
					$("#card_number").focus();
					$is_stop = false;
				}
				else if(expiry_month == '')
				{
					Materialize.toast('Enter expiry month.', 2000, 'red');
					$("#expiry_month").focus();
					$is_stop = false;
				}
				else if(expiry_year == '')
				{
					Materialize.toast('Enter expiry year.', 2000, 'red');
					$("#expiry_year").focus();
					$is_stop = false;
				}
				else if(cvv == '')
				{
					Materialize.toast('Enter CVV number.', 2000, 'red');
					$("#cvv").focus();
					$is_stop = false;
				}
				
				else
				{
					
					$is_stop = true;
				}	 
			}
			
			if($is_stop == true)
			{
				$('#cardSubmitBtn').attr("disabled",true);
			}
			return $is_stop;
	});
	/* End Function For Edited Ads Payment Via Card */
	
	$('body').on( "focusout","#min_budget",function (e) {
		var a = $(this).val();
		var b = parseFloat(a).toFixed(2);
		if(b != 'NaN')
		{
			$('#min_budget').val(b);
		}
		else
		{
			$('#min_budget').val('');
		}
	 	
	});