var oldLabel = '';
var $isCheckForFullDateTimeLabel = true;
var previousDate = '';
var previousTime = '';
var previousType = '';
var storedtemp = [];
var lastmsgpoint = '';
var Gab = {
    connection: null,
    jid_to_id: function (jid) {
        return Strophe.getBareJidFromJid(jid)
            .replace(/@/g, "-") 
            .replace(/\./g, "-");
    }, 

    on_roster: function (iq) {
        Gab.connection.addHandler(Gab.on_presence, null, "presence");
        Gab.connection.send($pres());
    },

    on_presence: function (presence) {
        return true;
    },

    on_roster_changed: function (iq) {
        return true;  
    },
 
    on_message: function(message) {
        $data = $(message).find("body");
        $all = $(message).find("all");
        $fromrow = $all['context']['attributes'][1]['value'];
        $torow = $all['context']['attributes'][2]['value'];
        $message = $all['context']['textContent'];

        $to = $torow.substr(0, $torow.indexOf('@')); 
        $from = $fromrow.substr(0, $fromrow.indexOf('@')); 
        
        filterlastmessage($message, $to, $from);
        return false;
    },
    onComplete: function(message) {
        return true;
    },
    onMessageWall: function(message) {
        if(message) {
            $data = $(message).find("body");
            $message = $data.context.innerHTML;
            storedtemp.push($message);
            //storedtemp = storedtemp.slice(-2);
        }
        return true;
    },
    onMessageWallComplete: function(message) {
        $blockData = $(message).find('first');
        $key = $blockData['0']['innerHTML'];
        lastmsgpoint = $key;

        if(storedtemp.length) { 
            $.ajax({ 
                url: '?r=messages/sethistory',
                type: 'POST',
                data: {storedtemp},
                success: function(data) {
                    storedtemp = [];
                    var result = JSON.parse(data);
                    $.each(result, function(i, v) {
                        eval(v.putblock);
                        scrollMessageBottom();
                    });
                }
            });
        }
        return true;
    }
};

function log(msg, data) {
    var tr = document.createElement('tr');
    var th = document.createElement('th');
    th.setAttribute( "style", "text-align: left; vertical-align: top;" );
    var td;
 
    th.appendChild( document.createTextNode(msg) );
    tr.appendChild( th );

    if (data) {
        td = document.createElement('td');
        pre = document.createElement('code');
        pre.setAttribute("style", "white-space: pre-wrap;");
        td.appendChild(pre);
        pre.appendChild( document.createTextNode( vkbeautify.xml(data) ) );
        tr.appendChild(td);
    } else {
        th.setAttribute('colspan', '2');
    }

    $('#log').append(tr);
}

function rawInput(data)
{
    //log('RECV', data);
}

function rawOutput(data)
{
    //log('SENT', data);
}

function messageSendFromMessagenew() {
    var $selector = $('#messages-inbox').find('ul').find('li').find('a.active');
    if($selector.length) {
        $id = $selector.attr('id');
        if($id) {
            $id = $id.replace("leftli_", "");
            $message = $('#inputMessageWall').val();
            if($.trim(message)) {
                $.ajax({ 
                    url: '?r=messages/get-category',
                    type: 'POST',
                    data: {id: $id},
                    success: function(result) {
                        var result = JSON.parse(result);
                        if(result.status) {
                            $userid = result.user_id;

                            var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'});
                            Gab.connection.sendIQ(iq, Gab.on_roster);
                            Gab.connection.addHandler(Gab.on_roster_changed, "jabber:iq:roster", "iq", "set");
                            Gab.connection.addHandler(Gab.on_message, "jabber:client", "message", "chat");
                            
                            var reply = $msg({to: $id+'@iaminaqaba.com', from: $userid+'@iaminaqaba.com', type: 'chat'})
                            .c('body').t($message).up()
                            .c('active', {xmlns: "http://jabber.org/protocol/chatstates"});

                            Gab.connection.send(reply);
                            
                            setleftsection($id, $message);
                            filterlastmessagemine($message, $id, $userid );

                            //getSigleMessage($id+'@iaminaqaba.com', 2);

                            $('textarea#inputMessageWall').val('');

                      } else if(result.status == false && result.reason != undefined) {
                          if(result.reason == 'block') {
                              var by = result.by;
                              if(by == 'self') {
                                Materialize.toast('You are not able to send message becuase you have blocked this person.', 2000, 'red');
                              } else {
                                Materialize.toast('This person isn\'t receiving messages from you at the moment.', 2000, 'red');
                              }
                          }
                      }             
                    }
                    });
            }
        }
    }
}

function setleftsection($id, $message) {
    if($('#messages-inbox').find('ul.users-display').find('li a#leftli_'+$id).length) {
        $leftmsgset = '<i class="msg-status mdi mdi-check"></i><i class="mdi mdi-reply"></i>'+$message;
        $('#messages-inbox').find('ul.users-display').find('li a#leftli_'+$id).find('.descholder').find('p').html($leftmsgset);
    }

}

function filterlastmessagemine($message, $to, $from) {
    //check last message is sender self...
    $datetime = '';
    $islastmine = 'no';

    if($to == data2.id) {
        $other = $from;
    } else {
        $other = $to;
    }

    if($('li#li_leftli_'+$other).find('li.msgli').length) {
        if($('li#li_leftli_'+$other).find('li.msgli:last').hasClass('msg-outgoing')) {
            $islastmine = 'yes';
            if($('li#li_leftli_'+$other).find('li.msgli:last').find('.msg-handle:last').find('p:last').length) {
                $datetime = $('li#li_leftli_'+$other).find('li.msgli:last').find('.msg-handle:last').find('p:last').attr('data-time');
            }
        }
    }

    $.ajax({ 
            url: '?r=messages/filterlastmessagemine',
            type: 'POST',
            data: {
                datetime: $datetime, 
                from: $from, 
                to: $to,
                islastmine: $islastmine,
                message: $message
            }, 
            success: function(data) {
                eval(data);
                scrollMessageBottom();
            }
    });
}

function filterlastmessage($message, $to, $from) {
    $datetime = '';
    $islastmine = 'no';
    isrightsection = 'no';
    isnullleft = 'yes';
    isleftsection = 'no';
    isputactive = false;


    /*
    $message = 'hi how are you';
    $to = '57972b76cf926ff93c90909f';
    $from = '579729e4cf926f773c90909f';
    */

    if($('.right-section').find('.current-messages').find('li#li_leftli_'+$from).length) {
        isrightsection = 'yes';
        $selector = $('.right-section').find('.current-messages').find('li#li_leftli_'+$from);
        if($selector.find('li.msgli').length) {
            if($selector.find('li.msgli:last').hasClass('msg-outgoing')) {
                $islastmine = 'yes';
                if($selector.find('li.msgli:last').find('.msg-handle:last').find('p:last').length) {
                    $datetime = $selector.find('li.msgli:last').find('.msg-handle:last').find('p:last').attr('data-time');
                }
            }
        }
    } else {

    }

    if($('#messages-inbox').find('ul.users-display').find('li a#leftli_'+$from).length) {
        isleftsection = 'yes';
    } 

    if($('#messages-inbox').find('ul.users-display').find('li.label-notice').length) {
        isnullleft = 'yes';
    } else if($('#messages-inbox').find('ul.users-display').find('li').length) {
        isnullleft = 'no';
    }   
     
    $.ajax({ 
        url: '?r=messages/filterlastmessage',
        type: 'POST',
        data: {
            datetime: $datetime, 
            islastmine: $islastmine, 
            message: $message,
            to: $to,
            from: $from,
            isleftsection: isleftsection,
            isrightsection: isrightsection,
            isnullleft: isnullleft
        }, 
        success: function(data) {
            eval(data);
            if($('#messages-inbox').find('ul.users-display').find('li').find('a#leftli_'+$from).hasClass('active')) {
                $('.right-section').find('.current-messages').find('li#li_leftli_'+$from).removeClass('active');
                $('.right-section').find('.current-messages').find('li#li_leftli_'+$from).addClass('active');
            }
            scrollMessageBottom();
        }
    });
}

function getHistory(peer, count) { 
    var iq = $iq({type: "set", id: peer.split("@")[0]})
    .c('query', {xmlns: "urn:xmpp:mam:2"})  
    .c('x', {xmlns: "jabber:x:data", type: "submit"})
    .c('field', {var: "FORM_TYPE", type: "hidden"})
    .c('value').t("urn:xmpp:mam:2")
    .up().up()
    .c('field', {var: "with"})
    .c('value').t(peer)
    .up()
    .up()
    .up()
    .c('set', {xmlns: "http://jabber.org/protocol/rsm"})
    .c('max').t(count)
    .up()
    //.c('reversed')
    .up()
    .c('before').t('')

    Gab.connection.send(iq, function () {
    });
}   

function getbasicinfoouser(id) {
    $.ajax({  
        url: '?r=messages/getbasicinfoouser',
        type: 'POST',
        data: {id},
        success: function(result) {
            var $result = JSON.parse(result);
            $fullname = $result.fullname;
            $country = $.trim($result.country);
            $lastseen = $result.lastseen;
            $thumbnail = $result.thumbnail;
            $time = getTime();
            $('.right-section').find('.msgwindow-name').html('<div class="imgholder dot_online"><img src="'+$thumbnail+'"></div> <span class="desc-holder">'+$fullname+'</span><span class="person_status">'+$lastseen+'</span><span class="usercountry">&nbsp;|&nbsp;'+$country+'</span><span class="usertime">&nbsp;|&nbsp;'+$time+'</span>');
        }
    });
}

function removeLoaderImg(obj){
    $(obj).css("background","none");
}

function getRecentMessagsUsers($isStart=0) { 
    $.ajax({ 
        type: 'GET',
        url: '?r=messages/get-recent-messages-users',
        success: function(data) {
            if(data) {
                var $result = JSON.parse(data);
                $selector = $('#messages-inbox').find('.message-userlist').find('ul');
                if($isStart == 0 || $isStart == '') {
                  $selector.html('');
                }
                $.each($result, function(i, v) {                              
                    $onlinehtml = '';
                    //if($.inArray($to_id, $getrecentmessageusersid) !== -1) {
                        $onlinehtml = '<span class="online-dot"></span>';
                    //}

                    var $thumb = v.thumbnail; 
                    $gender = v.gender;

                    imageExists('profile/'+$thumb, function(exists) {
                        if(!exists) {
                            $thumb = $gender+'.png';
                        }
                        $fullname = v.fullname;
                        //$type = v.type;
                        $type = '';
                        $message = v.message;
                        $from_id = v.from;
                        $otherid = v.other;
                        $to_id = v.to;
                        $isSenderSelf = v.isSenderSelf;
                        $time = '';
                        $sender = false;
                        
                        if($from_id == data2.id) { $sender = true; }
                        if($type == 'image') {
                            if($sender) { $box = '<i class="mdi mdi-file-image"></i> You sent a photo'; } else { $box = '<i class="mdi mdi-file-image"></i> You received a photo'; }
                        } else if($type == 'gift' && $category == 'inbox'){
                            if($sender) { $box = '<i class="mdi mdi-gift"></i> You sent a gift'; } else { $box = '<i class="mdi mdi-gift"></i> You received a gift'; }
                        } else {
                            if($message != undefined && $message != null) {
                              $message = $.emoticons.replace($message);
                            } else {
                              $message = '';
                            }
                            if($sender) { 
                                $box = '<i class="mdi mdi-reply"></i> '+$message; 
                            } else { 
                                $box = $message; 
                            }
                        }

                        var cls ='';
                        if(i == 0) {
                            cls = 'active';
                            var firstLi = $('.right-section ul.current-messages').find('li#li_leftli_'+$otherid);
                            if(!firstLi.length) {
                                $tempSel = $('.right-section').find('ul.current-messages');
                                $li = '<li class="mainli active" id="li_leftli_'+$otherid+'"> <div class="msgdetail-list nice-scroll" tabindex="18" style="opacity: 1; overflow-y: hidden; outline: none;"><div class="msglist-holder images-container"><ul class="outer"></ul></div></div></li>';
                                $($tempSel).append($li)
                            } else { 
                                firstLi.addClass('active'); 
                            }
                            getbasicinfoouser($otherid);
                            getHistory($otherid+"@iaminaqaba.com", "20");


                            //getHistory('57972b76cf926ff93c90909f@iaminaqaba.com', "5");
                        }
                        $li ='<li><a href="javascript:void(0)" class="'+cls+'" id="leftli_'+$otherid+'" onclick="openMessage(this);"><span class="muser-holder"> <span class="imgholder"><img src="profile/'+$thumb+'"></span>'+$onlinehtml+'<span class="descholder"><h5>'+$fullname+'</h5><p><i class="msg-status mdi mdi-check"></i>'+$box+'</p><span class="timestamp">'+$time+'</span></span></span></a></li>';
                        $selector.append($li);
                    });

                });
            } else {
                $norecord = "<li class='label-notice'><div class='no-listcontent'>No record found.</div></li>";
                var sparent=$(".messages-list .left-section #messages-inbox");
                removeLoaderImg(sparent);
                $('.msgwindow-name').html('<div class="imgholder"></div> <span class="desc-holder"></span><span class="person_status"></span> <span class="usertime"></span> <span class="usercountry"></span>');

                sparent.find('ul.users-display').html($norecord);
                setOpacity(sparent.find('.loading-holder'),1);
            }

        },
        complete: function() {
            var sparent=$(".messages-list .left-section #messages-inbox");
            removeLoaderImg(sparent);
            setOpacity(sparent.find('.loading-holder'),1);
        }
    });
}

function enablevideobox() {
    $('#detailbox').hide();
    $('#callbox').show();
}

function disablevideobox() {
    $('#callbox').hide();
    $('#detailbox').show();
}

function doCall(peer) {
    if(peer) {
        connection.send($pres({to:peer})); // sharing directed presence is a good idea
        connection.jingle.initiate(peer, Gab.connection.jid);
        return false;
    }
}

$(document).ready(function () {
    
    $(document).on('click', '#callReceived', function() {
        // call received
    });

    $(document).on('click', '#callRejected', function() {
        // call rejected
    });

    $(document).on('click', '#videocamcall', function() {
        if($('.users-display').length) {
            if($('.users-display').find('li').find('a.active').length) {
                $selector = $('.users-display').find('li').find('a.active');
                $id = $selector.attr('id')
                $id = $id.replace('leftli_', '');
                if($id) {
                    enablevideobox();
                    $src = $selector.find('.imgholder').find('img').attr('src');
                    $('#box1').find('img').attr('src', $src);
                    getUserMediaWithConstraints(['audio', 'video']);
                    $peerId = $id +'@'+ DOMAIN;
                    doCall($peerId);
                }
            }
        }
    });

    $(document).on('keypress', '#inputMessageWall', function(e) {
        if (e.which == 13) {
            messageSendFromMessagenew();
        }        
    });

    // call ajax function to check user is logged or not and if yes then get uid and upwd othewise false.
    $.ajax({
        type: 'GET',
        url: '?r=site/stropheloginchk',
        success: function(data){
            var result = $.parseJSON(data);   
            if(result.status != null && result.status == true) {
                //var BOSH_SERVICE = 'http://34.217.21.181:5280/http-bind';
                var connection = new Strophe.Connection(BOSH_SERVICE);
                var Uid = result.id;
                var Upwd = result.pwd;
                var Domain = '@iaminaqaba.com';
                connection.connect(Uid+Domain, '1', function (status) {
                    if (status == Strophe.Status.CONNECTING) {
                    } else if (status == Strophe.Status.CONNECTED) {
                        var iq = $iq({type: 'get'}).c('query', {xmlns: 'jabber:iq:roster'});
                        Gab.connection.sendIQ(iq, Gab.on_roster);
                        Gab.connection.addHandler(Gab.on_roster_changed, "jabber:iq:roster", "iq", "set");
                        Gab.connection.addHandler(Gab.on_message, "jabber:client", "message", "chat");
                        Gab.connection.addHandler(Gab.onMessageWall, "urn:xmpp:mam:2", 'message', null);
                        Gab.connection.addHandler(Gab.onMessageWallComplete, "urn:xmpp:mam:2", "iq", null);

                        getRecentMessagsUsers();    

                        connection.jingle.getStunAndTurnCredentials();

                        // disco stuff
                        if (connection.disco) {
                            connection.disco.addIdentity('client', 'web');
                            connection.disco.addFeature(Strophe.NS.DISCO_INFO);
                        }
                        
                        $(document).trigger('connected');


                    } else if (status == Strophe.Status.DISCONNECTED) {
                        //$(document).trigger('disconnected');
                    } else if (status == Strophe.Status.CONNFAIL) {
                    }
                }); 

                Gab.connection = connection;
            }
        }
    });
});
    -
$(document).bind('connected', function () {
    //getHistory("adel@iaminaqaba.com", "5");
});

$(document).bind('disconnected', function () {
    Gab.connection = null;
    Gab.pending_subscriber = null;
});
