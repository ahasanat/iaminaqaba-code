function buy_credits()
{
	var buy_credits = $('#buy_credits').val();
	if(buy_credits == ''){
		return false;
	}
	else
	{
		$.ajax({
			url: '?r=vip/buycredits', 
			type: 'POST',
			data: 'buy_credits='+buy_credits,
			success: function (data) {
				document.getElementById("myformsubmit").submit();
			}
		});
		return false;
	}
	return false;
}


/* Payment Via Card  */
	
	$( "#paymentForm" ).submit(function( event ) {
		var buy_credits = $('#buy_credits').val();
		if(buy_credits == ''){
				Materialize.toast('Select The Credit Plan First.', 2000, 'red');
				return false;
			}
			else
			{
				var $is_stop = true;
				$.ajax({
					url: '?r=vip/buycredits', 
					type: 'POST',
					async: false,
					data: 'buy_credits=' + buy_credits,
					success: function (data) {
						
						if(data == '1'){
							
							var name_on_card = $('#name_on_card').val();
							var card_number = $('#card_number').val();
							var expiry_month = $('#expiry_month').val();
							var expiry_year = $('#expiry_year').val();
							var cvv = $('#cvv').val();
							var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
							var password = $('#password').val();
							var reg_nm = /^[a-zA-Z\s]+$/;
							var spc = /^\s+$/;	
							var one_space = /^[A-Za-z]+\s+[A-Za-z]+$/;
							
							if(name_on_card == '')
							{
								Materialize.toast('Enter full name of the card holder.', 2000, 'red');
								$("#name_on_card").focus();
								$is_stop = false;
							}
							else if(!one_space.test(name_on_card))
							{
								Materialize.toast('Enter full First and last name of the card holder.', 2000, 'red');
								$("#name_on_card").focus();
								$is_stop = false;
							}
							else if(card_number == '')
							{
								Materialize.toast('Enter card number.', 2000, 'red');
								$("#card_number").focus();
								$is_stop = false;
							}
							else if(expiry_month == '')
							{
								Materialize.toast('Enter expiry month.', 2000, 'red');
								$("#expiry_month").focus();
								$is_stop = false;
							}
							else if(expiry_year == '')
							{
								Materialize.toast('Enter expiry year.', 2000, 'red');
								$("#expiry_year").focus();
								$is_stop = false;
							}
							else if(cvv == '')
							{
								Materialize.toast('Enter CVV number.', 2000, 'red');
								$("#cvv").focus();
								$is_stop = false;
							}
							
						}
						else if(data == '2'){
							$is_stop = false;
						}
						else{
							$is_stop = false;
						}	
					}
				});
				if($is_stop == true)
				{
					$('#cardSubmitBtn').attr("disabled",true);
				}
				return $is_stop;
			}
	});
	
	/* Payment Via Card */
	