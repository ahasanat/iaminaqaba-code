<?php
namespace yii\web; 

use Yii;
use yii\base\InlineAction;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;

use frontend\models\BookmarkForm;
use frontend\models\UnfollowConnect;
use frontend\models\MuteConnect;
use frontend\models\SavePost;
use frontend\models\ReportPost; 
use frontend\models\CountryCode;
use frontend\models\Comment;
use frontend\models\HideComment;
use frontend\models\HidePost;
use frontend\models\Like;
use frontend\models\Notification;
use frontend\models\Language;
use frontend\models\Education;
use frontend\models\Interests;
use frontend\models\Occupation;
use frontend\models\UserForm;
use frontend\models\LoginForm;
use frontend\models\UserSetting;
use frontend\models\Personalinfo;
use frontend\models\SecuritySetting;
use frontend\models\NotificationSetting;
use frontend\models\PostForm;
use frontend\models\Connect;
use frontend\models\TurnoffNotification;
use frontend\models\PinImage;
use frontend\models\BlockConnect;
use frontend\models\Page;
use frontend\models\CollectionFollow;
use frontend\models\PageEvents;
use frontend\models\EventVisitors;
use frontend\models\Referal;
use frontend\models\TravAds;
use frontend\models\Trip;
use frontend\models\PageEndorse;
use frontend\models\DefaultImage;
use frontend\models\Vip;
use frontend\models\Verify;
use frontend\models\PlaceAsk;
use frontend\models\PlaceTip;
use frontend\models\PlaceReview;
use frontend\models\PlaceDiscussion;
use frontend\models\Gallery;
use backend\models\Googlekey;

$session = Yii::$app->session; 
$user_id = (string)$session->get('user_id');
$Auth = '';
if(isset($user_id) && $user_id != '') 
{
$authstatus = UserForm::isUserExistByUid($user_id);
if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
{
    $Auth = $authstatus;
}
}   
else    
{
    $Auth = 'checkuserauthclassg';
}



/*

 * Controller is the base class of web controllers.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Controller extends \yii\base\Controller
{
    public $enableCsrfValidation = true;
 
    public $actionParams = [];

    public $assetsPath = '../../vendor/bower/travel/images/';

    public function renderAjax($view, $params = [])
    {
        return $this->getView()->renderAjax($view, $params, $this);
    }

    public function bindActionParams($action, $params)
    {
        if ($action instanceof InlineAction) {
            $method = new \ReflectionMethod($this, $action->actionMethod);
        } else {
            $method = new \ReflectionMethod($action, 'run');
        }

        $args = [];
        $missing = [];
        $actionParams = [];
        foreach ($method->getParameters() as $param) {
            $name = $param->getName();
            if (array_key_exists($name, $params)) {
                if ($param->isArray()) {
                    $args[] = $actionParams[$name] = (array) $params[$name];
                } elseif (!is_array($params[$name])) {
                    $args[] = $actionParams[$name] = $params[$name];
                } else {
                    throw new BadRequestHttpException(Yii::t('yii', 'Invalid data received for parameter "{param}".', [
                        'param' => $name,
                    ]));
                }
                unset($params[$name]);
            } elseif ($param->isDefaultValueAvailable()) {
                $args[] = $actionParams[$name] = $param->getDefaultValue();
            } else {
                $missing[] = $name;
            }
        }

        if (!empty($missing)) {
            throw new BadRequestHttpException(Yii::t('yii', 'Missing required parameters: {params}', [
                'params' => implode(', ', $missing),
            ]));
        }

        $this->actionParams = $actionParams;

        return $args;
    }

    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if ($this->enableCsrfValidation && Yii::$app->getErrorHandler()->exception === null && !Yii::$app->getRequest()->validateCsrfToken()) {
                throw new BadRequestHttpException(Yii::t('yii', 'Unable to verify your data submission.'));
            }
            return true;
        }
        
        return false;
    }

    public function redirect($url, $statusCode = 302)
    {
        return Yii::$app->getResponse()->redirect(Url::to($url), $statusCode);
    }

    public function goHome()
    {
        return Yii::$app->getResponse()->redirect(Yii::$app->getHomeUrl());
    }

    public function goBack($defaultUrl = null)
    {
        return Yii::$app->getResponse()->redirect(Yii::$app->getUser()->getReturnUrl($defaultUrl));
    }

    public function refresh($anchor = '')
    {
        return Yii::$app->getResponse()->redirect(Yii::$app->getRequest()->getUrl() . $anchor);
    }
    
    public function getcontinent($country)
    {
        $continent = null;
        if( $country == 'Afghanistan' ) $continent = 'Asia';
        if( $country == 'Aland Islands' ) $continent = 'Europe';
        if( $country == 'Albania' ) $continent = 'Europe';
        if( $country == 'Algeria' ) $continent = 'Africa';
        if( $country == 'American Samoa' ) $continent = 'Oceania';
        if( $country == 'Andorra' ) $continent = 'Europe';
        if( $country == 'Angola' ) $continent = 'Africa';
        if( $country == 'Anguilla' ) $continent = 'North America';
        if( $country == 'Antarctica' ) $continent = 'Antarctica';
        if( $country == 'Antigua and Barbuda' ) $continent = 'North America';
        if( $country == 'Argentina' ) $continent = 'South America';
        if( $country == 'Armenia' ) $continent = 'Asia';
        if( $country == 'Aruba' ) $continent = 'North America';
        if( $country == 'Australia' ) $continent = 'Oceania';
        if( $country == 'Austria' ) $continent = 'Europe';
        if( $country == 'Azerbaijan' ) $continent = 'Asia';
        if( $country == 'Bahamas' ) $continent = 'North America';
        if( $country == 'Bahrain' ) $continent = 'Asia';
        if( $country == 'Bangladesh' ) $continent = 'Asia';
        if( $country == 'Barbados' ) $continent = 'North America';
        if( $country == 'Belarus' ) $continent = 'Europe';
        if( $country == 'Belgium' ) $continent = 'Europe';
        if( $country == 'Belize' ) $continent = 'North America';
        if( $country == 'Benin' ) $continent = 'Africa';
        if( $country == 'Bermuda' ) $continent = 'North America';
        if( $country == 'Bhutan' ) $continent = 'Asia';
        if( $country == 'Bolivia' ) $continent = 'South America';
        if( $country == 'Bosnia and Herzegovina' ) $continent = 'Europe';
        if( $country == 'Botswana' ) $continent = 'Africa';
        if( $country == 'Bouvet Island' ) $continent = 'Antarctica';
        if( $country == 'Brazil' ) $continent = 'South America';
        if( $country == 'British Indian Ocean Territory' ) $continent = 'Asia';
        if( $country == 'Brunei Darussalam' ) $continent = 'Asia';
        if( $country == 'Bulgaria' ) $continent = 'Europe';
        if( $country == 'Burkina Faso' ) $continent = 'Africa';
        if( $country == 'Burundi' ) $continent = 'Africa';
        if( $country == 'Cambodia' ) $continent = 'Asia';
        if( $country == 'Cameroon' ) $continent = 'Africa';
        if( $country == 'Canada' ) $continent = 'North America';
        if( $country == 'Cape Verde' ) $continent = 'Africa';
        if( $country == 'Cayman Islands' ) $continent = 'North America';
        if( $country == 'Central African Republic' ) $continent = 'Africa';
        if( $country == 'Chad' ) $continent = 'Africa';
        if( $country == 'Chile' ) $continent = 'South America';
        if( $country == 'China' ) $continent = 'Asia';
        if( $country == 'Christmas Island' ) $continent = 'Asia';
        if( $country == 'Cocos (Keeling) Islands' ) $continent = 'Asia';
        if( $country == 'Colombia' ) $continent = 'South America';
        if( $country == 'Comoros' ) $continent = 'Africa';
        if( $country == 'The Democratic Republic of The Congo' ) $continent = 'Africa';
        if( $country == 'Congo' ) $continent = 'Africa';
        if( $country == 'Cook Islands' ) $continent = 'Oceania';
        if( $country == 'Costa Rica' ) $continent = 'North America';
        if( $country == "Cote D'ivoire" ) $continent = 'Africa';
        if( $country == 'Croatia' ) $continent = 'Europe';
        if( $country == 'Cuba' ) $continent = 'North America';
        if( $country == 'Cyprus' ) $continent = 'Asia';
        if( $country == 'Czech Republic' ) $continent = 'Europe';
        if( $country == 'Denmark' ) $continent = 'Europe';
        if( $country == 'Djibouti' ) $continent = 'Africa';
        if( $country == 'Dominica' ) $continent = 'North America';
        if( $country == 'Dominican Republic' ) $continent = 'North America';
        if( $country == 'Ecuador' ) $continent = 'South America';
        if( $country == 'Egypt' ) $continent = 'Africa';
        if( $country == 'El Salvador' ) $continent = 'North America';
        if( $country == 'Equatorial Guinea"' ) $continent = 'Africa';
        if( $country == 'Eritrea' ) $continent = 'Africa';
        if( $country == 'Estonia' ) $continent = 'Europe';
        if( $country == 'Ethiopia' ) $continent = 'Africa';
        if( $country == 'Faroe Islands' ) $continent = 'Europe';
        if( $country == 'Falkland Islands (Malvinas)' ) $continent = 'South America';
        if( $country == 'Fiji' ) $continent = 'Oceania';
        if( $country == 'Finland' ) $continent = 'Europe';
        if( $country == 'France' ) $continent = 'Europe';
        if( $country == 'French Guiana' ) $continent = 'South America';
        if( $country == 'French Polynesia' ) $continent = 'Oceania';
        if( $country == 'French Southern Territories' ) $continent = 'Antarctica';
        if( $country == 'Gabon' ) $continent = 'Africa';
        if( $country == 'Gambia' ) $continent = 'Africa';
        if( $country == 'Georgia' ) $continent = 'Asia';
        if( $country == 'Germany' ) $continent = 'Europe';
        if( $country == 'Ghana' ) $continent = 'Africa';
        if( $country == 'Gibraltar' ) $continent = 'Europe';
        if( $country == 'Greece' ) $continent = 'Europe';
        if( $country == 'Greenland' ) $continent = 'North America';
        if( $country == 'Grenada' ) $continent = 'North America';
        if( $country == 'Guadeloupe' ) $continent = 'North America';
        if( $country == 'Guam' ) $continent = 'Oceania';
        if( $country == 'Guatemala' ) $continent = 'North America';
        if( $country == 'Guernsey' ) $continent = 'Europe';
        if( $country == 'Guinea' ) $continent = 'Africa';
        if( $country == 'Guinea-bissau' ) $continent = 'Africa';
        if( $country == 'Guyana' ) $continent = 'South America';
        if( $country == 'Haiti' ) $continent = 'North America';
        if( $country == 'Heard Island and Mcdonald Islands' ) $continent = 'Antarctica';
        if( $country == 'Holy See (Vatican City State)' ) $continent = 'Europe';
        if( $country == 'Honduras' ) $continent = 'North America';
        if( $country == 'Hong Kong' ) $continent = 'Asia';
        if( $country == 'Hungary' ) $continent = 'Europe';
        if( $country == 'Iceland' ) $continent = 'Europe';
        if( $country == 'India' ) $continent = 'Asia';
        if( $country == 'Indonesia' ) $continent = 'Asia';
        if( $country == 'Iran' ) $continent = 'Asia';
        if( $country == 'Iraq' ) $continent = 'Asia';
        if( $country == 'Ireland' ) $continent = 'Europe';
        if( $country == 'Isle of Man' ) $continent = 'Europe';
        if( $country == 'Israel' ) $continent = 'Asia';
        if( $country == 'Italy' ) $continent = 'Europe';
        if( $country == 'Jamaica' ) $continent = 'North America';
        if( $country == 'Japan' ) $continent = 'Asia';
        if( $country == 'Jersey' ) $continent = 'Europe';
        if( $country == 'Jordan' ) $continent = 'Asia';
        if( $country == 'Kazakhstan' ) $continent = 'Asia';
        if( $country == 'Kenya' ) $continent = 'Africa';
        if( $country == 'Kiribati' ) $continent = 'Oceania';
        if( $country == "Democratic People's Republic of Korea" ) $continent = 'Asia';
        if( $country == 'Republic of Korea' ) $continent = 'Asia';
        if( $country == 'Kuwait' ) $continent = 'Asia';
        if( $country == 'Kyrgyzstan' ) $continent = 'Asia';
        if( $country == "Lao People's Democratic Republic" ) $continent = 'Asia';
        if( $country == 'Latvia' ) $continent = 'Europe';
        if( $country == 'Lebanon' ) $continent = 'Asia';
        if( $country == 'Lesotho' ) $continent = 'Africa';
        if( $country == 'Liberia' ) $continent = 'Africa';
        if( $country == 'Libya' ) $continent = 'Africa';
        if( $country == 'Liechtenstein' ) $continent = 'Europe';
        if( $country == 'Lithuania' ) $continent = 'Europe';
        if( $country == 'Luxembourg' ) $continent = 'Europe';
        if( $country == 'Macao' ) $continent = 'Asia';
        if( $country == 'Macedonia' ) $continent = 'Europe';
        if( $country == 'Madagascar' ) $continent = 'Africa';
        if( $country == 'Malawi' ) $continent = 'Africa';
        if( $country == 'Malaysia' ) $continent = 'Asia';
        if( $country == 'Maldives' ) $continent = 'Asia';
        if( $country == 'Mali' ) $continent = 'Africa';
        if( $country == 'Malta' ) $continent = 'Europe';
        if( $country == 'Marshall Islands' ) $continent = 'Oceania';
        if( $country == 'Martinique' ) $continent = 'North America';
        if( $country == 'Mauritania' ) $continent = 'Africa';
        if( $country == 'Mauritius' ) $continent = 'Africa';
        if( $country == 'Mayotte' ) $continent = 'Africa';
        if( $country == 'Mexico' ) $continent = 'North America';
        if( $country == 'Micronesia' ) $continent = 'Oceania';
        if( $country == 'Moldova' ) $continent = 'Europe';
        if( $country == 'Monaco' ) $continent = 'Europe';
        if( $country == 'Mongolia' ) $continent = 'Asia';
        if( $country == 'Montenegro' ) $continent = 'Europe';
        if( $country == 'Montserrat' ) $continent = 'North America';
        if( $country == 'Morocco' ) $continent = 'Africa';
        if( $country == 'Mozambique' ) $continent = 'Africa';
        if( $country == 'Myanmar' ) $continent = 'Asia';
        if( $country == 'Namibia' ) $continent = 'Africa';
        if( $country == 'Nauru' ) $continent = 'Oceania';
        if( $country == 'Nepal' ) $continent = 'Asia';
        if( $country == 'Netherlands Antilles' ) $continent = 'North America';
        if( $country == 'Netherlands' ) $continent = 'Europe';
        if( $country == 'New Caledonia' ) $continent = 'Oceania';
        if( $country == 'New Zealand' ) $continent = 'Oceania';
        if( $country == 'Nicaragua' ) $continent = 'North America';
        if( $country == 'Niger' ) $continent = 'Africa';
        if( $country == 'Nigeria' ) $continent = 'Africa';
        if( $country == 'Niue' ) $continent = 'Oceania';
        if( $country == 'Norfolk Island' ) $continent = 'Oceania';
        if( $country == 'Northern Mariana Islands' ) $continent = 'Oceania';
        if( $country == 'Norway' ) $continent = 'Europe';
        if( $country == 'Oman' ) $continent = 'Asia';
        if( $country == 'Pakistan' ) $continent = 'Asia';
        if( $country == 'Palau' ) $continent = 'Oceania';
        if( $country == 'Palestinia' ) $continent = 'Asia';
        if( $country == 'Panama' ) $continent = 'North America';
        if( $country == 'Papua New Guinea' ) $continent = 'Oceania';
        if( $country == 'Paraguay' ) $continent = 'South America';
        if( $country == 'Peru' ) $continent = 'South America';
        if( $country == 'Philippines' ) $continent = 'Asia';
        if( $country == 'Pitcairn' ) $continent = 'Oceania';
        if( $country == 'Poland' ) $continent = 'Europe';
        if( $country == 'Portugal' ) $continent = 'Europe';
        if( $country == 'Puerto Rico' ) $continent = 'North America';
        if( $country == 'Qatar' ) $continent = 'Asia';
        if( $country == 'Reunion' ) $continent = 'Africa';
        if( $country == 'Romania' ) $continent = 'Europe';
        if( $country == 'Russian Federation' ) $continent = 'Europe';
        if( $country == 'Rwanda' ) $continent = 'Africa';
        if( $country == 'Saint Helena' ) $continent = 'Africa';
        if( $country == 'Saint Kitts and Nevis' ) $continent = 'North America';
        if( $country == 'Saint Lucia' ) $continent = 'North America';
        if( $country == 'Saint Pierre and Miquelon' ) $continent = 'North America';
        if( $country == 'Saint Vincent and The Grenadines' ) $continent = 'North America';
        if( $country == 'Samoa' ) $continent = 'Oceania';
        if( $country == 'San Marino' ) $continent = 'Europe';
        if( $country == 'Sao Tome and Principe' ) $continent = 'Africa';
        if( $country == 'Saudi Arabia' ) $continent = 'Asia';
        if( $country == 'Senegal' ) $continent = 'Africa';
        if( $country == 'Serbia' ) $continent = 'Europe';
        if( $country == 'Seychelles' ) $continent = 'Africa';
        if( $country == 'Sierra Leone' ) $continent = 'Africa';
        if( $country == 'Singapore' ) $continent = 'Asia';
        if( $country == 'Slovakia' ) $continent = 'Europe';
        if( $country == 'Slovenia' ) $continent = 'Europe';
        if( $country == 'Solomon Islands' ) $continent = 'Oceania';
        if( $country == 'Somalia' ) $continent = 'Africa';
        if( $country == 'South Africa' ) $continent = 'Africa';
        if( $country == 'South Georgia and The South Sandwich Islands' ) $continent = 'Antarctica';
        if( $country == 'Spain' ) $continent = 'Europe';
        if( $country == 'Sri Lanka' ) $continent = 'Asia';
        if( $country == 'Sudan' ) $continent = 'Africa';
        if( $country == 'Suriname' ) $continent = 'South America';
        if( $country == 'Svalbard and Jan Mayen' ) $continent = 'Europe';
        if( $country == 'Swaziland' ) $continent = 'Africa';
        if( $country == 'Sweden' ) $continent = 'Europe';
        if( $country == 'Switzerland' ) $continent = 'Europe';
        if( $country == 'Syrian Arab Republic' ) $continent = 'Asia';
        if( $country == 'Taiwan, Province of China' ) $continent = 'Asia';
        if( $country == 'Tajikistan' ) $continent = 'Asia';
        if( $country == 'Tanzania' ) $continent = 'Africa';
        if( $country == 'Thailand' ) $continent = 'Asia';
        if( $country == 'Timor-leste' ) $continent = 'Asia';
        if( $country == 'Togo' ) $continent = 'Africa';
        if( $country == 'Tokelau' ) $continent = 'Oceania';
        if( $country == 'Tonga' ) $continent = 'Oceania';
        if( $country == 'Trinidad and Tobago' ) $continent = 'North America';
        if( $country == 'Tunisia' ) $continent = 'Africa';
        if( $country == 'Turkey' ) $continent = 'Asia';
        if( $country == 'Turkmenistan' ) $continent = 'Asia';
        if( $country == 'Turks and Caicos Islands' ) $continent = 'North America';
        if( $country == 'Tuvalu' ) $continent = 'Oceania';
        if( $country == 'Uganda' ) $continent = 'Africa';
        if( $country == 'Ukraine' ) $continent = 'Europe';
        if( $country == 'United Arab Emirates' ) $continent = 'Asia';
        if( $country == 'United Kingdom' ) $continent = 'Europe';
        if( $country == 'United States' ) $continent = 'North America';
        if( $country == 'United States Minor Outlying Islands' ) $continent = 'Oceania';
        if( $country == 'Virgin Islands, U.S.' ) $continent = 'North America';
        if( $country == 'Uruguay' ) $continent = 'South America';
        if( $country == 'Uzbekistan' ) $continent = 'Asia';
        if( $country == 'Vanuatu' ) $continent = 'Oceania';
        if( $country == 'Virgin Islands, British' ) $continent = 'North America';
        if( $country == 'Venezuela' ) $continent = 'South America';
        if( $country == 'Viet Nam' ) $continent = 'Asia';
        if( $country == 'Wallis and Futuna' ) $continent = 'Oceania';
        if( $country == 'Western Sahara' ) $continent = 'Africa';
        if( $country == 'Yemen' ) $continent = 'Asia';
        if( $country == 'Zambia' ) $continent = 'Africa';
        if( $country == 'Zimbabwe' ) $continent = 'Africa';
        return $continent;
    }
    
    public function getuserimageforadmin($userid,$type)
    {
          
        

        $resultimg = UserForm::find()->where(['_id' => $userid])->one();
        if(substr($resultimg['photo'],0,4) == 'http')
        {
            if($type == 'photo')
            {
                $dp = $resultimg['photo'];
            }
            else
            {
                $dp = $resultimg['thumbnail'];
            }
        }
        else 
        {
            if(isset($resultimg['thumbnail']) && !empty($resultimg['thumbnail']) && file_exists($this->assetsPath.$resultimg['gender'].'.jpg'))
            {
                $dp = "../frontend/web/profile/".$resultimg['thumbnail'];
            }
            else if(isset($resultimg['gender']) && !empty($resultimg['gender']) && file_exists($this->assetsPath.$resultimg['gender'].'.jpg'))
            {
                $dp = $this->assetsPath.$resultimg['gender'].'.jpg';
            }
            else
            {
                $dp = $this->assetsPath."Male.jpg";
            }
        }
        return $dp;
    }

    public function filtergetimage($resultimg)
    {
          
        
        if(isset($resultimg['photo']) && substr($resultimg['photo'],0,4) == 'http')
        {
            if($type == 'photo') {
                $dp = $resultimg['photo'];
            } else {
                $dp = $resultimg['thumbnail'];
            }
        } else {
            if(isset($resultimg['thumbnail']) && !empty($resultimg['thumbnail']) && file_exists('profile/'.$resultimg['thumbnail']))
            {
                $dp = "profile/".$resultimg['thumbnail'];
            } else if(isset($resultimg['gender']) && !empty($resultimg['gender']) && file_exists($this->assetsPath.$resultimg['gender'].'.jpg'))
            {
                $dp = $this->assetsPath.$resultimg['gender'].'.jpg';
            } else {
                $dp = $this->assetsPath."DefaultGender.jpg";
            }
        }
        return $dp;
    }

    public function msgGetIdsDATA($ids)
    {
          
        
        $usrData = LoginForm::find()->select(['fullname', 'photo', 'thumbnail'])->where(['in', (string)'_id', $ids])->limit(2)->offset(0)->asarray()->all();

        $result = array(); 
        foreach ($usrData as $S_usrData) {
            $uid = (string)$S_usrData['_id'];
            $fullname = $S_usrData['fullname'];
            $photo = isset($S_usrData['photo']) ? $S_usrData['photo'] : '';
            $thumb = isset($S_usrData['thumbnail']) ? $S_usrData['thumbnail'] : '';
            $gender = isset($S_usrData['gender']) ? $S_usrData['gender'].'.jpg' : 'Male.jpg';

            if($photo != '' && substr($photo,0,4) == 'http') {
                $dp = $thumb;
            } else {
                if($thumb != '' && file_exists('profile/'.$thumb))
                {
                    $dp = "profile/".$thumb;
                }
                else
                {
                    $dp = $this->assetsPath.$gender;
                }
            }

            $result[$uid]['fullname'] = $fullname;
            $result[$uid]['thumb'] = $dp;
        }
        
        return $result;
    }


    public function getimage($userid,$type)
    {
        
        $resultimg = LoginForm::find()->where(['_id' => $userid])->one();
        
        if(substr($resultimg['photo'],0,4) == 'http')
        {
            if($type == 'photo')
            {
                $dp = $resultimg['photo'];
            }
            else
            {
                $dp = $resultimg['thumbnail'];
            }
        }
        else
        {
            if(isset($resultimg['thumbnail']) && !empty($resultimg['thumbnail']) && file_exists('profile/'.$resultimg['thumbnail']))
            {
                $dp = "profile/".$resultimg['thumbnail'];
            }
            else if(isset($resultimg['gender']) && !empty($resultimg['gender']) && file_exists($this->assetsPath.$resultimg['gender'].'.jpg'))
            {
                $dp = $this->assetsPath.$resultimg['gender'].'.jpg';
            }
            else
            {
                $dp = $this->assetsPath."DefaultGender.jpg";
            }
        }
        return $dp;
    }
    
    public function getimageAPI($userid,$type)
    {   
        $resultimg = LoginForm::find()->where(['_id' => $userid])->one();
        if(substr($resultimg['photo'],0,4) == 'http')
        {
            if($type == 'photo')
            {
                $dp = $resultimg['photo'];
            }
            else
            {
                $dp = $resultimg['thumbnail'];
            }
        }
        else
        {
            if(isset($resultimg['thumbnail']) && !empty($resultimg['thumbnail']))
            {
                $dp = "profile/".$resultimg['thumbnail'];
            }
            else if(isset($resultimg['gender']) && !empty($resultimg['gender']))
            {
                $dp = $this->assetsPath.$resultimg['gender'].'.jpg';
            }
            else
            {
                $dp = $this->assetsPath."Male.jpg";
            }
        }
        return $dp;
    }
    
    public function getad($isCompulsory=false, $loadedAds=array())
    {
        $session = Yii::$app->session; 
        $uid = (string)$session->get('user_id');
        $types = array("pagelikes", "brandawareness", "websiteleads", "websiteconversion", "pageendorse", "eventpromo");
        $adtypefinal = $types[array_rand($types)];
        $ads = TravAds::getrandAd($adtypefinal,$uid, $isCompulsory, $loadedAds);
        return $ads;
    }
    
    public function getadrate($isvip,$type)
    {
        if($isvip)
        {
            if($type == 'impression'){return 2.40;}
            if($type == 'action'){return 0.20;}
            if($type == 'click'){return 0.40;}
        }
        else
        {
            if($type == 'impression'){return 3.00;}
            if($type == 'action'){return 0.25;}
            if($type == 'click'){return 0.50;}
        }
    }
    
    public function getplaceimage($placetitle)
    {
        $GApiKeyL = $GApiKeyP = Googlekey::getkey();
        $coordinates = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.$GApiKeyL.'&address=' . urlencode($placetitle) . '&sensor=true');
        $coordinates = json_decode($coordinates);
        
        if($coordinates->status == 'OK')
        {
            $placeid = $coordinates->results[0]->place_id;
            $url="https://maps.googleapis.com/maps/api/place/details/json?placeid=$placeid&key=$GApiKeyL";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL,$url);
            $result=curl_exec($ch);
            curl_close($ch);
            $rs = json_decode($result, true);
            $qlimg = $rs['status'];
            if($qlimg == 'OK') {
                $rs = $rs['result'];
                if(empty($rs['photos'][0]['photo_reference'])) {
                    $back_image = $this->assetsPath.'placebanner.jpg';
                } else {
                    $ref = $rs['photos'][0]['photo_reference'];
                    $height = $rs['photos'][0]['height'];
                    $width = $rs['photos'][0]['width'];
                    if($height < 1000) {
                        if(isset($rs['photos'][1]['photo_reference']) && isset($rs['photos'][1]['height'])) {
                            $ref = $rs['photos'][1]['photo_reference'];
                            $height = $rs['photos'][1]['height'];
                            $width = $rs['photos'][1]['width'];
                        }
                    }
                    $back_image = "https://maps.googleapis.com/maps/api/place/photo?photoreference=$ref&sensor=false&maxheight=$height&maxwidth=$width&key=$GApiKeyL";
                }
            } else {
                $back_image = $this->assetsPath.'placebanner.jpg';
            }
        } else {
            $placeid = '';
            $back_image = $this->assetsPath.'placebanner.jpg';
        }
        return $back_image;
    }
    
    public function getlatlng($place)
    {
        $connected = @fsockopen("www.google.com", 80); //website, port  (try 80 or 443)

        if($connected)
        {
            $GApiKeyL = $GApiKeyP = Googlekey::getkey();
            $coordinates = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.$GApiKeyL.'&address='.urlencode($place).'&sensor=true');
            $coordinates = json_decode($coordinates);
            if($coordinates->status == 'OK')
            {
                $lat = substr($coordinates->results[0]->geometry->location->lat,0,7);
                $lng = substr($coordinates->results[0]->geometry->location->lng,0,7);
                return $lat.','.$lng;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
    public function getdistance($tripid,$from)
    {
        $tripdetails = Trip::getTripDetails($tripid);
        $ori = $tripdetails['start_from'];
        $dest = substr(str_replace('**','|',$tripdetails['end_to']),0,-1);
        $ori = str_replace(" ","%20",$ori);
        $dest = str_replace(" ","%20",$dest);
        
        $GApiKeyL = $GApiKeyP = Googlekey::getkey();
        $url="https://maps.googleapis.com/maps/api/distancematrix/json?key=$GApiKeyL&origins=$ori&destinations=$dest";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result = curl_exec($ch);
        curl_close($ch);
        $rs = json_decode($result, true);
        $ql = $rs['status'];
        if($ql == 'OK')
        {
            if(isset($rs['rows'][0]['elements'][0]['distance']['text']) && !empty($rs['rows'][0]['elements'][0]['distance']['text']))
            {
                return str_replace(' km','',$rs['rows'][0]['elements'][0]['distance']['text']);
            }
            else
            {
                return 'No';
            }
        }
        else
        {
            return 'No';
        }
    }
    
    public function getuserdata($userid,$type)
    {
        $resultimg = LoginForm::find()->where(['_id' => $userid])->one();
        $fullname = $resultimg[$type];
        return $fullname;
    }

    public function getcoverpicforpage($userid, $type)
    {
        $resultimg = Page::find()->where(['_id' => $userid])->one();
        $fullname = $resultimg[$type];
        return $fullname;
    }
    
    public function getpostdata($postid,$type)
    {
        $result = PostForm::find()->where(['_id' => $postid])->one();
        $name = $result[$type];
        return $name;
    }
     
    public function getpagename($pageid)
    {
        $result = Page::find()->where(['_id' => $pageid])->one();
        if(!empty($result)) {
            $pagename = isset($result['page_name']) ? $result['page_name'] : '';
            return $pagename;
        } else {
            return '';
        }
    }    

    public function getpageimage($pageid)
    {
        $resultimg = Page::find()->where(['_id' => $pageid])->one();
        $default_image = DefaultImage::getimage('page');
        $pagethumb = $resultimg['page_thumb'];

          
        
        if(isset($pagethumb) && !empty($pagethumb) && file_exists('profile/'.$pagethumb))
        {
            $dp = 'profile/'.$pagethumb;
        }
        else
        {
            if($default_image)
            {   
                $default_image = str_replace('../frontend','..',$default_image);
                $dp = $default_image;
            } 
            else 
            {
                $dp = $this->assetsPath.'demo-business.jpg'; 
            }
        }
        return $dp;
    }

    public function geteventimage($eventid)
    {
          
        
        $resultimg = PageEvents::find()->where(['_id' => $eventid])->one();
        $default_image = $this->assetsPath.'additem-commevents.png';
        $event_thumb = $resultimg['event_thumb'];
        if(isset($event_thumb) && !empty($event_thumb) && file_exists('uploads/events/'.$event_thumb))
        {
            $eventimage = 'uploads/events/'.$event_thumb;
        }
        else
        {
            if($default_image)
            {   
                $default_image = str_replace('../frontend','..',$default_image);
                $eventimage = $default_image;
            } else {
                $eventimage = $this->assetsPath.'additem-commevents.png';    
            }
        }

        return $eventimage;
    }

    public function getimagefilename($image)
    { 
        $imgs = explode('/',$image);
        $img = explode('/',$imgs[2]);
        $name = explode('.',$img[0]);
        return $name[0];
    }
    
    public function getnameonly($image)
    {
        $name = explode('.',$image);
        return $name[0];
    }
    
    public function getimagename($image)
    {
        $imgs = explode('/',$image);
        $img = explode('/',$imgs[2]);
        return $img[0];
    }
    
    public function getpostcount($id, $type)
    {       
        $postcount = PostForm::find()->where([(string)$type => (string)$id, 'is_deleted'=> '0'])->count();
         return $postcount;
    }

    protected function view_event($event_id)
    {
        $event_id = substr($event_id,6);
        $dp = $this->geteventimage($event_id);
        $event_details = PageEvents::getEventdetailsshare($event_id);
        $event_count = EventVisitors::getEventCounts($event_id);
        $evdate = $event_details['event_date'];
        $userstatus = EventVisitors::getEventGoing($event_id)
    ?>
        <div class="shared-box shared-category">
            <div class="post-holder">                                   
                <div class="post-content">
                    <div class="post-img-holder">
                        <div class="post-img one-img gallery">
                            <div class="pimg-holder">
                                <img class="circle width-100" src="<?=$dp?>"/>
                            </div>
                        </div>
                    </div>
                    <div class="share-summery">
                        <div class="datebox">
                            <span class="month"><?=date("M", strtotime($evdate))?></span>
                            <span class="date"><?=date("d", strtotime($evdate))?></span>
                        </div>
                        <div class="sharedpost-title">
                            <a href="<?php echo Url::to(['event/detail', 'e' => "$event_id"]); ?>" style="color: black;"><?=$event_details['event_name']?></a>
                        </div>
                        <div class="sharedpost-subtitle"><?=$event_details['address']?></div>
                        <div class="sharedpost-desc"><?=$event_count?> Attending</div>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm actionbtn" onclick="joinEvent(event,'<?=$event_id;?>',this, false)"><?=$userstatus?></a>
                    </div>
                </div>                      
            </div>
        </div>
    <?php }

    protected function view_page($page_id)
    {
          
        
        $page_id = substr($page_id,5);
        $dp = $this->getpageimage($page_id);
        $page_details = Page::Pagedetails($page_id);
        $pagelike = Like::getLikeCount($page_id);
        $pagecover = $this->getuserdata($page_id,'cover_photo');
        if(isset($pagecover) && !empty($pagecover))
        {
            $cover_photo = "uploads/cover/".$pagecover;
        }
        else
        {
            $cover_photo = $this->assetsPath."wallbanner.jpg";
        }
    ?>
        <div class="shared-box shared-category">
            <div class="post-holder">                                   
                <div class="post-content">
                    <div class="post-img-holder">
                        <div class="post-img one-img gallery">
                            <div class="pimg-holder">
                                <div class="bannerimg circle width-100" style="background:url('<?=$cover_photo?>') center top no-repeat;background-size:cover;"></div>
                                <div class="profileimg"><img src="<?=$dp?>"/></div>                                                 
                            </div>
                        </div>
                    </div>
                    <div class="share-summery">
                        <div class="sharedpost-title">
                            <a href="<?php echo Url::to(['page/index', 'id' => "$page_id"]); ?>" style="color: black;"><?=$page_details['page_name']?></a>
                        </div>
                        <div class="sharedpost-tagline"><?=$page_details['short_desc']?></div>
                        <div class="sharedpost-desc"><?=$pagelike?> people liked this</div>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm actionbtn">Like</a>
                    </div>
                </div>                      
            </div>
        </div>
    <?php }
    
    protected function view_trip($trip_id)
    {
        $trip_id = substr($trip_id,5);
        $trip = Trip::getTripDetails($trip_id);
        $trip_name = $trip['trip_name'];
        $trip_summary = $trip['trip_summary'];
        $start_from = $trip['start_from'];
        $trip_stops = explode('**',$trip['end_to'],-1);
        $session = Yii::$app->session;
        $user_id = $trip['user_id'];
        $link = Url::to(['userwall/index', 'id' => $user_id]);
        $dp = $this->getimage($user_id,'thumb');
        $fullname = $this->getuserdata($user_id,'fullname');
        $time = Yii::$app->EphocTime->time_elapsed_A(time(),$trip['created_date']);
    ?>
        <div class="shared-box">
            <div class="post-holder">
                <div class="post-topbar">
                    <div class="post-userinfo">
                        <h4><?=$trip_name?></h4>
                        <div class="img-holder">
                            <div id="profiletip-1" class="profiletipholder">
                                <span class="profile-tooltip">
                                    <img class="circle width-100" src="<?=$dp?>"/>
                                </span>
                            </div>
                        </div>
                        <div class="desc-holder">
                            <span>By </span><a href="<?=$link?>"><?=$fullname?></a> from <?=$start_from?>
                            <span class="timestamp"><?=$time?><span class="glyphicon glyphicon-globe"></span></span>
                        </div>
                    </div>                                      
                </div>
                <div class="post-content tripexperince-post">
                    <div class="pdetail-holder">
                        <div class="post-details">
                            <div class="post-title">
                                <?=$trip_name?>
                            </div>
                        </div>
                        <div class="trip-summery">
                            <div class="route-holder">
                                <label>Stops :</label>
                                <ul class="triproute">
                                    <?php foreach ($trip_stops as $name) { ?>
                                    <li><?=$name?></li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="location-info">
                                <h5><i class="zmdi zmdi-pin"></i> Trip Route</h5>
                                <i class="mdi mdi-menu-right"></i>
                                <a href="javascript:void(0)" onclick="openViewMap(this,'<?=$trip_id?>')">View on map</a>
                            </div>                                          
                        </div>
                        <div class="map-holder" id="trip-map-share-<?=$trip_id?>"></div>
                        <?php if(isset($trip_summary) && !empty($trip_summary)){ ?>
                        <div class="post-details">                                                          
                            <div class="post-desc">
                                <?php if(strlen($trip_summary)>187){ ?>
                                    <div class="para-section">
                                        <div class="para">
                                            <p><?=$trip_summary?></p>
                                        </div>
                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                    </div>
                                <?php }else{ ?>
                                    <p><?=$trip_summary?></p>
                                <?php } ?>                          
                            </div>
                        </div>
                        <?php } ?>
                        <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $trip_id;?>" href="javascript:void(0)">&nbsp;</a>
                    </div>
                </div>
            </div>
        </div>
    <?php }

	protected function view_post_id_md($postid)
{
    $session = Yii::$app->session;
    $userid = $user_id = (string)$session->get('user_id');
    $post = PostForm::find()->where(['_id' => $postid])->one();
    $originalpost = PostForm::find()->where(['parent_post_id' => $postid])->one();
    $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
    $post_privacy = $post['post_privacy'];
    if($post_privacy == 'Private') {$post_class = 'lock';}
    else if($post_privacy == 'Connections') {$post_class = 'account';}
    else if($post_privacy = 'Public') {$post_class = 'earth';}
    $lmodel = new \frontend\models\LoginForm();
    $getsharename = LoginForm::find()->where(['_id' => $post['shared_from']])->one();
    $share_type = $post['post_type'];
    if($share_type == 'text' || $share_type == 'link' || $share_type == 'profilepic')
    {
        $sharetype = 'Post';
    }
    else
    {
        $sharetype = 'Photo';
    }
    $getshareid = $getsharename['_id'];
    //$link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
    $getuserinfo = LoginForm::find()->where(['_id' => $post['post_user_id']])->one();
    if($post['user']['status']=='44')
    {
        $dp = $this->getpageimage($post['user']['_id']);
        $link = Url::to(['page/index', 'id' => $post['post_user_id']]);
        $page = 'page';
    }
    else
    {
        $dp = $this->getimage($getuserinfo['_id'],'photo');
        $link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
        $page = 'userwall';
    }
    $personalinfo = Personalinfo::find()->where(['user_id' => $post['post_user_id']])->one();
    //$dpimg = $this->getimage($post['user']['_id'],'thumb');
    if($post['user']['status']=='44')
    {
        $dpimg = $this->getpageimage($post['user']['_id']);
    }
    else
    {
        $dpimg = $this->getimage($post['user']['_id'],'photo');
    }
    // START add tag content in post
    $posttag = '';
    if(isset($post['post_tags']) && !empty($post['post_tags']))
    {
        $posttag = explode(",", $post['post_tags']);
    }

    $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');

    $nkTag = array();
    $nvTag = array();

    $i=1;
    foreach ($taginfomatiom as $key => $value)
    {
        $nkTag[] = (string)$value; 
        $nvTag[] = $key;
        if($i != 1) {
            $content[] = $key;
        }
        $i++;
    }

    if(isset($content) && !empty($content))
    {
        $content = implode("<br/>", $content); 
    }

    $tagstr = '';
    if(!empty($taginfomatiom))
    {
        if(count($taginfomatiom) > 1) {
            $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
        } else {
            $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
        }
    }       
    if(isset($post['currentlocation']) && !empty($post['currentlocation']))
    { $locationtags = $tagstr . ' at '.$post['currentlocation'].'.'; }
    else{ $locationtags = $tagstr . ''; }
    if(isset($post['is_page_review']) && !empty($post['is_page_review']))
    {
        $page_details = Page::Pagedetails($post['page_id']);
        $review = " reviewed <a href=".Url::to(['page/index', 'id' => $post['page_id']]) ." class='sub-link'>" . $page_details['page_name'] . "</a>"; }
    else { $review = ''; }
    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='tip'){$locationtags = ' tip for '.$post['currentlocation'];}
    ?>
    <div class="shared-box">
        <div class="post-holder <?php if((isset($post['trav_item']) && !empty($post['trav_item']))){ ?>travelstore-ad<?php } ?>">
            <div class="post-topbar">
                <div class="post-userinfo">
                <?php if((isset($post['is_trip']) && !empty($post['is_trip']))){ ?>
                    <?php if($post['post_title'] != null) { ?>
                        <h4><?= $post['post_title'] ?></h4>
                    <?php } ?>
                <?php } ?>  
                    <div class="img-holder">
                        <div class="profiletipholder">
                            <span class="profile-tooltip">
                                <img class="circle width-100" src="<?= $dpimg?>"/>
                            </span>
                        </div>
                    </div>
                    <div class="desc-holder">
                        <?php if((isset($post['is_trip']) && !empty($post['is_trip']))){ ?>
                            <span>By </span><a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            <span class="timestamp"><?php echo $time; ?><span class="mdi mdi-<?= $post_class?> mdi-13px"></span></span>
                        <?php } else { ?>
                            <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?><?php if(empty($post['trav_item'])){ ?><?= $locationtags?><?php }?>
                            <?php /*if($post['is_album']=='1'){ ?>
                            added album <a href="javascript:void(0)"><?= $post['album_title']?>.</a>
                            <?php }  else */ if($post['post_type']=='profilepic'){ ?>
                            updated profile photo.
                            <?php } else if($post['is_coverpic']=='1'){ ?>
                            updated cover photo.
                            <?php } ?>
                            <span class="timestamp"><?php echo $time; ?><span class="mdi mdi-<?= $post_class?> mdi-13px"></span></span>
                        <?php } ?>
                    </div>
                </div>                          
            </div>
            <?php if(!isset($post['is_page_review']) && empty($post['is_page_review']))
            { ?>
                <?php if((isset($post['is_trip']) && !empty($post['is_trip']))){ ?>
                    <div class="post-content tripexperince-post">
                        <div class="pdetail-holder">
                            <div class="post-details">
                                <div class="post-title">
                                   <?= $post['post_title'] ?>
                                </div>
                            </div>
                            <div class="trip-summery">
                                <div class="location-info">
                                    <h5><i class="zmdi zmdi-pin"></i> <?= $post['currentlocation'];?></h5>
                                    <i class="mdi mdi-menu-right"></i>
                                    <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                </div>                                          
                            </div>
                            <div class="map-holder dis-none">
                                <iframe width="600" height="450" frameborder="0" src="https://maps.google.it/maps?q=<?= $post['currentlocation'];?>&output=embed"></iframe>
                            </div>
                            <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                        </div>
                        
                        <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                            $post['post_type'] = 'text and image';
                        }?>
                        <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                            $cnt = 1;
                            $eximgs = explode(',',$post['image'],-1);
                            if(isset($post['trav_item']) && $post['trav_item']== '1')
                            {
                                if($post['image'] == null)
                                {
                                    $eximgs[0] = '/uploads/travitem-default.png';
                                }
                                $eximgss[] = $eximgs[0];
                                $eximgs = $eximgss;                                     
                            }
                            $totalimgs = count($eximgs);
                            $imgcountcls="";
                            if($totalimgs == '1'){$imgcountcls = 'one-img';}
                            if($totalimgs == '2'){$imgcountcls = 'two-img';}
                            if($totalimgs == '3'){$imgcountcls = 'three-img';}
                            if($totalimgs == '4'){$imgcountcls = 'four-img';}
                            if($totalimgs == '5'){$imgcountcls = 'five-img';}
                            if($totalimgs > '5'){$imgcountcls = 'more-img';}
                        ?>
                        <div class="post-img-holder">
                            <div class="post-img <?= $imgcountcls?> gallery swipe-gallery">
                                <?php
                                foreach ($eximgs as $eximg) {
                                
                                if (file_exists('../web'.$eximg)) {
                                $picsize = '';
                                $val = getimagesize('../web'.$eximg);
                                $picsize .= $val[0] .'x'. $val[1] .', ';
                                $inameclass = $this->getimagename($eximg);
                                $iname = $this->getimagefilename($eximg);
                                $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                <?php /* <div class="pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?>"> */ ?>    
                                <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                <?php if($cnt == 5 && $totalimgs > 5){?>
                                    <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                <?php } ?>
                                </a>
                                <?php /* </div>*/ ?>
                                <?php } $cnt++; } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="pdetail-holder">
                            <div class="post-details">
                                <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                <div class="post-desc">                                 
                                    <?php if(strlen($post['post_text'])>187){ ?>
                                        <div class="para-section">
                                            <div class="para">
                                                <p><?= $post['post_text'] ?></p>
                                            </div>
                                           <a href="javascript:void(0)" class="readlink">Read More</a>
                                        </div>
                                    <?php }else{ ?>                                     
                                        <p><?= $post['post_text'] ?></p>
                                    <?php } ?>
                                </div>
                                <?php } ?>                          
                            </div>                                                      
                            <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                        </div>
                    </div>
                    
                
                <?php } else { ?>
                    <div class="post-content">
                        <div class="pdetail-holder">
                            <div class="post-details">
                                <?php if($post['post_title'] != null) { ?>
                                <div class="post-title"><?= $post['post_title'] ?></div>
                                <?php } ?>
                                <?php if($post['trav_price'] != null) { ?>
                                <div class="post-price">$<?= $post['trav_price'] ?></div>
                                <?php } ?>
                                <?php if((isset($post['trav_item']) && !empty($post['trav_item']) && $post['currentlocation'] != null)){ ?>
                                    <div class="post-location" style="display:block"><i class="zmdi zmdi-pin"></i><?= $post['currentlocation'] ?></div>
                                <?php } ?>
                                <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                <div class="post-desc">                                 
                                    <?php if(strlen($post['post_text'])>187){ ?>
                                        <div class="para-section">
                                            <div class="para">
                                                <p><?= $post['post_text'] ?></p>
                                            </div>
                                            <a href="javascript:void(0)" class="readlink">Read More</a>
                                        </div>
                                    <?php }else{ ?>                                     
                                        <p><?= $post['post_text'] ?></p>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                            <?php if($post['trav_item']== '1' && $post['is_ad']== '1'){
                                $actbtn = $post['adbtn'];
                                $link = $post['adurl'];
                                if(!empty($link))
                                {
                                    if(substr( $link, 0, 4 ) === "http")
                                    {
                                        $link = $link;
                                    }
                                    else
                                    {
                                        $link = 'https://'.$link;
                                    }
                                }
                                else
                                {
                                    $link = 'Not added';
                                }
                            ?>
                            <div class="post-paidad">
                                <div class="travad-sponsor">Sponsered by <a href="<?=$link?>" target="blank"><?=$link?></a></div>
                                <a class="travad-shop btn btn-primary btn-md">
                                    <?=$actbtn?>
                                </a>
                               </div>
                            <?php } ?>
                            <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                        </div>
                        <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                            $post['post_type'] = 'text and image';
                        }?>  
                        <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                            $cnt = 1;
                            $eximgs = explode(',',$post['image'],-1);
                            if(isset($post['trav_item']) && $post['trav_item']== '1')
                            {
                                if($post['image'] == null)
                                {
                                    $eximgs[0] = '/uploads/travitem-default.png';
                                }
                                $eximgss[] = $eximgs[0];
                                $eximgs = $eximgss;                                     
                            }
                            $totalimgs = count($eximgs);
                            $imgcountcls="";
                            if($totalimgs == '1'){$imgcountcls = 'one-img';}
                            if($totalimgs == '2'){$imgcountcls = 'two-img';}
                            if($totalimgs == '3'){$imgcountcls = 'three-img';}
                            if($totalimgs == '4'){$imgcountcls = 'four-img';}
                            if($totalimgs == '5'){$imgcountcls = 'five-img';}
                            if($totalimgs > '5'){$imgcountcls = 'more-img';}
                        ?>
                        <div class="post-img-holder">
                            <div class="post-img <?= $imgcountcls?> gallery swipe-gallery">
                                <?php
                                foreach ($eximgs as $eximg) {
                                
                                if (file_exists('../web'.$eximg)) {
                                $picsize = '';
                                $val = getimagesize('../web'.$eximg);
                                $picsize .= $val[0] .'x'. $val[1] .', ';
                                $inameclass = $this->getimagename($eximg);
                                $iname = $this->getimagefilename($eximg);
                                $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                <?php if($cnt == 5 && $totalimgs > 5){?>
                                    <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                <?php } ?>
                                </a>
                                <?php } $cnt++; } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if($post['post_type'] == 'image' && $post['is_coverpic'] == '1' && file_exists('uploads/cover/'.$post['image'])) { ?>
                            <div class="post-img-holder">
                                <div class="post-img one-img gallery swipe-gallery">
                                <?php
                                $picsize = '';
                                $val = getimagesize('uploads/cover/'.$post['image']);
                                $iname = $post['image'];
                                $inameclass = $this->getnameonly($post['image']);
                                
                                $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                
                                $picsize .= $val[0] .'x'. $val[1] .', ';
                                if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                       <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                        </a>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if($post['post_type'] == 'profilepic' && file_exists('profile/'.$post['image'])) { ?>
                            <div class="post-img-holder">
                                <div class="post-img gallery swipe-gallery">
                                <?php
                                    $picsize = '';
                                    $val = getimagesize('profile/'.$post['image']);
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);

                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}

                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" class="<?= $imgclass?>"/>                       
                                            </a>         
                                </div>
                            </div>
                        <?php } ?>
                        
                        <div class="pdetail-holder">
                            <?php if($post['post_type'] == 'link'){ ?>
                                    <div class="pvideo-holder">
                                            <?php if($post['image'] != 'No Image'){ ?>
                                                    <div class="img-holder"><img src="<?= $post['image'] ?>"/></div>
                                            <div class="desc-holder">
                                            <?php } ?>
                                                    <h4><a href="<?= $post['post_text']?>" target="_blank"><?= $post['link_title'] ?></a></h4>
                                                    <p><?= $post['link_description'] ?></p>
                                            <?php if($post['image'] != 'No Image'){ ?>
                                            </div>
                                            <?php } ?>
                                    </div>
                            <?php } ?>
                            <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                        </div>
                    </div>
                <?php }?>   
            <?php } else { ?>
            <div class="post-content <?php if($post['image'] != null) { ?>hasimg<?php } ?>">
                <div class="pdetail-holder">
                    <div class="post-details">
                        <?php if($post['post_title'] != null) { ?>
                        <div class="post-title"><?=$post['post_title']?></div>
                        <?php } ?>
                        <div class="rating-stars">
                        <?php for($i=0;$i<5;$i++)
                        { ?>
                            <i class="mdi mdi-star <?php if($i < $post['rating']){ ?>active<?php } ?>"></i>
                        <?php }
                        ?>
                        </div>
                        <div class="post-desc">
                            <?php if($post['post_text'] == null) { $post['post_text'] = 'Not added'; } ?>
                            <?php if(strlen($post['post_text'])>187){ ?>
                                <div class="para-section">
                                    <div class="para">
                                        <p><?= $post['post_text'] ?></p>
                                    </div>
                                    <a href="javascript:void(0)" class="readlink">Read More</a>
                                </div>
                            <?php }else{ ?>                                     
                                <p><?= $post['post_text'] ?></p>
                            <?php } ?>                                   
                        </div>
                    </div>
                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                </div>
                <?php if($post['image'] != null) {
                    $eximgs = explode(',',$post['image'],-1);
                    foreach ($eximgs as $eximg) {
                    if (file_exists('../web'.$eximg)) {
                    $picsize = '';
                    $val = getimagesize('../web'.$eximg);
                    $picsize .= $val[0] .'x'. $val[1] .', ';
                    $iname = $this->getimagename($eximg);
                    $inameclass = $this->getimagefilename($eximg);
                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}
                ?>
                <div class="post-img-holder">
                    <div class="post-img one-img gallery swipe-gallery">
                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                        </a>
                    </div>
                </div>
                <?php } } } ?>
            </div>
        <?php } ?>
        </div>
    </div>
    <?php 
}
	
    protected function view_post_id($postid)
    {
        //echo $postid;
        $session = Yii::$app->session;
        $userid = $user_id = (string)$session->get('user_id');
        $post = PostForm::find()->where(['_id' => $postid])->one();
        $originalpost = PostForm::find()->where(['parent_post_id' => $postid])->one();
        $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
        $post_privacy = $post['post_privacy'];
        if($post_privacy == 'Private') {$post_class = 'lock';}
        else if($post_privacy == 'Connections') {$post_class = 'account';}
        else if($post_privacy = 'Public') {$post_class = 'earth';}
        $lmodel = new \frontend\models\LoginForm();
        $getsharename = LoginForm::find()->where(['_id' => $post['shared_from']])->one();
        $share_type = $post['post_type'];
        if($share_type == 'text' || $share_type == 'link' || $share_type == 'profilepic')
        {
            $sharetype = 'Post';
        }
        else
        {
            $sharetype = 'Photo';
        }
        $getshareid = $getsharename['_id'];
        //$link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
        $getuserinfo = LoginForm::find()->where(['_id' => $post['post_user_id']])->one();
        if($post['user']['status']=='44')
        {
            $dp = $this->getpageimage($post['user']['_id']);
            $link = Url::to(['page/index', 'id' => $post['post_user_id']]);
            $page = 'page';
        }
        else
        {
            $dp = $this->getimage($getuserinfo['_id'],'photo');
            $link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
            $page = 'userwall';
        }
        $personalinfo = Personalinfo::find()->where(['user_id' => $post['post_user_id']])->one();
        if($post['user']['status']=='44')
        {
            $dpimg = $this->getpageimage($post['user']['_id']);
        }
        else
        {
            $dpimg = $this->getimage($post['user']['_id'],'photo');
        }
        // START add tag content in post
        $posttag = '';
        if(isset($post['post_tags']) && !empty($post['post_tags']))
        {
            $posttag = explode(",", $post['post_tags']);
        }

        $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');

        $nkTag = array();
        $nvTag = array();

        $i=1;
        foreach ($taginfomatiom as $key => $value)
        {
            $nkTag[] = (string)$value; 
            $nvTag[] = $key;
            if($i != 1) {
                $content[] = $key;
            }
            $i++;
        }

        if(isset($content) && !empty($content))
        {
            $content = implode("<br/>", $content); 
        }

        $tagstr = '';
        if(!empty($taginfomatiom))
        {
            if(count($taginfomatiom) > 1) {
                $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
            } else {
                $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
            }
        }       
        if(isset($post['currentlocation']) && !empty($post['currentlocation']))
        { $locationtags = $tagstr . ' at '.$post['currentlocation'].'.'; }
        else{ $locationtags = $tagstr . ''; }
        if(isset($post['is_page_review']) && !empty($post['is_page_review']))
        {
            $page_details = Page::Pagedetails($post['page_id']);
            $review = " reviewed <a href=".Url::to(['page/index', 'id' => $post['page_id']]) ." class='sub-link'>" . $page_details['page_name'] . "</a>"; }
        else { $review = ''; }
        if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='tip'){$locationtags = ' tip for '.$post['currentlocation'];}
        ?>
        <div class="shared-box">
            <div class="post-holder <?php if((isset($post['trav_item']) && !empty($post['trav_item']))){ ?>travelstore-ad<?php } ?>">
                <div class="post-topbar">
                    <div class="post-userinfo">
                    <?php if((isset($post['is_trip']) && !empty($post['is_trip']))){ ?>
                        <?php if($post['post_title'] != null) { ?>
                            <h4><?= $post['post_title'] ?></h4>
                        <?php } ?>
                    <?php } ?>  
                        <div class="img-holder">
                            <div class="profiletipholder">
                                <span class="profile-tooltip">
                                    <img class="circle" src="<?= $dpimg?>"/>
                                </span>
                            </div>
                        </div>
                        <div class="desc-holder">
                            <?php if((isset($post['is_trip']) && !empty($post['is_trip']))){ ?>
                                <span>By </span><a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                                <span class="timestamp"><?php echo $time; ?><span class="mdi mdi-<?= $post_class?> mdi-13px"></span></span>
                            <?php } else { ?>
                                <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?><?php if(empty($post['trav_item'])){ ?><?= $locationtags?><?php }?>
                                <?php 
                                if($post['post_type']=='profilepic'){ ?>
                                updated profile photo.
                                <?php } else if($post['is_coverpic']=='1'){ ?>
                                updated cover photo.
                                <?php } ?>
                                <span class="timestamp"><?php echo $time; ?><span class="mdi mdi-<?= $post_class?> mdi-13px"></span></span>
                            <?php } ?>
                        </div>
                    </div>                          
                </div>
                <?php if(!isset($post['is_page_review']) && empty($post['is_page_review']))
                { ?>
                    <?php if((isset($post['is_trip']) && !empty($post['is_trip']))){ ?>
                        <div class="post-content tripexperince-post">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                    <div class="post-title">
                                       <?= $post['post_title'] ?>
                                    </div>
                                </div>
                                <div class="trip-summery">
                                    <div class="location-info">
                                        <h5><i class="zmdi zmdi-pin"></i> <?= $post['currentlocation'];?></h5>
                                        <i class="mdi mdi-menu-right"></i>
                                        <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                    </div>                                          
                                </div>
                                <div class="map-holder dis-none">
                                    <iframe width="600" height="450" frameborder="0" src="https://maps.google.it/maps?q=<?= $post['currentlocation'];?>&output=embed"></iframe>
                                </div>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                            
                            <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                                $post['post_type'] = 'text and image';
                            }?>
                            <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                $cnt = 1;
                                $eximgs = explode(',',$post['image'],-1);
                                if(isset($post['trav_item']) && $post['trav_item']== '1')
                                {
                                    if($post['image'] == null)
                                    {
                                        $eximgs[0] = '/uploads/travitem-default.png';
                                    }
                                    $eximgss[] = $eximgs[0];
                                    $eximgs = $eximgss;                                     
                                }
                                $totalimgs = count($eximgs);
                                $imgcountcls="";
                                if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                if($totalimgs > '5'){$imgcountcls = 'more-img';}
                            ?>
                            <div class="post-img-holder">
                                <div class="post-img <?= $imgcountcls?> gallery swipe-gallery">
                                    <?php
                                    foreach ($eximgs as $eximg) {
                                    
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $inameclass = $this->getimagename($eximg);
                                    $iname = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                    <?php if($cnt == 5 && $totalimgs > 5){?>
                                        <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                    <?php } ?>
                                    </a>
                                    <?php } $cnt++; } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="pdetail-holder">
                                <div class="post-details">
                                    <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                    <div class="post-desc">                                 
                                        <?php if(strlen($post['post_text'])>187){ ?>
                                            <div class="para-section">
                                                <div class="para">
                                                    <p><?= $post['post_text'] ?></p>
                                                </div>
                                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                            </div>
                                        <?php }else{ ?>                                     
                                            <p><?= $post['post_text'] ?></p>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>                          
                                </div>                                                      
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                        </div>
                        
                    
                    <?php } else { ?>
                        <div class="post-content">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                    <?php if($post['post_title'] != null) { ?>
                                    <div class="post-title"><?= $post['post_title'] ?></div>
                                    <?php } ?>
                                    <?php if($post['trav_price'] != null) { ?>
                                    <div class="post-price">$<?= $post['trav_price'] ?></div>
                                    <?php } ?>
                                    <?php if((isset($post['trav_item']) && !empty($post['trav_item']) && $post['currentlocation'] != null)){ ?>
                                        <div class="post-location" style="display:block"><i class="zmdi zmdi-pin"></i><?= $post['currentlocation'] ?></div>
                                    <?php } ?>
                                    <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                    <div class="post-desc">                                 
                                        <?php if(strlen($post['post_text'])>187){ ?>
                                            <div class="para-section">
                                                <div class="para">
                                                    <p><?= $post['post_text'] ?></p>
                                                </div>
                                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                            </div>
                                        <?php }else{ ?>                                     
                                            <p><?= $post['post_text'] ?></p>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if($post['trav_item']== '1' && $post['is_ad']== '1'){
                                    $actbtn = $post['adbtn'];
                                    $link = $post['adurl'];
                                    if(!empty($link))
                                    {
                                        if(substr( $link, 0, 4 ) === "http")
                                        {
                                            $link = $link;
                                        }
                                        else
                                        {
                                            $link = 'https://'.$link;
                                        }
                                    }
                                    else
                                    {
                                        $link = 'Not added';
                                    }
                                ?>
                                <div class="post-paidad">
                                    <div class="travad-sponsor">Sponsered by <a href="<?=$link?>" target="blank"><?=$link?></a></div>
                                    <a class="travad-shop btn btn-primary btn-md">
                                        <?=$actbtn?>
                                    </a>
                                   </div>
                                <?php } ?>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                            <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                                $post['post_type'] = 'text and image';
                            }?>
                            <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                $cnt = 1;
                                $eximgs = explode(',',$post['image'],-1);
                                if(isset($post['trav_item']) && $post['trav_item']== '1')
                                {
                                    if($post['image'] == null)
                                    {
                                        $eximgs[0] = '/uploads/travitem-default.png';
                                    }
                                    $eximgss[] = $eximgs[0];
                                    $eximgs = $eximgss;                                     
                                }
                                $totalimgs = count($eximgs);
                                $imgcountcls="";
                                if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                if($totalimgs > '5'){$imgcountcls = 'more-img';}
                            ?>
                            <div class="post-img-holder">
                                <div class="post-img <?= $imgcountcls?> gallery swipe-gallery">
                                    <?php
                                    foreach ($eximgs as $eximg) {
                                    
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $inameclass = $this->getimagename($eximg);
                                    $iname = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                      <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                    <?php if($cnt == 5 && $totalimgs > 5){?>
                                        <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                    <?php } ?>
                                    </a>
                                    <?php } $cnt++; } ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'image' && $post['is_coverpic'] == '1' && file_exists('uploads/cover/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="post-img one-img gallery swipe-gallery">
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('uploads/cover/'.$post['image']);
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);
                                    
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                        <?php /*<div class="pimg-holder <?= $imgclass?>-box">*/ ?>
                                           <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                            </a>
                                            
                                        <?php /* </div> */?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'profilepic' && file_exists('profile/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="post-img gallery swipe-gallery">
                                    <?php
                                        $picsize = '';
                                        $val = getimagesize('profile/'.$post['image']);
                                        $iname = $post['image'];
                                        $inameclass = $this->getnameonly($post['image']);

                                        $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                        if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}

                                        $picsize .= $val[0] .'x'. $val[1] .', ';
                                        if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" class="<?= $imgclass?>"/>                       
                                                </a>           
                                    </div>
                                </div>
                            <?php } ?>
                            
                            <div class="pdetail-holder">
                                <?php if($post['post_type'] == 'link'){ ?>
                                        <div class="pvideo-holder">
                                                <?php if($post['image'] != 'No Image'){ ?>
                                                        <div class="img-holder"><img src="<?= $post['image'] ?>"/></div>
                                                <div class="desc-holder">
                                                <?php } ?>
                                                        <h4><a href="<?= $post['post_text']?>" target="_blank"><?= $post['link_title'] ?></a></h4>
                                                        <p><?= $post['link_description'] ?></p>
                                                <?php if($post['image'] != 'No Image'){ ?>
                                                </div>
                                                <?php } ?>
                                        </div>
                                <?php } ?>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                        </div>
                    <?php }?>   
                <?php } else { ?>
                <div class="post-content <?php if($post['image'] != null) { ?>hasimg<?php } ?>">
                    <div class="pdetail-holder">
                        <div class="post-details">
                            <?php if($post['post_title'] != null) { ?>
                            <div class="post-title"><?=$post['post_title']?></div>
                            <?php } ?>
                            <div class="rating-stars">
                            <?php for($i=0;$i<5;$i++)
                            { ?>
                                <i class="mdi mdi-star <?php if($i < $post['rating']){ ?>active<?php } ?>"></i>
                            <?php }
                            ?>
                            </div>
                            <div class="post-desc">
                                <?php if($post['post_text'] == null) { $post['post_text'] = 'Not added'; } ?>
                                <?php if(strlen($post['post_text'])>187){ ?>
                                    <div class="para-section">
                                        <div class="para">
                                            <p><?= $post['post_text'] ?></p>
                                        </div>
                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                    </div>
                                <?php }else{ ?>                                     
                                    <p><?= $post['post_text'] ?></p>
                                <?php } ?>                                   
                            </div>
                        </div>
                        <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                    </div>
                    <?php if($post['image'] != null) {
                        $eximgs = explode(',',$post['image'],-1);
                        foreach ($eximgs as $eximg) {
                        if (file_exists('../web'.$eximg)) {
                        $picsize = '';
                        $val = getimagesize('../web'.$eximg);
                        $picsize .= $val[0] .'x'. $val[1] .', ';
                        $iname = $this->getimagename($eximg);
                        $inameclass = $this->getimagefilename($eximg);
                        $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                        if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                        if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}
                    ?>
                    <div class="post-img-holder">
                        <div class="post-img one-img gallery swipe-gallery">
                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                            </a>
                        </div>
                    </div>
                    <?php } } } ?>
                </div>
            <?php } ?>
            </div>
        </div>
        <?php 
    }
    
    public function display_review_post($postid)
    {
        $post = PostForm::find()->where(['_id' => $postid])->one();
        $my_post_view_status = $post['post_privacy'];
        if($my_post_view_status == 'Private') {$post_dropdown_class = 'lock';}
        else if($my_post_view_status == 'Connections') {$post_dropdown_class = 'account';}
        else if($my_post_view_status = 'Public') {$post_dropdown_class = 'earth';}
        $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
        $posttitle = $post['post_title'];
        $posttext = $post['post_text'];
        $user_id = $userid = $post['shared_by'];
        $userdp = $this->getimage($userid,'thumb');
        $link = Url::to(['userwall/index', 'id' => $userid]);
        $fullname = $this->getuserdata($userid,'fullname');
        $posttag = '';
        if(isset($post['post_tags']) && !empty($post['post_tags']))
        {
            $posttag = explode(",", $post['post_tags']);
        }
        $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');
        $nkTag = array();
        $nvTag = array();
        $i=1;
        foreach ($taginfomatiom as $key => $value)
        {
            $nkTag[] = (string)$value; 
            $nvTag[] = $key;
            if($i != 1)
            {
                $content[] = $key;
            }
            $i++;
        }
        if(isset($content) && !empty($content))
        {
            $content = implode("<br/>", $content); 
        }
        $tagstr = '';
        if(!empty($taginfomatiom))
        {
            if(count($taginfomatiom) > 1) {
                $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
            } else {
                $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
            }
        }
        if(!empty($post['currentlocation'])){$tagstr = $tagstr . ' at '.$post['currentlocation'].'.';}
        else{$tagstr = $tagstr . '';}

          
        
    ?>
        <li class="mainli">
            <div class="review-holder">
                <div class="post-holder">
                    <div class="post-topbar">
                        <div class="post-userinfo">
                            <div class="img-holder">
                                <div class="profiletipholder">
                                    <span class="profile-tooltip">
                                        <img class="circle" src="<?=$userdp?>"/>
                                    </span>
                                    <span class="profiletooltip_content slidingpan-holder">
                                    <div class="profile-tip" style="display:none;">
                                                
                                        <div class="profile-tip-cover"><img src="<?=$this->assetsPath?>cover.jpg"></div>
                                        <div class="profile-tip-avatar">
                                            <a href="javascript:void(0)">
                                                <img alt="user-photo" class="img-responsive" src="<?=$this->assetsPath?>demo-profile.jpg">
                                            </a>
                                        </div>
                                        <div class="profile-tip-info">
                                            <div class="cover-username"><a href="javascript:void(0)">Adel Hasanat</a></div>
                                            <div class="cover-headline">
                                                <span class="ptip-icon"><i class="fa  fa-suitcase"></i></span>
                                                Web Designer, Cricketer
                                            </div>
                                            <div class="profiletip-bio">
                                                <span class="ptip-icon"><i class="mdi mdi-home"></i></span>
                                                Lives in : <span>Gariyadhar</span>
                                            </div>
                                            <div class="profiletip-bio">
                                                <span class="ptip-icon"><i class="zmdi zmdi-pin"></i></span>
                                                Currently in : <span>Gariyadhar, Gujarat, India</span>
                                            </div>

                                        </div>
                                        <div class="profile-tip-divider"></div>
                                        <div class="profile-tip-btn">
                                            <a href="javascript:void(0)" class="btn btn-primary btn-sm"><i class="mdi mdi-eye"></i>View Profile</a>
                                        </div>
                                    </div>
                                </span>
                                </div>
                            </div>
                            <div class="desc-holder">
                                <a href="<?=$link?>"><?=$fullname?></a><?=$tagstr?>
                                <span class="timestamp"><?=$time?><span class="mdi mdi-<?=$post_dropdown_class?> mdi-13px"></span></span>
                            </div>
                            <div class="settings-icon">
                                <div class="dropdown dropdown-custom dropdown-xxsmall">
                                    <a class="dropdown-button more_btn" href="javascript:void(0);" data-activates="review_post_new">
                                        <i class="zmdi zmdi-hc-2x zmdi-more"></i>
                                    </a>
                                    <ul id="review_post_new" class="dropdown-content custom_dropdown" >
                                        <li class="nicon"><a href="javascript:void(0)">Hide post</a></li>
                                        <li class="nicon"><a href="javascript:void(0)" class="savepost-link">Save post</a></li>
                                        <li class="nicon"><a href="javascript:void(0)">Mute notification for this post</a></li>
                                        <li class="nicon"><a href="javascript:void(0)">Mute connect post</a></li>
                                        <li class="nicon"><a href="javascript:void(0)">Report post</a></li>
                                    </ul>       
                                </div>
                            </div>
                    </div>
                    <div class="post-content">
                        <div class="pdetail-holder">
                            <?php if($post['post_type'] == 'link'){ ?>
                            <div class="pvideo-holder">
                                <?php if($post['image'] != 'No Image'){ ?>
                                <div class="img-holder"><img src="<?= $post['image'] ?>"/></div>
                                <div class="desc-holder">
                                <?php } ?>
                                    <h4><a href="<?= $post['post_text']?>" target="_blank"><?= $post['link_title'] ?></a></h4>
                                    <p><?= $post['link_description'] ?></p>
                                <?php if($post['image'] != 'No Image'){ ?>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } else { ?>
                            <div class="post-details">
                                <?php if(!empty($posttitle)) { ?><div class="post-title"><?=$posttitle?></div><?php } ?>
                                <?php if(!empty($posttext)){  ?>
                                    <div class="post-desc">
                                        <?php if(strlen($posttext)>187){ ?>
                                            <div class="para-section">
                                                <div class="para">
                                                    <p><?=$posttext?></p>
                                                </div>
                                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                            </div>
                                        <?php }else{ ?>                                     
                                            <p><?=$posttext?></p>
                                        <?php } ?>                                  
                                    </div>
                                <?php } ?>
                            </div>
                            <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                        </div>
                        <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image')) {
                                $cnt = 1;
                                $eximgs = explode(',',$post['image'],-1);
                                $totalimgs = count($eximgs);
                                $imgcountcls="";
                                if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                if($totalimgs > '5'){$imgcountcls = 'more-img';}
                            ?>
                            <div class="post-img-holder">
                                <div class="post-img <?= $imgcountcls?> gallery swipe-gallery">
                                    <?php
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                            <?php if($cnt == 5 && $totalimgs > 5){?>
                                                <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                            <?php } ?>
                                        </a>
                                    <?php } $cnt++; } ?>
                                </div>
                                <?php if($imgcountcls == "one-img") {
                                    if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                        <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                    <?php } else { 
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $image = '../web/uploads/gallery/'.$iname;
                                        $JIDSsdsa = '';           
                                        $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                        if(!empty($pinit)) {
                                            $JIDSsdsa = 'active';                    
                                        }
                                        ?>
                                        <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                    <?php 
                                    } 
                                } 
                                ?>
                            </div>
                        <?php } } ?>
                    </div>
                    <div class="clear"></div>
                    <div class="post-review">
                        <a href="javascript:void(0)" class="approve-btn" onclick="hideReviewPost(this,'approved','<?=$postid?>')"><i class="zmdi zmdi-check"></i></a>
                        <a href="javascript:void(0)" class="reject-btn" onclick="hideReviewPost(this,'rejected','<?=$postid?>')"><i class="mdi mdi-close"></i></a>
                    </div>
                </div>
            </div>
        </li>
    <?php }
    
    public function display_review_message()
    {
        echo '<div class="no-listcontent">
            Your last post on this page is in review.
        </div>';
    }
    
    public function getAdType($adtype)
    {
        if($adtype == 'pagelikes'){return 'Page Engagement';}
        else if($adtype == 'brandawareness'){return 'Brand Awareness';}
        else if($adtype == 'websiteleads'){return 'Website Leads';}
        else if($adtype == 'websiteconversion'){return 'Website Conversion';}
        else if($adtype == 'inboxhighlight'){return 'Inbox Highlight';}
        else if($adtype == 'pageendorse'){return 'Page Endorsement';}
        else if($adtype == 'travstorefocus'){return 'Travstore Focus';}
        else if($adtype == 'eventpromo'){return 'Event Promo';}
        else{return '-';}
    }

    public function display_ad_md($adid,$section)
    {
        $session = Yii::$app->session;
        $uid = (string)$session->get('user_id');
        $logo = '';
        $name = '';
        $catch = '';
        $image = '';
        $header = '';
        $text = '';
        $link = '';
        $adbtn = '';
          
        
        $post = PostForm::find()->where(['_id' => $adid])->one();
        if($post['adobj'] == 'pagelikes' || $post['adobj'] == 'pageendorse')
        {
            $page_id = $post['adid'];
            $logo = $this->getpageimage($page_id);
            $page_details = Page::Pagedetails($page_id);
            $name = $page_details['page_name'];
            $catch = $post['adcatch'];
            $header = $post['adheadeline'];
            $text = $post['adtext'];
            $image = $post['adimage'];
            if($image == 'undefined' || !file_exists('../web/'.$image) || $image == '')
            {
                $image = $this->assetsPath.'pagead-endorse-demo.png';
            }
            if($post['adobj'] == 'pageendorse')
            {
                $othertext = 'people endorsed this page';
                $count = PageEndorse::getAllEndorseCount($page_id);
                $adbtn = 'Endorse';
            }
            else
            {
                $othertext = 'people liked this page';
                $count = Like::getLikeCount($page_id);
                $adbtn = 'Like';
            }
            if($post['adobj'] == 'pageendorse')
            {
                $link = $post['adid'];
            }
            else
            {
                $link = 'javascript:void(0)';
            }
        }
        else if($post['adobj'] == 'brandawareness')
        {
            $logo = '';
            $header = '';
            $catch = $post['adcatch'];
            $text = $post['adtext'];
            $link = $post['adurl'];
            if(!empty($link))
            {
                if(substr( $link, 0, 4 ) === "http")
                {
                    $link = $link;
                }
                else
                {
                    $link = 'https://'.$link;
                }
            }
            else
            {
                $link = 'Not added';
            }
            $image = $post['adimage'];
            if($image == 'undefined' || !file_exists('../web/'.$image) || $image == '')
            {
                $image = $this->assetsPath.'brandaware-demo.png';
            }
            $adbtn = 'Explore';
        }
        else if($post['adobj'] == 'websiteleads')
        {
            $logo = $post['adlogo'];
            if($logo == 'undefined' || !file_exists('../web/'.$logo) || $logo == '')
            {
                $logo = $this->assetsPath.'demo-business.jpg';
            }
            $name = $post['adtitle'];
            $catch = $post['adcatch'];
            $header = $post['adheadeline'];
            $text = $post['adtext'];
            $adurl = $post['adurl'];
            $link = $post['adurl'];
            if(!empty($link))
            {
                if(substr( $link, 0, 4 ) === "http")
                {
                    $link = $link;
                }
                else
                {
                    $link = 'https://'.$link;
                }
            }
            else
            {
                $link = 'Not added';
            }
            $image = $post['adimage'];
            if($image == 'undefined' || !file_exists('../web/'.$image) || $image == '')
            {
                $image = $this->assetsPath.'webleads-demo.png';
            }
        }
        else if($post['adobj'] == 'websiteconversion')
        {
            $logo = $post['adlogo'];
            if($logo == 'undefined' || !file_exists('../web/'.$logo) || $logo == '')
            {
                $logo = $this->assetsPath.'demo-business.jpg';
            }
            $name = $post['adtitle'];
            $catch = $post['adcatch'];
            $header = $post['adheadeline'];
            $text = $post['adtext'];
            $adbtn = $post['adbtn'];
            $link = $post['adurl'];
            if(!empty($link))
            {
                if(substr( $link, 0, 4 ) === "http")
                {
                    $link = $link;
                }
                else
                {
                    $link = 'https://'.$link;
                }
            }
            else
            {
                $link = 'Not added';
            }
            $image = $post['adimage'];
            if($image == 'undefined' || !file_exists('../web/'.$image) || $image == '')
            {
                $image = $this->assetsPath.'webconversion-demo.png';
            }
        }
        else if($post['adobj'] == 'eventpromo')
        {
            $eventid = $post['adid'];
            $logo = '';
            $event_result = PageEvents::getEventdetails($eventid);
            $header = $post['adheadeline'];
            $catch = $post['adcatch'];
            $name = $event_result['event_name'];
            $address = $event_result['address'];
            $text = $post['adtext'];
            $image = $post['adimage'];
            $eventdate = $event_result['event_date'];
            $month = date("M", strtotime($eventdate));
            $date = date("d", strtotime($eventdate));
            if($image == 'undefined' || !file_exists('../web/'.$image))
            {
                $image = $this->assetsPath."eventad-demo.png";
            }
            $count = EventVisitors::getEventCounts($eventid);
            $othertext = 'people attending';
            $adbtn = 'Going';
            $link = 'javascript:void(0)';
        }
        
        $Auth = '';
        if(isset($uid) && $uid != '') 
        {
            $authstatus = UserForm::isUserExistByUid($uid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
            {
                $Auth = $authstatus;
            }
        }   
        else    
        {
            $Auth = 'checkuserauthclassg';
        }
        
        //echo $logo; die();
        if($section == 'topbar') {
    ?>
    <div class="post-topbar">
        <div class="post-userinfo">
            <?php if($post['adobj'] != 'brandawareness'){ ?>
            <div class="img-holder">
                <div class="profiletipholder">
                    <span class="profile-tooltip tooltipstered">
                        <?php if($post['adobj'] == 'eventpromo'){ ?>
                        <div class="travad-maintitle"><span><i class="mdi mdi-calendar"></i></span></div>
                        <?php } else { ?>
                        <img class="circle" src="<?=$logo?>" title="ad">
                        <?php } ?>
                    </span>
                </div>
            </div>
            <div class="desc-holder">
                <a href="javascript:void(0)"><?=$name?></a>
                <span class="timestamp">Sponsored Ad</span>
            </div>
            <?php } else { ?>
            <div class="post-userinfo">
                <?=$catch?>
            </div>
            <?php } ?>
        </div>
        <div class="settings-icon">
            <div class="dropdown dropdown-custom dropdown-xxsmall">
                <!-- Dropdown Trigger -->   
                     <?php if($Auth == 'checkuserauthclassg') { ?>
                        <a class='dropdown-button more_btn <?=$Auth?> directcheckuserauthclass' href='javascript:void(0)'>
                          <i class="zmdi zmdi-more"></i>
                        </a>
                     <?php } else { ?>  
                <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='setting_btn_<?= $post['_id']?>'>
                  <i class="zmdi zmdi-more"></i>
                </a>
                     <?php } ?>
                <!-- Dropdown Structure -->
                <ul id='setting_btn_<?= $post['_id']?>' class='dropdown-content custom_dropdown'>                   
                    <li>
                        <a href="javascript:void(0)" class="hidepost" onclick="hidePost('<?php echo $post['_id']?>')">Hide ad</a>
                    </li>                                                      
                </ul>
            </div>
       </div>        
    </div>
    <?php } if($section == 'content') {
    ?>
    <div class="post-content">                          
        <div class="shared-box shared-category">
            <div class="post-holder">                                   
                <div class="post-content">
                    <?php if($post['adobj'] != 'brandawareness'){ ?>
                    <div class="post-desc">
                        <?php if(strlen($catch)>187){ ?>
                            <div class="para-section">
                                <div class="para">
                                    <p><?=$catch?></p>
                                </div>
                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                            </div>
                        <?php }else{ ?>                                     
                            <p><?=$catch?></p>
                        <?php } ?>
                    </div>  
                    <?php } ?>
                    <div class="post-img-holder">
                        <div class="post-img one-img">
                            <div class="pimg-holder"><img src="<?=$image?>" style="visibility: visible; opacity: 1;"></div>
                        </div>
                    </div>
                    <div class="share-summery">
                        <?php if($post['adobj'] == 'eventpromo'){ ?>
                        <div class="datebox"><span class="month"><?=$month?></span><span class="date"><?=$date?></span></div>
                        <?php } ?>
                        <?php if($post['adobj'] != 'brandawareness'){ ?>
                        <div class="travad-title"><?=$header?></div>
                        <?php } ?>
                        <div class="travad-subtitle"><?=$text?></div>
                        <?php if($post['adobj'] == 'pagelikes'){ ?>
                        <div class="travad-info likecount_<?=$post['adid']?>">
                            
                            <?php if($count > 0){ ?><?=$count?> liked this page
                    <?php } else { ?>Become a first to like this page<?php } ?>
                        </div>
                        <?php } ?>
                        <?php if($post['adobj'] == 'pageendorse' || $post['adobj'] == 'eventpromo'){ ?>
                        <div class="travad-info likecount_<?=$post['adid']?>"><?=$count?> <?=$othertext?></div>
                        <?php } ?>
                        <?php if($post['adobj'] == 'websiteleads'){ ?>
                        <a href="javascript:void(0)" class="adlink" onclick="viewAdSite('<?=$adid?>','<?=$link?>','click')"><i class="mdi mdi-earth"></i><span><?=$adurl?></span></a>
                        <?php } 
                        else if($post['adobj'] == 'pagelikes'){ ?>
                        <?php if($page_details['created_by'] == $uid) { ?>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn" onclick="viewAdObjSite('<?=$adid?>','<?=$page_id?>')"><span class="likestatus_<?=$page_id?>">Admin</span></a>
                        <?php } else { ?>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn" onclick="viewAdObjSite('<?=$adid?>','<?=$page_id?>','action','pagelikes')"><i class="zmdi zmdi-thumb-up"></i><span class="likestatus_<?=$post['adid']?>">Like</span></a>
                        <?php } ?>
                        <?php } 
                         else { ?>
                         <?php if($post['adobj'] == 'pageendorse') { ?>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn" onclick="viewAdObjSite('<?=$adid?>','<?=$link?>','action','pageendorse')"><?=$adbtn?></a>
                         <?php } else { ?>
                            <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn" onclick="viewAdSite('<?=$adid?>','<?=$link?>','click')"><?=$adbtn?></a>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>                                                                              
            </div>
        </div>
    </div>
    <?php } }
    
    public function display_ad($adid,$section)
    {
        $session = Yii::$app->session;
        $uid = (string)$session->get('user_id');
        $logo = '';
        $name = '';
        $catch = '';
        $image = '';
        $header = '';
        $text = '';
        $link = '';
        $adbtn = '';
        $rand = rand(9999, 999999);  
        $Auth = '';
        if(isset($uid) && $uid != '') 
        {
            $authstatus = UserForm::isUserExistByUid($uid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
            {
                $Auth = $authstatus;
            }
        }   
        else    
        {
            $Auth = 'checkuserauthclassg';
        }

        $post = PostForm::find()->where(['_id' => $adid])->asarray()->one();
        $post_id = (string)$post['_id'];

        if($post['adobj'] == 'pagelikes' || $post['adobj'] == 'pageendorse')
        {
            $page_id = $post['adid'];
            $logo = $this->getpageimage($page_id);
            $page_details = Page::Pagedetails($page_id);
            $name = $page_details['page_name'];
            $catch = $post['adcatch'];
            $header = $post['adheadeline'];
            $text = $post['adtext'];
            $image = $post['adimage'];
            if($image == 'undefined' || !file_exists('../web/'.$image) || $image == '')
            {
                $image = $this->assetsPath.'pagead-endorse-demo.png';
            }
            if($post['adobj'] == 'pageendorse')
            {
                $othertext = 'people endorsed this page';
                $count = PageEndorse::getAllEndorseCount($page_id);
                $adbtn = 'Endorse';
            }
            else
            {
                $othertext = 'people liked this page';
                $count = Like::getLikeCount($page_id);
                $adbtn = 'Like';
            }
            if($post['adobj'] == 'pageendorse')
            {
                $link = $post['adid'];
            }
            else
            {
                $link = 'javascript:void(0)';
            }
        }
        else if($post['adobj'] == 'brandawareness')
        {
            $logo = '';
            $header = '';
            $catch = $post['adcatch'];
            $text = $post['adtext'];
            $link = $post['adurl'];
            if(!empty($link))
            {
                if(substr( $link, 0, 4 ) === "http")
                {
                    $link = $link;
                }
                else
                {
                    $link = 'https://'.$link;
                }
            }
            else
            {
                $link = 'Not added';
            }
            $image = $post['adimage'];
            if($image == 'undefined' || !file_exists('../web/'.$image) || $image == '')
            {
                $image = $this->assetsPath.'brandaware-demo.png';
            }
            $adbtn = 'Explore';
        }
        else if($post['adobj'] == 'websiteleads')
        {
            $logo = $post['adlogo'];
            if($logo == 'undefined' || !file_exists('../web/'.$logo) || $logo == '')
            {
                $logo = $this->assetsPath.'demo-business.jpg';
            }
            $name = $post['adtitle'];
            $catch = $post['adcatch'];
            $header = $post['adheadeline'];
            $text = $post['adtext'];
            $adurl = $post['adurl'];
            $link = $post['adurl'];
            if(!empty($link))
            {
                if(substr( $link, 0, 4 ) === "http")
                {
                    $link = $link;
                }
                else
                {
                    $link = 'https://'.$link;
                }
            }
            else
            {
                $link = 'Not added';
            }
            $image = $post['adimage'];
            if($image == 'undefined' || !file_exists('../web/'.$image) || $image == '')
            {
                $image = $this->assetsPath.'webleads-demo.png';
            }
        }
        else if($post['adobj'] == 'websiteconversion')
        {
            $logo = $post['adlogo'];
            if($logo == 'undefined' || !file_exists('../web/'.$logo) || $logo == '')
            {
                $logo = $this->assetsPath.'demo-business.jpg';
            }
            $name = $post['adtitle'];
            $catch = $post['adcatch'];
            $header = $post['adheadeline'];
            $text = $post['adtext'];
            $adbtn = $post['adbtn'];
            $link = $post['adurl'];
            if(!empty($link))
            {
                if(substr( $link, 0, 4 ) === "http")
                {
                    $link = $link;
                }
                else
                {
                    $link = 'https://'.$link;
                }
            }
            else
            {
                $link = 'Not added';
            }
            $image = $post['adimage'];
            if($image == 'undefined' || !file_exists('../web/'.$image) || $image == '')
            {
                $image = $this->assetsPath.'webconversion-demo.png';
            }
        }
        else if($post['adobj'] == 'eventpromo')
        {
            $eventid = $post['adid'];
            $logo = '';
            $event_result = PageEvents::getEventdetails($eventid);
            $header = $post['adheadeline'];
            $catch = $post['adcatch'];
            $name = $event_result['event_name'];
            $address = $event_result['address'];
            $text = $post['adtext'];
            $image = $post['adimage'];
            $eventdate = $event_result['event_date'];
            $month = date("M", strtotime($eventdate));
            $date = date("d", strtotime($eventdate));
            if($image == 'undefined' || !file_exists('../web/'.$image))
            {
                $image = $this->assetsPath."eventad-demo.png";
            }
            $count = EventVisitors::getEventCounts($eventid);
            $othertext = 'people attending';
            $adbtn = 'Going';
            $link = 'javascript:void(0)';
        }
        if($section == 'topbar') {
    ?>
    <div class="post-topbar">
        <div class="post-userinfo">
            <?php if($post['adobj'] != 'brandawareness'){ ?>
            <div class="img-holder">
                <div class="profiletipholder">
                    <span class="profile-tooltip tooltipstered">
                        <?php if($post['adobj'] == 'eventpromo'){ ?>
                        <div class="travad-maintitle">
                            <span><i class="mdi mdi-calendar"></i></span>
                        </div>
                        <?php } else { ?>
                        <img class="circle" src="<?=$logo?>" title="ad">
                        <?php } ?>
                    </span>
                </div>
            </div>
            <div class="desc-holder">
                <a href="javascript:void(0)"><?=$name?></a>
                <span class="timestamp">Sponsored Ad</span>
            </div>
            <?php } else { ?>
            <div class="post-userinfo">
                <?=$catch?>
            </div>
            <?php } ?>
        </div>
        <div class="settings-icon">
            <div class="dropdown dropdown-custom dropdown-xxsmall">
                <a class="dropdown-button <?=$Auth?> directcheckuserauthclass" href="javascript:void(0)" data-activates="dropdown-ads<?=$rand?>">
                    <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                </a>
                <ul class="dropdown-content custom_dropdown" id="dropdown-ads<?=$rand?>">
                    <li>
                        <ul class="post-sicon-list">
                            <li class="nicon" onclick="hidePost('<?php echo $post['_id']?>')"><a href="javascript:void(0)">Hide ad</a></li>
                        </ul>                                       
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php } if($section == 'content') {
    ?>
    <div class="post-content">                          
        <div class="shared-box shared-category">
                    <div class="post-details">
                    <?php if($post['adobj'] != 'brandawareness'){ ?>
                    <div class="post-desc">
                        <?php if(strlen($catch)>187){ ?>
                            <div class="para-section">
                                <div class="para">
                                    <p><?=$catch?></p>
                                </div>
                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                            </div>
                        <?php }else{ ?>                                    
                            <div class="para-section">
                                <div class="para">
                                    <p><?=$catch?></p>
                                </div>
                            </div> 
                        <?php } ?>
                    </div>  
                    <?php } ?>
                    </div>
                    <div class="post-img-holder">
                        <div class="post-img one-img">
                            <div class="pimg-holder"><img src="<?=$image?>" style="visibility: visible; opacity: 1;"></div>
                        </div>
                    </div>
                    <div class="share-summery">
                        <?php if($post['adobj'] == 'eventpromo'){ ?>
                        <div class="datebox"><span class="month"><?=$month?></span><span class="date"><?=$date?></span></div>
                        <?php } ?>
                        <?php if($post['adobj'] != 'brandawareness'){ ?>
                        <div class="travad-title"><?=$header?></div>
                        <?php } ?>
                        <div class="travad-subtitle"><?=$text?></div>
                        <?php if($post['adobj'] == 'pagelikes'){ ?>
                        <div class="travad-info likecount_<?=$post['adid']?>">
                            
                            <?php if($count > 0){ ?><?=$count?> liked this page
                    <?php } else { ?>Become a first to like this page<?php } ?>
                        </div>
                        <?php } ?>
                        <?php if($post['adobj'] == 'pageendorse' || $post['adobj'] == 'eventpromo'){ ?>
                        <div class="travad-info likecount_<?=$post['adid']?>"><?=$count?> <?=$othertext?></div>
                        <?php } ?>
                        <?php if($post['adobj'] == 'websiteleads'){ ?>
                        <a href="javascript:void(0)" class="adlink" onclick="viewAdSite('<?=$adid?>','<?=$link?>','click')"><i class="mdi mdi-earth"></i><span><?=$adurl?></span></a>
                        <?php } 
                        else if($post['adobj'] == 'pagelikes'){ ?>
                        <?php if($page_details['created_by'] == $uid) { ?>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn disactive" onclick="viewAdObjSite('<?=$adid?>','<?=$page_id?>')"><span class="likestatus_<?=$page_id?>">Admin</span></a>
                        <?php } else {
                            $already_like = Like::find()->select(['_id','status'])->where(['post_id' => $post_id , 'user_id' => $uid])->one();

                            $status = $already_like['status'];
                            if($status == '1') {
                                $label = 'Liked';
                                $label_st = 'active';
                            } else {
                                $label = 'Like';
                                $label_st = 'disactive';
                            }
                            ?>    
                            <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn <?=$label_st?>" onclick="viewAdObjSite('<?=$adid?>','<?=$page_id?>','action','pagelikes')" data-post_id="<?=$post_id?>" data-user_id="<?=$uid?>"><i class="zmdi zmdi-thumb-up"></i><span class="likestatus_<?=$post['adid']?>"><?=$label?></span></a>
                        <?php } ?>
                        <?php } 
                         else { ?>
                         <?php if($post['adobj'] == 'pageendorse') { ?>
                        <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn" onclick="viewAdObjSite('<?=$adid?>','<?=$link?>','action','pageendorse')"><?=$adbtn?></a>
                         <?php } else { ?>
                            <a href="javascript:void(0)" class="btn btn-primary btn-sm adbtn" onclick="viewAdSite('<?=$adid?>','<?=$link?>','click')"><?=$adbtn?></a>
                            <?php } ?>
                        <?php } ?>
                    </div>              
        </div>
    </div>
    <?php } }
     
    
    public function filterDisplayLastPost($postid, $postownerid, $postprivacy) {
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');        
        $reportpost = ReportPost::find()->where(['post_id' => $postid,'reporter_id' => (string)$user_id])->one();
        $isValid = false;
        if(!($reportpost))
        {  
            $suserid = $user_id;
            $is_connect = Connect::find()->where(['from_id' => $postownerid,'to_id' => $suserid,'status' => '1'])->one();
            if($postownerid == $suserid) {
                $isValid = true;
            } else if($postprivacy == 'Public') {
                $isValid = true;
            } else if($postprivacy == 'Connections' && !empty($is_connect)) {
                $isValid = true;
            } else if($postprivacy == 'Private' && $postownerid == $suserid) {
                $isValid = true;
            } else if($postprivacy == 'Custom') {
                $issearch = 'yes';
                $post = PostForm::find()->select(['customids'])->where([(string)'_id' => $postid])->one();
                if(!empty($post)) {
                    $issearch = 'no';   
                }

                if($issearch == 'yes') {
                    $post = PlaceReview::find()->select(['customids'])->where([(string)'_id' => $postid])->andWhere(['not','flagger', "yes"])->one();
                    if(!empty($post)) {
                        $issearch = 'no';   
                    }
                }

                if($issearch == 'yes') {
                    $post = PlaceDiscussion::find()->select(['customids'])->where([(string)'_id' => $postid])->andWhere(['not','flagger', "yes"])->one();
                    if(!empty($post)) {
                        $issearch = 'no';   
                    }
                }

                if($issearch == 'yes') {
                    $post = PlaceAsk::find()->select(['customids'])->where([(string)'_id' => $postid])->andWhere(['not','flagger', "yes"])->one();
                    if(!empty($post)) {
                        $issearch = 'no';   
                    }
                }

                if($issearch == 'yes') {
                    $post = PlaceTip::find()->select(['customids'])->where([(string)'_id' => $postid])->andWhere(['not','flagger', "yes"])->one();
                    if(!empty($post)) {
                        $issearch = 'no';   
                    }
                }

                if(!empty($post)) {
                    $customids = isset($post['customids']) ? $post['customids'] : '';
                    $customIds = explode(",", $customids);
                    if(in_array($user_id, $customIds)) {
                        $isValid = true;
                    }
                }
                $isValid = true;
            }
                
            if($isValid) {
                $block = BlockConnect::find()->where(['user_id' => $postownerid])->andwhere(['like','block_ids',$suserid])->one();

                if(!$block) {
                    $status = 'ok2389Ko';
                    return $status;
                }
            }
        }
        return false;
    }

    public function display_last_post($last_insert_id, $existing_posts = '', $imagefullarray = '', $cls='', $total='', $tempocls='')
    {
        $isAdsClsLabel = '';
        $session = Yii::$app->session;
        $userid = $user_id = (string)$session->get('user_id');
        $status = $session->get('status');
        $date = time();
        $profile_tip_counter = 1;
        
        $issearch = 'yes';
        $post = PostForm::find()->where([(string)'_id' => $last_insert_id])->one();
        $tableLabel = 'PostForm';

        if(!empty($post)) {
            $issearch = 'no';   
        }

        if($issearch == 'yes') {
            $post = PlaceReview::find()->where([(string)'_id' => $last_insert_id])->andWhere(['not','flagger', "yes"])->one();
            if(!empty($post)) {
                $issearch = 'no';   
            }
        }

        if($issearch == 'yes') {
            $post = PlaceDiscussion::find()->where([(string)'_id' => $last_insert_id])->andWhere(['not','flagger', "yes"])->one();
            if(!empty($post)) {
                $issearch = 'no';   
            }
        }

        if($issearch == 'yes') {
            $post = PlaceAsk::find()->where([(string)'_id' => $last_insert_id])->andWhere(['not','flagger', "yes"])->one();
            if(!empty($post)) {
                $issearch = 'no';   
            }
        }

        if($issearch == 'yes') {
            $post = PlaceTip::find()->where([(string)'_id' => $last_insert_id])->andWhere(['not','flagger', "yes"])->one();
            if(!empty($post)) {
                $issearch = 'no';   
            }
        }

        $Auth = '';
        if(isset($userid) && $userid != '') 
        {
            $authstatus = UserForm::isUserExistByUid($userid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
            {
                $Auth = $authstatus;
            }
        }   
        else    
        {
            $Auth = 'checkuserauthclassg';
        }

                $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => (string)$post['_id']])->one();
                $savestatuscalue = $savestatus['is_saved'];
                if($savestatuscalue == '1'){$savevar = 'Unsave';}else{$savevar = 'Save';}

                $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
                $like_count = Like::getLikeCount((string)$post['_id']);
                $comments = Comment::getAllPostLike($post['_id']);
                $init_comments = Comment::getFirstThreePostComments($post['_id']);
                $post_privacy = $post['post_privacy'];
                if($post_privacy == 'Private') {$post_class = 'lock';}
                else if($post_privacy == 'Connections') {$post_class = 'account';}
                else if($post_privacy == 'Custom') {$post_class = 'settings';}
                else {$post_class = 'earth';}

                // START add tag content in post
                $posttag = '';
                if(isset($post['post_tags']) && !empty($post['post_tags']))
                {
                    $posttag = explode(",", $post['post_tags']);
                }

                $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');

                $nkTag = array();
                $nvTag = array();

                $i=1;
                foreach ($taginfomatiom as $key => $value)
                {
                    $nkTag[] = (string)$value; 
                    $nvTag[] = $key;
                    if($i != 1) {
                        $content[] = $key;
                    }
                    $i++;
                }

                if(isset($content) && !empty($content))
                {
                    $content = implode("<br/>", $content); 
                }

                $tagstr = '';
                if(!empty($taginfomatiom))
                {
                    if(count($taginfomatiom) > 1) {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
                    } else {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
                    }
                }
                if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                {
                    $page_details = Page::Pagedetails($post['page_id']);
                    $review = " reviewed <a href=".Url::to(['page/index', 'id' => $post['page_id']]) ." class='sub-link'>" . $page_details['page_name'] . "</a>"; }
                else { $review = ''; }
                // END add tag content in post
                $profile_tip_counter++;         
                if(isset($post['currentlocation']) && !empty($post['currentlocation']))
                {
                    $current_location = $this->getshortcityname($post['currentlocation']);
                    $default = 'at';
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='reviews'){$default = 'reviewed';}
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='tip'){$default = 'tip for';}
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='ask'){$default = 'has a question about';}
                    $locationtags = $tagstr . ' '.$default.' <a class="sub-link" href="javascript:void(0)">'.$current_location.'</a>';
                }
                else{ $locationtags = $tagstr . ''; }
                if($post['is_timeline'] == '1')
                {
                    $getrateid = PostForm::find()->where(['_id' => $post['parent_post_id']])->one();
                    $getratedid = $getrateid['rating'];
                    if($getratedid == null){$getratedid = '6';}
                }
                else
                {
                    $getratedid = '6';
                }
                if((isset($post['is_ad']) && !empty($post['is_ad']) && $post['is_ad'] == '1'))
                {
                    $isAdsClsLabel = 'Ads89HG';
                    $is_ad = true;
                    $adclass = 'travad-box ';
                    if($post['adobj'] == 'pagelikes'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'brandawareness'){$adclass .= 'brand-ad';}
                    else if($post['adobj'] == 'websiteleads'){$adclass .= 'weblink-ad';}
                    else if($post['adobj'] == 'websiteconversion'){$adclass .= 'actionlink-ad';}
                    else if($post['adobj'] == 'pageendorse'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'eventpromo'){$adclass .= 'event-ad';}
                    else{$adclass .= '';}
                } else { 
                    $is_ad = false; $adclass = '';
                }
        ?>
        <div class="col s12 m12 <?=$isAdsClsLabel?>" data-postid='<?=$last_insert_id?>'>
            <div class="<?=$tempocls .' '.$cls?> post-holder <?php if($existing_posts != 'from_save'){ ?>bshadow<?php } ?> <?php if((isset($post['is_page_review']) && !empty($post['is_page_review']) || ($getratedid != '6'))){ ?>reviewpost-holder<?php } ?> <?php if((isset($post['trav_item']) && !empty($post['trav_item']))){ ?>travelstore-ad<?php } ?> <?php if((isset($post['is_ad']) && !empty($post['is_ad']))){ echo $adclass; } ?> post_user_id_<?php echo $post['post_user_id'];?>" id="hide_<?php echo $post['_id'];?>">
            <?php
            $getuserinfo = LoginForm::find()->where(['_id' => $post['post_user_id']])->one();
            $personalinfo = Personalinfo::find()->where(['user_id' => $post['post_user_id']])->one();
            if($post['user']['status']=='44')
            {
                $dp = $this->getpageimage($post['user']['_id']);
                $link = Url::to(['page/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'page';
            }
            else
            {
                $dp = $this->getimage($getuserinfo['_id'],'photo');
                $link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'userwall';
            }
            if(isset($getuserinfo['cover_photo']) && !empty($getuserinfo['cover_photo']))
            {
                $cover = $getuserinfo['cover_photo'];
                if(strstr($cover,'cover'))
                {
                    $cvr = substr_replace($getuserinfo['cover_photo'], '-thumb.jpg', -4);
                    $cover = '../uploads/cover/thumbs/'.$cvr;
                }
                else
                {
                    $cover = '../uploads/cover/thumbs/thumb_'.$getuserinfo['cover_photo'];
                }
            }
            else
            {
                $cover = 'cover.jpg';
            }
            $dpforpopup = $this->getimage($post['user']['_id'],'thumb');
            $mutualcount = Connect::mutualconnectcount($getuserinfo['_id']);

            $po_id = $post['post_user_id'];
            $isconnect = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '1'])->one();
            $isconnectrequestsent = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '0'])->one();
            ?>
            <?php if($post['is_trip'] == '1'){ 
                $uniqid = uniqid();
                ?>
                <div class="post-topbar">
                    <div class="post-userinfo">
                        <div class="img-holder">
                            <div class="profiletipholder">
                                <span id="profiletip-<?= $post['_id']?>" class="profile-tooltip">
                                    <img class="circle width-100" src="<?= $dp;?>">
                                </span>
                                <span class="profiletooltip_content profiletooltip_content_last">
                                    <div class="profile-tip slidingpan-holder dis-none" id="dispatchcontentprofiletip-<?= $post['_id']?>">
                                       
                                        <div class="profile-tip-avatar">
                                            <img alt="user-photo" class="img-responsive"  src="<?= $dp?>">
                                            <div class="sliding-pan location-span">
                                                <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                            </div>
                                        </div>
                                        <div class="profile-tip-name">
                                            <a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a>
                                        </div>
                                        <?php if($post['user']['status']!='44'){ ?>
                                        <div class="profile-tip-info">
                                            <div class="profiletip-icon">
                                                    <?php if($userid == $po_id){ ?>
                                                    <a href="javascript:void(0)">&nbsp;</a>
                                                    <?php } else if($isconnect) { ?>
                                                    <a href="javascript:void(0)"><?=$mutualcount?> Mutual connections</a>
                                                    <?php } else if($isconnectrequestsent) { ?>
                                                    <i title="Connect request sent" class="mdi mdi-account-minus"></i>
                                                    <?php } else { ?>
                                                    <a href="javascript:void(0)" title="Add Connect" class="title_<?php echo $po_id;?>">
                                                    <i class="mdi mdi-account-plus people_<?php echo $po_id;?>" onclick="addConnect('<?php echo $po_id;?>')"></i>
                                                    <i title="Connect request sent" class="mdi mdi-account-minus dis-none sendmsg_<?php echo $po_id;?>" ></i>
                                                    <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
                                                    </a>
                                                    <?php } ?>
                                            </div>
                                            <div class="profiletip-icon">
                                                    <a href="<?php echo $link;?>" title="View Profile"><i class="mdi mdi-eye"></i></a>
                                            </div>                  
                                        </div>
                                        <?php } ?>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="desc-holder">
                            <span>By </span><a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a><span class="withtext"><?= $tagstr;?></span>
                            <span class="timestamp"><?php echo $time; ?>
                                <a class="dropdown-button" href="javascript:void(0);" data-activates="<?=$uniqid?>">
                                    <i class="mdi mdi-<?=$post_class?> mdi-13px"></i>
                                    <?php
                                    $pageownerid = $post['post_user_id'];
                                    if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                    <i class="mdi mdi-menu-down mdi-22px"></i>
                                    <?php } ?>
                                </a>

                                <ul id="<?=$uniqid?>" class="dropdown-content directprivacydropdown">
                                    <li class="post-private" data-cls="post-private"> <a href="javascript:void(0)"> Private </a> </li>
                                    <li class="post-connections" data-cls="post-connections"> <a href="javascript:void(0)"> Connections </a> </li>
                                    <li class="post-custom customli_modal_post" data-cls="post-custom"> <a href="javascript:void(0)"> Custom </a> </li>
                                    <li class="post-public" data-cls="post-public"> <a href="javascript:void(0)"> Public </a> </li>
                               </ul>
                            </span>
                        </div>
                    </div>
                    <div class="settings-icon">
                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                        <!-- Dropdown Trigger -->
                        <?php if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                            <a class='dropdown-button more_btn <?=$Auth?> directcheckuserauthclass' href='javascript:void(0)'>
                              <i class="zmdi zmdi-more"></i>
                            </a>
                        <?php } else { ?>
                            <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='setting_btn_<?= $post['_id']?>'>
                              <i class="zmdi zmdi-more"></i>
                            </a>
                            <!-- Dropdown Structure -->
                            <ul id='setting_btn_<?= $post['_id']?>' class='dropdown-content custom_dropdown'>
                                
                                <?php if($status == '10') { if($post['is_deleted'] != '2') { ?> 
                                <li><a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Flag experience </a></li>
                                <?php } } else {?>  
                                    <?php if($userid != $post['post_user_id']) { ?> 
                                    <li>
                                        <a href="javascript:void(0)" class="hidepost" onclick="hidePost('<?php echo $post['_id']?>')">Hide experience</a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="savepost-link" id="save_post_<?php  echo $post['_id'];?>" data-pid="<?php  echo $post['_id'];?>" onclick="savePost('<?php echo $post['_id']?>','<?php echo $post['post_type']?>','<?=$savevar?>','normal')"><?=$savevar?> experience</a>
                                    </li>
                                    <?php } ?>
                                    <input type="hidden" class="tbsp_<?=$post['_id']?>" value="<?=$savevar?>">
                                    <li>
                                        <a href="javascript:void(0)" onclick="turn_off_notification('<?=$post['_id'] ?>')" class="turnnotpost_<?=$post['_id'] ?>">
                                        <?php
                                        $turnoff = TurnoffNotification::find()->where(['user_id' => "$user_id"])->andwhere(['like','post_ids',$post['_id']])->one();
                                        if($turnoff) { $turnoffvalue = 'Unmute this experience notification';}
                                        else { $turnoffvalue = 'Mute this experience notification';}?>
                                        <?=$turnoffvalue?>
                                        </a> 
                                    </li>    
                                    <?php if($post['user']['status']=='44' && ($post['is_coverpic']=='1' || $post['is_profilepic']=='1')) { $page_details = Page::Pagedetails($post['post_user_id']); $pageownerid = $page_details['created_by']; }
                                    else { $pageownerid = $post['post_user_id']; } ?>
                                    <?php if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                        <li>
                                            <a href="javascript:void(0)" class="deletepost-link" onclick="deletePost('<?php echo $post['post_user_id']?>','<?php echo $post['_id']?>')">Delete experience</a>
                                        </li>
                                        <?php if(($post['is_coverpic']!='1' && $post['is_profilepic']!='1' && $post['shared_from']!='shareentity')){ ?>
                                        <li>
                                            <a href="javascript:void(0)" data-editpostid="<?php echo $post['_id'];?>" class="composeeditpostAction editpost-link">Edit experience</a>
                                        </li>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php if(($userid != $post['post_user_id']) && ($user_id != $post['shared_by']) && ($user_id != $pageownerid)){
                                        $muteuser = UnfollowConnect::find()->where(['user_id' => "$user_id"])->andwhere(['like','unfollow_ids',$post['post_user_id']])->one();
                                        if($muteuser) { $mutevalue = 'Unmute this member posts';}
                                        else { $mutevalue = 'Mute this member posts';}
                                    ?>
                                        <li>
                                            <a href="javascript:void(0)" onclick="unfollowConnect('<?php echo $post['post_user_id']?>')"><?=$mutevalue?></a>
                                        </li>
                                        <?php 
                                        $userblock = BlockConnect::find()->select(['block_ids'])->where(['user_id' => "$user_id"])->one();
                                        if ($userblock)
                                        {
                                            if (strstr($userblock['block_ids'], $post['post_user_id']))
                                            {
                                                    $getblock = 'Unblock';
                                            }
                                            else
                                            {
                                                    $getblock = 'Block';
                                            }
                                        }
                                        else
                                        {
                                            $getblock = 'Block';
                                        }
                                        ?>
                                        <li>
                                            <a href="javascript:void(0)" onclick="blockConnect('<?php echo $post['post_user_id']?>')" class="getblock_<?php echo $post['post_user_id']?>"><?=$getblock?></a>
                                        </li>
                                        <li>
                                            <a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Report experience</a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>                                
                            </ul>
                        <?php } ?>
                    </div>
                   </div>
                </div>
            <?php } else if($is_ad && $post['adobj'] != 'travstorefocus'){
                $this->display_ad_md($post['_id'],'topbar');
            } else { ?>
                <div class="post-topbar">
                    <div class="post-userinfo">
                        <div class="img-holder">
                            <div  class="profiletipholder">
                                <span id="profiletip-<?= $post['_id']?>" class="profile-tooltip">
                                    <?php
                                    if($post['is_timeline']=='1')
                                    {
                                        $dpimg = $this->getimage($post['shared_by'],'photo');
                                    }
                                    else
                                    {
                                        $dpimg = $this->getimage($post['user']['_id'],'photo');
                                    }
                                    ?>
                                    <img class="circle width-100" src="<?= $dpimg?>">
                                </span>
                               <?php if($existing_posts != 'from_save'){ ?>
                                    <span class="profiletooltip_content profiletooltip_content_last">
                                        <div class="profile-tip slidingpan-holder dis-none" id="dispatchcontentprofiletip-<?= $post['_id']?>">
                                           <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive"  src="<?= $dp?>">
                                                <div class="sliding-pan location-span">
                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                            </div>
                                            <div class="profile-tip-name">
                                                <a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a>
                                            </div>
                                            <?php if($post['user']['status']!='44'){ ?>
                                            <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                        <?php if($userid == $po_id){ ?>
                                                        <a href="javascript:void(0)">&nbsp;</a>
                                                        <?php } else if($isconnect) { ?>
                                                        <a href="javascript:void(0)"><?=$mutualcount?> Mutual connections</a>
                                                        <?php } else if($isconnectrequestsent) { ?>
                                                        <i title="Connect request sent" class="mdi mdi-account-minus"></i>
                                                        <?php } else { ?>
                                                        <a href="javascript:void(0)" title="Add Connect" class="title_<?php echo $po_id;?>">
                                                        <i class="mdi mdi-account-plus people_<?php echo $po_id;?>" onclick="addConnect('<?php echo $po_id;?>')"></i>
                                                        <i class="mdi mdi-account-minus dis-none sendmsg_<?php echo $po_id;?>"></i>
                                                        <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
                                                        </a>
                                                        <?php } ?>
                                                </div>
                                                <div class="profiletip-icon">
                                                        <a href="<?php echo $link;?>" title="View Profile"><i class="mdi mdi-eye"></i></a>
                                                </div>                  
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="desc-holder">
                            <?php
                                $uniqid = uniqid();
                                if($post['is_timeline']=='1'/*&& $post['pagepost']==null*/) 
                                {
                                    $getsharename = LoginForm::find()->where(['_id' => $post['shared_by']])->one();
                                    if(strstr($post['parent_post_id'],'event_'))
                                    {
                                        $eventdetails = PageEvents::getEventdetailsshare(substr($post['parent_post_id'],6));
                                        $eid = $eventdetails['_id'];
                                        $ename = $eventdetails['event_name'];
                                        $elink = Url::to(['event/detail', 'e' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s event";
                                    }
                                    else if(strstr($post['parent_post_id'],'page_'))
                                    {
                                        $pagedetails = Page::Pagedetails(substr($post['parent_post_id'],5));
                                        $eid = $pagedetails['page_id'];
                                        $ename = $pagedetails['page_name'];
                                        $elink = Url::to(['page/index', 'id' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s page";
                                    }
                                    
                            ?>
                            <a href="<?php $id =  $getsharename['_id']; echo Url::to([$linkwall.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($getsharename['fname']).' '.ucfirst($getsharename['lname']) ;?></a><?= $review?>
                              <?php if(isset($locationtags) && !empty($locationtags)) { ?>
                                <?= $locationtags?>
                            <?php } } else { ?>
                            <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            <?php if((empty($post['trav_item']))){ ?><?= $locationtags?><?php } ?>
                            <?php if($post['post_type']=='profilepic'){ ?>
                            updated profile photo.
                            <?php } else if($post['is_coverpic']=='1'){ ?>
                            updated cover photo.
                            <?php } ?>
                            <?php } ?>

                            <span class="timestamp"><?=$time?>
                                <a class="dropdown-button" href="javascript:void(0);" data-activates="<?=$uniqid?>">
                                    <i class="mdi mdi-<?=$post_class?> mdi-13px"></i>
                                    <?php 
                                    $pageownerid = $post['post_user_id'];
                                    if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                    <i class="mdi mdi-menu-down mdi-22px"></i>
                                    <?php } ?>
                                </a>
                                <?php if($userid == $post['post_user_id']) { ?>
                                <ul id="<?=$uniqid?>" class="dropdown-content directprivacydropdown">
                                    <li class="post-private" data-cls="post-private"> <a href="javascript:void(0)"> Private </a> </li>
                                    <li class="post-connections" data-cls="post-connections"> <a href="javascript:void(0)"> Connections </a> </li>
                                    <li class="post-custom customli_modal_post" data-cls="post-custom"> <a href="javascript:void(0)"> Custom </a> </li>
                                    <li class="post-public" data-cls="post-public"> <a href="javascript:void(0)"> Public </a> </li>
                                </ul>
                                <?php } ?>
                            </span>
                        </div>
                    </div>
                    <?php
                    $p = 'post';
                    if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                    {
                        $p = 'review';
                    }
                    if(isset($post['placetype']) && !empty($post['placetype']))
                    {
                        $p = $post['placetype'];
                        if($p == 'reviews'){$p = 'review';}
                        if($p == 'ask'){$p = 'question';}
                    }
                    ?>
                    <div class="settings-icon">
                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                        <!-- Dropdown Trigger -->
                        <a class='dropdown-button more_btn <?=$Auth?> directcheckuserauthclass' href='javascript:void(0)' data-activates='setting_btn_<?=$post['_id']?>'>
                          <i class="zmdi zmdi-more"></i>
                        </a>
                        <!-- Dropdown Structure -->
                        <ul id='setting_btn_<?=$post['_id']?>' class='dropdown-content custom_dropdown'>
                            
                            <?php if($status == '10') { ?> 
                            <li><a href="javascript:void(0)" data-id="<?=(string)$post['_id']?>" data-module="<?=$p?>" class="customreportpopup-modal reportpost-link" onclick="flagpost(this)"></span>Flag <?=$p?></a></li>
                            <?php } else {?>  
                                                                
                                    <li><a href="javascript:void(0)" class="hidepost" onclick="hidePost('<?php echo $post['_id']?>')">Hide <?=$p?></a></li>
                                    <li><a href="javascript:void(0)" class="savepost-link" id="save_post_<?php  echo $post['_id'];?>" data-pid="<?php  echo $post['_id'];?>" onclick="savePost('<?php echo $post['_id']?>','<?php echo $post['post_type']?>','<?=$savevar?>','normal')"><?=$savevar?> <?=$p?></a></li>
                                        <input type="hidden" class="tbsp_<?=$post['_id']?>" value="<?=$savevar?>">
                                    <li><a href="javascript:void(0)" onclick="turn_off_notification('<?=$post['_id'] ?>')" class="turnnotpost_<?=$post['_id'] ?>">
                                    <?php
                                    $turnoff = TurnoffNotification::find()->where(['user_id' => "$user_id"])->andwhere(['like','post_ids',$post['_id']])->one();
                                    if($turnoff) { $turnoffvalue = "Unmute notification for this $p";}
                                    else { $turnoffvalue = "Mute notification for this $p";}                                
                                    ?>
                                    <?=$turnoffvalue?>
                                    </a></li>    
                                <?php if($post['user']['status']=='44' && ($post['is_coverpic']=='1' || $post['is_profilepic']=='1')) { $page_details = Page::Pagedetails($post['post_user_id']); $pageownerid = $page_details['created_by']; }
                                else { $pageownerid = $post['post_user_id']; } ?>
                                <?php if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                    <li><a href="javascript:void(0)" class="deletepost-link" onclick="deletePost('<?php echo $post['post_user_id']?>','<?php echo $post['_id']?>')">Delete <?=$p?></a></li>
                                    <?php if(($post['is_coverpic']!='1' && $post['is_profilepic']!='1' && $post['shared_from']!='shareentity')){ ?>
                                    <li><a href="javascript:void(0)" data-editpostid="<?php echo $post['_id'];?>" class="composeeditpostAction editpost-link">Edit <?=$p?></a></li>
                                    <?php } ?>
                                <?php } ?>
                                <?php if(($userid != $post['post_user_id']) && ($user_id != $post['shared_by']) && ($user_id != $pageownerid)){
                                    $muteuser = UnfollowConnect::find()->where(['user_id' => "$user_id"])->andwhere(['like','unfollow_ids',$post['post_user_id']])->one();
                                    if($muteuser) { $mutevalue = 'Unmute this member posts';}
                                    else { $mutevalue = 'Mute this member posts';}
                                ?>
                                    <li><a href="javascript:void(0)" onclick="unfollowConnect('<?php echo $post['post_user_id']?>')"><?=$mutevalue?></a></li>
                                    <li><a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Report post</a></li>
                                <?php } ?>
                            <?php } ?>
                            
                        </ul>
                    </div>
                    </div>
                </div>
            <?php } ?>          
                        <?php
                        if($post['is_timeline']=='1' && isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                        {
                        ?>
                            <div class="post-content on2">
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                <?php if($post['post_text'] != ''){ ?>
                                        <div class="post-desc">
                                            <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                            
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" class="readlink">Read More</a>
                                                    </div>
                                                <?php }else{ ?>                                     
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>
                                                
                                            <?php } ?>
                                        </div>
                                  <?php } ?>
                                    </div>
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                </div>
                            </div>
                            <?php
                            if(isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                            {
                                if(strstr($post['parent_post_id'],'page_'))
                                {
                                    $this->view_page((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'collection_'))
                                {
                                    $this->view_collection((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'trip_'))
                                {
                                    $this->view_trip((string)$post['parent_post_id']);
                                }
                                else
                                {
                                    $this->view_post_id_md((string)$post['parent_post_id']);
                                }
                            }
                        } else if($post['is_trip'] == '1'){
                        ?>
                            <div class="post-content on3 tripexperince-post">
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                        <div class="post-title">
                                            <?= $post['post_title'];?>
                                        </div>
                                    </div>
                                    <div class="trip-summery">
                                        <div class="location-info raani">
                                            <h5><i class="zmdi zmdi-pin"></i>
                                            <?php  
                                            $currentlocation = trim($post['currentlocation']);

                                            if($currentlocation != '') {
                                                $currentlocation = explode(",", $currentlocation);
                                                if(count($currentlocation) >1) {
                                                    $first = reset($currentlocation);
                                                    $last = end($currentlocation);
                                                    $currentlocation = $first.', '.$last;
                                                } else {
                                                    $currentlocation = implode(", ", $currentlocation);
                                                }
                                            }

                                            echo $currentlocation;
                                            ?></h5>
                                            <i class="mdi mdi-menu-right"></i>
                                            <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                        </div>
                                        <!--<div class="views-info"><i class="mdi mdi-eye"></i>45 Views</div>-->
                                    </div>
                                    <div class="map-holder dis-none">
                                        <iframe width="600" height="450" frameborder="0" src="https://maps.google.it/maps?q=<?= $post['currentlocation'];?>&output=embed"></iframe>
                                    </div>
                                </div>
                                <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                    $cnt = 1;
                                    $eximgs = explode(',',$post['image'],-1);
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        if($post['image'] == null)
                                        {
                                            $eximgs[0] = '/uploads/travitem-default.png';
                                        }
                                        $eximgss[] = $eximgs[0];
                                        $eximgs = $eximgss;                                     
                                    }
                                    $totalimgs = count($eximgs);
                                    $imgcountcls="";
                                    if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                    if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                    if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                    if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                    if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                    if($totalimgs > '5'){$imgcountcls = 'more-img';}
                                ?>
                                <div class="post-img-holder one II">
                                    <div class="lgt-gallery lgt-gallery-photo II post-img <?= $imgcountcls?> gallery swipe-gallery dis-none">
                                        <?php
                                        foreach ($eximgs as $eximg) {
                                            if (file_exists('../web'.$eximg)) {
                                                $picsize = '';
                                                $val = getimagesize('../web'.$eximg);
                                                $picsize .= $val[0] .'x'. $val[1] .', ';
                                                $iname = $this->getimagename($eximg);
                                                $inameclass = $this->getimagefilename($eximg);
                                                $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                                if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                                if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                                    <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                                        <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                                        <?php if($cnt == 5 && $totalimgs > 5){?>
                                                            <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                                        <?php } ?>
                                                    </a>
                                                <?php 
                                            } 
                                            $cnt++; 
                                        } ?>
                                    </div>
                                    <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                        if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                            <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                        <?php } else { 
                                            $iname = $this->getimagename($eximg);
                                            $inameclass = $this->getimagefilename($eximg);
                                            $image = '../web/uploads/gallery/'.$iname;
                                            $JIDSsdsa = '';           
                                            $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                            if(!empty($pinit)) {
                                                $JIDSsdsa = 'active';                    
                                            }
                                            ?>
                                            <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                        <?php 
                                        } 
                                    } 
                                    ?>
                                </div>
                                <?php } ?>
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                    <?php 
                                    if($post['post_text'] != ''){ ?>
                                            <div class="post-desc">
                                                <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                                    <?php if(strlen($post['post_text'])>187){ ?>
                                                        <div class="para-section">
                                                            <div class="para">
                                                                <p><?= $post['post_text'] ?></p>
                                                            </div>
                                                            <a href="javascript:void(0)" class="readlink">Read More</a>
                                                        </div>
                                                    <?php } else { ?>                                       
                                                        <p><?= $post['post_text'] ?></p>
                                                    <?php } ?>
                                                
                                                <?php } ?>
                                            </div>
                                      <?php } ?>
                                    </div>  
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                    
                                </div>
                            </div>
                        <?php   
                        } else if($is_ad && $post['adobj'] != 'travstorefocus'){
                            $this->display_ad($post['_id'],'content');
                        }
                        else
                        { if(!isset($post['is_page_review']) && empty($post['is_page_review'])) {
                        ?>
                        <div class="post-content on4 ">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                    <?php if($post['post_title'] != null) { ?>
                                    <div class="post-title"><?= $post['post_title'] ?></div>
                                    <?php } ?>
                                    <?php if(isset($post['placereview']) && !empty($post['placereview']) && $post['placetype']=='reviews'){ ?>
                                    <div class="rating-stars">
                                    <?php for($i=0;$i<5;$i++)
                                    { ?>
                                        <i class="mdi mdi-star <?php if($i < $post['placereview']){ ?>active<?php } ?>"></i>
                                    <?php }
                                    ?>
                                    </div>
                                    <?php } ?>
                                    <?php if($post['trav_price'] != null) { ?>
                                    <div class="post-price">$<?= $post['trav_price'] ?></div>
                                    <?php } ?>
                                    <?php if((isset($post['trav_item']) && !empty($post['trav_item']) && $post['currentlocation'] != null)){ ?>
                                        <div class="post-location" style="display:block"><i class="zmdi zmdi-pin"></i><?= $post['currentlocation'] ?></div>
                                    <?php } ?>
                                    <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                    <div class="post-desc">
                                    
                                        <?php if(strlen($post['post_text'])>187){ ?>
                                            <div class="para-section">
                                                <div class="para">
                                                    <p><?= $post['post_text'] ?></p>
                                                </div>
                                                <a href="javascript:void(0)" class="readlink">Read More</a>
                                            </div>
                                        <?php }else{ ?>                                     
                                            <p><?= $post['post_text'] ?></p>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if($post['trav_item']== '1' && $post['is_ad']== '1'){
                                    $actbtn = $post['adbtn'];
                                    $link = $post['adurl'];
                                    if(!empty($link))
                                    {
                                        if(substr( $link, 0, 4 ) === "http")
                                        {
                                            $link = $link;
                                        }
                                        else
                                        {
                                            $link = 'https://'.$link;
                                        }
                                    }
                                    else
                                    {
                                        $link = 'Not added';
                                    }
                                ?>
                                <div class="post-paidad">
                                    <div class="travad-sponsor">Sponsered by <a href="<?=$link?>" target="blank"><?=$link?></a></div>
                                    <a class="travad-shop btn btn-primary btn-md">
                                        <?=$actbtn?>
                                    </a>
                                   </div>
                                <?php } ?>
                                
                                <?php if($post['post_type'] == 'link'){ ?>
                                <div class="pvideo-holder">
                                    <?php if($post['image'] != 'No Image'){ ?>
                                    <div class="img-holder"><img src="<?= $post['image'] ?>"/></div>
                                    <div class="desc-holder">
                                    <?php } ?>
                                        <h4><a href="<?= $post['post_text']?>" target="_blank"><?= $post['link_title'] ?></a></h4>
                                        <p><?= $post['link_description'] ?></p>
                                    <?php if($post['image'] != 'No Image'){ ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <a class="overlay-link postdetail-link popup-modal" href="javascript:void(0)" data-postid="<?php echo $post['_id'];?>">&nbsp;</a>
                            </div>
                            <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                                $post['post_type'] = 'text and image';
                            }?>
                            <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                $cnt = 1;
                                $eximgs = explode(',',$post['image'],-1);
                                if(isset($post['trav_item']) && $post['trav_item']== '1')
                                {
                                    if($post['image'] == null)
                                    {
                                        $eximgs[0] = '/uploads/travitem-default.png';
                                    }
                                    $eximgss[] = $eximgs[0];
                                    $eximgs = $eximgss;                                     
                                }
                                $totalimgs = count($eximgs);
                                $imgcountcls="";
                                if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                if($totalimgs > '5'){$imgcountcls = 'more-img';}
                            ?>
                            <div class="post-img-holder two">
                                <div class="lgt-gallery lgt-gallery-photo III post-img <?= $imgcountcls?> gallery swipe-gallery dis-none">
                                    <?php
                                    foreach ($eximgs as $eximg) {
                                        if (file_exists('../web'.$eximg)) {
                                            $picsize = '';
                                            $val = getimagesize('../web'.$eximg);
                                            $picsize .= $val[0] .'x'. $val[1] .', ';
                                            $iname = $this->getimagename($eximg);
                                            $inameclass = $this->getimagefilename($eximg);
                                            $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                            if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                            if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                           <?php /*  <div class="pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?>">*/ ?>
                                                <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                                    <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                                    <?php if($cnt == 5 && $totalimgs > 5){?>
                                                        <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                                    <?php } ?>
                                                </a>
                                            <?php 
                                        } 
                                        $cnt++; 
                                    } ?>
                                </div>
                                <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                    if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                        <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                <?php } else { 
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $image = '../web/uploads/gallery/'.$iname;
                                        $JIDSsdsa = '';           
                                        $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                        if(!empty($pinit)) {
                                            $JIDSsdsa = 'active';                    
                                        }     
                                        ?>
                                        <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                    <?php 
                                    } 
                                } ?>
                            </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'image' && $post['is_coverpic'] == '1' && file_exists('uploads/cover/'.$post['image'])) { ?>
                                <div class="post-img-holder three">
                                    <div class="post-img one-img gallery swipe-gallery">
                                    <?php
                                    if(file_exists('uploads/cover/'.$post['image'])) {
                                        $picsize = '';
                                        $val = getimagesize('uploads/cover/'.$post['image']);
                                        $picsize .= $val[0] .'x'. $val[1] .', ';
                                        $iname = $post['image'];
                                        $inameclass = $this->getnameonly($post['image']);

                                        $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                        if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                
                                        if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                            <?php /* <div class="pimg-holder <?= $imgclass?>-box"> */ ?>
                                              <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                                </a>
                                            <?php /* </div> */ 
                                    } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'profilepic' && file_exists('profile/'.$post['image'])) { ?>
                                <div class="post-img-holder four">
                                    <div class="post-img gallery swipe-gallery">
                                    <?php
                                    if(file_exists('uploads/cover/'.$post['image'])) {
                                        $picsize = '';
                                        $val = getimagesize('profile/'.$post['image']);
                                        $picsize .= $val[0] .'x'. $val[1] .', ';
                                        $iname = $post['image'];
                                        $inameclass = $this->getnameonly($post['image']);
                                        $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                        if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                
                                        if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                            </a>
                                    <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            
                        </div>
                        <?php } else { ?>
                        <div class="post-content on5 <?php if($post['image'] != null) { ?>hasimg<?php } ?>">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                        <?php if($post['post_title'] != null) { ?>
                                        <div class="post-title"><?=$post['post_title']?></div>
                                        <?php } ?>
                                        <div class="rating-stars">
                                        <?php for($i=0;$i<5;$i++)
                                        { ?>
                                            <i class="mdi mdi-star <?php if($i < $post['rating']){ ?>active<?php } ?>"></i>
                                        <?php }
                                        ?>
                                        </div>
                                        <div class="post-desc">
                                                <?php if($post['post_text'] == null) { $post['post_text'] = 'Not added'; } ?>
                                                
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" class="readlink">Read More</a>
                                                    </div>
                                                <?php }else{ ?>                                     
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>                                             
                                        </div>
                                </div>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                                <?php if($post['image'] != null) {
                                    $eximgs = explode(',',$post['image'],-1);
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}
                                ?>
                                <div class="post-img-holder five">
                                    <div class="post-img one-img gallery swipe-gallery">
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                                <?php } } } ?>
                            
                        </div>
                        <?php } } ?>

                        <div class="clear"></div>
                        <div class="post-data">
                            <div class="post-actions">
                                <span class="likeholder"> 
                                    <?php
                                        $like_active = '';
                                        $post_id = $post['_id'];
                                        $like_names = Like::getLikeUserNames((string)$post['_id']);
                                        $like_buddies = Like::getLikeUser((string)$post['_id']);
                                        $newlike_buddies = array();
                                        foreach($like_buddies as $like_buddy) {
                                        $like_active = Like::find()->where(['post_id' => "$post_id",'status' => '1','user_id' => (string) $user_id])->one();
                                        if(!empty($like_active))
                                        {
                                            $like_active = 'active';
                                        }
                                        else
                                        {
                                            $like_active = '';
                                        }
                                            $newlike_buddies[] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
                                        }
                                        $newlike_buddies = implode('<br/>', $newlike_buddies);
                                    ?>
                                    <span class="like-tooltip">
                                         <a href="javascript:void(0)" onclick="doLike('<?=$post['_id']?>');" class="pa-like liveliketooltip waves-effect liketitle_<?=$post['_id']?> <?=$like_active?>"  data-title='<?=$newlike_buddies?>' title="Like">
                                            <i class="zmdi zmdi-thumb-up"></i>     
                                         </a>
                                         <?php if($like_count >0 ) { ?>
                                         <span class="tooltip_content lcount likecount_<?=$post['_id']?>">
                                         <?=$like_count?> 
                                         </span>
                                         <?php } ?>
                                    </span>  
                                  
                                </span>                         
                                <?php if($post['comment_setting'] != 'Disable'){?>
                                <span class="right">
                                    <a href="javascript:void(0)" class="pa-comment waves-effect" data-id="<?php echo $post['_id'];?>" title="Comment">
                                        <i class="zmdi zmdi-comment"></i>
                                    </a>
                                    <?php if((count($comments)) > 0){ ?>
                                    <span class="comment-lcount commentcountdisplay_<?=$post['_id']?>">
                                        <?php echo count($comments);?>
                                    </span>
                                    <?php } ?>
                                </span>
                                <?php } ?>
                                
                                <?php if($post['share_setting'] != 'Disable') { ?>
                                    <?php if(!(isset($post['is_page_review']) && $post['is_page_review'] == '1')) { ?>
                                    <a href="javascript:void(0)" data-sharepostid="<?=$post['_id'];?>" class="sharepostmodalAction pa-share waves-effect" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                                    <?php if(!empty($post['share_by'])) { 
                                        $shares = substr_count( $post['share_by'], ","); ?>
                                            <span class="lcount" id="shareid_<?=$post['_id'];?>"><?=$shares?></span>
                                    <?php } } ?>
                                <?php } ?>
                                
                                <?php 
                                $tag_review_on = Notification::find()->where(['notification_type' => 'tag_connect','user_id' => (string) $user_id,'post_id'=> (string) $post['_id']])->one();
                                if($tag_review_on['review_setting'] == 'Enabled'){ ?>
                                <a id="publishposticon" href="javascript:void(0)" onclick="publish_tag_post('<?php echo $post['_id'];?>')" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                <?php } ?>
                                <?php 
                                $notifica = Notification::find()->where(['notification_type' => 'editpostuser','post_id' => (string)$post['_id']])->one();
                                if($notifica && $status == '10' && $post['is_deleted'] == '2')
                                { ?>
                                    <a id="publishposticon" href="javascript:void(0)" onclick="publishpost('<?php echo $post['_id'];?>')" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                <?php } ?>       
                            </div>
                            <div class="comments-section panel">
                                <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                                <div class="comments-area">
                                <?php } ?>
                                <?php if(count($init_comments)>0){ ?>
                                <div class="post-more">
                                    <?php if(count($comments)>3){ ?>
                                    <a href="javascript:void(0)" class="view-morec-<?php echo $post['_id']; ?>" onclick="showPreviousComments('<?php echo $post['_id'];?>')">View more comments <i class="zmdi zmdi mdi-chevron-right trun90"></i></a>
                                    
                                    <span class="total-comments commment-ctr-<?php echo $post['_id'];?>" id="commment-ctr-<?php echo $post['_id'];?>"><font class="ctrdis_<?=$post['_id']?>"><?php echo count($init_comments);?></font> of <font class="countdisplay_<?=$post['_id']?>"><?php echo count($comments);?></font></span>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <input type="hidden" name="from_ctr" id="from_ctr_<?php echo $post['_id'];?>" value="<?php echo count($init_comments);?>">      
                                <input type="hidden" name="to_ctr" id="to_ctr_<?php echo $post['_id'];?>" value="<?php echo count($comments);?>">

                                <div class="post-comments post_comment_<?=$post['_id']?>">                     
                                    <div class="pcomments sub_post_comment_<?=$post['_id']?>">                                    
                                        <?php
                                        if(count($init_comments)>0) { 
                                            foreach($init_comments as $init_comment) {
                                                $cmnt_id = (string) $init_comment['_id'];
                                                $cmnt_like_active= '';
                                                $cmnt_like_active = Like::find()->where(['comment_id' => $cmnt_id,'status' => '1','user_id' => (string) $user_id])->one();
                                                if(!empty($cmnt_like_active))
                                                {
                                                    $cmnt_like_active = 'active';
                                                }
                                                else
                                                {
                                                    $cmnt_like_active = '';
                                                }
                                                $comment_time = Yii::$app->EphocTime->comment_time(time(),$init_comment['updated_date']);$commentcount =Comment::getCommentReply($init_comment['_id']);
                                                $hidecomments = new HideComment();
                                                $hidecomments = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                $hide_comments_ids = explode(',',$hidecomments['comment_ids']);
                                                if(!(in_array($init_comment['_id'],$hide_comments_ids)))
                                                {
                                                    if(($userid == $post['post_user_id']) || ($userid == $init_comment['user']['_id']))
                                                    {
                                                        $afun_post = 'deleteComment';
                                                        $atool_post = 'Delete';
                                                    }
                                                    else
                                                    {
                                                        $afun_post = 'hideComment';
                                                        $atool_post = 'Hide';
                                                    }
                                        ?>
                                        <div class="pcomment-holder <?php if(count($commentcount) > 0){ ?>has-comments<?php } ?>" id="comment_<?php echo $init_comment['_id']?>">
                                            <div class="pcomment main-comment">
                                                <div class="img-holder">
                                                    <div id="commentptip-4" class="profiletipholder">
                                                        <span class="profile-tooltip">
                                                            <?php $init_comment_img = $this->getimage($init_comment['user']['_id'],'thumb'); ?>
                                                            <img class="circle" src="<?= $init_comment_img?>"/>
                                                        </span>
                                                    </div>
                                                </div> 
                                                <div class="desc-holder">
                                                    <div class="normal-mode">
                                                        <div class="desc">
                                                            <a href="<?php $id = $init_comment['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>" class="userlink"><?php echo ucfirst($init_comment['user']['fname']).' '.ucfirst($init_comment['user']['lname']);?></a>
                                                            <?php if(strlen($init_comment['comment'])>200){ ?>
                                                                <p class="shorten" data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?>
                                                                <a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                </p>
                                                            <?php }else{ ?>                                                     
                                                                <p data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?></p>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="comment-stuff">
                                                            <div class="more-opt">
                                                               <span id="likeholder-7" class="likeholder">
                                                                <?php

                                                                     
                                                                    $like_count = Like::find()->where(['comment_id' => (string) $init_comment['_id']  ,'status' => '1'])->all();

                                                                    $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $init_comment['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                                    $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                                    $usrbox = array();
                                                                    foreach ($comlikeuseinfo as $key => $single) {
                                                                        $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                                         $usrbox[(string)$single['_id']] = $fullnm; 
                                                                    }
                                                                    
                                                                    $newlike_buddies = implode("<br/>", $usrbox);
                                                                ?>

                                                                        <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$init_comment['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $init_comment['_id']?>')" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                         </a>
                                                                </span> 
                                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply"></i></a>
                                                                
                                                                <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                    <!-- Dropdown Trigger -->
                                                                      <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='subset_<?=$init_comment['_id']?>'>
                                                                        <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                                      </a>
                                                                                                                                        
                                                                      <!-- Dropdown Structure -->
                                                                      <ul id='subset_<?=$init_comment['_id']?>' class='dropdown-content custom_dropdown'>
                                                                    
                                                                    <?php if($status == '10') { ?> 
                                                                        <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')">Flag</a></li>
                                                                    <?php } else { ?>                                                                    
                                                                        <?php if(($userid != $post['post_user_id']) && ($userid != $init_comment['user']['_id'])) { ?>
                                                                            <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')"><i class="mdi mdi-close mdi-20px "></i><?=$atool_post?></a></li>
                                                                        <?php } else { ?>
                                                                            <?php if($userid == $init_comment['user']['_id']){ ?>
                                                                            <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                            </li>
                                                                            <?php } ?>
                                                                            <li>
                                                                              <a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>','<?=$post['_id']?>')">Delete</a>
                                                                            </li>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                    
                                                                      </ul>                                                                 
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="less-opt"><div class="timestamp"><?php echo $comment_time?></div></div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-mode">
                                                        <div class="desc">
                                                            <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                <textarea class="editcomment-tt materialize-textarea" data-id="<?php echo $init_comment['_id'];?>" id="edit_comment_<?php echo $init_comment['_id'];?>"><?php echo $init_comment['comment']?></textarea>
                                                            </div>
                                                            <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px    "></i></a>
                                                        </div>                                                                          
                                                    </div>
                                                </div>                              
                                            </div>  
                                            <div class="clear"></div>
                                            
                                            <div class="comment-reply-holder reply_comments_<?php echo $init_comment['_id'];?>">
                                                <?php $comment_replies =Comment::getCommentReply($init_comment['_id']);
                                                    if(count($comment_replies)>0) {
                                                    $lastcomment = Comment::find()->where(['parent_comment_id' => (string)$init_comment['_id']])->orderBy(['updated_date'=>SORT_DESC])->one();
                                                    $last_comment_time = Yii::$app->EphocTime->comment_time(time(),$lastcomment['updated_date']);
                                                ?>
                                                <div class="comments-reply-summery">
                                                    <a href="javascript:void(0)" onclick="openReplies(this)">
                                                        <i class="mdi mdi-share"></i>
                                                        <?php echo count($comment_replies); 
                                                        if(count($comment_replies)>1) { echo " Replies"; }
                                                        else { echo " Reply"; } ?>
                                                    </a>
                                                    <i class="mdi mdi-bullseye dot-i"></i>
                                                    <?php echo $last_comment_time;?>
                                                </div>
                                                <?php }
                                                if(!empty($comment_replies))
                                                {
                                                    foreach($comment_replies AS $comment_reply) { 
                                                    $hidecomment = new HideComment();
                                                    $hidecomment = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                    $hide_comment_ids = explode(',',$hidecomment['comment_ids']); 
                                                    if(!(in_array($comment_reply['_id'],$hide_comment_ids)))
                                                    {
                                                        if(($userid == $post['post_user_id']) || ($userid == $comment_reply['user']['_id']))
                                                        {
                                                            $bfun_post = 'deleteComment';
                                                            $btool_post = 'Delete';
                                                        }
                                                        else
                                                        {
                                                            $bfun_post = 'hideComment';
                                                            $btool_post = 'Hide';
                                                        }
                                                    $comment_time = Yii::$app->EphocTime->comment_time(time(),$comment_reply['updated_date']);

                                                    $like_count = Like::find()->where(['comment_id' => (string) $comment_reply['_id']  ,'status' => '1'])->all();

                                                    $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $comment_reply['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                    $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                    $usrbox = array();
                                                    foreach ($comlikeuseinfo as $key => $single) {
                                                    $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                    $usrbox[(string)$single['_id']] = $fullnm; 
                                                    }

                                                    $newlike_buddies = implode("<br/>", $usrbox)

                                            ?>
                                            <div class="comments-reply-details">
                                                <div class="pcomment comment-reply" id="comment_<?php echo $comment_reply['_id']?>">
                                                    <div class="img-holder">
                                                        <div class="profiletipholder" id="commentptip-6">
                                                            <span class="profile-tooltip tooltipstered">
                                                                <img class="circle" src="<?php echo $this->getimage($comment_reply['user']['_id'],'thumb'); ?>">
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="desc-holder">
                                                        <div class="normal-mode">
                                                            <div class="desc">
                                                                <a class="userlink" href="<?php $id = $comment_reply['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>"><?php echo ucfirst($comment_reply['user']['fname']).' '.ucfirst($comment_reply['user']['lname']);?></a>
                                                                
                                                                <?php if(strlen($comment_reply['comment'])>200){ ?>
                                                                    <p class="shorten" data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?><a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                    </p>
                                                                <?php }else{ ?>                                                         
                                                                    <p data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?></p>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="comment-stuff">
                                                                <div class="more-opt">
                                                                        <span class="likeholder" id="likeholder-6">
                                                                        <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$comment_reply['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $comment_reply['_id']?>')" title="Like">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                            <!-- Dropdown Trigger -->
                                                                              <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='subset_<?=$comment_reply['_id']?>'>
                                                                                <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                                              </a>

                                                                              <!-- Dropdown Structure -->
                                                                              <ul id='subset_<?=$comment_reply['_id']?>' class='dropdown-content custom_dropdown'>
                                                                                <?php if(($userid != $post['post_user_id']) && ($userid != $comment_reply['user']['_id'])){?>
                                                                                    <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$btool_post?></a></li>
                                                                                    <?php } else { ?>
                                                                                        <?php if($userid == $comment_reply['user']['_id']){ ?>
                                                                                        <li>
                                                                                          <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                                        </li>
                                                                                        <?php } ?>
                                                                                        <li>
                                                                                          <a href="javascript:void(0)" class="delete-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')">Delete</a>
                                                                                        </li>
                                                                                    <?php } ?>
                                                                              </ul>
                                                                        </div>
                                                                </div>
                                                                <div class="less-opt"><div class="timestamp"><?= $comment_time;?></div></div>
                                                            </div>  
                                                        </div>
                                                        <div class="edit-mode">
                                                            <div class="desc">
                                                                <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                                    <textarea class="editcomment-tt materialize-textarea" data-id="<?php echo $comment_reply['_id'];?>" id="edit_comment_<?php echo $comment_reply['_id'];?>"><?php echo $comment_reply['comment']?></textarea>
                                                                </div>
                                                                <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px    "></i></a>
                                                            </div>                                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } } } ?>
                                            </div>
                                            
                                            <div class="comment-reply-holder comment-addreply">                                 
                                                <div class="addnew-comment comment-reply">                          
                                                    <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $this->getimage($userid,'thumb');?>"/></a></div>
                                                    <div class="desc-holder">                                   
                                                        <div class="sliding-middle-out anim-area tt-holder">
                                                            <textarea name="reply_txt" placeholder="Write a reply" data-postid="<?php echo $post['_id'];?>" data-commentid="<?php echo $init_comment['_id']?>" id="reply_txt_<?php echo $init_comment['_id'];?>" class="reply_class materialize-textarea"></textarea>
                                                        </div>  
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <?php  } } } ?> 
                                    </div>
                                    <?php if($post['comment_setting'] != 'Disable'){
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        $comment_placeholder = "Ask question or send query";
                                    }
                                    else{
                                        $comment_placeholder = "Write a comment";
                                    }   
                                    ?>
                                    <div class="addnew-comment valign-wrapper">
                                    <?php $comment_image = $this->getimage($userid,'thumb'); ?>
                                    <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $comment_image?>"/></a></div>
                                        <div class="desc-holder">                                   
                                            <div class="sliding-middle-out anim-area tt-holder">
                                                <textarea data-postid="<?php echo $post['_id'];?>" id="comment_txt_<?php echo $post['_id'];?>" placeholder="<?= $comment_placeholder;?>" class="comment_class  materialize-textarea"></textarea>
                                            </div>
                                        </div>
                                        <?php if($existing_posts == ''){ ?>
                                            <input type="hidden" id="last_inserted" name="last_inserted" value="<?php echo $post['_id']; ?>"/>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
    }

    public function display_last_tripstory($last_insert_id, $existing_posts = '', $imagefullarray = '', $cls='', $total='', $tempocls='')
    {
        $isAdsClsLabel = '';
        $session = Yii::$app->session;
        $userid = $user_id = (string)$session->get('user_id');
        $status = $session->get('status');
        $date = time();
        $profile_tip_counter = 1;
        
        $issearch = 'yes';
        $post = PostForm::find()->where([(string)'_id' => $last_insert_id])->one();
        $tableLabel = 'PostForm';

        if(!empty($post)) {
            $issearch = 'no';   
        }

        $Auth = '';
        if(isset($userid) && $userid != '') 
        {
            $authstatus = UserForm::isUserExistByUid($userid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
            {
                $Auth = $authstatus;
            }
        }   
        else    
        {
            $Auth = 'checkuserauthclassg';
        }

        $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => (string)$post['_id']])->one();
        $savestatuscalue = $savestatus['is_saved'];
        if($savestatuscalue == '1'){$savevar = 'Unsave';}else{$savevar = 'Save';}

        $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
        $like_count = Like::getLikeCount((string)$post['_id']);
        $comments = Comment::getAllPostLike($post['_id']);
        $init_comments = Comment::getFirstThreePostComments($post['_id']);
        $post_privacy = $post['post_privacy'];
        if($post_privacy == 'Private') {$post_class = 'lock';}
        else if($post_privacy == 'Connections') {$post_class = 'account';}
        else if($post_privacy == 'Custom') {$post_class = 'settings';}
        else {$post_class = 'earth';}

        // START add tag content in post
        $posttag = '';
        if(isset($post['post_tags']) && !empty($post['post_tags']))
        {
            $posttag = explode(",", $post['post_tags']);
        }

        $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');

        $nkTag = array();
        $nvTag = array();

        $i=1;
        foreach ($taginfomatiom as $key => $value)
        {
            $nkTag[] = (string)$value; 
            $nvTag[] = $key;
            if($i != 1) {
                $content[] = $key;
            }
            $i++;
        }

        if(isset($content) && !empty($content))
        {
            $content = implode("<br/>", $content); 
        }

        $tagstr = '';
        if(!empty($taginfomatiom))
        {
            if(count($taginfomatiom) > 1) {
                $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
            } else {
                $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
            }
        }
        if(isset($post['is_page_review']) && !empty($post['is_page_review']))
        {
            $page_details = Page::Pagedetails($post['page_id']);
            $review = " reviewed <a href=".Url::to(['page/index', 'id' => $post['page_id']]) ." class='sub-link'>" . $page_details['page_name'] . "</a>"; }
        else { $review = ''; }
        // END add tag content in post
        $profile_tip_counter++;         
        if(isset($post['currentlocation']) && !empty($post['currentlocation']))
        {
            $current_location = $this->getshortcityname($post['currentlocation']);
            $default = 'at';
            if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='reviews'){$default = 'reviewed';}
            if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='tip'){$default = 'tip for';}
            if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='ask'){$default = 'has a question about';}
            $locationtags = $tagstr . ' '.$default.' <a class="sub-link" href="javascript:void(0)">'.$current_location.'</a>';
        }
        else{ $locationtags = $tagstr . ''; }
        if($post['is_timeline'] == '1')
        {
            $getrateid = PostForm::find()->where(['_id' => $post['parent_post_id']])->one();
            $getratedid = $getrateid['rating'];
            if($getratedid == null){$getratedid = '6';}
        }
        else
        {
            $getratedid = '6';
        }
        if((isset($post['is_ad']) && !empty($post['is_ad']) && $post['is_ad'] == '1'))
        {
            $isAdsClsLabel = 'Ads89HG';
            $is_ad = true;
            $adclass = 'travad-box ';
            if($post['adobj'] == 'pagelikes'){$adclass .= 'page-ad';}
            else if($post['adobj'] == 'brandawareness'){$adclass .= 'brand-ad';}
            else if($post['adobj'] == 'websiteleads'){$adclass .= 'weblink-ad';}
            else if($post['adobj'] == 'websiteconversion'){$adclass .= 'actionlink-ad';}
            else if($post['adobj'] == 'pageendorse'){$adclass .= 'page-ad';}
            else if($post['adobj'] == 'eventpromo'){$adclass .= 'event-ad';}
            else{$adclass .= '';}
        } else { 
            $is_ad = false; $adclass = '';
        }
        ?>
        <div class="<?=$tempocls .' '.$cls?> post-holder <?php if($existing_posts != 'from_save'){ ?>bshadow<?php } ?> <?php if((isset($post['is_page_review']) && !empty($post['is_page_review']) || ($getratedid != '6'))){ ?>reviewpost-holder<?php } ?> <?php if((isset($post['trav_item']) && !empty($post['trav_item']))){ ?>travelstore-ad<?php } ?> <?php if((isset($post['is_ad']) && !empty($post['is_ad']))){ echo $adclass; } ?> post_user_id_<?php echo $post['post_user_id'];?>" id="hide_<?php echo $post['_id'];?>">
            <?php
            $getuserinfo = LoginForm::find()->where(['_id' => $post['post_user_id']])->one();
            $personalinfo = Personalinfo::find()->where(['user_id' => $post['post_user_id']])->one();
            if($post['user']['status']=='44')
            {
                $dp = $this->getpageimage($post['user']['_id']);
                $link = Url::to(['page/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'page';
            }
            else
            {
                $dp = $this->getimage($getuserinfo['_id'],'photo');
                $link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'userwall';
            }
            if(isset($getuserinfo['cover_photo']) && !empty($getuserinfo['cover_photo']))
            {
                $cover = $getuserinfo['cover_photo'];
                if(strstr($cover,'cover'))
                {
                    $cvr = substr_replace($getuserinfo['cover_photo'], '-thumb.jpg', -4);
                    $cover = '../uploads/cover/thumbs/'.$cvr;
                }
                else
                {
                    $cover = '../uploads/cover/thumbs/thumb_'.$getuserinfo['cover_photo'];
                }
            }
            else
            {
                $cover = 'cover.jpg';
            }
            $dpforpopup = $this->getimage($post['user']['_id'],'thumb');
            $mutualcount = Connect::mutualconnectcount($getuserinfo['_id']);

            $po_id = $post['post_user_id'];
            $isconnect = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '1'])->one();
            $isconnectrequestsent = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '0'])->one();
                $uniqid = uniqid();
            ?>
            <div class="post-topbar ee">
                <div class="post-userinfo">
                    <div class="img-holder">
                        <div class="profiletipholder">
                            <span id="profiletip-<?= $post['_id']?>" class="profile-tooltip">
                                <img class="circle width-100" src="<?= $dp;?>">
                            </span>
                            <span class="profiletooltip_content profiletooltip_content_last">
                                <div class="profile-tip slidingpan-holder dis-none" id="dispatchcontentprofiletip-<?= $post['_id']?>">
                                   
                                    <div class="profile-tip-avatar">
                                        <img alt="user-photo" class="img-responsive"  src="<?= $dp?>">
                                        <div class="sliding-pan location-span">
                                            <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                        </div>
                                    </div>
                                    <div class="profile-tip-name">
                                        <a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a>
                                    </div>
                                    <?php if($post['user']['status']!='44'){ ?>
                                    <div class="profile-tip-info">
                                        <div class="profiletip-icon">
                                                <?php if($userid == $po_id){ ?>
                                                <a href="javascript:void(0)">&nbsp;</a>
                                                <?php } else if($isconnect) { ?>
                                                <a href="javascript:void(0)"><?=$mutualcount?> Mutual connections</a>
                                                <?php } else if($isconnectrequestsent) { ?>
                                                <i title="Connect request sent" class="mdi mdi-account-minus"></i>
                                                <?php } else { ?>
                                                <a href="javascript:void(0)" title="Add Connect" class="title_<?php echo $po_id;?>">
                                                <i class="mdi mdi-account-plus people_<?php echo $po_id;?>" onclick="addConnect('<?php echo $po_id;?>')"></i>
                                                <i title="Connect request sent" class="mdi mdi-account-minus dis-none sendmsg_<?php echo $po_id;?>" ></i>
                                                <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
                                                </a>
                                                <?php } ?>
                                        </div>
                                        <div class="profiletip-icon">
                                                <a href="<?php echo $link;?>" title="View Profile"><i class="mdi mdi-eye"></i></a>
                                        </div>                  
                                    </div>
                                    <?php } ?>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="desc-holder">
                        <span>By </span><a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a><span class="withtext"><?= $tagstr;?></span>
                        <span class="timestamp"><?php echo $time; ?>
                            <a class="dropdown-button" href="javascript:void(0);" data-activates="<?=$uniqid?>">
                                <i class="mdi mdi-<?=$post_class?> mdi-13px"></i>
                                <?php
                                $pageownerid = $post['post_user_id'];
                                if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                <i class="mdi mdi-menu-down mdi-22px"></i>
                                <?php } ?>
                            </a>

                            <ul id="<?=$uniqid?>" class="dropdown-content directprivacydropdown">
                                <li class="post-private" data-cls="post-private"> <a href="javascript:void(0)"> Private </a> </li>
                                <li class="post-connections" data-cls="post-connections"> <a href="javascript:void(0)"> Connections </a> </li>
                                <li class="post-custom customli_modal_post" data-cls="post-custom"> <a href="javascript:void(0)"> Custom </a> </li>
                                <li class="post-public" data-cls="post-public"> <a href="javascript:void(0)"> Public </a> </li>
                           </ul>
                        </span>
                    </div>
                </div>
                <div class="settings-icon">
                    <div class="dropdown dropdown-custom dropdown-xxsmall">
                    <!-- Dropdown Trigger -->
                    <?php if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                        <a class='dropdown-button more_btn <?=$Auth?> directcheckuserauthclass' href='javascript:void(0)'>
                          <i class="zmdi zmdi-more"></i>
                        </a>
                    <?php } else { ?>
                        <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='setting_btn_<?= $post['_id']?>'>
                          <i class="zmdi zmdi-more"></i>
                        </a>
                        <!-- Dropdown Structure -->
                        <ul id='setting_btn_<?= $post['_id']?>' class='dropdown-content custom_dropdown'>
                            
                            <?php if($status == '10') { if($post['is_deleted'] != '2') { ?> 
                            <li><a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Flag experience </a></li>
                            <?php } } else {?>  
                                <?php if($userid != $post['post_user_id']) { ?> 
                                <li>
                                    <a href="javascript:void(0)" class="hidepost" onclick="hidePost('<?php echo $post['_id']?>')">Hide experience</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="savepost-link" id="save_post_<?php  echo $post['_id'];?>" data-pid="<?php  echo $post['_id'];?>" onclick="savePost('<?php echo $post['_id']?>','<?php echo $post['post_type']?>','<?=$savevar?>','normal')"><?=$savevar?> experience</a>
                                </li>
                                <?php } ?>
                                <input type="hidden" class="tbsp_<?=$post['_id']?>" value="<?=$savevar?>">
                                <li>
                                    <a href="javascript:void(0)" onclick="turn_off_notification('<?=$post['_id'] ?>')" class="turnnotpost_<?=$post['_id'] ?>">
                                    <?php
                                    $turnoff = TurnoffNotification::find()->where(['user_id' => "$user_id"])->andwhere(['like','post_ids',$post['_id']])->one();
                                    if($turnoff) { $turnoffvalue = 'Unmute this experience notification';}
                                    else { $turnoffvalue = 'Mute this experience notification';}?>
                                    <?=$turnoffvalue?>
                                    </a> 
                                </li>    
                                <?php if($post['user']['status']=='44' && ($post['is_coverpic']=='1' || $post['is_profilepic']=='1')) { $page_details = Page::Pagedetails($post['post_user_id']); $pageownerid = $page_details['created_by']; }
                                else { $pageownerid = $post['post_user_id']; } ?>
                                <?php if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                    <li>
                                        <a href="javascript:void(0)" class="deletepost-link" onclick="deletePost('<?php echo $post['post_user_id']?>','<?php echo $post['_id']?>')">Delete experience</a>
                                    </li>
                                    <?php if(($post['is_coverpic']!='1' && $post['is_profilepic']!='1' && $post['shared_from']!='shareentity')){ ?>
                                    <li>
                                        <a href="javascript:void(0)" data-editpostid="<?php echo $post['_id'];?>" class="composeeditpostAction editpost-link">Edit experience</a>
                                    </li>
                                    <?php } ?>
                                <?php } ?>
                                <?php if(($userid != $post['post_user_id']) && ($user_id != $post['shared_by']) && ($user_id != $pageownerid)){
                                    $muteuser = UnfollowConnect::find()->where(['user_id' => "$user_id"])->andwhere(['like','unfollow_ids',$post['post_user_id']])->one();
                                    if($muteuser) { $mutevalue = 'Unmute this member posts';}
                                    else { $mutevalue = 'Mute this member posts';}
                                ?>
                                    <li>
                                        <a href="javascript:void(0)" onclick="unfollowConnect('<?php echo $post['post_user_id']?>')"><?=$mutevalue?></a>
                                    </li>
                                    <?php 
                                    $userblock = BlockConnect::find()->select(['block_ids'])->where(['user_id' => "$user_id"])->one();
                                    if ($userblock)
                                    {
                                        if (strstr($userblock['block_ids'], $post['post_user_id']))
                                        {
                                                $getblock = 'Unblock';
                                        }
                                        else
                                        {
                                                $getblock = 'Block';
                                        }
                                    }
                                    else
                                    {
                                        $getblock = 'Block';
                                    }
                                    ?>
                                    <li>
                                        <a href="javascript:void(0)" onclick="blockConnect('<?php echo $post['post_user_id']?>')" class="getblock_<?php echo $post['post_user_id']?>"><?=$getblock?></a>
                                    </li>
                                    <li>
                                        <a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Report experience</a>
                                    </li>
                                <?php } ?>
                            <?php } ?>                                
                        </ul>
                    <?php } ?>
                </div>
               </div>
            </div>
            <?php
            if($post['is_timeline']=='1' && isset($post['parent_post_id']) && !empty($post['parent_post_id']))
            {
            ?>
                <div class="post-content on2">
                    <div class="pdetail-holder">
                        <div class="post-details">
                    <?php if($post['post_text'] != ''){ ?>
                            <div class="post-desc">
                                <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                
                                    <?php if(strlen($post['post_text'])>187){ ?>
                                        <div class="para-section">
                                            <div class="para">
                                                <p><?= $post['post_text'] ?></p>
                                            </div>
                                            <a href="javascript:void(0)" class="readlink">Read More</a>
                                        </div>
                                    <?php }else{ ?>                                     
                                        <p><?= $post['post_text'] ?></p>
                                    <?php } ?>
                                    
                                <?php } ?>
                            </div>
                      <?php } ?>
                        </div>
                        <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                    </div>
                </div>
                <?php
                if(isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                {
                    if(strstr($post['parent_post_id'],'trip_'))
                    {
                        $this->view_trip((string)$post['parent_post_id']);
                    }
                }
            } else if($post['is_trip'] == '1'){
            ?>
                <div class="post-content on3 tripexperince-post">
                    <div class="pdetail-holder">
                        <div class="post-details">
                            <div class="post-title">
                                <?= $post['post_title'];?>
                            </div>
                        </div>
                        <div class="trip-summery">
                            <div class="location-info raani">
                                <h5><i class="zmdi zmdi-pin"></i>
                                <?php  
                                $currentlocation = trim($post['currentlocation']);

                                if($currentlocation != '') {
                                    $currentlocation = explode(",", $currentlocation);
                                    if(count($currentlocation) >1) {
                                        $first = reset($currentlocation);
                                        $last = end($currentlocation);
                                        $currentlocation = $first.', '.$last;
                                    } else {
                                        $currentlocation = implode(", ", $currentlocation);
                                    }
                                }

                                echo $currentlocation;
                                ?></h5>
                                <i class="mdi mdi-menu-right"></i>
                                <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                            </div>
                            <!--<div class="views-info"><i class="mdi mdi-eye"></i>45 Views</div>-->
                        </div>
                        <div class="map-holder dis-none">
                            <iframe width="600" height="450" frameborder="0" src="https://maps.google.it/maps?q=<?= $post['currentlocation'];?>&output=embed"></iframe>
                        </div>
                    </div>
                    <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                        $cnt = 1;
                        $eximgs = explode(',',$post['image'],-1);
                        if(isset($post['trav_item']) && $post['trav_item']== '1')
                        {
                            if($post['image'] == null)
                            {
                                $eximgs[0] = '/uploads/travitem-default.png';
                            }
                            $eximgss[] = $eximgs[0];
                            $eximgs = $eximgss;                                     
                        }
                        $totalimgs = count($eximgs);
                        $imgcountcls="";
                        if($totalimgs == '1'){$imgcountcls = 'one-img';}
                        if($totalimgs == '2'){$imgcountcls = 'two-img';}
                        if($totalimgs == '3'){$imgcountcls = 'three-img';}
                        if($totalimgs == '4'){$imgcountcls = 'four-img';}
                        if($totalimgs == '5'){$imgcountcls = 'five-img';}
                        if($totalimgs > '5'){$imgcountcls = 'more-img';}
                    ?>
                    <div class="post-img-holder one II">
                        <div class="lgt-gallery lgt-gallery-photo II post-img <?= $imgcountcls?> gallery swipe-gallery dis-none">
                            <?php
                            foreach ($eximgs as $eximg) {
                                if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                            <?php if($cnt == 5 && $totalimgs > 5){?>
                                                <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                            <?php } ?>
                                        </a>
                                    <?php 
                                } 
                                $cnt++; 
                            } ?>
                        </div>
                        <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                            if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                            <?php } else { 
                                $iname = $this->getimagename($eximg);
                                $inameclass = $this->getimagefilename($eximg);
                                $image = '../web/uploads/gallery/'.$iname;
                                $JIDSsdsa = '';           
                                $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                if(!empty($pinit)) {
                                    $JIDSsdsa = 'active';                    
                                }
                                ?>
                                <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                            <?php 
                            } 
                        } 
                        ?>
                    </div>
                    <?php } ?>
                    <div class="pdetail-holder">
                        <div class="post-details">
                        <?php 
                        if($post['post_text'] != ''){ ?>
                                <div class="post-desc">
                                    <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                        <?php if(strlen($post['post_text'])>187){ ?>
                                            <div class="para-section">
                                                <div class="para">
                                                    <p><?= $post['post_text'] ?></p>
                                                </div>
                                                <a href="javascript:void(0)" class="readlink">Read More</a>
                                            </div>
                                        <?php } else { ?>                                       
                                            <p><?= $post['post_text'] ?></p>
                                        <?php } ?>
                                    
                                    <?php } ?>
                                </div>
                          <?php } ?>
                        </div>  
                        <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                        
                    </div>
                </div>
            <?php   
            } ?>

            <div class="clear"></div>
            <div class="post-data">
                <div class="post-actions">
                    <span class="likeholder"> 
                        <?php
                            $like_active = '';
                            $post_id = $post['_id'];
                            $like_names = Like::getLikeUserNames((string)$post['_id']);
                            $like_buddies = Like::getLikeUser((string)$post['_id']);
                            $newlike_buddies = array();
                            foreach($like_buddies as $like_buddy) {
                            $like_active = Like::find()->where(['post_id' => "$post_id",'status' => '1','user_id' => (string) $user_id])->one();
                            if(!empty($like_active))
                            {
                                $like_active = 'active';
                            }
                            else
                            {
                                $like_active = '';
                            }
                                $newlike_buddies[] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
                            }
                            $newlike_buddies = implode('<br/>', $newlike_buddies);
                        ?>
                        <span class="like-tooltip">
                             <a href="javascript:void(0)" onclick="doLike('<?=$post['_id']?>');" class="pa-like liveliketooltip waves-effect liketitle_<?=$post['_id']?> <?=$like_active?>"  data-title='<?=$newlike_buddies?>' title="Like">
                                <i class="zmdi zmdi-thumb-up"></i>     
                            </a>
                            <?php if($like_count >0 ) { ?>
                             <span class="tooltip_content lcount likecount_<?=$post['_id']?>">
                             <?=$like_count?> 
                            </span>
                            <?php } ?>
                        </span>  
                      
                    </span>                         
                    <?php if($post['comment_setting'] != 'Disable'){?>
                    <span class="right">
                        <a href="javascript:void(0)" class="pa-comment waves-effect" data-id="<?php echo $post['_id'];?>" title="Comment">
                            <i class="zmdi zmdi-comment"></i>
                        </a>
                        <?php if((count($comments)) > 0){ ?>
                        <span class="comment-lcount commentcountdisplay_<?=$post['_id']?>">
                            <?php echo count($comments);?>
                        </span>
                        <?php } ?>
                    </span>
                    <?php } ?>
                    
                    <?php if($post['share_setting'] != 'Disable') { ?>
                        <?php if(!(isset($post['is_page_review']) && $post['is_page_review'] == '1')) { ?>
                        <a href="javascript:void(0)" data-sharepostid="<?=$post['_id'];?>" class="sharepostmodalAction pa-share waves-effect" title="Share"><i class="zmdi zmdi-mail-reply zmdi-hc-flip-horizontal"></i></a>
                        <?php if(!empty($post['share_by'])) { 
                            $shares = substr_count( $post['share_by'], ","); ?>
                                <span class="lcount" id="shareid_<?=$post['_id'];?>"><?=$shares?></span>
                        <?php } } ?>
                    <?php } ?>
                    
                    <?php 
                    $tag_review_on = Notification::find()->where(['notification_type' => 'tag_connect','user_id' => (string) $user_id,'post_id'=> (string) $post['_id']])->one();
                    if($tag_review_on['review_setting'] == 'Enabled'){ ?>
                    <a id="publishposticon" href="javascript:void(0)" onclick="publish_tag_post('<?php echo $post['_id'];?>')" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                    <?php } ?>
                    <?php 
                    $notifica = Notification::find()->where(['notification_type' => 'editpostuser','post_id' => (string)$post['_id']])->one();
                    if($notifica && $status == '10' && $post['is_deleted'] == '2')
                    { ?>
                        <a id="publishposticon" href="javascript:void(0)" onclick="publishpost('<?php echo $post['_id'];?>')" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                    <?php } ?>       
                </div>
                <div class="comments-section panel">
                    <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                    <div class="comments-area">
                    <?php } ?>
                    <?php if(count($init_comments)>0){ ?>
                    <div class="post-more">
                        <?php if(count($comments)>3){ ?>
                        <a href="javascript:void(0)" class="view-morec-<?php echo $post['_id']; ?>" onclick="showPreviousComments('<?php echo $post['_id'];?>')">View more comments <i class="zmdi zmdi mdi-chevron-right trun90"></i></a>
                        
                        <span class="total-comments commment-ctr-<?php echo $post['_id'];?>" id="commment-ctr-<?php echo $post['_id'];?>"><font class="ctrdis_<?=$post['_id']?>"><?php echo count($init_comments);?></font> of <font class="countdisplay_<?=$post['_id']?>"><?php echo count($comments);?></font></span>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <input type="hidden" name="from_ctr" id="from_ctr_<?php echo $post['_id'];?>" value="<?php echo count($init_comments);?>">      
                    <input type="hidden" name="to_ctr" id="to_ctr_<?php echo $post['_id'];?>" value="<?php echo count($comments);?>">

                    <div class="post-comments post_comment_<?=$post['_id']?>">                     
                        <div class="pcomments sub_post_comment_<?=$post['_id']?>">                                    
                            <?php
                            if(count($init_comments)>0) { 
                                foreach($init_comments as $init_comment) {
                                    $cmnt_id = (string) $init_comment['_id'];
                                    $cmnt_like_active= '';
                                    $cmnt_like_active = Like::find()->where(['comment_id' => $cmnt_id,'status' => '1','user_id' => (string) $user_id])->one();
                                    if(!empty($cmnt_like_active))
                                    {
                                        $cmnt_like_active = 'active';
                                    }
                                    else
                                    {
                                        $cmnt_like_active = '';
                                    }
                                    $comment_time = Yii::$app->EphocTime->comment_time(time(),$init_comment['updated_date']);$commentcount =Comment::getCommentReply($init_comment['_id']);
                                    $hidecomments = new HideComment();
                                    $hidecomments = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                    $hide_comments_ids = explode(',',$hidecomments['comment_ids']);
                                    if(!(in_array($init_comment['_id'],$hide_comments_ids)))
                                    {
                                        if(($userid == $post['post_user_id']) || ($userid == $init_comment['user']['_id']))
                                        {
                                            $afun_post = 'deleteComment';
                                            $atool_post = 'Delete';
                                        }
                                        else
                                        {
                                            $afun_post = 'hideComment';
                                            $atool_post = 'Hide';
                                        }
                            ?>
                            <div class="pcomment-holder <?php if(count($commentcount) > 0){ ?>has-comments<?php } ?>" id="comment_<?php echo $init_comment['_id']?>">
                                <div class="pcomment main-comment">
                                    <div class="img-holder">
                                        <div id="commentptip-4" class="profiletipholder">
                                            <span class="profile-tooltip">
                                                <?php $init_comment_img = $this->getimage($init_comment['user']['_id'],'thumb'); ?>
                                                <img class="circle" src="<?= $init_comment_img?>"/>
                                            </span>
                                        </div>
                                    </div> 
                                    <div class="desc-holder">
                                        <div class="normal-mode">
                                            <div class="desc">
                                                <a href="<?php $id = $init_comment['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>" class="userlink"><?php echo ucfirst($init_comment['user']['fname']).' '.ucfirst($init_comment['user']['lname']);?></a>
                                                <?php if(strlen($init_comment['comment'])>200){ ?>
                                                    <p class="shorten" data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?>
                                                    <a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                    </p>
                                                <?php }else{ ?>                                                     
                                                    <p data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?></p>
                                                <?php } ?>
                                            </div>
                                            <div class="comment-stuff">
                                                <div class="more-opt">
                                                   <span id="likeholder-7" class="likeholder">
                                                    <?php

                                                         
                                                        $like_count = Like::find()->where(['comment_id' => (string) $init_comment['_id']  ,'status' => '1'])->all();

                                                        $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $init_comment['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                        $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                        $usrbox = array();
                                                        foreach ($comlikeuseinfo as $key => $single) {
                                                            $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                             $usrbox[(string)$single['_id']] = $fullnm; 
                                                        }
                                                        
                                                        $newlike_buddies = implode("<br/>", $usrbox);
                                                    ?>

                                                            <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$init_comment['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $init_comment['_id']?>')" title="Like">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                             </a>
                                                    </span> 
                                                    <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply"></i></a>
                                                    
                                                    <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                        <!-- Dropdown Trigger -->
                                                          <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='subset_<?=$init_comment['_id']?>'>
                                                            <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                          </a>
                                                                                                                            
                                                          <!-- Dropdown Structure -->
                                                          <ul id='subset_<?=$init_comment['_id']?>' class='dropdown-content custom_dropdown'>
                                                        
                                                        <?php if($status == '10') { ?> 
                                                            <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')">Flag</a></li>
                                                        <?php } else { ?>                                                                    
                                                            <?php if(($userid != $post['post_user_id']) && ($userid != $init_comment['user']['_id'])) { ?>
                                                                <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')"><i class="mdi mdi-close mdi-20px "></i><?=$atool_post?></a></li>
                                                            <?php } else { ?>
                                                                <?php if($userid == $init_comment['user']['_id']){ ?>
                                                                <li>
                                                                  <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                </li>
                                                                <?php } ?>
                                                                <li>
                                                                  <a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>','<?=$post['_id']?>')">Delete</a>
                                                                </li>
                                                            <?php } ?>
                                                        <?php } ?>
                                                        
                                                          </ul>                                                                 
                                                        
                                                    </div>
                                                </div>
                                                <div class="less-opt"><div class="timestamp"><?php echo $comment_time?></div></div>
                                            </div>
                                        </div>
                                        <div class="edit-mode">
                                            <div class="desc">
                                                <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                    <textarea class="editcomment-tt materialize-textarea" data-id="<?php echo $init_comment['_id'];?>" id="edit_comment_<?php echo $init_comment['_id'];?>"><?php echo $init_comment['comment']?></textarea>
                                                </div>
                                                <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px    "></i></a>
                                            </div>                                                                          
                                        </div>
                                    </div>                              
                                </div>  
                                <div class="clear"></div>
                                
                                <div class="comment-reply-holder reply_comments_<?php echo $init_comment['_id'];?>">
                                    <?php $comment_replies =Comment::getCommentReply($init_comment['_id']);
                                        if(count($comment_replies)>0) {
                                        $lastcomment = Comment::find()->where(['parent_comment_id' => (string)$init_comment['_id']])->orderBy(['updated_date'=>SORT_DESC])->one();
                                        $last_comment_time = Yii::$app->EphocTime->comment_time(time(),$lastcomment['updated_date']);
                                    ?>
                                    <div class="comments-reply-summery">
                                        <a href="javascript:void(0)" onclick="openReplies(this)">
                                            <i class="mdi mdi-share"></i>
                                            <?php echo count($comment_replies); 
                                            if(count($comment_replies)>1) { echo " Replies"; }
                                            else { echo " Reply"; } ?>
                                        </a>
                                        <i class="mdi mdi-bullseye dot-i"></i>
                                        <?php echo $last_comment_time;?>
                                    </div>
                                    <?php }
                                    if(!empty($comment_replies))
                                    {
                                        foreach($comment_replies AS $comment_reply) { 
                                        $hidecomment = new HideComment();
                                        $hidecomment = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                        $hide_comment_ids = explode(',',$hidecomment['comment_ids']); 
                                        if(!(in_array($comment_reply['_id'],$hide_comment_ids)))
                                        {
                                            if(($userid == $post['post_user_id']) || ($userid == $comment_reply['user']['_id']))
                                            {
                                                $bfun_post = 'deleteComment';
                                                $btool_post = 'Delete';
                                            }
                                            else
                                            {
                                                $bfun_post = 'hideComment';
                                                $btool_post = 'Hide';
                                            }
                                        $comment_time = Yii::$app->EphocTime->comment_time(time(),$comment_reply['updated_date']);

                                        $like_count = Like::find()->where(['comment_id' => (string) $comment_reply['_id']  ,'status' => '1'])->all();

                                        $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $comment_reply['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                        $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                        $usrbox = array();
                                        foreach ($comlikeuseinfo as $key => $single) {
                                        $fullnm = $single['fname'] . ' ' . $single['lname'];
                                        $usrbox[(string)$single['_id']] = $fullnm; 
                                        }

                                        $newlike_buddies = implode("<br/>", $usrbox)

                                ?>
                                <div class="comments-reply-details">
                                    <div class="pcomment comment-reply" id="comment_<?php echo $comment_reply['_id']?>">
                                        <div class="img-holder">
                                            <div class="profiletipholder" id="commentptip-6">
                                                <span class="profile-tooltip tooltipstered">
                                                    <img class="circle" src="<?php echo $this->getimage($comment_reply['user']['_id'],'thumb'); ?>">
                                                </span>
                                            </div>
                                        </div>
                                        <div class="desc-holder">
                                            <div class="normal-mode">
                                                <div class="desc">
                                                    <a class="userlink" href="<?php $id = $comment_reply['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>"><?php echo ucfirst($comment_reply['user']['fname']).' '.ucfirst($comment_reply['user']['lname']);?></a>
                                                    
                                                    <?php if(strlen($comment_reply['comment'])>200){ ?>
                                                        <p class="shorten" data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?><a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                        </p>
                                                    <?php }else{ ?>                                                         
                                                        <p data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?></p>
                                                    <?php } ?>
                                                </div>
                                                <div class="comment-stuff">
                                                    <div class="more-opt">
                                                            <span class="likeholder" id="likeholder-6">
                                                            <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$comment_reply['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $comment_reply['_id']?>')" title="Like">
                                                            <i class="zmdi zmdi-thumb-up"></i>
                                                            </a>
                                                            </span>
                                                            <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                <!-- Dropdown Trigger -->
                                                                  <a class='dropdown-button more_btn' href='javascript:void(0)' data-activates='subset_<?=$comment_reply['_id']?>'>
                                                                    <i class="zmdi zmdi-more-vert zmdi-hc-lg"></i>
                                                                  </a>

                                                                  <!-- Dropdown Structure -->
                                                                  <ul id='subset_<?=$comment_reply['_id']?>' class='dropdown-content custom_dropdown'>
                                                                    <?php if(($userid != $post['post_user_id']) && ($userid != $comment_reply['user']['_id'])){?>
                                                                        <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$btool_post?></a></li>
                                                                        <?php } else { ?>
                                                                            <?php if($userid == $comment_reply['user']['_id']){ ?>
                                                                            <li>
                                                                              <a class="edit-comment" href="javascript:void(0)">Edit</a>
                                                                            </li>
                                                                            <?php } ?>
                                                                            <li>
                                                                              <a href="javascript:void(0)" class="delete-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')">Delete</a>
                                                                            </li>
                                                                        <?php } ?>
                                                                  </ul>
                                                            </div>
                                                    </div>
                                                    <div class="less-opt"><div class="timestamp"><?= $comment_time;?></div></div>
                                                </div>  
                                            </div>
                                            <div class="edit-mode">
                                                <div class="desc">
                                                    <div class="sliding-middle-out anim-area tt-holder underlined fullwidth">
                                                        <textarea class="editcomment-tt materialize-textarea" data-id="<?php echo $comment_reply['_id'];?>" id="edit_comment_<?php echo $comment_reply['_id'];?>"><?php echo $comment_reply['comment']?></textarea>
                                                    </div>
                                                    <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px    "></i></a>
                                                </div>                                                                          
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } } } ?>
                                </div>
                                
                                <div class="comment-reply-holder comment-addreply">                                 
                                    <div class="addnew-comment comment-reply">                          
                                        <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $this->getimage($userid,'thumb');?>"/></a></div>
                                        <div class="desc-holder">                                   
                                            <div class="sliding-middle-out anim-area tt-holder">
                                                <textarea name="reply_txt" placeholder="Write a reply" data-postid="<?php echo $post['_id'];?>" data-commentid="<?php echo $init_comment['_id']?>" id="reply_txt_<?php echo $init_comment['_id'];?>" class="reply_class materialize-textarea"></textarea>
                                            </div>  
                                        </div>  
                                    </div>
                                </div>
                            </div>
                            <?php  } } } ?> 
                        </div>
                        <?php if($post['comment_setting'] != 'Disable'){
                        if(isset($post['trav_item']) && $post['trav_item']== '1')
                        {
                            $comment_placeholder = "Ask question or send query";
                        }
                        else{
                            $comment_placeholder = "Write a comment";
                        }   
                        ?>
                        <div class="addnew-comment valign-wrapper">
                        <?php $comment_image = $this->getimage($userid,'thumb'); ?>
                        <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $comment_image?>"/></a></div>
                            <div class="desc-holder">                                   
                                <div class="sliding-middle-out anim-area tt-holder">
                                    <textarea data-postid="<?php echo $post['_id'];?>" id="comment_txt_<?php echo $post['_id'];?>" placeholder="<?= $comment_placeholder;?>" class="comment_class  materialize-textarea"></textarea>
                                </div>
                            </div>
                            <?php if($existing_posts == ''){ ?>
                                <input type="hidden" id="last_inserted" name="last_inserted" value="<?php echo $post['_id']; ?>"/>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                    <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php
    }
          
           	
    public function display_last_ask($last_insert_id, $existing_posts = '', $imagefullarray = '', $cls='', $total='', $tempocls='')
    {
        $session = Yii::$app->session;
        $userid = $user_id = (string)$session->get('user_id');
        $email = $session->get('email');
        $status = $session->get('status');
        $result = LoginForm::find()->where(['_id' => $userid])->one();
        $date = time();
        if($cls == 'places-ads') {
            $post = PostForm::find()->where([(string)'_id' => $last_insert_id])->one();
            $tableLabel = 'PostForm';
        } else {
            $post = PlaceAsk::find()->where(['_id' => $last_insert_id])->andWhere(['not','flagger', "yes"])->one();
            $tableLabel = 'PlaceAsk';
        }
        $reportpost = ReportPost::find()->where(['post_id' => (string)$last_insert_id,'reporter_id' => (string)$user_id])->one();
        $profile_tip_counter = 1;
        $rand = rand(999, 99999);
          
        
        $Auth = '';
        if(isset($userid) && $userid != '') 
        {
            $authstatus = UserForm::isUserExistByUid($userid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
            {
                $Auth = $authstatus;
            }
        }   
        else    
        {
            $Auth = 'checkuserauthclassg';
        }
        $my_post_view_status = $post['post_privacy'];

        if($my_post_view_status == 'Private') {$post_dropdown_class = 'lock';}
        else if($my_post_view_status == 'Connections') {$post_dropdown_class = 'account';}
        else if($my_post_view_status = 'Public') {$post_dropdown_class = 'earth';}
        if(!($reportpost))
        {
            $postprivacy = $post['post_privacy'];
            $postownerid = (string)$post['post_user_id'];
            $suserid = (string)$user_id;
            $is_connect = Connect::find()->where(['from_id' => $postownerid,'to_id' => $suserid,'status' => '1'])->one();
            if(($postprivacy == 'Public') || ($postprivacy == 'Connections' && ($is_connect || $postownerid == $suserid)) || ($postprivacy == 'Private' && $postownerid == $suserid))
            {
                $block = BlockConnect::find()->where(['user_id' => "$postownerid"])->andwhere(['like','block_ids',$suserid])->one();
                if(!$block)
                {
                $savestatus = new SavePost();
                $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => (string)$post['_id']])->one();
                $savestatuscalue = $savestatus['is_saved'];
                if($savestatuscalue == '1'){$savevar = 'Unsave';}else{$savevar = 'Save';}

                $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
                $like_count = Like::getLikeCount((string)$post['_id']);
                $comments = Comment::getAllPostLike($post['_id']);
                $init_comments = Comment::getFirstThreePostComments($post['_id']);
                $post_privacy = $post['post_privacy'];
                if($post_privacy == 'Private') {$post_class = 'lock';}
                else if($post_privacy == 'Connections') {$post_class = 'user';}
                else {$post_privacy = 'Public'; $post_class = 'earth';}
                
                // START add tag content in post
                $posttag = '';
                if(isset($post['post_tags']) && !empty($post['post_tags']))
                {
                    $posttag = explode(",", $post['post_tags']);
                }

                $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');

                $nkTag = array();
                $nvTag = array();

                $i=1;
                foreach ($taginfomatiom as $key => $value)
                {
                    $nkTag[] = (string)$value; 
                    $nvTag[] = $key;
                    if($i != 1) {
                        $content[] = $key;
                    }
                    $i++;
                }

                if(isset($content) && !empty($content))
                {
                    $content = implode("<br/>", $content); 
                }

                $tagstr = '';
                if(!empty($taginfomatiom))
                {
                    if(count($taginfomatiom) > 1) {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
                    } else {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
                    }
                }
                if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                {
                    $page_details = Page::Pagedetails($post['page_id']);
                    $review = " reviewed <a href=".Url::to(['page/index', 'id' => $post['page_id']]) ." class='sub-link'>" . $page_details['page_name'] . "</a>"; }
                else { $review = ''; }
                // END add tag content in post
                $profile_tip_counter++;         
                if(isset($post['currentlocation']) && !empty($post['currentlocation']))
                {
                    $current_location = $this->getshortcityname($post['currentlocation']);
                    $default = 'at';
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='reviews'){$default = 'reviewed';}
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='tip'){$default = 'tip for';}
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='ask'){$default = 'has a question about';}
                    $locationtags = $tagstr . ' '.$default.' <a class="sub-link" href="javascript:void(0)">'.$current_location.'</a>';
                }
                else{ $locationtags = $tagstr . ''; }
                if($post['is_timeline'] == '1')
                {
                    $getrateid = PlaceAsk::find()->where(['_id' => $post['parent_post_id']])->andWhere(['not','flagger', "yes"])->one();
                    $getratedid = $getrateid['rating'];
                    if($getratedid == null){$getratedid = '6';}
                }
                else
                {
                    $getratedid = '6';
                }
                if((isset($post['is_ad']) && !empty($post['is_ad']) && $post['is_ad'] == '1'))
                {
                    $is_ad = true;
                    $adclass = 'travad-box ';
                    if($post['adobj'] == 'pagelikes'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'brandawareness'){$adclass .= 'brand-ad';}
                    else if($post['adobj'] == 'websiteleads'){$adclass .= 'weblink-ad';}
                    else if($post['adobj'] == 'websiteconversion'){$adclass .= 'actionlink-ad';}
                    else if($post['adobj'] == 'pageendorse'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'eventpromo'){$adclass .= 'event-ad';}
                    else{$adclass .= '';}
                }
                else{ $is_ad = false; $adclass = '';}
        ?>
        <style> 
        .pac-container
        {
            z-index: 1051 !important;
        }
        </style>
            <div class="<?=$tempocls .' '.$cls?> post-holder <?php if($existing_posts != 'from_save'){ ?>bshadow<?php } ?> <?php if((isset($post['is_page_review']) && !empty($post['is_page_review']) || ($getratedid != '6'))){ ?>reviewpost-holder<?php } ?> <?php if((isset($post['trav_item']) && !empty($post['trav_item']))){ ?>travelstore-ad<?php } ?> <?php if((isset($post['is_ad']) && !empty($post['is_ad']))){ echo $adclass; } ?> post_user_id_<?php echo $post['post_user_id'];?>" id="hide_<?php echo $post['_id'];?>">
            <?php
            $getuserinfo = LoginForm::find()->where(['_id' => $post['post_user_id']])->one();
            $personalinfo = Personalinfo::find()->where(['user_id' => $post['post_user_id']])->one();
            if($post['user']['status']=='44')
            {
                $dp = $this->getpageimage($post['user']['_id']);
                $link = Url::to(['page/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'page';
            }
            else
            {
                $dp = $this->getimage($getuserinfo['_id'],'photo');
                $link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'userwall';
            }
            if(isset($getuserinfo['cover_photo']) && !empty($getuserinfo['cover_photo']))
            {
                $cover = $getuserinfo['cover_photo'];
                if(strstr($cover,'cover'))
                {
                    $cvr = substr_replace($getuserinfo['cover_photo'], '-thumb.jpg', -4);
                    $cover = '../uploads/cover/thumbs/'.$cvr;
                }
                else
                {
                    $cover = '../uploads/cover/thumbs/thumb_'.$getuserinfo['cover_photo'];
                }
            }
            else
            {
                $cover = 'cover.jpg';
            }
            $dpforpopup = $this->getimage($post['user']['_id'],'thumb');
            $mutualcount = Connect::mutualconnectcount($getuserinfo['_id']);

            $po_id = $post['post_user_id'];
            $isconnect = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '1'])->one();
            $isconnectrequestsent = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '0'])->one();
            ?>
            <?php if($is_ad && $post['adobj'] != 'travstorefocus'){
                $this->display_ad($post['_id'],'topbar');
            } else { ?>
                <div class="post-topbar">
                    <div class="post-userinfo">
                        <div class="img-holder">
                            <div  class="profiletipholder">
                                <span id="profiletip-<?= $post['_id']?>" class="profile-tooltip">
                                    <?php
                                    if($post['is_timeline']=='1') {
                                             $dpimg = $this->getimage($post['shared_by'],'photo');
                                    } else {
                                        $dpimg = $this->getimage($post['user']['_id'],'photo');
                                    }
                                    ?>
                                    <img class="circle" src="<?= $dpimg?>">
                                </span>
                               <?php if($existing_posts != 'from_save'){ ?>
                                    <span class="profiletooltip_content profiletooltip_content_last">
                                        <div class="profile-tip slidingpan-holder dis-none" id="dispatchcontentprofiletip-<?= $post['_id']?>">
                                           <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive"  src="<?= $dp?>">
                                                <div class="sliding-pan location-span">
                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                            </div>
                                            <div class="profile-tip-name">
                                                <a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a>
                                            </div>
                                            <?php if($post['user']['status']!='44'){ ?>
                                            <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                        <?php if($userid == $po_id){ ?>
                                                        <a href="javascript:void(0)">&nbsp;</a>
                                                        <?php } else if($isconnect) { ?>
                                                        <a href="javascript:void(0)"><?=$mutualcount?> Mutual connections</a>
                                                        <?php } else if($isconnectrequestsent) { ?>
                                                        <i title="Connect request sent" class="mdi mdi-account-minus"></i>
                                                        <?php } else { ?>
                                                        <a href="javascript:void(0)" title="Add Connect" class="title_<?php echo $po_id;?>">
                                                        <i class="mdi mdi-account-plus people_<?php echo $po_id;?>" onclick="addConnect('<?php echo $po_id;?>')"></i>
                                                        <i class="mdi mdi-account-minus dis-none sendmsg_<?php echo $po_id;?>"></i>
                                                        <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
                                                        </a>
                                                        <?php } ?>
                                                </div>
                                                <div class="profiletip-icon">
                                                        <a href="<?php echo $link;?>" title="View Profile"><i class="mdi mdi-eye"></i></a>
                                                </div>                  
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="desc-holder">
                            <?php
                                if($post['is_timeline']=='1'/*&& $post['pagepost']==null*/) 
                                {
                                    $getsharename = LoginForm::find()->where(['_id' => $post['shared_by']])->one();
                                    if(strstr($post['parent_post_id'],'event_'))
                                    {
                                        $eventdetails = PageEvents::getEventdetailsshare(substr($post['parent_post_id'],6));
                                        $eid = $eventdetails['_id'];
                                        $ename = $eventdetails['event_name'];
                                        $elink = Url::to(['event/detail', 'e' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s event";
                                    }
                                    else if(strstr($post['parent_post_id'],'page_'))
                                    {
                                        $pagedetails = Page::Pagedetails(substr($post['parent_post_id'],5));
                                        $eid = $pagedetails['page_id'];
                                        $ename = $pagedetails['page_name'];
                                        $elink = Url::to(['page/index', 'id' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s page";
                                    }
                            ?>
                            <a href="<?php $id =  $getsharename['_id']; echo Url::to([$linkwall.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($getsharename['fname']).' '.ucfirst($getsharename['lname']) ;?></a><?= $review?>
                            <?php if($post['post_user_id'] != $post['shared_by']) { ?>
                            <div class="user-to">
                                <i class="mdi mdi-menu-right"></i>
                                <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            </div>
                            <?php } ?>
                            <?php if(isset($locationtags) && !empty($locationtags)) { ?>
                                <?= $locationtags?>
                            <?php } } else { ?>
                            <a href="<?php $id = $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            <?php if((empty($post['trav_item']))){ ?><?= $locationtags?><?php } ?>
                            <?php if($post['post_type']=='profilepic') { ?>
                            updated profile photo.
                            <?php } else if($post['is_coverpic']=='1') { ?>
                            updated cover photo.
                            <?php } ?>
                            <?php } ?>
                            <span class="timestamp"><?php echo $time; ?>
                            <?php /* <span class="mdi mdi-<?= $post_class?> mdi-13px"></span> */ ?>
                            </span>
                        </div>
                    </div>
                    <?php
                    $p = 'post';
                    if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                    {
                        $p = 'review';
                    }
                    if(isset($post['placetype']) && !empty($post['placetype']))
                    {
                        $p = $post['placetype'];
                        if($p == 'reviews'){$p = 'review';}
                        if($p == 'ask'){$p = 'question';}
                    }
                    ?>
                    <div class="settings-icon">
                        <div class="dropdown dropdown-custom dropdown-xxsmall">

                            <a class="dropdown-button more_btn <?=$Auth?> directcheckuserauthclass" href="javascript:void(0)" data-activates='save_inner<?=$last_insert_id?>'>
                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                            </a>
                            <ul id="save_inner<?=$last_insert_id?>" class="dropdown-content custom_dropdown">
                                <?php if($status == '10') { ?> 
                                <li><a href="javascript:void(0)" data-id="<?=(string)$post['_id']?>" data-module="<?=$p?>" class="customreportpopup-modal reportpost-link" onclick="flagpost(this)"></span>Flag <?=$p?></a></li>
                                <?php } else {?>  
                                <li><a href="javascript:void(0)" class="hidepost" onclick="hidePost('<?php echo $post['_id']?>')">Hide <?=$p?></a></li>
                                <li><a href="javascript:void(0)" class="savepost-link" id="save_post_<?php  echo $post['_id'];?>" data-pid="<?php  echo $post['_id'];?>" onclick="savePost('<?php echo $post['_id']?>','<?php echo $post['post_type']?>','<?=$savevar?>','ask')"><?=$savevar?> <?=$p?></a></li>
                                    <input type="hidden" class="tbsp_<?=$post['_id']?>" value="<?=$savevar?>">
                                <li><a href="javascript:void(0)" onclick="turn_off_notification('<?=$post['_id'] ?>', 'question')" class="turnnotpost_<?=$post['_id'] ?>">
                                <?php
                                $turnoff = TurnoffNotification::find()->where(['user_id' => "$user_id"])->andwhere(['like','post_ids',$post['_id']])->one();
                                if($turnoff) { 
                                    $turnoffvalue = "Unmute this question notification";
                                } else { 
                                    $turnoffvalue = "Mute this question notification";
                                }
                                ?>
                                <?=$turnoffvalue?>
                                </a></li>    
                                <?php if($post['user']['status']=='44' && ($post['is_coverpic']=='1' || $post['is_profilepic']=='1')) { $page_details = Page::Pagedetails($post['post_user_id']); $pageownerid = $page_details['created_by']; }
                                else { $pageownerid = $post['post_user_id']; } ?>
                                <?php if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                <li><a href="javascript:void(0)" class="deletepost-link" onclick="deleteAsk('<?php echo $post['post_user_id']?>','<?php echo $post['_id']?>')">Delete <?=$p?></a></li>
                                <?php if(($post['is_coverpic']!='1' && $post['is_profilepic']!='1' && $post['shared_from']!='shareentity')){ ?>
                                <li><a href="javascript:void(0)" data-editpostid="<?php echo $post['_id'];?>" class="customeditpopup-modal-ask editpost-link">Edit <?=$p?></a></li>
                                <?php } ?>
                                <?php } ?>
                                <?php if(($userid != $post['post_user_id']) && ($user_id != $post['shared_by']) && ($user_id != $pageownerid)){
                                $muteuser = UnfollowConnect::find()->where(['user_id' => "$user_id"])->andwhere(['like','unfollow_ids',$post['post_user_id']])->one();
                                if($muteuser) { $mutevalue = 'Unmute this member posts';}
                                else { $mutevalue = 'Mute this member posts';}
                                ?>
                                <li><a href="javascript:void(0)" onclick="unfollowConnect('<?php echo $post['post_user_id']?>')"><?=$mutevalue?></a></li>
                                <li><a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Report post</a></li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>          
                        <?php
                        if($post['is_timeline']=='1' && isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                        {
                        ?>
                            <div class="post-content">
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                <?php if($post['post_text'] != ''){ ?>
                                        <div class="post-desc">
                                            <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                            
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                    </div>
                                                <?php }else{ ?>                                     
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>
                                                
                                            <?php } ?>
                                        </div>
                                  <?php } ?>
                                    </div>
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                </div>
                            </div>
                            <?php
                            if(isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                            {
                                if(strstr($post['parent_post_id'],'event_'))
                                {
                                    $this->view_event((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'page_'))
                                {
                                    $this->view_page((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'collection_'))
                                {
                                    $this->view_collection((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'trip_'))
                                {
                                    $this->view_trip((string)$post['parent_post_id']);
                                }
                                else
                                {
                                    $this->view_post_id((string)$post['parent_post_id']);
                                }
                            }
                        } else if($post['is_trip'] == '1'){
                        ?>
                            <div class="post-content tripexperince-post">
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                        <div class="post-title">
                                            <?= $post['post_title'];?>
                                        </div>
                                    </div>
                                    <div class="trip-summery">
                                        <div class="location-info raani">
                                            <h5><i class="zmdi zmdi-pin"></i>
                                            <?php  
                                            $currentlocation = trim($post['currentlocation']);

                                            if($currentlocation != '') {
                                                $currentlocation = explode(",", $currentlocation);
                                                if(count($currentlocation) >1) {
                                                    $first = reset($currentlocation);
                                                    $last = end($currentlocation);
                                                    $currentlocation = $first.', '.$last;
                                                } else {
                                                    $currentlocation = implode(", ", $currentlocation);
                                                }
                                            }

                                            echo $currentlocation;
                                            ?></h5>
                                            <i class="mdi mdi-menu-right"></i>
                                            <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                        </div>
                                        <!--<div class="views-info"><i class="mdi mdi-eye"></i>45 Views</div>-->
                                    </div>
                                    <div class="map-holder dis-none">
                                        <iframe width="600" height="450" frameborder="0" src="https://maps.google.it/maps?q=<?= $post['currentlocation'];?>&output=embed"></iframe>
                                    </div>
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                </div>
                                <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                    $cnt = 1;
                                    $eximgs = explode(',',$post['image'],-1);
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        if($post['image'] == null)
                                        {
                                            $eximgs[0] = '/uploads/travitem-default.png';
                                        }
                                        $eximgss[] = $eximgs[0];
                                        $eximgs = $eximgss;                                     
                                    }
                                    $totalimgs = count($eximgs);
                                    $imgcountcls="";
                                    if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                    if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                    if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                    if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                    if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                    if($totalimgs > '5'){$imgcountcls = 'more-img';}
                                ?>
                                <div class="post-img-holder">
                                    <div class="post-img <?= $imgcountcls?> lgt-gallery lgt-gallery-photo dis-none">
                                        <?php
                                        foreach ($eximgs as $eximg) {
                                        if (file_exists('../web'.$eximg)) {
                                        $picsize = '';
                                        $val = getimagesize('../web'.$eximg);
                                        $picsize .= $val[0] .'x'. $val[1] .', ';
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                        if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                        if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$gallery_item_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                                <?php if($cnt == 5 && $totalimgs > 5){?>
                                                    <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                                <?php } ?>
                                            </a>
                                        <?php } $cnt++; } ?>
                                    </div>
                                    <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                        if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                            <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                        <?php } else { 
                                            $iname = $this->getimagename($eximg);
                                            $inameclass = $this->getimagefilename($eximg);
                                            $image = '../web/uploads/gallery/'.$iname;
                                            $JIDSsdsa = '';           
                                            $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                            if(!empty($pinit)) {
                                                $JIDSsdsa = 'active';                    
                                            }
                                            ?>
                                            <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                        <?php 
                                        } 
                                    } 
                                    ?>
                                </div>
                                <?php } ?>
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                    <?php 
                                    if($post['post_text'] != ''){ ?>
                                            <div class="post-desc">
                                                <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                                    <?php if(strlen($post['post_text'])>187){ ?>
                                                        <div class="para-section">
                                                            <div class="para">
                                                                <p><?= $post['post_text'] ?></p>
                                                            </div>
                                                            <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                        </div>
                                                    <?php } else { ?>                                       
                                                        <p><?= $post['post_text'] ?></p>
                                                    <?php } ?>
                                                
                                                <?php } ?>
                                            </div>
                                      <?php } ?>
                                    </div>  
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                    
                                </div>
                            </div>
                        <?php   
                        } else if($is_ad && $post['adobj'] != 'travstorefocus'){
                            $this->display_ad($post['_id'],'content');
                        }
                        else
                        { if(!isset($post['is_page_review']) && empty($post['is_page_review'])) {
                        ?>
                        <div class="post-content">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                    <?php if($post['post_title'] != null) { ?>
                                    <div class="post-title"><?= $post['post_title'] ?></div>
                                    <?php } ?>
                                    <?php if(isset($post['placereview']) && !empty($post['placereview']) && $post['placetype']=='reviews'){ ?>
                                    <div class="rating-stars">
                                    <?php for($i=0;$i<5;$i++)
                                    { ?>
                                        <i class="mdi mdi-star <?php if($i < $post['placereview']){ ?>active<?php } ?>"></i>
                                    <?php }
                                    ?>
                                    </div>
                                    <?php } ?>
                                    <?php if($post['trav_price'] != null) { ?>
                                    <div class="post-price">$<?= $post['trav_price'] ?></div>
                                    <?php } ?>
                                    <?php if((isset($post['trav_item']) && !empty($post['trav_item']) && $post['currentlocation'] != null)){ ?>
                                        <div class="post-location" style="display:block"><i class="zmdi zmdi-pin"></i><?= $post['currentlocation'] ?></div>
                                    <?php } ?>
                                    <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                    <div class="post-desc">
                                    
                                        <?php if(strlen($post['post_text'])>187){ ?>
                                            <div class="para-section">
                                                <div class="para">
                                                    <p><?= $post['post_text'] ?></p>
                                                </div>
                                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                            </div>
                                        <?php }else{ ?>                                     
                                            <p><?= $post['post_text'] ?></p>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if($post['trav_item']== '1' && $post['is_ad']== '1'){
                                    $actbtn = $post['adbtn'];
                                    $link = $post['adurl'];
                                    if(!empty($link))
                                    {
                                        if(substr( $link, 0, 4 ) === "http")
                                        {
                                            $link = $link;
                                        }
                                        else
                                        {
                                            $link = 'https://'.$link;
                                        }
                                    }
                                    else
                                    {
                                        $link = 'Not added';
                                    }
                                ?>
                                <div class="post-paidad">
                                    <div class="travad-sponsor">Sponsered by <a href="<?=$link?>" target="blank"><?=$link?></a></div>
                                    <a class="travad-shop btn btn-primary btn-md">
                                        <?=$actbtn?>
                                    </a>
                                   </div>
                                <?php } ?>
                                
                                <?php if($post['post_type'] == 'link'){ ?>
                                <div class="pvideo-holder">
                                    <?php if($post['image'] != 'No Image'){ ?>
                                    <div class="img-holder"><img src="<?= $post['image'] ?>"/></div>
                                    <div class="desc-holder">
                                    <?php } ?>
                                        <h4><a href="<?= $post['post_text']?>" target="_blank"><?= $post['link_title'] ?></a></h4>
                                        <p><?= $post['link_description'] ?></p>
                                    <?php if($post['image'] != 'No Image'){ ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <a class="overlay-link postdetail-link popup-modal" href="javascript:void(0)" id="MobilePost" data-postid="<?php echo $post['_id'];?>">&nbsp;</a>
                            </div>
                            <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                                $post['post_type'] = 'text and image';
                            }?>
                            <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                $cnt = 1;
                                $eximgs = explode(',',$post['image'],-1);
                                if(isset($post['trav_item']) && $post['trav_item']== '1')
                                {
                                    if($post['image'] == null)
                                    {
                                        $eximgs[0] = '/uploads/travitem-default.png';
                                    }
                                    $eximgss[] = $eximgs[0];
                                    $eximgs = $eximgss;                                     
                                }
                                $totalimgs = count($eximgs);
                                $imgcountcls="";
                                if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                if($totalimgs > '5'){$imgcountcls = 'more-img';}
                            ?>
                            <div class="post-img-holder">
                                <div class="post-img <?= $imgcountcls?> lgt-gallery lgt-gallery-photo dis-none">
                                    <?php
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$gallery_item_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                            <?php if($cnt == 5 && $totalimgs > 5){?>
                                                <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                            <?php } ?>
                                        </a>
                                    <?php } $cnt++; } ?>
                                </div>
                                <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                    if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                        <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                    <?php } else { 
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $image = '../web/uploads/gallery/'.$iname;
                                        $JIDSsdsa = '';           
                                        $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                        if(!empty($pinit)) {
                                            $JIDSsdsa = 'active';                    
                                        }
                                        ?>
                                        <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                    <?php 
                                    } 
                                } 
                                ?>
                            </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'image' && $post['is_coverpic'] == '1' && file_exists('uploads/cover/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="post-img one-img lgt-gallery lgt-gallery-photo dis-none">
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('uploads/cover/'.$post['image']);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);

                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                          <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?='/uploads/cover/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$gallery_item_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                            </a>
                                        <?php /* </div> */ ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'profilepic' && file_exists('profile/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="post-img one-img lgt-gallery lgt-gallery-photo dis-none">
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('profile/'.$post['image']);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$gallery_item_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            
                        </div>
                        <?php } else { ?>
                        <div class="post-content <?php if($post['image'] != null) { ?>hasimg<?php } ?>">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                        <?php if($post['post_title'] != null) { ?>
                                        <div class="post-title"><?=$post['post_title']?></div>
                                        <?php } ?>
                                        <div class="rating-stars">
                                        <?php for($i=0;$i<5;$i++)
                                        { ?>
                                            <i class="mdi mdi-star <?php if($i < $post['rating']){ ?>active<?php } ?>"></i>
                                        <?php }
                                        ?>
                                        </div>
                                        <div class="post-desc">
                                                <?php if($post['post_text'] == null) { $post['post_text'] = 'Not added'; } ?>
                                                
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                    </div>
                                                <?php }else{ ?>                                     
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>                                             
                                        </div>
                                </div>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                                <?php if($post['image'] != null) {
                                    $eximgs = explode(',',$post['image'],-1);
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}
                                ?>
                                <div class="post-img-holder">
                                    <div class="post-img one-img gallery swipe-gallery">
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                                <?php } } } ?>
                            
                        </div>
                        <?php } } ?>

                        <?php if($post['user']['status']=='44') { $page_details = Page::Pagedetails($post['page_id']); $pageownerid = $page_details['created_by']; }
                        else { $pageownerid = ''; } ?>
                        <!-- Edit Post Block -->
                        <div class="clear"></div>
                        <div class="post-data">
                            <div class="post-actions">
                            <div class="right like-tooltip">
                                <?php
                                    $like_active = '';
                                    $post_id = $post['_id'];
                                    $like_names = Like::getLikeUserNames((string)$post['_id']);
                                    $like_buddies = Like::getLikeUser((string)$post['_id']);
                                    $newlike_buddies = array();
                                    foreach($like_buddies as $like_buddy) {
                                    $like_active = Like::find()->where(['post_id' => "$post_id",'status' => '1','user_id' => (string) $user_id])->one();
                                    if(!empty($like_active))
                                    {
                                        $like_active = 'active';
                                    }
                                    else
                                    {
                                        $like_active = '';
                                    }
                                        $newlike_buddies[] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
                                    }
                                    $newlike_buddies = implode('<br/>', $newlike_buddies);
                                ?>
                                     <a href="javascript:void(0)" onclick="doLike('<?=$post['_id']?>');" class="pa-like liveliketooltip liketitle_<?=$post['_id']?> <?=$like_active?>"  data-title='<?=$newlike_buddies?>'>
                                        <i class="zmdi zmdi-thumb-up"></i>
                                     </a>
                                     <span class="tooltip_content lcount likecount_<?=$post['_id']?>"> <?php if($like_count >0 ) { echo $like_count; } ?></span>
                                 <?php if($post['comment_setting'] != 'Disable'){?>

                                <a href="javascript:void(0)" class="pa-comment <?=$Auth?>" data-id="<?php echo $post['_id'];?>" title="Comment">
                                    <i class="zmdi zmdi-comment"></i>
                                </a>
                                <?php if((count($comments)) > 0){ ?>
                                <span class="comment-lcount commentcountdisplay_<?=$post['_id']?>">
                                    <?php echo count($comments);?>
                                </span>
                                <?php } ?>

                                <?php } ?>
                                <?php 
                                $notifica = Notification::find()->where(['notification_type' => 'editpostuser','post_id' => (string)$post['_id']])->one();
                                if($notifica && $status == '10' && $post['is_deleted'] == '2')
                                { ?>
                                    <a id="publishposticon" href="javascript:void(0)" onclick="publishpost('<?php echo $post['_id'];?>')" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                <?php } ?>                              
                            </div>
                            </div>
                            <div class="comments-section panel">
                                <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                                <div class="comments-area">
                                <?php } ?>
                                <?php if(count($init_comments)>0){ ?>
                                <div class="post-more">
                                    <?php if(count($init_comments)>2){ ?>
                                    <a href="javascript:void(0)" class="view-morec-<?php echo $post['_id']; ?>" onclick="showPreviousComments('<?php echo $post['_id'];?>')">View more comments <i class="zmdi zmdi mdi-chevron-right trun90"></i></a>
                                    <?php } ?>
                                    <span class="total-comments commment-ctr-<?php echo $post['_id'];?>" id="commment-ctr-<?php echo $post['_id'];?>"><font class="ctrdis_<?=$post['_id']?>"><?php echo count($init_comments);?></font> of <font class="countdisplay_<?=$post['_id']?>"><?php echo count($comments);?></font></span>
                                </div>
                                <?php } ?>
                                <input type="hidden" name="from_ctr" id="from_ctr_<?php echo $post['_id'];?>" value="<?php echo count($init_comments);?>">      
                                <input type="hidden" name="to_ctr" id="to_ctr_<?php echo $post['_id'];?>" value="<?php echo count($comments);?>">

                                <div class="post-comments post_comment_<?=$post['_id']?>">                     
                                    <div class="pcomments sub_post_comment_<?=$post['_id']?>">                                    
                                        <?php
                                        if(count($init_comments)>0) { 
                                            foreach($init_comments as $init_comment) {
                                                $rand = rand(999, 99999).time();
                                                $cmnt_id = (string) $init_comment['_id'];
                                                $cmnt_like_active= '';
                                                $cmnt_like_active = Like::find()->where(['comment_id' => $cmnt_id,'status' => '1','user_id' => (string) $user_id])->one();
                                                if(!empty($cmnt_like_active))
                                                {
                                                    $cmnt_like_active = 'active';
                                                }
                                                else
                                                {
                                                    $cmnt_like_active = '';
                                                }
                                                $comment_time = Yii::$app->EphocTime->comment_time(time(),$init_comment['updated_date']);$commentcount =Comment::getCommentReply($init_comment['_id']);
                                                $hidecomments = new HideComment();
                                                $hidecomments = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                $hide_comments_ids = explode(',',$hidecomments['comment_ids']);
                                                if(!(in_array($init_comment['_id'],$hide_comments_ids)))
                                                {
                                                    if(($userid == $post['post_user_id']) || ($userid == $init_comment['user']['_id']))
                                                    {
                                                        $afun_post = 'deleteComment';
                                                        $atool_post = 'Delete';
                                                    }
                                                    else
                                                    {
                                                        $afun_post = 'hideComment';
                                                        $atool_post = 'Hide';
                                                    }
                                        ?>
                                        <div class="pcomment-holder <?php if(count($commentcount) > 0){ ?>has-comments<?php } ?>" id="comment_<?php echo $init_comment['_id']?>">
                                            <div class="pcomment main-comment">
                                                <div class="img-holder">
                                                    <div id="commentptip-4" class="profiletipholder">
                                                        <span class="profile-tooltip">
                                                            <?php $init_comment_img = $this->getimage($init_comment['user']['_id'],'thumb'); ?>
                                                            <img class="circle" src="<?= $init_comment_img?>"/>
                                                        </span>
                                                    </div>
                                                </div> 
                                                <div class="desc-holder">
                                                    <div class="normal-mode">
                                                        <div class="desc">
                                                            <a href="<?php $id = $init_comment['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>" class="userlink"><?php echo ucfirst($init_comment['user']['fname']).' '.ucfirst($init_comment['user']['lname']);?></a>
                                                            <?php if(strlen($init_comment['comment'])>200){ ?>
                                                                <p class="shorten" data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?>
                                                                <a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                </p>
                                                            <?php }else{ ?>                                                     
                                                                <p data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?></p>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="comment-stuff">
                                                            <div class="more-opt">
                                                                <span id="likeholder-7" class="likeholder">
                                                                <?php

                                                                     
                                                                    $like_count = Like::find()->where(['comment_id' => (string) $init_comment['_id']  ,'status' => '1'])->all();

                                                                    $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $init_comment['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                                    $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                                    $usrbox = array();
                                                                    foreach ($comlikeuseinfo as $key => $single) {
                                                                        $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                                         $usrbox[(string)$single['_id']] = $fullnm; 
                                                                    }
                                                                    
                                                                    $newlike_buddies = implode("<br/>", $usrbox);
                                                                ?>

                                                                        <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$init_comment['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $init_comment['_id']?>')">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                </span> 
                                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply"></i></a>
                                                                
                                                                <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                    <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="sadadas<?=$rand?>">
                                                                        <i class="mdi mdi-dots-vertical"></i>
                                                                    </a>
                                                                    <?php if($status == '10') { ?> 
                                                                    <ul class="dropdown-content" id="sadadas<?=$rand?>">
                                                                       <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')">Flag</a></li></ul>
                                                                    <?php } else { ?>
                                                                    <ul class="dropdown-content" id="sadadas<?=$rand?>">
                                                                        <?php if(($userid != $post['post_user_id']) && ($userid != $init_comment['user']['_id'])){?>
                                                                            <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$atool_post?></a></li>
                                                                            <?php } else { ?>
                                                                            <?php if($userid == $init_comment['user']['_id']){ ?>
                                                                            <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                            <?php } ?>
                                                                            <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>','<?=$post['_id']?>')">Delete</a></li>                                                  
                                                                        <?php } ?>
                                                                    </ul>
                                                                    <?php } ?>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="less-opt"><div class="timestamp"><?php echo $comment_time?></div></div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-mode">
                                                        <div class="desc">
                                                            <textarea class="materialize-textarea editcomment-tt" data-id="<?php echo $init_comment['_id'];?>" id="edit_comment_<?php echo $init_comment['_id'];?>"><?php echo $init_comment['comment']?></textarea>
                                                            <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px   "></i></a>
                                                        </div>                                                                          
                                                    </div>
                                                </div>                              
                                            </div>  
                                            <div class="clear"></div>
                                            
                                            <div class="comment-reply-holder reply_comments_<?php echo $init_comment['_id'];?>">
                                                <?php $comment_replies =Comment::getCommentReply($init_comment['_id']);
                                                    if(count($comment_replies)>0) {
                                                    $lastcomment = Comment::find()->where(['parent_comment_id' => (string)$init_comment['_id']])->orderBy(['updated_date'=>SORT_DESC])->one();
                                                    $last_comment_time = Yii::$app->EphocTime->comment_time(time(),$lastcomment['updated_date']);
                                                ?>
                                                <div class="comments-reply-summery">
                                                    <a href="javascript:void(0)" onclick="openReplies(this)">
                                                        <i class="mdi mdi-share"></i>
                                                        <?php echo count($comment_replies); 
                                                        if(count($comment_replies)>1) { echo " Replies"; }
                                                        else { echo " Reply"; } ?>
                                                    </a>
                                                    <i class="mdi mdi-bullseye dot-i"></i>
                                                    <?php echo $last_comment_time;?>
                                                </div>
                                                <?php }
                                                if(!empty($comment_replies))
                                                {
                                                    foreach($comment_replies AS $comment_reply) {
                                                    $rand = rand(999, 99999).time();
                                                    $hidecomment = new HideComment();
                                                    $hidecomment = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                    $hide_comment_ids = explode(',',$hidecomment['comment_ids']); 
                                                    if(!(in_array($comment_reply['_id'],$hide_comment_ids)))
                                                    {
                                                        if(($userid == $post['post_user_id']) || ($userid == $comment_reply['user']['_id']))
                                                        {
                                                            $bfun_post = 'deleteComment';
                                                            $btool_post = 'Delete';
                                                        }
                                                        else
                                                        {
                                                            $bfun_post = 'hideComment';
                                                            $btool_post = 'Hide';
                                                        }
                                                    $comment_time = Yii::$app->EphocTime->comment_time(time(),$comment_reply['updated_date']);

                                                    $like_count = Like::find()->where(['comment_id' => (string) $comment_reply['_id']  ,'status' => '1'])->all();

                                                    $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $comment_reply['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                    $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                    $usrbox = array();
                                                    foreach ($comlikeuseinfo as $key => $single) {
                                                    $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                    $usrbox[(string)$single['_id']] = $fullnm; 
                                                    }

                                                    $newlike_buddies = implode("<br/>", $usrbox)

                                            ?>
                                            <div class="comments-reply-details">
                                                <div class="pcomment comment-reply" id="comment_<?php echo $comment_reply['_id']?>">
                                                    <div class="img-holder">
                                                        <div class="profiletipholder" id="commentptip-6">
                                                            <span class="profile-tooltip tooltipstered">
                                                                <img class="circle" src="<?php echo $this->getimage($comment_reply['user']['_id'],'thumb'); ?>">
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="desc-holder">
                                                        <div class="normal-mode">
                                                            <div class="desc">
                                                                <a class="userlink" href="<?php $id = $comment_reply['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>"><?php echo ucfirst($comment_reply['user']['fname']).' '.ucfirst($comment_reply['user']['lname']);?></a>
                                                                
                                                                <?php if(strlen($comment_reply['comment'])>200){ ?>
                                                                    <p class="shorten" data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?><a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                    </p>
                                                                <?php }else{ ?>                                                         
                                                                    <p data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?></p>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="comment-stuff">
                                                                <div class="more-opt">
                                                                        <span class="likeholder" id="likeholder-6">
                                                                        <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$comment_reply['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $comment_reply['_id']?>')">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                                <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="sds<?=$rand?>">
                                                                                    <i class="mdi mdi-dots-vertical"></i>
                                                                                </a>
                                                                                <ul class="dropdown-content" id="sds<?=$rand?>">
                                                                                    <?php if(($userid != $post['post_user_id']) && ($userid != $comment_reply['user']['_id'])){?>
                                                                                        <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$btool_post?></a></li>
                                                                                        <?php } else { ?>
                                                                                        <?php if($userid == $comment_reply['user']['_id']){ ?>
                                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                        <?php } ?>
                                                                                        <li><a href="javascript:void(0)" class="delete-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')">Delete</a></li>                                                  
                                                                                    <?php } ?>
                                                                                </ul>
                                                                        </div>
                                                                </div>
                                                                <div class="less-opt"><div class="timestamp"><?= $comment_time;?></div></div>
                                                            </div>  
                                                        </div>
                                                        <div class="edit-mode">
                                                            <div class="desc">
                                                                <textarea class="materialize-textarea editcomment-tt" data-id="<?php echo $comment_reply['_id'];?>" id="edit_comment_<?php echo $comment_reply['_id'];?>"><?php echo $comment_reply['comment']?></textarea>
                                                                <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px   "></i></a>
                                                            </div>                                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } } } ?>
                                            </div>
                                            
                                            <div class="comment-reply-holder comment-addreply">                                 
                                                <div class="addnew-comment comment-reply">                          
                                                    <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $this->getimage($result['_id'],'thumb');?>"/></a></div>
                                                    <div class="desc-holder">                               
                                                        <textarea name="reply_txt" placeholder="Write a reply" data-postid="<?php echo $post['_id'];?>" data-commentid="<?php echo $init_comment['_id']?>" id="reply_txt_<?php echo $init_comment['_id'];?>" class="materialize-textarea reply_class capitalize"></textarea>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <?php  } } } ?>
                                    </div>
                                    <?php if($post['comment_setting'] != 'Disable'){
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        $comment_placeholder = "Ask question or send query";
                                    }
                                    else{
                                        $comment_placeholder = "Write a comment";
                                    }   
                                    ?>
                                    <div class="addnew-comment valign-wrapper">
                                        <?php $comment_image = $this->getimage($result['_id'],'thumb'); ?>
                                        <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $comment_image?>"/></a></div>
                                        <form name="imageCommentForm" id="imageCommentForm" enctype="multipart/form-data">
                                            <div class="desc-holder">                               
                                                    <textarea data-postid="<?php echo $post['_id'];?>" id="comment_txt_<?php echo $post['_id'];?>" placeholder="<?= $comment_placeholder;?>" class="materialize-textarea comment_class capitalize"></textarea>
                                            </div>
                                            <?php if($existing_posts == ''){ ?>
                                                <input type="hidden" id="last_inserted" name="last_inserted" value="<?php echo $post['_id']; ?>"/>
                                            <?php } ?>
                                        </form>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
        <?php }    
            }      
        }
    }         

    public function display_last_discussion($last_insert_id, $existing_posts = '', $imagefullarray = '', $cls='', $total='', $tempocls='')
    {
        $session = Yii::$app->session;
        $userid = $user_id = (string)$session->get('user_id');
        $email = $session->get('email');
        $status = $session->get('status');
        $result = LoginForm::find()->where(['_id' => $userid])->one();
        $date = time();

        if($cls == 'places-ads') {
            $post = PostForm::find()->where([(string)'_id' => $last_insert_id])->one();
            $tableLabel = 'PostForm';
        } else {
            $post = PlaceDiscussion::find()->where(['_id' => $last_insert_id])->andWhere(['not','flagger', "yes"])->one();
            $tableLabel = 'PlaceDiscussion';
        }
        $reportpost = ReportPost::find()->where(['post_id' => (string)$last_insert_id,'reporter_id' => (string)$user_id])->one();
        $profile_tip_counter = 1;
        $rand = rand(9999, 999999);
        $uniqid = uniqid();
          
        
        $Auth = '';
        if(isset($userid) && $userid != '') 
        {
            $authstatus = UserForm::isUserExistByUid($userid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
            {
                $Auth = $authstatus;
            }
        }   
        else    
        {
            $Auth = 'checkuserauthclassg';
        }
        
        $my_post_view_status = $post['post_privacy'];
        if($my_post_view_status == 'Private') {$post_dropdown_class = 'lock';}
        else if($my_post_view_status == 'Connections') {$post_dropdown_class = 'account';}
        else if($my_post_view_status == 'Custom') {$post_dropdown_class = 'settings';}
        else {$post_dropdown_class = 'earth';}


        if(!($reportpost))
        {
            $postprivacy = $post['post_privacy'];
            $postownerid = (string)$post['post_user_id'];
            $suserid = (string)$user_id;
            $is_connect = Connect::find()->where(['from_id' => $postownerid,'to_id' => $suserid,'status' => '1'])->one();
            $isWriteAllow = 'no';
            
            if($userid == $postownerid) {
                $isWriteAllow = 'yes';
            } else if(($postprivacy == 'Public' || $status == '10') || ($postprivacy == 'Connections' && ($is_connect || $status == '10')) || ($postprivacy == 'Private')) {
                $isWriteAllow = 'yes';
            } else if($postprivacy == 'Custom') {
                $customids = array();
                $customids = isset($post['customids']) ? $post['customids'] : '';
                $customids = explode(',', $customids);

                if(in_array($userid, $customids)) {
                    $isWriteAllow = 'yes';          
                }
            } else if($cls != '' && $cls == 'places-ads') {
                $isWriteAllow = 'yes';      
            }


            if($isWriteAllow == 'yes')
            {
                $block = BlockConnect::find()->where(['user_id' => "$postownerid"])->andwhere(['like','block_ids',$suserid])->one();
                if(!$block)
                {
                $savestatus = new SavePost();
                $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => (string)$post['_id']])->one();
                $savestatuscalue = $savestatus['is_saved'];
                if($savestatuscalue == '1'){$savevar = 'Unsave';}else{$savevar = 'Save';}

                $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
                $like_count = Like::getLikeCount((string)$post['_id']);
                $comments = Comment::getAllPostLike($post['_id']);
                $init_comments = Comment::getFirstThreePostComments($post['_id']);
                $post_privacy = $post['post_privacy'];

                if($post_privacy == 'Private') {$post_class = 'lock';}
                else if($post_privacy == 'Connections') {$post_class = 'account';}
                else if($post_privacy == 'Custom') {$post_class = 'settings';}
                else {$post_class = 'earth';}
                // START add tag content in post
                $posttag = '';
                if(isset($post['post_tags']) && !empty($post['post_tags']))
                {
                    $posttag = explode(",", $post['post_tags']);
                }

                $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');

                $nkTag = array();
                $nvTag = array();

                $i=1;
                foreach ($taginfomatiom as $key => $value)
                {
                    $nkTag[] = (string)$value; 
                    $nvTag[] = $key;
                    if($i != 1) {
                        $content[] = $key;
                    }
                    $i++;
                }

                if(isset($content) && !empty($content))
                {
                    $content = implode("<br/>", $content); 
                }

                $tagstr = '';
                if(!empty($taginfomatiom))
                {
                    if(count($taginfomatiom) > 1) {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
                    } else {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
                    }
                }
                if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                {
                    $page_details = Page::Pagedetails($post['page_id']);
                    $review = " reviewed <a href=".Url::to(['page/index', 'id' => $post['page_id']]) ." class='sub-link'>" . $page_details['page_name'] . "</a>"; }
                else { $review = ''; }
                // END add tag content in post
                $profile_tip_counter++;         
                if(isset($post['currentlocation']) && !empty($post['currentlocation']))
                {
                    $current_location = $this->getshortcityname($post['currentlocation']);
                    $default = 'at';
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='reviews'){
                        $default = 'reviewed';
                    }
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='tip'){
                        $default = 'tip for';
                    }
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='ask'){
                        $default = 'has a question about';
                    }
                    $locationtags = $tagstr . ' '.$default.' <a class="sub-link" href="javascript:void(0)">'.$current_location.'</a>';
                }
                else{ $locationtags = $tagstr . ''; }
                if($post['is_timeline'] == '1')
                {
                    $getrateid = PlaceDiscussion::find()->where(['_id' => $post['parent_post_id']])->andWhere(['not','flagger', "yes"])->one();
                    $getratedid = $getrateid['rating'];
                    if($getratedid == null){$getratedid = '6';}
                }
                else
                {
                    $getratedid = '6';
                }
                if((isset($post['is_ad']) && !empty($post['is_ad']) && $post['is_ad'] == '1'))
                {
                    $is_ad = true;
                    $adclass = 'travad-box ';
                    if($post['adobj'] == 'pagelikes'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'brandawareness'){$adclass .= 'brand-ad';}
                    else if($post['adobj'] == 'websiteleads'){$adclass .= 'weblink-ad';}
                    else if($post['adobj'] == 'websiteconversion'){$adclass .= 'actionlink-ad';}
                    else if($post['adobj'] == 'pageendorse'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'eventpromo'){$adclass .= 'event-ad';}
                    else{$adclass .= '';}
                }
                else{ $is_ad = false; $adclass = '';}
        ?>
        <style> 
        .pac-container
        {
            z-index: 1051 !important;
        }
        </style>
            <div data-postid="<?=$post['_id']?>" class="<?=$tempocls .' '.$cls?> post-holder <?php if($existing_posts != 'from_save'){ ?>bshadow<?php } ?> <?php if((isset($post['is_page_review']) && !empty($post['is_page_review']) || ($getratedid != '6'))){ ?>reviewpost-holder<?php } ?> <?php if((isset($post['trav_item']) && !empty($post['trav_item']))){ ?>travelstore-ad<?php } ?> <?php if((isset($post['is_ad']) && !empty($post['is_ad']))){ echo $adclass; } ?> post_user_id_<?php echo $post['post_user_id'];?>" id="hide_<?php echo $post['_id'];?>">
            <?php
            $getuserinfo = LoginForm::find()->where(['_id' => $post['post_user_id']])->one();
            $personalinfo = Personalinfo::find()->where(['user_id' => $post['post_user_id']])->one();
            if($post['user']['status']=='44')
            {
                $dp = $this->getpageimage($post['user']['_id']);
                $link = Url::to(['page/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'page';
            }
            else
            {
                $dp = $this->getimage($getuserinfo['_id'],'photo');
                $link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'userwall';
            }
            if(isset($getuserinfo['cover_photo']) && !empty($getuserinfo['cover_photo']))
            {
                $cover = $getuserinfo['cover_photo'];
                if(strstr($cover,'cover'))
                {
                    $cvr = substr_replace($getuserinfo['cover_photo'], '-thumb.jpg', -4);
                    $cover = '../uploads/cover/thumbs/'.$cvr;
                }
                else
                {
                    $cover = '../uploads/cover/thumbs/thumb_'.$getuserinfo['cover_photo'];
                }
            }
            else
            {
                $cover = 'cover.jpg';
            }
            $dpforpopup = $this->getimage($post['user']['_id'],'thumb');
            $mutualcount = Connect::mutualconnectcount($getuserinfo['_id']);

            $po_id = $post['post_user_id'];
            $isconnect = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '1'])->one();
            $isconnectrequestsent = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '0'])->one();
            ?>
            <?php if($is_ad && $post['adobj'] != 'travstorefocus') {
                $this->display_ad($post['_id'],'topbar');
            } else { ?>
                <div class="post-topbar">
                    <div class="post-userinfo">
                        <div class="img-holder">
                            <div  class="profiletipholder">
                                <span id="profiletip-<?= $post['_id']?>" class="profile-tooltip">
                                    <?php
                                    if($post['is_timeline']=='1')
                                    {
                                        $dpimg = $this->getimage($post['shared_by'],'photo');
                                    }
                                    else
                                    {
                                        $dpimg = $this->getimage($post['user']['_id'],'photo');
                                    }
                                    ?>
                                    <img class="circle" src="<?= $dpimg?>">
                                </span>
                               <?php if($existing_posts != 'from_save'){ ?>
                                    <span class="profiletooltip_content profiletooltip_content_last">
                                        <div class="profile-tip slidingpan-holder dis-none" id="dispatchcontentprofiletip-<?= $post['_id']?>">
                                           <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive"  src="<?= $dp?>">
                                                <div class="sliding-pan location-span">
                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                            </div>
                                            <div class="profile-tip-name">
                                                <a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a>
                                            </div>
                                            <?php if($post['user']['status']!='44'){ ?>
                                            <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                        <?php if($userid == $po_id){ ?>
                                                        <a href="javascript:void(0)">&nbsp;</a>
                                                        <?php } else if($isconnect) { ?>
                                                        <a href="javascript:void(0)"><?=$mutualcount?> Mutual connections</a>
                                                        <?php } else if($isconnectrequestsent) { ?>
                                                        <i title="Connect request sent" class="mdi mdi-account-minus"></i>
                                                        <?php } else { ?>
                                                        <a href="javascript:void(0)" title="Add Connect" class="title_<?php echo $po_id;?>">
                                                        <i class="mdi mdi-account-plus people_<?php echo $po_id;?>" onclick="addConnect('<?php echo $po_id;?>')"></i>
                                                        <i class="mdi mdi-account-minus dis-none sendmsg_<?php echo $po_id;?>"></i>
                                                        <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
                                                        </a>
                                                        <?php } ?>
                                                </div>
                                                <div class="profiletip-icon">
                                                        <a href="<?php echo $link;?>" title="View Profile"><i class="mdi mdi-eye"></i></a>
                                                </div>                  
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="desc-holder">
                            <?php
                                if($post['is_timeline']=='1') 
                                {
                                    $getsharename = LoginForm::find()->where(['_id' => $post['shared_by']])->one();
                                    if(strstr($post['parent_post_id'],'event_'))
                                    {
                                        $eventdetails = PageEvents::getEventdetailsshare(substr($post['parent_post_id'],6));
                                        $eid = $eventdetails['_id'];
                                        $ename = $eventdetails['event_name'];
                                        $elink = Url::to(['event/detail', 'e' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s event";
                                    }
                                    else if(strstr($post['parent_post_id'],'page_'))
                                    {
                                        $pagedetails = Page::Pagedetails(substr($post['parent_post_id'],5));
                                        $eid = $pagedetails['page_id'];
                                        $ename = $pagedetails['page_name'];
                                        $elink = Url::to(['page/index', 'id' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s page";
                                    }
                            ?>
                            <a href="<?php $id =  $getsharename['_id']; echo Url::to([$linkwall.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($getsharename['fname']).' '.ucfirst($getsharename['lname']) ;?></a><?= $review?>
                            <?php if($post['post_user_id'] != $post['shared_by']) { ?>
                            <div class="user-to">
                                <i class="mdi mdi-menu-right"></i>
                                <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            </div>
                            <?php } ?>
                            <?php if(isset($locationtags) && !empty($locationtags)) { ?>
                                <?= $locationtags?>
                            <?php } } else { ?>
                            <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            <?php if((empty($post['trav_item']))){ ?><?= $locationtags?>
                            <?php }  else if($post['post_type']=='profilepic'){ ?>
                            updated profile photo.
                            <?php } else if($post['is_coverpic']=='1'){ ?>
                            updated cover photo.
                            <?php } ?>
                            <?php } ?>
                            <span class="timestamp"><?=$time?>
                                <a class="dropdown-button" href="javascript:void(0);" data-activates="<?=$uniqid?>">
                                    <i class="mdi mdi-<?=$post_class?> mdi-13px"></i>
                                    <?php 
                                    $pageownerid = $post['post_user_id'];
                                    if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                    <i class="mdi mdi-menu-down mdi-22px"></i>
                                    <?php } ?>
                                </a>

                                <ul id="<?=$uniqid?>" class="dropdown-content directprivacydropdown">
                                    <li class="post-private" data-cls="post-private"> <a href="javascript:void(0)"> Private </a> </li>
                                    <li class="post-connections" data-cls="post-connections"> <a href="javascript:void(0)"> Connections </a> </li>
                                    <li class="post-custom customli_modal_post" data-cls="post-custom"> <a href="javascript:void(0)"> Custom </a> </li>
                                    <li class="post-public" data-cls="post-public"> <a href="javascript:void(0)"> Public </a> </li>
                               </ul>
                            </span>
                        </div>
                    </div>
                    <?php
                    $p = 'post';
                    if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                    {
                        $p = 'review';
                    }
                    if(isset($post['placetype']) && !empty($post['placetype']))
                    {
                        $p = $post['placetype'];
                        if($p == 'reviews'){$p = 'review';}
                        if($p == 'ask'){$p = 'question';}
                    }
                    ?>
                    <div class="settings-icon">
                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                            <a class="dropdown-button <?=$Auth?> directcheckuserauthclass" href="javascript:void(0)" data-activates="dropdown-editdisc<?=$rand?>">
                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                            </a>
                            <ul class="dropdown-content custom_dropdown" id="dropdown-editdisc<?=$rand?>">
                                <?php if($status == '10') { ?> 
                                <li><a href="javascript:void(0)" data-id="<?=(string)$post['_id']?>" data-module="<?=$p?>" class="customreportpopup-modal reportpost-link" onclick="flagpost(this)"></span>Flag <?=$p?></a></li>
                                <?php } else {?>  
                                <li><a href="javascript:void(0)" class="hidepost" onclick="hidePost('<?php echo $post['_id']?>')">Hide <?=$p?></a></li>
                                <li><a href="javascript:void(0)" class="savepost-link" id="save_post_<?php  echo $post['_id'];?>" data-pid="<?php  echo $post['_id'];?>" onclick="savePost('<?php echo $post['_id']?>','<?php echo $post['post_type']?>','<?=$savevar?>','tip')"><?=$savevar?> <?=$p?></a></li>
                                    <input type="hidden" class="tbsp_<?=$post['_id']?>" value="<?=$savevar?>">
                                <li><a href="javascript:void(0)" onclick="turn_off_notification('<?=$post['_id'] ?>')" class="turnnotpost_<?=$post['_id'] ?>">
                                <?php
                                $turnoff = TurnoffNotification::find()->where(['user_id' => "$user_id"])->andwhere(['like','post_ids',$post['_id']])->one();
                                if($turnoff) { 
                                    $turnoffvalue = "Unmute this post notification";
                                } else { 
                                    $turnoffvalue = "Mute this post notification";
                                }
                                ?>
                                <?=$turnoffvalue?>
                                </a></li>    
                                <?php if($post['user']['status']=='44' && ($post['is_coverpic']=='1' || $post['is_profilepic']=='1')) { $page_details = Page::Pagedetails($post['post_user_id']); $pageownerid = $page_details['created_by']; }
                                else { $pageownerid = $post['post_user_id']; } ?>
                                <?php if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                <li><a href="javascript:void(0)" class="deletepost-link" onclick="deleteDiscussion('<?php echo $post['post_user_id']?>','<?php echo $post['_id']?>')">Delete <?=$p?></a></li>
                                <?php if(($post['is_coverpic']!='1' && $post['is_profilepic']!='1' && $post['shared_from']!='shareentity')){ ?>
                                <li><a href="javascript:void(0)" data-editpostid="<?php echo $post['_id'];?>" class="customeditpopup-modal-discussion editpost-link">Edit <?=$p?></a></li>
                                <?php } ?>
                                <?php } ?>
                                <?php if(($userid != $post['post_user_id']) && ($user_id != $post['shared_by']) && ($user_id != $pageownerid)){
                                $muteuser = UnfollowConnect::find()->where(['user_id' => "$user_id"])->andwhere(['like','unfollow_ids',$post['post_user_id']])->one();
                                if($muteuser) { $mutevalue = 'Unmute this member posts';}
                                else { $mutevalue = 'Mute this member posts';}
                                ?>
                                <li><a href="javascript:void(0)" onclick="unfollowConnect('<?php echo $post['post_user_id']?>')"><?=$mutevalue?></a></li>
                                <li><a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Report post</a></li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>          
                        <?php
                        if($post['is_timeline']=='1' && isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                        {
                        ?>
                            <div class="post-content">
                                <div class="post-details">
                                <?php if($post['post_text'] != ''){ ?>
                                    <div class="post-desc">
                                        <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                        
                                            <?php if(strlen($post['post_text'])>187){ ?>
                                                <div class="para-section">
                                                    <div class="para">
                                                        <p><?= $post['post_text'] ?></p>
                                                    </div>
                                                    <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                </div>
                                            <?php }else{ ?>                                     
                                                <p><?= $post['post_text'] ?></p>
                                            <?php } ?>
                                            
                                        <?php } ?>
                                    </div>
                                <?php } ?>
                                </div>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                            <?php
                            if(isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                            {
                                if(strstr($post['parent_post_id'],'page_'))
                                {
                                    $this->view_page((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'collection_'))
                                {
                                    $this->view_collection((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'trip_'))
                                {
                                    $this->view_trip((string)$post['parent_post_id']);
                                }
                                else
                                {
                                    $this->view_post_id((string)$post['parent_post_id']);
                                }
                            }
                        } else if($post['is_trip'] == '1'){
                        ?>
                            <div class="post-content tripexperince-post">
                                <div class="post-details">
                                    <div class="post-title">
                                        <?= $post['post_title'];?>
                                    </div>
                                </div>
                                <div class="trip-summery">
                                    <div class="location-info raani">
                                        <h5><i class="zmdi zmdi-pin"></i>
                                        <?php  
                                        $currentlocation = trim($post['currentlocation']);

                                        if($currentlocation != '') {
                                            $currentlocation = explode(",", $currentlocation);
                                            if(count($currentlocation) >1) {
                                                $first = reset($currentlocation);
                                                $last = end($currentlocation);
                                                $currentlocation = $first.', '.$last;
                                            } else {
                                                $currentlocation = implode(", ", $currentlocation);
                                            }
                                        }

                                        echo $currentlocation;
                                        ?></h5>
                                        <i class="mdi mdi-menu-right"></i>
                                        <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                    </div>
                                </div>
                                <div class="map-holder dis-none">
                                    <iframe width="600" height="450" frameborder="0" src="https://maps.google.it/maps?q=<?= $post['currentlocation'];?>&output=embed"></iframe>
                                </div>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                    $cnt = 1;
                                    $eximgs = explode(',',$post['image'],-1);
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        if($post['image'] == null)
                                        {
                                            $eximgs[0] = '/uploads/travitem-default.png';
                                        }
                                        $eximgss[] = $eximgs[0];
                                        $eximgs = $eximgss;                                     
                                    }
                                    $totalimgs = count($eximgs);
                                    $imgcountcls="";
                                    if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                    if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                    if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                    if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                    if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                    if($totalimgs > '5'){$imgcountcls = 'more-img';}
                                ?>
                                <div class="post-img-holder">
                                    <div class="post-img <?= $imgcountcls?> gallery swipe-gallery">
                                        <?php
                                        foreach ($eximgs as $eximg) {
                                        if (file_exists('../web'.$eximg)) {
                                        $picsize = '';
                                        $val = getimagesize('../web'.$eximg);
                                        $picsize .= $val[0] .'x'. $val[1] .', ';
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                        if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                        if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-size="1600x1600"  data-med="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-med-size="1024x1024" class="imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                                <?php if($cnt == 5 && $totalimgs > 5){?>
                                                    <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                                <?php } ?>
                                            </a>
                                        <?php } $cnt++; } ?>
                                    </div>
                                    <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                        if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                            <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                        <?php } else { 
                                            $iname = $this->getimagename($eximg);
                                            $inameclass = $this->getimagefilename($eximg);
                                            $image = '../web/uploads/gallery/'.$iname;
                                            $JIDSsdsa = '';           
                                            $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                            if(!empty($pinit)) {
                                                $JIDSsdsa = 'active';                    
                                            }
                                            ?>
                                            <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                        <?php 
                                        } 
                                    } 
                                    ?>
                                </div>
                                <?php } ?>
                                <div class="post-details">
                                <?php 
                                if($post['post_text'] != ''){ ?>
                                        <div class="post-desc">
                                            <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                    </div>
                                                <?php } else { ?>                                       
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>
                                            
                                            <?php } ?>
                                        </div>
                                  <?php } ?>
                                </div>  
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                        <?php   
                        } else if($is_ad && $post['adobj'] != 'travstorefocus'){
                            $this->display_ad($post['_id'],'content');
                        }
                        else
                        { if(!isset($post['is_page_review']) && empty($post['is_page_review'])) {
                        ?>
                        <div class="post-content">
                            <div class="post-details">
                                <?php if($post['post_title'] != null) { ?>
                                <div class="post-title"><?= $post['post_title'] ?></div>
                                <?php } ?>
                                <?php if(isset($post['placereview']) && !empty($post['placereview']) && $post['placetype']=='reviews'){ ?>
                                <div class="rating-stars">
                                <?php for($i=0;$i<5;$i++)
                                { ?>
                                    <i class="mdi mdi-star <?php if($i < $post['placereview']){ ?>active<?php } ?>"></i>
                                <?php }
                                ?>
                                </div>
                                <?php } ?>
                                <?php if($post['trav_price'] != null) { ?>
                                <div class="post-price">$<?= $post['trav_price'] ?></div>
                                <?php } ?>
                                <?php if((isset($post['trav_item']) && !empty($post['trav_item']) && $post['currentlocation'] != null)){ ?>
                                    <div class="post-location" style="display:block"><i class="zmdi zmdi-pin"></i><?= $post['currentlocation'] ?></div>
                                <?php } ?>
                                <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                <div class="post-desc">
                                
                                    <?php if(strlen($post['post_text'])>187){ ?>
                                        <div class="para-section">
                                            <div class="para">
                                                <p><?= $post['post_text'] ?></p>
                                            </div>
                                            <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                        </div>
                                    <?php }else{ ?>                                     
                                        <p><?= $post['post_text'] ?></p>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                            <?php if($post['trav_item']== '1' && $post['is_ad']== '1'){
                                $actbtn = $post['adbtn'];
                                $link = $post['adurl'];
                                if(!empty($link))
                                {
                                    if(substr( $link, 0, 4 ) === "http")
                                    {
                                        $link = $link;
                                    }
                                    else
                                    {
                                        $link = 'https://'.$link;
                                    }
                                }
                                else
                                {
                                    $link = 'Not added';
                                }
                            ?>
                            <div class="post-paidad">
                                <div class="travad-sponsor">Sponsered by <a href="<?=$link?>" target="blank"><?=$link?></a></div>
                                <a class="travad-shop btn btn-primary btn-md">
                                    <?=$actbtn?>
                                </a>
                            </div>
                            <?php } ?>
                            
                            <?php if($post['post_type'] == 'link'){ ?>
                            <div class="pvideo-holder">
                                <?php if($post['image'] != 'No Image'){ ?>
                                <div class="img-holder"><img src="<?= $post['image'] ?>"/></div>
                                <div class="desc-holder">
                                <?php } ?>
                                    <h4><a href="<?= $post['post_text']?>" target="_blank"><?= $post['link_title'] ?></a></h4>
                                    <p><?= $post['link_description'] ?></p>
                                <?php if($post['image'] != 'No Image'){ ?>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <a class="overlay-link postdetail-link popup-modal" href="javascript:void(0)" id="MobilePost" data-postid="<?php echo $post['_id'];?>">&nbsp;</a>
                            <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                                $post['post_type'] = 'text and image';
                            }?>
                            <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                $cnt = 1;
                                $eximgs = explode(',',$post['image'],-1);
                                if(isset($post['trav_item']) && $post['trav_item']== '1')
                                {
                                    if($post['image'] == null)
                                    {
                                        $eximgs[0] = '/uploads/travitem-default.png';
                                    }
                                    $eximgss[] = $eximgs[0];
                                    $eximgs = $eximgss;                                     
                                }
                                $totalimgs = count($eximgs);
                                $imgcountcls="";
                                if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                if($totalimgs > '5'){$imgcountcls = 'more-img';}
                            ?>
                            <div class="post-img-holder">
                                <div class="post-img <?= $imgcountcls?> lgt-gallery lgt-gallery-photo dis-none">
                                    <?php
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                            <?php if($cnt == 5 && $totalimgs > 5){?>
                                                <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                            <?php } ?>
                                        </a>
                                    <?php } $cnt++; } ?>
                                </div>
                                <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                    if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                        <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                    <?php } else { 
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $image = '../web/uploads/gallery/'.$iname;
                                        $JIDSsdsa = '';           
                                        $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                        if(!empty($pinit)) {
                                            $JIDSsdsa = 'active';                    
                                        }
                                        ?>
                                        <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                    <?php 
                                    } 
                                } 
                                ?>
                            </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'image' && $post['is_coverpic'] == '1' && file_exists('uploads/cover/'.$post['image'])) { ?>
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('uploads/cover/'.$post['image']);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);

                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                        
                                    <div class="post-img-holder">
                                        <div class="lgt-gallery lgt-gallery-photo post-img one-img dis-none">
                                              <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                                </a>
                                        </div>
                                    </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'profilepic' && file_exists('profile/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="lgt-gallery lgt-gallery-photo post-img one-img dis-none">
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('profile/'.$post['image']);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            
                        </div>
                        <?php } else { ?>
                        <div class="post-content <?php if($post['image'] != null) { ?>hasimg<?php } ?>">
                            <div class="post-details">
                                    <?php if($post['post_title'] != null) { ?>
                                    <div class="post-title"><?=$post['post_title']?></div>
                                    <?php } ?>
                                    <div class="rating-stars">
                                    <?php for($i=0;$i<5;$i++)
                                    { ?>
                                        <i class="mdi mdi-star <?php if($i < $post['rating']){ ?>active<?php } ?>"></i>
                                    <?php }
                                    ?>
                                    </div>
                                    <div class="post-desc">
                                            <?php if($post['post_text'] == null) { $post['post_text'] = 'Not added'; } ?>
                                            
                                            <?php if(strlen($post['post_text'])>187){ ?>
                                                <div class="para-section">
                                                    <div class="para">
                                                        <p><?= $post['post_text'] ?></p>
                                                    </div>
                                                    <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                </div>
                                            <?php }else{ ?>                                     
                                                <p><?= $post['post_text'] ?></p>
                                            <?php } ?>                                             
                                    </div>
                            </div>
                            <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                <?php if($post['image'] != null) {
                                    $eximgs = explode(',',$post['image'],-1);
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}
                                ?>
                                <div class="post-img-holder">
                                    <div class="lgt-gallery lgt-gallery-photo post-img one-img dis-none">
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                                <?php } } } ?>
                            
                        </div>
                        <?php } } ?>
                        <div class="clear"></div>
                        <div class="post-data">
                            <div class="post-actions">
                            <div class="right like-tooltip">
                                <?php
                                    $like_active = '';
                                    $post_id = $post['_id'];
                                    $like_names = Like::getLikeUserNames((string)$post['_id']);
                                    $like_buddies = Like::getLikeUser((string)$post['_id']);
                                    $newlike_buddies = array();
                                    foreach($like_buddies as $like_buddy) {
                                    $like_active = Like::find()->where(['post_id' => "$post_id",'status' => '1','user_id' => (string) $user_id])->one();
                                        if(!empty($like_active)) {
                                            $like_active = 'active';
                                        } else {
                                            $like_active = '';
                                        }
                                        $newlike_buddies[] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
                                    }
                                    $newlike_buddies = implode('<br/>', $newlike_buddies);
                                ?>
                                <a href="javascript:void(0)" onclick="doLike('<?=$post['_id']?>');" class="pa-like liveliketooltip liketitle_<?=$post['_id']?> <?=$like_active?>"  data-title='<?=$newlike_buddies?>'>
                                    <i class="zmdi zmdi-thumb-up"></i>
                                </a>
                                <span class="tooltip_content lcount likecount_<?=$post['_id']?>"> <?php if($like_count >0 ) { echo $like_count; } ?></span>
                                <?php if($post['comment_setting'] != 'Disable'){?>

                                <a href="javascript:void(0)" class="pa-comment <?=$Auth?>" data-id="<?php echo $post['_id'];?>" title="Comment">
                                    <i class="zmdi zmdi-comment"></i>
                                </a>
                                <?php if((count($comments)) > 0){ ?>
                                <span class="comment-lcount commentcountdisplay_<?=$post['_id']?>">
                                    <?php echo count($comments);?>
                                </span>
                                <?php } ?>

                                <?php } ?>
                                <?php 
                                $notifica = Notification::find()->where(['notification_type' => 'editpostuser','post_id' => (string)$post['_id']])->one();
                                if($notifica && $status == '10' && $post['is_deleted'] == '2')
                                { ?>
                                    <a id="publishposticon" href="javascript:void(0)" onclick="publishpost('<?php echo $post['_id'];?>')" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                <?php } ?>
                            </div>
                            </div>
                            <div class="comments-section panel">
                            <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                            <div class="comments-area">
                            <?php } ?>
                            <?php if(count($init_comments)>0){ ?>
                            <div class="post-more">
                                <?php if(count($init_comments)>2){ ?>
                                <a href="javascript:void(0)" class="view-morec-<?php echo $post['_id']; ?>" onclick="showPreviousComments('<?php echo $post['_id'];?>')">View more comments <i class="zmdi zmdi mdi-chevron-right trun90"></i></a>
                                <?php } ?>
                                <span class="total-comments commment-ctr-<?php echo $post['_id'];?>" id="commment-ctr-<?php echo $post['_id'];?>"><font class="ctrdis_<?=$post['_id']?>"><?php echo count($init_comments);?></font> of <font class="countdisplay_<?=$post['_id']?>"><?php echo count($comments);?></font></span>
                            </div>
                            <?php } ?>
                            <input type="hidden" name="from_ctr" id="from_ctr_<?php echo $post['_id'];?>" value="<?php echo count($init_comments);?>">      
                            <input type="hidden" name="to_ctr" id="to_ctr_<?php echo $post['_id'];?>" value="<?php echo count($comments);?>">

                            <div class="post-comments post_comment_<?=$post['_id']?>">                     
                                <div class="pcomments sub_post_comment_<?=$post['_id']?>">                                    
                                    <?php
                                    if(count($init_comments)>0) { 
                                        foreach($init_comments as $init_comment) {
                                            $rand = rand(999, 9999);
                                            $cmnt_id = (string) $init_comment['_id'];
                                            $cmnt_like_active= '';
                                            $cmnt_like_active = Like::find()->where(['comment_id' => $cmnt_id,'status' => '1','user_id' => (string) $user_id])->one();
                                            if(!empty($cmnt_like_active))
                                            {
                                                $cmnt_like_active = 'active';
                                            }
                                            else
                                            {
                                                $cmnt_like_active = '';
                                            }
                                            $comment_time = Yii::$app->EphocTime->comment_time(time(),$init_comment['updated_date']);$commentcount =Comment::getCommentReply($init_comment['_id']);
                                            $hidecomments = new HideComment();
                                            $hidecomments = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                            $hide_comments_ids = explode(',',$hidecomments['comment_ids']);
                                            if(!(in_array($init_comment['_id'],$hide_comments_ids)))
                                            {
                                                if(($userid == $post['post_user_id']) || ($userid == $init_comment['user']['_id']))
                                                {
                                                    $afun_post = 'deleteComment';
                                                    $atool_post = 'Delete';
                                                }
                                                else
                                                {
                                                    $afun_post = 'hideComment';
                                                    $atool_post = 'Hide';
                                                }
                                    ?>
                                    <div class="pcomment-holder <?php if(count($commentcount) > 0){ ?>has-comments<?php } ?>" id="comment_<?php echo $init_comment['_id']?>">
                                        <div class="pcomment main-comment">
                                            <div class="img-holder">
                                                <div id="commentptip-4" class="profiletipholder">
                                                    <span class="profile-tooltip">
                                                        <?php $init_comment_img = $this->getimage($init_comment['user']['_id'],'thumb'); ?>
                                                        <img class="circle" src="<?= $init_comment_img?>"/>
                                                    </span>
                                                </div>
                                            </div> 
                                            <div class="desc-holder">
                                                <div class="normal-mode">
                                                    <div class="desc">
                                                        <a href="<?php $id = $init_comment['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>" class="userlink"><?php echo ucfirst($init_comment['user']['fname']).' '.ucfirst($init_comment['user']['lname']);?></a>
                                                        <?php if(strlen($init_comment['comment'])>200){ ?>
                                                            <p class="shorten" data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?>
                                                            <a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                            </p>
                                                        <?php }else{ ?>                                                     
                                                            <p data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="comment-stuff">
                                                        <div class="more-opt">
                                                            <span id="likeholder-7" class="likeholder">
                                                            <?php

                                                                 
                                                                $like_count = Like::find()->where(['comment_id' => (string) $init_comment['_id']  ,'status' => '1'])->all();

                                                                $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $init_comment['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                                $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                                $usrbox = array();
                                                                foreach ($comlikeuseinfo as $key => $single) {
                                                                    $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                                     $usrbox[(string)$single['_id']] = $fullnm; 
                                                                }
                                                                
                                                                $newlike_buddies = implode("<br/>", $usrbox);
                                                            ?>

                                                                    <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$init_comment['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $init_comment['_id']?>')">
                                                                    <i class="zmdi zmdi-thumb-up"></i>
                                                                    </a>
                                                            </span> 
                                                            <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply"></i></a>
                                                            
                                                            <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleteDisc1<?=$rand?>">
                                                                    <i class="mdi mdi-dots-vertical"></i>
                                                                </a>   
                                                                
                                                                <?php if($status == '10') { ?> 
                                                                <ul class="dropdown-content" id="dropdown-editdeleteDisc1<?=$rand?>">
                                                                   <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')">Flag</a></li></ul>
                                                                <?php } else { ?>
                                                                <ul class="dropdown-content" id="dropdown-editdeleteDisc1<?=$rand?>">
                                                                    <?php if(($userid != $post['post_user_id']) && ($userid != $init_comment['user']['_id'])){?>
                                                                        <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$atool_post?></a></li>
                                                                        <?php } else { ?>
                                                                        <?php if($userid == $init_comment['user']['_id']){ ?>
                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                        <?php } ?>
                                                                        <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>','<?=$post['_id']?>')">Delete</a></li>                                                  
                                                                    <?php } ?>
                                                                </ul>
                                                                <?php } ?>
                                                                
                                                            </div>
                                                        </div>
                                                        <div class="less-opt"><div class="timestamp"><?php echo $comment_time?></div></div>
                                                    </div>
                                                </div>
                                                <div class="edit-mode">
                                                    <div class="desc">
                                                        <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline" data-id="<?php echo $init_comment['_id'];?>" id="edit_comment_<?php echo $init_comment['_id'];?>"><?php echo $init_comment['comment']?></textarea>
                                                        <a href="javascript:void(0)" class="btn btn-primary btn-sm editcomment-cancel waves-effect waves-light">Close</a>
                                                    </div>                                                                          
                                                </div>
                                            </div>                              
                                        </div>  
                                        <div class="clear"></div>
                                        <div class="comment-reply-holder reply_comments_<?php echo $init_comment['_id'];?>">
                                            <?php $comment_replies =Comment::getCommentReply($init_comment['_id']);
                                                if(count($comment_replies)>0) {
                                                $lastcomment = Comment::find()->where(['parent_comment_id' => (string)$init_comment['_id']])->orderBy(['updated_date'=>SORT_DESC])->one();
                                                $last_comment_time = Yii::$app->EphocTime->comment_time(time(),$lastcomment['updated_date']);
                                            ?>
                                            <div class="comments-reply-summery">
                                                <a href="javascript:void(0)" onclick="openReplies(this)">
                                                    <i class="mdi mdi-share"></i>
                                                    <?php echo count($comment_replies); 
                                                    if(count($comment_replies)>1) { echo " Replies"; }
                                                    else { echo " Reply"; } ?>
                                                </a>
                                                <i class="mdi mdi-bullseye dot-i"></i>
                                                <?php echo $last_comment_time;?>
                                            </div>
                                            <?php }
                                            if(!empty($comment_replies))
                                            {
                                                foreach($comment_replies AS $comment_reply) { 
                                                $hidecomment = new HideComment();
                                                $hidecomment = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                $hide_comment_ids = explode(',',$hidecomment['comment_ids']); 
                                                if(!(in_array($comment_reply['_id'],$hide_comment_ids)))
                                                {
                                                    if(($userid == $post['post_user_id']) || ($userid == $comment_reply['user']['_id']))
                                                    {
                                                        $bfun_post = 'deleteComment';
                                                        $btool_post = 'Delete';
                                                    }
                                                    else
                                                    {
                                                        $bfun_post = 'hideComment';
                                                        $btool_post = 'Hide';
                                                    }
                                                $comment_time = Yii::$app->EphocTime->comment_time(time(),$comment_reply['updated_date']);

                                                $like_count = Like::find()->where(['comment_id' => (string) $comment_reply['_id']  ,'status' => '1'])->all();

                                                $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $comment_reply['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                $usrbox = array();
                                                foreach ($comlikeuseinfo as $key => $single) {
                                                $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                $usrbox[(string)$single['_id']] = $fullnm; 
                                                }

                                                $newlike_buddies = implode("<br/>", $usrbox)

                                        ?>
                                        <div class="comments-reply-details">
                                            <div class="pcomment comment-reply" id="comment_<?php echo $comment_reply['_id']?>">
                                                <div class="img-holder">
                                                    <div class="profiletipholder" id="commentptip-6">
                                                        <span class="profile-tooltip tooltipstered">
                                                            <img class="circle" src="<?php echo $this->getimage($comment_reply['user']['_id'],'thumb'); ?>">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="desc-holder">
                                                    <div class="normal-mode">
                                                        <div class="desc">
                                                            <a class="userlink" href="<?php $id = $comment_reply['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>"><?php echo ucfirst($comment_reply['user']['fname']).' '.ucfirst($comment_reply['user']['lname']);?></a>
                                                            
                                                            <?php if(strlen($comment_reply['comment'])>200){ ?>
                                                                <p class="shorten" data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?><a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                </p>
                                                            <?php }else{ ?>                                                         
                                                                <p data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?></p>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="comment-stuff">
                                                            <div class="more-opt">
                                                                    <span class="likeholder" id="likeholder-6">
                                                                    <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$comment_reply['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $comment_reply['_id']?>')">
                                                                    <i class="zmdi zmdi-thumb-up"></i>
                                                                    </a>
                                                                    </span>
                                                                    <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                                                <i class="mdi mdi-dots-vertical"></i>
                                                                            </a>
                                                                            
                                                                            <ul class="dropdown-menu">
                                                                                <?php if(($userid != $post['post_user_id']) && ($userid != $comment_reply['user']['_id'])){?>
                                                                                    <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$btool_post?></a></li>
                                                                                    <?php } else { ?>
                                                                                    <?php if($userid == $comment_reply['user']['_id']){ ?>
                                                                                    <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                    <?php } ?>
                                                                                    <li><a href="javascript:void(0)" class="delete-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')">Delete</a></li>                                                  
                                                                                <?php } ?>
                                                                            </ul>
                                                                    </div>
                                                            </div>
                                                            <div class="less-opt"><div class="timestamp"><?= $comment_time;?></div></div>
                                                        </div>  
                                                    </div>
                                                    <div class="edit-mode">
                                                        <div class="desc">
                                                            <div class="sliding-middle-custom anim-area tt-holder underlined fullwidth">
                                                                <textarea class="editcomment-tt materialize-textarea mb0 md_textarea item_tagline" data-id="<?php echo $comment_reply['_id'];?>" id="edit_comment_<?php echo $comment_reply['_id'];?>"><?php echo $comment_reply['comment']?></textarea>
                                                            </div>
                                                            <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px   "></i></a>
                                                        </div>                                                                          
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } } } ?>
                                        </div>
                                        
                                        <div class="comment-reply-holder comment-addreply">                                 
                                            <div class="addnew-comment comment-reply">                          
                                                <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $this->getimage($result['_id'],'thumb');?>"/></a></div>
                                                <div class="desc-holder">                                   
                                                    <div class="sliding-middle-custom anim-area">
                                                        <textarea name="reply_txt" placeholder="Write a reply" data-postid="<?php echo $post['_id'];?>" data-commentid="<?php echo $init_comment['_id']?>" id="reply_txt_<?php echo $init_comment['_id'];?>" class="reply_class capitalize materialize-textarea mb0 md_textarea item_tagline"></textarea>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                    <?php  } } } ?>
                                </div>
                                <?php if($post['comment_setting'] != 'Disable'){
                                if(isset($post['trav_item']) && $post['trav_item']== '1')
                                {
                                    $comment_placeholder = "Ask question or send query";
                                }
                                else{
                                    $comment_placeholder = "Write a comment";
                                }   
                                ?>
                                <div class="addnew-comment valign-wrapper">
                                    <?php $comment_image = $this->getimage($result['_id'],'thumb'); ?>
                                    <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $comment_image?>"/></a></div>
                                    <form name="imageCommentForm" id="imageCommentForm" enctype="multipart/form-data">
                                        <div class="desc-holder">                                   
                                            <div class="sliding-middle-custom anim-area">
                                                <textarea data-postid="<?php echo $post['_id'];?>" id="comment_txt_<?php echo $post['_id'];?>" placeholder="<?= $comment_placeholder;?>" class="comment_class capitalize materialize-textarea mb0 md_textarea item_tagline"></textarea>
                                            </div>
                                        </div>
                                        <?php if($existing_posts == ''){ ?>
                                            <input type="hidden" id="last_inserted" name="last_inserted" value="<?php echo $post['_id']; ?>"/>
                                        <?php } ?>
                                    </form>
                                </div>
                                <?php } ?>
                            </div>
                            <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                            </div>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
        <?php }    
            }      
        }
    }
            
    public function display_last_tip($last_insert_id, $existing_posts = '', $imagefullarray = '', $cls='', $total='', $tempocls='')
    {
        $rand = rand(999, 99999).time();
        $session = Yii::$app->session;
        $userid = $user_id = (string)$session->get('user_id');
        $email = $session->get('email');
        $status = $session->get('status');
        $result = LoginForm::find()->where(['_id' => $userid])->one();
        $date = time();
        if($cls == 'places-ads') {
            $post = PostForm::find()->where([(string)'_id' => $last_insert_id])->one();
            $tableLabel = 'PostForm';
        } else {
            $post = PlaceTip::find()->where(['_id' => $last_insert_id])->andWhere(['not','flagger', "yes"])->one();
            $tableLabel = 'PlaceTip';
        }
        $reportpost = ReportPost::find()->where(['post_id' => (string)$last_insert_id,'reporter_id' => (string)$user_id])->one();
        $profile_tip_counter = 1;
          
        
        $Auth = '';
        if(isset($userid) && $userid != '') 
        {
            $authstatus = UserForm::isUserExistByUid($userid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
            {
                $Auth = $authstatus;
            }
        }   
        else    
        {
            $Auth = 'checkuserauthclassg';
        }
        $my_post_view_status = $post['post_privacy'];
        if($my_post_view_status == 'Private') {$post_dropdown_class = 'lock';}
        else if($my_post_view_status == 'Connections') {$post_dropdown_class = 'account';}
        else if($my_post_view_status = 'Public') {$post_dropdown_class = 'earth';}
        if(!($reportpost))
        {
            $postprivacy = $post['post_privacy'];
            $postownerid = (string)$post['post_user_id'];
            $suserid = (string)$user_id;
            $is_connect = Connect::find()->where(['from_id' => $postownerid,'to_id' => $suserid,'status' => '1'])->one();
            if(($postprivacy == 'Public') || ($postprivacy == 'Connections' && ($is_connect || $postownerid == $suserid)) || ($postprivacy == 'Private' && $postownerid == $suserid))
            {
                $block = BlockConnect::find()->where(['user_id' => "$postownerid"])->andwhere(['like','block_ids',$suserid])->one();
                if(!$block)
                {
                $savestatus = new SavePost();
                $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => (string)$post['_id']])->one();
                $savestatuscalue = $savestatus['is_saved'];
                if($savestatuscalue == '1'){$savevar = 'Unsave';}else{$savevar = 'Save';}

                $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
                $like_count = Like::getLikeCount((string)$post['_id']);
                $comments = Comment::getAllPostLike($post['_id']);
                $init_comments = Comment::getFirstThreePostComments($post['_id']);
                $post_privacy = $post['post_privacy'];
                if($post_privacy == 'Private') {$post_class = 'lock';}
                else if($post_privacy == 'Connections') {$post_class = 'account';}
                else if($post_privacy = 'Public') {$post_class = 'earth';}
                
                // START add tag content in post
                $posttag = '';
                if(isset($post['post_tags']) && !empty($post['post_tags']))
                {
                    $posttag = explode(",", $post['post_tags']);
                }

                $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');

                $nkTag = array();
                $nvTag = array();

                $i=1;
                foreach ($taginfomatiom as $key => $value)
                {
                    $nkTag[] = (string)$value; 
                    $nvTag[] = $key;
                    if($i != 1) {
                        $content[] = $key;
                    }
                    $i++;
                }

                if(isset($content) && !empty($content))
                {
                    $content = implode("<br/>", $content); 
                }

                $tagstr = '';
                if(!empty($taginfomatiom))
                {
                    if(count($taginfomatiom) > 1) {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
                    } else {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
                    }
                }
                if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                {
                    $page_details = Page::Pagedetails($post['page_id']);
                    $review = " reviewed <a href=".Url::to(['page/index', 'id' => $post['page_id']]) ." class='sub-link'>" . $page_details['page_name'] . "</a>"; }
                else { $review = ''; }
                // END add tag content in post
                $profile_tip_counter++;         
                if(isset($post['currentlocation']) && !empty($post['currentlocation']))
                {
                    $current_location = $this->getshortcityname($post['currentlocation']);
                    $default = 'at';
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='reviews'){$default = 'reviewed';}
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='tip'){$default = 'tip for';}
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='ask'){$default = 'has a question about';}
                    $locationtags = $tagstr . ' '.$default.' <a class="sub-link" href="javascript:void(0)">'.$current_location.'</a>';
                }
                else{ $locationtags = $tagstr . ''; }
                if($post['is_timeline'] == '1')
                {
                    $getrateid = PlaceTip::find()->where(['_id' => $post['parent_post_id']])->andWhere(['not','flagger', "yes"])->one();
                    $getratedid = $getrateid['rating'];
                    if($getratedid == null){$getratedid = '6';}
                }
                else
                {
                    $getratedid = '6';
                }
                if((isset($post['is_ad']) && !empty($post['is_ad']) && $post['is_ad'] == '1'))
                {
                    $is_ad = true;
                    $adclass = 'travad-box ';
                    if($post['adobj'] == 'pagelikes'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'brandawareness'){$adclass .= 'brand-ad';}
                    else if($post['adobj'] == 'websiteleads'){$adclass .= 'weblink-ad';}
                    else if($post['adobj'] == 'websiteconversion'){$adclass .= 'actionlink-ad';}
                    else if($post['adobj'] == 'pageendorse'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'eventpromo'){$adclass .= 'event-ad';}
                    else{$adclass .= '';}
                }
                else{ $is_ad = false; $adclass = '';}
        ?>
        <style> 
        .pac-container
        {
            z-index: 1051 !important;
        }
        </style>
            <div class="<?=$tempocls .' '.$cls?> post-holder <?php if($existing_posts != 'from_save'){ ?>bshadow<?php } ?> <?php if((isset($post['is_page_review']) && !empty($post['is_page_review']) || ($getratedid != '6'))){ ?>reviewpost-holder<?php } ?> <?php if((isset($post['trav_item']) && !empty($post['trav_item']))){ ?>travelstore-ad<?php } ?> <?php if((isset($post['is_ad']) && !empty($post['is_ad']))){ echo $adclass; } ?> post_user_id_<?php echo $post['post_user_id'];?>" id="hide_<?php echo $post['_id'];?>">
            <?php
            $getuserinfo = LoginForm::find()->where(['_id' => $post['post_user_id']])->one();
            $personalinfo = Personalinfo::find()->where(['user_id' => $post['post_user_id']])->one();
            if($post['user']['status']=='44')
            {
                $dp = $this->getpageimage($post['user']['_id']);
                $link = Url::to(['page/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'page';
            }
            else
            {
                $dp = $this->getimage($getuserinfo['_id'],'photo');
                $link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'userwall';
            }
            if(isset($getuserinfo['cover_photo']) && !empty($getuserinfo['cover_photo']))
            {
                $cover = $getuserinfo['cover_photo'];
                if(strstr($cover,'cover'))
                {
                    $cvr = substr_replace($getuserinfo['cover_photo'], '-thumb.jpg', -4);
                    $cover = '../uploads/cover/thumbs/'.$cvr;
                }
                else
                {
                    $cover = '../uploads/cover/thumbs/thumb_'.$getuserinfo['cover_photo'];
                }
            }
            else
            {
                $cover = 'cover.jpg';
            }
            $dpforpopup = $this->getimage($post['user']['_id'],'thumb');
            $mutualcount = Connect::mutualconnectcount($getuserinfo['_id']);

            $po_id = $post['post_user_id'];
            $isconnect = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '1'])->one();
            $isconnectrequestsent = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '0'])->one();
            ?>
            <?php if($is_ad && $post['adobj'] != 'travstorefocus'){
                $this->display_ad($post['_id'],'topbar');
            } else { ?>
                <div class="post-topbar">
                    <div class="post-userinfo">
                        <div class="img-holder">
                            <div  class="profiletipholder">
                                <span id="profiletip-<?= $post['_id']?>" class="profile-tooltip">
                                    <?php
                                    if($post['is_timeline']=='1')
                                    {
                                        $dpimg = $this->getimage($post['shared_by'],'photo');
                                    }
                                    else
                                    {
                                        $dpimg = $this->getimage($post['user']['_id'],'photo');
                                    }
                                    ?>
                                    <img class="circle" src="<?= $dpimg?>">
                                </span>
                               <?php if($existing_posts != 'from_save'){ ?>
                                    <span class="profiletooltip_content profiletooltip_content_last">
                                        <div class="profile-tip slidingpan-holder dis-none" id="dispatchcontentprofiletip-<?= $post['_id']?>">
                                           <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive"  src="<?= $dp?>">
                                                <div class="sliding-pan location-span">
                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                            </div>
                                            <div class="profile-tip-name">
                                                <a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a>
                                            </div>
                                            <?php if($post['user']['status']!='44'){ ?>
                                            <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                        <?php if($userid == $po_id){ ?>
                                                        <a href="javascript:void(0)">&nbsp;</a>
                                                        <?php } else if($isconnect) { ?>
                                                        <a href="javascript:void(0)"><?=$mutualcount?> Mutual connections</a>
                                                        <?php } else if($isconnectrequestsent) { ?>
                                                        <i title="Connect request sent" class="mdi mdi-account-minus"></i>
                                                        <?php } else { ?>
                                                        <a href="javascript:void(0)" title="Add Connect" class="title_<?php echo $po_id;?>">
                                                        <i class="mdi mdi-account-plus people_<?php echo $po_id;?>" onclick="addConnect('<?php echo $po_id;?>')"></i>
                                                        <i class="mdi mdi-account-minus dis-none sendmsg_<?php echo $po_id;?>"></i>
                                                        <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
                                                        </a>
                                                        <?php } ?>
                                                </div>
                                                <div class="profiletip-icon">
                                                        <a href="<?php echo $link;?>" title="View Profile"><i class="mdi mdi-eye"></i></a>
                                                </div>                  
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="desc-holder">
                            <?php
                                if($post['is_timeline']=='1') 
                                {
                                    $getsharename = LoginForm::find()->where(['_id' => $post['shared_by']])->one();
                                    if(strstr($post['parent_post_id'],'event_'))
                                    {
                                        $eventdetails = PageEvents::getEventdetailsshare(substr($post['parent_post_id'],6));
                                        $eid = $eventdetails['_id'];
                                        $ename = $eventdetails['event_name'];
                                        $elink = Url::to(['event/detail', 'e' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s event";
                                    }
                                    else if(strstr($post['parent_post_id'],'page_'))
                                    {
                                        $pagedetails = Page::Pagedetails(substr($post['parent_post_id'],5));
                                        $eid = $pagedetails['page_id'];
                                        $ename = $pagedetails['page_name'];
                                        $elink = Url::to(['page/index', 'id' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s page";
                                    }
                            ?>
                            <a href="<?php $id =  $getsharename['_id']; echo Url::to([$linkwall.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($getsharename['fname']).' '.ucfirst($getsharename['lname']) ;?></a><?= $review?>
                            <?php if($post['post_user_id'] != $post['shared_by']) { ?>
                            <div class="user-to">
                                <i class="mdi mdi-menu-right"></i>
                                <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            </div>
                            <?php } ?>
                            <?php if(isset($locationtags) && !empty($locationtags)) { ?>
                                <?= $locationtags?>
                            <?php } } else { ?>
                            <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            <?php if((empty($post['trav_item']))){ ?><?= $locationtags?><?php } ?>
                            <?php if($post['post_type']=='profilepic'){ ?>
                            updated profile photo.
                            <?php } else if($post['is_coverpic']=='1'){ ?>
                            updated cover photo.
                            <?php } ?>
                            <?php } ?>
                            <span class="timestamp"><?php echo $time; ?>
                            <?php /*<span class="mdi mdi-<?= $post_class?> mdi-13px"></span> */ ?>
                            </span>
                        </div>
                    </div>
                    <?php
                    $p = 'post';
                    if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                    {
                        $p = 'review';
                    }
                    if(isset($post['placetype']) && !empty($post['placetype']))
                    {
                        $p = $post['placetype'];
                        if($p == 'reviews'){$p = 'review';}
                        if($p == 'ask'){$p = 'question';}
                    }
                    ?>
                    <div class="settings-icon">
                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                            <a class="dropdown-button <?=$Auth?> directcheckuserauthclass" href="javascript:void(0)" data-activates="dropdown-edittip<?=$rand?>">
                               <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                            </a>
                            <ul class="dropdown-content custom_dropdown" id="dropdown-edittip<?=$rand?>">
                                <?php if($status == '10') { ?> 
                                <li><a href="javascript:void(0)" data-id="<?=(string)$post['_id']?>" data-module="<?=$p?>" class="customreportpopup-modal reportpost-link" onclick="flagpost(this)"></span>Flag <?=$p?></a></li>
                                <?php } else {?>  
                                <li class="nicon"><a href="javascript:void(0)" class="hidepost" onclick="hidePost('<?php echo $post['_id']?>')">Hide <?=$p?></a></li>
                                <li class="nicon"><a href="javascript:void(0)" class="savepost-link" id="save_post_<?php  echo $post['_id'];?>" data-pid="<?php  echo $post['_id'];?>" onclick="savePost('<?php echo $post['_id']?>','<?php echo $post['post_type']?>','<?=$savevar?>','tip')"><?=$savevar?> <?=$p?></a></li>
                                    <input type="hidden" class="tbsp_<?=$post['_id']?>" value="<?=$savevar?>">
                                <li class="nicon"><a href="javascript:void(0)" onclick="turn_off_notification('<?=$post['_id'] ?>', 'tip')" class="turnnotpost_<?=$post['_id'] ?>">
                                <?php
                                $turnoff = TurnoffNotification::find()->where(['user_id' => "$user_id"])->andwhere(['like','post_ids',$post['_id']])->one();
                                if($turnoff) { 
                                    $turnoffvalue = "Unmute this tip notification";
                                } else { 
                                    $turnoffvalue = "Mute this tip notification";
                                }                                
                                ?>
                                <?=$turnoffvalue?>
                                </a></li>    
                                <?php if($post['user']['status']=='44' && ($post['is_coverpic']=='1' || $post['is_profilepic']=='1')) { $page_details = Page::Pagedetails($post['post_user_id']); $pageownerid = $page_details['created_by']; }
                                else { $pageownerid = $post['post_user_id']; } ?>
                                <?php if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                <li class="nicon"><a href="javascript:void(0)" class="deletepost-link" onclick="deleteTip('<?php echo $post['post_user_id']?>','<?php echo $post['_id']?>')">Delete <?=$p?></a></li>
                                <?php if(($post['is_coverpic']!='1' && $post['is_profilepic']!='1' && $post['shared_from']!='shareentity')){ ?>
                                <li class="nicon"><a href="javascript:void(0)" data-editpostid="<?php echo $post['_id'];?>" class="customeditpopup-modal-tip editpost-link">Edit <?=$p?></a></li>
                                <?php } ?>
                                <?php } ?>
                                <?php if(($userid != $post['post_user_id']) && ($user_id != $post['shared_by']) && ($user_id != $pageownerid)){
                                $muteuser = UnfollowConnect::find()->where(['user_id' => "$user_id"])->andwhere(['like','unfollow_ids',$post['post_user_id']])->one();
                                if($muteuser) { $mutevalue = 'Unmute this member posts';}
                                else { $mutevalue = 'Mute this member posts';}
                                ?>
                                <li class="nicon"><a href="javascript:void(0)" onclick="unfollowConnect('<?php echo $post['post_user_id']?>')"><?=$mutevalue?></a></li>
                                <li class="nicon"><a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Report post</a></li>
                                <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>          
                        <?php
                        if($post['is_timeline']=='1' && isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                        {
                        ?>
                            <div class="post-content">
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                <?php if($post['post_text'] != ''){ ?>
                                        <div class="post-desc">
                                            <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                            
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                    </div>
                                                <?php }else{ ?>                                     
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>
                                                
                                            <?php } ?>
                                        </div>
                                  <?php } ?>
                                    </div>
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                </div>
                            </div>
                            <?php
                            if(isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                            {
                                if(strstr($post['parent_post_id'],'event_'))
                                {
                                    $this->view_event((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'page_'))
                                {
                                    $this->view_page((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'collection_'))
                                {
                                    $this->view_collection((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'trip_'))
                                {
                                    $this->view_trip((string)$post['parent_post_id']);
                                }
                                else
                                {
                                    $this->view_post_id((string)$post['parent_post_id']);
                                }
                            }
                        } else if($post['is_trip'] == '1'){
                        ?>
                            <div class="post-content tripexperince-post">
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                        <div class="post-title">
                                            <?= $post['post_title'];?>
                                        </div>
                                    </div>
                                    <div class="trip-summery">
                                        <div class="location-info raani">
                                            <h5><i class="zmdi zmdi-pin"></i>
                                            <?php  
                                            $currentlocation = trim($post['currentlocation']);

                                            if($currentlocation != '') {
                                                $currentlocation = explode(",", $currentlocation);
                                                if(count($currentlocation) >1) {
                                                    $first = reset($currentlocation);
                                                    $last = end($currentlocation);
                                                    $currentlocation = $first.', '.$last;
                                                } else {
                                                    $currentlocation = implode(", ", $currentlocation);
                                                }
                                            }

                                            echo $currentlocation;
                                            ?></h5>
                                            <i class="mdi mdi-menu-right"></i>
                                            <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                        </div>
                                        <!--<div class="views-info"><i class="mdi mdi-eye"></i>45 Views</div>-->
                                    </div>
                                    <div class="map-holder dis-none">
                                        <iframe width="600" height="450" frameborder="0" src="https://maps.google.it/maps?q=<?= $post['currentlocation'];?>&output=embed"></iframe>
                                    </div>
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                </div>
                                <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                    $cnt = 1;
                                    $eximgs = explode(',',$post['image'],-1);
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        if($post['image'] == null)
                                        {
                                            $eximgs[0] = '/uploads/travitem-default.png';
                                        }
                                        $eximgss[] = $eximgs[0];
                                        $eximgs = $eximgss;                                     
                                    }
                                    $totalimgs = count($eximgs);
                                    $imgcountcls="";
                                    if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                    if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                    if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                    if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                    if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                    if($totalimgs > '5'){$imgcountcls = 'more-img';}
                                ?>
                                <div class="post-img-holder">
                                    <div class="lgt-gallery lgt-gallery-photo post-img <?= $imgcountcls?> dis-none">
                                        <?php
                                        foreach ($eximgs as $eximg) {
                                        if (file_exists('../web'.$eximg)) {
                                        $picsize = '';
                                        $val = getimagesize('../web'.$eximg);
                                        $picsize .= $val[0] .'x'. $val[1] .', ';
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                        if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                        if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                                <?php if($cnt == 5 && $totalimgs > 5){?>
                                                    <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                                <?php } ?>
                                            </a>
                                        <?php } $cnt++; } ?>
                                    </div>
                                    <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                        if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                            <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                        <?php } else { 
                                            $iname = $this->getimagename($eximg);
                                            $inameclass = $this->getimagefilename($eximg);
                                            $image = '../web/uploads/gallery/'.$iname;
                                            $JIDSsdsa = '';           
                                            $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                            if(!empty($pinit)) {
                                                $JIDSsdsa = 'active';                    
                                            }
                                            ?>
                                            <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                        <?php 
                                        } 
                                    } 
                                    ?>
                                </div>
                                <?php } ?>
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                    <?php 
                                    if($post['post_text'] != ''){ ?>
                                            <div class="post-desc">
                                                <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                                    <?php if(strlen($post['post_text'])>187){ ?>
                                                        <div class="para-section">
                                                            <div class="para">
                                                                <p><?= $post['post_text'] ?></p>
                                                            </div>
                                                            <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                        </div>
                                                    <?php } else { ?>                                       
                                                        <p><?= $post['post_text'] ?></p>
                                                    <?php } ?>
                                                
                                                <?php } ?>
                                            </div>
                                      <?php } ?>
                                    </div>  
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                    
                                </div>
                            </div>
                        <?php   
                        } else if($is_ad && $post['adobj'] != 'travstorefocus'){
                            $this->display_ad($post['_id'],'content');
                        }
                        else
                        { if(!isset($post['is_page_review']) && empty($post['is_page_review'])) {
                        ?>
                        <div class="post-content">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                    <?php if($post['post_title'] != null) { ?>
                                    <div class="post-title"><?= $post['post_title'] ?></div>
                                    <?php } ?>
                                    <?php if(isset($post['placereview']) && !empty($post['placereview']) && $post['placetype']=='reviews'){ ?>
                                    <div class="rating-stars">
                                    <?php for($i=0;$i<5;$i++)
                                    { ?>
                                        <i class="mdi mdi-star <?php if($i < $post['placereview']){ ?>active<?php } ?>"></i>
                                    <?php }
                                    ?>
                                    </div>
                                    <?php } ?>
                                    <?php if($post['trav_price'] != null) { ?>
                                    <div class="post-price">$<?= $post['trav_price'] ?></div>
                                    <?php } ?>
                                    <?php if((isset($post['trav_item']) && !empty($post['trav_item']) && $post['currentlocation'] != null)){ ?>
                                        <div class="post-location" style="display:block"><i class="zmdi zmdi-pin"></i><?= $post['currentlocation'] ?></div>
                                    <?php } ?>
                                    <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                    <div class="post-desc">
                                    
                                        <?php if(strlen($post['post_text'])>187){ ?>
                                            <div class="para-section">
                                                <div class="para">
                                                    <p><?= $post['post_text'] ?></p>
                                                </div>
                                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                            </div>
                                        <?php }else{ ?>                                     
                                            <p><?= $post['post_text'] ?></p>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if($post['trav_item']== '1' && $post['is_ad']== '1'){
                                    $actbtn = $post['adbtn'];
                                    $link = $post['adurl'];
                                    if(!empty($link))
                                    {
                                        if(substr( $link, 0, 4 ) === "http")
                                        {
                                            $link = $link;
                                        }
                                        else
                                        {
                                            $link = 'https://'.$link;
                                        }
                                    }
                                    else
                                    {
                                        $link = 'Not added';
                                    }
                                ?>
                                <div class="post-paidad">
                                    <div class="travad-sponsor">Sponsered by <a href="<?=$link?>" target="blank"><?=$link?></a></div>
                                    <a class="travad-shop btn btn-primary btn-md">
                                        <?=$actbtn?>
                                    </a>
                                   </div>
                                <?php } ?>
                                
                                <?php if($post['post_type'] == 'link'){ ?>
                                <div class="pvideo-holder">
                                    <?php if($post['image'] != 'No Image'){ ?>
                                    <div class="img-holder"><img src="<?= $post['image'] ?>"/></div>
                                    <div class="desc-holder">
                                    <?php } ?>
                                        <h4><a href="<?= $post['post_text']?>" target="_blank"><?= $post['link_title'] ?></a></h4>
                                        <p><?= $post['link_description'] ?></p>
                                    <?php if($post['image'] != 'No Image'){ ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <a class="overlay-link postdetail-link popup-modal" href="javascript:void(0)" id="MobilePost" data-postid="<?php echo $post['_id'];?>">&nbsp;</a>
                            </div>
                            <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                                $post['post_type'] = 'text and image';
                            }?>
                            <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                $cnt = 1;
                                $eximgs = explode(',',$post['image'],-1);
                                if(isset($post['trav_item']) && $post['trav_item']== '1')
                                {
                                    if($post['image'] == null)
                                    {
                                        $eximgs[0] = '/uploads/travitem-default.png';
                                    }
                                    $eximgss[] = $eximgs[0];
                                    $eximgs = $eximgss;                                     
                                }
                                $totalimgs = count($eximgs);
                                $imgcountcls="";
                                if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                if($totalimgs > '5'){$imgcountcls = 'more-img';}
                            ?>
                            <div class="post-img-holder">
                                <div class="lgt-gallery lgt-gallery-photo post-img <?= $imgcountcls?> dis-none">
                                    <?php
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                            <?php if($cnt == 5 && $totalimgs > 5){?>
                                                <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                            <?php } ?>
                                        </a>
                                    <?php } $cnt++; } ?>
                                </div>
                                <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                    if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                        <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                    <?php } else { 
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $image = '../web/uploads/gallery/'.$iname;
                                        $JIDSsdsa = '';           
                                        $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                        if(!empty($pinit)) {
                                            $JIDSsdsa = 'active';                    
                                        }
                                        ?>
                                        <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                    <?php 
                                    } 
                                } 
                                ?>
                            </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'image' && $post['is_coverpic'] == '1' && file_exists('uploads/cover/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="lgt-gallery lgt-gallery-photo post-img one-img dis-none">
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('uploads/cover/'.$post['image']);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);

                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                          <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                            </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'profilepic' && file_exists('profile/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="lgt-gallery lgt-gallery-photo post-img dis-none">
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('profile/'.$post['image']);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            
                        </div>
                        <?php } else { ?>
                        <div class="post-content <?php if($post['image'] != null) { ?>hasimg<?php } ?>">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                        <?php if($post['post_title'] != null) { ?>
                                        <div class="post-title"><?=$post['post_title']?></div>
                                        <?php } ?>
                                        <div class="rating-stars">
                                        <?php for($i=0;$i<5;$i++)
                                        { ?>
                                            <i class="mdi mdi-star <?php if($i < $post['rating']){ ?>active<?php } ?>"></i>
                                        <?php }
                                        ?>
                                        </div>
                                        <div class="post-desc">
                                                <?php if($post['post_text'] == null) { $post['post_text'] = 'Not added'; } ?>
                                                
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                    </div>
                                                <?php }else{ ?>                                     
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>                                             
                                        </div>
                                </div>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                                <?php if($post['image'] != null) {
                                    $eximgs = explode(',',$post['image'],-1);
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}
                                ?>
                                <div class="post-img-holder">
                                    <div class="lgt-gallery lgt-gallery-photo post-img dis-none">
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                                <?php } } } ?>
                            
                        </div>
                        <?php } } ?>

                        <?php if($post['user']['status']=='44') { $page_details = Page::Pagedetails($post['page_id']); $pageownerid = $page_details['created_by']; }
                        else { $pageownerid = ''; } ?>
                        
                        <div class="clear"></div>
                        <div class="post-data">
                            <div class="post-actions">
                                <div class="right like-tooltip"> 
                                    <?php
                                        $like_active = '';
                                        $post_id = $post['_id'];
                                        $like_names = Like::getLikeUserNames((string)$post['_id']);
                                        $like_buddies = Like::getLikeUser((string)$post['_id']);
                                        $newlike_buddies = array();
                                        foreach($like_buddies as $like_buddy) {
                                        $like_active = Like::find()->where(['post_id' => "$post_id",'status' => '1','user_id' => (string) $user_id])->one();
                                        if(!empty($like_active))
                                        {
                                            $like_active = 'active';
                                        }
                                        else
                                        {
                                            $like_active = '';
                                        }
                                            $newlike_buddies[] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
                                        }
                                        $newlike_buddies = implode('<br/>', $newlike_buddies);
                                    ?>
                                         <a href="javascript:void(0)" onclick="doLike('<?=$post['_id']?>');" class="pa-like liveliketooltip liketitle_<?=$post['_id']?> <?=$like_active?>"  data-title='<?=$newlike_buddies?>'>
                                            <i class="zmdi zmdi-thumb-up"></i>
                                         </a>
                                        <span class="tooltip_content lcount likecount_<?=$post['_id']?>"> <?php if($like_count >0 ) { echo $like_count; } ?></span>
                                <?php if($post['comment_setting'] != 'Disable'){?>

                                <a href="javascript:void(0)" class="pa-comment <?=$Auth?>" data-id="<?php echo $post['_id'];?>" title="Comment">
                                    <i class="zmdi zmdi-comment"></i>
                                </a>
                                <?php if((count($comments)) > 0){ ?>
                                <span class="comment-lcount commentcountdisplay_<?=$post['_id']?>">
                                    <?php echo count($comments);?>
                                </span>
                                <?php } ?>
                                <?php } ?>

                                <?php 
                                $notifica = Notification::find()->where(['notification_type' => 'editpostuser','post_id' => (string)$post['_id']])->one();
                                if($notifica && $status == '10' && $post['is_deleted'] == '2')
                                { ?>
                                    <a id="publishposticon" href="javascript:void(0)" onclick="publishpost('<?php echo $post['_id'];?>')" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                <?php } ?>
                                </div>
                            </div>
                            <div class="comments-section panel">
                                <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                                <div class="comments-area">
                                <?php } ?>
                                <?php if(count($init_comments)>0){ ?>
                                <div class="post-more">
                                    <?php if(count($init_comments)>2){ ?>
                                    <a href="javascript:void(0)" class="view-morec-<?php echo $post['_id']; ?>" onclick="showPreviousComments('<?php echo $post['_id'];?>')">View more comments <i class="zmdi zmdi mdi-chevron-right trun90"></i></a>
                                    <?php } ?>
                                    <span class="total-comments commment-ctr-<?php echo $post['_id'];?>" id="commment-ctr-<?php echo $post['_id'];?>"><font class="ctrdis_<?=$post['_id']?>"><?php echo count($init_comments);?></font> of <font class="countdisplay_<?=$post['_id']?>"><?php echo count($comments);?></font></span>
                                </div>
                                <?php } ?>
                                <input type="hidden" name="from_ctr" id="from_ctr_<?php echo $post['_id'];?>" value="<?php echo count($init_comments);?>">      
                                <input type="hidden" name="to_ctr" id="to_ctr_<?php echo $post['_id'];?>" value="<?php echo count($comments);?>">

                                <div class="post-comments post_comment_<?=$post['_id']?>">                     
                                    <div class="pcomments sub_post_comment_<?=$post['_id']?>">                                    
                                        <?php
                                        if(count($init_comments)>0) { 
                                            foreach($init_comments as $init_comment) {
                                                $rand = rand(999, 999999) . time();
                                                $cmnt_id = (string) $init_comment['_id'];
                                                $cmnt_like_active= '';
                                                $cmnt_like_active = Like::find()->where(['comment_id' => $cmnt_id,'status' => '1','user_id' => (string) $user_id])->one();
                                                if(!empty($cmnt_like_active))
                                                {
                                                    $cmnt_like_active = 'active';
                                                }
                                                else
                                                {
                                                    $cmnt_like_active = '';
                                                }
                                                $comment_time = Yii::$app->EphocTime->comment_time(time(),$init_comment['updated_date']);$commentcount =Comment::getCommentReply($init_comment['_id']);
                                                $hidecomments = new HideComment();
                                                $hidecomments = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                $hide_comments_ids = explode(',',$hidecomments['comment_ids']);
                                                if(!(in_array($init_comment['_id'],$hide_comments_ids)))
                                                {
                                                    if(($userid == $post['post_user_id']) || ($userid == $init_comment['user']['_id']))
                                                    {
                                                        $afun_post = 'deleteComment';
                                                        $atool_post = 'Delete';
                                                    }
                                                    else
                                                    {
                                                        $afun_post = 'hideComment';
                                                        $atool_post = 'Hide';
                                                    }
                                        ?>
                                        <div class="pcomment-holder <?php if(count($commentcount) > 0){ ?>has-comments<?php } ?>" id="comment_<?php echo $init_comment['_id']?>">
                                            <div class="pcomment main-comment">
                                                <div class="img-holder">
                                                    <div id="commentptip-4" class="profiletipholder">
                                                        <span class="profile-tooltip">
                                                            <?php $init_comment_img = $this->getimage($init_comment['user']['_id'],'thumb'); ?>
                                                            <img class="circle" src="<?= $init_comment_img?>"/>
                                                        </span>
                                                    </div>
                                                </div> 
                                                <div class="desc-holder">
                                                    <div class="normal-mode">
                                                        <div class="desc">
                                                            <a href="<?php $id = $init_comment['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>" class="userlink"><?php echo ucfirst($init_comment['user']['fname']).' '.ucfirst($init_comment['user']['lname']);?></a>
                                                            <?php if(strlen($init_comment['comment'])>200){ ?>
                                                                <p class="shorten" data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?>
                                                                <a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                </p>
                                                            <?php }else{ ?>                                                     
                                                                <p data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?></p>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="comment-stuff">
                                                            <div class="more-opt">
                                                                <span id="likeholder-7" class="likeholder">
                                                                <?php

                                                                     
                                                                    $like_count = Like::find()->where(['comment_id' => (string) $init_comment['_id']  ,'status' => '1'])->all();

                                                                    $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $init_comment['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                                    $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                                    $usrbox = array();
                                                                    foreach ($comlikeuseinfo as $key => $single) {
                                                                        $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                                         $usrbox[(string)$single['_id']] = $fullnm; 
                                                                    }
                                                                    
                                                                    $newlike_buddies = implode("<br/>", $usrbox);
                                                                ?>

                                                                        <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$init_comment['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $init_comment['_id']?>')">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                </span> 
                                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply"></i></a>
                                                                
                                                                <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                    <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete<?=$rand?>">
                                                                        <i class="mdi mdi-dots-vertical"></i>
                                                                    </a>
                                                                    <?php if($status == '10') { ?> 
                                                                    <ul class="dropdown-content" id="dropdown-editdelete<?=$rand?>">
                                                                       <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')">Flag</a></li></ul>
                                                                    <?php } else { ?>
                                                                    <ul class="dropdown-content" id="dropdown-editdelete<?=$rand?>">
                                                                        <?php if(($userid != $post['post_user_id']) && ($userid != $init_comment['user']['_id'])){?>
                                                                            <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$atool_post?></a></li>
                                                                            <?php } else { ?>
                                                                            <?php if($userid == $init_comment['user']['_id']){ ?>
                                                                            <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                            <?php } ?>
                                                                            <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>','<?=$post['_id']?>')">Delete</a></li>                                                  
                                                                        <?php } ?>
                                                                    </ul>
                                                                    <?php } ?>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="less-opt"><div class="timestamp"><?php echo $comment_time?></div></div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-mode">
                                                        <div class="desc">
                                                                <textarea class="editcomment-tt" data-id="<?php echo $init_comment['_id'];?>" id="edit_comment_<?php echo $init_comment['_id'];?>"><?php echo $init_comment['comment']?></textarea>
                                                            <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px   "></i></a>
                                                        </div>                                                                          
                                                    </div>
                                                </div>                              
                                            </div>  
                                            <div class="clear"></div>
                                            
                                            <div class="comment-reply-holder reply_comments_<?php echo $init_comment['_id'];?>">
                                                <?php $comment_replies =Comment::getCommentReply($init_comment['_id']);
                                                    if(count($comment_replies)>0) {
                                                    $lastcomment = Comment::find()->where(['parent_comment_id' => (string)$init_comment['_id']])->orderBy(['updated_date'=>SORT_DESC])->one();
                                                    $last_comment_time = Yii::$app->EphocTime->comment_time(time(),$lastcomment['updated_date']);
                                                ?>
                                                <div class="comments-reply-summery">
                                                    <a href="javascript:void(0)" onclick="openReplies(this)">
                                                        <i class="mdi mdi-share"></i>
                                                        <?php echo count($comment_replies); 
                                                        if(count($comment_replies)>1) { echo " Replies"; }
                                                        else { echo " Reply"; } ?>
                                                    </a>
                                                    <i class="mdi mdi-bullseye dot-i"></i>
                                                    <?php echo $last_comment_time;?>
                                                </div>
                                                <?php }
                                                if(!empty($comment_replies))
                                                {
                                                    foreach($comment_replies AS $comment_reply) { 
                                                    $rand = rand(999, 99999) . time();
                                                    $hidecomment = new HideComment();
                                                    $hidecomment = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                    $hide_comment_ids = explode(',',$hidecomment['comment_ids']); 
                                                    if(!(in_array($comment_reply['_id'],$hide_comment_ids)))
                                                    {
                                                        if(($userid == $post['post_user_id']) || ($userid == $comment_reply['user']['_id']))
                                                        {
                                                            $bfun_post = 'deleteComment';
                                                            $btool_post = 'Delete';
                                                        }
                                                        else
                                                        {
                                                            $bfun_post = 'hideComment';
                                                            $btool_post = 'Hide';
                                                        }
                                                    $comment_time = Yii::$app->EphocTime->comment_time(time(),$comment_reply['updated_date']);

                                                    $like_count = Like::find()->where(['comment_id' => (string) $comment_reply['_id']  ,'status' => '1'])->all();

                                                    $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $comment_reply['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                    $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                    $usrbox = array();
                                                    foreach ($comlikeuseinfo as $key => $single) {
                                                    $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                    $usrbox[(string)$single['_id']] = $fullnm; 
                                                    }

                                                    $newlike_buddies = implode("<br/>", $usrbox)

                                            ?>
                                            <div class="comments-reply-details">
                                                <div class="pcomment comment-reply" id="comment_<?php echo $comment_reply['_id']?>">
                                                    <div class="img-holder">
                                                        <div class="profiletipholder" id="commentptip-6">
                                                            <span class="profile-tooltip tooltipstered">
                                                                <img class="circle" src="<?php echo $this->getimage($comment_reply['user']['_id'],'thumb'); ?>">
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="desc-holder">
                                                        <div class="normal-mode">
                                                            <div class="desc">
                                                                <a class="userlink" href="<?php $id = $comment_reply['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>"><?php echo ucfirst($comment_reply['user']['fname']).' '.ucfirst($comment_reply['user']['lname']);?></a>
                                                                
                                                                <?php if(strlen($comment_reply['comment'])>200){ ?>
                                                                    <p class="shorten" data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?><a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                    </p>
                                                                <?php }else{ ?>                                                         
                                                                    <p data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?></p>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="comment-stuff">
                                                                <div class="more-opt">
                                                                        <span class="likeholder" id="likeholder-6">
                                                                        <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$comment_reply['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $comment_reply['_id']?>')">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                                <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdelete<?=$rand?>">
                                                                                    <i class="mdi mdi-dots-vertical"></i>
                                                                                </a>
                                                                                
                                                                                <ul class="dropdown-menu">
                                                                                <ul class="dropdown-content" id="dropdown-editdelete<?=$rand?>">
                                                                                    <?php if(($userid != $post['post_user_id']) && ($userid != $comment_reply['user']['_id'])){?>
                                                                                        <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$btool_post?></a></li>
                                                                                        <?php } else { ?>
                                                                                        <?php if($userid == $comment_reply['user']['_id']){ ?>
                                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                        <?php } ?>
                                                                                        <li><a href="javascript:void(0)" class="delete-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')">Delete</a></li>                                                  
                                                                                    <?php } ?>
                                                                                </ul>
                                                                        </div>
                                                                </div>
                                                                <div class="less-opt"><div class="timestamp"><?= $comment_time;?></div></div>
                                                            </div>  
                                                        </div>
                                                        <div class="edit-mode">
                                                            <div class="desc">
                                                                <textarea class="editcomment-tt" data-id="<?php echo $comment_reply['_id'];?>" id="edit_comment_<?php echo $comment_reply['_id'];?>"><?php echo $comment_reply['comment']?></textarea>
                                                                <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px   "></i></a>
                                                            </div>                                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } } } ?>
                                            </div>
                                            
                                            <div class="comment-reply-holder comment-addreply">                                 
                                                <div class="addnew-comment comment-reply">                          
                                                    <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $this->getimage($result['_id'],'thumb');?>"/></a></div>
                                                    <div class="desc-holder">                                   
                                                        <textarea name="reply_txt" placeholder="Write a reply" data-postid="<?php echo $post['_id'];?>" data-commentid="<?php echo $init_comment['_id']?>" id="reply_txt_<?php echo $init_comment['_id'];?>" class="reply_class capitalize"></textarea>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <?php  } } } ?>
                                    </div>
                                    <?php if($post['comment_setting'] != 'Disable'){
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        $comment_placeholder = "Ask question or send query";
                                    }
                                    else{
                                        $comment_placeholder = "Write a comment";
                                    }   
                                    ?>
                                    <div class="addnew-comment valign-wrapper">
                                        <?php $comment_image = $this->getimage($result['_id'],'thumb'); ?>
                                        <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $comment_image?>"/></a></div>
                                        <form name="imageCommentForm" id="imageCommentForm" enctype="multipart/form-data">
                                            <div class="desc-holder">                                   
                                                <textarea data-postid="<?php echo $post['_id'];?>" id="comment_txt_<?php echo $post['_id'];?>" placeholder="<?= $comment_placeholder;?>" class="comment_class  materialize-textarea capitalize"></textarea>
                                            </div>
                                            <?php if($existing_posts == ''){ ?>
                                                <input type="hidden" id="last_inserted" name="last_inserted" value="<?php echo $post['_id']; ?>"/>
                                            <?php } ?>
                                        </form>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
        <?php }    
            }      
        }
    }

    public function display_last_place_review($last_insert_id, $existing_posts = '', $imagefullarray = '', $cls='', $total='', $tempocls='')
    {
        $session = Yii::$app->session;
        $userid = $user_id = (string)$session->get('user_id');
        $email = $session->get('email');
        $status = $session->get('status');
        $result = LoginForm::find()->where(['_id' => $userid])->one();
        $date = time();
        if($cls == 'places-ads') {
            $post = PostForm::find()->where([(string)'_id' => $last_insert_id])->one();
            $tableLabel = 'PostForm';
        } else {
            $post = PlaceReview::find()->where(['_id' => $last_insert_id])->andWhere(['not','flagger', "yes"])->one();
            $tableLabel = 'PlaceReview';
        }
        $reportpost = ReportPost::find()->where(['post_id' => (string)$last_insert_id,'reporter_id' => (string)$user_id])->one();
        $profile_tip_counter = 1;
        $rand = rand(999, 99999);
        $uniqid = uniqid();
          
        
        $Auth = '';
        if(isset($userid) && $userid != '') 
        {
            $authstatus = UserForm::isUserExistByUid($userid);
            if($authstatus == 'checkuserauthclassg' || $authstatus == 'checkuserauthclassnv') 
            {
                $Auth = $authstatus;
            }
        }   
        else    
        {
            $Auth = 'checkuserauthclassg';
        }
        

        $my_post_view_status = $post['post_privacy'];
        if($my_post_view_status == 'Private') {$post_dropdown_class = 'lock';}
        else if($my_post_view_status == 'Connections') {$post_dropdown_class = 'account';}
        else if($my_post_view_status == 'Custom') {$post_dropdown_class = 'settings';}
        else {$post_dropdown_class = 'earth';}
        if(!($reportpost))
        {
            $postprivacy = $post['post_privacy'];
            $postownerid = (string)$post['post_user_id'];
            $suserid = (string)$user_id;
            $is_connect = Connect::find()->where(['from_id' => $postownerid,'to_id' => $suserid,'status' => '1'])->one();
            $isWriteAllow = 'no';

            if($userid == $postownerid) {
                $isWriteAllow = 'yes';
            } else if(($postprivacy == 'Public' || $status == '10') || ($postprivacy == 'Connections' && ($is_connect || $status == '10')) || ($postprivacy == 'Private')) {
                $isWriteAllow = 'yes';
            } else if($postprivacy == 'Custom') {
                $customids = array();
                $customids = isset($post['customids']) ? $post['customids'] : '';
                $customids = explode(',', $customids);

                if(in_array($userid, $customids)) {
                    $isWriteAllow = 'yes';          
                }
            } else if($cls != '' && $cls == 'places-ads') {
                $isWriteAllow = 'yes';      
            }

            if($isWriteAllow == 'yes')
            {
                $block = BlockConnect::find()->where(['user_id' => "$postownerid"])->andwhere(['like','block_ids',$suserid])->one();
                if(!$block)
                {
                $savestatus = new SavePost();
                $savestatus = SavePost::find()->where(['user_id' => (string)$user_id,'post_id' => (string)$post['_id']])->one();
                $savestatuscalue = $savestatus['is_saved'];
                if($savestatuscalue == '1'){$savevar = 'Unsave';}else{$savevar = 'Save';}

                $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['post_created_date']);
                $like_count = Like::getLikeCount((string)$post['_id']);
                $comments = Comment::getAllPostLike($post['_id']);
                $init_comments = Comment::getFirstThreePostComments($post['_id']);
                $post_privacy = $post['post_privacy'];

                if($post_privacy == 'Private') {$post_class = 'lock';}
                else if($post_privacy == 'Connections') {$post_class = 'account';}
                else if($post_privacy == 'Custom') {$post_class = 'settings';}
                else {$post_class = 'earth';}

                // START add tag content in post
                $posttag = '';
                if(isset($post['post_tags']) && !empty($post['post_tags']))
                {
                    $posttag = explode(",", $post['post_tags']);
                }

                $taginfomatiom = ArrayHelper::map(UserForm::find()->where(['IN', '_id',  $posttag])->all(), 'fullname', (string)'_id');

                $nkTag = array();
                $nvTag = array();

                $i=1;
                foreach ($taginfomatiom as $key => $value)
                {
                    $nkTag[] = (string)$value; 
                    $nvTag[] = $key;
                    if($i != 1) {
                        $content[] = $key;
                    }
                    $i++;
                }

                if(isset($content) && !empty($content))
                {
                    $content = implode("<br/>", $content); 
                }

                $tagstr = '';
                if(!empty($taginfomatiom))
                {
                    if(count($taginfomatiom) > 1) {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . " </a> and <span id='likeholder-1' class='likeholder'><a href='javascript:void(0)' class='pa-like sub-link livetooltip' title='".$content."'>".(count($nkTag) - 1)." others</a></span>";
                    } else {
                        $tagstr =  " with <a href=".Url::to(['userwall/index', 'id' => $nkTag[0]]) ." class='sub-link'>" . $nvTag[0] . "</a>";
                    }
                }
                if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                {
                    $page_details = Page::Pagedetails($post['page_id']);
                    $review = " reviewed <a href=".Url::to(['page/index', 'id' => $post['page_id']]) ." class='sub-link'>" . $page_details['page_name'] . "</a>"; }
                else { $review = ''; }
                // END add tag content in post
                $profile_tip_counter++;         
                if(isset($post['currentlocation']) && !empty($post['currentlocation']))
                {
                    $current_location = $this->getshortcityname($post['currentlocation']);
                    $default = 'at';
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='reviews'){$default = 'reviewed';}
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='tip'){$default = 'tip for';}
                    if(isset($post['placetype']) && !empty($post['placetype']) && $post['placetype']=='ask'){$default = 'has a question about';}
                    $locationtags = $tagstr . ' '.$default.' <a class="sub-link" href="javascript:void(0)">'.$current_location.'</a>';
                }
                else{ $locationtags = $tagstr . ''; }
                if($post['is_timeline'] == '1')
                {
                    $getrateid = PlaceReview::find()->where(['_id' => $post['parent_post_id']])->andWhere(['not','flagger', "yes"])->one();
                    $getratedid = $getrateid['rating'];
                    if($getratedid == null){$getratedid = '6';}
                }
                else
                {
                    $getratedid = '6';
                }
                if((isset($post['is_ad']) && !empty($post['is_ad']) && $post['is_ad'] == '1'))
                {
                    $is_ad = true;
                    $adclass = 'travad-box ';
                    if($post['adobj'] == 'pagelikes'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'brandawareness'){$adclass .= 'brand-ad';}
                    else if($post['adobj'] == 'websiteleads'){$adclass .= 'weblink-ad';}
                    else if($post['adobj'] == 'websiteconversion'){$adclass .= 'actionlink-ad';}
                    else if($post['adobj'] == 'pageendorse'){$adclass .= 'page-ad';}
                    else if($post['adobj'] == 'eventpromo'){$adclass .= 'event-ad';}
                    else{$adclass .= '';}
                }
                else{ $is_ad = false; $adclass = '';}
        ?>
        <style> 
        .pac-container
        {
            z-index: 1051 !important;
        }
        </style>
            <div data-postid="<?=$post['_id']?>" class="<?=$tempocls .' '.$cls?> post-holder <?php if($existing_posts != 'from_save'){ ?>bshadow<?php } ?> <?php if((isset($post['is_page_review']) && !empty($post['is_page_review']) || ($getratedid != '6'))){ ?>reviewpost-holder<?php } ?> <?php if((isset($post['trav_item']) && !empty($post['trav_item']))){ ?>travelstore-ad<?php } ?> <?php if((isset($post['is_ad']) && !empty($post['is_ad']))){ echo $adclass; } ?> post_user_id_<?php echo $post['post_user_id'];?>" id="hide_<?php echo $post['_id'];?>">
            <?php
            $getuserinfo = LoginForm::find()->where(['_id' => $post['post_user_id']])->one();
            $personalinfo = Personalinfo::find()->where(['user_id' => $post['post_user_id']])->one();
            if($post['user']['status']=='44')
            {
                $dp = $this->getpageimage($post['user']['_id']);
                $link = Url::to(['page/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'page';
            }
            else
            {
                $dp = $this->getimage($getuserinfo['_id'],'photo');
                $link = Url::to(['userwall/index', 'id' => $post['post_user_id']]);
                $linkwall = 'userwall';
                $page = 'userwall';
            }
            if(isset($getuserinfo['cover_photo']) && !empty($getuserinfo['cover_photo']))
            {
                $cover = $getuserinfo['cover_photo'];
                if(strstr($cover,'cover'))
                {
                    $cvr = substr_replace($getuserinfo['cover_photo'], '-thumb.jpg', -4);
                    $cover = '../uploads/cover/thumbs/'.$cvr;
                }
                else
                {
                    $cover = '../uploads/cover/thumbs/thumb_'.$getuserinfo['cover_photo'];
                }
            }
            else
            {
                $cover = 'cover.jpg';
            }
            $dpforpopup = $this->getimage($post['user']['_id'],'thumb');
            $mutualcount = Connect::mutualconnectcount($getuserinfo['_id']);

            $po_id = $post['post_user_id'];
            $isconnect = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '1'])->one();
            $isconnectrequestsent = Connect::find()->where(['from_id' => "$userid",'to_id' => "$po_id",'status' => '0'])->one();
            ?>
            <?php if($is_ad && $post['adobj'] != 'travstorefocus'){
                $this->display_ad($post['_id'],'topbar');
            } else { ?>
                <div class="post-topbar">
                    <div class="post-userinfo">
                        <div class="img-holder">
                            <div  class="profiletipholder">
                                <span id="profiletip-<?= $post['_id']?>" class="profile-tooltip">
                                    <?php
                                    if($post['is_timeline']=='1')
                                    {
                                        $dpimg = $this->getimage($post['shared_by'],'photo');
                                    }
                                    else
                                    {                                        
                                        $dpimg = $this->getimage($post['user']['_id'],'photo');
                                    }
                                    ?>
                                    <img class="circle" src="<?= $dpimg?>">
                                </span>
                               <?php if($existing_posts != 'from_save'){ ?>
                                    <span class="profiletooltip_content profiletooltip_content_last">
                                        <div class="dis-none profile-tip slidingpan-holder" id="dispatchcontentprofiletip-<?= $post['_id']?>">
                                           <div class="profile-tip-avatar">
                                                <img alt="user-photo" class="img-responsive"  src="<?= $dp?>">
                                                <div class="sliding-pan location-span">
                                                    <div class="sliding-span location-span">Ahmedabad, Gujarat, India</div>
                                                </div>
                                            </div>
                                            <div class="profile-tip-name">
                                                <a href="<?php echo $link;?>"><?php echo ucfirst($getuserinfo['fname']).' '.ucfirst($getuserinfo['lname'])?></a>
                                            </div>
                                            <?php if($post['user']['status']!='44'){ ?>
                                            <div class="profile-tip-info">
                                                <div class="profiletip-icon">
                                                        <?php if($userid == $po_id){ ?>
                                                        <a href="javascript:void(0)">&nbsp;</a>
                                                        <?php } else if($isconnect) { ?>
                                                        <a href="javascript:void(0)"><?=$mutualcount?> Mutual connections</a>
                                                        <?php } else if($isconnectrequestsent) { ?>
                                                        <i title="Connect request sent" class="mdi mdi-account-minus"></i>
                                                        <?php } else { ?>
                                                        <a href="javascript:void(0)" title="Add Connect" class="title_<?php echo $po_id;?>">
                                                        <i class="mdi mdi-account-plus people_<?php echo $po_id;?>" onclick="addConnect('<?php echo $po_id;?>')"></i>
                                                        <i class="mdi mdi-account-minus dis-none sendmsg_<?php echo $po_id;?>"></i>
                                                        <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
                                                        </a>
                                                        <?php } ?>
                                                </div>
                                                <div class="profiletip-icon">
                                                        <a href="<?php echo $link;?>" title="View Profile"><i class="mdi mdi-eye"></i></a>
                                                </div>                  
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </span>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="desc-holder">
                            <?php
                                if($post['is_timeline']=='1'/*&& $post['pagepost']==null*/) 
                                {
                                    $getsharename = LoginForm::find()->where(['_id' => $post['shared_by']])->one();
                                    if(strstr($post['parent_post_id'],'event_'))
                                    {
                                        $eventdetails = PageEvents::getEventdetailsshare(substr($post['parent_post_id'],6));
                                        $eid = $eventdetails['_id'];
                                        $ename = $eventdetails['event_name'];
                                        $elink = Url::to(['event/detail', 'e' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s event";
                                    }
                                    else if(strstr($post['parent_post_id'],'page_'))
                                    {
                                        $pagedetails = Page::Pagedetails(substr($post['parent_post_id'],5));
                                        $eid = $pagedetails['page_id'];
                                        $ename = $pagedetails['page_name'];
                                        $elink = Url::to(['page/index', 'id' => "$eid"]);
                                        $review = " Shared <a href=$elink>".$ename."</a>'s page";
                                    }
                            ?>
                            <a href="<?php $id =  $getsharename['_id']; echo Url::to([$linkwall.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($getsharename['fname']).' '.ucfirst($getsharename['lname']) ;?></a><?= $review?>
                            <?php if($post['post_user_id'] != $post['shared_by']) { ?>
                            <div class="user-to">
                                <i class="mdi mdi-menu-right"></i>
                                <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            </div>
                            <?php } ?>
                            <?php if(isset($locationtags) && !empty($locationtags)) { ?>
                                <?= $locationtags?>
                            <?php } } else { ?>
                            <a href="<?php $id =  $post['user']['_id']; echo Url::to([$page.'/index', 'id' => "$id"]); ?>"><?php echo ucfirst($post['user']['fname']).' '.ucfirst($post['user']['lname']) ;?></a><?= $review?>
                            <?php if((empty($post['trav_item']))){ ?><?= $locationtags?><?php } ?>
                            <?php if($post['post_type']=='profilepic'){ ?>
                            updated profile photo.
                            <?php } else if($post['is_coverpic']=='1'){ ?>
                            updated cover photo.
                            <?php } ?>
                            <?php } ?>
                            <span class="timestamp"><?=$time?>
                                <a class="dropdown-button" href="javascript:void(0);" data-activates="<?=$uniqid?>">
                                    <i class="mdi mdi-<?=$post_class?> mdi-13px"></i>
                                    <?php 
                                    $pageownerid = $post['post_user_id'];
                                    if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                    <i class="mdi mdi-menu-down mdi-22px"></i>
                                    <?php } ?>
                                </a>

                                <ul id="<?=$uniqid?>" class="dropdown-content directprivacydropdown">
                                    <li class="post-private" data-cls="post-private"> <a href="javascript:void(0)"> Private </a> </li>
                                    <li class="post-connections" data-cls="post-connections"> <a href="javascript:void(0)"> Connections </a> </li>
                                    <li class="post-custom customli_modal_post" data-cls="post-custom"> <a href="javascript:void(0)"> Custom </a> </li>
                                    <li class="post-public" data-cls="post-public"> <a href="javascript:void(0)"> Public </a> </li>
                               </ul>
                            </span>
                        </div>
                    </div>
                    <?php
                    $p = 'post';
                    if(isset($post['is_page_review']) && !empty($post['is_page_review']))
                    {
                        $p = 'review';
                    }
                    if(isset($post['placetype']) && !empty($post['placetype']))
                    {
                        $p = $post['placetype'];
                        if($p == 'reviews'){$p = 'review';}
                        if($p == 'ask'){$p = 'question';}
                    }
                    ?>
                    <div class="settings-icon">
                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                            <a class="dropdown-button <?=$Auth?> directcheckuserauthclass" href="javascript:void(0)" data-activates='dropdown-editreview<?=$rand?>'>
                                <i class="zmdi zmdi-more zmdi-hc-2x"></i>
                            </a>
                            <ul id="dropdown-editreview<?=$rand?>" class="dropdown-content custom_dropdown">
                                    <?php if($status == '10') { ?> 
                                    <li><a href="javascript:void(0)" data-id="<?=(string)$post['_id']?>" data-module="<?=$p?>" class="customreportpopup-modal reportpost-link" onclick="flagpost(this)"></span>Flag <?=$p?></a></li>
                                    <?php } else {?>
                                            <li><a href="javascript:void(0)" class="hidepost" onclick="hidePost('<?php echo $post['_id']?>')">Hide <?=$p?></a></li>
                                            <li><a href="javascript:void(0)" class="savepost-link" id="save_post_<?php  echo $post['_id'];?>" data-pid="<?php  echo $post['_id'];?>" onclick="savePost('<?php echo $post['_id']?>','<?php echo $post['post_type']?>','<?=$savevar?>','place_review')"><?=$savevar?> <?=$p?></a></li>
                                                <input type="hidden" class="tbsp_<?=$post['_id']?>" value="<?=$savevar?>">
                                            <li><a href="javascript:void(0)" onclick="turn_off_notification('<?=$post['_id'] ?>', 'review')" class="turnnotpost_<?=$post['_id'] ?>">
                                            <?php
                                            $turnoff = TurnoffNotification::find()->where(['user_id' => "$user_id"])->andwhere(['like','post_ids',$post['_id']])->one();
                                            if($turnoff) { $turnoffvalue = "Unmute this review notification"; }
                                            else { $turnoffvalue = "Mute this review notification";}                                
                                            ?>
                                            <?=$turnoffvalue?>
                                            </a></li>    
                                        <?php if($post['user']['status']=='44' && ($post['is_coverpic']=='1' || $post['is_profilepic']=='1')) { $page_details = Page::Pagedetails($post['post_user_id']); $pageownerid = $page_details['created_by']; }
                                        else { $pageownerid = $post['post_user_id']; } ?>
                                        <?php if(($userid == $post['post_user_id']) || ($user_id == $post['shared_by']) || ($user_id == $pageownerid)){ ?>
                                            <li><a href="javascript:void(0)" class="deletepost-link" onclick="deletePlaceReview('<?php echo $post['post_user_id']?>','<?php echo $post['_id']?>')">Delete <?=$p?></a></li>
                                            <?php if(($post['is_coverpic']!='1' && $post['is_profilepic']!='1' && $post['shared_from']!='shareentity')){ ?>
                                            <li><a href="javascript:void(0)" data-editpostid="<?php echo $post['_id'];?>" class="customeditpopup-modal-place-review editpost-link">Edit <?=$p?></a></li>
                                            <?php } ?>
                                        <?php } ?>
                                        <?php if(($userid != $post['post_user_id']) && ($user_id != $post['shared_by']) && ($user_id != $pageownerid)){
                                            $muteuser = UnfollowConnect::find()->where(['user_id' => "$user_id"])->andwhere(['like','unfollow_ids',$post['post_user_id']])->one();
                                            if($muteuser) { $mutevalue = 'Unmute this member posts';}
                                            else { $mutevalue = 'Mute this member posts';}
                                        ?>
                                            <li><a href="javascript:void(0)" onclick="unfollowConnect('<?php echo $post['post_user_id']?>')"><?=$mutevalue?></a></li>
                                            <li><a href="#reportpost-popup-<?php echo $post['_id'];?>" data-reportpostid="<?php echo $post['_id'];?>" class="customreportpopup-modal reportpost-link"></span>Report post</a></li>
                                        <?php } ?>
                                    <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php } ?>          
                        <?php
                        if($post['is_timeline']=='1' && isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                        {
                        ?>
                            <div class="post-content">
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                <?php if($post['post_text'] != ''){ ?>
                                        <div class="post-desc">
                                            <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                            
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                    </div>
                                                <?php }else{ ?>                                     
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>
                                                
                                            <?php } ?>
                                        </div>
                                  <?php } ?>
                                    </div>
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                </div>
                            </div>
                            <?php
                            if(isset($post['parent_post_id']) && !empty($post['parent_post_id']))
                            {
                                if(strstr($post['parent_post_id'],'event_'))
                                {
                                    $this->view_event((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'page_'))
                                {
                                    $this->view_page((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'collection_'))
                                {
                                    $this->view_collection((string)$post['parent_post_id']);
                                }
                                else if(strstr($post['parent_post_id'],'trip_'))
                                {
                                    $this->view_trip((string)$post['parent_post_id']);
                                }
                                else
                                {
                                    $this->view_post_id((string)$post['parent_post_id']);
                                }
                            }
                        } else if($post['is_trip'] == '1'){
                        ?>
                            <div class="post-content tripexperince-post">
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                        <div class="post-title">
                                            <?= $post['post_title'];?>
                                        </div>
                                    </div>
                                    <div class="trip-summery">
                                        <div class="location-info raani">
                                            <h5><i class="zmdi zmdi-pin"></i>
                                            <?php  
                                            $currentlocation = trim($post['currentlocation']);

                                            if($currentlocation != '') {
                                                $currentlocation = explode(",", $currentlocation);
                                                if(count($currentlocation) >1) {
                                                    $first = reset($currentlocation);
                                                    $last = end($currentlocation);
                                                    $currentlocation = $first.', '.$last;
                                                } else {
                                                    $currentlocation = implode(", ", $currentlocation);
                                                }
                                            }

                                            echo $currentlocation;
                                            ?></h5>
                                            <i class="mdi mdi-menu-right"></i>
                                            <a href="javascript:void(0)" onclick="openViewMap(this)">View on map</a>
                                        </div>
                                    </div>
                                    <div class="map-holder dis-none">
                                        <iframe width="600" height="450" frameborder="0" src="https://maps.google.it/maps?q=<?= $post['currentlocation'];?>&output=embed"></iframe>
                                    </div>
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                </div>
                                <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                    $cnt = 1;
                                    $eximgs = explode(',',$post['image'],-1);
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        if($post['image'] == null)
                                        {
                                            $eximgs[0] = '/uploads/travitem-default.png';
                                        }
                                        $eximgss[] = $eximgs[0];
                                        $eximgs = $eximgss;                                     
                                    }
                                    $totalimgs = count($eximgs);
                                    $imgcountcls="";
                                    if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                    if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                    if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                    if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                    if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                    if($totalimgs > '5'){$imgcountcls = 'more-img';}
                                ?>
                                <div class="post-img-holder">
                                    <div class="post-img <?= $imgcountcls?> lgt-gallery lgt-gallery-photo dis-none">
                                        <?php
                                        foreach ($eximgs as $eximg) {
                                        if (file_exists('../web'.$eximg)) {
                                        $picsize = '';
                                        $val = getimagesize('../web'.$eximg);
                                        $picsize .= $val[0] .'x'. $val[1] .', ';
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                        if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                        if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                            <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                                <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                                <?php if($cnt == 5 && $totalimgs > 5){?>
                                                    <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                                <?php } ?>
                                            </a>
                                        <?php } $cnt++; } ?>
                                    </div>
                                    <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                        if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                            <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                        <?php } else { 
                                            $iname = $this->getimagename($eximg);
                                            $inameclass = $this->getimagefilename($eximg);
                                            $image = '../web/uploads/gallery/'.$iname;
                                            $JIDSsdsa = '';           
                                            $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                            if(!empty($pinit)) {
                                                $JIDSsdsa = 'active';                    
                                            }
                                            ?>
                                            <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                        <?php 
                                        } 
                                    } 
                                    ?>
                                </div>
                                <?php } ?>
                                <div class="pdetail-holder">
                                    <div class="post-details">
                                    <?php 
                                    if($post['post_text'] != ''){ ?>
                                            <div class="post-desc">
                                                <?php if(substr($post['post_text'],0,4) != 'http'){ ?>
                                                    <?php if(strlen($post['post_text'])>187){ ?>
                                                        <div class="para-section">
                                                            <div class="para">
                                                                <p><?= $post['post_text'] ?></p>
                                                            </div>
                                                            <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                        </div>
                                                    <?php } else { ?>                                       
                                                        <p><?= $post['post_text'] ?></p>
                                                    <?php } ?>
                                                
                                                <?php } ?>
                                            </div>
                                      <?php } ?>
                                    </div>  
                                    <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                                    
                                </div>
                            </div>
                        <?php   
                        } else if($is_ad && $post['adobj'] != 'travstorefocus'){
                            $this->display_ad($post['_id'],'content');
                        }
                        else
                        { if(!isset($post['is_page_review']) && empty($post['is_page_review'])) {
                        ?>
                        <div class="post-content">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                    <?php if($post['post_title'] != null) { ?>
                                    <div class="post-title"><?= $post['post_title'] ?></div>
                                    <?php } ?>
                                    <?php if(isset($post['placereview']) && !empty($post['placereview']) && $post['placetype']=='reviews'){ ?>
                                    <div class="rating-stars">
                                    <?php for($i=0;$i<5;$i++)
                                    { ?>
                                        <i class="mdi mdi-star <?php if($i < $post['placereview']){ ?>active<?php } ?>"></i>
                                    <?php }
                                    ?>
                                    </div>
                                    <?php } ?>
                                    <?php if($post['trav_price'] != null) { ?>
                                    <div class="post-price">$<?= $post['trav_price'] ?></div>
                                    <?php } ?>
                                    <?php if((isset($post['trav_item']) && !empty($post['trav_item']) && $post['currentlocation'] != null)){ ?>
                                        <div class="post-location" style="display:block"><i class="zmdi zmdi-pin"></i><?= $post['currentlocation'] ?></div>
                                    <?php } ?>
                                    <?php if($post['post_type'] != 'link' && $post['post_type'] != 'profilepic'){ ?>
                                    <div class="post-desc">
                                    
                                        <?php if(strlen($post['post_text'])>187){ ?>
                                            <div class="para-section">
                                                <div class="para">
                                                    <p><?= $post['post_text'] ?></p>
                                                </div>
                                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                            </div>
                                        <?php }else{ ?>                                     
                                            <p><?= $post['post_text'] ?></p>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if($post['trav_item']== '1' && $post['is_ad']== '1'){
                                    $actbtn = $post['adbtn'];
                                    $link = $post['adurl'];
                                    if(!empty($link))
                                    {
                                        if(substr( $link, 0, 4 ) === "http")
                                        {
                                            $link = $link;
                                        }
                                        else
                                        {
                                            $link = 'https://'.$link;
                                        }
                                    }
                                    else
                                    {
                                        $link = 'Not added';
                                    }
                                ?>
                                <div class="post-paidad">
                                    <div class="travad-sponsor">Sponsered by <a href="<?=$link?>" target="blank"><?=$link?></a></div>
                                    <a class="travad-shop btn btn-primary btn-md">
                                        <?=$actbtn?>
                                    </a>
                                   </div>
                                <?php } ?>
                                
                                <?php if($post['post_type'] == 'link'){ ?>
                                <div class="pvideo-holder">
                                    <?php if($post['image'] != 'No Image'){ ?>
                                    <div class="img-holder"><img src="<?= $post['image'] ?>"/></div>
                                    <div class="desc-holder">
                                    <?php } ?>
                                        <h4><a href="<?= $post['post_text']?>" target="_blank"><?= $post['link_title'] ?></a></h4>
                                        <p><?= $post['link_description'] ?></p>
                                    <?php if($post['image'] != 'No Image'){ ?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <a class="overlay-link postdetail-link popup-modal" href="javascript:void(0)" id="MobilePost" data-postid="<?php echo $post['_id'];?>">&nbsp;</a>
                            </div>
                            <?php if($post['post_type'] == 'text' && $post['trav_item']== '1'){
                                $post['post_type'] = 'text and image';
                            }?>
                            <?php if(($post['post_type'] == 'image' || $post['post_type'] == 'text and image') && $post['is_coverpic'] == null) {
                                $cnt = 1;
                                $eximgs = explode(',',$post['image'],-1);
                                if(isset($post['trav_item']) && $post['trav_item']== '1')
                                {
                                    if($post['image'] == null)
                                    {
                                        $eximgs[0] = '/uploads/travitem-default.png';
                                    }
                                    $eximgss[] = $eximgs[0];
                                    $eximgs = $eximgss;                                     
                                }
                                $totalimgs = count($eximgs);
                                $imgcountcls="";
                                if($totalimgs == '1'){$imgcountcls = 'one-img';}
                                if($totalimgs == '2'){$imgcountcls = 'two-img';}
                                if($totalimgs == '3'){$imgcountcls = 'three-img';}
                                if($totalimgs == '4'){$imgcountcls = 'four-img';}
                                if($totalimgs == '5'){$imgcountcls = 'five-img';}
                                if($totalimgs > '5'){$imgcountcls = 'more-img';}
                            ?>
                            <div class="post-img-holder">
                                <div class="post-img <?= $imgcountcls?> lgt-gallery lgt-gallery-photo dis-none">
                                    <?php
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';} ?>
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>"  data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box <?php if($cnt > 5){?>extraimg<?php } ?> <?php if($cnt ==5 && $totalimgs > 5){?>more-box<?php } ?>">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                            <?php if($cnt == 5 && $totalimgs > 5){?>
                                                <span class="moreimg-count"><i class="mdi mdi-plus"></i><?= $totalimgs - $cnt +1;?></span>
                                            <?php } ?>
                                        </a>
                                    <?php } $cnt++; } ?>
                                </div>
                                <?php if($imgcountcls == "one-img" && $post['trav_item'] != '1') {
                                    if($Auth == 'checkuserauthclassg' || $Auth == 'checkuserauthclassnv') { ?>
                                        <a href="javascript:void(0)" class="pinlink <?=$Auth?> directcheckuserauthclass"><i class="mdi mdi-nature"></i></a>
                                    <?php } else { 
                                        $iname = $this->getimagename($eximg);
                                        $inameclass = $this->getimagefilename($eximg);
                                        $image = '../web/uploads/gallery/'.$iname;
                                        $JIDSsdsa = '';           
                                        $pinit = Gallery::find()->where(['post_id' => (string)$post['_id'], 'image' => $image])->andWhere(['not','flagger', "yes"])->one();
                                        if(!empty($pinit)) {
                                            $JIDSsdsa = 'active';                    
                                        }
                                        ?>
                                        <a href="javascript:void(0)" class="pinlink pin_<?=$inameclass?> <?=$JIDSsdsa?>" onclick="pinImage('<?=$iname?>','<?=$post['_id']?>', '<?=$tableLabel?>')"><i class="mdi mdi-nature"></i></a>
                                    <?php 
                                    } 
                                } 
                                ?>
                            </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'image' && $post['is_coverpic'] == '1' && file_exists('uploads/cover/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="post-img one-img gallery lgt-gallery lgt-gallery-photo dis-none">
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('uploads/cover/'.$post['image']);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);

                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                          <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/uploads/cover/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                            </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if($post['post_type'] == 'profilepic' && file_exists('profile/'.$post['image'])) { ?>
                                <div class="post-img-holder">
                                    <div class="post-img gallery one-img lgt-gallery lgt-gallery-photo dis-none">
                                    <?php
                                    $picsize = '';
                                    $val = getimagesize('profile/'.$post['image']);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $post['image'];
                                    $inameclass = $this->getnameonly($post['image']);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                            
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}?>
                                          
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= '/profile/'.$post['image'] ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            
                        </div>
                        <?php } else { ?>
                        <div class="post-content <?php if($post['image'] != null) { ?>hasimg<?php } ?>">
                            <div class="pdetail-holder">
                                <div class="post-details">
                                        <?php if($post['post_title'] != null) { ?>
                                        <div class="post-title"><?=$post['post_title']?></div>
                                        <?php } ?>
                                        <div class="rating-stars">
                                        <?php for($i=0;$i<5;$i++)
                                        { ?>
                                            <i class="mdi mdi-star <?php if($i < $post['rating']){ ?>active<?php } ?>"></i>
                                        <?php }
                                        ?>
                                        </div>
                                        <div class="post-desc">
                                                <?php if($post['post_text'] == null) { $post['post_text'] = 'Not added'; } ?>
                                                
                                                <?php if(strlen($post['post_text'])>187){ ?>
                                                    <div class="para-section">
                                                        <div class="para">
                                                            <p><?= $post['post_text'] ?></p>
                                                        </div>
                                                        <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                    </div>
                                                <?php }else{ ?>                                     
                                                    <p><?= $post['post_text'] ?></p>
                                                <?php } ?>                                             
                                        </div>
                                </div>
                                <a class="overlay-link postdetail-link popup-modal" data-postid="<?php echo $post['_id'];?>" href="javascript:void(0)">&nbsp;</a>
                            </div>
                                <?php if($post['image'] != null) {
                                    $eximgs = explode(',',$post['image'],-1);
                                    $imagesbolk = array();
                                    foreach ($eximgs as $eximg) {
                                    if (file_exists('../web'.$eximg)) {
                                    $picsize = '';
                                    $imagesbolk[] = '1';
                                    $val = getimagesize('../web'.$eximg);
                                    $picsize .= $val[0] .'x'. $val[1] .', ';
                                    $iname = $this->getimagename($eximg);
                                    $inameclass = $this->getimagefilename($eximg);
                                    $pinit = PinImage::find()->where(['user_id' => "$user_id",'imagename' => $iname,'is_saved' => '1'])->one();
                                    if($pinit){ $pinval = 'pin';} else {$pinval = 'unpin';}
                                    if($val[0] > $val[1]){$imgclass = 'himg';}else if($val[1] > $val[0]){$imgclass = 'vimg';}else{$imgclass = 'himg';}
                                ?>
                                <div class="post-img-holder">
                                    <div class="post-img <?=count($imagesbolk)?> lgt-gallery lgt-gallery-photo dis-none">
                                        <a href="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" data-imgid="<?=$inameclass?>" data-sizes="<?=$last_insert_id?>|||<?=$tableLabel?>" class="allow-gallery imgpin pimg-holder <?= $imgclass?>-box">
                                            <img src="<?= Yii::$app->getUrlManager()->getBaseUrl() ?><?= $eximg ?>" class="<?= $imgclass?>"/>
                                        </a>
                                    </div>
                                </div>
                                <?php } } } ?>
                            
                        </div>
                        <?php } } ?>

                        <?php if($post['user']['status']=='44') { $page_details = Page::Pagedetails($post['page_id']); $pageownerid = $page_details['created_by']; }
                        else { $pageownerid = ''; } ?>

                        <div class="clear"></div>
                        <div class="post-data">
                            <div class="post-actions">
                            <div class="right like-tooltip">
                                    <?php
                                        $like_active = '';
                                        $post_id = $post['_id'];
                                        $like_names = Like::getLikeUserNames((string)$post['_id']);
                                        $like_buddies = Like::getLikeUser((string)$post['_id']);
                                        $newlike_buddies = array();
                                        foreach($like_buddies as $like_buddy) {
                                        $like_active = Like::find()->where(['post_id' => "$post_id",'status' => '1','user_id' => (string) $user_id])->one();
                                        if(!empty($like_active))
                                        {
                                            $like_active = 'active';
                                        }
                                        else
                                        {
                                            $like_active = '';
                                        }   
                                            $newlike_buddies[] = ucfirst($like_buddy['user']['fname']). ' '.ucfirst($like_buddy['user']['lname']);
                                        }
                                        $newlike_buddies = implode('<br/>', $newlike_buddies);
                                    ?>
                                        <a href="javascript:void(0)" onclick="doLike('<?=$post['_id']?>');" class="pa-like liveliketooltip liketitle_<?=$post['_id']?> <?=$like_active?>"  data-title='<?=$newlike_buddies?>'>
                                            <i class="zmdi zmdi-thumb-up"></i>
                                        </a>
                                        <span class="tooltip_content lcount likecount_<?=$post['_id']?>"> <?php if($like_count >0 ) { echo $like_count; } ?></span>
                                                            
                                <?php if($post['comment_setting'] != 'Disable'){?>

                                <a href="javascript:void(0)" class="pa-comment <?=$Auth?>" data-id="<?php echo $post['_id'];?>" title="Comment">
                                    <i class="zmdi zmdi-comment"></i>
                                </a>
                                <?php if((count($comments)) > 0){ ?>
                                <span class="comment-lcount commentcountdisplay_<?=$post['_id']?>">
                                    <?php echo count($comments);?>
                                </span>
                                <?php } ?>

                                <?php } ?>
                                
                                <?php 
                                $notifica = Notification::find()->where(['notification_type' => 'editpostuser','post_id' => (string)$post['_id']])->one();
                                if($notifica && $status == '10' && $post['is_deleted'] == '2')
                                { ?>
                                    <a id="publishposticon" href="javascript:void(0)" onclick="publishpost('<?php echo $post['_id'];?>')" class="pa-publish" title="Publish"><i class="zmdi zmdi-upload"></i></a>
                                <?php } ?>                              
                            </div>
                            </div>
                                <div class="comments-section panel">
                                <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                                <div class="comments-area">
                                <?php } ?>
                                <?php if(count($init_comments)>0){ ?>
                                <div class="post-more">
                                    <?php if(count($init_comments)>2){ ?>
                                    <a href="javascript:void(0)" class="view-morec-<?php echo $post['_id']; ?>" onclick="showPreviousComments('<?php echo $post['_id'];?>')">View more comments <i class="zmdi zmdi mdi-chevron-right trun90"></i></a>
                                    <?php } ?>
                                    <span class="total-comments commment-ctr-<?php echo $post['_id'];?>" id="commment-ctr-<?php echo $post['_id'];?>"><font class="ctrdis_<?=$post['_id']?>"><?php echo count($init_comments);?></font> of <font class="countdisplay_<?=$post['_id']?>"><?php echo count($comments);?></font></span>
                                </div>
                                <?php } ?>
                                <input type="hidden" name="from_ctr" id="from_ctr_<?php echo $post['_id'];?>" value="<?php echo count($init_comments);?>">      
                                <input type="hidden" name="to_ctr" id="to_ctr_<?php echo $post['_id'];?>" value="<?php echo count($comments);?>">

                                <div class="post-comments post_comment_<?=$post['_id']?>">                     
                                    <div class="pcomments sub_post_comment_<?=$post['_id']?>">                                    
                                        <?php
                                        if(count($init_comments)>0) { 
                                            foreach($init_comments as $init_comment) {
                                                $cmnt_id = (string) $init_comment['_id'];
                                                $cmnt_like_active= '';
                                                $cmnt_like_active = Like::find()->where(['comment_id' => $cmnt_id,'status' => '1','user_id' => (string) $user_id])->one();
                                                if(!empty($cmnt_like_active))
                                                {
                                                    $cmnt_like_active = 'active';
                                                }
                                                else
                                                {
                                                    $cmnt_like_active = '';
                                                }
                                                $comment_time = Yii::$app->EphocTime->comment_time(time(),$init_comment['updated_date']);$commentcount =Comment::getCommentReply($init_comment['_id']);
                                                $hidecomments = new HideComment();
                                                $hidecomments = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                $hide_comments_ids = explode(',',$hidecomments['comment_ids']);
                                                if(!(in_array($init_comment['_id'],$hide_comments_ids)))
                                                {
                                                    if(($userid == $post['post_user_id']) || ($userid == $init_comment['user']['_id']))
                                                    {
                                                        $afun_post = 'deleteComment';
                                                        $atool_post = 'Delete';
                                                    }
                                                    else
                                                    {
                                                        $afun_post = 'hideComment';
                                                        $atool_post = 'Hide';
                                                    }
                                        ?>
                                        <div class="pcomment-holder <?php if(count($commentcount) > 0){ ?>has-comments<?php } ?>" id="comment_<?php echo $init_comment['_id']?>">
                                            <div class="pcomment main-comment">
                                                <div class="img-holder">
                                                    <div id="commentptip-4" class="profiletipholder">
                                                        <span class="profile-tooltip">
                                                            <?php $init_comment_img = $this->getimage($init_comment['user']['_id'],'thumb'); ?>
                                                            <img class="circle" src="<?= $init_comment_img?>"/>
                                                        </span>
                                                    </div>
                                                </div> 
                                                <div class="desc-holder">
                                                    <div class="normal-mode">
                                                        <div class="desc">
                                                            <a href="<?php $id = $init_comment['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>" class="userlink"><?php echo ucfirst($init_comment['user']['fname']).' '.ucfirst($init_comment['user']['lname']);?></a>
                                                            <?php if(strlen($init_comment['comment'])>200){ ?>
                                                                <p class="shorten" data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?>
                                                                <a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                </p>
                                                            <?php }else{ ?>                                                     
                                                                <p data-id="<?php echo $init_comment['_id'];?>" id="text_<?= $init_comment['_id']?>"><?php echo $init_comment['comment']?></p>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="comment-stuff">
                                                            <div class="more-opt">
                                                                <span id="likeholder-7" class="likeholder">
                                                                <?php

                                                                     
                                                                    $like_count = Like::find()->where(['comment_id' => (string) $init_comment['_id']  ,'status' => '1'])->all();

                                                                    $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $init_comment['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                                    $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                                    $usrbox = array();
                                                                    foreach ($comlikeuseinfo as $key => $single) {
                                                                        $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                                         $usrbox[(string)$single['_id']] = $fullnm; 
                                                                    }
                                                                    
                                                                    $newlike_buddies = implode("<br/>", $usrbox);
                                                                ?>

                                                                        <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$init_comment['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $init_comment['_id']?>')">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                </span> 
                                                                <a href="javascript:void(0)" class="pa-reply reply-comment" title="Reply"><i class="zmdi zmdi-mail-reply"></i></a>
                                                                
                                                                <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                    <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev1<?=$rand?>">
                                                                        <i class="mdi mdi-dots-vertical"></i>
                                                                    </a>
                                                                    <?php if($status == '10') { ?> 
                                                                    <ul class="dropdown-content" id="dropdown-editdeleterev1<?=$rand?>">
                                                                       <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')">Flag</a></li></ul>
                                                                    <?php } else { ?>
                                                                    <ul class="dropdown-content" id="dropdown-editdeleterev1<?=$rand?>">
                                                                        <?php if(($userid != $post['post_user_id']) && ($userid != $init_comment['user']['_id'])){?>
                                                                            <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$atool_post?></a></li>
                                                                            <?php } else { ?>
                                                                            <?php if($userid == $init_comment['user']['_id']){ ?>
                                                                            <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                            <?php } ?>
                                                                            <li><a href="javascript:void(0)" class="delete-comment" onclick="<?= $afun_post?>('<?php echo $init_comment['_id']?>','<?=$post['_id']?>')">Delete</a></li>                                                  
                                                                        <?php } ?>
                                                                    </ul>
                                                                    <?php } ?>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="less-opt"><div class="timestamp"><?php echo $comment_time?></div></div>
                                                        </div>
                                                    </div>
                                                    <div class="edit-mode">
                                                        <div class="desc">
                                                            <textarea class="materialize-textarea editcomment-tt" data-id="<?php echo $init_comment['_id'];?>" id="edit_comment_<?php echo $init_comment['_id'];?>"><?php echo $init_comment['comment']?></textarea>
                                                            <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px   "></i></a>
                                                        </div>                                                                          
                                                    </div>
                                                </div>                              
                                            </div>  
                                            <div class="clear"></div>
                                            
                                            <div class="comment-reply-holder reply_comments_<?php echo $init_comment['_id'];?>">
                                                <?php $comment_replies =Comment::getCommentReply($init_comment['_id']);
                                                    if(count($comment_replies)>0) {
                                                    $lastcomment = Comment::find()->where(['parent_comment_id' => (string)$init_comment['_id']])->orderBy(['updated_date'=>SORT_DESC])->one();
                                                    $last_comment_time = Yii::$app->EphocTime->comment_time(time(),$lastcomment['updated_date']);
                                                ?>
                                                <div class="comments-reply-summery">
                                                    <a href="javascript:void(0)" onclick="openReplies(this)">
                                                        <i class="mdi mdi-share"></i>
                                                        <?php echo count($comment_replies); 
                                                        if(count($comment_replies)>1) { echo " Replies"; }
                                                        else { echo " Reply"; } ?>
                                                    </a>
                                                    <i class="mdi mdi-bullseye dot-i"></i>
                                                    <?php echo $last_comment_time;?>
                                                </div>
                                                <?php }
                                                if(!empty($comment_replies))
                                                {
                                                    foreach($comment_replies AS $comment_reply) { 
                                                    $hidecomment = new HideComment();
                                                    $hidecomment = HideComment::find()->where(['user_id' => (string)$user_id])->one();
                                                    $hide_comment_ids = explode(',',$hidecomment['comment_ids']); 
                                                    if(!(in_array($comment_reply['_id'],$hide_comment_ids)))
                                                    {
                                                        if(($userid == $post['post_user_id']) || ($userid == $comment_reply['user']['_id']))
                                                        {
                                                            $bfun_post = 'deleteComment';
                                                            $btool_post = 'Delete';
                                                        }
                                                        else
                                                        {
                                                            $bfun_post = 'hideComment';
                                                            $btool_post = 'Hide';
                                                        }
                                                    $comment_time = Yii::$app->EphocTime->comment_time(time(),$comment_reply['updated_date']);

                                                    $like_count = Like::find()->where(['comment_id' => (string) $comment_reply['_id']  ,'status' => '1'])->all();

                                                    $user_ids = ArrayHelper::map(Like::find()->select(['user_id'])->where(['comment_id' => (string) $comment_reply['_id'], 'like_type' => 'comment', 'status' => '1'])->orderBy(['updated_date'=>SORT_DESC])->all(), 'user_id', 'user_id');
                                                    $comlikeuseinfo = UserForm::find()->select(['_id','fname', 'lname'])->asArray()->where(['in','_id',$user_ids])->all();

                                                    $usrbox = array();
                                                    foreach ($comlikeuseinfo as $key => $single) {
                                                    $fullnm = $single['fname'] . ' ' . $single['lname'];
                                                    $usrbox[(string)$single['_id']] = $fullnm; 
                                                    }

                                                    $newlike_buddies = implode("<br/>", $usrbox)

                                            ?>
                                            <div class="comments-reply-details">
                                                <div class="pcomment comment-reply" id="comment_<?php echo $comment_reply['_id']?>">
                                                    <div class="img-holder">
                                                        <div class="profiletipholder" id="commentptip-6">
                                                            <span class="profile-tooltip tooltipstered">
                                                                <img class="circle" src="<?php echo $this->getimage($comment_reply['user']['_id'],'thumb'); ?>">
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="desc-holder">
                                                        <div class="normal-mode">
                                                            <div class="desc">
                                                                <a class="userlink" href="<?php $id = $comment_reply['user']['_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>"><?php echo ucfirst($comment_reply['user']['fname']).' '.ucfirst($comment_reply['user']['lname']);?></a>
                                                                
                                                                <?php if(strlen($comment_reply['comment'])>200){ ?>
                                                                    <p class="shorten" data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?><a href="javascript:void(0)" class="overlay" onclick="explandReadMore(this)"><span class="readlink">Read More</span></a>
                                                                    </p>
                                                                <?php }else{ ?>                                                         
                                                                    <p data-id="<?php echo $comment_reply['_id'];?>" id="text_<?= $comment_reply['_id']?>"><?php echo $comment_reply['comment']?></p>
                                                                <?php } ?>
                                                            </div>
                                                            <div class="comment-stuff">
                                                                <div class="more-opt">
                                                                        <!--<a href="javascript:void(0)" class="pa-like"><span>icon</span></a>-->
                                                                        <span class="likeholder" id="likeholder-6">
                                                                        <a href="javascript:void(0)" class="pa-like liveliketooltip commentcounttitle_<?=$comment_reply['_id']?> <?=$cmnt_like_active?>" data-title='<?=$newlike_buddies?>' onclick="likeComment('<?php echo $comment_reply['_id']?>')">
                                                                        <i class="zmdi zmdi-thumb-up"></i>
                                                                        </a>
                                                                        </span>
                                                                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                                                                                <a href="javascript:void(0)" class="dropdown-toggle dropdown-button" data-activates="dropdown-editdeleterev1<?=$rand?>">
                                                                                    <i class="mdi mdi-dots-vertical"></i>
                                                                                </a>
                                                                                <ul class="dropdown-content" id="dropdown-editdeleterev1<?=$rand?>">
                                                                                    <?php if(($userid != $post['post_user_id']) && ($userid != $comment_reply['user']['_id'])){?>
                                                                                        <li><a href="javascript:void(0)" class="close-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')"><span class="glyphicon glyphicon-remove"></span><?=$btool_post?></a></li>
                                                                                        <?php } else { ?>
                                                                                        <?php if($userid == $comment_reply['user']['_id']){ ?>
                                                                                        <li><a href="javascript:void(0)" class="edit-comment">Edit</a></li>
                                                                                        <?php } ?>
                                                                                        <li><a href="javascript:void(0)" class="delete-comment <?=$Auth?> directcheckuserauthclass" onclick="<?= $bfun_post?>('<?php echo $comment_reply['_id']?>')">Delete</a></li>                                                  
                                                                                    <?php } ?>
                                                                                </ul>
                                                                        </div>
                                                                </div>
                                                                <div class="less-opt"><div class="timestamp"><?= $comment_time;?></div></div>
                                                            </div>  
                                                        </div>
                                                        <div class="edit-mode">
                                                            <div class="desc">
                                                                <textarea class="materialize-textarea editcomment-tt" data-id="<?php echo $comment_reply['_id'];?>" id="edit_comment_<?php echo $comment_reply['_id'];?>"><?php echo $comment_reply['comment']?></textarea>
                                                                <a class="editcomment-cancel waves-effect waves-light" href="javascript:void(0)"><i class="mdi mdi-close mdi-20px   "></i></a>
                                                            </div>                                                                          
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php } } } ?>
                                            </div>
                                            
                                            <div class="comment-reply-holder comment-addreply">                                 
                                                <div class="addnew-comment comment-reply">                          
                                                    <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $this->getimage($result['_id'],'thumb');?>"/></a></div>
                                                    <div class="desc-holder">                                   
                                                            <textarea name="reply_txt" placeholder="Write a reply" data-postid="<?php echo $post['_id'];?>" data-commentid="<?php echo $init_comment['_id']?>" id="reply_txt_<?php echo $init_comment['_id'];?>" class="materialize-textarea reply_class capitalize"></textarea>
                                                        
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>
                                        <?php  } } } ?>
                                    </div>
                                    <?php if($post['comment_setting'] != 'Disable'){
                                    if(isset($post['trav_item']) && $post['trav_item']== '1')
                                    {
                                        $comment_placeholder = "Ask question or send query";
                                    }
                                    else{
                                        $comment_placeholder = "Write a comment";
                                    }   
                                    ?>
                                    <div class="addnew-comment valign-wrapper">
                                        <?php $comment_image = $this->getimage($result['_id'],'thumb'); ?>
                                        <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $comment_image?>"/></a></div>
                                        <form name="imageCommentForm" id="imageCommentForm" enctype="multipart/form-data">
                                            <div class="desc-holder">                                   
                                                <textarea data-postid="<?php echo $post['_id'];?>" id="comment_txt_<?php echo $post['_id'];?>" placeholder="<?= $comment_placeholder;?>" class="materialize-textarea comment_class capitalize"></textarea>
                                            </div>
                                            <?php if($existing_posts == ''){ ?>
                                                <input type="hidden" id="last_inserted" name="last_inserted" value="<?php echo $post['_id']; ?>"/>
                                            <?php } ?>
                                        </form>
                                    </div>
                                    <?php } ?>
                                </div>
                                <?php if((isset($post['is_page_review']) && !empty($post['is_page_review'])) || (isset($post['placetype']) && !empty($post['placetype']))) { ?>
                                </div>
                                <?php } ?>
                                </div>
                        </div>
                    </div>
        <?php }    
            }      
        }  
    }
    
    public function display_last_referal($last_insert_id, $existing_posts = '', $imagefullarray = '', $cls='', $total='', $tempocls='')
    {
        $isemptypostreply = true;
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $url = $_SERVER['HTTP_REFERER'];
        $urls = explode('&',$url);
        $url = explode('=',$urls[1]);
        $wall_user_id = $url[1];
        $post = Referal::find()->where(['_id' => $last_insert_id])->one();
        $post_id = (string)$post['_id']; 
        $uniqId = rand(999, 99999).$post_id;
        $post_reply = Referal::find()->where(['referal_id'=>"$post_id",'is_deleted'=>'1'])->one();
        if(!empty($post_reply)) {
            $isemptypostreply = false;
        }
        $dp = $this->getimage($post['sender_id'],'photo');
        $login_dp = $this->getimage($user_id,'photo');
        $time = Yii::$app->EphocTime->time_elapsed_A(time(),$post['created_date']);
        $time_reply = Yii::$app->EphocTime->time_elapsed_A(time(),$post_reply['created_date']);
    ?>
        <div class="post-holder refer-post bshadow dr_<?= $post_id;?>">
            <div class="post-topbar">
                <div class="post-userinfo">
                    <div class="img-holder">
                        <div id="profiletip-1" class="profiletipholder">
                            <span class="profile-tooltip tooltipstered">
                                <img class="circle" src="<?= $dp;?>">
                            </span>
                        </div>
                    </div>
                    <div class="desc-holder">
                        <a href="<?php $id = $post['sender_id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>"><?= $this->getuserdata($post['sender_id'],'fullname');?></a>
                        <span class="timestamp"><?= $time;?><span class="glyphicon glyphicon-globe"></span></span>
                    </div>
                </div>
                <?php if($post['sender_id'] == $user_id) {?>
                    <?php if(!$post_reply){ ?>
                    <div class="settings-icon">
                        <div class="dropdown dropdown-custom dropdown-xxsmall">
                            <a href="javascript:void(0)" data-activates="<?=$uniqId?>">
                                <i class="zmdi zmdi-more-vert zmdi-hc-lg trun90"></i>
                            </a>
                            <ul id="<?=$uniqId?>" class="dropdown-content custom_dropdown width240">
                                <li class="nicon"><a href="javascript:void(0)" onclick="delete_referencce('<?= $post_id;?>')">Delete reference</a></li>
                            </ul>
                        </div>
                    </div>
                    <?php }?>
                <?php }?>
            </div>
            <div class="post-content">
                <div class="post-details">
                    <div class="post-feedback">
                        <div class="feedback <?= lcfirst($post['recommend']);?>">
                            <i class="mdi mdi-star<?php if($post['recommend']=='Negative'){echo "-o";}?>"></i> <?= ucfirst($post['recommend']);?>
                        </div>
                    </div>
                    <div class="post-desc">
                        <?php if(strlen($post['referal_text'])>187){ ?>
                            <div class="para-section">
                                <div class="para">
                                    <p><?= $post['referal_text'];?></p>
                                </div>
                                <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                            </div>
                        <?php }else{ ?>                                     
                            <p><?= $post['referal_text'];?></p>
                        <?php } ?>
                    </div>
                    <div class="reply-holder reply_<?= $post['_id'];?>" <?php if(!$post_reply){ ?> style="display:none;" <?php } ?>>
                        <div class="feedback-reply">
                            <div class="post-topbar">
                                <div class="post-userinfo">
                                    <div class="img-holder">
                                        <div id="profiletip-1" class="profiletipholder">
                                            <span class="profile-tooltip tooltipstered">
                                                <img class="circle" src="<?= $this->getimage($post_reply['sender_id'],'photo');?>">
                                            </span>
                                            
                                        </div>
                                        
                                    </div>
                                    <div class="desc-holder">
                                        <a href="<?php $ids = $post_reply['sender_id']; echo Url::to(['userwall/index', 'id' => "$ids"]); ?>"><?= $this->getuserdata($post_reply['sender_id'],'fullname');?></a>
                                        <span class="timestamp"><?= $time_reply;?></span>
                                    </div>
                                </div>
                                <?php if($post_reply['sender_id'] == $user_id) {?>
                                <div class="settings-icon">
                                    <div class="dropdown dropdown-custom dropdown-xxsmall">
                                    <a href="javascript:void(0)" onclick="replyDelete('<?= $post_reply['_id'];?>','<?= $post['_id'];?>')"><i class="zmdi zmdi-delete mdi-16px"></i></a>
                                </div>
                                </div>
                                <?php }?>
                            </div>
                            
                            <div class="post-content">
                                <div class="post-details">              
                                    <div class="post-desc">
                                        <i class="mdi mdi-share"></i>
                                        <div class="normal-mode">
                                            <?php if(strlen($post_reply['referal_text'])>187){ ?>
                                                <div class="para-section">
                                                    <div class="para">
                                                        <p><?= $post_reply['referal_text'];?></p>                           
                                                    </div>
                                                    <a href="javascript:void(0)" onclick="showAllContent(this)">Read More</a>
                                                </div>
                                            <?php }else{ ?>                                     
                                                <p><?= $post_reply['referal_text'];?></p>                           
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>                                                                           
            </div>
            <div class="clear"></div>
            <?php if($wall_user_id == $user_id) { 
                if($isemptypostreply) { ?>           
                <div class="post-data post_comment_<?= $post['_id'];?>">
                    <div class="post-comments">                                       
                        <div class="addnew-comment">                            
                            <div class="img-holder"><a href="javascript:void(0)"><img src="<?= $login_dp;?>"></a></div>
                            <div class="desc-holder">                                   
                                <div class="sliding-middle-out anim-area tt-holder">
                                    <textarea class="materialize-textarea" na="<?= $post['_id'];?>" id="comment_txt_<?= $post['_id'];?>" placeholder="Write a reply..."></textarea>
                                </div>
                            </div>             
                        </div>       
                    </div>                      
                </div>
            <?php } } ?>        
        </div>
    <?php                                               
    }    
    
    public function getshortcityname($location)         
    {
        $location = isset($location) ? $location : '';
        $location = trim($location);
        if($location != '') {
            $location = explode(",", $location);
            if(count($location) >1) {
                $first = reset($location);
                $last = end($location);
                $location = $first.', '.$last;
            } else {
                $location = implode(", ", $location);
            }
        }
        return $location;
    }
    
    public function getwelcomebox($type){
        
        if($type == "collection"){ ?>
                <div class="post-holder bshadow">      
                    <div class="joined-tb">
                        <i class="mdi mdi-folder-open"></i>        
                        <h4>Welcome to Iaminaqaba</h4>
                        <p>Be the first one to add a post to this collection</p>
                    </div>    
                </div>
        <?php 
        } else if($type == "tripexperience"){
        ?>
            <div class="post-holder bshadow">      
                <div class="joined-tb">
                    <i class="mdi mdi-file-document"></i>        
                    <h4>Welcome to Iaminaqaba</h4>
                    <p>Add your trip experience to let the world</p>
                </div>    
            </div>
        <?php 
         } else if($type == "page"){
        ?>
            <div class="post-holder bshadow">      
                <div class="joined-tb">
                    <i class="mdi mdi-file-outline"></i>        
                    <h4>Welcome to Iaminaqaba</h4>
                    <p>Add post to see more posts and photos on business page</p>
                </div>    
            </div>
        <?php 
         } else if($type == "review-page"){
        ?>
            <div class="post-holder bshadow">      
                <div class="joined-tb">
                    <i class="mdi mdi-star"></i>        
                    <h4>Welcome to Iaminaqaba</h4>
                    <p>Be the first to review page</p>
                </div>    
            </div>
        <?php 
         }else{
        ?>
        <div class="col s12 m12">
            <div class="post-holder bshadow">      
                <div class="joined-tb">
                    <i class="mdi mdi-home"></i>        
                    <h4>Welcome to Iaminaqaba</h4>
                    <p>Add connections to see more posts and photos in your feed</p>
                </div>    
            </div>
        </div>
        <?php 
         }      
    }
    
    public function getprofilepercentage($user_id)
    {
        $percentage = '';
        $isvip = Vip::isVIP($user_id);
        $isVerify = Verify::isVerify($user_id);
        $result = LoginForm::find()->where(['_id' => $user_id])->one();
        $record_personal = Personalinfo::find()->where(['user_id' => "$user_id"])->one();
        
        if(isset($isvip) && !empty($isvip) && $isvip == '1')
        {
            $percentage = $percentage + 20;
        }
        if(isset($isVerify) && !empty($isVerify) && $isVerify == '1')
        {
            $percentage = $percentage + 10;
        }
        if(isset($result['photo']) && !empty($result['photo']))
        {
            $percentage = $percentage + 10;
        }
        if(isset($result['fname']) && !empty($result['fname']))
        {
            $percentage = $percentage + 5;
        }
        if(isset($result['lname']) && !empty($result['lname']))
        {
            $percentage = $percentage + 5;
        }
        if(isset($result['birth_date']) && !empty($result['birth_date']))
        {
            $percentage = $percentage + 5;
        }
        if(isset($result['country']) && !empty($result['country']))
        {
            $percentage = $percentage + 5;
        }
        if(isset($result['phone']) && !empty($result['phone']))
        {
            $percentage = $percentage + 5;
        }
        if(isset($record_personal['about']) && !empty($record_personal['about']))
        {
            $percentage = $percentage + 10;
        }
        if(isset($record_personal['education']) && !empty($record_personal['education']))
        {
            $percentage = $percentage + 5;
        }
        if(isset($record_personal['interests']) && !empty($record_personal['interests']))
        {
            $percentage = $percentage + 5;
        }
        if(isset($record_personal['occupation']) && !empty($record_personal['occupation']))
        {
            $percentage = $percentage + 5;
        }
        if(isset($record_personal['language']) && !empty($record_personal['language']))
        {
            $percentage = $percentage + 5;
        }
        return $percentage;
    }
    
    public function usercleanup()
    {
        $all_users = LoginForm::find()->all();
        $all_connections = Connect::find()->all();
        $all_likes = Like::find()->all();
        $all_comments = Comment::find()->all();
        $all_posts = PostForm::find()->all();
        
        $connect = array();
        $result = array();
        $likes = array();
        $comments = array();
        $posts = array();
        $delete_connections = array();
        $delete_likes = array();
        $delete_comments = array();
        $delete_posts = array();
        
        /* Start Delete User From Connections */
        
        foreach($all_connections as $connect1)
        {
            $connect[] = $connect1['from_id'];
        }
        foreach($all_users as $result1)
        {
            $result[] = (string) $result1['_id'];
        }
        
        $delete_connections = array_diff($connect,$result);
        
        foreach($delete_connections as $delete_connect2)
        {
            Connect::deleteAll(['from_id' => $delete_connect2]);
            Connect::deleteAll(['to_id' => $delete_connect2]);
        }
        
        /* End Delete User From Connections */
        
        /* Start Delete Likes From User_likes */
        foreach($all_likes as $likes1)
        {
            $likes[] = $likes1['user_id'];
        }
        
        $delete_likes = array_diff($likes,$result);
        
        foreach($delete_likes as $delete_likes2)
        {
            Like::deleteAll(['user_id' => $delete_likes2]);
        }
        
        /* End Delete Likes From User_likes */
        
        /* Start Delete Comments From Post_comments */
        
        foreach($all_comments as $comments1)
        {
            $comments[] = $comments1['user_id'];
        }
        
        $delete_comments = array_diff($comments,$result);
        
        foreach($delete_comments as $delete_comments2)
        {
            Comment::deleteAll(['user_id' => $delete_comments2]);
        }
        
        /* End Delete Comments From Post_comments */
        
        /* Start Delete posts From user_Post */
        
        foreach($all_posts as $post1)
        {
            $posts[] = $post1['post_user_id'];
        }
        
        $delete_posts = array_diff($posts,$result);
        
        foreach($delete_posts as $delete_posts2)
        {
            PostForm::deleteAll(['post_user_id' => $delete_posts2]);
        }
        PostForm::deleteAll(['is_album' => "1"]);
        
        PostForm::deleteAll(['is_album' => "0"]);
        
        /* End Delete Comments From Post_comments */
    }
    
    public function getnolistfound($type){
		 if($type == "noconnectfound"){ 
			//echo '<div class="no-listcontent">No connect found</div>';
			?>
			<div class="post-holder bshadow">       
					<div class="joined-tb">
						<i class="mdi mdi-file-outline"></i>        
						<p>No connect found</p>
					</div>    
				</div>
			<?php	
		 }
		 else if($type == "nopinnedphotos"){ 
			?>
			<div class="post-holder bshadow">      
					<div class="joined-tb">
						<i class="mdi mdi-file-outline"></i>        
						<p>No pinned photos</p>
					</div>    
				</div>
			<?php
		 }
		 else if($type == "nophotosaved"){ 
			?>
			<div class="post-holder bshadow">      
					<div class="joined-tb">
						<i class="mdi mdi-file-outline"></i>        
						<p>No posts has been saved by you yet</p>
					</div>    
			</div>
			<?php
		 }
		 else if($type == "norecentlyaddedconnect"){ 
			?>
			<div class="post-holder bshadow">      
					<div class="joined-tb">
						<i class="mdi mdi-file-outline"></i>        
						<p>User has not recently added connect in last month</p>
					</div>    
				</div>
			<?php
		 }
		 else if($type == "noupcomingbirthdays"){ 
			?>
			<div class="post-holder bshadow">      
					<div class="joined-tb">
						<i class="mdi mdi-file-outline"></i>        
						<p>No upcoming birthdays for this month</p>
					</div>    
				</div>
			<?php
		 }
		 else if($type == "nophotofound"){ 
			?>
			<div class="post-holder bshadow">      
					<div class="joined-tb">
						<i class="mdi mdi-file-outline"></i>        
						<p>No photos found</p>
					</div>    
				</div>
			<?php
		 }
		 else if($type == "nolikefound"){ 
			?>
			<div class="post-holder bshadow">      
					<div class="joined-tb">
						<i class="mdi mdi-file-outline"></i>        
						<p>No likes found</p>
					</div>    
				</div>
			<?php
		 }
		 else if($type == "norefersfound"){ 
			?>
			<div class="post-holder bshadow">      
					<div class="joined-tb">
						<i class="mdi mdi-file-outline"></i>        
						<p>No refers found</p>
					</div>    
				</div>
			<?php
		 }
		 else if($type == "noaccesstoviewevent"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No access to view it</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "noconnectrequestfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No new Connect Request</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nonotificationfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No New Notifications</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nopagefound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No page found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "noendorsefound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No one has endorsed</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "sameconnections"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>You both have same connections</p>
				</div>    
			</div>
			<?php
			
		 }
		 else if($type == "noconnectionsonline"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No connections online</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "noactivityfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No activity found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "noadcreated"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No ads has been created by you</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nopopularcollection"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No popular collection found</p>
				</div>    
			</div>
			<?php
		 } 
		 else if($type == "noorganizerrequest"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No request found for this event</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "becomefirsttojoinevent"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Become a first to join this event</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "allconnectionsattendingevent"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>All connections attending this event</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "noattendeerequest"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No attendee request</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "somethingwentwrong"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Something is wrong please try after sometimes</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "norecordfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No record found</p>
				</div>    
			</div>
			<?php
		 } 
		 else if($type == "nopeoplefound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No people found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "novisitedpage"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>You don't have visited any page</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "becomefirsttocreatepage"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Become a first to create page</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "noeventexists"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No event exists</p>
				</div>    
			</div>
			<?php
		 } 
		 else if($type == "nomoreconnectfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No more connections found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nophotoadded"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No photos added</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "becomefirsttolikepage"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Become a first to like this page</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "allconnectionslikepage"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>All connections liked this page</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "becomefirsttoreview"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Become a first to write a review this page</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nopromotepagefound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No promoted pages found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "allconnectreviewthispage"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>All connections reviewd this page</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nocityfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No cities found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "noeventfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No events found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nohotelsfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No hotels found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nolocalfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No local found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nolocalsfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No locals found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nophotosfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No photos found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "norestaurantfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No Restaurant found</p>
				</div>    
			</div>
			<?php
		 } 
		 else if($type == "norestaurantsfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No Restaurants found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "norestaurantsfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No Restaurants found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nothingsfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No things found</p>
				</div>    
			</div>
			<?php
		 }
         else if($type == "notransaction"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No transaction in this month</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "comingsoon"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Coming soon</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "notallowtoviewwall"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>You are not allowed to see the user wall as you are blocked</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nocredithistory"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No credit history</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nonewmessage"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No New Messages</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nosearchresultfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Sorry, no search results found</p>
				</div>    
			</div>
			<?php
		 } 
		 else if($type == "nosuggetionforyou"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No suggetion for you</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "nonewconnectrequest"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No new connect request</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "connectionswithall"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>You are connections with all</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "postkeptprivate"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>As post owner has kept this post Private, you will be not able to see this post</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "posthide"){ 
			?>
			<div class="post-holder bshadow">       
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>As you have hide this post, you will be not able to see this post</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "postunfollow"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>As you are unfollow of this post owner, you will be not able to see this post</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "notripfound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No trips found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "notripexperiencefound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No trip experience found</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "notripexperienceaddedbyyou"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No trip experience added by you</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "noreferencefound"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No references</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "photosecurity"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>User has kept security for photo section</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "novisitedplaces"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>No want to visit places has been added</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "becomefirstforask"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Become a first to ask a question for this place</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "becomefirstfortip"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Become a first to write a tip for this place</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "becomefirstforplacereview"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Become a first to review for this place</p>
				</div>    
			</div>
			<?php
		 }
		 else if($type == "becomefirstforplacediscussion"){ 
			?>
			<div class="post-holder bshadow">      
				<div class="joined-tb">
					<i class="mdi mdi-file-outline"></i>        
					<p>Become a first to write a Discussion for this place</p>
				</div>    
			</div>
			<?php
		 }
         else if($type == "notravellerfound"){ 
            ?>
            <div class="post-holder bshadow">      
                <div class="joined-tb">
                    <i class="mdi mdi-file-outline"></i>        
                    <p>No traveller found</p>
                </div>    
            </div>
            <?php
         }
         else {
			 echo '<div class="no-listcontent">No data found </div>';
		 }
	}
	public function GetMap($location)
	{
	?>
		<iframe src="https://maps.google.it/maps?q=<?=$location?>&output=embed" width="100%" height="260" frameborder="0" style="border:0" allowfullscreen></iframe>
	<?php 	
	}  
 

    public function getUserGridLayout($getUsers, $type, $labeltype='') 
    {   
        
        $isEmpty = true; 
        $directauthcall = '';
        $totalUserCount = 0;
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        $online = array(); 

        if(isset($user_id) && $user_id != '') {
            $checkuserauthclass = UserForm::isUserExistByUid($user_id);
        } else {
            $checkuserauthclass = 'checkuserauthclassg';
        }

        if($checkuserauthclass == 'checkuserauthclassg' || $checkuserauthclass == 'checkuserauthclassnv') {
            $directauthcall = $checkuserauthclass . ' directcheckuserauthclass';
        }
        
        $cls = 'col s12 m3 l3 xl3';
        if($type == 'traveller' || $type == 'local') {
            $cls = 'col s12 m4 l3 xl3';
        }

        if(!empty($getUsers)) {
            foreach($getUsers as $getUser) {
                $rand = rand(999, 99999).time();
                if($type == 'local') {
                    $safarUserId = array_values((array)$getUser['_id']);
                    $safarUserId = (string)$safarUserId[0];

                    if($safarUserId == $user_id) {
                        continue;
                    }
                } else if($type == 'wall') {
                    if(isset($getUser['otherid'])) {
                        $safarUserId = $getUser['otherid'];
                    } else if($labeltype != ''){
                        if($user_id == $labeltype) {
                            if($getUser['to_id'] == $user_id) {
                                $safarUserId = $getUser['from_id'];
                            } else {
                                $safarUserId = $getUser['to_id'];
                            }
                        } else {
                            if($getUser['to_id'] == $labeltype) {
                                $safarUserId = $getUser['from_id'];
                            } else {
                                $safarUserId = $getUser['to_id'];
                            }
                        }   
                    } else {
                        $safarUserId = $getUser['to_id'];
                    }
                } else {
                    if(isset($getUser['user_id'])) {
                        $safarUserId = $getUser['user_id'];
                    } else {
                        $safarUserId = $getUser;
                    }
                    if($safarUserId == $user_id) {
                        continue;
                    }
                }

                
                $getUserinfo = LoginForm::find()->where([(string)'_id' => (string)$safarUserId])->asarray()->one();
                
                if($type == 'wall' && $labeltype == 'birthdate') {

                    if(isset($getUserinfo['birth_date']) && $getUserinfo['birth_date'] != '') {
                        if(strpos($getUserinfo['birth_date'], '-')) {
                            $birthArray = explode('-', $getUserinfo['birth_date']);
                        } else if(strpos($getUserinfo['birth_date'], '/')) {
                            $birthArray = explode('/', $getUserinfo['birth_date']);
                        } else {
                            continue;
                        }

                        if(count($birthArray) == 3) {
                            if($birthArray[1] != date('m')){
                                continue;
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                }

                $thumbnail = $this->getimage($safarUserId, 'photo');            
                $name = $getUserinfo['fullname'];
                
                $city = isset($getUserinfo['city']) ? $getUserinfo['city'] : '';
                $country  = isset($getUserinfo['country']) ? $getUserinfo['country'] : '';
                $address = $city.', '.$country;
                $address = trim($address);
                $address = explode(",", $address);
                $address = array_filter($address);
                $addressLabel = '&nbsp;';
                if(count($address) >1) {
                    $first = reset($address);
                    $last = end($address);
                    $addressLabel = 'Lives in ' . $first.', '.$last;
                } else if(count($address) == 1) {
                    $addressLabel = 'Lives in '. implode(", ", $address);
                } else {
                    $personalinfo = Personalinfo::find()->where(['user_id' => $safarUserId])->asArray()->one();
                    if(!empty($personalinfo)) {
                        $personalinfo = $personalinfo['occupation'];
                        $personalinfo = explode(',', $personalinfo);
                        $personalinfo = array_values($personalinfo);
                        if(count($personalinfo) >2) {
                            $tempCount = count($personalinfo) - 1;
                            $tempNames = array_slice($personalinfo, 1);

                            $addressLabel = $personalinfo[0] . ' and <a href="javascript:void(0)" class="liveliketooltip" data-title="'.implode('<br/>', $tempNames).'">'.(count($personalinfo) - 1).' others</a>';     
                        } else if (count($personalinfo) >1) {
                            $addressLabel = $personalinfo[0] .' and ' . $personalinfo[1];  
                        } else if (count($personalinfo) == 1) { 
                            $addressLabel = $personalinfo[0];  
                        }
                    }
                }

                $connectinfo = LoginForm::find()->select(['_id','fname','lname','country'])->where([(string)'_id' => $safarUserId])->asarray()->one();
                if(!empty($connectinfo)) {
                    $connectid = array_values((array)$connectinfo['_id']);
                    if(!isset($connectid[0])) {
                        continue;
                    }
                }
                $connectid = (string)$connectid[0];
                $isconnect = Connect::find()->select(['_id'])->where(['from_id' => "$connectid",'to_id' => "$user_id",'status' => '1'])->asarray()->one();
                $isconnectrequestsent = Connect::find()->select(['_id'])->where(['from_id' => "$connectid",'to_id' => "$user_id",'status' => '0'])->asarray()->one();
                
                $isvip = false;
                if(isset($getUserinfo['vip_flag']) && $getUserinfo['vip_flag'] != '0' && $getUserinfo['vip_flag'] != '') {
                    $isvip = true;
                }

                $ctr = Connect::mutualconnectcount($connectid);
                $result_security = SecuritySetting::find()->where(['user_id' => $connectid])->asarray()->one();
                $connect_list = isset($result_security['connect_list']) ? $result_security['connect_list'] : '';
                $mutualLabel =  '';
                $totalconnections = Connect::find()->where(['to_id' => (string)$connectid, 'status' => '1'])->count();
                if($connect_list == 'Public') {
                    if($totalconnections>1) {
                        $mutualLabel = $totalconnections .' Connections';
                    } else if($totalconnections == 1) {
                        $mutualLabel = '1 Connect';
                    }
                } else if($connect_list == 'Private') {
                    if($ctr >0) {
                        $mutualLabel =  $ctr.' Mutual Connections';
                    }
                } else if($connect_list == 'Connections') {
                    if(!empty($isconnect)) {
                        if($totalconnections>1) {
                            $mutualLabel = $totalconnections .' Connections';
                        } else if($totalconnections == 1) {
                            $mutualLabel = '1 Connect';
                        }   
                    } else {
                        if($ctr >0) {
                            $mutualLabel =  $ctr.' Mutual Connections';
                        }   
                    }
                }
                
                $isEmpty = false;
                $mobileArrowdisplay = '-';
                ?>
                <input type="hidden" name="login_id" id="login_id" value="<?php echo $session->get('user_id');?>">
                <!-- <div class="col l3 m4 s12"> -->
                <div class="<?=$cls?> <?=$isvip?>">
                    <div class="person-box"> 
                        <div class="imgholder">
                            <img src="<?=$thumbnail?>" class="main-img"/>
                            <div class="overlay">
                            <?php if($isvip == true) { ?>                                                           
                            <div class="vip-span"><img src="<?=$this->assetsPath?>/vip-tag.png"/></div>
                            <?php } ?>                                      
                            <?php  if(!$isconnect) {
                                    if(!$isconnectrequestsent) { ?>
                                    <div class="add-span add-icon_<?=$safarUserId?>">
                                        <a href="javascript:void(0);" title="Add connect" class="add-icon request_btn gray-text-555 " onclick="addConnect('<?=$safarUserId?>')"><i class="mdi mdi-account-plus"></i></a>
                                        <a href="javascript:void(0)" class="add-btn btn btn-primary directcheckuserauthclass" onclick="addConnect('<?=$safarUserId?>')">Add connect</a>
                                    </div>
                                <?php } else { ?>
                                    <div class="add-span add-icon_<?=$safarUserId?>">
                                        <a href="javascript:void(0);" title="Cancel connect request" class="add-icon directcheckuserauthclass gray-text-555" onclick="removeConnect('<?=$safarUserId?>','<?=$user_id?>','<?=$safarUserId?>', 'cancle_connect_request')"><i class="mdi mdi-account-minus"></i></a>
                                        <a href="javascript:void(0)" class="add-btn btn btn-primary directcheckuserauthclass" onclick="addConnect('<?=$safarUserId?>')">Add connect</a>
                                    </div>
                                <?php } 
                                } else { 
                                    $mobileArrowdisplay = 'dis-block';
                                } ?>
                                <div class="more-span mobile-none-who <?=$mobileArrowdisplay?>">
                                    <div class="dropdown dropdown-custom">
                                        <a href="javascript:void(0)" class="dropdown-button more_btn <?=$checkuserauthclass?> directcheckuserauthclass gray-text-555" data-activates="dropdownlocal<?=$rand?>" onclick="fetchconnectmenu('<?=$safarUserId?>', 'yes', this)"><i class="mdi mdi-chevron-down"></i></a>
                                        <?php 
                                        if($checkuserauthclass != 'checkuserauthclassg' && $checkuserauthclass != 'checkuserauthclassnv') { ?>
                                        <ul class="dropdown-content custom_dropdown fetchconnectmenu" id="dropdownlocal<?=$rand?>">
                                        <center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
                                        </ul>
                                        <?php } ?> 
                                    </div>                                                                  
                                </div>
                            </div>
                        </div>

                        <div class="descholder">
                            <h5>
                                <a href="<?php echo Url::to(['userwall/index', 'id' => $safarUserId]); ?>">
                                    <span class="etext" style="color: #000;"><?=$name?></span>
                                    <?php 
                                    if(array_key_exists($safarUserId, $online)) { ?>
                                        <span class="online-dot"><i class="zmdi zmdi-check"></i></span>
                                    <?php } ?>
                                </a></h5>
                            <p><?=$addressLabel?></p>
                        </div>
                    </div>
                </div>
                <?php 
            }
        } 

        if($isEmpty == true) {
            if($type == 'local') {
                $this->getnolistfound('nolocalsfound');
            } else if($type == 'traveller') {
                $this->getnolistfound('notravellerfound');
            } else if($type == 'wall') {
                if($labeltype == 'birthdate') {
                    $this->getnolistfound('noupcomingbirthdays');
                } else if($labeltype == 'recently') {
                    $this->getnolistfound('norecentlyaddedconnect');
                } else {
                    $this->getnolistfound('noconnectfound');
                }
            }
        } 

    }
 
    public function getUserGridLayout2($getUsers, $type, $labeltype='')
    {
        
        $isEmpty = true; 
        $directauthcall = '';
        $totalUserCount = 0;
        $session = Yii::$app->session;
        $user_id = (string)$session->get('user_id');
        
        if(!empty($getUsers) && count($getUsers)>0) {                 
            foreach($getUsers as $getUser){
                $connect_id = (string) $getUser['_id'];   
                $isverify = Verify::isVerify($connect_id);                                               
                $isvip = Vip::isVIP($connect_id);                                                
                $block = BlockConnect::find()->where(['user_id' => (string)$getUser["id"]])->andwhere(['like','block_ids',(string)$user_id])->one();

                if(!$block)
                {               
                    $is_connect = Connect::find()->where(['from_id' => (string)$user_id, 'to_id' => (string)$getUser["id"], 'status' => '1'])->one();  
                    $is_connect_request_sent = Connect::find()->where(['from_id' => (string)$user_id, 'to_id' => (string)$getUser["id"], 'status' => '0'])->one();
                    $userexist = MuteConnect::find()->where(['user_id' => $getUser['id']])->one();
                    if($userexist) {
                        if (strstr($userexist['mute_ids'], $connectid)) {
                            $getnot = 'no';
                        } else {
                            $getnot = 'exist';
                        }
                    } else {
                        $getnot = 'exist';
                    }
                    
                    $userblock = BlockConnect::find()->where(['user_id' => (string)$user_id])->one();
                    if ($userblock) {
                        if (strstr($userblock['block_ids'], (string)$getUser['id'])) {
                            $getblock = 'no';
                        } else {
                            $getblock = 'exist';
                        }
                    } else {
                        $getblock = 'exist';
                    }
                    $requestexists = Connect::requestexists($getUser['id']);
                    $alreadysend = Connect::requestalreadysend($getUser['id']);
                    $ctr = Connect::mutualconnectcount($getUser['id']);
                    $dp = $this->getimage($getUser['_id'],'photo');
                    
                    $result_security = SecuritySetting::find()->where(['user_id' => $connect_id])->one();
                    if(isset($result_security['my_connect_view_status'])) {
                        if($result_security['my_connect_view_status'] != '') {
                            $my_connect_view_status = $result_security['connect_list'];
                        } else {
                            $my_connect_view_status = 'Public';
                        }
                    } else {
                        $my_connect_view_status = 'Public';
                    }
                    
                    if($result_security) {
                        $lookup_settings = $result_security['my_view_status'];
                    } else {
                        $lookup_settings = 'Public';
                    }
                    $is_connect = Connect::find()->where(['from_id' => $connect_id,'to_id' => (string) $user_id,'status' => '1'])->one();

                    $connect_list = isset($result_security['connect_list']) ? $result_security['connect_list'] : '';
                    $mutualLabel =  '';
                    $totalconnections = Connect::find()->where(['to_id' => (string)$connect_id, 'status' => '1'])->count();

                    $city = isset($getUser['city']) ? $getUser['city'] : '';
                    $country  = isset($getUser['country']) ? $getUser['country'] : '';
                    $address = $city.', '.$country;
                    $address = trim($address);
                    $address = explode(",", $address);
                    $address = array_filter($address);
                    $addressLabel = '&nbsp;';
                    if(count($address) >1) {
                        $first = reset($address);
                        $last = end($address);
                        $addressLabel = 'Lives in ' . $first.', '.$last;
                    } else if(count($address) == 1) {
                        $addressLabel = 'Lives in '. implode(", ", $address);
                    } else {
                        $personalinfo = Personalinfo::find()->where(['user_id' => $connect_id])->asArray()->one();
                        if(!empty($personalinfo)) {
                            $personalinfo = $personalinfo['occupation'];
                            $personalinfo = explode(',', $personalinfo);
                            $personalinfo = array_values($personalinfo);
                            if(count($personalinfo) >2) {
                                $tempCount = count($personalinfo) - 1;
                                $tempNames = array_slice($personalinfo, 1);

                                $addressLabel = $personalinfo[0] . ' and <a href="javascript:void(0)" class="liveliketooltip" data-title="'.implode('<br/>', $tempNames).'">'.(count($personalinfo) - 1).' others</a>';     
                            } else if (count($personalinfo) >1) {
                                $addressLabel = $personalinfo[0] .' and ' . $personalinfo[1];  
                            } else if (count($personalinfo) == 1) { 
                                $addressLabel = $personalinfo[0];  
                            }
                        }
                    }

                    if($connect_list == 'Public') {
                        if($totalconnections>1) {
                            $mutualLabel = $totalconnections .' Connections';
                        } else if($totalconnections == 1) {
                            $mutualLabel = '1 Connect';
                        }
                    } else if($connect_list == 'Private') {
                        if($ctr >0) {
                            $mutualLabel =  $ctr.' Mutual Connections';
                        }
                    } else if($connect_list == 'Connections') {
                        if(!empty($is_connect)) {
                            if($totalconnections>1) {
                                $mutualLabel = $totalconnections .' Connections';
                            } else if($totalconnections == 1) {
                                $mutualLabel = '1 Connect';
                            }   
                        } else {
                            if($ctr >0) {
                                $mutualLabel =  $ctr.' Mutual Connections';
                            }
                        }
                    }
                    if(($lookup_settings == 'Public') || ($lookup_settings == 'Connections' && $is_connect)) 
                    {     
                        $isEmpty = false;                                          
                        ?>
                        <input type="hidden" name="to_id" id="to_id" value="<?php echo $getUser['id'];?>">
                        <div class="grid-box overlay">
                            <div class="connect-box">
                                <div class="imgholder online-img">
                                    <img src="<?= $dp?>"/>
                                    <?php if($isverify) { ?>
                                    <span class="online-mark"><i class="zmdi zmdi-check"></i></span>
                                    <?php } ?>
                                </div>
                                <div class="descholder more-span">
                                    <a href="<?php $id = $getUser['id']; echo Url::to(['userwall/index', 'id' => "$id"]); ?>" class="userlink">
                                        <span><?=$getUser['fullname']?></span>
                                    </a>
                                    <span class="info"><?=$addressLabel?></span>
                                    <?php
                                    if(($my_connect_view_status == 'Public') || ($my_connect_view_status == 'Connections' && ($is_connect || $getUser['id'] == $user_id))){ ?>
                                    <span class="info"><?=$mutualLabel?></span>
                                    <?php } ?>
                                    <?php 
                                    $result_security = SecuritySetting::find()->where(['user_id' => "$id"])->one();
                                    if ($result_security)
                                    {
                                        $request_setting = $result_security['connect_request'];
                                    }
                                    else
                                    {
                                        $request_setting = 'Public';
                                    }
                                                                                
                                    if($getUser['_id'] != $user_id){
                                    if(($request_setting == 'Public') || ($request_setting == 'Connections of Connections' && $ctr > 0)){ 
                                    
                                    if(!$is_connect) { 
                                        if($is_connect_request_sent)
                                        {
                                            $fr_title='Connect request sent';
                                        }
                                        else
                                        {
                                            $fr_title='';
                                        }
                                    ?>
                                    <?php if($is_connect_request_sent){?>
                                    <div class="btn-area 1travconnections_<?=$getUser['id']?>">
                                        <a href="javascript:void(0)" class="gray-text-555" title="<?=$fr_title?>">
                                            <i class="mdi mdi-account-minus sendmsg_<?php echo $getUser['id'];?>" onclick="removeConnect('<?=$getUser['_id']?>','<?=$user_id?>','<?=$getUser['_id']?>', 'cancle_connect_request')"></i>
                                            <i class="mdi mdi-account-plus dis-none people_<?php echo $getUser['id'];?>" onclick="addConnect('<?=$getUser['id']?>')"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                    </div>
                                    <?php } else { ?>
                                    <div class="btn-area 2travconnections_<?=$getUser['id']?>">
                                        <a href="javascript:void(0)" class="gray-text-555" title="<?=$fr_title?>">
                                            <i class="mdi mdi-account-plus people_<?php echo $getUser['id'];?>" onclick="addConnect('<?=$getUser['id']?>')"></i>
                                            <i class="mdi mdi-account-minus dis-none sendmsg_<?php echo $getUser['id'];?>" onclick="removeConnect('<?=$getUser['_id']?>','<?=$user_id?>','<?=$getUser['_id']?>','cancle_connect_request')"></i>
                                        </a>
                                        <a href="javascript:void(0)" class="tb-pyk-remove"></a>
                                    </div>
                                    <?php } } else { ?>                                             
                                    <div class="btn-area 3travconnections_<?=$getUser['id']?> dropdown dropdown-custom">

                                        <a href="javascript:void(0)" class="dropdown-button gray-text-555" data-activates='travconnections_<?=$getUser['id']?>' onclick="fetchconnectmenu('<?=$is_connect['from_id']?>', 'yes', this)">
                                            <i class="mdi mdi-account-plus dis-none people_<?php echo $getUser['id'];?>" onclick="addConnect('<?=$getUser['id']?>')"></i>
                                            <i class="mdi mdi-account-minus dis-none sendmsg_<?php echo $getUser['id'];?>" onclick="removeConnect('<?=$getUser['_id']?>','<?=$user_id?>','<?=$getUser['_id']?>', 'cancle_connect_request')"></i>
                                            <i class="mdi mdi-chevron-down "></i>
                                        </a>
                                        <ul id='travconnections_<?=$getUser['id']?>' class='dropdown-content custom_dropdown fetchconnectmenu'></ul>  
                                    </div>              
                                    <?php } } else { ?>
                                    <div class="dropdown dropdown-custom ">
                                        <a href="javascript:void(0)" class="">
                                            <img onclick="privateMessage()" src="<?=$this->assetsPath?>/private-connect.png"/>
                                        </a>
                                        </div>
                                    <?php } } ?>
                                </div>                                                                                  
                            </div>
                        </div>
                        <?php 
                    } 
                } 
            } 
        } 

        if($isEmpty) {
            if($type == 'travconnections') {
                $this->getnolistfound('nosearchresultfound');
            } else if ($type == 'travpeople') {
                $this->getnolistfound('connectionswithall');
            } else {
                
            }

        }
    }
    
    public function calculateLocalGuideRating($id)
    {
        $totalrating = Referal::getAllReferalscount($id);
        $positive = Referal::getTotalPositiveReferals($id);

        $gap = $totalrating  / 10;

        $diff1 = $gap * 1;
        $diff2 = $gap * 2;
        $diff3 = $gap * 3;
        $diff4 = $gap * 4;
        $diff5 = $gap * 5;
        $diff6 = $gap * 6;
        $diff7 = $gap * 7;
        $diff8 = $gap * 8;
        $diff9 = $gap * 9;
        $diff10 = $gap * 10;

        if($positive <= $diff10){ $route = 10; }
        if($positive <= $diff9) { $route = 9; }
        if($positive <= $diff8) { $route = 8; }
        if($positive <= $diff7) { $route = 7; }
        if($positive <= $diff6) { $route = 6; }
        if($positive <= $diff5) { $route = 5; }
        if($positive <= $diff4) { $route = 4; }
        if($positive <= $diff3) { $route = 3; }
        if($positive <= $diff2) { $route = 2; }
        if($positive <= $diff1) { $route = 1; }

        if($route == 10){
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 9) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-half"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 8) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 7) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-half"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 6) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 5) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-half"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 4) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 3) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star-half"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 2) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 1) {
          $starHTML = '<i class="mdi mdi-star-half"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else {
          $starHTML = '<i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';
        }

        return $starHTML;
    }

    public function calculateLocalDriverRating($id)
    {
        $totalrating = Referal::getAllReferalscount($id);
        $positive = Referal::getTotalPositiveReferals($id);

        $gap = $totalrating  / 10;

        $diff1 = $gap * 1;
        $diff2 = $gap * 2;
        $diff3 = $gap * 3;
        $diff4 = $gap * 4;
        $diff5 = $gap * 5;
        $diff6 = $gap * 6;
        $diff7 = $gap * 7;
        $diff8 = $gap * 8;
        $diff9 = $gap * 9;
        $diff10 = $gap * 10;

        if($positive <= $diff10){ $route = 10; }
        if($positive <= $diff9) { $route = 9; }
        if($positive <= $diff8) { $route = 8; }
        if($positive <= $diff7) { $route = 7; }
        if($positive <= $diff6) { $route = 6; }
        if($positive <= $diff5) { $route = 5; }
        if($positive <= $diff4) { $route = 4; }
        if($positive <= $diff3) { $route = 3; }
        if($positive <= $diff2) { $route = 2; }
        if($positive <= $diff1) { $route = 1; }

        if($route == 10){
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 9) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-half"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 8) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 7) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-half"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 6) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 5) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-half"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 4) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 3) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star-half"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 2) {
          $starHTML = '<i class="mdi mdi-star"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else if($route == 1) {
          $starHTML = '<i class="mdi mdi-star-half"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';

        } else {
          $starHTML = '<i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i><i class="mdi mdi-star-outline"></i>&nbsp;&nbsp;<span>('.$totalrating.')</span>';
        }

        return $starHTML;
    }
}
?>