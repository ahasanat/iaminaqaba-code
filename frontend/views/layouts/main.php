<?php
use frontend\assets\AppAsset; 
use frontend\models\UserSetting;

$asset = frontend\assets\AppAsset::register($this);
$baseUrl = AppAsset::register($this)->baseUrl;
$session = Yii::$app->session;
$user_id = (string)$session->get('user_id');
$thumb = $session->get('thumb');
$fullname = $session->get('fullname');
$_SESSION['loadedAds'] = array();
$theme = 'theme-color';

$cuser_email = $this->context->getuserdata($user_id,'email');
$cuser_fullname = $this->context->getuserdata($user_id,'fullname');
$cuser_thumb = $this->context->getimage($user_id, 'thumb');
$cuser_country = $this->context->getuserdata($user_id,'country');
$userinfo = array('id' => (string)$user_id, 'email'=> $cuser_email, 'fullname' => $cuser_fullname, 'thumb' => $cuser_thumb, 'country' => $cuser_country);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	<meta name='B-verify' content='f6ff550e35a415473235fe823b67cd1a97675712' />
    <link rel="icon" href="<?=$baseUrl?>/images/favicon.ico">
    <title>I am in Aqaba</title>	
	<link href="<?=$baseUrl?>/css/materialize.css" rel="stylesheet">
    <link href="<?=$baseUrl?>/css/material-design-iconic-font.css" rel="stylesheet">    
    <link href="<?=$baseUrl?>/css/materialdesignicons.min.css" rel="stylesheet">    
    <link href="<?=$baseUrl?>/css/tooltipster.bundle.min.css" rel="stylesheet">
    <link href="<?=$baseUrl?>/css/tooltipster-sideTip-borderless.min.css" rel="stylesheet">
    <!-- <link href="<?=$baseUrl?>/css/font-awesome.css" rel="stylesheet"> -->
    <link href="<?=$baseUrl?>/css/template.css" rel="stylesheet">
    <link href="<?=$baseUrl?>/css/themes.css" rel="stylesheet">  
    <link href="<?=$baseUrl?>/css/all-ie-only.css" rel="stylesheet" type="text/css" media="screen"/> 
    <link href="<?=$baseUrl?>/css/master-responsive.css" rel="stylesheet">
    
    <script src="<?=$baseUrl?>/js/jquery.min.js"></script> 
    <script src="<?=$baseUrl?>/js/jquery-ui.min.js"></script>
    <script src="<?=$baseUrl?>/js/materialize.min.js" type="text/javascript" charset="utf-8" ></script>    

    <link href="<?=$baseUrl?>/css/justifiedGallery.css" rel="stylesheet" >
    <link href="<?=$baseUrl?>/css/lightgallery.css" rel="stylesheet">
    <link href="<?=$baseUrl?>/css/lg-transitions.css" rel="stylesheet">
    <link href="<?=$baseUrl?>/css/demo-cover.css" type="text/css" media="screen" rel="stylesheet" />
    <link href="<?=$baseUrl?>/css/datepicker.min.css" type="text/css" media="screen" rel="stylesheet" />
    <link href="<?=$baseUrl?>/css/daterangepicker.css" type="text/css" media="screen" rel="stylesheet" />
    <link href="<?=$baseUrl?>/css/owl.carousel.css" type="text/css" media="screen" rel="stylesheet" />
    <link href="<?=$baseUrl?>/css/nouislider.css" type="text/css" media="screen" rel="stylesheet" />

    <?php $this->head() ?>
    </head>
    
    <body class="<?=$theme?>">
        <!-- IE lower version notice -->
        <div class="ienotice">
            <div class="notice-holder">
                <h5>Iaminaqaba can be best viewed in IE 9 or greater</h5>
                <br />
                <a href="https://www.microsoft.com/en-in/download/internet-explorer.aspx">Update your browser here</a>
            </div>
        </div>
        <!-- end IE lower version notice -->

        <script type="text/javascript">
        var thumb = '<?=isset($thumb) ? $thumb : '';?>';
        var fullname = '<?=isset($fullname) ? $fullname : '';?>';
        var postNumber = 1;
        var currentUserScoketId = '';
        var $getrecentmessageusersid = [];
        var addUserForAccountSettingsArray = [];
        var addUserForAccountSettingsArrayTemp = [];
        
        var customArray = [];
        var customArrayTemp = [];
        
        var $isMapLocationId = '';
        var socket = '';
        var user_id = '<?=$user_id?>';
        var $baseUrl = '<?=$baseUrl?>';
        var $assetsPath = '../../vendor/bower/travel';
        </script>

        <?php $this->beginBody() ?>
            <?=$content?>
        <?php $this->endBody() ?>
        <script type="text/javascript">
            var data2 = <?php echo (isset($userinfo) && !empty($userinfo)) ? json_encode($userinfo) : '';?>;
            if(data2) {
                if (window.location.href.indexOf("localhost") > -1) {
                    var socket = io('http://localhost:4000'); ////////// LOCAL
                } else {
                    var socket = io('https://www.iaminaqaba.com:4000'); ////////// LIVE
                } 
                socket.emit('userInfo', data2);  
            } 
        </script>

        <div id="login_modal" class="modal login-popup home-page popup-area login_modal_general">
            <center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
        </div>

        <!-- open" style="z-index: 1019;display: block;opacity: 1;transform: scaleX(1) translateY(-50%);top: 50%;width: 12%;background: black;" -->
        <div id="receiveCallBox" class="modal">
            <div id="callReceived" style="width: 12%; float: left; margin: 29px 35px; cursor: pointer; hover: red;">
                <i class="zmdi zmdi-phone" style="color: green;"></i>
            </div>
            <div id="callRejected" style="width: 12%; float: left; margin: 30px 16px; cursor: pointer; ">
                <i class="mdi mdi-close	-circle" style="color: red;"></i>
            </div>
        </div>

        <div id="Complete_loged" class="modal custom_modal complete-popup complete-loged">
                <div class="modal_content_container">
                    <div class="modal_content_child modal-content">
                        <div class="custom_modal_content modal_content" id="createpopup">
                            <div class="comp_popup profile-tab">
                                <div class="comp_popup_box detail-box">
                                    <div class="content-holder main-holder">
                                        <div class="summery">
                                            <div class="complete-profile-page">
                                                <div class="signup-part">
                                                    <div class="complete-profile-header" id="wlcmsrnbx">
                                                    </div>
                                                    <div class="box-content">
                                                        <center><div class="lds-css ng-scope"> <div class="lds-rolling lds-rolling100"> <div></div> </div></div></center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        
        <div id="custom_dropdown_modal" class="modal tbpost_modal custom_dropdown_modal"></div>
        
        <div id="datepickerDropdown" class="modal tbpost_modal modal-datepicker modalxii_level1 nice-scroll">
            <div class="content_header">
                <button class="close_span waves-effect">
                <i class="mdi mdi-close mdi-20px material_close resetdatepicker"></i>
                </button>
                <p class="selected_photo_text">Select Date</p>
                <a href="javascript:void(0)" class="done_btn action_btn closedatepicker">Done</a>
            </div>
            <div class="modal-content">
              <div id="datepickerBlock"></div>
            </div>
        </div>   
        
        <!--attachment modal-->
        <div id="compose_visitcountry" class="modal compose_inner_modal modalxii_level1 visit-country">
           <div class="content_header">
              <button class="close_span waves-effect">
              <i class="mdi mdi-close mdi-20px material_close"></i>
              </button>
              <p class="selected_person_text">Select Country</p>
           </div>
           <div class="person_box">
              <div class="collection visit-country-list">
                 <a href="http://www.visitjordan.com" target="_blank" class="collection-item">Visit Jordan</a>
                 <a href="http://www.visitpetra.jo" target="_blank" class="collection-item">Visit Petra</a>
                 <a href="http://na.visitjordan.com/Wheretogo/Aqaba.aspx" target="_blank" class="collection-item">Visit Aqaba</a>
                 <a href="https://rcu.gov.sa/" target="_blank" class="collection-item">Visit Madain</a>
                 <a href="https://www.visitsaudi.com" target="_blank" class="collection-item">Visit Saudi</a>
                 <a href="http://ee.france.fr" target="_blank" class="collection-item">Visit France</a>
                 <a href="https://www.visitdubai.com" target="_blank" class="collection-item">Visit Dubai</a>
                 <a href="http://www.tourspain.org" target="_blank" class="collection-item">Visit Spain</a>
                 <a href="https://www.visitqatar.qa" target="_blank" class="collection-item">Visit Qatar</a>
                 <a href="https://visitbahrain.bh" target="_blank" class="collection-item">Visit Bahrain</a>
                 <a href="https://visitabudhabi.ae" target="_blank" class="collection-item">Visit Abu Dhabi</a>
                 <a href="https://www.visittheusa.com" target="_blank" class="collection-item">Visit USA</a>
                 <a href="https://www.visititaly.eu" target="_blank" class="collection-item">Visit Italy</a>
                 <a href="https://www.visitmexico.com" target="_blank" class="collection-item">Visit Mexico</a>
                 <a href="" target="_blank" class="collection-item">Visit UK</a>
                 <a href="https://visit.istanbul target="_blank" class="collection-item">Visit Istanbul</a>
                 <a href="https://chinatour.net" target="_blank" class="collection-item">Visit China</a>
                 <a href="https://www.germany.travel" target="_blank" class="collection-item">Visit Germany</a>
                 <a href="https://www.tourismthailand.org" target="_blank" class="collection-item">Visit Thailand</a>
                 <a href="https://www.austria.info" target="_blank" class="collection-item">Visit Austria</a>
                 <a href="https://www.visithongkong.gov.hk" target="_blank" class="collection-item">Visit Hong Kong</a>
                 <a href="http://www.visitgreece.gr" target="_blank" class="collection-item">Visit Greece</a>
                 <a href="https://www.visitrussia.com" target="_blank" class="collection-item">Visit Russia</a>
                 <a href="https://us.jnto.go.jp" target="_blank" class="collection-item">Visit Japan</a>
                 <a href="https://www.visitcyprus.com" target="_blank" class="collection-item">Visit Cyprus</a>
                 <a href="https://www.visitjamaica.com" target="_blank" class="collection-item">Visit Jamaica</a>
                 <a href="https://www.visitpoland.com" target="_blank" class="collection-item">Visit Poland</a>
                 <a href="http://visit-hungary.com/budapest" target="_blank" class="collection-item">Visit Hungary</a>
                 <a href="https://www.zimbabwetourism.net" target="_blank" class="collection-item">Visit Zimbabwe</a>
                 <a href="http://visittoukraine.com/en" target="_blank" class="collection-item">Visit Ukraine</a>
                 <a href="https://www.holland.com/" target="_blank" class="collection-item">Visit Netherlands</a>
                 <a href="https://www.indianvisit.com/" target="_blank" class="collection-item">Visit India</a>
                 <a href="https://www.visitsingapore.com/en/" target="_blank" class="collection-item">Visit Singapore</a>
                 <a href="http://www.tourism.gov.pk/" target="_blank" class="collection-item">Visit Pakistan</a>
                 <a href="https://www.visitmorocco.com/en" target="_blank" class="collection-item">Visit Morocco</a>
                 <a href="http://www.egypt.travel/" target="_blank" class="collection-item">Visit Egypt</a>
                 <a href="https://www.visitbrasil.com/" target="_blank" class="collection-item">Visit Brazil</a>
                 <a href="https://www.australia.com/en" target="_blank" class="collection-item">Visit Australia</a>
                 <a href="http://www.visit-malaysia.com/" target="_blank" class="collection-item">Visit Malaysia</a>
                 <a href="https://www.indonesia.travel/gb/en/home" target="_blank" class="collection-item">Visit Indonesia</a>
                 <a href="https://www.argentina.travel/" target="_blank" class="collection-item">Visit Argentina</a>
                 <a href="https://www.zimbabwetourism.net/" target="_blank" class="collection-item">Visit Zimbabwe</a>
                 <a href="https://www.visitmacao.com.au/" target="_blank" class="collection-item">Visit Macao</a>
                 <a href="https://www.visit-croatia.co.uk/" target="_blank" class="collection-item">Visit Croatia</a>
                 <a href="http://english.visitkorea.or.kr/enu/index.kto" target="_blank" class="collection-item">Visit Southkorea</a>
                 <a href="https://www.visitiran.ir/" target="_blank" class="collection-item">Visit Iran</a>
              </div>
           </div>
        </div>

        <div id="compose_iwasincountry" class="modal compose_inner_modal modalxii_level1 visit-country">
           <div class="content_header">
              <button class="close_span waves-effect">
              <i class="mdi mdi-close mdi-20px material_close"></i>
              </button>
              <p class="selected_person_text">Select Destination</p>
           </div>
           <div class="person_box">
              <div class="collection visit-country-list">
                 <a href="https://www.iaminjordan.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Jordan</a>
                 <a href="https://www.iaminpeta.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Petra</a>
                 <a href="https://www.iaminmadain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Madain</a>
                 <a href="https://www.iaminsaudi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Saudi</a>
                 <a href="https://www.iaminfrance.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in France</a>
                 <a href="https://www.iamindubai.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Dubai</a>
                 <a href="https://www.iaminspain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Spain</a>
                 <a href="https://www.iaminqatar.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Qatar</a>
                 <a href="https://www.iaminbahrain.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Bahrain</a>
                 <a href="https://www.iaminabudhabi.com/frontend/web/index.php?r=site%2Fmainfeed" target="_blank" class="collection-item">I am in Abu Dhabi</a>
                 <a href="#!" class="collection-item">I am in USA</a>
                 <a href="#!" class="collection-item">I am in Italy</a>
                 <a href="#!" class="collection-item">I am in Mexico</a>
                 <a href="#!" class="collection-item">I am in UK</a>
                 <a href="#!" class="collection-item">I am in Istanbul</a>
                 <a href="#!" class="collection-item">I am in China</a>
                 <a href="#!" class="collection-item">I am in Germany</a>
                 <a href="#!" class="collection-item">I am in Thailand</a>
                 <a href="#!" class="collection-item">I am in Austria</a>
                 <a href="#!" class="collection-item">I am in Hong Kong</a>
                 <a href="#!" class="collection-item">I am in Greece</a>
                 <a href="#!" class="collection-item">I am in Russia</a>
                 <a href="#!" class="collection-item">I am in Japan</a>
                 <a href="#!" class="collection-item">I am in Cyprus</a>
                 <a href="#!" class="collection-item">I am in Jamaica</a>
                 <a href="#!" class="collection-item">I am in Poland</a>
                 <a href="#!" class="collection-item">I am in Hungary</a>
                 <a href="#!" class="collection-item">I am in Zimbabwe</a>
                 <a href="#!" class="collection-item">I am in Ukraine</a>
                 <a href="#!" class="collection-item">I am in Netherlands</a>
                 <a href="#!" class="collection-item">I am in India</a>
                 <a href="#!" class="collection-item">I am in Singapore</a>
                 <a href="#!" class="collection-item">I am in Morocco</a>
                 <a href="#!" class="collection-item">I am in Egypt</a>
                 <a href="#!" class="collection-item">I am in Brazil</a>
                 <a href="#!" class="collection-item">I am in Australia</a>
                 <a href="#!" class="collection-item">I am in Malaysia</a>
                 <a href="#!" class="collection-item">I am in Indonesia</a>
                 <a href="#!" class="collection-item">I am in Argentina</a>
                 <a href="#!" class="collection-item">I am in Zimbabwe</a>
                 <a href="#!" class="collection-item">I am in Macao</a>
                 <a href="#!" class="collection-item">I am in Croatia</a>
                 <a href="#!" class="collection-item">I am in Southkorea</a>
              </div>
           </div>
        </div>


        <div id="privacymodal" class="modal compose_inner_modal modalxii_level1"></div>
        <?php include('../views/layouts/discardmodal.php'); ?>
        <?php include('../views/layouts/reportmodal.php'); ?>
    </body>
</html>