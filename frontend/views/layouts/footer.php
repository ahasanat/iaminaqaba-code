<?php
use frontend\assets\AppAsset;
$baseUrl = AppAsset::register($this)->baseUrl;
?>
<div class="footer-section">
	<div class="mobile-footer-arrow">
		<div class="mobile-footer">
			<div class="dropdown dropdown-custom mmenu text-center">
				<a href="<?php echo Yii::$app->urlManager->createUrl(['site/mainfeed']); ?>" class="dropdown-toggle"  role="button" aria-haspopup="true" aria-expanded="false">
				<img src="<?=$baseUrl?>/images/foot-home.png" />
					Home
				</a>					
			</div>
			<div class="dropdown-button more_btn footer-menu dropdown-custom mmenu text-center footer_booking">
				<a href="javascript:void(0)" class="dropdown-toggle"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
				<i class="mdi mdi-plus-circle mdi-20px" style="
				    width: 22px;
				    display: block;
				    opacity: 0.6;
				    margin: 3px auto;
				"></i>	
				Booking
				</a> 
				<ul class="dropdown-menu large-menuicons">
					<li>
						<a href="<?php echo Yii::$app->urlManager->createUrl(['site/hotellist']); ?>" title="Hotels"><i class="mdi mdi-hospital-building"></i>Hotels</a>
					</li>
					<li>
						<a href="<?php echo Yii::$app->urlManager->createUrl(['homestay']);?>" title="Homestay"><i class="mdi mdi-hotel blue"></i>Homestay</a>
					</li>
					<li>
						<a href="<?php echo Yii::$app->urlManager->createUrl(['site/restaurantlist']);?>" title="Restaurant"><i class="mdi mdi-silverware"></i>Restaurant</a>
					</li>
					<li>
						<a href="<?php echo Yii::$app->urlManager->createUrl(['localdine']);?>" title="Local Dine"><i class="mdi mdi-basecamp"></i>Local Dine</a>
					</li>
					<li>
						<a href="<?php echo Yii::$app->urlManager->createUrl(['tours']);?>" title="Tours"><i class="mdi mdi-ticket" ></i>Tours</a>
					</li>
					<li>
						<a href="<?php echo Yii::$app->urlManager->createUrl(['camping']);?>" title="Camping"><i class="mdi mdi-terrain"></i>Camping</a>
					</li>
				</ul>
			</div>
			<div class="dropdown dropdown-custom mmenu text-center footer_local">
				<a href="<?php echo Yii::$app->urlManager->createUrl(['locals']); ?>" class="dropdown-toggle" role="button" aria-haspopup="true" aria-expanded="false">
				<img src="<?=$baseUrl?>/images/foot-places.png" />
					Locals
				</a>					
			</div> 
			<div class="dropdown-button more_btn footer-menu dropdown-custom mmenu text-center footer_alert" onclick="openalert()">
				<a href="javascript:void(0)" class="dropdown-toggle">
				<img src="<?=$baseUrl?>/images/foot-more.png" />
					Alert
				</a>
			</div>		
		</div>
		<div class="master_alert">
		   <div class="littlemaster_alert">
		      <a href="<?php echo Yii::$app->urlManager->createUrl(['site/travpeople']); ?>">
		         <span class="icon default-icon">
		            <img src="<?=$baseUrl?>/images/connectreq-icon.png">
		         </span>Connect Requests
		      </a>
		   </div>
		   <div class="littlemaster_alert">
		      <a href="<?php echo Yii::$app->urlManager->createUrl(['site/messages']); ?>">
		         <span class="icon default-icon">
		            <img src="<?=$baseUrl?>/images/message-icon.png">
		         </span>Messages
		      </a>
		   </div>
		   <div class="littlemaster_alert">
		      <a href="<?php echo Yii::$app->urlManager->createUrl(['site/travnotifications']); ?>">
		         <span class="icon default-icon">
		            <img src="<?=$baseUrl?>/images/notification-icon.png">
		         </span>Notifications
		      </a>
		   </div>
		</div>
	</div>
	<div class="main-footer">
		<ul class="center-align">
			<li><a href="javascript:void(0)">Aqaba</a></li>
			<li><a href="javascript:void(0)">Booking</a></li>
			<li><a href="javascript:void(0)">Locals</a></li>
			<li><a href="javascript:void(0)">Alert</a></li>
		</ul>
	</div>
</div>